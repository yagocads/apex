@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> 
                    {{$servico}}
                </h2>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-12">
            <iframe 
                name="bpm-iframe" 
                id="bpm-iframe" 
                src="{{$caminho}}?processInstanceId={{ $processInstanceId }}&activityInstanceId={{ $activityInstanceId }}&cycle={{ $cycle }}&uuid={{ $uuid }}&language=pt_BR&displayFormHeader={{$header}}&highContrast=false" 
                frameborder="0" 
                scrolling="auto" 
                height="100%" 
                width="100%" 
                style="height: {{$altura}};"
                ></iframe>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'rh']) }}" class="btn btn-primary">
                <i class="fa fa-arrow-left"></i> Voltar
            </a>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection
