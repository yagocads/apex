@extends('layouts.tema_principal')

@section('content')
    <style>
        .fase {
            width: 100% !important;
            margin-bottom: 20px;
        }

        .alert {
            margin-top: 15px !important;
            margin-bottom: 15px !important;
        }

        .registro-fase {
            border-bottom: 1px solid #227DC7;
            padding-top: 5px;
            padding-bottom: 10px;
        }

        .registro-fase-topo {
            padding-top: 30px;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3><i class="fa fa-search"></i> Consulta</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('acompanhamento') }}">
                            @csrf

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <div class="col-lg-3 form-group">
                                            <label for="chamado">Nº do Processo:</label>
                                            <input type="text" name="chamado" id="chamado"
                                                class="form-control form-control-sm" autofocus />
                                        </div>

                                        <div class="col-lg-9 form-group">
                                            <label for="responsavel">Responsável:</label>
                                            <input type="text" name="responsavel" id="responsavel"
                                                class="form-control form-control-sm" />
                                        </div>

                                        <div class="col-lg-12 form-group">
                                            <label for="Servico">Serviço:</label>
                                            <input type="text" name="Servico" id="Servico"
                                                class="form-control form-control-sm" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-row">
                                        <div class="col-lg-12 form-group">
                                            <label for="Situacao">Situação:</label>
                                            <select id="Situacao" class="form-control form-control-sm" name="Situacao">
                                                <option value=""></option>

                                                @inject('servicos', 'App\Services\ServicosService')

                                                @foreach ($servicos->fases() as $fase)
                                                    <option value="{{$fase->fase}}">{{$fase->fase}}</option>
                                                @endforeach

                                            </select>
                                        </div>

                                        <div class="col-lg-6 form-group">
                                            <label for="dataIni">Data Inicial:</label>
                                            <input type="date" name="dataIni" id="dataIni"
                                                class="form-control form-control-sm" />
                                        </div>

                                        <div class="col-lg-6 form-group">
                                            <label for="dataFim">Data Final:</label>
                                            <input type="date" name="dataFim" id="dataFim"
                                                class="form-control form-control-sm" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Pesquisar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                @isset($pass)
                @if($pass > 0)
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> Você já tem uma solicitação para este serviço. Verifique o status deste
                    serviço:
                </div>
                <br>
                <br>
                @endif
                @endisset

                @if(count($registros) > 0)
                    @foreach ($registros as $registro)
                        @if( $registro->Serviço == 'Cadastro Geral do Município - PF' || $registro->Serviço == 'Cadastro Geral do Município - PJ')
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>

                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                    @case('Abertura da Solicitação')
                                    @case('Inscrição')
                                    @if( $registro->Serviço == 'Cadastro Geral do Município - PF' )
                                    <img src="{{ asset('img/fase_cgm_01.png') }}" alt="Fase" class="fase" />
                                    @else
                                    <img src="{{ asset('img/fase_01.png') }}" alt="Fase" class="fase" />
                                    @endif
                                    @break
                                    @case('Em Análise')
                                    @case('Análise Inicial')
                                    <img src="{{ asset('img/fase_02.png') }}" alt="Fase" class="fase" />
                                    @break
                                    @case('Execução do Serviço')
                                    @if( $registro->Serviço == 'Cadastro Geral do Município - PF' )
                                    <img src="{{ asset('img/fase_cgm_02.png') }}" alt="Fase" class="fase" />
                                    @else
                                    <img src="{{ asset('img/fase_03.png') }}" alt="Fase" class="fase" />
                                    @endif
                                    @break
                                    @case('Resposta ao Contribuinte')
                                    <img src="{{ asset('img/fase_04.png') }}" alt="Fase" class="fase" />
                                    @break
                                    @case('Cadastro Aprovado')
                                    <img src="{{ asset('img/fase_cgm_04.png') }}" alt="Fase" class="fase" />
                                    @break

                                    @case('Concluído')
                                    @case('Finalizado')
                                    @case('Cadastro Reprovado')
                                    @case('Reprovado')
                                    @case('Aprovado')
                                    @if( $registro->Serviço == 'Cadastro Geral do Município - PF' )
                                    <img src="{{ asset('img/fase_cgm_03.png') }}" alt="Fase" class="fase" />
                                    @else
                                    <img src="{{ asset('img/fase_05.png') }}" alt="Fase" class="fase" />
                                    @endif
                                    @break
                                    @endswitch
                                </center>
                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == "Portal")
                                    <br>
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                        <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar
                                            Solicitação</a>
                                    </div>
                                    @endif
                                @endif

                                @if($registro->motivo != "" || $registro->Fase == "Cadastro Aprovado")
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Atenção!</strong><br>
                                    Esta solicitação foi finalizada e teve como conclusão:<br>
                                    Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                    @if($registro->motivo != "")
                                        Motivo: {{$registro->motivo}}<br>
                                    @endif
                                </div>
                                @endif
                            </div>
                        @elseif( $registro->Serviço == 'Cadastro Geral do Município' )
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>

                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura de Solicitação')
                                            <img src="{{ asset('img/fases_SIM_CGM_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Solicitação em Análise')
                                            <img src="{{ asset('img/fases_SIM_CGM_02.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cumprir Exigências')
                                            <img src="{{ asset('img/fases_SIM_CGM_03.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cadastro Aprovado')
                                            <img src="{{ asset('img/fases_SIM_CGM_04.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cadastro Reprovado')
                                            <img src="{{ asset('img/fases_SIM_CGM_05.png') }}" alt="Fase" class="fase" />
                                            @break
                                    @endswitch
                                </center>

                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == "Portal")
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar
                                                Solicitação</a>
                                        </div>
                                    @endif
                                @endif

                                @if($registro->motivo != "" &&  $registro->FINALIZADO != "andamento")
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Atenção!</strong><br>
                                        Esta solicitação foi finalizada e teve como conclusão:<br>
                                        Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                        @if(trim($registro->motivo) != "")
                                            Motivo: {{$registro->motivo}}<br>
                                        @endif
                                    </div>
                                @endif
                            </div>

                        @elseif( $registro->Serviço == 'Alteração de Vencimento de IPTU - Idoso' )
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>

                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura de Solicitação')
                                            <img src="{{ asset('img/fases_UPTU_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Solicitação em Análise')
                                            <img src="{{ asset('img/fases_UPTU_02.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cumprir Exigências')
                                            <img src="{{ asset('img/fases_UPTU_03.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Venc. IPTU em Análise')
                                            <img src="{{ asset('img/fases_UPTU_04.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Exigências no IPTU')
                                            <img src="{{ asset('img/fases_UPTU_05.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Venc. do IPTU Alterado')
                                            <img src="{{ asset('img/fases_UPTU_06.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Venc. do IPTU Inalterado')
                                            <img src="{{ asset('img/fases_UPTU_07.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cadastro Reprovado')
                                            <img src="{{ asset('img/fases_UPTU_08.png') }}" alt="Fase" class="fase" />
                                            @break
                                    @endswitch
                                </center>

                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == "Portal")
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar
                                                Solicitação</a>
                                        </div>
                                    @endif
                                @endif

                                @if($registro->motivo != "" &&  $registro->FINALIZADO != "andamento")
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Atenção!</strong><br>
                                        Esta solicitação foi finalizada e teve como conclusão:<br>
                                        Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                        @if(trim($registro->motivo) != "")
                                            Motivo: {{$registro->motivo}}<br>
                                        @endif
                                    </div>
                                @endif
                            </div>


                        @elseif( $registro->Serviço == 'Averbar Imóvel' )
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>

                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura da Solicitação')
                                            <img src="{{ asset('img/fases_averbar_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Análise da Documentação')
                                        @case('Análise Documental')
                                            <img src="{{ asset('img/fases_averbar_02.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cumprir Exigências')
                                            <img src="{{ asset('img/fases_averbar_03.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cadastro do CGM')
                                            <img src="{{ asset('img/fases_averbar_04.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Averbação Realizada')
                                            <img src="{{ asset('img/fases_averbar_05.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cancelado')
                                            <img src="{{ asset('img/fases_averbar_06.png') }}" alt="Fase" class="fase" />
                                            @break
                                    @endswitch
                                </center>
                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == "Portal")
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar
                                                Solicitação</a>
                                        </div>
                                    @endif
                                @endif

                                @if($registro->motivo != "" &&  $registro->FINALIZADO != "andamento")
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Atenção!</strong><br>
                                        Esta solicitação foi finalizada e teve como conclusão:<br>
                                        Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                        @if($registro->motivo != "")
                                            Motivo: {{$registro->motivo}}<br>
                                        @endif
                                    </div>
                                @endif

                                @if( $registro->Serviço == 'Averbar Imóvel' )
                                    @if( $registro->Etapa == "11")
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;<br><br>
                                            <strong>Pendências:</strong> {{$registro->motivo}}<br><br>

                                            <a href="{{ route('averbacaoImovel', $registro->Chamado) }}" class="btn btn-blue">Corrigir pendências</a>
                                        </div>
                                    @endif
                                @endif

                            </div>
                        @elseif( $registro->Serviço == 'Solicitação de ITBI' )
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>

                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura da Solicitação')
                                            <img src="{{ asset('img/fases_solicitar_itbi_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Análise da Documentação')
                                        @case('Análise Documental')
                                            <img src="{{ asset('img/fases_solicitar_itbi_02.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cumprir Exigência')
                                        @case('Cumprir Exigências')
                                            <img src="{{ asset('img/fases_solicitar_itbi_03.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cadastro do CGM')
                                            <img src="{{ asset('img/fases_solicitar_itbi_04.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Emitir ITBI')
                                        @case('Pagamento Efetuado')
                                            <img src="{{ asset('img/fases_solicitar_itbi_05.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('expirada')
                                            <img src="{{ asset('img/fases_solicitar_itbi_06.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Cancelado')
                                            <img src="{{ asset('img/fases_solicitar_itbi_08.png') }}" alt="Fase" class="fase" />
                                            @break
                                    @endswitch
                                </center>

                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == 'Portal')
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <br>resp: $registro->responsavel<br>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar
                                                Solicitação</a>
                                        </div>
                                    @endif
                                @endif



                                @if( $registro->Serviço == 'Solicitação de ITBI' && $registro->FINALIZADO == "andamento")
                                    @if( $registro->Etapa == "6")
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> A sua Guia de ITBI já está disponível. Clique no botão para baixar a guia: &nbsp;<br><br>
                                            <a href="{{ route('baixarguiaitbi', $registro->Chamado) }}" class="btn btn-blue">Baixar Guia de ITBI</a>
                                        </div>
                                    @elseif( $registro->Etapa == "7")
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;<br><br>
                                            <strong>Pendências:</strong> {{$registro->motivo}}<br><br>

                                            <a href="{{ route('editarItbi', $registro->Chamado) }}" class="btn btn-blue">Corrigir pendências</a>
                                        </div>
                                    @endif
                                @endif

                                @if($registro->motivo != "" && $registro->FINALIZADO != "andamento")
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Atenção!</strong><br>
                                    Esta solicitação foi finalizada e teve como conclusão:<br>
                                    Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                    @if($registro->motivo != "")
                                        Motivo: {{$registro->motivo}}<br>
                                    @endif
                                </div>
                                @endif
                            </div>
                        @elseif( $registro->Serviço == 'Solicitar Licença Maternidade' || $registro->Serviço == 'Solicitar Licença Prêmio')
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>
                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura da Solicitação')
                                        @case('Abertura de Solicitação')
                                            <img src="{{ asset('img/fase_licenca_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Análise da Documentação')
                                        @case('Análise Documental')
                                            <img src="{{ asset('img/fase_licenca_02.png') }}" alt="Fase" class="fase" />
                                        @break
                                        @case('Licença Aprovada')
                                        @case('Licença Aprovada')
                                            <img src="{{ asset('img/fase_licenca_03.png') }}" alt="Fase" class="fase" />
                                        @break
                                        @case('Finalizado')
                                            <img src="{{ asset('img/fase_licenca_04.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @endswitch
                                </center>
                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == 'portal')
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <br>resp: {{ $registro->responsavel }}<br>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar Solicitação</a>
                                        </div>
                                    @endif
                                @endif
                                @if($registro->motivo != "" && $registro->FINALIZADO != "andamento")
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Atenção!</strong><br>
                                    Esta solicitação foi finalizada e teve como conclusão:<br>
                                    Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                    @if($registro->motivo != "")
                                        Motivo: {{$registro->motivo}}<br>
                                    @endif
                                </div>
                                @endif
                            </div>
                        @elseif( $registro->Serviço == 'Solicitar Auxílio Transporte')
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>
                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura da Solicitação')
                                        @case('Abertura de Solicitação')
                                            <img src="{{ asset('img/fase_auxilio_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Análise da Documentação')
                                        @case('Análise Documental')
                                            <img src="{{ asset('img/fase_auxilio_02.png') }}" alt="Fase" class="fase" />
                                        @break
                                        @case('Auxílio Aprovado')
                                        @case('Auxílio Aprovado')
                                            <img src="{{ asset('img/fase_auxilio_03.png') }}" alt="Fase" class="fase" />
                                        @break
                                        @case('Finalizado')
                                            <img src="{{ asset('img/fase_auxilio_04.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @endswitch
                                </center>
                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == 'portal')
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <br>resp: {{ $registro->responsavel }}<br>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar Solicitação</a>
                                        </div>
                                    @endif
                                @endif
                                @if($registro->motivo != "" && $registro->FINALIZADO != "andamento")
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Atenção!</strong><br>
                                    Esta solicitação foi finalizada e teve como conclusão:<br>
                                    Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                    @if($registro->motivo != "")
                                        Motivo: {{$registro->motivo}}<br>
                                    @endif
                                </div>
                                @endif
                            </div>

                            <!-- ********** INICIO PARCELAMENTO DE DÍVIDA ************** -->

                            @elseif( $registro->Serviço == 'Parcelamento de Dívida Ativa')
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>
                            <div class="justfy-content-center registro-fase">
                                @switch($registro->Fase)
                                    @case('Abertura da Solicitação')
                                    @case('Abertura da Solicitação')
                                        <img src="{{ asset('img/fases_SIM_Parcelamento_Divida_01.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @case('Solicitação em Análise')
                                    @case('Gerar Protocolo')
                                    @case('Assumir Análise')
                                    @case('Análisar Solicitação de Dívida Ativa')
                                    @case('Cadastrar Parcelamento de Dívida Ativa')
                                    @case('Assumir Revisão de Parcelamento')
                                    @case('Revisar Parcelamento de Dívida Ativa')
                                        <img src="{{ asset('img/fases_SIM_Parcelamento_Divida_02.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @case('Cumprir Exigências')
                                    @case('Cumprir Exigências')
                                        <img src="{{ asset('img/fases_SIM_Parcelamento_Divida_03.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @case('Parcelamento Deferido')
                                    @case('Concluir Solicitação de Parcelamento')
                                        <img src="{{ asset('img/fases_SIM_Parcelamento_Divida_04.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @case('Parcelamento Indeferido')
                                    @case('Indeferir Solicitação de Parcelamento')
                                        <img src="{{ asset('img/fases_SIM_Parcelamento_Divida_05.png') }}" alt="Fase" class="fase" />
                                        @break
                                @endswitch
                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->Etapa == 3)
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;
                                            <a href="{{ route('formularioDivida', ['chamado' => $registro->Chamado, 'ciclo' => $registro->Ciclo]) }}" class="btn btn-blue">Editar Solicitação</a>
                                            <div class="col-lg-12">
                                                Resp: {{ $registro->responsavel }}
                                                <div class="row">
                                                    <div class="col-lg-6 form-group">
                                                        Motivo: {{ $registro->motivo }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="row">
                                                @if($registro->motivo == ' ')
                                                    <div class="col-lg-6 form-group">
                                                        <h5>Aceite do Parcelamento</h5>
                                                        <p>
                                                            Para registrar o parcelamento é necessário anexar o Termo de Compromisso assinado. Você encontra este termo clicando
                                                            <a href="{{ route('termo_aceite') }}" id="termo">aqui <i class="fa fa-download"></i></a>
                                                        </p>
                                                    </div>
                                                @else
                                                    <div class="col-lg-6 form-group">
                                                        <h5>Exigência Documental</h5>
                                                        <p>
                                                            Para andamento do seu processo, foram exigidas as seguintes documentações:
                                                            {{$registro->motivo}}
                                                        </p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                @if($registro->motivo != "" && $registro->FINALIZADO != "andamento")
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Atenção!</strong><br>
                                    Esta solicitação foi finalizada e teve como conclusão:<br>
                                    Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                    @if($registro->motivo != "")
                                        Motivo: {{$registro->motivo}}<br>
                                    @endif
                                </div>
                                @endif
                            </div>

                            <!-- ********** FIM PARCELAMENTO DE DÍVIDA ************** -->

                        @elseif( $registro->Serviço == 'Alterar Lotação Servidor')
                            <div class="row registro-fase-topo">
                                <div class="col-lg-2">
                                    Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
                                </div>
                                <div class="col-lg-2">
                                    Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Serviço: <b>{{ $registro->Serviço }}</b>
                                </div>
                                <div class="col-lg-4">
                                    Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
                                </div>
                            </div>
                            <div class="justfy-content-center registro-fase">
                                <center>
                                    @switch($registro->Fase)
                                        @case('Abertura da Solicitação')
                                        @case('Abertura de Solicitação')
                                            <img src="{{ asset('img/fase_lotacao_01.png') }}" alt="Fase" class="fase" />
                                            @break
                                        @case('Análise da Documentação')
                                        @case('Análise Documental')
                                            <img src="{{ asset('img/fase_lotacao_02.png') }}" alt="Fase" class="fase" />
                                        @break
                                        @case('Alteração Aprovada')
                                        @case('Alteração Aprovada')
                                            <img src="{{ asset('img/fase_lotacao_03.png') }}" alt="Fase" class="fase" />
                                        @break
                                        @case('Finalizado')
                                            <img src="{{ asset('img/fase_lotacao_04.png') }}" alt="Fase" class="fase" />
                                        @break
                                    @endswitch
                                </center>
                                @if($registro->Fase != "Finalizado" && $registro->Fase != 'Concluído')
                                    @if($registro->responsavel == 'portal')
                                        <br>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <br>resp: {{ $registro->responsavel }}<br>
                                            <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                                            <a href="{{ route('formularioEdita', $registro->Chamado) }}" class="btn btn-blue">Editar Solicitação</a>
                                        </div>
                                    @endif
                                @endif
                                @if($registro->motivo != "" && $registro->FINALIZADO != "andamento")
                                <div class="alert alert-info">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Atenção!</strong><br>
                                    Esta solicitação foi finalizada e teve como conclusão:<br>
                                    Conclusão: <strong>{{$registro->Fase}}</strong><br>
                                    @if($registro->motivo != "")
                                        Motivo: {{$registro->motivo}}<br>
                                    @endif
                                </div>
                                @endif
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="alert alert-warning text-center">
                                <p><strong>Atenção!</strong>
                                    Solicitação não identificada em seu cadastro.
                                </p>
                            </div>
                        </div>
                    </div>
                @endif
                <br>
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <a href="{{ route('home') }}">
                            <button id="btn_voltar" class="btn btn-primary btn-sm" value="Voltar"><i class="fa fa-arrow-left"></i> Voltar</button>
                        </a>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
@endsection
