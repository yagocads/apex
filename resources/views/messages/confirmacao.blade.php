@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <i class="fa {{$icone}}"></i>
                            @isset($titulo)
                            {{ $titulo }}
                            @endisset
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-12">
                                <h5>
                                    <b>Alerta para confirmação de processo logado no sistema:</b>
                                </h5>
                                <hr/>

                                <div class="alert alert-success">
                                    <h3 class="mt-5 mb-5">
                                        <i class="fa fa-check-square-o"></i>  Solicitação de IPTU Realizado com Sucesso!
                                    </h3>

                                    <p class="alert alert-info">
                                        <b>IMPORTANTE!</b> Guardar as informações de confirmação para consultar o andamento do processo.
                                    </p>

                                    @isset($mensagem)
                                        {!! $mensagem !!}
                                    @endisset

                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="{{ isset($rota)? route($rota, ['guia' => get_guia_services(), 'aba' => $retorno] ) : URL::previous() }}">
                                    <button type="button" class="btn btn-primary mt-3" id="autenticar">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </button>
                                    <a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection
