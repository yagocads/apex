@extends('layouts.tema_principal')
@section('content')
<div class="container-fluid">
    @include('modals.modal_financeiro')
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Consulta Geral Financeira</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h4>{{ $pagina }}</h4></div>
                <div class="card-body">
                    <form action="{{ Route('integracaoPagamentosEfetuados') }}" method="post" id="formulario">
                        @csrf
                        <input type="hidden" name="tipo" id="tipo">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    @if(strlen(Auth::user()->cpf ) == 14)
                                    <label for="cpf">CPF</label>
                                    @else
                                    <label for="cpf">CNPJ</label>
                                    @endif
                                    <input type="hidden"name="cpf" id="cpf" value="{{ $cpf }}">
                                    <input type="text" class="form-control form-control-sm" name="ro_cpf" id="ro_cpf" value="{{ $cpf }}" readonly="readonly">
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label for="ro_nome">Nome:</label>
                                    <input type="hidden"name="nome" id="nome" value="{{ $nome }}">
                                    <input type="text" class="form-control form-control-sm" name="ro_nome" id="ro_nome" value="{{ $nome }}" readonly="readonly">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <h5>Informe o tipo</h5>
                                    <span id="matriculaB" class="btn btn-primary btn-sm">Matrícula do Imóvel</span>
                                    &nbsp;
                                    <span id="inscricaoB" class="btn btn-success btn-sm">Inscrição Municipal</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    <label for="matricula">Matrícula do Imóvel:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="matricula" id="matricula" maxlength="8" disabled onkeypress='return onlyNumber(event)' />
                                        <div class="input-group-append">
                                            <input type="button" id="btn_pesquisar_matricula" class="btn btn-primary btn-sm" value="Pesquisar" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label for="inscricao">Inscrição Municipal:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="inscricao" id="inscricao" maxlength="8" disabled onkeypress='return onlyNumber(event)' />
                                        <div class="input-group-append">
                                            <input type="button" id="btn_pesquisar_inscricao" class="btn btn-primary btn-sm" value="Pesquisar" disabled  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <h5>Informe o tipo</h5>
                                    <span id="periodoB" class="btn btn-primary btn-sm">Período</span>
                                    &nbsp;
                                    <span id="exercicioB" class="btn btn-success btn-sm">Exercício</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="periodo">{{ __('Período') }}</label>
                                    <input type="date" class="form-control form-control-sm" max="{{ date('Y-m-d') }}"  id="periodo_inicial" name="periodo_inicial" disabled />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="date" class="form-control form-control-sm" max="{{ date('Y-m-d') }}"  id="periodo_final" name="periodo_final" disabled />
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="exercicio">{{ __('Exercício') }}</label>
                                    <select name="exercicio" id="select_exercicio" class="form-control form-control-sm" disabled >
                                        <option value="0">Selecione o ano de exercício</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'financeiro']) }}"
                                class="btn btn-primary">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>&nbsp;
                            <button id="btn_avancar" class="btn btn-success" style="visibility: hidden;" >Avançar <i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
@endsection

@section('post-script')

<script type="text/javascript">
        $(document).ready(function(){

            $("#periodoB").click(function (){
                $("#exercicio").prop("disabled", true);
                $("#select_exercicio").prop("disabled",true);
                $("#periodo_inicial").prop("disabled", false);
                $("#periodo_final").prop("disabled", false);
            });

            $("#exercicioB").click(function (){
                $("#exercicio").prop("disabled", false);
                $("#select_exercicio").prop("disabled",false);
                $("#periodo_inicial").prop("disabled", true);
                $("#periodo_final").prop("disabled", true);
            });

            $('#exercicio').keyup(function(){
                if($('#exercicio').val().length == 4){
                    $("#select_exercicio").prop("disabled",true);
                }
                else{
                    $("#select_exercicio").prop("disabled",false);
                }
            });

            $("#select_exercicio").change(function(){
                if($("#select_exercicio").val() != ""){
                    $("#exercicio").prop("disabled", true);
                }
                else{
                    $("#exercicio").prop("disabled", false);
                }
            });

            if($("#cpf").val().length == 14){
                $('#btn_voltar').click(function(){
                    window.location.replace("servicos/1/financeiro");
                });
            }
            else{
                $('#btn_voltar').click(function(){
                    window.location.replace("servicos/2/financeiro");
                });
            }

            $('#btn_pesquisar_inscricao').click(function(){
                let load = $(".ajax_load");
                load.fadeIn(200).css("display", "flex");

                $(".modal-title").html("Pesquisar Inscrição");
                $(".modal-body").html("<div class='callout-white'><div class'table_head'><div class='row'><div class='col-lg-3'><p><strong>Inscrição</strong></p></div><div class='col-lg-3'><p><strong>Nome</strong></p></div><div class='col-lg-3'><p><strong>Endereço</strong></p></div><div class='col-lg-3'><p><strong>Ações</strong></p></div></div></div><div class='table_body'></div></div>");
                $('.table_body').html('');

                if($('#ro_cpf').val().length == 14){
                    var cpf = $('#ro_cpf').val().replace(".","");
                    var cpf1 = cpf.replace("-","");
                    cpf = cpf1;
                }
                else{
                    var cpf = $('#ro_cpf').val().replace(".","");
                    var cpf1 = cpf.replace("/","");
                    var cpf2 = cpf1.replace(".","");
                    var cpf3 = cpf2.replace("-","");
                    cpf = cpf3;
                }

                var url = 'taxasconsultainscricao/' + cpf +'/';
                $.get(url, function (resposta) {
                    var obj = jQuery.parseJSON(resposta);
                    if (obj.iStatus == 1) {
                        load.fadeOut(200);
                        $('#pesquisaInscricaoTaxaTbl').dataTable().fnClearTable();
                        if(obj.aInscBase.length > 0){
                            $.each(obj.aInscBase, function(i, item) {
                                $('.table_body').append(
                                    "<div class='row'>"+
                                        "<div class='col-lg-3'>"+item.q02_inscr+"</div>"+
                                        "<div class='col-lg-3'>"+unescape(item.z01_nome).replace(/[+]/g, " ")+"</div>"+
                                        "<div class='col-lg-3'>"+unescape(item.z01_ender).replace(/[+]/g, " ")+"</div>"+
                                        "<div class='col-lg-3'><button type='button' class='btn btn-primary' onClick='selecionaPesquisaInscricao(" + item.q02_inscr + ");'  >Selecionar</button></div>"+
                                    "</div>");
                            });
                            $("#footer-modal").modal('show');
                        }
                        else{
                            alert("Nenhuma inscrição foi encontrada para este usuário.");
                        }
                    }
                    else {
                        alert("Quantidade de imóveis acima do permitido.");
                        return false;
                    }
                });
            });

            $('#btn_avancar').click(function(){

                let load = $(".ajax_load");
                load.fadeIn(200).css("display", "flex");

                if($('#tipo').val() == 'm'){
                    if($('#matricula').val() != '' && ($('#select_exercicio').val() != "") || ($('#matricula').val() != '' && $('#periodo_inicial').val() != "" &&  $('#periodo_final').val() != "")){

                        var exercicio = $('#select_exercicio').val();

                        $.post("{{ route('integracaoPagamentosEfetuados') }}",{
                            '_token':'{{ csrf_token() }}',
                            'cpf': $('#cpf').val(),
                            'matricula': $('#matricula').val(),
                             'exercicio': exercicio,
                             'periodo_inicial': $('#periodo_inicial').val(),
                             'periodo_final': $('#periodo_final').val() },function(data){
                            if(data.error == 'E17_GFD_PGEF_INFO_CGM_NAO_ENCONTADO_M' || data == ''){
                                load.fadeOut(200);
                                alert("Não será possível gerar o relatório de Pagamentos Efetuados à partir dos dados fornecidos.");
                            }
                            else{
                                $('#formulario').submit();
                            }
                        });
                    } else {
                        load.fadeOut(200);
                        alert("Não será possível gerar o relatório de Pagamentos Efetuados sem o dado da Matrícula e os dados do período ou exercício.");
                        return false;
                    }

                } else {
                    if($('#inscricao').val() != '' && ($('#select_exercicio').val() != "") || ($('#inscricao').val() != '' && $('#periodo_inicial').val() != "" &&  $('#periodo_final').val() != "")){

                        var exercicio = $('#select_exercicio').val();

                        $.post("{{ route('integracaoPagamentosEfetuados') }}",{
                            '_token':'{{ csrf_token() }}',
                            'cpf': $('#cpf').val(),
                            'inscricao': $('#inscricao').val(),
                            'exercicio': exercicio,
                            'periodo_inicial': $('#periodo_inicial').val(),
                            'periodo_final': $('#periodo_final').val() },function(data){
                            if(data.error == 'E17_GFD_PGEF_INFO_CGM_NAO_ENCONTADO_M' || data == ''){
                                load.fadeOut(200);
                                alert("Não será possível gerar o relatório de Pagamentos Efetuados à partir dos dados fornecidos.");
                            } else {
                                $('#formulario').submit();
                            }
                        });
                    } else {
                        load.fadeOut(200);
                        alert("Não será possível gerar o relatório de Pagamentos Efetuados sem o dado da Inscrição e os dados do período ou exercício.");
                        return false;
                    }
                }
            });
        });
        function selectionaPesquisa(matricula){

            let load = $(".ajax_load");
            load.fadeIn(200).css("display", "flex");

            $('#tipo').val('m');
            $('#inscricao').val('');
            $.post("{{ route('anosPagamento') }}",{
                '_token':'{{ csrf_token() }}',
                'cpf': $('#ro_cpf').val(),
                'matricula': matricula },function(data){
                if(data.iStatus != 1){
                    load.fadeOut(200);
                    alert("Esse imóvel não possui Pagamentos para a geração do relatório.");
                } else {
                    load.fadeOut(200);

                    $('option', '#select_exercicio').remove();
                    $('#select_exercicio').append('<option value="">Selecione o ano de exercício</option>');
                    const select = document.getElementById("select_exercicio");
                    const list = data.aPagamentosEfetuadoss;
                    list.forEach((years) => {
                        option = new Option(years.ano_efetpagto, years.ano_efetpagto);
                        select.options[select.options.length] = option;
                    });

                    $("#matricula").val(matricula);
                    $('#hd_matricula').val(matricula);
                    $("#footer-modal").modal('hide');
                    $('#btn_avancar').css('visibility','visible');
                }
            });
        }

        function selecionaPesquisaInscricao(inscricao){

            let load = $(".ajax_load");
            load.fadeIn(200).css("display", "flex");
            $('#tipo').val('i');
            $('#matricula').val('');
            $.post("{{ route('anosPagamento') }}",{
                '_token':'{{ csrf_token() }}',
                'cpf': $('#ro_cpf').val(),
                'inscricao': inscricao },function(data){
                if(data.iStatus != 1){
                    load.fadeOut(200);
                    alert("Essa Inscrição Municipal está baixada e não possui débitos em aberto.");
                } else {
                    load.fadeOut(200);

                    $('option', '#select_exercicio').remove();
                    $('#select_exercicio').append('<option value="">Selecione o ano de exercício</option>');
                    const select = document.getElementById("select_exercicio");
                    const list = data.aPagamentosEfetuadoss;
                    list.forEach((years) => {
                        option = new Option(years.ano_efetpagto, years.ano_efetpagto);
                        select.options[select.options.length] = option;
                    });

                    $("#inscricao").val(inscricao);
                    $('#hd_inscricao').val(inscricao);
                    $("#footer-modal").modal('hide');
                    $('#btn_avancar').css('visibility','visible');
                }
            });
        }
</script>
@endsection
