<div class="row">
    <div class="col-lg-12">
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <input id="notificacao_nao_recebida" type="checkbox"
                    class="form-input" name="notificacao_nao_recebida"
                    {{ old('notificacao_nao_recebida') ? 'checked' : '' }}>
                <label for="notificacao_nao_recebida" class="control-label">Notificação não recebida</label>
            </div>

            <div class="col-lg-4 form-group">
                <label for="numero_auto">Número do auto: <span class="asterisc">*</span></label>
                <input type="text" name="numero_auto" id="numero_auto"
                    class="form-control form-control-sm mask_numero_auto {{ ($errors->has('numero_auto') ? 'is-invalid' : '') }}"
                    value="{{ old('numero_auto') }}" maxlength="10" />

                @if ($errors->has('numero_auto'))
                    <div class="invalid-feedback">
                        {{ $errors->first('numero_auto') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="data_recebimento_notificacao">Data de recebimento da notificação:</label>
                <input type="date" name="data_recebimento_notificacao" id="data_recebimento_notificacao"
                    class="form-control form-control-sm {{ ($errors->has('data_recebimento_notificacao') ? 'is-invalid' : '') }}"
                    value="{{ old('data_recebimento_notificacao') }}" maxlength="100" />

                @if ($errors->has('data_recebimento_notificacao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('data_recebimento_notificacao') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="data_entrada_requerimento">Data de entrada no requerimento:</label>
                <input type="date" name="data_entrada_requerimento" id="data_entrada_requerimento"
                    class="form-control form-control-sm {{ ($errors->has('data_entrada_requerimento') ? 'is-invalid' : '') }}"
                    value="{{ old('data_entrada_requerimento') ? old('data_entrada_requerimento') : date('Y-m-d') }}" maxlength="100" />

                @if ($errors->has('data_entrada_requerimento'))
                    <div class="invalid-feedback">
                        {{ $errors->first('data_entrada_requerimento') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="tipo_multa">Tipo de Multa: <span class="asterisc">*</span></label>
                <select name="tipo_multa" id="tipo_multa"
                    class="form-control form-control-sm {{ ($errors->has('tipo_multa') ? 'is-invalid' : '') }}">
                    <option></option>
                    <option {{ old('tipo_multa') === 'Leve' ? 'selected' : '' }}>Leve</option>
                    <option {{ old('tipo_multa') === 'Média' ? 'selected' : '' }}>Média</option>
                    <option {{ old('tipo_multa') === 'Grave' ? 'selected' : '' }}>Grave</option>
                    <option {{ old('tipo_multa') === 'Gravíssima' ? 'selected' : '' }}>Gravíssima</option>
                </select>

                @if ($errors->has('tipo_multa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tipo_multa') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="logradouro_infracao">Logradouro da infração (Endereço): <span class="asterisc">*</span></label>
                <input type="text" name="logradouro_infracao" id="logradouro_infracao"
                    class="form-control form-control-sm {{ ($errors->has('logradouro_infracao') ? 'is-invalid' : '') }}"
                    value="{{ old('logradouro_infracao') }}" maxlength="50" />

                @if ($errors->has('logradouro_infracao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('logradouro_infracao') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-10 form-group">
                <label for="observacoes">Observações:</label>
                <textarea type="text" name="observacoes" id="observacoes" cols="4" rows="8"
                    class="form-control form-control-sm {{ ($errors->has('observacoes') ? 'is-invalid' : '') }}"
                        maxlength="1000">{{ old('observacoes') }}</textarea>

                @if ($errors->has('observacoes'))
                    <div class="invalid-feedback">
                        {{ $errors->first('observacoes') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
