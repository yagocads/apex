@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Lista de Imóveis</h2>

                <div class="card">
                    <div class="card-header">
                        <h5>Suas Informações</h5>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="cpf">{{ (strlen(Auth::user()->cpf) > 14 ? 'CNPJ:' : 'CPF:') }}</label>
                                <input type="text" class="form-control form-control-sm" name="cpf" value="{{ Auth::user()->cpf }}" readonly="readonly" />
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="nome">{{ __('Nome') }}</label>
                                <input type="text" class="form-control form-control-sm" name="nome" value="{{ Auth::user()->name }}" readonly="readonly" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col d-flex justify-content-center mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <a data-toggle="collapse" class="no-underline text-dark" href="#collapseBuscaImoveis" aria-controls="collapseBuscaImoveis">
                        <div class="card-header">
                            <i class="fa fa-arrow-down arrow-red"></i>
                            <h5 class="d-inline">Buscar Imóvel</h5>
                        </div>
                    </a>
                    <div class="collapse" id="collapseBuscaImoveis">
                        <div class="card-body" >
                            @include('imovel.busca_imoveis')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col d-flex justify-content-center mt-3">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Lista de Imóveis</h5>
                    </div>
                    <div class="card-body">
                        @foreach ($registros->aMatriculasListaCertidaoEncontradas as $registro)
                            <!-- Baixados -->
                            @php $valBaixa = 0; @endphp

                            @foreach ($baixados as $baixa)
                                @if($registro->matricula == $baixa['matricula'])
                                    @php $valBaixa = $baixa['baixado']; @endphp
                                    @break
                                @endif
                            @endforeach

                            <div class="callout-red shadow-sm mb-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <p><b>Matrícula:</b> {{ $registro->matricula }}</p>
                                        <p><b>Tipo:</b> {{ $registro->tipo }}</p>
                                        <p><b>Bairro:</b> {{ convert_accentuation($registro->bairro) }}</p>
                                        <p><b>Logradouro:</b> {{ convert_accentuation($registro->logradouro) }}</p>
                                    </div>

                                    <div class="col-lg-6">
                                        <p><b>Número:</b> {{ $registro->numero }}</p>
                                        <p><b>Complemento:</b> {{ convert_accentuation($registro->complemento) }}</p>
                                        <p><b>Baixado:</b> {{ $valBaixa == "0" ? "Não" : "Sim" }}</p>
                                        <p><b>Planta/Quadra/Lote:</b> {{ convert_accentuation($registro->planta) }} / {{ $registro->quadra }} / {{ $registro->lote }}</p>
                                    </div>

                                    <div class="col-lg-12">
                                        @switch ($url_servico)
                                            @case('consultaInformacoes')

                                                <a href="{{ route('imovelInformacao', $registro->matricula) }}" class="btn btn-sm btn-outline-danger d-block">
                                                    <span style="font-size: 15px;">
                                                        Consultar <i class="fa fa-arrow-right"></i>
                                                    </span>
                                                </a>
                                                @break
                                            @case('averbacaoImovel')
                                            @case('certidaoITBI')

                                                <a href="javascript:;" data-url="{{ route('certidaoQuitacaoItbi', $registro->matricula) }}" class="selected_certificate btn btn-sm btn-outline-danger d-block">
                                                    <span style="font-size: 15px;">
                                                        Selecionar Certidão <i class="fa fa-file-o"></i>
                                                    </span>
                                                </a>
                                                @break
                                            @case('certidaoNumeroPorta')

                                                <a href="{{ route('certidaoNumeroPortaImprimir', $registro->matricula) }}"
                                                    class="btn btn-sm btn-outline-danger d-block">
                                                    <span style="font-size: 15px;">
                                                        Imprimir <i class="fa fa-print"></i>
                                                    </span>
                                                </a>
                                                @break
                                            @case('certidaoNegativa')

                                                <a href="{{ route('certidaoNegativaImprimir', $registro->matricula) }}"
                                                    class="btn btn-sm btn-outline-danger d-block">
                                                    <span style="font-size: 15px;">
                                                        Imprimir <i class="fa fa-print"></i>
                                                    </span>
                                                </a>
                                                @break
                                        @endswitch
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'imovel']) }}"
                            class="btn btn-primary">

                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                    </div>
                </div>

                <div class="text-center">
                    {!! $paginator !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.modal_default')

@endsection

@section('post-script')

<script>
    $(function() {

        $('.selected_certificate').click(function() {

            let url = $(this).data('url');

            // Spinner
            let load = $(".ajax_load");

            $.ajax({
                url: url,
                method: 'get',
                data: {
                    url_servico: "{{ $url_servico }}"
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    $('#default-modal').find('.modal-title').html("Certidão Quitação ITBI");
                    $('#default-modal').find('.modal-body').html(response);
                    $('#default-modal').modal('show');

                    load.fadeOut(200);
                }
            });
        });

    });
</script>

@endsection
