<div class="card">
    <div class="card-header">
        <h5>Icones</h5>
    </div>

    <div class="card-body">
        <h6><b>Icones utilizados:</b></h6>
        <hr>

        <b><h4 class="d-inline"><i class="fa fa-home"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-search"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-user"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-send"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-paperclip"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-folder-open"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-envelope"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-phone"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-lock"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-cogs"></i></h4></b>
        <b><h4 class="d-inline m-2"><i class="fa fa-book"></i></h4></b>
        <b><h4 class="d-inline"><i class="fa fa-link"></i></h4></b>

        <div class="alert alert-info mt-4">
            <p>Para utilizar um ícone que seja necessário para sua funcionalidade, acesse no link abaixo: </p>
            <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank"><i class="fa fa-link"></i> Font Awesome Icons</a>
        </div>
    </div>
</div>