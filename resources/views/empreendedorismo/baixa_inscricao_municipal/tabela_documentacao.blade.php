
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela_documentacao">
        <thead>
            <tr>
                <th>Ordenação</th>
                <th>Descrição</th>
                <th>Documento</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentacao_baixa_inscricao_municipal"))
                @foreach(session("documentacao_baixa_inscricao_municipal") as $dados)
                    <tr>
                        <td class="text-center" width="10%">{{ explode('.', $dados->descricao)[0] }}</td>
                        <td>{{ explode('.', $dados->descricao)[1] }}</td>
                        <td>{{ $dados->arquivo }}</td>
                        <td class="text-center" width="10%">
                            <button 
                                class="btn btn-sm btn-danger btn-remover-documento"
                                data-descricao="{{ $dados->descricao }}"
                                data-caminho="{{ $dados->caminho }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>