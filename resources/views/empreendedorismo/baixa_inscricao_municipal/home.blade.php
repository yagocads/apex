@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">

        @if (session('success'))
            @include('confirmacao.confirmacao_processo')
        @else
            <div class="col-lg-12">
                <h3 class="mb-3">
                    <i class="fa fa-home"></i> Baixa de Inscrição Municipal
                </h3>

                <form method="POST" id="formBaixaInscricaoMunicipal"
                    action="{{ route('salvar-baixa-inscricao-municipal') }}" autocomplete="off">

                    @csrf

                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <span class="d-block">* {{ $error }}</span>
                            @endforeach
                        </div>
                    @endif

                    <div id="accordion">

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#contribuinte">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Contribuinte
                                </div>
                            </a>
                            <div id="contribuinte" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empreendedorismo.baixa_inscricao_municipal.dados_contribuinte")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#requerente">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                                </div>
                            </a>
                            <div id="requerente" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empreendedorismo.baixa_inscricao_municipal.dados_requerente")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#solicitacao">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Solicitação
                                </div>
                            </a>
                            <div id="solicitacao" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empreendedorismo.baixa_inscricao_municipal.solicitacao")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3 documentacao {{ empty(old('tipo_baixa_inscricao')) ? 'd-none' : ''  }}">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#documentacao">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Documentação do Processo
                                </div>
                            </a>
                            <div id="documentacao" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empreendedorismo.baixa_inscricao_municipal.documentacao_processo")
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'empreendedorismo']) }}" class="btn btn-primary mt-2">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>

                            <button class="btn btn-success mt-2 enviar_informacoes" {{ empty(old('tipo_baixa_inscricao')) ? 'disabled' : ''  }}>Enviar Informações <i class="fa fa-send"></i></button>
                        </div>
                    </div>

                </form>
            </div>
        @endif
    </div>
</div>

@endsection

@section('post-script')
    <script src="{{ asset('js/baixa-inscricao-municipal/scripts.js') }}"></script>
@endsection
