<div class="row">
    <div class="col-lg-6 form-group">
        <div class="alert alert-info documentacao_obrigatoria d-none">
            <p>Documentação Obrigatória:</p>

            <ol class="lista_documentacao_obrigatoria">
            </ol>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="col-lg-12">
            <div class="alert alert-info">
                <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 5Mb.
            </div>
        </div>
        
        <div class="col-lg-12 form-group">
            <label for="descricao_documento">Descrição do documento:</label>
            <select name="descricao_documento" id="descricao_documento" 
                class="form-control form-control-sm">
                <option></option>
            </select>
        </div>

        <div class="col-lg-4 form-group">
            <label for="documento"><i class="fa fa-paperclip"></i> Anexar Documento:</label>
            <input type="file" name="documento" 
                id="documento" />
        </div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary btn-enviar-documento"><i class="fa fa-plus"></i> Adicionar Documento</button>
        </div>
    </div>
</div>

<hr />

<div class="row col-lg-12 mb-3 mt-3">
    <h5><i class="fa fa-file-o"></i> Documentos Adicionados:</h5>
</div>

<div class="row">
    <div class="col-lg-12 form-group">
        <div class="conteudo_documentacao">
            @include('imovel.tramitacao_imoveis.documentacao.tabela_documentacao')
        </div>
    </div>
</div>