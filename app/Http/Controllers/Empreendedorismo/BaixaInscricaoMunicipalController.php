<?php

namespace App\Http\Controllers\Empreendedorismo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaixaInscricaoMunicipalRequest;
use App\Support\CidadaoSupport;
use App\Traits\HandleArquivos;
use Illuminate\Support\Facades\DB;

class BaixaInscricaoMunicipalController extends Controller
{
    use HandleArquivos;

    public function index()
    {
        return view('empreendedorismo.baixa_inscricao_municipal.home', [
            'user' => Auth::user()
        ]);
    }

    public function getDadosContribuinte(Request $request)
    {
        if ($this->verificarSolicitacaoJaEmProcessamento($request->cpfOuCnpj)) {
            return response()->json(['solicitacaoJaEmProcessamento' => true]);
        }

        $cidadaoSupport = new CidadaoSupport();
        $contribuinte = $cidadaoSupport->getDadosCGM(remove_character_document($request->cpfOuCnpj));

        if ($contribuinte->iStatus == 1) {
            $dadosContribuinte = [
                'razao_social' => convert_accentuation($contribuinte->aCgmPessoais->z01_nome),
                'inscricao_municipal' => '',
                'telefone' => formatTelefone(convert_accentuation($contribuinte->aCgmContato->z01_telef)),
                'celular' => formatTelefone(convert_accentuation($contribuinte->aCgmContato->z01_telcel)),
                'email' => convert_accentuation($contribuinte->aCgmContato->z01_email),
                'inscricao_estadual' => '',
                'iptu' => ''
            ];
        } else {
            $dadosContribuinte = false;
        }

        return response()->json($dadosContribuinte);
    }

    public function salvar(BaixaInscricaoMunicipalRequest $request)
    {
        if ($this->verificarSolicitacaoJaEmProcessamento($request->cpfOuCnpj_contribuinte)) {
            return redirect()
                ->route('baixa-inscricao-municipal')
                ->withInput()
                ->with('solicitacaoJaEmProcessamento', "Já existe uma solicitação em processamento para este CNPJ: {$request->cpfOuCnpj_contribuinte}.");
        }

        $idBaixaInscricaoMunicipal = DB::connection('integracao')
            ->table('lecom_baixa_inscricao_municipal')
            ->insertGetId(
                [
                    'razao_social_contribuinte' => $request->razao_social_contribuinte,
                    'cnpj_contribuinte' => $request->cpfOuCnpj_contribuinte,
                    'inscricao_municipal' => $request->inscricao_municipal,
                    'telefone_contribuinte' => $request->telefone_contribuinte,
                    'celular_contribuinte' => $request->celular_contribuinte,
                    'email_contribuinte' => $request->email_contribuinte,
                    'inscricao_estadual' => $request->inscricao_estadual,
                    'iptu' => $request->iptu,

                    'cpf_requerente' => $request->cpf_requerente,
                    'nome_requerente' => $request->nome_requerente,
                    'email_requerente' => $request->email_requerente,
                    'celular_requerente' => $request->celular_requerente,

                    'tipo_baixa_inscricao' => $request->tipo_baixa_inscricao,
                    'observacoes_adicionais' => $request->observacoes_adicionais
                ]
            );

        $this->salvarArquivosNaBaseDeDados($idBaixaInscricaoMunicipal, $request->cpfOuCnpj_contribuinte);

        return redirect()
            ->route('baixa-inscricao-municipal')
            ->with('success', ['title' => 'Processo de Baixa de Inscrição Municipal solicitado com sucesso!']);
    }

    private function verificarSolicitacaoJaEmProcessamento($documentoCpfOuCnpj): int
    {
        return DB::connection('integracao')
            ->table('lecom_baixa_inscricao_municipal')
            ->where('cnpj_contribuinte', '=', $documentoCpfOuCnpj)
            ->count();
    }

    private function salvarArquivosNaBaseDeDados($idBaixaInscricaoMunicipal, $documentoCpfOuCnpj)
    {
        if (session()->has('documentacao_baixa_inscricao_municipal')) {
            foreach (session('documentacao_baixa_inscricao_municipal') as $dados) {
                DB::connection('integracao')
                    ->table('lecom_baixa_inscricao_municipal_arquivos')
                    ->insert([
                        'id_baixa_inscricao_municipal' => $idBaixaInscricaoMunicipal,
                        'cpf_cnpj' => $documentoCpfOuCnpj,
                        'descricao' => $dados->descricao,
                        'documento' => $dados->caminho
                    ]);
            }

            session()->forget("documentacao_baixa_inscricao_municipal");
        }
    }

    public function removerDocumentosDaSessao()
    {
        session()->forget("documentacao_baixa_inscricao_municipal");
        return view('empreendedorismo.baixa_inscricao_municipal.tabela_documentacao');
    }

    public function exigencia($idChamado)
    {
        $exigenciaNaoRespondida = DB::connection('integracao')
            ->table('lecom_baixa_inscricao_municipal_exigencias')
            ->where('processo', '=', $idChamado)
            ->whereNull('data_leitura_lecom')
            ->count();

        if (!$exigenciaNaoRespondida) {

            $notificacao = DB::connection('lecom')
                ->table('v_consulta_servico_inscricao')
                ->where('Chamado', '=', $idChamado)
                ->orderBy('Ciclo', 'DESC')
                ->first();
        }

        return view("empreendedorismo.baixa_inscricao_municipal.exigencia.exigencia", [
            'idChamado' => $idChamado,
            'exigencia' => isset($notificacao) ? $notificacao->motivo : null,
            'exigenciaNaoRespondida' => $exigenciaNaoRespondida ?? null
        ]);
    }

    public function enviarExigencia(Request $request): void
    {
        $processoLecom = DB::connection('lecom')
            ->table('v_consulta_servico_inscricao')
            ->where('Chamado', '=', $request->idChamado)
            ->orderBy('Ciclo', 'DESC')
            ->first();

        $idBaixaInscricao = DB::connection('integracao')
            ->table('lecom_baixa_inscricao_municipal')
            ->where('chamado_lecom', '=', $processoLecom->Chamado)
            ->pluck('id')
            ->first();

        $exigencia = new \stdClass();
        $exigencia->id_baixa_inscricao_municipal = $idBaixaInscricao;
        $exigencia->processo = $processoLecom->Chamado;
        $exigencia->cpf_cnpj = $processoLecom->cpf;
        $exigencia->ciclo = $processoLecom->Ciclo;
        $exigencia->exigencia = $request->exigencia;
        $exigencia->cumprimento_exigencia = $request->cumprimentoExigencia;
        $exigencia->data = date('Y-m-d H:i:s');

        $idExigencia = $this->salvarExigencia($exigencia);

        $this->salvarDocumentacaoExigencia($idExigencia, $processoLecom->cpf, $processoLecom->Chamado);
        session()->forget('documentos_exigencia');
    }

    private function salvarExigencia(object $exigencia): int
    {
        $idExigencia = DB::connection('integracao')
            ->table('lecom_baixa_inscricao_municipal_exigencias')
            ->insertGetId((array) $exigencia);

        return $idExigencia;
    }

    private function salvarDocumentacaoExigencia($idExigencia, $cpfOuCnpj, $processo)
    {
        if (session()->has('documentos_exigencia')) {
            foreach (session('documentos_exigencia') as $documento) {

                DB::connection('integracao')
                    ->table('lecom_baixa_inscricao_municipal_exigencias_arquivos')
                    ->insert([
                        "id_exigencia" => $idExigencia,
                        "processo" => $processo,
                        "cpf_cnpj" => $cpfOuCnpj,
                        "descricao" => $documento->descricao,
                        "documento" => $documento->caminho,
                        "data" => date('Y-m-d H:i:s')
                    ]);
            }
        }
    }
}
