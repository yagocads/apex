<div class="card">
    <div class="card-header">
        <h5>Agrupamentos</h5>
    </div>

    <div class="card-body">
        <h6><b>Accordions:</b></h6>
        <hr>

        <div id="accordion">
            <div class="card mb-3">
                <a class="card-link no-underline text-dark" data-toggle="collapse" href="#titulo">
                    <div class="card-header">
                        <i class="fa fa-arrow-down arrow-red"></i> Título 1
                    </div>
                </a>
                <div id="titulo" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        <p>Conteúdo...</p>
                    </div>
                </div>
            </div>

            <div class="card">
                <a class="card-link no-underline text-dark" data-toggle="collapse" href="#titulo2">
                    <div class="card-header">
                        <i class="fa fa-arrow-down arrow-red"></i> Título 2
                    </div>
                </a>
                <div id="titulo2" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        <p>Conteúdo...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>