@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h3>
                <i class="fa fa-home mb-3"></i> Legalização de Empresa - MEI
            </h3>

            <div class="alert alert-info">
                Alvará de Localização e Funcionamento é o documento liberado pelo município (Prefeitura Municipal), para os estabelecimentos que exercem atividades comerciais, de prestação de serviços ou de atividades industriais, como forma de comprovação de que eles se encontram autorizados e aptos a exercerem suas atividades em determinado local.
            </div>


            <form method="POST" name="formLegalizacaoMei" action="{{ route('formLegalizacaoMeiSalvar') }}" autocomplete="off">
                @csrf
   
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <b>ATENÇÃO:</b><br><br>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            
                <div id="accordion">
                    
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#inicio">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Informações Iniciais
                            </div>
                        </a>
                        <div id="inicio" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("empresa.mei.inicio")
                            </div>
                        </div>
                    </div>

                    <section id="camposEmpresa" style="display: none;">
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#empresa">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados da Empresa - MEI
                                </div>
                            </a>
                            <div id="empresa" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empresa.mei.empresa.empresa")
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="camposGeral" style="display: none;">
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#solicitante">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Solicitante
                                </div>
                            </a>
                            <div id="solicitante" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empresa.mei.solicitante.solicitante")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#cnae">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> CNAE
                                </div>
                            </a>
                            <div id="cnae" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empresa.mei.cnae.cnae")
                                </div>
                            </div>
                        </div>
                        
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#endereco">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Endereço da Empresa
                                </div>
                            </a>
                            <div id="endereco" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empresa.mei.endereco.endereco")
                                </div>
                            </div>
                        </div>
                        
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#contato">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados de Contato
                                </div>
                            </a>
                            <div id="contato" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empresa.mei.contato.contato")
                                </div>
                            </div>
                        </div>
                        
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#socios">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Quadro Societário
                                </div>
                            </a>
                            <div id="socios" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("empresa.mei.socios.socios")
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'cadastro']) }}" class="btn btn-primary mt-3">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                    
                        <button class="btn btn-success mt-3 enviar_informacoes" style="display: none;">Enviar Informações <i class="fa fa-send"></i></button>
                    </div>
                </div>
 
            </form>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script>
    // Spinner
    var load = $(".ajax_load");

    $(function() {

        $("#percentual_socio").mask("000,0",{reverse: true});
        $("#numero_empresa").mask("0000000");
        $("#iptu_empresa").mask("000000");

        @if( empty( $errors->all() ) )
            $("#inicio").collapse('show');
        @endif

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#cnae').on('shown.bs.collapse', function () {
            recalcularDataTable("dados_cnae");
        });

        $('#socios').on('shown.bs.collapse', function () {
            recalcularDataTable("dados_socios");
        });

        $('#empresa').on('shown.bs.collapse', function () {
            recalcularDataTable("documentos_empresa");
        });

        $('#solicitante').on('shown.bs.collapse', function () {
            recalcularDataTable("documentos_solicitante");
        });

        $('#endereco').on('shown.bs.collapse', function () {
            recalcularDataTable("documento_endereco");
        });

        $("#cnpj_empresa").focusin(function(){
            $("#cnpj_empresa").val("");
            $("#mensagemCnpjCadastrado").hide(200);
            $("#mensagemCnpjExistente").hide(200);

            $("#camposGeral").hide();
            $("#camposCNPJ").hide();
            $("#arquivosCNPJ").hide();
            $("#arquivosCNPJEnviados").hide();
            $("#exibe_protocolo").hide();

            limparCamposEmpresa()
            limparCamposSocio()
            limpaFormularioCepSolicitante()
            limpaFormularioCepEmpresa()

            limparArquivosSessao("documentos_empresa", "empresa", "tabela_documentos", "conteudo_documentos_empresa", true);
            limparArquivosSessao("Socios", "socios", "tabela_documentos", "conteudo_socios", true);
            limparArquivosSessao("documentos_solicitante", "solicitante", "tabela_documentos", "conteudo_documentos_solicitante", true);
            limparArquivosSessao("dados_cnae", "cnae", "tabela_documentos", "conteudo_dados_cnae", false);

        });

        $("input[name='oquefazer']").on("click", function() {
            if($("input[name='oquefazer']:checked").val() == "Alterar uma inscrição municipal existente"){
                $("#perguntaCNPJ").hide();
                $("#mensagemAlteracao").show();
                $("#camposEmpresa").hide();
                $("#camposGeral").hide();
            }
            else{
                $("#perguntaCNPJ").show();
                $("#mensagemAlteracao").hide();

            }
        });

        $("input[name='possuiCNPJ']").on("click", function() {

            limparArquivosSessao("dados_cnae", "cnae", "tabela_documentos", "conteudo_dados_cnae", false);
            limparArquivosSessao("documentos_empresa", "empresa", "tabela_documentos", "conteudo_documentos_empresa", true);
            limparArquivosSessao("Socios", "socios", "tabela_documentos", "conteudo_socios", true);
            limparArquivosSessao("documentos_solicitante", "solicitante", "tabela_documentos", "conteudo_documentos_solicitante", true);
            limparArquivosSessao("dados_cnae", "cnae", "tabela_documentos", "conteudo_dados_cnae", false);
            $("#exibe_protocolo").hide();

            if($("input[name='possuiCNPJ']:checked").val() == "Sim"){
                $("#camposEmpresa").show();
                $("#btnConsultarCNPJ").show();
                $("#campoCNPJ").show();
                
                $("#camposGeral").hide();
                $("#camposCNPJ").hide();
                $("#arquivosCNPJ").hide();
                $("#arquivosCNPJEnviados").hide();
                $("#div_DadosEMpresa").removeClass("col-lg-12")
                $("#div_DadosEMpresa").addClass("col-lg-6")
                $(".enviar_informacoes").hide(); 
                $("#empresa").collapse('show');
                $("#cnpj_empresa").focus(); 
                
                $("#exibe_cnpj").show();
                $("#exibe_certificado").show();
                $("#exibe_contrato").show();
            }
            else{
                $("#camposEmpresa").show();
                $("#btnConsultarCNPJ").hide();
                $("#campoCNPJ").hide();

                $("#camposGeral").show();
                $("#camposCNPJ").show();

                $("#div_DadosEMpresa").removeClass("col-lg-6")
                $("#div_DadosEMpresa").addClass("col-lg-12")

                // $("#arquivosCNPJ").show();                
                // $("#arquivosCNPJEnviados").show(function() {
                //     recalcularDataTable("documentos_empresa");        
                // });   
                $(".enviar_informacoes").show();  
                $("#empresa").collapse('show');
                $("#possui_protocolo_viabilidade").focus();  
                
                $("#exibe_cnpj").hide();
                $("#exibe_certificado").hide();
                $("#exibe_contrato").hide();

            }
        });

        $("#horario_funcionamento").on("change", function() {
            if($("#horario_funcionamento").val() == "Outros"){
                $("#outros_horarios").attr("disabled", false);
                $("#outros_horarios").focus();
            }
            else{
                $("#outros_horarios").attr("disabled", true);
            }
        });

        $("#possui_protocolo_viabilidade").on("change", function() {
            if($("#possui_protocolo_viabilidade").val() == ""){
                $("#protocolo_viabilidade").attr("disabled", true);
                $("#questionarioViabilidade").hide();
                if($("input[name='possuiCNPJ']:checked").val() == "Sim"){
                    $("#div_DadosEMpresa").removeClass("col-lg-12")
                    $("#div_DadosEMpresa").addClass("col-lg-6")

                    $("#arquivosCNPJ").show();                
                    $("#arquivosCNPJEnviados").show(function() {
                        recalcularDataTable("documentos_empresa");        
                    }); 
                }
                else{
                    $("#div_DadosEMpresa").removeClass("col-lg-6")
                    $("#div_DadosEMpresa").addClass("col-lg-12")

                    $("#arquivosCNPJ").hide();
                    $("#arquivosCNPJEnviados").hide(); 
                }
                $("#exibe_protocolo").hide();
            }
            else if($("#possui_protocolo_viabilidade").val() == "Sim"){
                console.log("SIM");
                $("#protocolo_viabilidade").attr("disabled", false);
                $("#protocolo_viabilidade").attr("readonly", false);
                $("#protocolo_viabilidade").focus();
                $("#questionarioViabilidade").hide();
                $("#div_DadosEMpresa").removeClass("col-lg-12")
                $("#div_DadosEMpresa").addClass("col-lg-6")

                $("#arquivosCNPJ").show();                
                $("#arquivosCNPJEnviados").show(function() {
                    recalcularDataTable("documentos_empresa");        
                }); 
                $("#exibe_protocolo").show();

            }
            else{
                $("#protocolo_viabilidade").attr("disabled", true);
                $("#questionarioViabilidade").show();
                if($("input[name='possuiCNPJ']:checked").val() == "Sim"){
                    $("#div_DadosEMpresa").removeClass("col-lg-12")
                    $("#div_DadosEMpresa").addClass("col-lg-6")

                    $("#arquivosCNPJ").show();                
                    $("#arquivosCNPJEnviados").show(function() {
                        recalcularDataTable("documentos_empresa");        
                    }); 
                }
                else{
                    $("#div_DadosEMpresa").removeClass("col-lg-6")
                    $("#div_DadosEMpresa").addClass("col-lg-12")

                    $("#arquivosCNPJ").hide();
                    $("#arquivosCNPJEnviados").hide(); 
                }
                $("#exibe_protocolo").hide();
            }
        });

        $("#possui_recurso_viabilidade").on("change", function() {
            if($("#possui_recurso_viabilidade").val() == ""){
                $("#recurso_viabilidade").attr("disabled", true);
            }
            else if($("#possui_recurso_viabilidade").val() == "Sim"){
                $("#recurso_viabilidade").attr("disabled", false);
                $("#recurso_viabilidade").attr("readonly", false);
                $("#recurso_viabilidade").focus();
            }
            else{
                $("#recurso_viabilidade").attr("disabled", true);
            }
        });

        $("#atividade_cnae").on("change", function() {
            var atividade = $("#atividade_cnae option:selected").val().split("|");;
            $("#codigo_cnae").val(atividade[1]);
            $("#grupo_cnae").val(atividade[2]);
        });

        $("#cpf_requerente").on("change", function() {
            let cpf = $(this).val();

            if (!validarCPF(cpf)) {
                alert("Atenção: CPF Inválido! Informe corretamente o número do seu CPF.");
                $(this).val("");
                $(this).focus();
                return;
            }
        });

        validarTodosCamposEmail();

        function validarTodosCamposEmail() {

            $("input[type=email]").on("change", function() {
                let email = $(this).val();

                if (!validaEmail(email)) {
                    alert("Atenção: O formato do E-mail é inválido.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        validarEmailsCorrespondem();

        function validarEmailsCorrespondem() {

            $("input[name=confirmacao_email]").on("change", function() {
                
                let email = $("input[name=email]").val();
                let confirmacao_email = $(this).val();

                if (email !== confirmacao_email) {
                    alert("Atenção: Os e-mails não correspondem.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        $("body").on("click", ".consultar-cnpj", function(e) {
            e.preventDefault();

            if(!validarCNPJ($("input[name=cnpj_empresa]").val())){
                alert("CNPJ Inválido! Informe corretamente o número do CNPJ.");
                return false;
            }

            let cnpj = $("input[name=cnpj_empresa]").val();

            let formData = new FormData();

            formData.append("cnpj", cnpj);

            $.ajax({
                url: "{{ route('validaCnpjECidade') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    resposta = JSON.parse(response);

                    if(resposta.empresaCadastrada){
                        if(resposta.sistema == "e-cidade"){
                            $("#mensagemCnpjExistente").show();
                        }
                        else if(resposta.sistema == "portal"){
                            $("#mensagemCnpjCadastrado").show();
                        }
                    }
                    else{
                        dadosEmpresa = JSON.parse(resposta.dados);

                        // $("#cnpj_empresa").attr("readonly", true);
                        // $(".consultar-cnpj").attr("disabled", true);
                        
                        $("#camposCNPJ").show();
                        $("#arquivosCNPJ").show();
                        $("#camposGeral").show();
                        $(".enviar_informacoes").show();
                        $("#arquivosCNPJEnviados").show();
                        $("#mensagemCnpjExistente").hide();
                        
                        $("#razao_social").val(dadosEmpresa.nome);
                        $("#nome_fantasia").val(dadosEmpresa.fantasia);

                        $("#cep_empresa").val(dadosEmpresa.cep);
                        $("#endereco_empresa").val(dadosEmpresa.logradouro);
                        
                        if(!Number.isInteger(parseInt(dadosEmpresa.numero))){
                            $("#numero_empresa").val("");
                        }
                        else{
                            $("#numero_empresa").val(dadosEmpresa.numero);
                        }
                        $("#complemento_empresa").val(dadosEmpresa.complemento);
                        $("#bairro_empresa").val(dadosEmpresa.bairro);
                        $("#municipio_empresa").val(dadosEmpresa.municipio);
                        $("#estado_empresa").val(dadosEmpresa.uf);

                        if($("#razao_social").val() != "" ){
                            $("#razao_social").attr("readonly", true);
                        }
                        if($("#nome_fantasia").val() != "" ){
                            $("#nome_fantasia").attr("readonly", true);
                        }
                        if($("#cep_empresa").val() != "" ){
                            $("#cep_empresa").attr("readonly", true);
                        }
                        if($("#numero_empresa").val() != "" ){
                            $("#numero_empresa").attr("readonly", true);
                        }
                        if($("#complemento_empresa").val() != "" ){
                            $("#complemento_empresa").attr("readonly", true);
                        }

                        $("#comercial").val(dadosEmpresa.telefone);
                        $("#comercial").attr("readonly", true);


                        let cnaePrincipal = JSON.stringify(dadosEmpresa.atividade_principal);
                        let cnaeSecundarias = JSON.stringify(dadosEmpresa.atividades_secundarias);

                        let formDataCnae = new FormData();

                        formDataCnae.append("principal", cnaePrincipal);
                        formDataCnae.append("secundarias", cnaeSecundarias);

                        $.ajax({
                            url: "{{ route('incluirCNAE') }}",
                            method: 'POST',
                            data: formDataCnae,
                            processData: false,
                            contentType: false,
                            success(response) {

                                $(".conteudo_dados_cnae").html(response);

                                recalcularDataTable("dados_cnae", false);
                            }
                        });
                    }
                    
                }
            });
        });

        $("body").on("click", ".btn-enviar-documento-cnae", function(e) {
            e.preventDefault();

            if($("#tipo_cnae").val() == "" ){
                alert("Por favor, informe o o Tipo do CNAE");
                $("#tipo_cnae").focus();
                return false;
            }

            if($("#atividade_cnae").val() == "" ){
                alert("Por favor, informe a Atividade CNAE");
                $("#atividade_cnae").focus();
                return false;
            }



            let formData = new FormData();

            atividade = $("#atividade_cnae").val().split("|");
            formData.append("tipo_cnae", $("#tipo_cnae").val());
            formData.append("atividade_cnae", atividade[0]);
            formData.append("codigo_cnae", $("#codigo_cnae").val());
            formData.append("grupo_cnae", $("#grupo_cnae").val());

            $.ajax({
                url: "{{ route('salvar_dados_cnae') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);


                   
                    if (response.principalIncluido) {
                        alert("ATENÇÃO: Já existe uma atividade cadastrada como Principal");
                        return false;
                    }
                   
                    if (response.codigoIncluido) {
                        alert("ATENÇÃO: A atividade informada já foi incluida");
                        return false;
                    }


                    $(".conteudo_dados_cnae").html(response);
                    let id_tabela ="dados_cnae";
                    recalcularDataTable(id_tabela);

                    $("#tipo_cnae").val("");
                    $("#tipo_cnae").focus();
                    $("#atividade_cnae").val("");
                    $("#codigo_cnae").val("");
                    $("#grupo_cnae").val("");

                }
            });

        });

        $("body").on("click", ".btn-enviar-documento-socio", function(e) {
            e.preventDefault();

            if($("#cpf_socio").val() == "" ){
                alert("Por favor, informe o CPF do sócio que deseja cadastrar");
                $("#cpf_socio").focus();
                return false;
            }

            if($("#nome_socio").val() == "" ){
                alert("Por favor, informe o Nome do sócio");
                $("#nome_socio").focus();
                return false;
            }

            if($("#data_nascimento").val() == "" ){
                alert("Por favor, informe Data de Nascimento do sócio");
                $("#data_nascimento").focus();
                return false;
            }

            if($("#email_socio").val() == "" ){
                alert("Por favor, informe o E-Mail do sócio");
                $("#email_socio").focus();
                return false;
            }

            if($("#mae_socio").val() == "" ){
                alert("Por favor, informe o Nome da Mãe do sócio");
                $("#mae_socio").focus();
                return false;
            }

            if($("#percentual_socio").val() == "" ){
                alert("Por favor, informe o Percentual de Participação do sócio");
                $("#percentual_socio").focus();
                return false;
            }

            var percent = $("#percentual_socio").val().replace(",", '.');
            if( percent > 100 ){
                alert("Por favor, o Percentual máximo de Participação do sócio é 100");
                $("#percentual_socio").focus();
                return false;
            }

            if($("#cep_socios").val() == "" ){
                alert("Por favor, informe o CEP do sócio");
                $("#cep_socios").focus();
                return false;
            }

            if($("#endereco_socios").val() == "" ){
                alert("Por favor, informe o Endereço do sócio");
                $("#endereco_socios").focus();
                return false;
            }

            if($("#numero_socios").val() == "" ){
                alert("Por favor, informe o Número do Endereço do sócio");
                $("#numero_socios").focus();
                return false;
            }

            if($("#bairro_socios").val() == "" ){
                alert("Por favor, informe o Bairro do sócio");
                $("#bairro_socios").focus();
                return false;
            }

            if($("#municipio_socios").val() == "" ){
                alert("Por favor, informe o Município do sócio");
                $("#municipio_socios").focus();
                return false;
            }

            if($("#estado_socios").val() == "" ){
                alert("Por favor, informe o Estado do sócio");
                $("#estado_socios").focus();
                return false;
            }

            let arquivo_RG = $('input[name=documento_socio_RG]')[0].files[0];

            if (!validarAnexo(arquivo_RG, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let arquivo_CPF = $('input[name=documento_socio_cpf]')[0].files[0];

            if (!validarAnexo(arquivo_CPF, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let arquivo_CE = $('input[name=documento_socio_comprovante_residencia]')[0].files[0];

            if (!validarAnexo(arquivo_CE, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivoRG", arquivo_RG);
            formData.append("arquivoCPF", arquivo_CPF);
            formData.append("arquivoCE", arquivo_CE);

            formData.append("cpf_socio", $("#cpf_socio").val());
            formData.append("nome_socio", $("#nome_socio").val());
            formData.append("data_nascimento", $("#data_nascimento").val());
            formData.append("email_socio", $("#email_socio").val());
            formData.append("mae_socio", $("#mae_socio").val());
            formData.append("percentual_socio", $("#percentual_socio").val());
            formData.append("cep_socios", $("#cep_socios").val());
            formData.append("endereco_socios", $("#endereco_socios").val());
            formData.append("numero_socios", $("#numero_socios").val());
            formData.append("complemento_socios", $("#complemento_socios").val());
            formData.append("bairro_socios", $("#bairro_socios").val());
            formData.append("municipio_socios", $("#municipio_socios").val());
            formData.append("estado_socios", $("#estado_socios").val());

            $.ajax({
                url: "{{ route('salvar_dados_socio') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    $("input[name=documento_socio_RG]").val("");
                    $("input[name=documento_socio_cpf]").val("");
                    $("input[name=documento_socio_comprovante_residencia]").val("");

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }


                    $(".conteudo_socios").html(response);
                    let id_tabela ="dados_socios";
                    recalcularDataTable(id_tabela);

                    $("#cpf_socio").focus();
                    $("#cpf_socio").val("");

                }
            });

        });

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $(this).data("descricao");
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");
            
            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);

            $.ajax({
                url: "{{ route('salvar_documento_servidor_legalizaMEI') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-excluir-documento-cnae", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let codigo = $(this).data("codigo");

            $.ajax({
                url: "{{ route('excluir_dados_cnae') }}",
                method: 'POST',
                data: {
                    "codigo": codigo,
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $(".conteudo_dados_cnae").html(response);
                    let id_tabela ="dados_cnae";
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-excluir-documento", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let nome_arquivo = $(this).data("nome_arquivo");
            let nome_sessao = $(this).data("nome_sessao");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = $(this).data("nome_tabela");
            let pasta = $(this).data("pasta");
            let conteudo_documentos = $(this).data("conteudo_documentos");

            $.ajax({
                url: "{{ route('excluir_documento_servidor_legalizaMEI') }}",
                method: 'POST',
                data: {
                    "nome_arquivo": nome_arquivo,
                    "nome_sessao": nome_sessao,
                    "pasta": pasta,
                    "nome_tabela": nome_tabela,
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-excluir-documento-socio", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }
            let cpfSocio = $(this).data("cpf");

            let formData = new FormData();

            formData.append( "cpfSocio" , cpfSocio);
            formData.append( "nome_arquivoRG" , $(this).data("nome_arquivorg"));
            formData.append( "nome_arquivoCPF" , $(this).data("nome_arquivocpf"));
            formData.append( "nome_arquivoCE" , $(this).data("nome_arquivoce"));

            $.ajax({
                url: "{{ route('excluir_documento_servidor_SocioMEI') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $(".conteudo_socios").html(response);
                    let id_tabela ="dados_socios";
                    recalcularDataTable(id_tabela);
                }
            });
        });

    });


    verificarDocumentosNecessariosEInformacoes();

    function verificarDocumentosNecessariosEInformacoes() {
        $(".enviar_informacoes").on("click", function(e) {
            e.preventDefault();

            if( $("#possui_protocolo_viabilidade").val() == "" ){
                alert("Por Favor informe se possuiu Protocolo de Viabilidade");
                $("#empresa").collapse('show');
                $("#possui_protocolo_viabilidade").focus();
                return false;
            }
            
            if( $("#possui_protocolo_viabilidade").val() == "Sim" ){
                if( $("#protocolo_viabilidade").val() == "" ){
                    alert("Por Favor informe Número do Protocolo de Viabilidade");
                    $("#empresa").collapse('show');
                    $("#protocolo_viabilidade").focus();
                    return false;
                }
            }

            if( $("#possui_protocolo_viabilidade").val() == "Não" ){
                if( $("#horario_funcionamento").val() == "" ){
                    alert("Por Favor informe o Horário de Funcionamento da Empresa");
                    $("#empresa").collapse('show');
                    $("#horario_funcionamento").focus();
                    return false;
                }
                if( $("#horario_funcionamento").val() == "Outros" ){
                    if( $("#outros_horarios").val() == "" ){
                        alert("Por Favor informe o Horário da Empresa");
                        $("#empresa").collapse('show');
                        $("#outros_horarios").focus();
                        return false;
                    }
                }

                if( $("#anexo_residencia").val() == "" ){
                    alert("Por Favor informe se a Empresa é Anexa a Residência");
                    $("#empresa").collapse('show');
                    $("#anexo_residencia").focus();
                    return false;
                }

                if( $("#porte_empresa").val() == "" ){
                    alert("Por Favor informe o Porte da Empresa da Empresa");
                    $("#empresa").collapse('show');
                    $("#porte_empresa").focus();
                    return false;
                }

                if( $("#ponto_referencia").val() == "" ){
                    alert("Por Favor informe de existe um Ponto de Refrência");
                    $("#empresa").collapse('show');
                    $("#ponto_referencia").focus();
                    return false;
                }

                if( $("#tipo_uso").val() == "" ){
                    alert("Por Favor informe o Tipo de Uso da Empresa");
                    $("#empresa").collapse('show');
                    $("#tipo_uso").focus();
                    return false;
                }

                if( $("#outra_empresa").val() == "" ){
                    alert("Por Favor informe se existe outra Empresa no local");
                    $("#empresa").collapse('show');
                    $("#outra_empresa").focus();
                    return false;
                }

                if( $("#atendimento_publico").val() == "" ){
                    alert("Por Favor informe se a Empresa faz Atendimento ao Público");
                    $("#empresa").collapse('show');
                    $("#atendimento_publico").focus();
                    return false;
                }

                if( $("#autorizacao").val() == "" ){
                    alert("Por Favor informe se existe Autorização do Proprietário para o funcionamento da Empresa");
                    $("#empresa").collapse('show');
                    $("#autorizacao").focus();
                    return false;
                }

                if( $("#separacao_fisica").val() == "" ){
                    alert("Por Favor informe se existe separação entre as Empresas");
                    $("#empresa").collapse('show');
                    $("#separacao_fisica").focus();
                    return false;
                }
            }
            if( $("#possui_recurso_viabilidade").val() == "" ){
                alert("Por Favor informe se possuiu Recurso de Viabilidade");
                $("#empresa").collapse('show');
                $("#possui_recurso_viabilidade").focus();
                return false;
            }
            if( $("#possui_recurso_viabilidade").val() == "Sim" ){
                if( $("#recurso_viabilidade").val() == "" ){
                    alert("Por Favor informe o Recurso de Viabilidade");
                    $("#empresa").collapse('show');
                    $("#recurso_viabilidade").focus();
                    return false;
                }
            }

            if( $("#fator_tipo_juridico").val() == "" ){
                alert("Por Favor informe o Tipo Jurídico da Empresa");
                $("#empresa").collapse('show');
                $("#fator_tipo_juridico").focus();
                return false;
            }

            if( $("#razao_social").val() == "" ){
                alert("Por Favor informe a Razão Social da Empresa");
                $("#empresa").collapse('show');
                $("#razao_social").focus();
                return false;
            }

            // Solicitante
            if( $("#identidade_solicitante").val() == "" ){
                alert("Por Favor informe a Identidade do Solicitante");
                $("#solicitante").collapse('show');
                $("#identidade_solicitante").focus();
                return false;
            }

            if( $("#orgao_emissor_solicitante").val() == "" ){
                alert("Por Favor informe o Orgão Emissor da Identidade do Solicitante");
                $("#solicitante").collapse('show');
                $("#orgao_emissor_solicitante").focus();
                return false;
            }

            if( $("#data_emissao_solicitante").val() == "" ){
                alert("Por Favor informe a Data de Emissão da Identidade do Solicitante");
                $("#solicitante").collapse('show');
                $("#data_emissao_solicitante").focus();
                return false;
            }

            if( $("#cep_solicitante").val() == "" ){
                alert("Por Favor informe o CEP do Solicitante");
                $("#solicitante").collapse('show');
                $("#cep_solicitante").focus();
                return false;
            }

            if( $("#numero_solicitante").val() == "" ){
                alert("Por Favor informe o Número do Endereço do Solicitante");
                $("#solicitante").collapse('show');
                $("#numero_solicitante").focus();
                return false;
            }

            // CNAE
            var $achouCnaePrincipal = false;

            var documentosCnae = $("#dados_cnae").DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Pesquisar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "bLengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "paginate": true
            }).columns.adjust().responsive.recalc();

            documentosCnae.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 0){
                        console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                        if(column == "ATIVIDADE PRINCIPAL"){
                            $achouCnaePrincipal = true
                        }
                    }
                });
            });
            if(!$achouCnaePrincipal){
                alert("Por Favor é necessário ter uma ATIVIDADE PRINCIPAL");
                $("#cnae").collapse('show');
                $("#tipo_cnae").focus();
                return false;   
            } 






            // Endereço da Empresa
            if( $("#cep_empresa").val() == "" ){
                alert("Por Favor informe o CEP da Empresa");
                $("#endereco").collapse('show');
                $("#cep_empresa").focus();
                return false;
            }

            if( $("#numero_empresa").val() == "" ){
                alert("Por Favor informe o Número do Endereço da Empresa");
                $("#endereco").collapse('show');
                $("#numero_empresa").focus();
                return false;
            }

            if( $("#iptu_empresa").val() == "" ){
                alert("Por Favor informe o IPTU da Empresa");
                $("#endereco").collapse('show');
                $("#iptu_empresa").focus();
                return false;
            }

            if( $("#ii_empresa").val() == "" ){
                alert("Por Favor informe a Inscrição Imobiliária da Empresa");
                $("#endereco").collapse('show');
                $("#ii_empresa").focus();
                return false;
            }

            // Contato
            if( $("#celular").val() == "" ){
                alert("Por Favor informe o Número de Celular da Empresa");
                $("#contato").collapse('show');
                $("#celular").focus();
                return false;
            }

            // CNAE
            var $totalPercentualSocios = 0;

            var documentosSocios = $("#dados_socios").DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Pesquisar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "bLengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "paginate": true
            }).columns.adjust().responsive.recalc();

            documentosSocios.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 3){
                        console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                        $totalPercentualSocios = $totalPercentualSocios + parseFloat(column.replace(",", "."));
                    }
                });
            });

            console.log($totalPercentualSocios);
            if($totalPercentualSocios != 100){
                alert("Por Favor é necessário que o Percentual de Participação dos Sócios seja igual a 100");
                $("#socios").collapse('show');
                $("#cpf_socio").focus();
                return false;   
            } 





            let documentos_necessarios = [];

            verificarNecessidadeAdicionarDocumentosEmpresa(documentos_necessarios);
            adicionarDocumentosEnderecoSolicitante(documentos_necessarios);
            // adicionarDocumentosEnderecoEmpresa(documentos_necessarios);

            verificarEnvioDocumentosGeral(documentos_necessarios);
        });
    }

    function verificarNecessidadeAdicionarDocumentosEmpresa(documentos_necessarios) {
        if($("input[name='possuiCNPJ']:checked").val() == "Sim"){
            documentos_necessarios.push({
                "nome_sessao": "documentos_empresa",
                "data_descricao":"Cartão CNPJ",
                "descricao": "Cartão CNPJ em (Dados da Empresa - MEI)",
                "qtd": 1
            });
            documentos_necessarios.push({
                "nome_sessao": "documentos_empresa",
                "data_descricao":"Certificado de Microempreendedor",
                "descricao": "Certificado de Microempreendedor em (Dados da Empresa - MEI)",
                "qtd": 1
            });
            documentos_necessarios.push({
                "nome_sessao": "documentos_empresa",
                "data_descricao":"Contrato de Locação",
                "descricao": "Contrato de Locação em (Dados da Empresa - MEI)",
                "qtd": 1
            });

        }

        if($("#possui_protocolo_viabilidade").val() == "Sim"){
            documentos_necessarios.push({
                "nome_sessao": "documentos_empresa",
                "data_descricao":"Protocolo de Viabilidade - REGIN",
                "descricao": "Protocolo de Viabilidade - REGIN em (Dados da Empresa - MEI)",
                "qtd": 1
            });
        }
    }

    function adicionarDocumentosEnderecoEmpresa(documentos_necessarios) {
        documentos_necessarios.push({
            "nome_sessao": "documento_endereco",
            "data_descricao":"Comprovante de Endereço",
            "descricao": "Comprovante de Endereço em (Endereço)",
            "qtd": 1
        });
    }

    function limparArquivosSessao(sessao, pasta, tabela, destino, apagarArquivos){

        $.ajax({
            url: "{{ route('limpar_arquivos_sessao') }}",
            method: 'POST',
            data: {
                "sessao": sessao,
                "pasta": pasta,
                "tabela": tabela,
                "apagarArquivos": apagarArquivos
            },
            // beforeSend: function () {
            //     // load.fadeIn(200).css("display", "flex");
            // },
            success(response) {
                // load.fadeOut(200);

                $("." + destino).html(response);
                recalcularDataTable(tabela);
            }
        });
    }

    function adicionarDocumentosEnderecoSolicitante(documentos_necessarios) {
        documentos_necessarios.push({
            "nome_sessao": "documentos_solicitante",
            "data_descricao":"CPF Solicitante",
            "descricao": "CPF Solicitante em (Dados do Solicitante)",
            "qtd": 1
        });
        documentos_necessarios.push({
            "nome_sessao": "documentos_solicitante",
            "data_descricao":"RG Solicitante",
            "descricao": "RG Solicitante em (Dados do Solicitante)",
            "qtd": 1
        });
        documentos_necessarios.push({
            "nome_sessao": "documentos_solicitante",
            "data_descricao":"Comprovante Residência Solicitante",
            "descricao": "Comprovante Residência Solicitante em (Dados do Solicitante)",
            "qtd": 1
        });

        var $achouCpf = false;

        var documentosSocios = $("#dados_socios").DataTable({
            destroy: true,
            "oLanguage": {
                "sProcessing": "Processando...",
                "sLengthMenu": "_MENU_ resultados por página",
                "sZeroRecords": "Não foram encontrados resultados",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sSearch": "Pesquisar:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "bLengthChange": true,
            "ordering": true,
            "info": true,
            "searching": true,
            "paginate": true
        }).columns.adjust().responsive.recalc();

        documentosSocios.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == $("#cpf_solicitante").val()){
                        $achouCpf = true
                    }
                }
            });
        });
        if(!$achouCpf){
            documentos_necessarios.push({
                "nome_sessao": "documentos_solicitante",
                "data_descricao":"Procuração Solicitante",
                "descricao": "Procuração em (Dados do Solicitante)",
                "qtd": 1
            });
        } 

    }

    function verificarEnvioDocumentosGeral(documentos_necessarios) {

        $.ajax({
            url: "{{ route('verificar_envio_documentos_mei') }}",
            method: 'POST',
            data: {
                "documentos_necessarios": documentos_necessarios
            },
            success(response) {

                if (response.documentos_em_falta.length > 0) {
                    for (let i = 0; i < response.documentos_em_falta.length; i++) {
                        alert("Atenção: Os seguintes documentos são necessários: " + response.documentos_em_falta[i].descricao);
                    }
                } 
                else {

                    $.ajax({
                        url: "{{ route('verificar_atividade_cnae') }}",
                        method: 'GET',
                        success(response) {
                            resposta =  JSON.parse(response);
                            if( !resposta.atividade){
                                alert("Por Favor inclua as Atividades da Empresa");
                                $("#cnae").collapse('show');
                                $("#tipo_cnae").focus();
                                return false;
                            }
                            else{
                                $.ajax({
                                    url: "{{ route('verificar_socios_cadastrados') }}",
                                    method: 'GET',
                                    success(response) {
                                        resposta =  JSON.parse(response);
                                        if( !resposta.cadastrados ){
                                            alert("Por Favor informe os Dados dos Sócios da Empresa");
                                            $("#socios").collapse('show');
                                            $("#cpf_socio").focus();                    
                                            return false;
                                        }
                                        else{
                                            $("form[name=formLegalizacaoMei]").submit();
                                        }
                                    }
                                });
                            }
                        }
                    });



                }
            }
        });
    }

    function limpaFormularioCpfSocios() {
        document.getElementById('nome_socio').value = ("");
        document.getElementById('data_nascimento').value = ("");
        document.getElementById('email_socio').value = ("");
        document.getElementById('mae_socio').value = ("");
        document.getElementById('endereco_socios').value = ("");
        document.getElementById('bairro_socios').value = ("");
        document.getElementById('municipio_socios').value = ("");
        document.getElementById('estado_socios').value = ("");
    }
        
    function limpaFormularioCepSocios() {
        document.getElementById('endereco_socios').value = ("");
        document.getElementById('bairro_socios').value = ("");
        document.getElementById('municipio_socios').value = ("");
        document.getElementById('estado_socios').value = ("");
    }
    
    function limpaFormularioCepSolicitante() {
        document.getElementById('endereco_solicitante').value = ("");
        document.getElementById('bairro_solicitante').value = ("");
        document.getElementById('municipio_solicitante').value = ("");
        document.getElementById('estado_solicitante').value = ("");
    }

    function limpaFormularioCepEmpresa() {
        document.getElementById('endereco_empresa').value = ("");
        document.getElementById('bairro_empresa').value = ("");
        document.getElementById('municipio_empresa').value = ("");
        document.getElementById('estado_empresa').value = ("");
    }
    
    function meuCallbackEmpresa(conteudo) {

        if (!("erro" in conteudo)) {
            // if(conteudo.localidade.toUpperCase() == "MARICÁ" || conteudo.localidade.toUpperCase() == "MARICA"  ){
                document.getElementById('endereco_empresa').value = (conteudo.logradouro);
                document.getElementById('bairro_empresa').value = (conteudo.bairro);
                document.getElementById('municipio_empresa').value = (conteudo.localidade);
                document.getElementById('estado_empresa').value = (conteudo.uf);
            // } else {
            //     limpaFormularioCepRequerente();
            //     alert("Somente endereços do município de Maricá são permitidos");
            // }
        } else {
            limpaFormularioCepEmpresa();
            alert("CEP não encontrado.");
        }
    }
    
    function meuCallbackSolicitantes(conteudo) {

        if (!("erro" in conteudo)) {
            document.getElementById('endereco_solicitante').value = (conteudo.logradouro);
            document.getElementById('bairro_solicitante').value = (conteudo.bairro);
            document.getElementById('municipio_solicitante').value = (conteudo.localidade);
            document.getElementById('estado_solicitante').value = (conteudo.uf);
        } else {
            limpaFormularioCepRequerente();
            alert("CEP não encontrado.");
        }
    }
    
    function meuCallbackSocios(conteudo) {

        if (!("erro" in conteudo)) {
            // if(conteudo.localidade.toUpperCase() == "MARICÁ" || conteudo.localidade.toUpperCase() == "MARICA"  ){
                document.getElementById('endereco_socios').value = (conteudo.logradouro);
                document.getElementById('bairro_socios').value = (conteudo.bairro);
                document.getElementById('municipio_socios').value = (conteudo.localidade);
                document.getElementById('estado_socios').value = (conteudo.uf);
            // } else {
            //     limpaFormularioCepRequerente();
            //     alert("Somente endereços do município de Maricá são permitidos");
            // }
        } else {
            limpaFormularioCepRequerente();
            alert("CEP não encontrado.");
        }
    }

    function pesquisaCepEmpresa(cep) {

        var cep = cep.replace(/\D/g, '');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $('.ajax_load').fadeIn(200);

                var script = document.createElement('script');

                script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meuCallbackEmpresa';

                document.body.appendChild(script);

                $('.ajax_load').fadeOut(200);
            } else {
                limpaFormularioCepEmpresa();
                alert("Formato de CEP inválido.");

            }
        } else {
            limpaFormularioCepEmpresa();
        }
    }

    function pesquisaCepSolicitante(cep) {

        var cep = cep.replace(/\D/g, '');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $('.ajax_load').fadeIn(200);

                var script = document.createElement('script');

                script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meuCallbackSolicitantes';

                document.body.appendChild(script);

                $('.ajax_load').fadeOut(200);
            } else {
                limpaFormularioCepSolicitante();
                alert("Formato de CEP inválido.");

            }
        } else {
            limpaFormularioCepSolicitante();
        }
    }

    function pesquisaCepSocios(cep) {

        var cep = cep.replace(/\D/g, '');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $('.ajax_load').fadeIn(200);

                var script = document.createElement('script');

                script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meuCallbackSocios';

                document.body.appendChild(script);

                $('.ajax_load').fadeOut(200);
            } else {
                limpaFormularioCepSocios();
                alert("Formato de CEP inválido.");

            }
        } else {
            limpaFormularioCepSocios();
        }
    }

    function pesquisaCPFSocios(cpf) {

        var cpfNum = cpf.replace(/\D/g, '');

        if ($("#cpf_socio").val() != ""){
            if(!validarCPF(cpf)){
                alert("CPF Inválido! Informe corretamente o CPF do Sócio.");
                $("#cpf_socio").focus();
                return false;
            }

            if (cpfNum != "") {

                let formDataCpf = new FormData();

                formDataCpf.append("cpf", cpf);

                $.ajax({
                    url: "{{ route('validaCpfECidade') }}",
                    method: 'POST',
                    data: formDataCpf,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);
                        resposta = JSON.parse(response);

                        if (resposta.cpfIncluido){

                            alert("Este CPF já está Cadastrado.");
                            $("#cpf_socio").focus();
                        }
                        else{
                            if (resposta.cpfCadastrado){

                                popularCamposSocio(resposta)
                            }
                        }
                    }
                });
            } else {
                limpaFormularioCpfSocios();
            }
        }
    }
    
    function limparCamposSocio(){
        document.getElementById('cpf_socio').value = ("");
        document.getElementById('nome_socio').value = ("");
        document.getElementById('data_nascimento').value = ("");
        document.getElementById('mae_socio').value = ("");
        document.getElementById('email_socio').value = ("");
        document.getElementById('cep_socios').value = ("");
        document.getElementById('endereco_socios').value = ("");
        document.getElementById('bairro_socios').value = ("");
        document.getElementById('municipio_socios').value = ("");
        document.getElementById('estado_socios').value = (""); 
        document.getElementById('complemento_socios').value = (""); 
        document.getElementById('numero_socios').value = (""); 
        document.getElementById('percentual_socio').value = (""); 

        $("#nome_socio").attr("readonly", false);
        $("#data_nascimento").attr("readonly", false);
        $("#mae_socio").attr("readonly", false);
        $("#email_socio").attr("readonly", false);
        $("#cep_socios").attr("readonly", false);
        $("#endereco_socios").attr("readonly", false);
        $("#bairro_socios").attr("readonly", false);
        $("#municipio_socios").attr("readonly", false);
        $("#estado_socios").attr("readonly", false);
        $("#complemento_socios").attr("readonly", false);
        $("#numero_socios").attr("readonly", false);

    }

    function popularCamposSocio(informacoes){

        document.getElementById('nome_socio').value = (informacoes.dados.aCgmPessoais.z01_nome);
        document.getElementById('data_nascimento').value = (informacoes.dados.aCgmPessoais.z01_nasc);
        document.getElementById('mae_socio').value = (informacoes.dados.aCgmPessoais.z01_mae);
        document.getElementById('email_socio').value = (informacoes.dados.aCgmContato.z01_email);
        document.getElementById('cep_socios').value = (informacoes.endereco.endereco.iCep);
        document.getElementById('endereco_socios').value = (informacoes.endereco.endereco.sRua);
        document.getElementById('bairro_socios').value = (informacoes.endereco.endereco.sBairro);
        document.getElementById('municipio_socios').value = (informacoes.endereco.endereco.sMunicipio);
        document.getElementById('estado_socios').value = (informacoes.endereco.endereco.sUf); 
        document.getElementById('complemento_socios').value = (informacoes.endereco.endereco.sComplemento); 
        document.getElementById('numero_socios').value = (informacoes.endereco.endereco.sNumeroLocal); 

        $("#nome_socio").attr("readonly", true);
        if ($("#data_nascimento").val() != ""){
            $("#data_nascimento").attr("readonly", true);
        }
        if ($("#mae_socio").val() != ""){
            $("#mae_socio").attr("readonly", true);
        }
        if ($("#email_socio").val() != ""){
            $("#email_socio").attr("readonly", true);
        }
        if ($("#cep_socios").val() != ""){
            $("#cep_socios").attr("readonly", true);
        }
        if ($("#endereco_socios").val() != ""){
            $("#endereco_socios").attr("readonly", true);
        }
        if ($("#bairro_socios").val() != ""){
            $("#bairro_socios").attr("readonly", true);
        }
        if ($("#municipio_socios").val() != ""){
            $("#municipio_socios").attr("readonly", true);
        }
        if ($("#estado_socios").val() != ""){
            $("#estado_socios").attr("readonly", true);
        }
        if ($("#complemento_socios").val() != ""){
            $("#complemento_socios").attr("readonly", true);
        }
        if ($("#numero_socios").val() != ""){
            $("#numero_socios").attr("readonly", true);
        }


    }

    function limparCamposEmpresa(){
        document.getElementById('possui_protocolo_viabilidade').value = ("");
        document.getElementById('protocolo_viabilidade').value = ("");
        $("#protocolo_viabilidade").attr("readonly", true);

        document.getElementById('horario_funcionamento').value = ("");   
        document.getElementById('outros_horarios').value = ("");   
        document.getElementById('anexo_residencia').value = ("");   
        document.getElementById('porte_empresa').value = ("");   
        document.getElementById('ponto_referencia').value = ("");   
        document.getElementById('tipo_uso').value = ("");   
        document.getElementById('outra_empresa').value = ("");   
        document.getElementById('atendimento_publico').value = ("");   
        document.getElementById('autorizacao').value = ("");   
        document.getElementById('separacao_fisica').value = ("");   

        document.getElementById('possui_recurso_viabilidade').value = ("");   
        document.getElementById('recurso_viabilidade').value = (""); 
        $("#recurso_viabilidade").attr("readonly", true);

        document.getElementById('fator_tipo_juridico').value = ("");   
        document.getElementById('nire').value = ("");   
        document.getElementById('razao_social').value = ("");   
        document.getElementById('nome_fantasia').value = (""); 
          
        document.getElementById('documento_cartao_cnpj').value = ("");   
    }

</script>

@endsection