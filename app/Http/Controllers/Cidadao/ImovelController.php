<?php

namespace App\Http\Controllers\Cidadao;

use App\Http\Controllers\Controller;
use App\Models\Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use App\Models\AverbacaoLecom;
use App\Support\ImovelSupport;
use App\Support\CidadaoSupport;
use Illuminate\Support\Facades\Validator;

class ImovelController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->paginator = new \CoffeeCode\Paginator\Paginator();
        $this->properties = new ImovelSupport();
    }

    public function consultaInformacoes(Request $request, $url_servico)
    {
        $document = remove_character_document(Auth::user()->cpf);

        if(isset($request->matricula) && !empty($request->matricula)
            || isset($request->planta) && !empty($request->planta)
            || isset($request->logradouro) && !empty($request->logradouro)
            || isset($request->quadra) && !empty($request->quadra)
            || isset($request->lote) && !empty($request->lote)) {

            $resposta = $this->properties->searchPropertie($request, $document, $request->page);
        } else {
            $resposta = $this->properties->listProperties($request->page, $document, API_LISTA_IMOVEIS_CERTIDAO);
        }
        
        if ($resposta->iStatus === 2) {

            if ($resposta->iNumeroErro === 201) {

                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Foram identificados diversos imóveis em seu cadastro. Dirija-se a uma agência do SIM.<br>
                Em breve este serviço estará disponível aqui para você!.</p>
                Código: ERR_API 002',
                ]);
            } else {

                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi encontrado nenhum imóvel registrado no seu nome.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: IMOVEL_002',
                ]);
            }
        } else {

            $this->paginator->pager(
                $resposta->iPaginacao, // total de páginas
                count($resposta->aMatriculasListaCertidaoEncontradas), // qtd por página
                isset($request->page) ? $request->page : 1 // página atual
            );

            Log::create([ 'servico'=> 11 , 'descricao' => "Consulta de Informações de Exames", 'cpf' => isset(Auth::user()->cpf)?Auth::user()->cpf:"", 'data' => DB::raw('now()'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'], ]);

            $arrBaixa = [];

            foreach ($resposta->aMatriculasListaCertidaoEncontradas as $value) {

                $baixa = $this->properties->getImovelBaixado($value->matricula, $document);
                array_push($arrBaixa, [
                    "matricula" =>  $value->matricula,
                    "baixado" => $baixa->i_matricula_baixada
                ]);
            }

            return view('imovel.consultaimoveis', [
                'registros' => $resposta,
                'baixados' => $arrBaixa,
                'paginator' => $this->paginator->render(),
                'url_servico' => $url_servico
            ]);
        }
    }

    public function imovelInformacao($id)
    {
        $cpfUsuario = remove_character_document(Auth::user()->cpf);

        try {

            $resposta = $this->properties->getPropertie($id, $cpfUsuario);

            if ($resposta->iStatus == 1) {

                $servicosImovel = [
                    'imovel.cadastro' => 'Cadastro do Imóvel',
                    'imovel.caracteristicas' => 'Características do Imóvel',
                    'imovel.lote' => 'Cadastro de Lote',
                    'imovel.construcao' => 'Cadastro de Construções',
                    'imovel.proprietarios' => 'Cadastro de Outros Proprietários',
                    'imovel.isencao' => 'Isenção / Imunidade',
                ];

                return view('imovel.imovel', [
                    'registros' => $resposta,
                    'servicosImovel' => $servicosImovel,
                ]);
            } else {

                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi possível verificar a relação dos imóveis.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: IMOVEL_001',
                ]);
            }
        } catch (\Exception $e) {

            return view('cidadao.falha_integracao', [
                'titulo' => 'Imóvel',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível verificar a relação dos imóveis.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001',
            ]);
        }
    }

    public function imprimeInformacoes($id)
    {
        $cpfUsuario = remove_character_document(Auth::user()->cpf);

        try {

            $data1 = [
                'sTokeValiadacao' => API_TOKEN,
                'sExec' => API_BIC_IMOVEL,
                'i_codigo_maticula' => $id,
                'i_cpfcnpj' => $cpfUsuario,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json' => $json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_IMOVEL,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string)),
                ),
            ));

            if (USAR_SSL) {

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
            }

            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $resposta = json_decode($response);

            if ($resposta->iBicImovelGerada != 1) {

                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi possível gerar a impressão das informações do imóvel.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: IMOVEL_003',
                ]);
            } else {

                stream_context_set_default([
                    'ssl' => [
                        'verify_peer' => SSL_API_VERIFICA,
                        'verify_peer_name' => SSL_API_VERIFICA,
                    ],
                ]);

                $filename = explode("/", $resposta->sUrl);
                $tempImage = tempnam(sys_get_temp_dir(), $filename[count($filename) - 1]);
                copy($resposta->sUrl, $tempImage);
                $headers = array(
                    'Content-Type: application/pdf',
                );
                return response()->download($tempImage, $filename[count($filename) - 1], $headers);
            }
        } catch (\Exception $e) {

            return view('cidadao.falha_integracao', [
                'titulo' => 'Imóvel',
                "mensagem" => '<p><strong>Atenção!</strong>  Não foi possível gerar a impressão das informações do imóvel.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001',
            ]);
        }
    }

    /**
     * listaImoveis -> retorna uma lista ou um determinado imovel. Usado para api
     *
     * @param  mixed $document
     * @param  mixed $matricula
     * @return void
     */
    public function listaImoveis(Request $request, $document = null, $matricula = null)
    {
        if (isset($document)) {
            $document = remove_character_document($document);
        } else {
            $document = remove_character_document(Auth::user()->cpf);
        }

        try {

            if (isset($matricula)) {
                $data = (object) [
                    'matricula' => $matricula
                ];
                $resposta = $this->properties->searchPropertie($data, $document);
                $data = $resposta->aMatriculasListaCertidaoEncontradas;

            } else {

                $resposta = $this->properties->listProperties($request->page, $document);
                $data = $resposta->aMatriculasCgmEncontradas;
            }

            $this->paginator->pager(
                $resposta->iPaginacao, // total de páginas
                isset($resposta->aMatriculasCgmEncontradas) ? count($resposta->aMatriculasCgmEncontradas) : 1 , // qtd por página
                isset($request->page) ? $request->page : 1 // página atual
            );
            return view("imovel.tabela_imoveis" , [
                'registros' => $data,
                'paginator' => $this->paginator->render()]);

        } catch (\Exception $e) {

            return response()->json([
                "mensagem" => 'Imóvel não encontrado.'
            ]);
        }
    }

    /*
    *
    * Lista todos os imóveis do contribuinte
    * ou apenas um imóvel pela matrícula
    *
    */
    public function listaImoveisAverbacao(Request $request, $document, $matricula = null)
    {
        try {
            if ($matricula) {
                $resposta = $this->properties->getPropertie($matricula, $document);
            } else {
                $resposta = $this->properties->listProperties($request->page, $document);

                /**
                * Paginação
                */
                $this->paginator->pager(
                    $resposta->iPaginacao, // total de páginas
                    count($resposta->aMatriculasCgmEncontradas), // qtd por página
                    isset($request->page) ? $request->page : 1 // página atual
                );
            }
        } catch (\Exception $e) {

            return view('cidadao.falha_integracao', [
                'titulo' => 'Imóvel',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível verificar a relação dos imóveis.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001'
            ]);
        }

        return view("imovel.averbacao.pesquisa_imoveis", [
            "registros" => $resposta,
            "paginator" => $this->paginator->render()
        ]);
    }

    public function averbacaoImovel(Request $request, $processo = "")
    {
        try {

            session()->forget("adquirentes");
            session()->forget("documentos_adquirentes");

            $adquirentesImovel = $this->properties->getAdquirentesImovel($request->guia);

            $adquirentePrincipal = 0;

            foreach($adquirentesImovel->aListaAdquiriteTransmitente as $adquirente) {

                session()->push("adquirentes", $adquirente);

                if ($adquirente->princ === "Sim") {
                    $adquirentePrincipal = 1;
                }
            }

            $resposta = $this->properties->getTipoAverbacao();

            if ($resposta->iStatus != 1) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Averbação de Imóveis',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi possível recuperar as informações de solicitação.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: AVERB_001',
                ]);
            } else {
                $exigencias = DB::connection('lecom')
                ->table('v_exigencias_averbar')
                ->where('COD_PROCESSO', '=', $processo )
                ->get();

                $pendencias = [];

                if ($exigencias->count()) {

                    $newDate = date_create($exigencias[$exigencias->count()-1]->DT_REG_IMOVEL);

                    $pendencias = [
                        "COD_PROCESSO" => $exigencias[$exigencias->count() - 1]->COD_PROCESSO,
                        "COD_ETAPA_ATUAL" => $exigencias[$exigencias->count() - 1]->COD_ETAPA_ATUAL,
                        "COD_CICLO_ATUAL" => $exigencias[$exigencias->count() - 1]->COD_CICLO_ATUAL,
                        "NU_GUIA_ITBI" => $exigencias[$exigencias->count() - 1]->NU_GUIA_ITBI,
                        "NU_REG_GERAL_IMOVEL" => $exigencias[$exigencias->count() - 1]->NU_REG_GERAL_IMOVEL,
                        "PROT_REG_IMOVEL" => $exigencias[$exigencias->count() - 1]->PROT_REG_IMOVEL,
                        "MAT_IMOV_CARTORIO" => $exigencias[$exigencias->count() - 1]->MAT_IMOV_CARTORIO,
                        "DT_REG_IMOVEL" => date_format($newDate, 'd-m-Y'),
                    ];
                }

                return view('imovel.averbacao.averbacao', [
                    'registros' => $resposta,
                    'estados' => $this->getStats(),
                    'pendencias' => $pendencias,
                    'matricula' => $request->matricula,
                    'guia' => $request->guia,
                    'adquirentePrincipal' => $adquirentePrincipal
                ]);
            }
        } catch (\Exception $e) {

            return view('cidadao.falha_integracao', [
                'titulo' => 'Imóvel',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível verificar a relação dos imóveis.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001',
            ]);
        }
    }

    public function salvarAdquirenteSessao(Request $request)
    {
        if (!$request->adquierente_existente) {
            $validate = Validator::make(
                $request->all(),
                $this->regrasCadastroAdquirente()
            );
    
            if ($validate->fails()) {
                return ["error" => true];
            }
        }

        $adquirente = (object) [
            "nome" => convert_accentuation($request->razao_social),
            "cgccpf" => remove_character_document($request->document),
            "telef" => $request->telefone,
            "telcel" => $request->celular,
            "numcgm" => $request->cgm,
            "cep" => $request->cep,
            "ender" => convert_accentuation($request->endereco),
            "numero" => $request->numero,
            "compl" => (!empty($request->complemento) ? convert_accentuation($request->complemento) : $request->complemento),
            "bairro" => convert_accentuation($request->bairro),
            "munic" => convert_accentuation($request->municipio),
            "uf" => $request->estado,
            "email" => (!empty($request->email) ? convert_accentuation($request->email) : $request->email),
            "princ" => $request->adquirente_principal === "true" ? "Sim" : ""
        ];

        if (session()->has("adquirentes")) {

            foreach(session("adquirentes") as $dados) {

                if (in_array(remove_character_document($request->document), (array) $dados)) {
                    // caso exista o adquirente ja retorna
                    return ["exists" => true];
                }
            }
        }

        session()->push("adquirentes", $adquirente);

        return view("imovel.averbacao.adquirentes.tabela_adquirentes");
    }

    public function removerAdquirenteSessao($document)
    {
        $adquirentes = session()->get("adquirentes");

        if (!empty($adquirentes)) {

            foreach($adquirentes as $key => $adquirente) {

                if (in_array($document, (array) $adquirente)) {
                    unset($adquirentes[$key]);
                    session()->forget("adquirentes");
                    session()->put('adquirentes', $adquirentes);
                }
            }
        }

        return view("imovel.averbacao.adquirentes.tabela_adquirentes");
    }

    public function removerDocumentoAdquirenteSessao($documento)
    {
        $documentos = session()->get("documentos_adquirentes");

        if (!empty($documentos)) {

            foreach($documentos as $key => $documentoAdquirente) {

                if (in_array($documento, (array) $documentoAdquirente)) {
                    unset($documentos[$key]);
                    session()->forget("documentos_adquirentes");
                    session()->put('documentos_adquirentes', $documentos);
                }
            }
        }

        return view("imovel.averbacao.documentos.tabela_documentos");
    }

    /**
     * Validation regrasCadastroAdquirente
     * @return array
     */
    private function regrasCadastroAdquirente()
    {
        return [
            'razao_social' => 'required|string',
            'celular' => 'required|string',
            'email' => 'required|string',
            'cep' => 'required|string',
            'endereco' => 'required|string',
            'numero' => 'required|string',
            'complemento' => 'required|string',
            'bairro' => 'required|string',
            'municipio' => 'required|string',
            'estado' => 'required|string'
        ];
    }



    /*
    Buscar ITBI’s quitados referente ao imóvel informado
     */

    public function listaItbiImovel($matricula)
    {
        $data1 = [
            'sTokeValiadacao' => API_TOKEN,
            'sExec' => API_LISTA_ITBI,
            's_limit' => "0",
            'i_matricula_imovel' => $matricula,
        ];

        $json_data = json_encode($data1);
        $data_string = ['json' => $json_data];

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => API_URL . API_ITBI,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($data_string),
            CURLOPT_HTTPHEADER => array(
                'Content-Length: ' . strlen(http_build_query($data_string)),
            ),
        ));

        if (USAR_SSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
        }

        $responseItbi = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        return $responseItbi;
    }

    public function listaAdquirentes($matricula, $guia)
    {
        $data1 = [
            'sTokeValiadacao' => API_TOKEN,
            'sExec' => API_ADQUIRENTE_AVERBACAO,
            'i_codigo_guia' => $guia,
            'i_matricula_imovel' => $matricula,
        ];

        $json_data = json_encode($data1);
        $data_string = ['json' => $json_data];

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => API_URL . API_AVERBACAO,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($data_string),
            CURLOPT_HTTPHEADER => array(
                'Content-Length: ' . strlen(http_build_query($data_string)),
            ),
        ));

        if (USAR_SSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
        }

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        return $response;
    }

    public function verifica_status_lecom($matricula)
    {
        $cpf = Auth::user()->cpf;

        $averba = AverbacaoLecom::where('cpf', $cpf)
            ->where('matricula', $matricula)
            ->get();

        if ($averba->count()) {
            $serv = DB::connection('lecom')
                ->table('v_consulta_servico_sim')
                ->where('Chamado', '=', $averba[0]->cod_lecom)
                ->get();

            if ($serv[0]->FINALIZADO == 'Finalizado') {
                return (['status' => true]);
            } else {
                return (['status' => false]);
            }
        } else {
            return (['status' => true]);
        }
    }

    public function verifica_debitos($matricula)
    {
        $data1 = [
            'sTokeValiadacao' => API_TOKEN,
            "sExec" => "integracaoverificardebitosdividaimovel",
            "i_codigo_maticula" => $matricula,
        ];

        $json_data = json_encode($data1);
        $data_string = ['json' => $json_data];

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => API_URL . API_ITBI,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($data_string),
            CURLOPT_HTTPHEADER => array(
                'Content-Length: ' . strlen(http_build_query($data_string)),
            ),
        ));

        if (USAR_SSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
        }

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        return $response;
    }

    public function pendencias_gravar(request $request)
    {
        DB::beginTransaction();

        try {

            $anexos = json_decode($request["anexos"]);

            $check = DB::connection('mysql')
                ->table('averbacao_exigencias')
                ->where([
                    ['processo', '=', $request['processo_pendencias']],
                    ['etapa', '=', $request['etapa_pendencias']],
                    ['ciclo', '=', $request['ciclo_pendencias']],
                ])
                ->get();

            if ($check->count()) {
                $view = view('cidadao.falha_integracao', ['titulo' => 'AVERBAÇÃO DE IMÓVEIS', 'mensagem' => 'Esse processo de edição de pendências da averbação já foi respondido!'])->render();

                return response()->json(['html' => $view]);
            }

            foreach ($anexos as $key => $anx) {

                $anexos_lecom = DB::connection('lecom')

                    ->table('pmm_portal_arquivo')
                    ->insert([
                        'COD_PROCESSO' => $request['processo_pendencias'],
                        'COD_ETAPA' => $request['etapa_pendencias'],
                        'COD_CICLO' => $request['ciclo_pendencias'],
                        'TIPO' => $anx->descricao,
                        'DESCRICAO' => $anx->nome,
                    ]);
            }

            $data_explode = explode('/', $request['data_pendencias']);
            $data_pendencias = $data_explode[2] . '-' . $data_explode[1] . '-' . $data_explode[0];
            $dados_cumprir = DB::connection('lecom')
                ->table('pmm_cumprir_exigencia_averbar')
                ->insert([
                    'PROCESSO' => $request['processo_pendencias'],
                    'ETAPA' => $request['etapa_pendencias'],
                    'CICLO' => $request['ciclo_pendencias'],
                    'NU_GUIA_ITBI' => $request['guia_pendencias'],
                    'NU_REG_GERAL_IMOVEL' => $request['imovel_pendencias'],
                    'PROT_REG_IMOVEL' => $request['protocolo_pendencias'],
                    'DT_REG_IMOVEL' => $data_pendencias,
                ]);

            $dados_local = DB::connection('mysql')
                ->table('averbacao_exigencias')
                ->insert([
                    'processo' => $request['processo_pendencias'],
                    'etapa' => $request['etapa_pendencias'],
                    'ciclo' => $request['ciclo_pendencias'],
                    'data' => date('Y-m-d'),
                ]);

            if ($dados_cumprir && $dados_local) {
                DB::commit();
                $view = view('cidadao.sucesso_integracao', ['titulo' => 'AVERBAÇÃO DE IMÓVEIS', 'mensagem' => 'Solicitação encaminhada para análise. Acompanhe sua solicitação ' . $request['processo_pendencias'] . ' na aba consulta'])->render();

                return response()->json(['html' => $view]);
            } else {

                DB::rollBack();
                $view = view('cidadao.falha_integracao', ['titulo' => 'AVERBAÇÃO DE IMÓVEIS', 'mensagem' => 'Falha no processo de edição de pendências da averbação!'])->render();

                return response()->json(['html' => $view]);
            }
        } catch (\Throwable $th) {

            DB::rollBack();

            $view = view('cidadao.falha_integracao', ['titulo' => 'AVERBAÇÃO DE IMÓVEIS', 'mensagem' => 'Falha no processo de edição de pendências da averbação, tente novamente mais tarde!'])->render();

            return response()->json(['html' => $view]);
        }
    }

    public function gravarAverbacao(Request $request)
    {
        if (empty($request->dataRegistroImovelCartorio)) {
            return [
                'titulo' => 'AVERBAÇÃO DE IMÓVEIS',
                'mensagem' => 'A data de Registro do imóvel não pode ser vazio.',
                'reload' => false
            ];
        }

        if (empty($request->numeroRegistro)) {
            return [
                'titulo' => 'AVERBAÇÃO DE IMÓVEIS',
                'mensagem' => 'O número do Registro não pode ser vazio.',
                'reload' => false
            ];
        }

        if (empty($request->protocoloReg)) {
            return [
                'titulo' => 'AVERBAÇÃO DE IMÓVEIS',
                'mensagem' => 'O protocolo Reg não pode ser vazio.',
                'reload' => false
            ];
        }

        $adquirentes = session("adquirentes");
        $documentos_adquirentes = session("documentos_adquirentes");

        $dadosAdquirente = null;
        $dadosDocumentosAdquirentes = null;

        if (!empty($adquirentes)) {
            foreach($adquirentes as $adquirente) {
                $dadosAdquirente .= implode(";", (array) $adquirente);
            }
        }

        if (!empty($documentos_adquirentes)) {
            foreach($documentos_adquirentes as $documento) {
                $dadosDocumentosAdquirentes .= implode(";", (array) $documento);
            }
        }

        $cidadaoSupport = new CidadaoSupport();
        $cgm = $cidadaoSupport->getDadosCGM(remove_character_document(Auth::user()->cpf));

        if ($cgm->iStatus == 1) {

            $guia = "Cidadão";
            $assunto = "Imóvel";
            $servico = "Averbar Imóvel";

            $catalogoSIM = DB::connection('lecom')
                ->table('v_catalogo_sim')
                ->where('guia', '=', $guia)
                ->where('assunto', '=', $assunto)
                ->where('servico', '=', $servico)
                ->get()[0];

            $ticketSSO = ((array) json_decode($this->ticketSSO()))['ticket-sso'];

            $processoCatalogoSIM = $catalogoSIM->processo;
            $versaoCatalagoSIM = $catalogoSIM->versao;
            $dadosJsonCatalogoSIM = $catalogoSIM->json;

            $dadosJsonCatalogoSIM = str_replace(
                [
                    "[nome]",
                    "[cpf]",
                    "[telefone]",
                    "[celular]",
                    "[email]",
                    "[itbi]",
                    "[dadosadquirente]",
                    "[rgi]",
                    "[matimovel]",
                    "protimovel]",
                    "[transacao]",
                    "[data_reg_imovel]",
                    "[correspondecia]",
                    "[anexos]"
                ],
                [
                    Auth::user()->name,
                    Auth::user()->cpf,
                    Auth::user()->celphone,
                    Auth::user()->celphone,
                    Auth::user()->email,
                    $request->guiaItbi,
                    $dadosAdquirente,
                    $request->numeroRegistro,
                    $request->matricula,
                    $request->protocoloReg,
                    $request->tipoSolicitacao . ' - ' . $this->converteTipoSolicacao($request->tipoSolicitacao),
                    $request->dataRegistroImovelCartorio,
                    "",
                    $dadosDocumentosAdquirentes
                ],
                $dadosJsonCatalogoSIM
            );

            $url = str_replace(
                [":processo", ":versao"],
                [$processoCatalogoSIM, $versaoCatalagoSIM],
                SISTEMACAMINHOBASE.CAMINHO
            );

            return $this->envioAverbacaoLecom(
                $url, 
                $dadosJsonCatalogoSIM, 
                $ticketSSO, 
                $request->matricula
            );

        } else {

            return [
                'titulo' => 'AVERBAÇÃO DE IMÓVEIS',
                'mensagem' => "<p><strong>Atenção!</strong>
                    Não foi possível recuperar as informações.</p>
                    <p>Tente Novamente em alguns instantes.</p>
                    Código: INT_1001",
                'reload' => false
            ];
        }
    }

    private function envioAverbacaoLecom($url, $dadosJsonCatalogoSIM, $ticketSSO, $matricula)
    {
        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $dadosJsonCatalogoSIM,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "language: pt_BR",
                "ticket-sso: $ticketSSO",
            )
        ));

        if (USAR_SSL) {
            curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_LECOM);
        }

        $response = curl_exec($ch);
        curl_close($ch);

        $retorno = [
            'ticket' => $ticketSSO,
            'resposta' => json_decode($response)
        ];

        if (isset($retorno['resposta']->content->processInstanceId)) {

            DB::table('averbacao_lecom')->insert([
                'cpf'               => Auth::user()->cpf,
                'cod_lecom'         => $retorno['resposta']->content->processInstanceId,
                'matricula'         => $matricula,
                'data_solicitacao'  => date("Y-m-d H:i:s"),
            ]);

            return [
                'titulo' => 'AVERBAÇÃO DE IMÓVEIS',
                'mensagem' => 'Solicitação encaminhada para análise.
                    Acompanhe sua solicitação ' . $retorno['resposta']->content->processInstanceId . ' na aba consulta.',
                'reload' => true
            ];

        } else {
            return [
                'titulo' => 'AVERBAÇÃO DE IMÓVEIS',
                'mensagem' => 'Falha no processo de averbação!',
                'reload' => false
            ];
        }
    }

    private function getStats()
    {

        $stats = [

            ['value' => 'ACRE', 'key' => 'AC'],

            ['value' => 'ALAGOAS', 'key' => 'AL'],

            ['value' => 'AMAPÁ', 'key' => 'AP'],

            ['value' => 'AMAZONAS', 'key' => 'AM'],

            ['value' => 'BAHIA', 'key' => 'BA'],

            ['value' => 'CEARÁ', 'key' => 'CE'],

            ['value' => 'DISTRITO FEDERAL', 'key' => 'DF'],

            ['value' => 'ESPÍRITO SANTO', 'key' => 'ES'],

            ['value' => 'GOIÁS', 'key' => 'GO'],

            ['value' => 'MARANHÃO', 'key' => 'MA'],

            ['value' => 'MATO GROSSO', 'key' => 'MT'],

            ['value' => 'MATO GROSSO DO SUL', 'key' => 'MS'],

            ['value' => 'MINAS GERAIS', 'key' => 'MG'],

            ['value' => 'PARÁ', 'key' => 'PA'],

            ['value' => 'PARAÍBA', 'key' => 'PB'],

            ['value' => 'PERNAMBUCO', 'key' => 'PE'],

            ['value' => 'PIAUÍ', 'key' => 'PI'],

            ['value' => 'RIO DE JANEIRO', 'key' => 'RJ'],

            ['value' => 'RIO GRANDE DO NORTE', 'key' => 'RN'],

            ['value' => 'RIO GRANDE DO SUL', 'key' => 'RS'],

            ['value' => 'RONDÔNIA', 'key' => 'RO'],

            ['value' => 'RORAIMA', 'key' => 'RR'],

            ['value' => 'SANTA CATARINA', 'key' => 'SC'],

            ['value' => 'SÃO PAULO', 'key' => 'SP'],

            ['value' => 'SERGIPE', 'key' => 'SE'],

            ['value' => 'TOCANTINS', 'key' => 'TO'],

        ];

        return $stats;
    }

    private function ticketSSO()
    {

        $data1 = [

            'user' => USUARIO,

            'pass' => SENHA,

        ];

        $data_string = json_encode($data1);

        $ch = curl_init();

        curl_setopt_array($ch, array(

            CURLOPT_URL => SISTEMACAMINHOBASE . "/" . CAMINHORELATIVO,

            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_TIMEOUT => 30000,

            CURLOPT_CUSTOMREQUEST => "POST",

            CURLOPT_POSTFIELDS => $data_string,

            CURLOPT_HTTPHEADER => array(

                "content-type: application/json",

                'Content-Length: ' . strlen($data_string),

            ),

        ));

        if (USAR_SSL) {

            curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_LECOM);
        }

        $response = curl_exec($ch);

        $err = curl_error($ch);

        curl_close($ch);

        return $response;
    }

    public function salvarDocumentoAverbacao(Request $request)
    {
        $extensoesPermitidas = ["jpeg", "png", "jpg", "gif", "pdf"];
        $extensao = $request->arquivo->extension();

        if (!in_array($extensao, $extensoesPermitidas)) {
            return ['error' => 'O arquivo deve ser uma imagem ou um pdf com as seguintes extensões (jpeg, png, jpg, gif, pdf).'];
        }

        $tamanhoArquivo = $request->arquivo->getClientSize();

        if ($tamanhoArquivo > 2048000) {
            return ['error' => 'O arquivo deve ter no máximo 2MB.'];
        }

        $cpf = remove_character_document(Auth::user()->cpf);

        $descricao = $request->descricao != "Outros" ? $request->descricao : $request->outro_anexo;

        $nomeNovoArquivo =  $cpf . "_" . date('Y-m-d H:i:s') . "_" . $descricao . "." . $extensao;
        $nomeNovoArquivo = str_replace(" ", "_", $nomeNovoArquivo);

        $upload = $request->arquivo->storeAs('documentos', $nomeNovoArquivo, 'ftp');

        if ($upload) {

            session()->push("documentos_adquirentes", (object) [
                "descricao" => $descricao,
                "nome_original" => $request->arquivo->getClientOriginalName(),
                "nome_novo" => $nomeNovoArquivo
            ]);

            return view('imovel.averbacao.documentos.tabela_documentos');
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function averbacao_documentos_excluir($id)
    {

        $resultado = DB::table('averbacao_documentos')

            ->where('id', '=', $id)

            ->where('cpf', '=', Auth::user()->cpf)

            ->get();

        if (count($resultado) > 0) {

            $retorno = Storage::disk('ftp')->delete('documentos/' . $resultado[0]->nome_documento);

            $resultadox = DB::table('averbacao_documentos')

                ->where('id', '=', $id)

                ->where('cpf', '=', Auth::user()->cpf)

                ->delete();
        }

        return back()->with('status', 1);
    }



    public function getDadosCGM($cpf, $tipo = "P")
    {
        try {
            $cidadaoSupport = new CidadaoSupport();
            
            $document = remove_character_document($cpf);

            $dadosPessoaisCGM = $cidadaoSupport->getDadosCGM($document);
            $enderecoCGM = $cidadaoSupport->getEnderecoCGM($document, $tipo);

            $existeAdquirentePrincipal = false;

            if (!empty(session("adquirentes"))) {

                foreach(session("adquirentes") as $adquirente) {
                    if ($adquirente->princ === "Sim") {
                        $existeAdquirentePrincipal = true;
                        break;
                    }
                }
            }

            $dadosCGM = [
                "nome" => convert_accentuation($dadosPessoaisCGM->aCgmPessoais->z01_nome),
                "cpf" => $dadosPessoaisCGM->aCgmPessoais->z01_cgccpf,
                "telefone" => $dadosPessoaisCGM->aCgmContato->z01_telef,
                "celular" => $dadosPessoaisCGM->aCgmContato->z01_telcel,
                "cgm" => $dadosPessoaisCGM->cgm,
                "cep" => $enderecoCGM->endereco->iCep,
                "endereco" => convert_accentuation($enderecoCGM->endereco->sRua),
                "numero" => $enderecoCGM->endereco->sNumeroLocal,
                "complemento" => convert_accentuation($enderecoCGM->endereco->sComplemento),
                "bairro" => convert_accentuation($enderecoCGM->endereco->sBairro),
                "municipio" => convert_accentuation($enderecoCGM->endereco->sMunicipio),
                "estado" => convert_accentuation($enderecoCGM->endereco->sUf),
                "email" => convert_accentuation($dadosPessoaisCGM->aCgmContato->z01_email),
                "error" => false
            ];

            return [
                'cgm'   => $dadosCGM,
                'exists' => true,
                'adquirente_principal' => $existeAdquirentePrincipal
            ];

        } catch (\Exception $e) {

            return [
                'exists'  => false,
                'adquirente_principal' => $existeAdquirentePrincipal
            ];
        }
    }

    public function recuperaCGM($cpf)
    {

        try {

            $params = [".", "-", "/", " "];

            $cpf = str_replace($params, "", $cpf);

            // Recuperando dados do CGM...

            $data1 = [

                'sTokeValiadacao' => API_TOKEN,

                'sExec' => API_CONSULTA_CGM,

                'z01_cgccpf' => $cpf,

            ];

            $json_data = json_encode($data1);

            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(

                CURLOPT_URL => API_URL . API_CGM,

                CURLOPT_RETURNTRANSFER => true,

                CURLOPT_TIMEOUT => 30000,

                CURLOPT_CUSTOMREQUEST => "POST",

                CURLOPT_POSTFIELDS => http_build_query($data_string),

                CURLOPT_HTTPHEADER => array(

                    'Content-Length: ' . strlen(http_build_query($data_string)),

                ),

            ));

            if (USAR_SSL) {

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);

                curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
            }

            $response = curl_exec($ch);

            $err = curl_error($ch);

            curl_close($ch);

            $userCGM = json_decode($response);
            $retorno = [

                'error' => false,

                'cgm' => $userCGM,

            ];

            return $retorno;
        } catch (\Exception $e) {

            $retorno = [

                'error' => true,

            ];

            return $retorno;
        }
    }

    public function recuperaEnderecoCGM($cpf, $tipo = "P")
    {

        try {

            $params = [".", "-", "/", " "];

            $cpf = str_replace($params, "", $cpf);

            // Recuperando dados do CGM...

            $data1 = [

                'sTokeValiadacao' => API_TOKEN,

                'sExec' => API_CONSULTA_ENDERECO_CGM,

                'sCpfcnpj' => $cpf,

                'sTipoEndereco' => $tipo,

            ];

            $json_data = json_encode($data1);

            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(

                CURLOPT_URL => API_URL . API_CGM,

                CURLOPT_RETURNTRANSFER => true,

                CURLOPT_TIMEOUT => 30000,

                CURLOPT_CUSTOMREQUEST => "POST",

                CURLOPT_POSTFIELDS => http_build_query($data_string),

                CURLOPT_HTTPHEADER => array(

                    'Content-Length: ' . strlen(http_build_query($data_string)),

                ),

            ));

            if (USAR_SSL) {

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);

                curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
            }

            $response = curl_exec($ch);

            $err = curl_error($ch);

            curl_close($ch);

            $enderecoCGM = json_decode($response);

            $retorno = [

                'error' => false,

                'endereco' => $enderecoCGM,

            ];

            return $retorno;
        } catch (\Exception $e) {

            $retorno = [

                'error' => true,

            ];

            return $retorno;
        }
    }

    public function recupera_adquirentes_imovel($guia)
    {

        try {

            // Recuperando dados dos adquirentes de um imóvel...

            $data1 = [

                'sTokeValiadacao' => API_TOKEN,

                'sExec' => API_LISTA_ADQUIRENTES_ITBI,

                'i_codigo_guia' => $guia,

                's_tipo' => "C",

            ];

            $json_data = json_encode($data1);

            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(

                CURLOPT_URL => API_URL . API_AVERBACAO,

                CURLOPT_RETURNTRANSFER => true,

                CURLOPT_TIMEOUT => 30000,

                CURLOPT_CUSTOMREQUEST => "POST",

                CURLOPT_POSTFIELDS => http_build_query($data_string),

                CURLOPT_HTTPHEADER => array(

                    'Content-Length: ' . strlen(http_build_query($data_string)),

                ),

            ));

            if (USAR_SSL) {

                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);

                curl_setopt($ch, CURLOPT_CAINFO, getcwd() . '/cert/' . SSL_API);
            }

            $response = curl_exec($ch);

            $err = curl_error($ch);

            curl_close($ch);

            return $response;
        } catch (\Exception $e) {

            $retorno = [

                'error' => true,

            ];

            return $retorno;
        }
    }

    public function converteTipoSolicacao($tipo)
    {
        switch ($tipo) {
            case '2':
                return "ESCRITURA";
            case '3':
                return "REGISTRO DE IMOVEIS";
            case '4':
                return "CONTRATO";
            case '9':
                return "MIGRACAO";
        }
    }

    public function converteEstadoSigla($estado)
    {

        $estado = utf8_encode(urldecode(iconv('UTF-8', 'ISO-8859-1', $estado)));

        $estado = strtoupper($estado);

        switch ($estado) {

            case 'ACRE':

                return "AC";

                break;

            case 'ALAGOAS':

                return "AL";

                break;

            case 'AMAPÁ':

                return "AP";

                break;

            case 'AMAZONAS':

                return "AM";

                break;

            case 'BAHIA':

                return "BA";

                break;

            case 'CEARÁ':

                return "CE";

                break;

            case 'DISTRITO FEDERAL':

                return "DF";

                break;

            case 'ESPÍRITO SANTO':

                return "ES";

                break;

            case 'GOIÁS':

                return "GO";

                break;

            case 'MARANHÃO':

                return "MA";

                break;

            case 'MATO GROSSO':

                return "MT";

                break;

            case 'MATO GROSSO DO SUL':

                return "MS";

                break;

            case 'MINAS GERAIS':

                return "MG";

                break;

            case 'PARÁ':

                return "PA";

                break;

            case 'PARAÍBA':

                return "PB";

                break;

            case 'PERNAMBUCO':

                return "PE";

                break;

            case 'PIAUÍ':

                return "PI";

                break;

            case 'RIO DE JANEIRO':

                return "RJ";

                break;

            case 'RIO GRANDE DO NORTE':

                return "RN";

                break;

            case 'RIO GRANDE DO SUL':

                return "RS";

                break;

            case 'RONDÔNIA':

                return "RO";

                break;

            case 'RORAIMA':

                return "RR";

                break;

            case 'SANTA CATARINA':

                return "SC";

                break;

            case 'SÃO PAULO':

                return "SP";

                break;

            case 'SERGIPE':

                return "SE";

                break;

            case 'TOCANTINS':

                return "TO";

                break;
        }
    }
}
