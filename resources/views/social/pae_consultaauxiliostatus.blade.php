@extends('layouts.tema01')

@section('content')
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span8">
                <div class="inner-heading">
                    <h2>CONSULTA DE BENEFÍCIO</h2>
                </div>
            </div>
            <div class="span4">
                <ul class="breadcrumb">
                    <li><a href="{{  url('/') }}"><i class="fa fa-home"></i></a><i class="fa fa-angle-right"></i></li>
                    <li class="active">CONSULTA DE BENEFÍCIO</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="span12">

                <div class="row registro-fase-topo">
                    <div class="span2">
                        Chamado: <b>{{ $processo->Chamado }}</b>
                    </div>
                    <div class="span2">
                        Data: <b>{{ date('d/m/Y', strtotime($processo->abertura)) }}</b>
                    </div>
                    <div class="span4">
                        Serviço: <b>{{ $processo->Serviço }}</b>
                    </div>
                    <div class="span4">
                        Responsável: <b>{{ $processo->responsavel_consulta }}</b><br>
                    </div>
                </div>
                <div class="justfy-content-center registro-fase">
                    <center>
                        @switch($processo->Fase)
                            @case('Abertura de Solicitação')
                                <img src="{{ asset('img/fases_pat_01.png') }}" alt="Fase" class="fase" />
                                @break
                            @case('Solicitação em Análise')
                                <img src="{{ asset('img/fases_pat_02.png') }}" alt="Fase" class="fase" />
                                @break
                            @case('Benefício Concedido')
                                <img src="{{ asset('img/fases_pat_03.png') }}" alt="Fase" class="fase" />
                                @break
                            @case('Solicitação Recusada')
                                <img src="{{ asset('img/fases_pat_04.png') }}" alt="Fase" class="fase" />
                                @break
                            @case('Solicitação Não Aprovada')
                                <img src="{{ asset('img/fases_pat_05.png') }}" alt="Fase" class="fase" />
                                @break
                        @endswitch
                    </center>
                </div>
                <div class="row registro-fase-topo">
                    <div class="span2 text-md-right">
                        Nome do Solicitante:
                    </div>
                    <div class="span5 text-md-left">
                        <strong>{{$usuario->razaosocial}}</strong>
                    </div>
                </div>
                <div class="row registro-fase-topo">
                    <div class="span2 text-md-right">
                        Data da Solicitação:
                    </div>
                    <div class="span5 text-md-left">
                        <strong>{{date('d/m/Y H:i:s', strtotime($usuario->DATA_CADASTRO))}}</strong>
                    </div>
                </div>
                <div class="row registro-fase-topo">
                    <div class="span2 text-md-right">
                        CNPJ da Empresa:
                    </div>
                    <div class="span5 text-md-left">
                        <strong>{{$usuario->cnpj}}</strong>
                    </div>
                </div>
                <div class="row registro-fase-topo">
                    <div class="span2 text-md-right">
                        Protocolo:
                    </div>
                    <div class="span5 text-md-left">
                        <strong>{{$usuario->PROTOCOLO}}</strong>
                    </div>
                </div>
                @if($processo->Fase == 'Solicitação Não Aprovada' || $processo->Fase == 'Solicitação Recusada' || $processo->Etapa == 31)
                <div class="row registro-fase-topo">
                    <div class="span2 text-md-right">
                        Motivo:
                    </div>
                    <div class="span5 text-md-left">
                        <strong>{{$processo->motivo}}</strong>
                    </div>
                </div>
                <br>
                <br>
                @endif
                @if( $processo->Etapa == 31 )
                    <h3>Pendência Prestação de Contas:</h3>

                    @if( isset($recursoPC->cnpj) )
                        <div class="alert alert-info">
                            A sua documentação foi enviada em {{date('d/m/Y H:i:s', strtotime($recursoPC->data))}}
                        </div>
                    @else
                        <form method="POST" action="{{ route('pae_recurso_pc') }}" id="pae_recurso_pc" name="pae_recurso_pc">
                            @csrf
                            <b>Justifique a pendência: <span class="required">*</span></b><br>
                            <input id="cnpjPC" type="hidden" name="cnpj" value="{{ $usuario->cnpj }}">
                            <input id="processoPC" type="hidden" name="processo" value="{{ $processo->Chamado }}">
                            <input id="documentosPC" type="hidden" name="documentos" value="">
                            <input id="id_pae" type="hidden" name="id_pae" value="{{ $usuario->id }}">
                            <textarea required class="form-input" id="txtRecursoPC" name="txtRecursoPC" rows="6" data-rule="required" data-msg="" placeholder=""></textarea>

                            <br>
                            <br>

                            <form method="POST">
                                @csrf
                            </form>
                            <form method="POST" action="" id="formComprovanteRecursoPC" name="formComprovanteRecursoPC" enctype="multipart/form-data">
                                @csrf
                                <div class="row formrow">
                                    <div class="controls span3 form-label">
                                        <label for="DescrDoc" class="col-md-4 col-form-label text-md-right">
                                                {{ __('CPF do Funcionário') }} <span class="required">*</span>
                                        </label>
                                    </div>
                            
                                    <div class="span3">
                                        <input maxlength="20" id="pae_cpf" type="text" class="form-input" name="pae_cpf" required>
                                    </div>
                                </div>                                
                                <div class="row formrow">
                                    <div class="controls span3 form-label">
                                        <label for="DescrDoc" class="col-md-4 col-form-label text-md-right">
                                                {{ __('Tipo de Documento') }} <span class="required">*</span>
                                        </label>
                                    </div>
                            
                                    <div class="span7">
                                        <select name="DescrDocPC" id="DescrDocPC" style="width:100%;">
                                            <option value=""></option>
                                            <option value="SEFIP da Empresa">SEFIP da Empresa</option>
                                            <option value="Termo de Adesão">Termo de Adesão</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row formrow">
                                    <div class="controls span3 form-label">
                                        <label for="documento" class="col-md-4 col-form-label text-md-right">
                                            {{ __('Anexar Arquivos') }} 
                                        </label>
                                    </div>
                        
                                    <div class="span6">
                                        <input type="hidden" name="tipoDoc" value="ComprovanteRecurso" />
                                        <input type="hidden" name="idCpf" id="idCompRecursoPC" value="{{ $usuario->cnpj }}" />
                                        <input type="file" name="arquivo" id="arquivoCompRecursoPC" /><br>
                                        Obs I: O tamanho máximo dos arquivos é de 2Mb.<br>
                                    </div>
                                    <div class="span2">
                                        <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCompRecursoPC">
                                            {{ __('Lançar') }}
                                        </button>
                                    </div>
                                </div>
                            </form>


                            <div class="row formrow">
                                <div class="span11">
                                    <h4 class="heading">Documentos Lançados<span></span></h4>
                        
                                    <table id="documentosComprovanteRecursoPC" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>CPF</th>
                                                <th>Descrição</th>
                                                <th>Nome Original</th>
                                                <th>Nome Final</th>
                                                <th width='20%'>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>     
                            <br>     
                            
                            <div class="row formrow">
                                <div class="span11">
                                    <button type="button" class="btn btn-small btn-blue e_wiggle align-right" id="solicitarRecursoPrestacaoContas">
                                        {{ __('Sanar Pendências') }}
                                    </button>
                                </div>
                            </div>
                           
                        </form>
                    @endif


                @elseif(($processo->Fase == 'Solicitação Recusada'|| $processo->Fase == 'Solicitação Não Aprovada') && ($processo->FINALIZADO == 'andamento' || $processo->FINALIZADO == 'reprovado') )
                    <h3>Recurso:</h3>


                    @if( isset($recurso->cnpj) )
                        <div class="alert alert-info">
                            Já foi efetuada uma solicitação de recurso em {{date('d/m/Y H:i:s', strtotime($recurso->data))}}
                        </div>
                    @else
                        <form method="POST" action="{{ route('pae_recurso') }}" id="pae_recurso" name="pae_recurso">
                            @csrf
                            <b>Justifique o motivo do recurso: <span class="required">*</span></b><br>
                            <input id="cnpj" type="hidden" name="cnpj" value="{{ $usuario->cnpj }}">
                            <input id="processo" type="hidden" name="processo" value="{{ $processo->Chamado }}">
                            <input id="documentos" type="hidden" name="documentos" value="">
                            <input id="id_pae" type="hidden" name="id_pae" value="{{ $usuario->id }}">
                            <textarea required class="form-input" id="txtRecurso" name="txtRecurso" rows="6" data-rule="required" data-msg="" placeholder=""></textarea>

                            <br>
                            <br>
                            <form method="POST">
                                @csrf
                            </form>
                            <form method="POST" action="" id="formComprovanteRecurso" name="formComprovanteRecurso" enctype="multipart/form-data">
                                @csrf
                                <div class="row formrow">
                                    <div class="controls span3 form-label">
                                        <label for="DescrDoc" class="col-md-4 col-form-label text-md-right">
                                                {{ __('Tipo de Documento') }} <span class="required">*</span>
                                        </label>
                                    </div>
                            
                                    <div class="span7">
                                        <select name="DescrDoc" id="DescrDoc" style="width:100%;">
                                            <option value=""></option>
                                            <option value="Comprovação e/ou solicitação de Inscrição Municipal">Comprovação e/ou solicitação de Inscrição Municipal </option>
                                            <option value="Demonstrativo de Folha Salarial (Abril/2020)">Demonstrativo de Folha Salarial (Abril/2020)</option>
                                            <option value="Contrato Social">Contrato Social</option>
                                            <option value="Cartão de CNPJ">Cartão de CNPJ</option>
                                            <option value="Certidão de débitos com o Município">Certidão de débitos com o Município</option>
                                            <option value="Documento de Identidade do(s) Sócio(s)">Documento de Identidade do(s) Sócio(s)</option>
                                            <option value="CPF do(s) Sócio(s)">CPF do(s) Sócio(s)</option>
                                            <option value="CPF do Representante">CPF do Representante</option>
                                            <option value="Comprovante de Residência">Comprovante de Residência</option>
                                            <option value="Representação Legal">Representação Legal</option>
                                            <option value="Comprovante de Conta">Comprovante de Conta</option>
                                            <option value="Acordo de Redução Salarial">Acordo de Redução Salarial</option>
                                            <option value="SEFIP da Empresa">SEFIP da Empresa</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row formrow">
                                    <div class="controls span3 form-label">
                                        <label for="documento" class="col-md-4 col-form-label text-md-right">
                                            {{ __('Anexar Arquivos') }} 
                                        </label>
                                    </div>
                        
                                    <div class="span6">
                                        <input type="hidden" name="tipoDoc" value="ComprovanteRecurso" />
                                        <input type="hidden" name="idCpf" id="idCompRecurso" value="{{ $usuario->cnpj }}" />
                                        <input type="file" name="arquivo" id="arquivoCompRecurso" /><br>
                                        Obs I: O tamanho máximo dos arquivos é de 2Mb.<br>
                                    </div>
                                    <div class="span2">
                                        <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCompRecurso">
                                            {{ __('Lançar') }}
                                        </button>
                                    </div>
                                </div>
                            </form>


                            <div class="row formrow">
                                <div class="span11">
                                    <h4 class="heading">Documentos Lançados<span></span></h4>
                        
                                    <table id="documentosComprovanteRecurso" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Descrição</th>
                                                <th>Nome Original</th>
                                                <th>Nome Final</th>
                                                <th width='20%'>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>     
                            <br>     
                            
                            <div class="row formrow">
                                <div class="span11">
                                    <button type="button" class="btn btn-small btn-blue e_wiggle align-right" id="solicitarRecurso">
                                        {{ __('Solicitar Recurso') }}
                                    </button>
                                </div>
                            </div>
                           
                        </form>
                    @endif
                @endif

                @if($processo->Fase == 'Benefício Concedido' && $processo->Etapa != 31)
                    <h3>Dados do Banco:</h3>
                    <div class="row registro-fase-topo">
                        <div class="span2 text-md-right">
                            Banco:
                        </div>
                        <div class="span5 text-md-left">
                            <strong>{{$banco->banco}}</strong>
                        </div>
                    </div>
                    <div class="row registro-fase-topo">
                        <div class="span2 text-md-right">
                            Agência:
                        </div>
                        <div class="span5 text-md-left">
                            <strong>{{$banco->agencia}}</strong>
                        </div>
                    </div>
                    <div class="row registro-fase-topo">
                        <div class="span2 text-md-right">
                            Conta:
                        </div>
                        <div class="span5 text-md-left">
                            <strong>{{$banco->conta}}</strong>
                        </div>
                    </div>
                    {{-- <div class="row registro-fase-topo">
                        <div class="span2 text-md-right">
                            Senha de 1º acesso:
                        </div>
                        <div class="span8 text-md-left">
                            <div class="alert alert-info">

                                <p>Sua senha de primeiro acesso foi enviada para o e-mail cadastrado.</p>
                                <p>Ao realizar seu primeiro acesso e troca da senha, você concorda com a auto declaração abaixo:</p>
                                <p>Declaro e ratifico para os devidos fins, em conformidade com o Art. 2º, § 1º, XI, "alínea f" da Lei 2920/20, modificada pela Lei Municipal  2922/20, que todos os documentos probatórios apresentados, previstos nas alíneas "a","b","c","d","e", no ato da inscrição, correspondem as provas da atividade econômica por mim declarada e que foi afetada a partir da publicação do Decreto Municipal nº 499 de 18 de março de 2020, o qual veio a declarar, dentre outras medidas, o estado de emergência em saúde pública no Município de Maricá.</p>
                                <p><b>ATENÇÃO</b></p>
                                <p>Importante que você baixe o Aplicativo "e-dinheiro" na Play Store ou IOS. Informações de como baixar o Aplicativo, veja o vídeo explicativo em: https://institutoedinheiromarica.org.</p>
                                <p>Ao baixar o aplicativo, altere de imediato sua Senha. Você só terá acesso ao benefício quando efetivar a troca de senha. Veja o vídeo explicativo de como trocar a senha em:
                                    <a href="https:\\institutoedinheiromarica.org" target="_BLANK">https:\\institutoedinheiromarica.org</a></p>
                                <p>Em caso dúvidas, você pode entrar em contato com o Banco Mumbuca nos seguintes telefones: 3731-1021 / 97285-6635 / 96726-3882 / 3731-6550.</p>
                            </div>
                        </div>
                    </div> --}}
                @endif
            </div>
        </div>
        <div class="row">
            <div class="span3">
                <a href="{{ route('consultarsituacaopae' )  }}">
                    <button type="button" class="btn btn-theme e_wiggle" id="voltar">
                        {{ __('Voltar') }}
                    </button>
                    <a>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
</section>
@endsection

@section('post-script')

<script type="text/javascript">

    $("#pae_cpf").mask('000.000.000-00');

    var documentosComprovanteRecursoPC = $('#documentosComprovanteRecursoPC').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 5,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 230,
        "info"          : false,
    });

    var documentosComprovanteRecurso = $('#documentosComprovanteRecurso').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 5,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 230,
        "info"          : false,
    });

    $("#lancarArquivoCompRecursoPC").click(function () {
        if ($("#DescrDocPC").val() != "Termo de Adesão") {
            if ($("#pae_cpf").val() == "") {
                alert("Por favor informe o CPF do Funcionário");
                $("#pae_cpf").focus();
                return false;
            }

            if (!validarCPF($("#pae_cpf").val()) ) {
                alert("CPF Inválido!");
                $("#pae_cpf").focus();
                return false;
            }
        }

        
        if ($("#DescrDocPC").val() == "") {
            alert("Por favor informe que TIPO DE DOCUMENTO irá enviar");
            $("#DescrDocPC").focus();
            return false;
        }

        if ($("#arquivoCompRecursoPC").val() == "") {
            alert("Por favor escolha o DOCUMENTO para enviar");
            $("#arquivoCompRecursoPC").focus();
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCompRecursoPC');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formComprovanteRecursoPC']")[0]);
        var $descricao = dados.get("DescrDocPC");
        var $cpf = dados.get("pae_cpf");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos_recurso_pc') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosComprovanteRecursoPC.row.add([
                    $cpf,
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosComprovanteRecursoPC.columns.adjust().draw();
                $("#arquivoCompRecursoPC").val("");
                $("#DescrDocPC").val("");
                $("#pae_cpf").val("");
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoCompRecurso").click(function () {
        if ($("#DescrDoc").val() == "") {
            alert("Por favor informe que TIPO DE DOCUMENTO irá enviar");
            $("#DescrDoc").focus();
            return false;
        }

        if ($("#arquivoCompRecurso").val() == "") {
            alert("Por favor escolha o DOCUMENTO para enviar");
            $("#arquivoCompRecurso").focus();
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCompRecurso');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formComprovanteRecurso']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosComprovanteRecurso.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosComprovanteRecurso.columns.adjust().draw();
                $("#arquivoCompRecurso").val("");
                $("#DescrDoc").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $('#documentosComprovanteRecurso').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = documentosComprovanteRecurso.row($(this).parents('tr')).data()[0];
        var nomeArquivo = documentosComprovanteRecurso.row($(this).parents('tr')).data()[2];
        documentosComprovanteRecurso.row($(this).parents('tr')).remove().draw(false);

        $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivo, function (resposta) {

            $("#mdlAguarde").modal('toggle');

        })
    });

    $('#documentosComprovanteRecursoPC').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = documentosComprovanteRecursoPC.row($(this).parents('tr')).data()[0];
        var nomeArquivo = documentosComprovanteRecursoPC.row($(this).parents('tr')).data()[2];
        documentosComprovanteRecursoPC.row($(this).parents('tr')).remove().draw(false);

        $.get("{{ url('/pae_documentos_PC_remove') }}/"  + nomeArquivo, function (resposta) {

            $("#mdlAguarde").modal('toggle');

        })
    });

    
    $("#solicitarRecurso").click(function(){

        if($("#txtRecurso").val()==""){
            alert("or favor preencha as informações para sanar pendência");
            $("#txtRecurso").focus();
            return false; 
        }

        var $documentos = "";
        documentosComprovanteRecurso.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($documentos.length > 0 ){
                $documentos += "|"
            }
            $documentos += JSON.stringify(this.data());
        });
        $("#documentos").val($documentos);

        $("#pae_recurso").submit();
    });

    
    $("#solicitarRecursoPrestacaoContas").click(function(){

        if($("#txtRecursoPC").val()==""){
            alert("Por favor preencha as informações para sanar pendência");
            $("#txtRecursoPC").focus();
            return false; 
        }

        var $documentos = "";
        documentosComprovanteRecursoPC.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($documentos.length > 0 ){
                $documentos += "|"
            }
            $documentos += JSON.stringify(this.data());
        });
        $("#documentosPC").val($documentos);

        $("#pae_recurso_pc").submit();
    });

    function validarCPF(cpf) {
        var add;
        var rev;
        cpf = cpf.replace(/[^\d]+/g, '');
        if (cpf == '') return false;
        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return false;
        // Valida 1o digito	
        add = 0;
        for (var i = 0; i < 9; i++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito	
        add = 0;
        for (i = 0; i < 10; i++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }

</script>

@endsection