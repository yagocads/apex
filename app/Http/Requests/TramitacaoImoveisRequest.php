<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TramitacaoImoveisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Dados do Requerente
            'nome_requerente' => 'required',
            'endereco_requerente' => 'required',
            'cpf_cnpj_requerente' => 'required',
            'email_requerente' => 'email|required',
            'tipo_requerente' => 'required',

            // Dados Processo
            'local_abertura' => 'required',
            'requisitante' => 'required',
            'secretaria_requisitante' => 'required',
            'enviar_processo_para' => 'required',
            'despacho' => 'required',
            'categoria_processo' => 'required',
            'objeto_processo' => 'required',

            // Dados Imóvel
            'matricula_imovel' => 'required',
            'setor_imovel' => 'required',
            'quadra_imovel' => 'required',
            'lote_imovel' => 'required'
        ];
    }

    public function messages()
    {  
        return [
            // Dados do Requerente
            'nome_requerente.required' => 'Preencha o campo Nome (Dados do Requerente)',
            'endereco_requerente.required' => 'Preencha o campo Endereço (Dados do Requerente)',
            'cpf_cnpj_requerente.required' => 'Preencha o campo CPF/CNPJ (Dados do Requerente)',
            'email_requerente.required' => 'Preencha o campo E-mail (Dados do Requerente)',
            'tipo_requerente.required' => 'Preencha o campo Tipo de Requerente (Dados do Requerente)',

            // Dados do Processo
            'categoria_processo.required' => 'Preencha o campo Categoria Processo (Dados do Processo)',
            'objeto_processo.required' => 'Preencha o campo Objeto (Dados do Processo)',

            // Dados Imóvel
            'matricula_imovel.required' => 'Preencha o campo Matrícula do Imóvel (Dados do Processo)',
            'setor_imovel.required' => 'Preencha o campo Setor do Imóvel (Dados do Processo)',
            'quadra_imovel.required' => 'Preencha o campo Quadra do Imóvel (Dados do Processo)',
            'lote_imovel.required' => 'Preencha o campo Lote do Imóvel (Dados do Processo)',
        ];
    }
}
