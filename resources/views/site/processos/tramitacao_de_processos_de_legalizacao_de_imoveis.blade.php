@if($processo->Serviço == 'Tramitação de Processos de Legalização de Imóveis')
    @if($processo->Fase == 'Cumprir Exigências')
    <div class="justfy-content-center registro-fase">
        <div class="alert alert-info">
            <strong>Atenção!</strong> Existem exigências na sua solicitação:
            <a href="{{ route('tramitacao-imoveis-exigencias', $processo->Chamado) }}" class="ml-2">Editar Solicitação <i class="fa fa-edit"></i></a>
            
            <div class="col-lg-12">
                <div>Motivo: {{ $processo->motivo }}</div>
            </div>
        </div>
    </div>
    @endif
@endif