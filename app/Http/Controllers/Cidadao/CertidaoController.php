<?php

namespace App\Http\Controllers\Cidadao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Support\ImovelSupport;
use App\Support\ArquivoSupport;
use Auth;
use DB;

class CertidaoController extends Controller
{
    private $properties;
    private $paginator;

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->properties = new ImovelSupport();
        $this->paginator = new \CoffeeCode\Paginator\Paginator();
    }

    public function validaAutenticidade(Request $request)
    {

        try {
            $data1 = [
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'             => API_AUTENTICIDADE_CERTIDAO,
                'i_codigo_barra_cerdidao' => $request->codigo,
            ];
            $json_data = json_encode($data1, JSON_UNESCAPED_UNICODE);
            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_CERTIDAO,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));
            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $resposta = json_decode($response);

            if ($resposta->iStatus === 2) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>INVÁLIDA</strong> </p>
                <p>O código informado não é valido para nenhuma certidão dos nossos registros ou a validade está expirada.</p>
                <p>Verifique o código informado e tente novamente.</p>
                <p>Caso não consiga confirmar a autenticidade da certidão, digira-se a uma agência do SIM</p>
                Código: CERT_003'
                ]);
            } else {
                return view('cidadao.sucesso_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>CERTIDÃO VÁLIDA</strong> </p>
                <p>O código informado corresponde a uma certidão emitida e dentro do período de validade</p>'
                ]);
            }
        } catch (\Exception $e) {
            return view('cidadao.falha_integracao', [
                'titulo' => 'Certidões',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível verificar a autenticidade da certidão.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001'
            ]);
        }
    }

    public function certidaoQuitacaoItbi(Request $request, $matricula)
    {
        $suporteImovel = new ImovelSupport();

        $registros = $suporteImovel->listItbiImmobile($matricula);

        return view('cidadao.pesquisa_certidaoitbi', [
            'registros' => $registros,
            'url_servico' => $request->url_servico,
            'matricula' => $matricula
        ]);
    }

    public function certidaoNegativaImprimir($id)
    {
        $cpfUsuario = str_replace(".", "", Auth::user()->cpf);
        $cpfUsuario = str_replace("-", "", $cpfUsuario);
        $cpfUsuario = str_replace("/", "", $cpfUsuario);
        $cpfUsuario = str_replace(" ", "", $cpfUsuario);
        try {
            $data1 = [
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'             => API_IMOVEL_CERTIDAO,
                'i_codigo_maticula' => $id,
                'z01_cgccpf'        => $cpfUsuario,
            ];
            $json_data = json_encode($data1, JSON_UNESCAPED_UNICODE);
            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_CERTIDAO,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));
            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $resposta = json_decode($response);

            if ($resposta->iStatus === 2) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Este imóvel/matrícula possui débito(s), que impede(m) a emissão da correspondente certidão negativa</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_004',
                    'rota' => '../consultaInformacoes/certidaoNegativa'
                ]);
            } else {

                stream_context_set_default([
                    'ssl' => [
                        'verify_peer' => SSL_API_VERIFICA,
                        'verify_peer_name' => SSL_API_VERIFICA,
                    ]
                ]);

                if ($_SERVER['HTTP_HOST'] == 'ticmarica.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == '2amsolucoes.com.br') {
                    $resposta->sUrl = str_replace('https:', 'http:', $resposta->sUrl);
                }

                $filename = explode("/", $resposta->sUrl);

                if ($_SERVER['HTTP_HOST'] == '2amsolucoes.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1') {
                    $resposta->sUrl = str_replace("https", "http", $resposta->sUrl);
                }

                $tempImage = tempnam(sys_get_temp_dir(), $resposta->sUrl);
                copy($resposta->sUrl, $tempImage);

                $headers = array(
                    'Content-Type: application/pdf',
                );

                return response()->download($tempImage, $filename[count($filename) - 1], $headers);
            }
        } catch (\Exception $e) {

            return view('cidadao.falha_integracao', [
                'titulo' => 'Certidões',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível gerar a certidão deste imóvel.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001',
                'rota' => '../consultaInformacoes/certidaoNegativa'
            ]);
        }
    }

    public function certidaoValorVenalImprimir($id)
    {
        $cpfUsuario = str_replace(".", "", Auth::user()->cpf);
        $cpfUsuario = str_replace("-", "", $cpfUsuario);
        $cpfUsuario = str_replace("/", "", $cpfUsuario);
        $cpfUsuario = str_replace(" ", "", $cpfUsuario);
        try {

            $data1 = [
                'sNome'  => '$matricula_imovel',
                'sValor' => $id,
            ];
            $data2 = [
                'sNome'  => '$instituicao',
                'sValor' => '',
            ];
            $json_data1 = json_encode($data1, JSON_UNESCAPED_UNICODE);
            $json_data2 = json_encode($data2, JSON_UNESCAPED_UNICODE);

            $data1 = [
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'       => API_VALOR_VENAL_CERTIDAO,
                'aParametros' => [$data1, $data2],
            ];
            $json_data = json_encode($data1, JSON_UNESCAPED_UNICODE);

            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_CERTIDAO,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));
            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $resposta = json_decode($response);

            // Verifica se o imóvel está baixado
            $data1 = [
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'             => API_IMOVEL_CERTIDAO,
                'i_codigo_maticula' => $id,
                'z01_cgccpf'        => $cpfUsuario,
            ];
            $json_data = json_encode($data1, JSON_UNESCAPED_UNICODE);
            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_CERTIDAO,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));
            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $baixa = json_decode($response);

            if ($baixa->i_matricula_baixada === 1) { // Baixado
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi foi possível gerar a certidão deste imóvel.</p>
                <p>O imóvel está baixado e não possui certidão</p>
                <p><b>Código:</b> CERT_001</p>',
                    'rota' => '../consultaInformacoes/certidaoValorVenal'
                ]);
            } else {
                if ($resposta->iStatus === 2) {
                    return view('cidadao.falha_integracao', [
                        'titulo' => 'Certidões',
                        "mensagem" => '<p><strong>Atenção!</strong> Não foi foi possível gerar a certidão deste imóvel.</p>
                    <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                    <p><b>Código:</b> CERT_008</p>',
                        'rota' => '../consultaInformacoes/certidaoValorVenal'
                    ]);
                } else {

                    if ($resposta->iCertidaoGerada !== 1) {
                        return view('cidadao.falha_integracao', [
                            'titulo' => 'Certidões',
                            "mensagem" => '<p><strong>Atenção!</strong> Não foi foi possível gerar a certidão deste imóvel.</p>
                        <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                        <p><b>Código:</b> CERT_007</p>',
                            'rota' => '../consultaInformacoes/certidaoValorVenal'
                        ]);
                    } else {

                        stream_context_set_default([
                            'ssl' => [
                                'verify_peer' => SSL_API_VERIFICA,
                                'verify_peer_name' => SSL_API_VERIFICA,
                            ]
                        ]);

                        if ($_SERVER['HTTP_HOST'] == 'ticmarica.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == '2amsolucoes.com.br') {
                            $resposta->sUrl = str_replace('https:', 'http:', $resposta->sUrl);
                        }

                        $filename = explode("/", $resposta->sUrl);

                        if ($_SERVER['HTTP_HOST'] == '2amsolucoes.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1') {
                            $resposta->sUrl = str_replace("https", "http", $resposta->sUrl);
                        }

                        $tempImage = tempnam(sys_get_temp_dir(), $resposta->sUrl);
                        copy($resposta->sUrl, $tempImage);

                        $headers = array(
                            'Content-Type: application/pdf',
                        );

                        return response()->download($tempImage, $filename[count($filename) - 1], $headers);
                    }
                }  // Fim Baixado
            }
        } catch (\Exception $e) {
            return view('cidadao.falha_integracao', [
                'titulo' => 'Certidões',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível gerar a certidão deste imóvel.</p>
                <p>Tente Novamente em alguns instantes.</p>
                <p><b>Código:</b> INT_1001</p>',
                'rota' => '../consultaInformacoes/certidaoValorVenal'
            ]);
        }
    }

    public function certidaoITBIImprimir($matr, $guia)
    {
        $cpfUsuario = str_replace(".", "", Auth::user()->cpf);
        $cpfUsuario = str_replace("-", "", $cpfUsuario);
        $cpfUsuario = str_replace("/", "", $cpfUsuario);
        $cpfUsuario = str_replace(" ", "", $cpfUsuario);
        try {
            $data1 = [
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'                 => API_ITBI_CERTIDAO,
                'i_matricula_imovel'    => $matr,
                'i_itbi_guia'           => $guia,
            ];
            $json_data = json_encode($data1, JSON_UNESCAPED_UNICODE);
            $data_string = ['json' => $json_data];


            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_CERTIDAO,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));
            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $resposta = json_decode($response);

            if ($resposta->iStatus === 2) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi foi possível gerar a certidão deste imóvel.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_005',
                    'rota' => '../consultaInformacoes/certidaoITBI'
                ]);
            } else {

                stream_context_set_default([
                    'ssl' => [
                        'verify_peer' => SSL_API_VERIFICA,
                        'verify_peer_name' => SSL_API_VERIFICA,
                    ]
                ]);

                if ($_SERVER['HTTP_HOST'] == 'ticmarica.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == '2amsolucoes.com.br') {
                    $resposta->sUrl = str_replace('https:', 'http:', $resposta->sUrl);
                }

                $filename = explode("/", $resposta->sUrl);

                if ($_SERVER['HTTP_HOST'] == '2amsolucoes.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1') {
                    $resposta->sUrl = str_replace("https", "http", $resposta->sUrl);
                }

                $tempImage = tempnam(sys_get_temp_dir(), $resposta->sUrl);
                copy($resposta->sUrl, $tempImage);

                $headers = array(
                    'Content-Type: application/pdf',
                );

                return response()->download($tempImage, $filename[count($filename) - 1], $headers);
            }
        } catch (\Exception $e) {

            return view('cidadao.falha_integracao', [
                'titulo' => 'Certidões',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível gerar a certidão deste imóvel.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001',
                'rota' => '../consultaInformacoes/certidaoITBI'
            ]);
        }
    }

    public function certidaoNumeroPortaImprimir($id)
    {
        $cpfUsuario = str_replace(".", "", Auth::user()->cpf);
        $cpfUsuario = str_replace("-", "", $cpfUsuario);
        $cpfUsuario = str_replace("/", "", $cpfUsuario);
        $cpfUsuario = str_replace(" ", "", $cpfUsuario);
        try {
            $data1 = [
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'                 => API_NUMERO_PORTA_CERTIDAO,
                'i_matricula_imovel'    => $id,
            ];
            $json_data = json_encode($data1, JSON_UNESCAPED_UNICODE);
            $data_string = ['json' => $json_data];

            $ch = curl_init();

            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL . API_CERTIDAO,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));
            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            $resposta = json_decode($response);

            if ($resposta->iStatus === 2) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi foi possível gerar a certidão deste imóvel.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_002',
                    'rota' => '../consultaInformacoes/certidaoNumeroPorta'
                ]);
            } elseif ($resposta->iMatriculaInscBaixada !== 0) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> O imóvel está baixado.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_001',
                    'rota' => '../consultaInformacoes/certidaoNumeroPorta'
                ]);
            } else {

                stream_context_set_default([
                    'ssl' => [
                        'verify_peer' => SSL_API_VERIFICA,
                        'verify_peer_name' => SSL_API_VERIFICA,
                    ]
                ]);

                if ($_SERVER['HTTP_HOST'] == 'ticmarica.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == '2amsolucoes.com.br') {
                    $resposta->sUrl = str_replace('https:', 'http:', $resposta->sUrl);
                }

                $filename = explode("/", $resposta->sUrl);

                if ($_SERVER['HTTP_HOST'] == '2amsolucoes.com.br' || $_SERVER['HTTP_HOST'] == '127.0.0.1') {
                    $resposta->sUrl = str_replace("https", "http", $resposta->sUrl);
                }

                $tempImage = tempnam(sys_get_temp_dir(), $resposta->sUrl);
                copy($resposta->sUrl, $tempImage);

                $headers = array(
                    'Content-Type: application/pdf',
                );

                return response()->download($tempImage, $filename[count($filename) - 1], $headers);
            }
        } catch (\Exception $e) {
            return view('cidadao.falha_integracao', [
                'titulo' => 'Certidões',
                "mensagem" => '<p><strong>Atenção!</strong> Não foi possível gerar a certidão deste imóvel.</p>
                <p>Tente Novamente em alguns instantes.</p>
                Código: INT_1001',
                'rota' => '../consultaInformacoes/certidaoNumeroPorta'
            ]);
        }
    }

    public function certidaoITBI(Request $request)
    {
        $document = remove_character_document(Auth::user()->cpf);
        $resposta = $this->properties->listProperties($request->page, $document);

        $this->paginator->pager(
            $resposta->iPaginacao, // total de páginas
            count($resposta->aMatriculasCgmEncontradas), // qtd por página
            isset($request->page) ? $request->page : 1 // página atual
        );

        if ($resposta->iStatus === 2) {
            if ($resposta->iNumeroErro === 201) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Foram identificados diversos imóveis em seu cadastro. Dirija-se a uma agência do SIM.<br>
                Em breve este serviço estará disponível aqui para você!.</p>
                Código: ERR_API 002'
                ]);
            } else {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi encontrado nenhum imóvel registrado no seu nome.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_006'
                ]);
            }
        } else {
            Log::create(['servico' => 9, 'descricao' => "Certidão de Quitação de ITBI", 'cpf' => isset(Auth::user()->cpf) ? Auth::user()->cpf : "", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'],]);
            return view('cidadao.certidaoitbi', [
                'registros' => $resposta,
                'paginator' => $this->paginator->render()
            ]);
        }
    }

    public function certidaoValorVenal(Request $request)
    {
        $document = remove_character_document(Auth::user()->cpf);
        $resposta = $this->properties->listProperties($request->page, $document);

        $this->paginator->pager(
            $resposta->iPaginacao, // total de páginas
            count($resposta->aMatriculasCgmEncontradas), // qtd por página
            isset($request->page) ? $request->page : 1 // página atual
        );

        if ($resposta->iStatus === 2) {
            if ($resposta->iNumeroErro === 201) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Foram identificados diversos imóveis em seu cadastro. Dirija-se a uma agência do SIM.<br>
                Em breve este serviço estará disponível aqui para você!.</p>
                Código: ERR_API 002'
                ]);
            } else {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi encontrado nenhum imóvel registrado no seu nome.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_006'
                ]);
            }
        } else {
            Log::create(['servico' => 8, 'descricao' => "Declaração de Valor Venal de Imóvel", 'cpf' => isset(Auth::user()->cpf) ? Auth::user()->cpf : "", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'],]);
            return view('cidadao.certidaovalorvenal', [
                'registros' => $resposta,
                'paginator' => $this->paginator->render()
            ]);
        }
    }

    public function certidaoNegativa(Request $request)
    {
        $document = remove_character_document(Auth::user()->cpf);
        $resposta = $this->properties->listProperties($request->page, $document);

        $this->paginator->pager(
            $resposta->iPaginacao, // total de páginas
            count($resposta->aMatriculasCgmEncontradas), // qtd por página
            isset($request->page) ? $request->page : 1 // página atual
        );

        if ($resposta->iStatus === 2) {
            if ($resposta->iNumeroErro === 201) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Foram identificados diversos imóveis em seu cadastro. Dirija-se a uma agência do SIM.<br>
                Em breve este serviço estará disponível aqui para você!.</p>
                Código: ERR_API 002'
                ]);
            } else {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi encontrado nenhum imóvel registrado no seu nome.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_006'
                ]);
            }
        } else {
            Log::create(['servico' => 2, 'descricao' => "Certidão Negativa de Imóveis", 'cpf' => isset(Auth::user()->cpf) ? Auth::user()->cpf : "", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'],]);
            return view('cidadao.certidaonegativa', [
                'registros' => $resposta,
                'paginator' => $this->paginator->render()
            ]);
        }
    }

    public function certidaoNumeroPorta(Request $request)
    {
        $document = remove_character_document(Auth::user()->cpf);
        $resposta = $this->properties->listProperties($request->page, $document);

        $this->paginator->pager(
            $resposta->iPaginacao, // total de páginas
            count($resposta->aMatriculasCgmEncontradas), // qtd por página
            isset($request->page) ? $request->page : 1 // página atual
        );

        if ($resposta->iStatus === 2) {
            if ($resposta->iNumeroErro === 201) {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Imóvel',
                    "mensagem" => '<p><strong>Atenção!</strong> Foram identificados diversos imóveis em seu cadastro. Dirija-se a uma agência do SIM.<br>
                Em breve este serviço estará disponível aqui para você!.</p>
                Código: ERR_API 002'
                ]);
            } else {
                return view('cidadao.falha_integracao', [
                    'titulo' => 'Certidões',
                    "mensagem" => '<p><strong>Atenção!</strong> Não foi encontrado nenhum imóvel registrado no seu nome.</p>
                <p>Dirija-se a uma agência do SIM e verifique o seu cadastro.</p>
                Código: CERT_006'
                ]);
            }
        } else {
            Log::create(['servico' => 10, 'descricao' => "Certidão de Numeração Predial Oficial", 'cpf' => isset(Auth::user()->cpf) ? Auth::user()->cpf : "", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'],]);

            return view('cidadao.certidaonumeroporta', [
                'registros' => $resposta,
                'paginator' => $this->paginator->render()
            ]);
        }
    }

    public function solicitaCertidaoNota($tipo = null)
    {
        $docs = [
            'Última alteração do Contrato Social', 'Alvará de localização e funcionamento', 'Identidade', 'CPF',
            'Comprovante Residência do procurador com firma reconhecida', 'Pagamento da taxa de requerimento'
        ];
        switch ($tipo) {
            case 1:
                $type = "Certidão de Não Inscrito.";
                $title = "Certidão de Não Inscrito";
                $docs = $docs;
                break;
            case 2:
                $type = "Certidão Negativa de Débitos - Inscrição Municipal.";
                $title = "Certidão Empresarial Negativa de Débitos";
                $docs = $docs;
                break;
            case 3:
                $type = "Cancelamento de NF.";
                $title = "Cancelamento de Nota Fiscal";
                $docs = [
                    'Última alteração do Contrato Social', 'Alvará de localização e funcionamento', 'Identidade', 'CPF',
                    'Comprovante Residência do procurador com firma reconhecida', 'Pagamento da taxa de requerimento'
                ];
                break;
            default:
                $type = "Certidão de Não Inscrito";
                $title = "Certidão de Não Inscrito";
                break;
        }
        return view('certidao.solicitacao_certidao',  ['titulo' => $title, 'tipo' => $type, 'docs' => $docs, "estados" => getEstados()]);
    }

    public function salvarCertidaoNota(Request $request)
    {

        isset($request->rep_legal) ? $respPreenchimento = 'Representante Legal' : $respPreenchimento = 'Próprio';

        $divida = DB::connection('integracao')->table('lecom_certidoes_notas')->insertGetId([
            'cpf_cnpj'              => $request->documento,
            'rep_legal'             => $respPreenchimento,
            'nome'                  => $request->nome,
            'celular'               => $request->celular,
            'telefone'              => $request->telefone,
            'email'                 => $request->email,
            'tipo_solicitacao'      => $request->tipo_solicitacao,
            'cpf_cnpj_requerente'   => $request->cpf_req,
            'nome_contribuinte'     => $request->nome_rep,
            'inscricao_municipal'   => $request->insc_cont,
            'celular_requerente'    => $request->cel_cont,
            'telefone_requerente'   => $request->tel_cont,
            'email_requerente'      => $request->email_cont,
            'cep'                   => $request->cep,
            'endereco'              => $request->endereco,
            'complemento'           => $request->complemento,
            'numero'                => $request->numero,
            'bairro'                => $request->bairro,
            'municipio'             => $request->municipio,
            'estado'                => $request->estado,
            'data_cadastro'         => date('Y-m-d H:i:s'),

            'tipo_prestador'             => $request->tipo_prestador,
            'tipo_tomador'               => $request->tipo_tomador,
            'assinatura_carta_anuencia'  => $request->assinatura_carta_anuencia,
            'justificativa_cancelamento' => $request->justificativa_cancelamento,
            'emissao_guia_pagamento'     => isset($request->emissao_guia_pagamento) ? $request->emissao_guia_pagamento : '',
            'competencia_nota'           => isset($request->competencia_nota) ? $request->competencia_nota : '',
        ]);

        if ($divida) {
            session()->forget("documentos_processo");

            DB::connection('integracao')->table('lecom_certidoes_notas_documentos')
                ->where('cpf_cnpj', Auth::user()->cpf)
                ->where('id_certidao_nota', null)
                ->update(['id_certidao_nota' => $divida]);

            return redirect()
                ->route("certidoes-cancelamento-notas")
                ->with('success', ['title' => 'Processo de Legalização de Imóvel solicitado com sucesso']);
        } else {
            return response()->json(['error' => 'Houve um erro no envio de seus dados, favor tente mais tarde'], 200);
        }
    }

    public function salvarDocSessao(Request $request)
    {
        $extensoesPermitidas = ["pdf"];
        $extensao = $request->arquivo->extension();

        if (!in_array($extensao, $extensoesPermitidas)) {
            return ['error' => 'O arquivo deve ser uma imagem ou um pdf com as seguintes extensões (pdf).'];
        }

        $tamanhoArquivo = $request->arquivo->getClientSize();

        if ($tamanhoArquivo > 10024000) {
            return ['error' => 'O arquivo deve ter no máximo 10MB.'];
        }

        $cpf = remove_character_document(Auth::user()->cpf);
        $nomeNovoArquivo =  $cpf . "_" . remove_accentuation($request->descricao) . "_" . uniqid() . '.' . $extensao;
        $nomeNovoArquivo = str_replace("-", "_", $nomeNovoArquivo);

        if (isset($_FILES['arquivo']) && !empty($_FILES['arquivo']['name'])) {
            $file = file_get_contents($_FILES['arquivo']['tmp_name']);
            $doc = base64_encode($file);
            if ($doc) {
                if (isset($request->exigencia)) {
                    $data = DB::connection('integracao')->table('lecom_certidoes_notas_exigencias_arquivos')->insertGetId([
                        'cpf_cnpj'        => Auth::user()->cpf,
                        'processo'        => $request->processo,
                        'descricao'       => $request->descricao,
                        'tipo_arquivo'    => $extensao,
                        "nome_arquivo"    => $nomeNovoArquivo,
                        'arquivo_base64'  => $doc
                    ]);
                } else {
                    $data = DB::connection('integracao')->table('lecom_certidoes_notas_documentos')->insertGetId([
                        'cpf_cnpj'             => Auth::user()->cpf,
                        'nome_documento'       =>  $nomeNovoArquivo,
                        'documento'       => $doc,
                        'data_cadastramento' => date('Y-m-d H:i:s'),
                        "responsavel_anexo" => $request->resp_anexo,
                        'descricao'       => $request->descricao
                    ]);
                }
            }
        }

        $upload = $request->arquivo->storeAs('documentos', $nomeNovoArquivo, 'ftp');

        if ($upload) {

            session()->push("documentos_processo", (object) [
                "descricao" => $request->descricao,
                "nome_original" => $nomeNovoArquivo,
                "data_cad" => $request->data_cad,
                "resp_anexo" => $request->resp_anexo,
                'id' => $data
            ]);

            return view('certidao.tabela_certidao_nf');
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removeDocSessao(Request $request)
    {
        $documentos = session()->get("documentos_processo");

        if (!empty($documentos)) {

            DB::connection('integracao')->table('lecom_certidoes_notas_documentos')->delete($request->documento);

            foreach ($documentos as $key => $value) {

                if (in_array($request->documento, (array) $value)) {
                    unset($documentos[$key]);
                    session()->forget("documentos_processo");
                    session()->put('documentos_processo', $documentos);
                }
            }
        }

        return view("certidao.tabela_certidao_nf");
    }

    public function salvarExigenciaNfCertidao(Request $request)
    {
        $data = $this->getDataNfCertidao($request->processo);

        $divida = DB::connection('integracao')->table('lecom_certidoes_notas_exigencias')->insertGetId([
            'id_certidao_nota'  => $data->id,
            'cpf_cnpj'          => Auth::user()->cpf,
            'ciclo'             => $request->ciclo,
            'processo'          => $request->processo,
            'recurso'           => $request->obs,
            'data'              => date('Y-m-d H:i:s')
        ]);

        if ($divida) {
            session()->forget("documentos_processo");

            DB::connection('integracao')->table('lecom_certidoes_notas_exigencias_arquivos')
                ->where('cpf_cnpj', Auth::user()->cpf)
                ->where('id_exigencia', null)
                ->update(['id_exigencia' => $divida]);

            return redirect()
                ->route("certidoes-cancelamento-notas")
                ->with('success', ['title' => 'Documentos enviados com sucesso']);
        } else {
            return response()->json(['error' => 'Houve um erro no envio de seus dados, favor tente mais tarde'], 200);
        }
    }

    public function getDataNfCertidao($chamado)
    {
        return DB::connection('integracao')
            ->table('lecom_certidoes_notas')
            ->where('chamado_lecom', '=', $chamado)
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function verificaDocumentacao(Request $request)
    {
        return ArquivoSupport::verificarArquivosExigidosNaSessao('documentos_processo', $request->documentosExigidos);
    }
}
