@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-home"></i> Certidão Negativa de Débitos de Empresas</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('certnegativa_validarCpf') }}" id="infoCpf">
                            @csrf

                            <div class="form-group">
                                <label for="cgm_cpf" class="control-label">CPF/CNPJ</label>
                                <input maxlength="20" id="cgm_cpf" type="text" class="form-control {{ $errors->has('cgm_cpf') ? ' is-invalid' : '' }}" name="cgm_cpf" 
                                    @auth
                                        value="{{Auth::user()->cpf}}" 
                                    @else
                                        value="" 
                                    @endif
                                    required>
            
                                @if ($errors->has('pat_cpf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pat_cpf') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>
        
                            <a href="{{ route('servicos', [1, "certidoes"] )  }}">
                                <button type="button" class="btn btn-primary form-group">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </button>
                            <a>

                            <button type="button" class="btn btn-success form-group" id="validar_CPF">
                                Gerar Certidão
                            </button>
                        </form>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#validar_CPF").prop('disabled', true);
    $("#cgm_cpf").mask('00.000.000/0000-00');

    function onReCaptchaTimeOut(){
        $("#validar_CPF").prop('disabled', true);
    }
    function onReCaptchaSuccess(){
        $("#validar_CPF").prop('disabled', false);
    }


    $("#validar_CPF").click(function() {

        if ($("#cgm_cpf").val() == "") {
            alert("Por favor informe o CNPJ");
            $("#cgm_cpf").val("");
            $("#cgm_cpf").focus();
            return false;
        }

        if (!validarCNPJ($("#cgm_cpf").val())) {
            alert("CNPJ Inválido!");
            $("#cgm_cpf").val("");
            $("#cgm_cpf").focus();
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        $("#infoCpf").submit();
        $("#cgm_cpf").val("");
        grecaptcha.reset();

        setTimeout(function(){ $("#mdlAguarde").modal('toggle'); }, 5000);
    });

</script>
@endsection