@component('mail::message')
Prezado(a) cidadão(ã),

<p>
    Informamos que sua inscrição no Programa Fomenta Maricá MEI Emergencial foi realizada com sucesso.
</p>
<p>
    Peço que anote o seu Protocolo <strong>{{ $protocolo }}</strong>.
</p>
<p>
    Com o protocolo acima poderá realizar o acompanhamento de sua solicitação no endereço:<br> {{ $url }}
</p>
<p>
    Todas  as  informações  sobre  a  sua  solicitação  também  serão  enviadas por e-mail.
</p>
<p>
    Fique  atento(a)  a  caixa  de  entrada  de  e-mail  e  não  deixe  de  verificar  o SPAM
</p>
<p>
    <strong>ATENÇÃO:</strong>
</p>
<ul>
    <li>
        É condição para o recebimento do crédito ter seguido o Passo 2. Caso não tenha realizado clique no link abaixo e conclua seu cadastro: <a href="https://forms.gle/wRnyNsMU8ShAz3Wg8">https://forms.gle/wRnyNsMU8ShAz3Wg8</a>
    </li>
    <li>
        As análises documentais somente serão iniciadas a partir início de <b><u>setembro  de  2020</b></u>, acompanhe sua solicitação utilizando-se deste protocolo para mais informações dentro do próprio sistema.
    </li>
    <li>
        O Programa Fomenta Maricá MEI Emergencial dará prioridade ao atendimento de Microempreendedores <b><u>NÃO beneficiados</b></u> pelo Programa de Amparo ao Trabalhador – PAT e pelo Programa de Amparo ao Emprego – PAE e que sejam mais antigos.
    </li>
    <li>
        A inscrição no Programa <b><u>NÃO garante</u></b> que a sua solicitação será aprovada ficando sujeita, além dos itens acima, ao limite de recurso destinado ao Programa Fomenta Maricá Emergencial MEI.
    </li>

</ul>

<p>
    Para  dúvidas  e  outros  esclarecimentos,  entre  em  contato  com  nossa equipe de atendimento ao cidadão.
</p>
<p>
    E-mail: sacfomentamarica@marica.rj.gov.br
</p>
<p>
    Whatsapp 1: (21) 99260-2952 <br>
    Whatsapp 2: (21) 99497-0865
</p>

Atenciosamente,<br>
Equipe Programa Fomenta Maricá

@endcomponent
