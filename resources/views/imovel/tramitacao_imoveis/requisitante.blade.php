<div class="form-row">
    <div class="col-lg-6 form-group">
        <label for="requisitante">Nome:</label>
        <input type="text" name="requisitante" id="requisitante" 
        class="form-control form-control-sm {{ ($errors->has('requisitante') ? 'is-invalid' : '') }}"
            value="Portal" maxlength="50" readonly="true" />

            @if ($errors->has('requisitante'))
                <div class="invalid-feedback">
                    {{ $errors->first('requisitante') }}
                </div>
            @endif
    </div>

    <div class="col-lg-6 form-group">
        <label for="secretaria_requisitante">Secretaria Requisitante:</label>
        <input type="text" name="secretaria_requisitante" id="secretaria_requisitante" 
        class="form-control form-control-sm {{ ($errors->has('secretaria_requisitante') ? 'is-invalid' : '') }}"
            value="Secretaria de Planejamento Orçamento e Gestão" maxlength="100" readonly="true" />

            @if ($errors->has('secretaria_requisitante'))
                <div class="invalid-feedback">
                    {{ $errors->first('secretaria_requisitante') }}
                </div>
            @endif
    </div>
</div>
