
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="documento_endereco_requrente">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Nome Original</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documento_endereco_requerente"))
                @foreach(session("documento_endereco_requerente") as $documento)
                    <tr>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento" title="Remover Documento" 
                                data-nome_arquivo="{{ $documento->nome_novo }}"
                                data-pasta="endereco_requerente"
                                data-nome_sessao="documento_endereco_requerente"
                                data-id_tabela="documento_endereco_requerente"
                                data-conteudo_documentos="conteudo_endereco_requerente_documento">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>