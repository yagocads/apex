
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela_dados_mercantis">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Arquivo</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_dados_mercantis"))
                @foreach(session("documentos_dados_mercantis") as $documento)
                    <tr>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento" title="Remover Documento" 
                                data-nome_arquivo="{{ $documento->nome_novo }}"
                                data-pasta="dados_mercantis"
                                data-nome_sessao="documentos_dados_mercantis"
                                data-id_tabela="tabela_dados_mercantis"
                                data-conteudo_documentos="conteudo_dados_mercantis">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>