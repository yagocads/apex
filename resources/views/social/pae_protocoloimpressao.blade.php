<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PAE</title>

        <!--Custon CSS-->
        <link rel="stylesheet" href="{{ getcwd().'/css/certidao.css' }}">

        <!--Favicon-->
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    </head>
    <body>

        <div class="header">
            <div class="div-logo">
                <img src="{{ getcwd().'/img/brasao.png'  }}" class="logo" alt="Prefeitura de Maricá">
            </div>
            <div class="titulo">
                ESTADO DO RIO DE JANEIRO<br>
                PREFEITURA MUNICIPAL DE MARICÁ<br>
                PAE - PROGRAMA DE AMPARO AO EMPREGO
            </div>
        </div>
            <div>
                <h1 class="titulo-autenticacao">
                    PROTOCOLO DE SOLICITAÇÃO<br>
                    DE BENFÍCIO
                </h1>

                <p class="dados-certidao">
                    A sua solicitação do benefício do PAE - PROGRAMA DE AMPARO AO EMPREGO foi realizada com sucesso.<br>
                </p>
                <br>
                <p class="texto-autenticacao">
                    Número do protocolo:<br>
                </p>

                <h2 class="status">{{$protocolo}}</h2>

                <p class="dados-certidao"><b>CNPJ: </b>{{$cnpj}} </p>
                <p class="dados-certidao"><b>Razão Social: </b> {{$razaoSocial}}</p>
                <p class="dados-certidao"><b>Data de Emissão:</b> {{ \Carbon\Carbon::parse()->format('d/m/Y H:i:s') }}</p>
                <br>
                <br>
                <br>
                <p class="data-autenticacao">
                    
                    Maricá, {{ \Carbon\Carbon::parse(date("Y-m-d"))->locale("pt-BR")->format('d/m/Y') }}.
                </p>
            </div>


    </body>
</html>
