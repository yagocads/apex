<div class="alert alert-info">
    <div class="row">
        <div class="col-lg-12">
            <h6 class="">Documentação Obrigatória:</h6>
            <ul class="doc_obrigatorio">
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-user mt-3 mb-3"></i> Dados do cidadão:</h5>
        <input type="hidden" name="resp_anexo" id="resp_anexo" value="{{ Auth::user()->name }}" class="form-control form-control-sm" />
        <input type="hidden" name="data_cad" id="data_cad" value="{{ date('d/m/Y', strtotime(now())) }}" class="form-control form-control-sm" />
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="descricao">Descrição <span class="asterisc">*</span></label>
                <select name="descricao" id="descricao" class="form-control form-control-sm">
                    <option value="">Selecione</option>
                    <option value="Alvará de localização e funcionamento" id="alvara_localizacao_funcionamento">Alvará de localização e funcionamento</option>
                    <option value="CPF">CPF</option>
                    @if($tipo ==  'Cancelamento de NF.')
                        <option value="Cópia da Nota Fiscal">Cópia da Nota Fiscal</option>
                    @endif
                    <option value="Comprovante Residência do procurador com firma reconhecida">Comprovante Residência do procurador com firma reconhecida</option>
                    <option value="Identidade">Identidade</option>
                    <option value="Pagamento da taxa de requerimento">Pagamento da taxa de requerimento</option>
                    <option value="Última alteração do Contrato Social" id="ultima_alteracao_contrato_social">Última alteração do Contrato Social</option>
                    @if($tipo ==  'Cancelamento de NF.')
                        <option value="Cópia da Nota Fiscal">Cópia da Nota Fiscal</option>
                    @endif
                </select>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>
        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 10Mb.
                </div>
            </div>

            <div class="col-lg-12 form-group">
                <label for="documento_representacao_legal"><i class="fa fa-paperclip"></i> Anexar Documento:</label>
                <input type="file" name="documento_representacao_legal" id="documento_representacao_legal">
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="row col-lg-12 mb-3 mt-3">
    <h5><i class="fa fa-file-o"></i> Documentos Enviados</h5>
</div>

<div class="row">
    <div class="col-lg-12 conteudo_documentos">
        @include('certidao.tabela_certidao_nf')
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#add").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){
            $(this).parents(".control-group").remove();
        });

        $(".btn-enviar-documento").click(function(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            let load = $(".ajax_load");
            let arquivo = $('#documento_representacao_legal').prop('files')[0];
            let descricao = $("#descricao").val();
            let resp_anexo =  $("#resp_anexo").val();
            let data_cad =  $("#data_cad").val();

            if (!validarAnexo(arquivo, ["pdf"],'10485760')) {
                return false;
            }

            if (!arquivo || descricao == '') {
                alert("Escolha um arquivo e o documento do processo.");
                return false;
            }

            let formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("resp_anexo", resp_anexo);
            formData.append("data_cad", data_cad);

            let url = "{{ route('salvar-doc-cn') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }
                    $('#descricao').val('');
                    $('#documento_representacao_legal').val('');
                    $(".conteudo_documentos").html(response);
                }
            });
        });
    });
</script>
