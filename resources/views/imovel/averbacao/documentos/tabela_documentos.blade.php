
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="documentos_adquirentes">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Nome Original</th>
                <th>Nome Novo</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_adquirentes"))
                @foreach(session("documentos_adquirentes") as $documento)
                    <tr>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td>{{ $documento->nome_novo }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm remover_documento_adquirente" 
                                title="Remover Documento" data-documento="{{ $documento->nome_novo }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
