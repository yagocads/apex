@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('fornecedorExcluidoComSucesso'))
                <div class="alert alert-success">
                    {{ session('fornecedorExcluidoComSucesso') }}
                </div>
            @endif

            @if (session()->has('fornecedorCadastradoComSucesso'))
                <div class="alert alert-success">
                    {{ session('fornecedorCadastradoComSucesso') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>{{__('Gerenciar Fornecedor')}}</h4>
                </div>

                <div class="card-body">
                    @if (!$fornecedor)
                        <a href="{{ route('cadastrar-fornecedor') }}"
                            class="btn btn-primary mb-3">{{__('Cadastrar Fornecedor')}}</a>
                    @endif

                    <div class="table-responsive py-1 pl-1 pr-1">
                        <table class="table table-striped table-bordered dataTable dt-responsive w-100">
                            <thead>
                                <tr>
                                    <th>CNPJ</th>
                                    <th>{{__('Razão Social')}}</th>
                                    <th data-priority="1">{{__('Ações')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($fornecedor)
                                    <tr>
                                        <td>{{ $fornecedor->cnpj }}</td>
                                        <td>{{ $fornecedor->razao_social }}</td>
                                        <td>
                                            <a href="{{ route('editar-fornecedor', $fornecedor->id) }}" class="btn btn-primary">Editar</a>
                                            <button class="btn btn-danger"
                                                data-toggle="modal" data-target="#modal-motivo-exclusao">{{__('Excluir')}}</button>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('apex.fornecedores.modal-motivo-exclusao')
</div>

@endsection

@section('post-script')
    <script>
        let load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });

        $('body').on('click', '.btn-excluir-fornecedor', function(e) {
            e.preventDefault();

            let motivo = $('#motivo');

            if (!motivo.val()) {
                alert('Atenção: preencha o campo motivo para continuar.');
                motivo.focus();
                return;
            }

            $.ajax({
                url: "{{ route('excluir-fornecedor', (!empty($fornecedor->id) ? $fornecedor->id : '')) }}",
                method: "POST",
                data: { motivo: $('textarea[name=motivo]').val() },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    if (response) {
                        location.reload();
                    }
                },
            });
        });
    </script>
@endsection
