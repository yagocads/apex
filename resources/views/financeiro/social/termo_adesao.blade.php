<div class="col-lg-5 form-group">
    <label for="descricao">Descrição:</label>
    <select name="descricao" id="descricao_termo_adesao" class="form-control form-control-sm">
        <option></option>
        <option>TERMO DE ADESÃO</option>
    </select>
</div>

<div class="col-lg-7 form-group">
    <label for="arquivo">Documento:</label><br>
    <input type="file" name="arquivo" id="arquivo_termo_adesao" />
</div>

<div class="col-lg-12">
    <button class="btn btn-primary btn-sm salvar_documento"><i class="fa fa-save"></i> Salvar Documento</button>
</div>

<div class="row col-lg-12 mb-3 mt-3">
    <h5><i class="fa fa-file-o"></i> Documentos Enviados:</h5>
</div>

<div class="row">
    <div class="col-lg-12 conteudo_termo_adesao">
        @include('financeiro.social.tabela_termo_adesao')
    </div>
</div>