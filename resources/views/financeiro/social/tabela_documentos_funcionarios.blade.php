
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100"  id="documentos_funcionarios">
        <thead>
            <tr>
                <th>Mês</th>
                <th>CPF</th>
                <th>Nome</th>
                <th>Tipo</th>
                <th>Descrição</th>
                <th>Nome Original</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_funcionarios"))
                @foreach(session("documentos_funcionarios") as $documento)
                    <tr>
                        <td>{{ $documento->mes }}</td>
                        <td>{{ $documento->cpf }}</td>
                        <td>{{ $documento->nome }}</td>
                        <td>{{ $documento->tipo }}</td>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm remover_documento_funcionario" 
                                title="Remover Documento" data-documento="{{ $documento->nome_novo }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
