<div class="row mdi-image-filter-3">
    <div class="col-lg-12">
        <div class="alert alert-success">
            <h5 class="mb-4 mt-3"><i class="fa fa-dollar"></i> Dados Bancários:</h5>

            <p>Agência: {{ $dadosBancarios->agencia }}</p>
            <p>Conta: {{ $dadosBancarios->conta }}</p>
        </div>
    </div>
</div>