@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <i class="fa fa-home"></i> 
                            Fomenta Maricá
                        </h4>
                    </div>

                    <div class="card-body">
                        
                        <form method="POST" action="{{ route('fomentavalidasituacao') }}" id="infoCpf">
                            @csrf

                            <input id="vencimentoIPTU" type="hidden" name="vencimentoIPTU" value="{{ isset($vencimentoIPTU) ? $vencimentoIPTU : 0}}" />
                            
                            <div class="form-group">
                                <label for="cnpj">Digite seu CNPJ</label>
                                
                                <input type="text" name="cnpj" id="cnpj" 
                                    class="form-control form-control-sm col-lg-12 cpfOuCnpj 
                                    {{ $errors->has('cnpj') ? 'is-invalid' : '' }}" maxlength="20" required="true" />
                                
                                @if ($errors->has('cnpj'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('cnpj') }}
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label for="protocolo">Digite seu Protocolo</label>
                                
                                <input type="text" name="protocolo" id="protocolo" 
                                    class="form-control form-control-sm col-lg-12  
                                    {{ $errors->has('protocolo') ? 'is-invalid' : '' }}" maxlength="20" required="true" />
                                
                                @if ($errors->has('protocolo'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('protocolo') }}
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <label for="dtAbertura">Digite a Data de Abertura da Empresa</label>
                                
                                <input type="text" name="dtAbertura" id="dtAbertura" 
                                    class="form-control form-control-sm col-lg-12  
                                    {{ $errors->has('dtAbertura') ? 'is-invalid' : '' }}" maxlength="20" required="true" />
                                
                                @if ($errors->has('dtAbertura'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('dtAbertura') }}
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <a href="{{ route('fomenta') }}" class="btn btn-primary mt-3">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>
                            
                                <button type="submit" id="validar_CPF" class="btn btn-success mt-3">
                                    Validar CNPJ
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#dtAbertura").mask("00/00/0000");
    $("#validar_CPF").prop('disabled', true);
   
    function onReCaptchaTimeOut() {
        $("#validar_CPF").prop('disabled', true);
    }

    function onReCaptchaSuccess() {
        $("#validar_CPF").prop('disabled', false);
    }

    $('#cnpj').change(function() {

        if(!validarCNPJ($("#cnpj").val())) {
            alert("CNPJ Inválido!");
            $("#cnpj").focus();
            return false;
        }

    });

</script>

@endsection
