@if( $processo->Serviço == 'Solicitar Auxílio Transporte')
    <div class="justfy-content-center processo-fase">
        @if($processo->Fase != "Finalizado" && $processo->Fase != 'Concluído')
            @if($processo->responsavel == 'portal')
                <br>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                    <a href="{{ route('formularioEdita', $processo->Chamado) }}" class="btn btn-blue">Editar Solicitação</a>
                </div>
            @endif
        @endif
        @if($processo->motivo != "" && $processo->FINALIZADO != "andamento")
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Atenção!</strong><br>
            Esta solicitação foi finalizada e teve como conclusão:<br>
            Conclusão: <strong>{{$processo->Fase}}</strong><br>
            @if($processo->motivo != "")
                Motivo: {{$processo->motivo}}<br>
            @endif
        </div>
        @endif
    </div>
@endif