@extends('layouts.tema01')

@section('content')
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span8">
                <div class="inner-heading">
                    <h2>PAE</h2>
                </div>
            </div>
            <div class="span4">
                <ul class="breadcrumb">
                    <li><a href="{{  url('/') }}"><i class="fa fa-home"></i></a><i class="fa fa-angle-right"></i></li>
                    <li class="active">PAE</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="tabbable tabs-left">
            <ul class="nav nav-tabs" id="menuTab">
                <li class="active">
                    <a href="#tab-1" data-toggle="tab">
                        BENEFÍCIO
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-1">
                    <div class="tab-interna">
                        <div class="titulo-servico">
                            <h2>
                                BENEFÍCIO
                            </h2>
                            <a href="{{ route( 'consultarsituacaopae' ) }}">
                                <button type="button" class="btn btn-theme e_wiggle" id="autenticar">
                                    {{ __('Consultar Situação do Benefício') }}
                                </button>
                            </a>
                        </div>
                        <p>
                            <a href="{{  route( 'PAE_CNPJ' ) }}" class="btnServico">
                            Solicite o <strong>Benefício</strong> aqui:
                            </a>
                        </p>
                            <p class="lead" >
                                <a href="{{ route( "PAE_CNPJ" ) }}" class="btnServico" >
                                    <button type="button" class="btn btn-theme full">
                                        PROGRAMA DE AMPARO AO EMPREGO
                                    </button>
                                </a>

                                <span class="pullquote-left" style="margin-top: 6px;">
                                    Programa de Amparo ao Emprego é Iniciativa de apoio ao empregador MEI, Micro ou Pequena empresa, com efetivo de até 49 empregados, afetadas pela pandemia do COVID-19, visando a manutenção dos empregos formais no município, através do repasse de 01 (um) salário mínimo mensal, por 03 meses a fim de subsidiar o salário de seus funcionários.
                                    <br><br>
                                        {{-- <br>
                                        <span style="font-size: 16px;">
                                            <b>
                                            Encerramento das inscrições no dia 07/04/2020 foi alterado de 01:31:46 para 13:31:46
                                            </b>
                                        </span>
                                        <br>
                                        <br> --}}
                                </span>

                            </p>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="span1 align-left">
            </div>
            <div class="span8 align-left">
                <a href="{{ route('PAE_PAGINA' )  }}">
                    <button type="button" class="btn btn-theme e_wiggle">
                        {{ __('Voltar') }}
                    </button>
                    <a>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@endsection

@section('post-script')
<script type="text/javascript">
    jQuery(document).ready(function(){
        
        $(".btnServico").click(function () {
            $("#mdlAguarde").modal('toggle');
        });

    });


</script>
@endsection
