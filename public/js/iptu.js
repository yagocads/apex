/************** IPTU **********/

/**********habilita ou desabilita botoes e inputs de inscricao ou matricula*/
$("#inscricaoB").click(function (){
    $("#btn_pesquisar_matricula").prop("disabled", true);
    $("#btn_pesquisar_inscricao").prop("disabled", false);
    $("#inscricao").prop("disabled", false);
    $("#matricula").prop("disabled", true);
});

$("#matriculaB").click(function (){
    $("#btn_pesquisar_matricula").prop("disabled", false);
    $("#btn_pesquisar_inscricao").prop("disabled", true);
    $("#inscricao").prop("disabled", true);
    $("#matricula").prop("disabled", false);
});
/**********Fim**************/

/**********lista imoveis ou busca um determinado imóvel */
$("#btn_pesquisar_matricula").click(function(e) {

    e.preventDefault();

    let load = $(".ajax_load");
    let cpf = $('#ro_cpf').val();
    let document = formatDocument(cpf);
    let url = 'imoveis/' + document;
    var mat =  $("#matricula").val();

    $("#matricula").val() != "" ? url =  url + '/' + mat : '';

    $.ajax({
        url: url,
        method: 'GET',
        beforeSend: function () {
            load.fadeIn(200).css("display", "flex");
        },
        success(response) {
            if (response.mensagem) {
                load.fadeOut(200);
                alert(response.mensagem);
            } else {
                load.fadeOut(200);
                $(".modal-body").html(response);
                $("#footer-modal").modal('show');
            }
            load.fadeOut(200);
        }
    });
});
/**********Fim**************/

/**********checkbox normais debitos em aberto **************/

    var total_valor = 0;
    var total_corrigido = 0;
    var total_juros = 0;
    var total_multa = 0;
    var total_desconto = 0;
    var resultado = 0;

    $(':checkbox[name^=registros_normais]').click(function() {

        var colunas;
        var valor;
        var corrigido;
        var juros;
        var multa;
        var desconto;
        var total;
        var checkbox = $('input:checkbox[name^=registros_normais]');
        var checkbox_checked = $('input:checkbox[name^=registros_normais]:checked');
        var values = [];

        $('#linha_totais').html('');
        $('body #registros_unicos').prop('checked',false);
        $('input[type=radio]').prop('checked',false);
        if(checkbox_checked.length > 0){
            var total_valor = 0;
            var total_corrigido = 0;
            var total_juros = 0;
            var total_multa = 0;
            var total_desconto = 0;
            var resultado = 0;

            checkbox_checked.each(function(){

                values.push($(this).val());
                colunas = $(this).val().split("_");

                valor = Number(colunas[4].replace(",",""));
                corrigido = Number(colunas[5].replace(",",""));
                juros = Number(colunas[6].replace(",",""));
                multa = Number(colunas[7].replace(",",""));
                desconto = Number(colunas[8].replace(",",""));
                total = Number(colunas[9].replace(",",""));

                total_valor += valor;
                total_corrigido += corrigido;
                total_juros += juros;
                total_multa += multa;
                total_desconto += desconto;
                resultado += total;
            });
        }

        if(checkbox_checked.length == checkbox.length){
            $('#checkAll').prop('checked', true);
        } else {
             $('body #checkAll').prop('checked',false);
        }
        $('#linha_totais').append("<p><b>Valor: "+total_valor.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='valor_total' id='valor_total' value='"+total_valor+"'></b></p><p><b>Corrigido: "+total_corrigido.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='corrigido_total' id='corrigido_total' value='"+total_corrigido+"'></b></p><p><b>Juros: "+total_juros.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='juros_total' id='juros_total' value='"+total_juros+"'></b></p><p><b>Multa: "+total_multa.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='multa_total' id='multa_total' value='"+total_multa+"'></b></p><p><b>Desconto: "+total_desconto.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='desconto_total' id='desconto_total' value='"+total_desconto+"'></b></p><p><b>Total: "+resultado.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='total_total' id='total_total' value='"+resultado+"'></b></p>");

    });
/**********Fim**************/

/**********radio registro unicos debitos em aberto **************/
    $(':radio[name^=registros_unicos]').click(function() {

        var total_valor = 0;
        var total_corrigido = 0;
        var total_juros = 0;
        var total_multa = 0;
        var total_desconto = 0;
        var resultado = 0;
        var colunas = $(this).val().split("_");

        $('#linha_totais').html('');
        $('input[type=checkbox]').prop('checked',false);
        $('body #checkAll').prop('checked',false);


        if (colunas.length == 10) {
            var valor = Number(colunas[4].replace(",","."));
            var corrigido = Number(colunas[5].replace(",","."));
            var juros = Number(colunas[6].replace(",","."));
            var multa = Number(colunas[7].replace(",","."));
            var desconto = Number(colunas[8].replace(",","."));
            var total = Number(colunas[9].replace(",","."));
        } else {
            var valor = colunas[5] == 'NaN' ? Number(colunas[5].replace(",",".")) : colunas[5];
            var corrigido = colunas[6] == 'NaN' ? Number(colunas[6].replace(",",".")) : colunas[6];
            var juros = colunas[7] == 'NaN' ? Number(colunas[7].replace(",",".")) : colunas[7];
            var multa = colunas[8] == 'NaN' ? Number(colunas[8].replace(",",".")) : colunas[8];
            var desconto = colunas[9] == 'NaN' ? Number(colunas[9].replace(",",".")) : colunas[9];
            var total = colunas[10] == 'NaN' ? Number(colunas[10].replace(",",".")) : colunas[10];
        }

        if ($(this).is(":checked")) {
            total_valor = valor;
            total_corrigido = corrigido;
            total_juros = juros;
            total_multa = multa;
            total_desconto = desconto;
            resultado = total;
        } else {
            total_valor -= valor;
            total_corrigido -= corrigido;
            total_juros -= juros;
            total_multa -= multa;
            total_desconto -= desconto;
            resultado -= total;
        }

        $('#linha_totais').append("<p><b>Valor: "+total_valor.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='valor_total' id='valor_total' value='"+total_valor+"'></b></p><p><b>Corrigido: "+total_corrigido.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='corrigido_total' id='corrigido_total' value='"+total_corrigido+"'></b></p><p><b>Juros: "+total_juros.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='juros_total' id='juros_total' value='"+total_juros+"'></b></p><p><b>Multa: "+total_multa.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='multa_total' id='multa_total' value='"+total_multa+"'></b></p><p><b>Desconto: "+total_desconto.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='desconto_total' id='desconto_total' value='"+total_desconto+"'></b></p><p><b>Total: "+resultado.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='total_total' id='total_total' value='"+resultado+"'></b></p>");

    });
    /**********Fim**************/

/**********radio registro normais debitos em aberto **************/
    $(':radio[name^=registros_normais]').click(function() {

        var total_valor = 0;
        var total_corrigido = 0;
        var total_juros = 0;
        var total_multa = 0;
        var total_desconto = 0;
        var resultado = 0;
        var colunas = $(this).val().split("_");

        $('#linha_totais').html('');
        $('input[type=checkbox]').prop('checked',false);
        $('body #checkAll').prop('checked',false);


        if (colunas.length == 10) {
            tmpValor1 = colunas[4].replace(".","");
            var valor = Number(tmpValor1.replace(",","."));
            tmpValor2 = colunas[5].replace(".","");
            var corrigido = Number(tmpValor2.replace(",","."));
            tmpValor3 = colunas[6].replace(".","");
            var juros = Number(tmpValor3.replace(",","."));
            tmpValor4 = colunas[7].replace(".","");
            var multa = Number(tmpValor4.replace(",","."));
            tmpValor5 = colunas[8].replace(".","");
            var desconto = Number(tmpValor5.replace(",","."));
            tmpValor6 = colunas[9].replace(".","");
            var total = Number(tmpValor6.replace(",","."));
        } else {
            var valor = colunas[5] == 'NaN' ? Number(colunas[5].replace(",",".")) : colunas[5];
            var corrigido = colunas[6] == 'NaN' ? Number(colunas[6].replace(",",".")) : colunas[6];
            var juros = colunas[7] == 'NaN' ? Number(colunas[7].replace(",",".")) : colunas[7];
            var multa = colunas[8] == 'NaN' ? Number(colunas[8].replace(",",".")) : colunas[8];
            var desconto = colunas[9] == 'NaN' ? Number(colunas[9].replace(",",".")) : colunas[9];
            var total = colunas[10] == 'NaN' ? Number(colunas[10].replace(",",".")) : colunas[10];
        }

        if ($(this).is(":checked")) {
            total_valor = valor;
            total_corrigido = corrigido;
            total_juros = juros;
            total_multa = multa;
            total_desconto = desconto;
            resultado = total;
        } else {
            total_valor -= valor;
            total_corrigido -= corrigido;
            total_juros -= juros;
            total_multa -= multa;
            total_desconto -= desconto;
            resultado -= total;
        }

        $('#linha_totais').append("<p><b>Valor: "+total_valor.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='valor_total' id='valor_total' value='"+total_valor+"'></b></p><p><b>Corrigido: "+total_corrigido.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='corrigido_total' id='corrigido_total' value='"+total_corrigido+"'></b></p><p><b>Juros: "+total_juros.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='juros_total' id='juros_total' value='"+total_juros+"'></b></p><p><b>Multa: "+total_multa.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='multa_total' id='multa_total' value='"+total_multa+"'></b></p><p><b>Desconto: "+total_desconto.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='desconto_total' id='desconto_total' value='"+total_desconto+"'></b></p><p><b>Total: "+resultado.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='total_total' id='total_total' value='"+resultado+"'></b></p>");

    });
/**********Fim**************/

/**********emissao de boleto hora **************/
    let now = new Date();
    if (now.getHours() >= 7 && now.getHours() < 19) {
        $('#btn_emitir_boleto').attr('disabled',false);
        $('#mensagem').hide();
        $('#dataVencimento').attr('disabled',false);
    }
/**********Fim**************/

/**********emissao de boleto **************/
    $('#btn_emitir_boleto').click(function(){
        $('#formulario').submit(function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var load = $(".ajax_load");
            var data_vencimento = $('#dataVencimento').val();
            let url = "dia_util_iptu";
            let formData = new FormData();
            formData.append("date", data_vencimento);

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                    exibeMensagem();
                },
                success(response) {
                    load.fadeOut(200);

                    if(response == 't'){
                        if($('.registros_unicos').is(':checked')){
                            var data_vencimento = $('#dataVencimento').val();
                            var numpar_unica;

                            $('.registros_unicos').each(function(){
                                var dados = $(this).val().split("_");
                                $('#tipo_debito').val() == 34 ? numpar_unica = dados[2]: numpar_unica = dados[1];


                                $.post("{{ route('integracaoReciboDebitosIptu') }}",{
                                '_token':'{{ csrf_token() }}',
                                'matricula_inscricao':$('#matricula_inscricao').val(),
                                'tipo_pesquisa':$('#tipo_pesquisa').val(),
                                'tipo_debito':$('#tipo_debito').val(),
                                'numpre_unica':dados[0],
                                'numpar_unica':numpar_unica,
                                'd_data_vencimento':data_vencimento  },function(data){
                                    if(data == ''){
                                        // alert("Não foi possível emitir um boleto à partir deste número de arrecadação.");
                                        $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
                                        $('#footer-modal').find('.modal-body').html('<p>A data de vencimento não pode ser anterior ao dia de hoje.</p>')
                                        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                                        
                                        $('#footer-modal').modal("show");

                                    }
                                });
                            });
                        }

                        if($('.registros_normais').is(':checked') || $('.inicial_foro').is(':checked') || $('.iptu_desconto').is(':checked')){

                            var data_vencimento = $('#dataVencimento').val();
                            var checkbox = $('input:checkbox[name^=registros_normais]:checked');
                            var radio = $('input:radio[name^=registros_normais]:checked');
                            if(checkbox.length > 0 || radio.length > 0){
                                if(data_vencimento != ""){
                                    let compare = compareDates(data_vencimento);
                                    if(compare == true){
                                        $('#formulario').unbind('submit').submit();
                                    } else {
                                        $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
                                        $('#footer-modal').find('.modal-body').html('<p>A data de vencimento não pode ser anterior ao dia de hoje.</p>')
                                        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                                        
                                        $('#footer-modal').modal("show");
                                        // alert("A data de vencimento não pode ser anterior ao dia de hoje.");
                                    }
                                } else {
                                    // alert("Por favor, preencha uma data de vencimento.");
                                    $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
                                    $('#footer-modal').find('.modal-body').html('<p>Por favor, preencha uma data de vencimento.</p>')
                                    $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                                    
                                    $('#footer-modal').modal("show");
                                }
                            }
                        }
                    } else {
                        $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
                        $('#footer-modal').find('.modal-body').html('<p>Data informada para o pagamento não é um dia útil</p>')
                        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                        
                        $('#footer-modal').modal("show");

                        // alert('Data informada para o pagamento não é um dia útil');
                        return false;
                    }
                },
                error(erro) {
                    load.fadeOut(200);

                    $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
                    $('#footer-modal').find('.modal-body').html('<p>Não foi possível gerar o boleto.  Por favor tente novamente em alguns instantes.</p>')
                    $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
    
                    $('#footer-modal').modal("show");   
                }
            });

            if(!$('.registros_unicos').is(':checked') && !$('.registros_normais').is(':checked') && !$('.inicial_foro').is(':checked') && !$('.iptu_desconto').is(':checked')){
                // alert("Por favor, selecione uma arrecadação.");
                $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
                $('#footer-modal').find('.modal-body').html('<p>Por favor, selecione uma arrecadação.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')

                $('#footer-modal').modal("show");
                
            }

        });
    });

    function exibeMensagem(){
        $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
        $('#footer-modal').find('.modal-body').html('<p>Prezado Contribuinte, favor efetuar o pagamento após duas horas da emissão da guia para que conste o registro do boleto nos bancos conveniados.</p>')
        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')

        $('#footer-modal').modal("show");
    }

/**********Fim**************/
