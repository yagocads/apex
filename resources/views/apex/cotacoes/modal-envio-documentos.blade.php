<!-- The Modal -->
<div class="modal fade" id="modal-envio-documentos-cotacao">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">
                    {{__('Envio de Documentos')}}
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-row">
                            <div class="col-lg-5 form-group">
                                <label for="descricao_documento">{{__('Descrição do Documento')}}:</label>
                                <input type="text" name="descricao_documento" id="descricao_documento"
                                    class="form-control form-control-sm" />
                            </div>

                            <div class="col-lg-5 form-group mt-2">
                                <label class="btn btn-primary" style="margin-top: 18px !important;">
                                    <i class="fa fa-image"></i> {{__('Escolher Arquivo')}} <input type="file" style="display: none;" name="documento" id="documento">
                                </label>
                                <label id="mensagem_arquivo_cotacao">{{__('Nenhum Arquivo Selecionado')}}</label>
                            </div>

                            <div class="col-lg-12">
                                <button class="btn btn-primary btn-enviar-documento">
                                    {{__('Adicionar Documento')}}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 mb-3 mt-3">
                        <hr />

                        <h5><i class="fa fa-file"></i> {{__('Documentos Adicionados')}}:</h5>
                    </div>

                    <div class="col-lg-12">
                        <div class="conteudo_documentacao">
                            @include('apex.cotacoes.tabela-documentos-enviados')
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <button class="btn btn-success btn-salvar-documentos">{{__('Salvar')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#documento').change(function() {
         $('#mensagem_arquivo_cotacao').html('<b>{{__("Arquivo Selecionado")}}:</b> ' + $(this).val());
    });
</script>
