<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100">
        <thead>
            <tr>
                <th>{{__('Código')}}</th>
                <th>{{__('Categoria')}}</th>
                <th>{{__('Nome')}}</th>
                <th>{{__('Descrição')}}</th>
                <th>{{__('Unidade')}}</th>
            </tr>
        </thead>
        <tbody>
            @if (!empty($produtos))
                @foreach($produtos as $produto)
                    <tr>
                        <td>{{ $produto->codigo }}</td>
                        <td>{{ $produto->categoria }}</td>
                        <td>{{ $produto->nome }}</td>
                        <td>{{ $produto->descricao }}</td>
                        <td>{{ $produto->unidade }}</td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
