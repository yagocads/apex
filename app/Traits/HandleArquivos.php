<?php

namespace App\Traits;

use App\Support\ArquivoSupport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait HandleArquivos
{
    public function enviarDocumento(Request $request)
    {
        if (isset($request->verificarDescricaoArquivoJaIncluidoNaSessao)) {
            $verificarDescricaoArquivoJaIncluidoNaSessao = false;
        } else {
            $verificarDescricaoArquivoJaIncluidoNaSessao = true;
        }

        if (ArquivoSupport::verificarSeArquivoJaExisteNaSessao(
            $request->nomeSessao,
            $request->descricao,
            $verificarDescricaoArquivoJaIncluidoNaSessao
        )) {
            return response()->json([
                "error" => "O documento {$request->descricao} já foi enviado, caso deseje alterar o documento, por favor o remova-o primeiro."
            ]);
        }

        if ($this->enviarArquivo($request)) {
            return view($request->caminhoTabela);
        } else {
            return response()->json([
                "error" => "Houve um problema ao enviar o arquivo para o servidor, entre em contato com a administração."
            ]);
        }
    }

    private function enviarArquivo(Request $request): bool
    {
        $user = Auth::user();
        $caminho = remove_character_document($user->cpf) . '_' . str_slug($request->descricao) . '_' . uniqid() . '.' . $request->arquivo->extension();
        $arquivoSupport = new ArquivoSupport($request->pastaServidor, $caminho, $request->servidor);

        if ($arquivoSupport->enviarArquivoParaServidor($request->arquivo)) {

            $arquivoSupport->salvarArquivoNaSessao($request->nomeSessao, (object) [
                "descricao" => $request->descricao,
                "arquivo" => $request->arquivo->getClientOriginalName(),
                "responsavel" => $user->name,
                "data" => date("d/m/Y"),
                "caminho" => $caminho
            ]);

            return true;
        }

        return false;
    }

    public function removerDocumento(Request $request)
    {
        $arquivoSupport = new ArquivoSupport($request->pastaServidor, $request->caminho, $request->servidor);

        if ($arquivoSupport->removerArquivoDoServidor($request->caminho)) {
            $arquivoSupport->removerArquivoNaSessao($request->nomeSessao, $request->descricao);

            return view($request->caminhoTabela);
        }

        return response()->json(["error" => "O documento não existe no servidor."]);
    }

    public function verificarDocumentosExigidos(Request $request)
    {
        return ArquivoSupport::verificarArquivosExigidosNaSessao($request->nomeSessao, $request->documentosExigidos);
    }
}
