@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <i class="fa {{$icone}}"></i> 
                            @isset($titulo)
                            {{ $titulo }}
                            @endisset
                        </h3>
                    </div>
                    <br>
                    <br>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="alert alert-danger">
                                    <p><strong>Atenção!</strong> 
                                        @isset($mensagem)
                                            {!! $mensagem !!}
                                        @endisset
                                    </p>
                                    <p>
                                        @isset($codigo)
                                            <b>Código:</b> 
                                            {{ $codigo }}
                                        @endisset
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="{{ isset($rota)? route($rota, ['guia' => get_guia_services(), 'aba' => $retorno] ) : URL::previous() }}">
                                    <button type="button" class="btn btn-primary mt-3" id="autenticar">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </button>
                                    <a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection
