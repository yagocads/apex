<form method="POST" action="{{ route('acompanhamento') }}">
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <div class="form-row">
                <div class="col-lg-6 form-group">
                    <label for="chamado">Nº do Protocolo:</label>
                    <input type="text" name="chamado" id="chamado"
                        class="form-control form-control-sm" autofocus onkeypress='return onlyNumber(event)' maxlength="10" />
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="processos_busca">Serviços Solicitados:</label>
                    <select id="processos_busca" class="form-control form-control-sm">
                        <option value="">Selecione</option>
                        @foreach ($registros as $key => $registro)
                            <option value="{{$registro->view}}">{{$registro->servico}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <button class="btn btn-primary btn-sm btn-pesquisar"><i class="fa fa-search"></i> Pesquisar</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('#chamado').keypress(function(e){
            if(e.which == 13){
                $('.btn-pesquisar').click();
            }
        });

        $(".btn-pesquisar").click(function(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            let load = $(".ajax_load");
            let chamado = $("#chamado").val();
            let processos_busca = $("#processos_busca").val();

            let formData = new FormData();
            formData.append("chamado", chamado);
            formData.append("processos_busca", processos_busca);
            
            let url = "{{ route('acompanhamento') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    if (response == '') {
                        alert('Processo não encontrado!')
                        return false;
                    } else {
                        $("#conteudo").html(response);
                        efeito_ancora('#processos_abertos');
                    }
                }
            });
        });
    });
</script>

