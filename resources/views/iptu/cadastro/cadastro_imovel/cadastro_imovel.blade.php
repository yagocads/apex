
<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-home mt-3 mb-3"></i> Dados do Imóvel:</h5>

        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="possui_matricula">Você tem o número da matrícula do Imóvel? <i class="fa fa-question-circle-o" title="Número de IPTU"></i> <span class="text-danger">*</span></label>
                <select name="possui_matricula" id="possui_matricula" 
                    class="form-control form-control-sm">
                    <option></option>
                    <option value="SIM">SIM</option>
                    <option value="NÃO">NÃO</option>
                </select>
            </div>
            <div class="col-lg-6 form-group">
                <label for="formaPagamento">Forma de Pagamento:</label>
                <select name="formaPagamento" id="formaPagamento" 
                    class="form-control form-control-sm">
                    <option></option>
                    <option value="Cota Única">Cota Única</option>
                    <option value="Parcelado">Parcelado</option>
                </select>
            </div>
        </div>

        <section  class="d-none" id="dadosImovel">
            <div class="form-row" >
                <div class="col-lg-6 form-group">
                    <label for="matricula_imovel">Matricula do Imóvel:</label>
                    <input type="text" name="matricula_imovel" id="matricula_imovel" 
                        class="form-control form-control-sm mask-matricula_imovel" />
                </div>

            </div>
        </section>
    

        <section id="enderecoImovel" class="d-none">
            <div class="form-row">
                <div class="col-lg-12">
                    <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Referência do imóvel:</h5>
                    <div class="form-row">
                        <div class="col-lg-4 form-group">
                            <label for="i_cep">CEP:</label>
                            <input type="text" name="i_cep" id="i_cep" onChange="pesquisaCepImovel(this.value);" 
                                class="form-control form-control-sm mask-cep {{ ($errors->has('i_cep') ? 'is-invalid' : '') }}"
                                    value="{{ old('i_cep') }}"
                            />
                            @if ($errors->has('i_cep'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('i_cep') }}
                                </div>
                            @endif
                        </div>
                    
                        <div class="col-lg-8 form-group">
                            <label for="i_endereco">Endereço:</label>
                            <input type="text" name="i_endereco" id="i_endereco" 
                                class="form-control form-control-sm {{ ($errors->has('i_endereco') ? 'is-invalid' : '') }}"
                                maxlength="100" 
                                value="{{ old('i_endereco') }}"
                                readonly
                            />
                            @if ($errors->has('i_endereco'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('i_endereco') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col-lg-2 form-group">
                            <label for="i_numero">Número:</label>
                            <input type="text" name="i_numero" id="i_numero" 
                                class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('i_numero') ? 'is-invalid' : '') }}"
                                value="{{ old('i_numero') }}"
                                />
                                @if ($errors->has('i_numero'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('i_numero') }}
                                    </div>
                                @endif
                        </div>
                    
                        <div class="col-lg-6 form-group">
                            <label for="i_complemento">Complemento:</label>
                            <input type="text" name="i_complemento" id="i_complemento" 
                                class="form-control form-control-sm {{ ($errors->has('i_complemento') ? 'is-invalid' : '') }}"
                                maxlength="50" 
                                value="{{ old('i_complemento') }}"
                                />
                                @if ($errors->has('i_complemento'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('i_complemento') }}
                                    </div>
                                @endif
                        </div>
                    
                        <div class="col-lg-4 form-group">
                            <label for="i_bairro">Bairro:</label>
                            <input type="text" name="i_bairro" id="i_bairro" 
                                class="form-control form-control-sm {{ ($errors->has('i_bairro') ? 'is-invalid' : '') }}" readonly="true"
                                maxlength="40" 
                                value="{{ old('i_bairro') }}"
                                />
            
                                @if ($errors->has('i_bairro'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('i_bairro') }}
                                    </div>
                                @endif
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col-lg-6 form-group">
                            <label for="i_municipio">Município:</label>
                            <input type="text" name="i_municipio" id="i_municipio" 
                                class="form-control form-control-sm {{ ($errors->has('i_municipio') ? 'is-invalid' : '') }}" readonly="true"
                                maxlength="40" 
                                value="{{ old('i_municipio') }}"
                                />
            
                                @if ($errors->has('i_municipio'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('i_municipio') }}
                                    </div>
                                @endif
                        </div>
            
                        <div class="col-lg-6 form-group">
                            <label for="i_estado">Estado:</label>
                            <select name="i_estado" id="i_estado" class="form-control form-control-sm {{ ($errors->has('i_estado') ? 'is-invalid' : '') }}" 
                                readonly="true">
                                <option></option>
            
                                @foreach($estados->geonames as $estado)
                                    <option value="{{ $estado->adminCodes1->ISO3166_2 }}" 
                                    {{ old('i_estado') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}
                                    >
                                        {{ $estado->name }} 
                                    </option>
                                @endforeach
                            </select>
            
                            @if ($errors->has('i_estado'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('i_estado') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-lg-4 form-group">
                            <label for="i_nome_loteamento">Nome do Loteamento:</label>
                            <input type="text" name="i_nome_loteamento" id="i_nome_loteamento" 
                                class="form-control form-control-sm {{ ($errors->has('i_nome_loteamento') ? 'is-invalid' : '') }}" 
                                maxlength="25" 
                                value="{{ old('i_nome_loteamento') }}"
                                />
            
                                @if ($errors->has('i_nome_loteamento'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('i_nome_loteamento') }}
                                    </div>
                                @endif
                        </div>
            
                        <div class="col-lg-4 form-group">
                            <label for="i_quadra">Quadra:</label>
                            <input type="text" name="i_quadra" id="i_quadra" 
                                class="form-control form-control-sm {{ ($errors->has('i_quadra') ? 'is-invalid' : '') }}" 
                                maxlength="9" 
                                value="{{ old('i_quadra') }}"
                                />

                            @if ($errors->has('i_quadra'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('i_quadra') }}
                                </div>
                            @endif
                        </div>
            
                        <div class="col-lg-4 form-group">
                            <label for="i_lote">Lote:</label>
                            <input type="text" name="i_lote" id="i_lote" 
                                class="form-control form-control-sm {{ ($errors->has('i_lote') ? 'is-invalid' : '') }}" 
                                maxlength="10" 
                                value="{{ old('i_lote') }}"
                                />

                            @if ($errors->has('i_lote'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('i_lote') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="i_referencia">Ponto de Referência:</label>
                            <input type="text" name="i_referencia" id="i_referencia" 
                                class="form-control form-control-sm {{ ($errors->has('i_referencia') ? 'is-invalid' : '') }}" 
                                maxlength="50" 
                                value="{{ old('i_referencia') }}"
                                />
            
                                @if ($errors->has('i_referencia'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('i_referencia') }}
                                    </div>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <div class="form-row">
            <div class="col-lg-12 form-group">
                <button class="btn btn-primary btn-enviar-dados-imovel"
                    data-nome_input="documento_referencia_imovel" 
                    data-descricao=""
                    data-nome_sessao="documentos_imovel"
                    data-conteudo_documentos="conteudo_documentos_imovel"
                    data-id_tabela="tabela_documentos_imovel"
                    data-pasta="cadastro_imovel">
                    Incluir Imóvel <i class="fa fa-send"></i>
                </button>
            </div>
        </div> 
    </div>

    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Imóveis Enviados:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 8Mb.
                </div>
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="tipo_documento">Tipo de Documento:</label>
                <select name="tipo_documento" id="tipo_documento" 
                    class="form-control form-control-sm">
        
                    <option></option>
                    <option>IPTU</option>
                    <option>Contrato de compra e venda</option>
                    <option>Promessa de compra e venda</option>
                    <option>Escritura pública ou particular</option>
                    <option>Documento de doação</option>
                    <option>Conta de luz em nome do possuidor com o endereço do imóvel em questão</option>
                </select>
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="documento_referencia_imovel"><i class="fa fa-paperclip"></i> Documento opcional para referência do imóvel. Ex. Iptu de anos anteriores, Promessa de Compra e venda.</label>
                <input type="file" name="documento_referencia_imovel" 
                    id="documento_referencia_imovel" />
            </div>
        </div>
        
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_documentos_imovel">
            @include('iptu.cadastro.cadastro_imovel.tabela_documentos')
        </div>
    </div>
</div>