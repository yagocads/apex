@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">

                @if ($vencimentoIPTU == 1)
                    <div class="alert alert-info">
                        <p>Este cadastro é uma iniciativa da Prefeitura Municipal de Maricá para minimizar a exposição dos idosos ao Covid-19.</p>
                        <p>O idoso que se cadastrar receberá seu IPTU em casa, antecipadamente, o qual será entregue pela equipe da Prefeitura e não pelos Correios.</p>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h4>
                            <i class="fa fa-home"></i> 
                            {{ $vencimentoIPTU == 0 ? 'Cadastrar/Atualizar CGM' : 'IPTU Solicitar Recebimento em Casa - Idoso' }}
                        </h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('cgm_validarCpf') }}" id="infoCpf">
                            @csrf

                            <input id="vencimentoIPTU" type="hidden" name="vencimentoIPTU" value="{{ isset($vencimentoIPTU) ? $vencimentoIPTU : 0}}" />
                            
                            <div class="form-group">
                                <label for="cgm_cpf">{{ $vencimentoIPTU == 0 ? 'Digite seu CPF/CNPJ' : 'Digite seu CPF' }}</label>
                                
                                <input type="text" name="cgm_cpf" id="cgm_cpf" 
                                    class="form-control form-control-sm col-lg-6 cpfOuCnpj 
                                    {{ $errors->has('cgm_cpf') ? 'is-invalid' : '' }}" maxlength="20" required="true" />
                                
                                @if ($errors->has('pat_cpf'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('cgm_cpf') }}
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                @if ($vencimentoIPTU == 0)
                                    <a href="{{ route('servicos', [1, "alteracao-cadastral"]) }}" class="btn btn-sm btn-primary mt-3"><i class="fa fa-arrow-left"></i> Voltar</a>
                                @else 
                                    <a href="{{ route('servicos', [1, "imovel"]) }}" class="btn btn-sm btn-primary mt-3"><i class="fa fa-arrow-left"></i> Voltar</a>
                                @endif
                            
                                <button type="submit" id="validar_CPF" class="btn btn-sm btn-success mt-3">
                                    {{ $vencimentoIPTU == 0 ? 'Validar CPF/CNPJ' : 'Validar CPF' }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#validar_CPF").prop('disabled', true);
   
    function onReCaptchaTimeOut() {
        $("#validar_CPF").prop('disabled', true);
    }

    function onReCaptchaSuccess() {
        $("#validar_CPF").prop('disabled', false);
    }

    $('#cgm_cpf').change(function() {

        if ($("#cgm_cpf").val().length <= 14) {
            if(!validarCPF($("#cgm_cpf").val())) {
                alert("CPF Inválido!");
                $("#cgm_cpf").val("");
                $("#cgm_cpf").focus();
                return false;
            }
        } else{
            if(!validarCNPJ($("#cgm_cpf").val())) {
                alert("CNPJ Inválido!");
                $("#cgm_cpf").val("");
                $("#cgm_cpf").focus();
                return false;
            }
        }
    });

</script>

@endsection