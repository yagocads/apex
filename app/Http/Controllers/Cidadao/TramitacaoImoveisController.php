<?php

namespace App\Http\Controllers\Cidadao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TramitacaoImoveisRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\imoveis\TramitacaoImoveisDadosProcesso;
use App\Support\CidadaoSupport;
use App\Support\ImovelSupport;
use App\Support\ArquivoSupport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class TramitacaoImoveisController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $cidadaoSupport = new CidadaoSupport();
        $documentoCpfOuCnpj = remove_character_document(Auth::user()->cpf);

        return view("imovel.tramitacao_imoveis.tramitacao_imoveis", [
            'dadosCgm' => (object) array_merge(
                (array) $cidadaoSupport->getDadosCGM($documentoCpfOuCnpj), 
                (array) $cidadaoSupport->getEnderecoCGM($documentoCpfOuCnpj)
            ),
            'dadosProcessos' => TramitacaoImoveisDadosProcesso::all()
        ]);
    }

    public function salvar(TramitacaoImoveisRequest $request)
    {
        if ($this->verificarSeTramitacaoJaExiste($request->matricula_imovel, $request->categoria_processo)) {
            return redirect()
                ->route("tramitacao-processos-legalizacao-imoveis")
                ->with('tramitacaoJaExiste', '<b>Atenção!</b> Já existe um processo para esta matrícula de imóvel. 
                    Procure a Secretaria de Urbanismo para maiores informações.');
        }

        $idTramitacao = DB::connection('integracao')
            ->table('lecom_tramitacao')
            ->insertGetId(
                [
                    "nome_requerente" => $request->nome_requerente,
                    "identidade_requerente" => $request->identidade_requerente,
                    "orgao_emissor_requerente" => $request->orgao_emissor_requerente,
                    "data_emissao_requerente" => $request->data_emissao_requerente,
                    "endereco_requerente" => $request->endereco_requerente,
                    "telefone_requerente" => $request->telefone_requerente,
                    "cpf_cnpj_requerente" => $request->cpf_cnpj_requerente,
                    "email_requerente" => $request->email_requerente,
                    "tipo_requerente" => $request->tipo_requerente,
                    
                    "local_abertura" => $request->local_abertura,
                    "requisitante" => $request->requisitante,
                    "secretaria_requisitante" => $request->secretaria_requisitante,
                    "categoria_processo" => $request->categoria_processo,
                    "objeto_processo" => $request->objeto_processo,
                    "enviar_processo_para" => $request->enviar_processo_para,
                    "despacho" => $request->despacho,

                    "matricula_imovel" => $request->matricula_imovel,
                    "setor_imovel" => $request->setor_imovel,
                    "quadra_imovel" => $request->quadra_imovel,
                    "lote_imovel" => $request->lote_imovel
                ]
            );

        $this->enviarArquivosTramitacao($idTramitacao, $request->cpf_cnpj_requerente);

        return redirect()
            ->route("tramitacao-processos-legalizacao-imoveis")
            ->with('success', ['title' => 'Processo de Legalização de Imóvel solicitado com sucesso!']);
    }

    private function verificarSeTramitacaoJaExiste($matriculaImovel, $categoriaProcesso): int
    {
        return DB::connection('integracao')
            ->table('lecom_tramitacao')
            ->where('matricula_imovel', '=', $matriculaImovel)
            ->where('categoria_processo', '=', $categoriaProcesso)
            ->count();
    }

    private function enviarArquivosTramitacao($idTramitacao, $documentoCpfOuCnpj)
    {
        if (session()->has('documentacao')) {
            foreach(session('documentacao') as $dados) {
                DB::connection('integracao')
                    ->table('lecom_tramitacao_arquivos')
                    ->insert([
                        'id_tramitacao' => $idTramitacao,
                        'cpf_cnpj' => $documentoCpfOuCnpj,
                        'descricao' => $dados->descricao,
                        'documento' => $dados->caminho
                    ]);
            }

            session()->forget("documentacao");
        }
    }

    public function dadosImovelItbiJson(Request $request)
    { 
        $imovelSupport = new ImovelSupport();

        return response()->json(
            $imovelSupport->getInfoPropertieITBI($request->matriculaImovel)
        );
    }

    public function dadosProcessoJson(Request $request)
    {   
        if (isset($request->forgetSession)) {
            session()->forget("documentacao");
        }
    
        return response()->json(
            TramitacaoImoveisDadosProcesso::where(
                "categoria_processo", "=", $request->categoriaProcesso
            )->first()
        );
    }

    public function enviarDocumentoParaServidorEsalvarNaSessao(Request $request)
    {
        $documentoCpfOuCnpj = remove_character_document(Auth::user()->cpf);
        $extensao = $request->arquivo->extension();

        $caminho = $documentoCpfOuCnpj . "_" . str_slug($request->descricao) . "_" . uniqid() . "." . $extensao;

        $arquivoSupport = new ArquivoSupport($request->pastaServidor, $caminho, $request->servidor);

        if (ArquivoSupport::verificarSeArquivoJaExisteNaSessao($request->nomeSessao, $request->descricao)) {
            return response()->json([
                "error" => "O documento {$request->descricao} já foi enviado, caso deseje alterar o documento, por favor o remova-o primeiro."
            ]);
        }

        if ($arquivoSupport->enviarArquivoParaServidor($request->arquivo)) {

            $arquivoSupport->salvarArquivoNaSessao($request->nomeSessao, (object) [
                "descricao" => $request->descricao,
                "arquivo" => $request->arquivo->getClientOriginalName(),
                "responsavel" => Auth::user()->name,
                "data" => date("d/m/Y"),
                "caminho" => $caminho
            ]);
        }

        if ($request->nomeSessao == "documentacao") {
            return $this->renderTabelaDocumentacao();
        } else if ($request->nomeSessao == "documentos_exigencias") {
            return $this->renderTabelaDocumentosExigencia();
        }
    }

    public function removerDocumentoDoServidorEsessao(Request $request)
    {
        $arquivoSupport = new ArquivoSupport($request->pastaServidor, $request->caminho, $request->servidor);

        if ($arquivoSupport->removerArquivoDoServidor($request->caminho)) {
            $arquivoSupport->removerArquivoNaSessao($request->nomeSessao, $request->descricao);

            if ($request->nomeSessao == "documentacao") {
                return $this->renderTabelaDocumentacao();
            } else if ($request->nomeSessao == "documentos_exigencias") {
                return $this->renderTabelaDocumentosExigencia();
            }
        }

        return response()->json(["error" => "O documento não existe no servidor."]);
    }

    public function renderTabelaDocumentacao()
    {
        return view("imovel.tramitacao_imoveis.documentacao.tabela_documentacao");
    }

    public function renderTabelaDocumentosExigencia()
    {
        return view("imovel.tramitacao_imoveis.exigencias.tabela_documentos");
    }
    
    public function verificarDocumentosExigidos(Request $request)
    {
        return ArquivoSupport::verificarArquivosExigidosNaSessao("documentacao", $request->documentosExigidos);
    }

    public function exigencias($idChamado)
    {
        $notificacoes = DB::connection('integracao')
            ->table('lecom_tramitacao_notificacao')
            ->where('processo', '=', $idChamado)
            ->orderBy('ciclo', 'DESC')
            ->first();

        if (!empty($notificacoes->documento_prefeitura)) {
            $arquivo = Storage::disk('ftp')
            ->get('tramitacao_notificacao/' . $notificacoes->documento_prefeitura);

            Storage::disk('public')->put('tramitacao_notificacao/' . $notificacoes->documento_prefeitura, $arquivo);
        }

        return view("imovel.tramitacao_imoveis.exigencias.exigencias", [
            'idChamado' => $idChamado,
            'exigencia' => $notificacoes->exigencia,
            'documentoPrefeitura' => $notificacoes->documento_prefeitura
        ]);
    }

    public function downloadArquivoPrefeitura($documentoPrefeitura) 
    {
        return Storage::download(
            'public/tramitacao_notificacao/' . $documentoPrefeitura, 
            'exigencias-' . $documentoPrefeitura
        );
    }

    public function enviarExigencia(Request $request): void
    {
        $processoLecom = DB::connection('lecom')
            ->table('v_consulta_servico_sim')
            ->where('Chamado', '=', $request->idChamado)
            ->first();

        $idTramitacao = DB::connection('integracao')
            ->table('lecom_tramitacao')
            ->where('chamado_lecom', '=', $processoLecom->Chamado)
            ->pluck('id')
            ->first();

        $exigencia = new stdClass();
        $exigencia->id_tramitacao = $idTramitacao;
        $exigencia->processo = $processoLecom->Chamado;
        $exigencia->cpf_cnpj = $processoLecom->cpf;
        $exigencia->ciclo = $processoLecom->Ciclo;
        $exigencia->exigencia = $request->exigencia;
        $exigencia->cumprimento_exigencia = $request->cumprimentoExigencia;
        $exigencia->data = date('Y-m-d H:i:s');

        $idExigencia = $this->salvarExigencia($exigencia);
        
        $this->salvarDocumentacaoExigencias($idExigencia, $processoLecom->cpf, $processoLecom->Chamado);
        session()->forget('documentos_exigencias');
    }

    private function salvarExigencia(object $exigencia): int
    {
        $idExigencia = DB::connection('integracao')
            ->table('lecom_tramitacao_exigencias')
            ->insertGetId((array) $exigencia);

        return $idExigencia;
    }

    private function salvarDocumentacaoExigencias($idExigencia, $cpfOuCnpj, $processo)
    {   
        if (session()->has('documentos_exigencias')) {
            foreach(session('documentos_exigencias') as $documento) {

                DB::connection('integracao')
                    ->table('lecom_tramitacao_exigencias_arquivos')
                    ->insert([
                        "id_exigencia" => $idExigencia,
                        "processo" => $processo,
                        "cpf_cnpj" => $cpfOuCnpj,
                        "descricao" => $documento->descricao,
                        "documento" => $documento->caminho,
                        "data" => date('Y-m-d H:i:s')
                    ]);
            }
        }
    }
}