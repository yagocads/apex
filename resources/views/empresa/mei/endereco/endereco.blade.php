
<div class="row">
    <div class="col-lg-12">
        <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Endereço:</h5>
        
        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="cep_empresa">CEP: <span class="text-danger">*</span></label>
                <input type="text" name="cep_empresa" id="cep_empresa" onChange="pesquisaCepEmpresa(this.value);" 
                    class="form-control form-control-sm mask-cep {{ ($errors->has('cep_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('cep_empresa') }}" />

                    @if ($errors->has('cep_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cep_empresa') }}
                        </div>
                    @endif
            </div>
                    
            <div class="col-lg-8 form-group">
                <label for="endereco_empresa">Endereço: <span class="text-danger">*</span></label>
                <input type="text" name="endereco_empresa" id="endereco_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('endereco_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('endereco_empresa') }}" maxlength="100" readonly/>

                    @if ($errors->has('endereco_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('endereco_empresa') }}
                        </div>
                    @endif
            </div>

        </div>
        
        <div class="form-row">
            <div class="col-lg-2 form-group">
                <label for="numero_empresa">Número: <span class="text-danger">*</span></label>
                <input type="text" name="numero_empresa" id="numero_empresa" 
                    class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('numero_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('numero_empresa') }}" />

                    @if ($errors->has('numero_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('numero_empresa') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-6 form-group">
                <label for="complemento_empresa">Complemento:</label>
                <input type="text" name="complemento_empresa" id="complemento_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('complemento_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('complemento_empresa') }}" maxlength="50" />

                    @if ($errors->has('complemento_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('complemento_empresa') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-4 form-group">
                <label for="bairro_empresa">Bairro: <span class="text-danger">*</span></label>
                <input type="text" name="bairro_empresa" id="bairro_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('bairro_empresa') ? 'is-invalid' : '') }}" readonly="true"
                    value="{{ old('bairro_empresa') }}" maxlength="40" />

                    @if ($errors->has('bairro_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('bairro_empresa') }}
                        </div>
                    @endif
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="municipio_empresa">Município: <span class="text-danger">*</span></label>
                <input type="text" name="municipio_empresa" id="municipio_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('municipio_empresa') ? 'is-invalid' : '') }}" readonly="true"
                    value="{{ old('municipio_empresa') }}" maxlength="40" />

                    @if ($errors->has('municipio_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('municipio_empresa') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="estado_empresa">Estado: <span class="text-danger">*</span></label>
                <select name="estado_empresa" id="estado_empresa" class="form-control form-control-sm {{ ($errors->has('estado_empresa') ? 'is-invalid' : '') }}" 
                    readonly="true">
                    <option></option>

                    @foreach($estados->geonames as $estado)
                        <option value="{{ $estado->adminCodes1->ISO3166_2 }}" {{ old('estado_empresa') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}>
                            {{ $estado->name }}
                        </option>
                    @endforeach
                </select>

                @if ($errors->has('estado_empresa'))
                    <div class="invalid-feedback">
                        {{ $errors->first('estado_empresa') }}
                    </div>
                @endif
            </div>

            {{-- <div class="col-lg-2 form-group">
                <label for="iptu_empresa">IPTU:</label>
                <input type="text" name="iptu_empresa" id="iptu_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('iptu_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('iptu_empresa') }}" maxlength="6" />

                    @if ($errors->has('iptu_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('iptu_empresa') }}
                        </div>
                    @endif
            </div> --}}
        </div>

        <div class="form-row">
            <div class="col-lg-8 form-group">
                <label for="pr_empresa">Ponto de referência</label>
                <input type="text" name="pr_empresa" id="pr_empresa" maxlength="100"
                    class="form-control form-control-sm {{ ($errors->has('pr_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('pr_empresa') }}" />

                    @if ($errors->has('pr_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('pr_empresa') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="ii_empresa">Inscrição Imobiliária <span class="text-danger">*</span></label>
                <input type="text" name="ii_empresa" id="ii_empresa" maxlength="10" 
                    class="form-control form-control-sm {{ ($errors->has('ii_empresa') ? 'is-invalid' : '') }}"
                    value="{{ old('ii_empresa') }}" />

                    @if ($errors->has('ii_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('ii_empresa') }}
                        </div>
                    @endif
            </div>
        </div>

    </div>

</div>