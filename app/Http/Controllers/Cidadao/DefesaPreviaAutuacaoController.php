<?php

namespace App\Http\Controllers\Cidadao;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\DefesaPreviaAutuacaoRequest;
use App\Support\CidadaoSupport;
use App\Traits\HandleArquivos;
use Illuminate\Support\Facades\DB;

class DefesaPreviaAutuacaoController extends Controller
{
    use HandleArquivos;
    
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $cidadaoSupport = new CidadaoSupport();
        $documentoCpfOuCnpj = remove_character_document(Auth::user()->cpf);

        return view('cidadao.transito.defesa_previa_autuacao.home', [
            'dadosEcidade' => (object) array_merge(
                (array) $cidadaoSupport->getDadosCGM($documentoCpfOuCnpj),
                (array) $cidadaoSupport->getEnderecoCGM($documentoCpfOuCnpj)
            ),
        ]);
    }

    public function salvar(DefesaPreviaAutuacaoRequest $request)
    {
        if ($this->verificarSolicitacaoJaEmProcessamento($request->numero_auto)) {
            return redirect()
                ->route('defesa-previa-autuacao')
                ->withInput()
                ->with('solicitacaoJaEmProcessamento', "Atenção: Já existe uma solicitação em processamento para esse número de auto. ( {$request->numero_auto} )");
        }

        $idDefesaPrevia = DB::connection('integracao')
            ->table('lecom_defesa_previa_autuacao')
            ->insertGetId(
                [
                    'responsavel_preenchimento' => $request->responsavel_preenchimento,
                    'nome_requerente' => $request->nome_requerente,
                    'cpf_cnpj_requerente' => $request->cpf_cnpj_requerente,
                    'data_nasc_requerente' => $request->data_nasc_requerente,
                    'cnh_requerente' => $request->cnh_requerente,
                    'identidade_requerente' => $request->identidade_requerente,
                    'orgao_exp_requerente' => $request->orgao_exp_requerente,
                    'nacionalidade_requerente' => $request->nacionalidade_requerente,
                    'naturalidade_requerente' => $request->naturalidade_requerente,
                    'telefone_requerente' => $request->telefone_requerente,
                    'celular_requerente' => $request->celular_requerente,
                    'email_requerente' => $request->email_requerente,

                    'cep' => $request->cep,
                    'endereco' => $request->endereco,
                    'numero' => $request->numero,
                    'complemento' => $request->complemento,
                    'bairro' => $request->bairro,
                    'municipio' => $request->municipio,
                    'estado' => $request->estado,

                    'notificacao_nao_recebida' => isset($request->notificacao_nao_recebida) ? 1 : 0,
                    'numero_auto' => $request->numero_auto,
                    'data_recebimento_notificacao' => isset($request->data_recebimento_notificacao) ? $request->data_recebimento_notificacao : null,
                    'data_entrada_requerimento' => $request->data_entrada_requerimento,
                    'tipo_multa' => $request->tipo_multa,
                    'logradouro_infracao' => $request->logradouro_infracao,
                    'observacoes' => $request->observacoes,
                    'confirmacao_de_informacoes' => $request->confirmacao_de_informacoes
                ]
            );

        $this->salvarArquivosNaBaseDeDados($idDefesaPrevia, $request->cpf_cnpj_requerente);

        return redirect()
            ->route('defesa-previa-autuacao')
            ->with('success', ['title' => 'Processo de Defesa Prévia de Autuação solicitado com sucesso!']);
    }

    private function verificarSolicitacaoJaEmProcessamento($numeroDoAuto): int
    {
        return DB::connection('integracao')
            ->table('lecom_defesa_previa_autuacao')
            ->where('numero_auto', '=', $numeroDoAuto)
            ->count();
    }

    private function salvarArquivosNaBaseDeDados($idDefesaPrevia, $cpf_cnpj_requerente)
    {
        if (session()->has('documentacao_defesa_previa_autuacao')) {
            foreach (session('documentacao_defesa_previa_autuacao') as $dados) {
                DB::connection('integracao')
                    ->table('lecom_defesa_previa_autuacao_arquivos')
                    ->insert([
                        'id_defesa_previa' => $idDefesaPrevia,
                        'cpf_cnpj' => $cpf_cnpj_requerente,
                        'descricao' => $dados->descricao,
                        'documento' => $dados->caminho
                    ]);
            }

            session()->forget("documentacao_defesa_previa_autuacao");
        }
    }

    public function removerDocumentosDaSessao()
    {
        session()->forget("documentacao_defesa_previa_autuacao");
        return view('cidadao.transito.defesa_previa_autuacao.tabela_documentacao');
    }
}
