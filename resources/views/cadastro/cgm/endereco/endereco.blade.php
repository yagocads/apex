
<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Endereço do Requerente:</h5>
        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="cep">CEP:</label>
                <input type="text" name="cep" id="cep" onChange="pesquisaCep(this.value);" 
                    class="form-control form-control-sm mask-cep {{ ($errors->has('cep') ? 'is-invalid' : '') }}"
                    @auth
                        value="{{ $endereco->iCep }}" readonly
                    @else
                        value="{{ old('cep') }}"
                    @endauth
                />

                    @if ($errors->has('cep'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cep') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-8 form-group">
                <label for="endereco">Endereço:</label>
                <input type="text" name="endereco" id="endereco" 
                    class="form-control form-control-sm {{ ($errors->has('endereco') ? 'is-invalid' : '') }}"
                    maxlength="100" 
                    @auth
                        value="{{ convert_accentuation($endereco->sRua) }}" readonly
                    @else
                        value="{{ old('endereco') }}"
                    @endauth
                />

                    @if ($errors->has('endereco'))
                        <div class="invalid-feedback">
                            {{ $errors->first('endereco') }}
                        </div>
                    @endif
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-2 form-group">
                <label for="numero">Número:</label>
                <input type="text" name="numero" id="numero" 
                    class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('numero') ? 'is-invalid' : '') }}"
                    @auth
                        value="{{ $endereco->sNumeroLocal }}" readonly
                    @else
                        value="{{ old('numero') }}"
                    @endauth
                    />

                    @if ($errors->has('numero'))
                        <div class="invalid-feedback">
                            {{ $errors->first('numero') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-6 form-group">
                <label for="complemento">Complemento:</label>
                <input type="text" name="complemento" id="complemento" 
                    class="form-control form-control-sm {{ ($errors->has('complemento') ? 'is-invalid' : '') }}"
                    maxlength="50" 
                    @auth
                        value="{{ convert_accentuation($endereco->sComplemento) }}" readonly
                    @else
                        value="{{ old('complemento') }}"
                    @endauth
                    />

                    @if ($errors->has('complemento'))
                        <div class="invalid-feedback">
                            {{ $errors->first('complemento') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-4 form-group">
                <label for="bairro">Bairro:</label>
                <input type="text" name="bairro" id="bairro" 
                    class="form-control form-control-sm {{ ($errors->has('bairro') ? 'is-invalid' : '') }}" readonly="true"
                    maxlength="40" 
                    @auth
                        value="{{ convert_accentuation($endereco->sBairro) }}" readonly
                    @else
                        value="{{ old('bairro') }}"
                    @endauth
                    />

                    @if ($errors->has('bairro'))
                        <div class="invalid-feedback">
                            {{ $errors->first('bairro') }}
                        </div>
                    @endif
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="municipio">Município:</label>
                <input type="text" name="municipio" id="municipio" 
                    class="form-control form-control-sm {{ ($errors->has('municipio') ? 'is-invalid' : '') }}" readonly="true"
                    maxlength="40" 
                    @auth
                        value="{{ convert_accentuation($endereco->sMunicipio) }}" readonly
                    @else
                        value="{{ old('municipio') }}"
                    @endauth
                    />

                    @if ($errors->has('municipio'))
                        <div class="invalid-feedback">
                            {{ $errors->first('municipio') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="estado">Estado:</label>
                <select name="estado" id="estado" class="form-control form-control-sm {{ ($errors->has('estado') ? 'is-invalid' : '') }}" 
                    readonly="true">
                    <option></option>

                    @foreach($estados->geonames as $estado)
                        <option value="{{ $estado->adminCodes1->ISO3166_2 }}" 
                            @auth
                                {{ $endereco->sUf == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}
                            @else
                                {{ old('estado') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}
                            @endauth
                        >
                            {{ $estado->name }} 
                        </option>
                    @endforeach
                </select>

                @if ($errors->has('estado'))
                    <div class="invalid-feedback">
                        {{ $errors->first('estado') }}
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-row">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>

            <div class="col-lg-6 form-group">
                <label for="documento_comprovante_residencia"><i class="fa fa-paperclip"></i> Anexar Comprovante de Residência:</label>
                <input type="file" name="documento_comprovante_residencia" 
                    id="documento_comprovante_residencia" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_comprovante_residencia" 
                    data-descricao="Comprovante de Endereço"
                    data-nome_sessao="documento_endereco"
                    data-conteudo_documentos="conteudo_endereco_documento"
                    data-id_tabela="documento_endereco"
                    data-pasta="endereco">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
        </div>    
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_endereco_documento">
            @include('cadastro.cgm.endereco.tabela_documentos')
        </div>
    </div>
</div>