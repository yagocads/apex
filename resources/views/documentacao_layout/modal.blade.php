<div class="card">
    <div class="card-header">
        <h5>Modal</h5>
    </div>

    <div class="card-body">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#default-modal">
            Abrir Modal
        </button>   
        
        <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#error">
            Abrir Modal de Erro
        </button>   
        
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#divida-modal">
            Abrir Modal de Sucesso
        </button>    -->
        
        @include('modals.modal_default')
        @include('modals.modal_error')
        @include('modals.modal_msg')

        <script>
            $('#default-modal').find('.modal-title').html('<h3>Título do modal</h3>');
            $('#default-modal').find('.modal-body').html('<p>Conteúdo do modal...</p>')
        </script>
    </div>
</div>