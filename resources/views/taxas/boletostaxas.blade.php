@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <h2 class="mb-4"><i class="fa fa-file-pdf-o"></i> Boletos de Taxas</h2>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="alert alert-info" role="alert">
                        <h4><i class="fa fa-volume-up"></i> Formulário destinado a emissão de boletos de taxas</h4>
                    <p>
                        Serviço/item que possibilita ao cidadão a retirada/impressão de boleto da Taxa de requerimento para fins de abertura de processo administrativo.  <br>
                        Processos que NÃO necessitam de pagamento de Taxa de requerimento:
                        Revisão de Área Construída (Imóvel); 
                        Baixa de Débitos/Baixa por Pagamento; 
                        Compensação; 
                        Impugnação de ITBI/Suspensão de débitos para emissão de ITBI e Ouvidoria; 
                        Requerimento Funcional (RH); 
                        e Elogios, Denúncias e Reclamações.
                    </p>
                    <p>
                        <strong>Atenção:</strong> Prezado Contribuinte, favor efetuar o pagamento após duas horas da emissão da guia para que conste o registro do boleto nos bancos conveniados.
                    </p>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card mb-3">
                <div class="card-body">
                    <form class="form-horizontal topo-pagina" method="POST" action="{{ route('emitirboletotaxa') }}" id="formEmitirBoleto">
                    @csrf
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="cpf">Serviços</label>

                            <select id="grupo" class="form-control form-control-sm " name="grupo">
                                <option value="">Escolha o serviço para o qual deseja emitir o boleto</option>
                                @foreach ($registros->aGrupoTaxa as $registro)
                                <option value="{{ $registro->k06_taxagrupo}}|{{$registro->k06_tipo}}">
                                    {{ utf8_encode(urldecode(iconv('UTF-8', 'ISO-8859-1', $registro->k06_descr)))}}
                                </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="row formrow">
                        <div class="col-lg-3 form-group">
                            <label for="inscricao" id="tituloPesquisa">Inscrição</label>

                            <input id="inscricao" type="text" name="inscricao" value="" class="form-control form-control-sm" readonly>
                        </div>
                        <div class="col-lg-2 form-group">
                            <br>
                            <button type="button" class="btn btn-primary mt-1" id="pesquisar" disabled>
                                Pesquisar
                            </button>
                        </div>
                    </div>

                    <div class="row formrow">
                        <div class="col-lg-12 form-group">
                            <label for="profissao">Nome</label>

                            <input id="profissao" type="text" name="profissao" value="" class="form-control form-control-sm" readonly>
                        </div>
                    </div>

                    <div class="row formrow">
                        <div class="col-lg-4 form-group">
                            <label for="data">Data de Vencimento</label>

                            <input id="data" type="text" name="data" value="" class="form-control form-control-sm" readonly>
                        </div>
                        {{-- Separação --}}
                        <div class="col-lg-6 form-group">
                            <label for="">
                                &nbsp;
                            </label>
                        </div>
                        <div class="col-lg-2 form-group">
                            <label for="cidadao">Cidadão</label>

                            <input id="cidadao" type="text" name="cidadao" value="" class="form-control form-control-sm" readonly>
                        </div>
                    </div>

                    <div class="row formrow">
                        <div class="col-lg-4 form-group">
                            <label for="vlTaxa">Valor da Taxa</label>

                            <input id="vlTaxa" type="text" name="vlTaxa" value="" class="form-control form-control-sm" readonly>
                        </div>
                    </div>

                    <div class="row formrow">
                        <div class="col-lg-6 form-group">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'tributos']) }}" class="btn btn-primary mt-3">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>

                            <button type="button" class="btn btn-success mt-3" id="gerarBoleto" data-toggle="modal" data-target="#footer-modal">
                                {{ __('Gerar Boleto') }}
                            </button>
                        </div>
                    </div>

            </form>
        </div>
    </div>
    <br>
    <br>

    @include('modals.modal_footer')

</section>


@endsection


@section('post-script')
<script type="text/javascript">
    jQuery(document).ready(function(){

        $('#footer-modal').find('.modal-title').html('<h3>Emitir Boleto</h3>');
        $('#footer-modal').find('.modal-body').html('<p>Prezado Contribuinte, favor efetuar o pagamento após duas horas da emissão da guia para que conste o registro do boleto nos bancos conveniados.</p>')
        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
        
        // Spinner
        let load = $(".ajax_load");

        var tablePesquisaTaxa = $('#pesquisaTaxa').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            },
            "bLengthChange": false,
            "ordering": false,
            "info": false,
            "pageLength": 5,
            "scrollY": 250,
            "searching": false,
        });

        var pesquisaInscricaoTaxaTbl = $('#pesquisaInscricaoTaxaTbl').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            },
            "bLengthChange" : false,
            "pageLength"    : 5,
            "searching"     : false,
            "ordering"      : false,
            "scrollY"       : 250,
            "info"          : false,
        });

        $("#pesquisar").click(function(){
            var str = $("#grupo").val();
            var res = str.split("|");
            var cod = '{{ str_replace("/", "_", Auth::user()->cpf)  }}';
            // res[1]="i";

            if(res[1]=="m"){
                var url = 'taxasconsultamatricula/' + cod;
                if($("#inscricao").val() != ""){
                    url = url + '/' + $("#inscricao").val();
                }

                $.get(url, function (resposta) {
                    var obj = jQuery.parseJSON(resposta);
                    if (obj.iStatus == 1) {
                        // $("#profissao").val(obj.nome);
                        // $("#cidadao").val(obj.numcgm);
                        // $("#inscricao").val(obj.numcgm);
                        $('#pesquisaTaxa').dataTable().fnClearTable();
                        $.each(obj.aMatriculasCgmEncontradas, function(i, item) {
                            // alert(item.matricula);
                            tablePesquisaTaxa.row.add( [
                                item.matricula,
                                item.tipo,
                                unescape(item.bairro).replace(/[+]/g, " "),
                                unescape(item.logradouro).replace(/[+]/g, " "),
                                item.numero,
                                unescape(item.complemento).replace(/[+]/g, " "),
                                unescape(item.planta + " / " +
                                item.quadra + " / " +
                                item.lote).replace(/[+]/g, " "),
                                '<button type="button" class="btn e_wiggle" onClick="javascript:selectionaPesquisa(' + item.matricula + ');"  >Selecionar</button>'
                            ] ).draw( false );
                        });
                        $('#pesquisa').css('display', 'none' );
                        $("#pesquisa").modal('toggle');
                        $('#pesquisa').css('display', 'block' );
                        tablePesquisaTaxa.columns.adjust().draw();
                    }
                    else {
                        alert("Não foi possivel consultar a matrícula dos seus imóveis\n\nCódigo: TAX_003");
                        return false;
                    }
                });
            }
            else if(res[1]=="i"){

                // var url = 'taxasconsultainscricao/97323250706';
                // var url = 'taxasconsultainscricao/{{ Auth::user()->cpf }}';
                var url = 'taxasconsultainscricao/' + cod ;
                $.get(url, function (resposta) {
                    var obj = jQuery.parseJSON(resposta);
                    if (obj.iStatus == 1) {
                        $('#pesquisaInscricaoTaxaTbl').dataTable().fnClearTable();
                        $.each(obj.aInscBase, function(i, item) {
                            pesquisaInscricaoTaxaTbl.row.add( [
                                item.q02_inscr,
                                unescape(item.z01_nome).replace(/[+]/g, " "),
                                unescape(item.z01_ender).replace(/[+]/g, " "),
                                item.z01_numero,
                                unescape(item.z01_compl).replace(/[+]/g, " "),
                                '<button type="button" class="btn e_wiggle" onClick="javascript:selectionaPesquisaInscricao(' + item.q02_inscr + ');"  >Selecionar</button>'
                            ] ).draw( true );
                        });
                        $('#pesquisaInscricaoTaxa').css('display', 'none' );
                        $("#pesquisaInscricaoTaxa").modal('toggle');
                        $('#pesquisaInscricaoTaxa').css('display', 'block' );
                        pesquisaInscricaoTaxaTbl.columns.adjust().draw();
                    }
                    else {
                        alert("Não foi possivel consultar a inscrição dos seus imóveis\n\nCódigo: TAX_004");
                        return false;
                    }
                });


            }
        });

        $("#msg_ok").click(function(){
            $("#formEmitirBoleto").submit();
        });


        $("#grupo").change(function(){
            $("#profissao").val("");
            $("#cidadao").val("");
            $("#inscricao").val("");
            $("#vlTaxa").val("");
            $("#data").val("");
            $("#historico").val("");
            $("#historico").prop('readonly', true);

            var str = $("#grupo").val();
            var res = str.split("|");

            // res[1]="i";

            if(res[1]=="c"){
                $("#inscricao").prop('readonly', true);
                $("#pesquisar").prop('disabled', true);
                $("#tituloPesquisa").html('CGM');
                $("#pesquisar").removeClass("btn-blue");
                $("#inscricao").val('');

                var cod = '{{ str_replace("/", "_", Auth::user()->cpf)  }}';
                var url = 'taxasconsultacgm/' + cod;
    
                $.get(url, function (resposta) {
                    var obj = jQuery.parseJSON(resposta);
                    if (obj.iStatus == 1) {
                        $("#profissao").val(obj.nome);
                        $("#cidadao").val(obj.numcgm);
                        $("#inscricao").val(obj.numcgm);

                        var url2 = 'taxasconsultavalor/'+ res[1] +'/'+ obj.numcgm+'/'+res[0];
    
                        $.get(url2, function (resposta2) {
                            var obj2 = jQuery.parseJSON(resposta2);

                            if (obj2.iStatus == 1) {

                                $("#vlTaxa").val(obj2.fTotalTaxa.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                                // $("#historico").val();
                                $("#data").val(dataAtualFormatada());
                                $("#historico").val(obj2.sMessage)
                                $("#historico").prop('readonly', true);
                                // $("#historico").setfocus();

                            }
                            else {
                                alert("Não foi possivel gerar o boleto para o serviço escolhido\n\nCódigo: TAX_002");
                                return false;
                            }
                        });
                    }
                    else {
                        alert("Não foi possivel gerar nenhum boleto para o serviço escolhido\n\nCódigo: TAX_005");
                        return false;
                    }
                });
            }
            else if(res[1]=="m"){
                //****************** Matrícula
                $("#inscricao").val('');
                $("#inscricao").prop('readonly', false);
                $("#pesquisar").prop('disabled', false);
                $("#tituloPesquisa").html('Matrícula');
                $("#pesquisar").addClass("btn-blue");
                $("#inscricao").focus();
            }
            else if(res[1]=="i"){
                $("#inscricao").val('');
                $("#inscricao").prop('readonly', false);
                $("#tituloPesquisa").html('Inscrição');
                $("#pesquisar").prop('disabled', false);
                $("#pesquisar").addClass("btn-blue");            
                $("#inscricao").focus();
            }

        });
        


    });
    function dataAtualFormatada(){
        var data = new Date()
        data.setDate(data.getDate() + 1);
        var dia  = data.getDate().toString(),
            diaF = (dia.length == 1) ? '0'+dia : dia,
            mes  = (data.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF = (mes.length == 1) ? '0'+mes : mes,
            anoF = data.getFullYear();
        return diaF+"/"+mesF+"/"+anoF;
    }

    function selectionaPesquisa(matricula){
        $("#inscricao").val(matricula);

        var str = $("#grupo").val();
        var res = str.split("|");

        var url2 = 'taxasconsultavalor/'+ res[1] +'/'+ matricula +'/'+res[0];
    
        $.get(url2, function (resposta2) {
            var obj2 = jQuery.parseJSON(resposta2);
            console.log(obj2);

            if (obj2.iStatus == 1) {
                
                $("#profissao").val(obj2.sNomeCgm);
                $("#vlTaxa").val(obj2.fTotalTaxa.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                // $("#historico").val();
                $("#data").val(dataAtualFormatada());
                $("#cidadao").val(obj2.iNumCgm)
                $("#historico").val(obj2.sMessage)
                $("#historico").prop('readonly', true);
                $("#historico").setfocus();                

            }
            else {
                alert("Não foi possivel gerar nenhum boleto para o serviço escolhido\n\nCódigo: TAX_006");
                return false;
            }
        });
        $("#pesquisa").modal('toggle');
    }

    function selectionaPesquisaInscricao(inscricao){
        $("#inscricao").val(inscricao);

        var str = $("#grupo").val();
        var res = str.split("|");

        var url2 = 'taxasconsultavalor/'+ res[1] +'/'+ inscricao +'/'+res[0];
    
        $.get(url2, function (resposta2) {
            var obj2 = jQuery.parseJSON(resposta2);
            console.log(obj2);

            if (obj2.iStatus == 1) {
                
                $("#profissao").val((obj2.sNomeCgm));
                $("#vlTaxa").val(obj2.fTotalTaxa.toLocaleString('pt-br', {minimumFractionDigits: 2, maximumFractionDigits: 2}));
                // $("#historico").val();
                $("#data").val(dataAtualFormatada());
                $("#cidadao").val(obj2.iNumCgm)
                //$("#historico").val((obj2.sMessage))
                $("#historico").prop('readonly', false);
                $("#historico").setfocus();
            }
            else {
                alert("Não foi possivel gerar nenhum boleto para o serviço escolhido\n\nCódigo: TAX_006");
                return false;
            }
        });

        $("#pesquisaInscricaoTaxa").modal('toggle');
    }
</script>
@endsection