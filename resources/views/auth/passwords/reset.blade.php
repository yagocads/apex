@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-lock"></i> Trocar a Senha</h4>
                    </div>

                    <div class="card-body">

                        @if (session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('resetPassword') }}" id="form_register" autocomplete="off">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label for="password">Digite sua nova senha:</label>
                                <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ $password ?? old('password') }}" required="true" />

                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="passwordconfirm">Confirme sua senha:</label>
                                <input type="password" name="passwordconfirm" id="passwordconfirm" class="form-control {{ $errors->has('passwordconfirm') ? ' is-invalid' : '' }}" required="true" />

                                @if ($errors->has('passwordconfirm'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('passwordconfirm') }}
                                    </div>
                                @endif
                            </div>

                            {!! $showRecaptcha !!}

                            <div class="span12 aligncenter">
                                <button type="submit" class="btn btn-danger" id="register_submit" disabled>
                                    {{ __('Resetar a Senha') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
    <script type="text/javascript">
        $('.g-recaptcha').attr('data-callback', 'onReCaptchaSuccess');
        $('.g-recaptcha').attr('data-expired-callback', 'onReCaptchaTimeOut');

        function onReCaptchaTimeOut() {
            $("#register_submit").prop('disabled', true);
        }

        function onReCaptchaSuccess() {
            $("#register_submit").prop('disabled', false);
        }
    </script>
@endsection
