<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-home mt-3 mb-3"></i> Dados Mercantis:</h5>

        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="cnpj">CNPJ:</label>
                <input type="text" name="cnpj" id="cnpj" 
                    class="form-control form-control-sm mask-cnpj {{ ($errors->has('cnpj') ? 'is-invalid' : '') }}" 
                    value="{{ $cpfOuCnpj }}" readonly />

                    @if ($errors->has('cnpj'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cnpj') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="razao_social">Razão Social:</label>
                <input type="text" name="razao_social" id="razao_social" 
                    class="form-control form-control-sm {{ ($errors->has('razao_social') ? 'is-invalid' : '') }}" 
                    value="{{ old('razao_social') }}" maxlength="100" />

                    @if ($errors->has('razao_social'))
                        <div class="invalid-feedback">
                            {{ $errors->first('razao_social') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="nome_fantasia">Nome Fantasia:</label>
                <input type="text" name="nome_fantasia" id="nome_fantasia" 
                    class="form-control form-control-sm {{ ($errors->has('nome_fantasia') ? 'is-invalid' : '') }}" 
                    value="{{ old('nome_fantasia') }}" maxlength="100" />

                    @if ($errors->has('nome_fantasia'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_fantasia') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="natureza_juridica">Natureza Jurídica:</label>
                <input type="text" name="natureza_juridica" id="natureza_juridica" 
                    class="form-control form-control-sm {{ ($errors->has('natureza_juridica') ? 'is-invalid' : '') }}"
                    value="{{ old('natureza_juridica') }}" maxlength="40" />

                    @if ($errors->has('natureza_juridica'))
                        <div class="invalid-feedback">
                            {{ $errors->first('natureza_juridica') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="data_abertura_empresa">Data de Abertura:</label>
                <input type="date" name="data_abertura_empresa" id="data_abertura_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('data_abertura_empresa') ? 'is-invalid' : '') }}" 
                    value="{{ old('data_abertura_empresa') }}"/>

                    @if ($errors->has('data_abertura_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_abertura_empresa') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="inscricao_estadual">Inscrição Estadual:</label>
                <input type="text" name="inscricao_estadual" id="inscricao_estadual" 
                    class="form-control form-control-sm mask-inscricao_estadual {{ ($errors->has('inscricao_estadual') ? 'is-invalid' : '') }}" 
                    value="{{ old('inscricao_estadual') }}" />

                    @if ($errors->has('inscricao_estadual'))
                        <div class="invalid-feedback">
                            {{ $errors->first('inscricao_estadual') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>
            
            <div class="col-lg-6 form-group">
                <label for="documento_contrato_social"><i class="fa fa-paperclip"></i> Anexar Contrato Social:</label>
                <input type="file" name="documento_contrato_social" 
                    id="documento_contrato_social" />
            </div>

            <div class="col-lg-12 mb-3">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_contrato_social" 
                    data-descricao="Contrato Social"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>

            <div class="col-lg-6 form-group">
                <label for="documento_identididade_socio"><i class="fa fa-paperclip"></i> Anexar Documento de Identidade do Sócio:</label>
                <input type="file" name="documento_identididade_socio" 
                    id="documento_identididade_socio" />
            </div>

            <div class="col-lg-12 mb-3">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_identididade_socio" 
                    data-descricao="Documento de Identidade do Sócio"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>

            <div class="col-lg-6 form-group">
                <label for="documento_cpf_socio"><i class="fa fa-paperclip"></i> Anexar CPF do Sócio:</label>
                <input type="file" name="documento_cpf_socio" 
                    id="documento_cpf_socio" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_cpf_socio" 
                    data-descricao="CPF do Sócio"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
        </div>    
    </div>  
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_dados_mercantis">
            @include('cadastro.cgm.dados_mercantis.tabela_documentos')
        </div>
    </div>
</div>