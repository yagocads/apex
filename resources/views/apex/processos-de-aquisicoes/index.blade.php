@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('success'))
                <div class="alert alert-primary">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>{{__('Processos de aquisições')}}</h4>
                </div>

                <div class="card-body">
                    <div class="table-responsive py-1 pl-1 pr-1">
                        <table class="table table-striped table-bordered dataTable dt-responsive w-100">
                            <thead>
                                <tr>
                                    <th>{{__('Nº Processo')}}</th>
                                    <th>{{__('Modalidade')}}</th>
                                    <th>{{__('Objeto')}}</th>
                                    <th>{{__('Data Início')}}</th>
                                    <th>{{__('Data Final')}}</th>
                                    <th>{{__('Status')}}</th>
                                    @if ($fornecedor)
                                        <th>{{__('Participar')}}</th>
                                    @endif
                                    <th data-priority="1">{{__('Ações')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($processos as $processo)
                                    <tr>
                                        <td>{{ $processo->numero }}</td>
                                        <td>{{ $processo->modalidade }}</td>
                                        <td>{{ $processo->objeto }}</td>
                                        <td>
                                            {{
                                                !empty($processo->data_inicio) ?
                                                date('d/m/Y', strtotime($processo->data_inicio))
                                                : '-'
                                            }}
                                        </td>
                                        <td>
                                            {{
                                                !empty($processo->data_final) ?
                                                date('d/m/Y', strtotime($processo->data_final))
                                                : '-'
                                            }}
                                        </td>
                                        <td>{{ $processo->status }}</td>

                                        @if ($fornecedor && $processo->status !== 'Finalizado')
                                            <td class="text-center">
                                                <input type="checkbox" name="serNotificado" data-processo="{{ $processo->numero }}"
                                                    {{ !empty($notificacoes[$processo->numero]) && $notificacoes[$processo->numero] === 1 ? 'checked' : '' }} />
                                            </td>
                                        @elseif ($fornecedor && $processo->status === 'Finalizado')
                                            <td class="text-center">
                                                -
                                            </td>
                                        @endif

                                        <td class="text-center">
                                            @if ($fornecedor)
                                                <button class="btn btn-sm btn-primary btn-ofertas mb-1 w-100"
                                                    data-processo="{{ $processo->numero }}">
                                                    Ofertas
                                                </button>
                                            @endif

                                            <button class="btn btn-sm btn-primary btn-documentos mb-1 w-100"
                                                data-processo="{{ $processo->numero }}">
                                                Documentação
                                            </button>

                                            <button class="btn btn-sm btn-primary btn-comunicados w-100"
                                                data-processo="{{ $processo->numero }}">
                                                Comunicados
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('apex.processos-de-aquisicoes.modal-documentos')
    @include('apex.processos-de-aquisicoes.modal-comunicados')
    @include('apex.processos-de-aquisicoes.modal-ofertas')
    @include('modals.modal-exigencias-apex')
</div>

@endsection

@section('post-script')
    <script>
        $(function() {
            let load = $(".ajax_load");

            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });

            $('body').on('click', '.btn-ofertas', function() {
                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "carregar-ofertas",
                    method: "POST",
                    data: { numeroProcesso },
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-ofertas')
                            .find('.modal-header .numero-processo')
                            .html(numeroProcesso);

                        $('#modal-ofertas')
                            .find('.modal-body')
                            .html(response);

                        recalcularDataTable('tabela-ofertas');

                        $('#modal-ofertas').modal('show');
                    },
                });
            });

            $('body').on('click', '.btn-documentos', function() {
                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "carregar-documentos-processos-de-aquisicoes",
                    method: "POST",
                    data: { numeroProcesso },
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-documentos-processos-de-aquisicoes')
                            .find('.modal-header .numero-processo')
                            .html(numeroProcesso);

                        $('#modal-documentos-processos-de-aquisicoes')
                            .find('.modal-body')
                            .html(response);

                        $('#modal-documentos-processos-de-aquisicoes').modal('show');
                    },
                });
            });

            $('body').on('click', '.btn-comunicados', function() {
                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "carregar-comunicados",
                    method: "POST",
                    data: { numeroProcesso },
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-comunicados')
                            .find('.modal-header .numero-processo')
                            .html(numeroProcesso);

                        $('#modal-comunicados')
                            .find('.modal-body')
                            .html(response);

                        $('#modal-comunicados').modal('show');
                    },
                });
            });

            $('body').on('click', 'input[name=serNotificado]', function() {

                let numeroProcesso = $(this).data('processo');
                let checkSerNotificado = $(this).is(':checked');

                $('.btn-concordo-exigencias').attr('data-processo', numeroProcesso);
                $('.btn-concordo-exigencias').attr('data-notificar', checkSerNotificado);

                $('#modal-exigencias-apex').modal('show');
            });

            $('.btn-concordo-exigencias').on('click', function() {

                const exigencias = $('.exigencias');
                let existeAlgumTermoNaoCheck = false;

                let checkSerNotificado = $(this).data('notificar');

                exigencias.each(function() {
                    let exigenciaCheck = $(this).is(':checked');

                    if (!exigenciaCheck) {
                        existeAlgumTermoNaoCheck = true;
                    }
                });

                if (existeAlgumTermoNaoCheck) {
                    alert('Ateção: Para continuar precisa concordar com todos os termos.');
                    existeAlgumTermoNaoCheck = false;
                } else {
                    let numeroProcesso = $(this).data('processo');

                    $.ajax({
                        url: "notificar-usuario-processos-de-aquisicoes",
                        method: "POST",
                        data: { numeroProcesso, checkSerNotificado },
                        beforeSend: function () {
                            load.fadeIn(200).css("display", "flex");
                        },
                        success(response) {
                            load.fadeOut(200);

                            if (response) {
                                location.reload();
                            }
                        },
                    });
                }
            });

            $('.btn-cancelar-exigencias').on('click', function() {
                location.reload();
            });
        });
    </script>
@endsection
