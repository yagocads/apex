<div class="row">
    @if($processo->Fase == "Cumprir Exigências")
        <div class="col-lg-12">
            <div class="alert alert-info">
                <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;
                <a href="{{ route('sanar_pendencias_iptu', ['chamado' => $processo->Chamado, 'ciclo' => $processo->Ciclo , 'fase' => $processo->Etapa]) }}" class="btn btn-blue">Sanar Pendências</a>
                <div class="col-lg-6">
                    Motivo: {{ $processo->motivo }}
                </div>
            </div>
        </div>
    @endif
    @if($processo->finalizado == "concluído" || $processo->finalizado == "cancelado")
        <div class="col-lg-12">
            <div class="alert alert-info">
                <strong>Atenção!</strong><br>
                Esta solicitação foi finalizada e teve como conclusão:<br>
                Conclusão: <strong>{{$processo->Fase}}</strong><br>
                @if($processo->motivo != "")
                    Motivo: {{$processo->motivo}}<br>
                @endif
            </div>
        </div>
    @endif
</div>
