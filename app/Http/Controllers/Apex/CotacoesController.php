<?php

namespace App\Http\Controllers\Apex;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Cotacao;
use App\Mail\EnvioEsclarecimentoCotacao;
use App\Models\Log;
use App\Traits\HandleArquivos;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CotacoesController extends Controller
{
    use HandleArquivos;

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        if ($fornecedor) {
            $cotacoes = DB::connection('lecom')
                ->table('v_apex_cotacao')
                ->select(
                    'processo_lecom_pai',
                    'processo_lecom',
                    'cnpj',
                    'razao_social',
                    'objeto',
                    'moedas',
                    'resultado',
                    'motivo'
                )
                ->distinct('processo_lecom')
                ->where('cnpj', $fornecedor->cnpj)
                ->get();

            if ($cotacoes) {
                foreach ($cotacoes as $cotacao) {
                    $cotacoesJaEnviadasPorNumeroProcesso = DB::connection('integracao')
                        ->table('lecom_apex_cotacoes_produtos_servicos')
                        ->where([
                            'id_fornecedor' => $fornecedor->id,
                            'processo_lecom' => $cotacao->processo_lecom
                        ])->count();

                    if ($cotacoesJaEnviadasPorNumeroProcesso) {
                        $cotacoesJaEnviadas[$cotacao->processo_lecom] = $cotacao->processo_lecom;
                    }

                    $existeNotificacaoComMesmoNumeroDoProcesso = DB::connection('integracao')
                        ->table('lecom_apex_notificacoes_cotacoes')
                        ->where([
                            'id_fornecedor' => $fornecedor->id,
                            'processo_lecom' => $cotacao->processo_lecom
                        ])->count();

                    if ($existeNotificacaoComMesmoNumeroDoProcesso) {
                        DB::connection('integracao')
                            ->table('lecom_apex_notificacoes_cotacoes')
                            ->where([
                                'id_fornecedor' => $fornecedor->id,
                                'processo_lecom' => $cotacao->processo_lecom
                            ])->update([
                                'resultado' => $cotacao->resultado,
                                'motivo' => $cotacao->motivo
                            ]);
                    } else {
                        DB::connection('integracao')
                            ->table('lecom_apex_notificacoes_cotacoes')
                            ->insert([
                                'id_fornecedor' => $fornecedor->id,
                                'processo_lecom' => $cotacao->processo_lecom,
                                'resultado' => $cotacao->resultado,
                                'motivo' => $cotacao->motivo
                            ]);
                    }
                }

                $notificacoesNaoLidas = DB::connection('integracao')
                    ->table('lecom_apex_notificacoes_cotacoes')
                    ->where([
                        'id_fornecedor' => $fornecedor->id,
                        'lida' => 0
                    ])->get();
            }
        }

        Log::criarLog(Auth::user()->cpf, 'Acesso a página Cotações');

        return view('apex.cotacoes.index', [
            'pagina' => "Cotação",
            'cotacoes' => !empty($cotacoes) ? $cotacoes : null,
            'fornecedor' => !empty($fornecedor) ? $fornecedor : null,
            'cotacoesJaEnviadas' => !empty($cotacoesJaEnviadas) ? $cotacoesJaEnviadas : null,
            'notificacoes' => !empty($notificacoesNaoLidas) ? $notificacoesNaoLidas : null
        ]);
    }

    public function carregarProdutos(Request $request)
    {
        $produtos = DB::connection('lecom')
            ->table('v_apex_cotacao')
            ->select(
                'processo_lecom_pai',
                'processo_lecom',
                'discriminacao_item',
                'quantidade',
                'unidade_fornecimento',
                'objeto',
                'moedas',
                'item'
            )->where([
                'processo_lecom' => $request->numeroProcesso,
                'resultado' => 'Cotação aberta'
            ])->get();

        $cotacoesJaEnviadasPorNumeroProcesso = DB::connection('integracao')
            ->table('lecom_apex_cotacoes_produtos_servicos')
            ->where([
                'id_fornecedor' => $request->idFornecedor,
                'processo_lecom' => $request->numeroProcesso,
                'cotacao_atual' => 1
            ])->get();

        if ($cotacoesJaEnviadasPorNumeroProcesso) {
            foreach ($cotacoesJaEnviadasPorNumeroProcesso as $cotacao) {
                $cotacoesComValores[$cotacao->item] = $cotacao->valor;
            }
        }

        return view('apex.cotacoes.produtos', [
            'pagina' => "Cotação",
            'produtos' => !empty($produtos) ? $produtos : null,
            'cotacoesComValores' => !empty($cotacoesComValores) ? $cotacoesComValores : null,
            'observacoes' => count($cotacoesJaEnviadasPorNumeroProcesso) ? $cotacoesJaEnviadasPorNumeroProcesso[0]->observacoes : []
        ]);
    }

    public function salvarCotacaoProdutos(Request $request)
    {
        $cadastrouCotacao = false;

        $processoComCotacaoAberta = DB::connection('lecom')
            ->table('v_apex_cotacao')
            ->where('processo_lecom', $request->numeroProcesso)
            ->where('resultado', 'Cotação aberta')
            ->count();

        if ($processoComCotacaoAberta) {

            DB::connection('integracao')
                ->table('lecom_apex_cotacoes_produtos_servicos')
                ->where([
                    'id_fornecedor' => $request->idFornecedor,
                    'processo_lecom' => $request->numeroProcesso,
                ])->update([
                    'cotacao_atual' => 0
                ]);

            $dataAtual = date('Y-m-d H:i:s');

            foreach ($request->cotacoes as $cotacao) {

                DB::connection('integracao')
                    ->table('lecom_apex_cotacoes_produtos_servicos')
                    ->insert([
                        'id_fornecedor' => $request->idFornecedor,
                        'processo_lecom_pai' => $cotacao['numeroProcessoPai'],
                        'processo_lecom' => $request->numeroProcesso,
                        'item' => $cotacao['item'],
                        'quantidade' => $cotacao['quantidade'],
                        'unidade' => $cotacao['unidade'],
                        'moeda' => $cotacao['moeda'],
                        'valor' => !empty($cotacao['valor']) ? str_price_american($cotacao['valor']) : '',
                        'observacoes' => $cotacao['observacoes'],
                        'cotacao_atual' => 1,
                        'data' => $dataAtual
                    ]);

                $cadastrouCotacao = true;
            }
        }

        if ($cadastrouCotacao) {
            $fornecedor = DB::connection('integracao')
                ->table('lecom_apex_fornecedores')
                ->where('id', $request->idFornecedor)
                ->first();

            if ($fornecedor) {
                Mail::to($fornecedor->email_representante)->send(new Cotacao($request->numeroProcesso));
            }
        }

        if (!empty($processoComCotacaoAberta)) {
            Log::criarLog(Auth::user()->cpf, 'Enviou cotação para o processo Nº ' . $request->numeroProcesso);

            session()->flash('success', 'Cotação enviada com sucesso!');
        }

        return response()->json(true);
    }

    public function salvarDocumentosNaBaseDeDados(Request $request)
    {
        $documentosJaSalvosNaBaseAnteriormente = DB::connection('integracao')
            ->table('lecom_apex_documentos_cotacoes')
            ->select('id', 'documento')
            ->where('processo_lecom', $request->numeroProcesso)
            ->where('status', '!=', 'exclusao')
            ->get();


        if (session()->has('documentacao_cotacoes')) {

            $documentosEnviados = [];

            foreach (session('documentacao_cotacoes') as $documento) {
                $documentosEnviados[] = $documento->caminho;
            }

            foreach ($documentosJaSalvosNaBaseAnteriormente as $documento) {

                if (!in_array($documento->documento, $documentosEnviados)) {

                    DB::connection('integracao')
                        ->table('lecom_apex_documentos_cotacoes')
                        ->where('id', $documento->id)
                        ->update([
                            'status' => 'exclusao',
                            'data_alteracao' => date('Y-m-d H:i:s')
                        ]);
                }
            }
        }

        if (session()->has('documentacao_cotacoes')) {
            foreach (session('documentacao_cotacoes') as $dados) {
                DB::connection('integracao')
                    ->table('lecom_apex_documentos_cotacoes')
                    ->updateOrInsert([
                        'processo_lecom' => $request->numeroProcesso,
                        'descricao' => $dados->descricao,
                        'documento' => $dados->caminho,
                        'status' => 'inclusao',
                        'data_alteracao' => date('Y-m-d H:i:s')
                    ]);
            }

            Log::criarLog(Auth::user()->cpf, 'Salvando documentos referente ao processo Nº ' . $request->numeroProcesso);

            session()->forget("documentacao_cotacoes");
            session()->flash('success', 'Documento salvo com sucesso!');
        }

        return response()->json(true);
    }

    public function carregarDocumentos(Request $request)
    {
        $documentos = DB::connection('integracao')
            ->table('lecom_apex_documentos_cotacoes')
            ->where([
                'processo_lecom' => $request->numeroProcesso,
                'status' => 'inclusao'
            ])->get();

        if ($documentos) {
            session()->forget('documentacao_cotacoes');

            foreach ($documentos as $documento) {
                session()->push('documentacao_cotacoes', (object) [
                    'descricao' => $documento->descricao,
                    'arquivo' => $documento->documento,
                    'caminho' => $documento->documento
                ]);
            }
        }

        return view('apex.cotacoes.tabela-documentos-enviados',['pagina' => "Cotação"]);
    }

    public function carregarNotificacoes()
    {

        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        if ($fornecedor) {
            $notificacoesNaoLidas = DB::connection('integracao')
                ->table('lecom_apex_notificacoes_cotacoes')
                ->where([
                    'id_fornecedor' => $fornecedor->id,
                    'lida' => 0
                ])->get();
        }

        Log::criarLog(Auth::user()->cpf, 'Acesso a notificações');

        return view('apex.cotacoes.notificacoes', [
            'pagina' => "Cotação",
            'notificacoes' => !empty($notificacoesNaoLidas) ? $notificacoesNaoLidas : []
        ]);
    }

    function marcarNotificacaoComoLida(Request $request)
    {
        $fornecedor = DB::connection('integracao')
            ->table('lecom_apex_fornecedores')
            ->where('cpf_responsavel', Auth::user()->cpf)
            ->where('status', '!=', 'exclusao')
            ->whereNull('data_exclusao')
            ->first();

        $notificacaoAlterada = DB::connection('integracao')
            ->table('lecom_apex_notificacoes_cotacoes')
            ->where([
                'id_fornecedor' => $fornecedor->id,
                'processo_lecom' => $request->numeroProcesso
            ])->update([
                'lida' => 1
            ]);

        if ($notificacaoAlterada) {
            Log::criarLog(Auth::user()->cpf, 'Marcou a notificação como lida referente ao processo Nº ' . $request->numeroProcesso);

            return $this->carregarNotificacoes();
        }
    }

    function enviarEsclarecimento(Request $request)
    {
        if (!empty($request->numeroProcessoPai) && !empty($request->numeroProcesso)) {

            DB::connection('integracao')
                ->table('lecom_apex_esclarecimentos')
                ->insert([
                    'id_fornecedor' => $request->idFornecedor,
                    'processo_lecom_pai' => $request->numeroProcessoPai,
                    'processo_lecom' => $request->numeroProcesso,
                    'esclarecimento' => $request->esclarecimento,
                    'data' => date('Y-m-d H:i:s')
                ]);

            Log::criarLog(Auth::user()->cpf, 'Esclarecimento referente a cotação do processo Nº ' . $request->numeroProcesso);

            Mail::to('fernanda.chamma@plano.inf.br')->send(new EnvioEsclarecimentoCotacao(
                $request->numeroProcesso,
                $request->esclarecimento
            ));

            session()->flash('success', 'Esclarecimento enviado com sucesso!');

            return response()->json(true);
        }

        return response()->json(false);
    }
}
