
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="documentacao_cotacoes">
        <thead>
            <tr>
                <th>{{__('Descrição')}}</th>
                <th>{{__('Documento')}}</th>
                <th data-priority="1">{{__('Ações')}}</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentacao_cotacoes"))
                @foreach(session("documentacao_cotacoes") as $dados)
                    <tr>
                        <td>{{ $dados->descricao }}</td>
                        <td>{{ $dados->arquivo }}</td>
                        <td class="text-center" width="10%">
                            <button
                                class="btn btn-sm btn-danger btn-remover-documento"
                                data-descricao="{{ $dados->descricao }}"
                                data-caminho="{{ $dados->caminho }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
