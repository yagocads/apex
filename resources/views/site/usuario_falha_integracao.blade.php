@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">

    <div class="alert alert-danger">
        <p>
            <strong>Atenção!</strong> Não foi possível verificar as suas 
            informações no "Cadastro Geral do Município" (CGM).
        </p>
        
        <p>Tente Novamente em alguns instantes.</p>
        <p><b>Código:</b> INT_1001</p>
    </div>
</div>

@endsection