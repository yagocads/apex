
<div class="solicitante_prosseguimento">
    <div class="row">
        <div class="col-lg-6" id="div_DadosEMpresa">
            <h5><i class="fa fa-user mt-3 mb-3"></i> Dados da Empresa:</h5>
            <input type="hidden" name="tipo_inscricao" id="tipo_inscricao" value="Inscriçao Municipal" />
            <input type="hidden" name="simples_nacional" class="form-check-input" value="Sim" /> 

            
            <div class="form-row" id="campoCNPJ">
                <div class="col-lg-6 form-group">
                    <label for="cnpj_empresa">CNPJ:</label>
                    <input type="text" name="cnpj_empresa" id="cnpj_empresa" 
                        class="form-control form-control-sm  mask-cnpj {{ ($errors->has('cnpj_empresa') ? 'is-invalid' : '') }}" 
                        value="{{ old('cnpj_empresa') }}" maxlength="100" />

                        @if ($errors->has('cnpj_empresa'))
                            <div class="invalid-feedback">
                                {{ $errors->first('cnpj_empresa') }}
                            </div>
                        @endif
                </div>
                <div class="col-lg-6 form-group" id="btnConsultarCNPJ" style="display: none;">
                    <label>&nbsp;</label><br>
                    <button class="btn btn-success btn-sm consultar-cnpj">Consultar CNPJ <i class="fa fa-send"></i></button>
                </div>
            </div>

            <div class="row mt-3" id="mensagemCnpjExistente" style="display: none;">
                <div class="col-lg-12">
                    <div class="alert alert-danger">
                        <b>ATENÇÃO:</b><br>
                        <p>
                            Este CNPJ já possui um processo de legalização perante a Prefeitura Municipal de Maricá, para mais informações compareça no SIM
                        </p>
                    </div>
                </div>
            </div>

            <div class="row mt-3" id="mensagemCnpjCadastrado" style="display: none;">
                <div class="col-lg-12">
                    <div class="alert alert-danger">
                        <b>ATENÇÃO:</b><br>
                        <p>
                            Este CNPJ já possui um processo de legalização solicitado no portal. Acomponhe o andamento da solicitação acessando a opção <a href="{{ route('acompanhamento') }}">Consulta</a>.
                        </p>
                    </div>
                </div>
            </div>

            <section id="camposCNPJ" style="display: none;">
                <div class="row mt-3">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <i class="fa fa-volume-up"></i> Preencha os campos do formulário
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-6 form-group">
                        <label for="possui_protocolo_viabilidade">Tem Protocolo de Viabilidade?  <span class="text-danger">*</span></label>
                        <select name="possui_protocolo_viabilidade" id="possui_protocolo_viabilidade" 
                            class="form-control form-control-sm {{ ($errors->has('possui_protocolo_viabilidade') ? 'is-invalid' : '') }}">
                            <option></option>
                            <option {{ old('possui_protocolo_viabilidade') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                            <option {{ old('possui_protocolo_viabilidade') == "Não" ? "selected" : "" }} value="Não">Não</option>
                        </select>

                            @if ($errors->has('possui_protocolo_viabilidade'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('possui_protocolo_viabilidade') }}
                                </div>
                            @endif
                    </div>

                    <div class="col-lg-6 form-group">
                        <label for="protocolo_viabilidade">Protocolo de Viabilidade:</label>
                        <input type="text" name="protocolo_viabilidade" id="protocolo_viabilidade" 
                            class="form-control form-control-sm {{ ($errors->has('protocolo_viabilidade') ? 'is-invalid' : '') }}" 
                            value="{{ old('protocolo_viabilidade') }}" maxlength="20" disabled />

                            @if ($errors->has('protocolo_viabilidade'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('protocolo_viabilidade') }}
                                </div>
                            @endif
                    </div>
                </div>

                <section id="questionarioViabilidade" style="display: none">

                    <div class="form-row">
                        <div class="col-lg-6 form-group">
                            <label for="horario_funcionamento">Horário de Funcionamento: <span class="text-danger">*</span></label>
                            <select name="horario_funcionamento" id="horario_funcionamento" 
                                class="form-control form-control-sm {{ ($errors->has('horario_funcionamento') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('horario_funcionamento') == "Horário comercial ( de 8H até 18H )" ? "selected" : "" }} value="Horário comercial ( de 8H até 18H )">Horário comercial ( de 8H até 18H )</option>
                                <option {{ old('horario_funcionamento') == "24 Horas" ? "selected" : "" }} value="24 Horas">24 Horas</option>
                                <option {{ old('horario_funcionamento') == "Outros" ? "selected" : "" }} value="Outros">Outros</option>
                            </select>
    
                                @if ($errors->has('horario_funcionamento'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('horario_funcionamento') }}
                                    </div>
                                @endif
                        </div>
    
                        <div class="col-lg-6 form-group">
                            <label for="outros_horarios">Outros Horários:</label>
                            <input type="text" name="outros_horarios" id="outros_horarios" 
                                class="form-control form-control-sm {{ ($errors->has('outros_horarios') ? 'is-invalid' : '') }}" 
                                value="{{ old('outros_horarios') }}" maxlength="50" disabled />
    
                                @if ($errors->has('outros_horarios'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('outros_horarios') }}
                                    </div>
                                @endif
                        </div>
                    </div>     

                    <div class="form-row">
                        <div class="col-lg-6 form-group">
                            <label for="anexo_residencia">Anexo a residência? <span class="text-danger">*</span></label>
                            <select name="anexo_residencia" id="anexo_residencia" 
                                class="form-control form-control-sm {{ ($errors->has('anexo_residencia') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('anexo_residencia') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                                <option {{ old('anexo_residencia') == "Não" ? "selected" : "" }} value="Não">Não</option>
                            </select>
    
                                @if ($errors->has('anexo_residencia'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('anexo_residencia') }}
                                    </div>
                                @endif
                        </div>

                        <div class="col-lg-6 form-group">
                            <label for="porte_empresa">Porte da Empresa: <span class="text-danger">*</span></label>
                            <select name="porte_empresa" id="porte_empresa" 
                                class="form-control form-control-sm {{ ($errors->has('porte_empresa') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('porte_empresa') == "ME" ? "selected" : "" }} value="ME">MICRO EMPRESA</option>
                                <option {{ old('porte_empresa') == "EPP" ? "selected" : "" }} value="EPP">EMPRESA DE PEQUENO PORTE</option>
                                <option {{ old('porte_empresa') == "NORMAL" ? "selected" : "" }} value="NORMAL">NORMAL</option>
                            </select>
    
                                @if ($errors->has('porte_empresa'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('porte_empresa') }}
                                    </div>
                                @endif
                        </div>
                    </div>                

                    <div class="form-row">
                        <div class="col-lg-6 form-group">
                            <label for="ponto_referencia">Local de ponto de referência (Escritório)? <span class="text-danger">*</span></label>
                            <select name="ponto_referencia" id="ponto_referencia" 
                                class="form-control form-control-sm {{ ($errors->has('ponto_referencia') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('ponto_referencia') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                                <option {{ old('ponto_referencia') == "Não" ? "selected" : "" }} value="Não">Não</option>
                            </select>
    
                                @if ($errors->has('ponto_referencia'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('ponto_referencia') }}
                                    </div>
                                @endif
                        </div>

                        <div class="col-lg-6 form-group">
                            <label for="tipo_uso">Tipo de uso do estabelecimento: <span class="text-danger">*</span></label>
                            <select name="tipo_uso" id="tipo_uso" 
                                class="form-control form-control-sm {{ ($errors->has('tipo_uso') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('tipo_uso') == "Comercial" ? "selected" : "" }} value="Comercial">Comercial</option>
                                <option {{ old('tipo_uso') == "Residencial" ? "selected" : "" }} value="Residencial">Residencial</option>
                                <option {{ old('tipo_uso') == "Misto" ? "selected" : "" }} value="Misto">Misto</option>
                            </select>
    
                                @if ($errors->has('tipo_uso'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('tipo_uso') }}
                                    </div>
                                @endif
                        </div>
                    </div>                
                
                    <div class="form-row">
                        <div class="col-lg-6 form-group">
                            <label for="outra_empresa">Existe outra empresa no local? <span class="text-danger">*</span></label>
                            <select name="outra_empresa" id="outra_empresa" 
                                class="form-control form-control-sm {{ ($errors->has('outra_empresa') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('outra_empresa') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                                <option {{ old('outra_empresa') == "Não" ? "selected" : "" }} value="Não">Não</option>
                            </select>
    
                                @if ($errors->has('outra_empresa'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('outra_empresa') }}
                                    </div>
                                @endif
                        </div>

                        <div class="col-lg-6 form-group">
                            <label for="atendimento_publico">Faz atendimento ao público? <span class="text-danger">*</span></label>
                            <select name="atendimento_publico" id="atendimento_publico" 
                                class="form-control form-control-sm {{ ($errors->has('atendimento_publico') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('atendimento_publico') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                                <option {{ old('atendimento_publico') == "Não" ? "selected" : "" }} value="Não">Não</option>
                            </select>
    
                                @if ($errors->has('atendimento_publico'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('atendimento_publico') }}
                                    </div>
                                @endif
                        </div>
                    </div>                
                
                    <div class="form-row">
                        <div class="col-lg-6 form-group">
                            <label for="autorizacao">Autorização do proprietário com fins comerciais? <span class="text-danger">*</span></label>
                            <select name="autorizacao" id="autorizacao" 
                                class="form-control form-control-sm {{ ($errors->has('autorizacao') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('autorizacao') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                                <option {{ old('autorizacao') == "Não" ? "selected" : "" }} value="Não">Não</option>
                            </select>
    
                                @if ($errors->has('autorizacao'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('autorizacao') }}
                                    </div>
                                @endif
                        </div>

                        <div class="col-lg-6 form-group">
                            <label for="separacao_fisica">Separação física das empresas? <span class="text-danger">*</span></label>
                            <select name="separacao_fisica" id="separacao_fisica" 
                                class="form-control form-control-sm {{ ($errors->has('separacao_fisica') ? 'is-invalid' : '') }}">
                                <option></option>
                                <option {{ old('separacao_fisica') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                                <option {{ old('separacao_fisica') == "Não" ? "selected" : "" }} value="Não">Não</option>
                            </select>
    
                                @if ($errors->has('separacao_fisica'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('separacao_fisica') }}
                                    </div>
                                @endif
                        </div>
                    </div>                
                
                </section>

                <div class="form-row">
                    <div class="col-lg-6 form-group">
                        <label for="possui_recurso_viabilidade">Tem Recurso de Viabilidade? <span class="text-danger">*</span></label>
                        <select name="possui_recurso_viabilidade" id="possui_recurso_viabilidade" 
                            class="form-control form-control-sm {{ ($errors->has('possui_recurso_viabilidade') ? 'is-invalid' : '') }}">
                            <option></option>
                            <option {{ old('possui_recurso_viabilidade') == "Sim" ? "selected" : "" }} value="Sim">Sim</option>
                            <option {{ old('possui_recurso_viabilidade') == "Não" ? "selected" : "" }} value="Não">Não</option>
                        </select>

                            @if ($errors->has('possui_recurso_viabilidade'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('possui_recurso_viabilidade') }}
                                </div>
                            @endif
                    </div>

                    <div class="col-lg-6 form-group">
                        <label for="recurso_viabilidade">Recurso de Viabilidade:</label>
                        <input type="text" name="recurso_viabilidade" id="recurso_viabilidade" 
                            class="form-control form-control-sm {{ ($errors->has('recurso_viabilidade') ? 'is-invalid' : '') }}" 
                            value="{{ old('recurso_viabilidade') }}" maxlength="20" disabled />

                            @if ($errors->has('recurso_viabilidade'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('recurso_viabilidade') }}
                                </div>
                            @endif
                    </div>
                </div>            

                <div class="form-row">
                    <div class="col-lg-6 form-group">
                        <label for="fator_tipo_juridico">Fator Tipo Jurídico: <span class="text-danger">*</span></label>
                        <select name="fator_tipo_juridico" id="fator_tipo_juridico" 
                            class="form-control form-control-sm {{ ($errors->has('fator_tipo_juridico') ? 'is-invalid' : '') }}">
                            <option></option>
                            @include("empresa.mei.empresa.tipos_juridico")
                        </select>

                            @if ($errors->has('fator_tipo_juridico'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('fator_tipo_juridico') }}
                                </div>
                            @endif
                    </div>

                    <div class="col-lg-6 form-group">
                        <label for="nire">NIRE / Número de Registro:</label>
                        <input type="text" name="nire" id="nire" 
                            class="form-control form-control-sm {{ ($errors->has('nire') ? 'is-invalid' : '') }}" 
                            value="{{ old('nire') }}" maxlength="45" />

                            @if ($errors->has('nire'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('nire') }}
                                </div>
                            @endif
                    </div>
                </div>            

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="razao_social">Razão Social: <span class="text-danger">*</span></label>
                        <input type="text" name="razao_social" id="razao_social" 
                            class="form-control form-control-sm {{ ($errors->has('razao_social') ? 'is-invalid' : '') }}" 
                            value="{{ old('razao_social') }}" maxlength="100" />

                            @if ($errors->has('razao_social'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('razao_social') }}
                                </div>
                            @endif
                    </div>
                </div>            

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="nome_fantasia">Nome Fantasia:</label>
                        <input type="text" name="nome_fantasia" id="nome_fantasia" 
                            class="form-control form-control-sm {{ ($errors->has('nome_fantasia') ? 'is-invalid' : '') }}" 
                            value="{{ old('nome_fantasia') }}" maxlength="100" />

                            @if ($errors->has('nome_fantasia'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('nome_fantasia') }}
                                </div>
                            @endif
                    </div>
                </div>   
            </section>         
        </div>
        
        <div class="col-lg-6" id="campos_documentos">
            <section id="arquivosCNPJ" style="display: none;">
                <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                        </div>
                    </div>
                </div>
                
                <section id="exibe_cnpj">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="documento_cartao_cnpj"><i class="fa fa-paperclip"></i> Anexar Cartão CNPJ: <span class="text-danger">*</span></label><br>
                            <input type="file" name="documento_cartao_cnpj" 
                                id="documento_cartao_cnpj" />
                        </div>
    
                        <div class="col-lg-12">
                            <button class="btn btn-primary btn-enviar-documento" 
                                data-nome_input="documento_cartao_cnpj" 
                                data-descricao="Cartão CNPJ"
                                data-nome_sessao="documentos_empresa"
                                data-conteudo_documentos="conteudo_documentos_empresa"
                                data-id_tabela="documentos_empresa"
                                data-pasta="empresa">
                                Enviar Documento <i class="fa fa-send"></i>
                            </button>
                        </div>
                    </div>  
                    
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <hr/>
                        </div>
                    </div>
                </section>

                <section  id="exibe_certificado">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="documento_certificado_micro"><i class="fa fa-paperclip"></i> Anexar Certificado de Microempreendedor: <span class="text-danger">*</span></label><br>
                            <input type="file" name="documento_certificado_micro" 
                                id="documento_certificado_micro" />
                        </div>
    
                        <div class="col-lg-12">
                            <button class="btn btn-primary btn-enviar-documento" 
                                data-nome_input="documento_certificado_micro" 
                                data-descricao="Certificado de Microempreendedor"
                                data-nome_sessao="documentos_empresa"
                                data-conteudo_documentos="conteudo_documentos_empresa"
                                data-id_tabela="documentos_empresa"
                                data-pasta="empresa">
                                Enviar Documento <i class="fa fa-send"></i>
                            </button>
                        </div>
                    </div>    
                    
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <hr/>
                        </div>
                    </div>
                </section>

                <section  id="exibe_contrato">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="documento_contrato_locacao"><i class="fa fa-paperclip"></i> Anexar Contrato de Locação: <span class="text-danger">*</span></label><br>
                            <input type="file" name="documento_contrato_locacao" 
                                id="documento_contrato_locacao" />
                        </div>
    
                        <div class="col-lg-12">
                            <button class="btn btn-primary btn-enviar-documento" 
                                data-nome_input="documento_contrato_locacao" 
                                data-descricao="Contrato de Locação"
                                data-nome_sessao="documentos_empresa"
                                data-conteudo_documentos="conteudo_documentos_empresa"
                                data-id_tabela="documentos_empresa"
                                data-pasta="empresa">
                                Enviar Documento <i class="fa fa-send"></i>
                            </button>
                        </div>
                    </div>    
                    
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <hr/>
                        </div>
                    </div>
                </section>

                <section  id="exibe_protocolo" style="display: none;">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="documento_viabilidade"><i class="fa fa-paperclip"></i> Anexar Protocolo de Viabilidade - REGIN: <span class="text-danger">*</span></label><br>
                            <input type="file" name="documento_viabilidade" 
                                id="documento_viabilidade" />
                        </div>
    
                        <div class="col-lg-12">
                            <button class="btn btn-primary btn-enviar-documento" 
                                data-nome_input="documento_viabilidade" 
                                data-descricao="Protocolo de Viabilidade - REGIN"
                                data-nome_sessao="documentos_empresa"
                                data-conteudo_documentos="conteudo_documentos_empresa"
                                data-id_tabela="documentos_empresa"
                                data-pasta="empresa">
                                Enviar Documento <i class="fa fa-send"></i>
                            </button>
                        </div>
                    </div>    
                </section>
            </section>
        </div>  
    </div>

    <section id="arquivosCNPJEnviados" style="display: none;">
        <hr/>

        <div class="row">
            <div class="col-lg-12 form-group">
                <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

                <div class="conteudo_documentos_empresa">
                    @include('empresa.mei.empresa.tabela_documentos')
                </div>
            </div>
        </div>
    </section>
</div>
