<div class="requerente_prosseguimento">
    <div class="row">
        <div class="col-lg-6">
            <h5><i class="fa fa-user mt-3 mb-3"></i> Solicitação para Sanar Pendências:</h5>

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="nome_requerente">Justifique o motivo da solicitação:</label>
                    <textarea 
                    id="justificativa_recurso" 
                    name="justificativa_recurso" 
                    class="form-control form-control-sm {{ ($errors->has('justificativa_recurso') ? 'is-invalid' : '') }}" 
                    placeholder="" 
                    cols="5" rows="6" 
                    maxlength="1200">{{ old('justificativa_recurso') }}</textarea>

                        @if ($errors->has('justificativa_recurso'))
                            <div class="invalid-feedback">
                                {{ $errors->first('justificativa_recurso') }}
                            </div>
                        @endif
                </div>
            </div>
        </div>
        
        <div class="col-lg-6">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

            <div class="form-row">
                <div class="col-lg-12">
                    <div class="alert alert-info">
                        <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                    </div>
                </div>
                
                <div class="col-lg-12 form-group">
                    <select name="DescrDoc" id="DescrDoc" class="form-control form-control-sm {{ ($errors->has('DescrDoc') ? 'is-invalid' : '') }}">
                        <option value="">Selecione o Tipo do Documento</option>
                        <option value="Ficha Cadastral">Ficha Cadastral</option>
                        <option value="CPF do requerente">CPF do requerente</option>
                        <option value="Identidade do requerente">Identidade do requerente</option>
                        <option value="foto selfie com documento de identificação">Foto selfie com documento de identificação</option>
                        <option value="Comprovante de Residência Requerente">Comprovante de Residência Requerente (mês atual ou mês anterior)</option>
                        <option value="Comprovante do MEI">Comprovante do MEI (Certificado do MEI ou cartão de CNPJ)</option>
                        <option value="Documento Probatório da Atividade">Documento Probatório da Atividade</option>
                        <option value="Fotos do Empreendimento">Fotos do Empreendimento (com o empreendedor aparecendo na foto)</option>
                        <option value="Comprovante de Residência">Comprovante de Residência (mês atual ou mês anterior)</option>
                    </select>
                </div>

                <div class="col-lg-12 form-group">
                    <input type="file" name="arquivo_recurso" id="arquivo_recurso" />
                </div>

                <div class="col-lg-12">
                    <button class="btn btn-primary btn-enviar-documento" 
                        id="enviar_documento"
                        data-nome_input="arquivo_recurso" 
                        data-descricao="Teste"
                        data-nome_sessao="documentos_recurso"
                        data-conteudo_documentos="conteudo_documentos_recurso"
                        data-id_tabela="documentos_recurso"
                        data-pasta="recurso"
                        type="button">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
                
            </div>    
        </div>  
    </div>

    <hr/>

    <div class="row">
        <div class="col-lg-12 form-group">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

            <div class="conteudo_documentos_recurso">
                @include('social.fomenta.recurso.tabela_documentos')
            </div>
        </div>
    </div>
</div>
