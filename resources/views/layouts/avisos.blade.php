<div class="container-fluid">
    <div class="row" style="margin-top: -50px !important;">
        @if (( session('tipoUsuario') == "EMPRESA" && session('tipoUsuario') == "EMPRESA NÃO ESTABELECIDA") || empty(session('tipoUsuario')))
        @else
            <div class="col-lg-1 text-center py-5 avisos-home">
            </div> 
        @endif


        @if (( session('tipoUsuario') != "EMPRESA" && session('tipoUsuario') != "EMPRESA NÃO ESTABELECIDA") || empty(session('tipoUsuario')))
            @if (session('tipoUsuario') == "SERVIDOR" )
                <div class="col-lg-2 text-center py-5 avisos-home">
                    
                    <a href="{{ route('servicos', 1 ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Nesta área do Portal SIM, O Servidor Municipal tem acesso aos diversos serviços oferecidos pela Prefeitura de Maricá.">
                        <img src="img/icones/servidor.png" alt="Icone Servidor" title="Icone Servidor" width="70px" />

                        <h5 class="mt-4 no-underline">SERVIDOR</h5>
                    
                        <p class="mt-3 text-secondary texto_servico d-none">
                            Nesta área do Portal SIM, O Servidor Municipal tem acesso aos diversos serviços oferecidos pela Prefeitura de Maricá.
                        </p>
                    </a>
                </div>
            @elseif(session('tipoUsuario') == "CIDADÃO" || empty(session('tipoUsuario')))
                <div class="col-lg-2 text-center py-5 avisos-home">
                    
                    <a href="{{ route('servicos', 1 ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Nesta área do Portal SIM, você pode administrar os serviços oferecidos pela Prefeitura de Maricá.">
                        <img src="img/icones/cidadao.png" alt="Icone Cidadão" title="Icone Cidadão" width="70px" />

                        <h5 class="mt-4 no-underline">CIDADÃO</h5>
                    
                        <p class="mt-3 text-secondary texto_servico d-none">
                            Nesta área do Portal SIM, você pode administrar os serviços oferecidos pela Prefeitura de Maricá.
                        </p>
                    </a>
                </div>
            @endif
        @endif


        @if (session('tipoUsuario') == "EMPRESA" || 
            session('tipoUsuario') == "EMPRESA NÃO ESTABELECIDA" ||
            empty(session('tipoUsuario')))
            <div class="col-lg-2 text-center py-5 avisos-home">
                <a href="{{ route('servicos', 2 ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Nesta área do Portal SIM, você pode administrar os serviços oferecidos pela Prefeitura de Maricá
                à sua empresa, bem como as pessoas autorizadas a responder por ela.">
                    <img src="img/icones/empresa.png" alt="Icone Empresa" title="Icone Empresa" width="70px" />

                    <h5 class="mt-4">EMPRESA</h5>

                    <p class="mt-3 text-secondary texto_servico d-none">
                        Nesta área do Portal SIM, você pode administrar os serviços oferecidos pela Prefeitura de Maricá
                        à sua empresa, bem como as pessoas autorizadas a responder por ela.
                    </p>
                </a>
            </div>
        @endif

        {{-- <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('cadastro_iptu' ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Se você é corretor, despachante, administrador, entre outros, de uma grande quantidade de imóveis, solicite aqui a emissão dos boletos do IPTU 2021. OBS: Pedido mínimo de 10 matrículas">
                <img src="img/icones/corretor.png" alt="IPTU 2021 - Grandes Contribuintes" title="IPTU 2021 - Grandes Contribuintes" width="70px" />

                <h5 class="mt-4">IPTU 2021 - Grandes Contribuintes</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                    Se você é corretor, despachante, administrador, entre outros, de uma grande quantidade de imóveis, solicite aqui a emissão dos boletos do IPTU 2021. OBS: Pedido mínimo de 10 matrículas
                </p>
            </a>
        </div>

        <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('cadastro_iptu' ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Procedimento destinado à solicitação de IPTU para aqueles que não sabem a matrícula do imóvel e/ou não são proprietários do mesmo.">
                <img src="img/icones/responsavel_tribitario.png" alt="IPTU 2021 - Responsável tributário/Possuidores" title="IPTU 2021 - Responsável tributário/Possuidores" width="70px" />

                <h5 class="mt-4">IPTU 2021 - Responsável tributário/Possuidores</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                    Procedimento destinado à solicitação de IPTU para aqueles que não sabem a matrícula do imóvel e/ou não são proprietários do mesmo.
                </p>
            </a>
        </div> --}}
{{-- 
        <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('cadastro_iptu' ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Procedimento destinado à solicitação de IPTU para aqueles que não sabem a matrícula do imóvel e/ou não são proprietários do mesmo.">
                <img src="img/icones/responsavel_tribitario.png" alt="IPTU 2021 - Responsável tributário/Possuidores" title="IPTU 2021 - Responsável tributário/Possuidores" width="70px" />

                <h5 class="mt-4">IPTU 2021 - Responsável tributário/Possuidores</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                    Procedimento destinado à solicitação de IPTU para aqueles que não sabem a matrícula do imóvel e/ou não são proprietários do mesmo.
                </p>
            </a>
        </div> --}}

        <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('PAE_PAGINA' ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Programa de Amparo ao Trabalhador deverá atender aqueles trabalhadores formais,
            informais, autônomos e MEI prejudicados pela pandemia.Programa de Amparo ao Emprego é Iniciativa de apoio ao(s) empregado(s) de um(a) MEI, Micro ou Pequena empresa, prejudicados pela pandemia.">
                <img src="img/icones/pae.png" alt="Programa de Amparo ao Emprego" title="Programa de Amparo ao Emprego" width="70px" />

                <h5 class="mt-4">PAE</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                    Programa de Amparo ao Trabalhador deverá atender aqueles trabalhadores formais,
                    informais, autônomos e MEI prejudicados pela pandemia.Programa de Amparo ao Emprego é Iniciativa de apoio ao(s) empregado(s) de um(a) MEI, Micro ou Pequena empresa, prejudicados pela pandemia.
                </p>
            </a>
        </div>

        <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('consultarsituacaomrc' ) }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Cadastro dos artistas e trabalhadores da Cultura - Lei Aldir Blanc.">
                <img src="img/icones/logo_mrc.png" style="margin-top: -10px;" 
                    alt="Maricá na rede da Cultura" title="Maricá na rede da Cultura"
                    width="100px" />

                <h5 class="mt-1">MRC</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                    Cadastro dos artistas e trabalhadores da Cultura - Lei Aldir Blanc.
                </p>
            </a>
        </div>

        {{-- <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('PAT', 1 ) }}" class="no-underline">
                <img src="img/icones/corona.png" alt="Icone Coronavírus" title="Icone Coronavírus" width="70px" />

                <h5 class="mt-4 barra_title">PAT</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                    Programa de Amparo ao Trabalhador deverá atender aqueles trabalhadores formais,
                    informais, autônomos e MEI prejudicados pela pandemia.
                </p>
            </a>
        </div> --}}

        {{-- <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('alteraIPTU') }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="Este cadastro é uma iniciativa da Prefeitura Municipal de Maricá para minimizar a exposição dos idosos ao Covid-19.">
                <img src="img/icones/idoso.png" alt="Icone IPTU" title="Icone IPTU" width="70px" />

                <h5 class="mt-4">IPTU IDOSO</h5>
            
                <p class="mt-3 text-secondary texto_servico d-none">
                Este cadastro é uma iniciativa da Prefeitura Municipal de Maricá para minimizar a exposição dos idosos ao Covid-19.
                </p>
            </a>
        </div> --}}

        <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="{{ route('fomenta') }}" class="no-underline" data-toggle="tooltip" data-placement="bottom" title="É uma ação da Prefeitura de Maricá que disponibilizará linhas de crédito emergenciais para os micros e pequenos empresários.">
                <img src="img/icones/bot_fomenta.png" alt="Icone Fomenta" title="Icone Fomenta" width="70px" />

                <h5 class="mt-4">FOMENTA MARICÁ</h5>

                <p class="mt-3 text-secondary texto_servico d-none">
                    É uma ação da Prefeitura de Maricá que disponibilizará linhas de crédito emergenciais para os micros e pequenos empresários.
                </p>
            </a>
        </div>

        <div class="col-lg-2 text-center py-5 avisos-home">
            <a href="https://maricadigital.com.br/agendar-servico" class="no-underline" data-toggle="tooltip" data-placement="bottom" target="_blank" title="Agende um serviço nas Centrais do SIM">
                <img src="img/icones/agendamento.png" alt="Icone Agendamento" title="Icone Agendamento" width="70px" />

                <h5 class="mt-4">AGENDAMENTO</h5>

                <p class="mt-3 text-secondary texto_servico d-none">
                    Agende um serviço nas Centrais do SIM
                </p>
            </a>
        </div>

    </div>
</div>
