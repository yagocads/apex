<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests\CadastroFomentaRequest;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Notice;
use App\Models\Contact;
use App\Models\Log;
use App\Mail\FomentaSucesso;
use App\Mail\ResetPassword;
use App\User;
use Carbon\Carbon;
use Auth;
use DB;

class FomentaController extends Controller{

    public function index(){
        Log::create([ 'servico'=> 0,  'descricao' => "Home", 'cpf' => isset(Auth::user()->cpf)?Auth::user()->cpf:"", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'], ]);

        return view('site.welcome', [ 'pagina' => "Principal"]);
    }

    public function cadastrofomenta() {

        session()->forget('fomenta.cnpj');

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt', 
            'fomenta_cpfcnpj', 
            'no_debug', 
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('social.fomenta.fomentaValidaCpfCnpj', [ 'pagina' => "Principal",'showRecaptcha'=> $showRecaptcha]);

    }

    public function fomentaValidaCpfCnpj(Request $request){

        GoogleRecaptcha::verify('6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG', 'Por favor clique no checkbox do reCAPTCHA primeiro!');

        $validatedData = $request->validate([
            'fomenta_cpfcnpj' => ['required', 'string'],
        ]);

        if ( !$this->validaCNPJ($request->fomenta_cpfcnpj) ){
            return redirect()->back()->withInput()->withErrors( ["fomenta_cpfcnpj"=>"O CNPJ informado é inválido"] );
        }

        session()->put('fomenta.cnpj', $request->fomenta_cpfcnpj);

        return redirect()->route(
            "fomentaCadastrarMEI"
        );

    }

    public function fomentaCadastrarMEI(){

        if (!session()->has('fomenta.cnpj')) {
            return redirect()->route(
                "cadastrofomenta"
            );
        }

        $fomenta_cpfcnpj = session('fomenta.cnpj');

        $totalCadastros = DB::connection('integracao')
        ->table('lecom_fomenta')
        ->where('cnpj', '=', $fomenta_cpfcnpj)
        ->count();

        if($totalCadastros > 0){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"A empresa já está cadastrada no programa Fomenta Maricá"] );
        }

        // if(strtotime(now()) >= strtotime("2020-09-04 18:00:00")){
        //     $totalCadastrosComplementar = DB::connection('integracao')
        //     ->table('lecom_fomenta_complementar')
        //     ->where('cnpj', '=', $fomenta_cpfcnpj)
        //     ->count();   

        //     if($totalCadastrosComplementar == 0){
        //         return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"O período de cadastramento para o programa Fomenta Marica está ENCERRADO."] );
        //     }
        // }




        $cnpj = remove_character_document($fomenta_cpfcnpj);

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.receitaws.com.br/v1/cnpj/". $cnpj ."/days/30",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer b21989b535bde93f103ef1254ca087f703d704615e13bff434282c6abee13d97"
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $resposta=json_decode($response);

        // dd($response, $resposta);

        if(is_null($resposta)){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"Não foi possível validar o CNPJ informado"] );
        }

        if($resposta->status == "ERROR"){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>$resposta->message] );
        }

        if($resposta->situacao != "ATIVA"){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"A situação da empresa não é ATIVA"] );
        }

        if($resposta->porte != "MICRO EMPRESA"){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"É necessário que a Empresa seja MEI"] );
        }

        if($resposta->natureza_juridica != "213-5 - Empresário (Individual)"){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"É necessário que a Empresa seja MEI"] );
        }

        if( strtoupper($resposta->municipio) != "MARICÁ" && strtoupper($resposta->municipio) != "MARICA"){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"A Empresa deve ser da Cidade de Maricá"] );
        }

        $data1 = explode("/", $resposta->abertura);
        if( strtotime($data1[2]."-".$data1[1]."-".$data1[0]) >= strtotime("2020-04-01")){
            return redirect()->route("cadastrofomenta")->withInput()->withErrors( ["fomenta_cpfcnpj"=>"A data de abertura da empresa deve ser inferior a 1 de abril de 2020."] );
        }

        // dd($cnpj , $resposta);

        return view('social.fomenta.fomenta_cadastro', [ 
            "pagina" => "principal", 
            "cnpj" => $fomenta_cpfcnpj,
            'empresa' => $resposta, 
            "estados" => getEstados(),
            'recurso'=> isset($request->recurso)?1:0]);

    }
   
    public function fomentaAgendamento(Request $request){
        
        $agendamentos = DB::connection('integracao')
        ->table('lecom_fomenta_agendamento')
        ->where('cnpj', '=', $request->cnpj)
        ->where('processo', '=', $request->processo)
        ->where('fase', '=', $request->etapa)
        ->where('ciclo', '=', $request->ciclo)
        ->get();

        // dd($agendamentos);

        if(count($agendamentos) > 0){
            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>Já foi feito o agendamento. <br><br> 
                    Local: <b>'.$agendamentos[0]->local_agendamento.'</b><br/>
                    Data do Agendamento: <b>' .date('d/m/Y', strtotime($agendamentos[0]->data_agendamento)).'</b><br/>
                    Hora do Agendamento: <b>'.$agendamentos[0]->hora_agendamento.'</b><br/>
                    Posto:  <b>'.$request->posto_agendamento.'</b><br/><br/>
                    Data de efetivação: <b>'.date('d/m/Y H:i:s', strtotime($agendamentos[0]->data)).'</b><br/>
                    ',
                "rota" => "fomentaconsultarsituacao",
                "retorno" => "",
                "codigo" => "FOM_006",
            ]);
        }

        
        $agendamentos = DB::connection('integracao')
        ->table('lecom_fomenta_agendamento')
        ->where('local_agendamento', '=', $request->local_agendamento)
        ->where('data_agendamento', '=', implode('-', array_reverse(explode('/', $request->data_agendamento))) )
        ->where('hora_agendamento', '=', $request->hora_agendamento)
        ->where('posto_agendamento', '=', $request->posto_agendamento)
        ->get();

        // dd($agendamentos);

        if(count($agendamentos) > 0){
            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>Já foi realizado um agendamento o horário solicitado. <br/>Escolha um novo dia/horário.<br/><br/> 
                    Local: <b>'.$agendamentos[0]->local_agendamento.'</b><br/>
                    Data do Agendamento: <b>' .date('d/m/Y', strtotime($agendamentos[0]->data_agendamento)).'</b><br/>
                    Hora do Agendamento: <b>'.$agendamentos[0]->hora_agendamento.'</b><br/>
                    Posto:  <b>'.$request->posto_agendamento.'</b><br/><br/>
                    ',
                "rota" => "fomentaconsultarsituacao",
                "retorno" => "",
                "codigo" => "FOM_007",
            ]);
        }



        DB::beginTransaction();

        try{
            $idRecurso = DB::connection('integracao')
            ->table('lecom_fomenta_agendamento')
            ->insertGetId(
                array_merge(
                    $this->dadosAgendamento($request)
                )
            );

            DB::commit();

            session()->forget('fomenta.cnpj');
           
        }
        catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => $request->cnpj, 
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "fomentaController",
                    'rotina'    => "fomenta_recurso",
                    'erro'      => $th, 
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "fomentaconsultarsituacao",
                "retorno" => "",
                "codigo" => "FOM_005",
                ]);
        }
            
        return view('messages.sucesso', [
            "icone" => "fa-lock",
            "titulo" => "Fomenta Maricá",
            "mensagem" => '<p>A sua solicitação de agendamento foi registrada com sucesso.</p><p>Compareça com máscara, leve sua caneta, chegue pontualmente no horário marcado e compareça desacompanhado para evitar aglomeração, exceto caso de comprovada necessidade.</p>'.
            'Local: <b>'.$request->local_agendamento."</b><br/>".
            'Data:  <b>'.$request->data_agendamento."</b><br/>".
            'Horário:  <b>'.$request->hora_agendamento."</b><br/>".
            'Posto:  <b>'.$request->posto_agendamento."</b><br/>",
            "rota" => "fomentaconsultarsituacao",
            "retorno" => "",
            ]);
    }

    
    private function dadosAgendamento(Request $request){ 
        return [
            'id_fomenta' => $request->id_fomenta,
            'cnpj' => $request->cnpj,
            'processo' => $request->processo,
            'fase' => $request->etapa,
            'ciclo' => $request->ciclo,
            'local_agendamento' => $request->local_agendamento,
            'data_agendamento' =>  implode('-', array_reverse(explode('/', $request->data_agendamento))),
            'hora_agendamento' =>  $request->hora_agendamento,
            'posto_agendamento' =>  $request->posto_agendamento,
            'data' => date('Y-m-d H:i:s'),
        ];
    }

    public function fomentaConsultaHorariosDisponiveis(Request $request){
        $horarios = array(
            ["horario" => '09:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '09:30', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '10:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '10:30', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '11:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '11:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '13:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '13:30', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '14:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '14:30', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '15:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '15:30', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '16:00', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
            ["horario" => '16:30', "posto_1" => "", "posto_2" => "", "posto_3" => ""],
        );

        $agendamentos = DB::connection('integracao')
        ->table('lecom_fomenta_agendamento')
        ->where('local_agendamento', '=', $request->localAgendamento)
        ->where('data_agendamento', '=', implode('-', array_reverse(explode('/', $request->diaAgendamento))))
        ->orderBy('data_agendamento','asc')
        ->orderBy('hora_agendamento', 'asc')
        ->orderBy('posto_agendamento', 'asc')
        ->get();

        foreach($horarios as $id_horario => $horario) {
            foreach($agendamentos as $agendamento) {
                if($horario['horario'].":00" == $agendamento->hora_agendamento){
                    $horario = array_replace($horario, ["posto_".$agendamento->posto_agendamento => "Ocupado"]);
                    $horarios = array_replace($horarios, array($id_horario => $horario));
                }
            }
        }

        $tabelaHtml = "<div class='table-responsive py-1 pl-1 pr-1'>";
        $tabelaHtml .= "<table class='table table-striped table-bordered dataTable dt-responsive w-100' id='tabela_horario'>";
        $tabelaHtml .= "<thead>";
        $tabelaHtml .= "<tr>";
        $tabelaHtml .= "<th>Horário</th>";
        $tabelaHtml .= "<th>Posto 1</th>";
        // $tabelaHtml .= "<th>Posto 2</th>";
        // if ($request->localAgendamento == "CENTRO"){
        //     $tabelaHtml .= "<th>Posto 3</th>";
        // }
        $tabelaHtml .= "</tr>";
        $tabelaHtml .= "</thead>";

        $tabelaHtml .= "<tbody>";
        foreach($horarios as $id_horario => $horario){
            $tabelaHtml .= "<tr>";
            $tabelaHtml .= "<td>".$horario["horario"]."</td>";
            if($horario["posto_1"] == ""){
                $tabelaHtml .='<td>
                    <button class="btn btn-success btn-sm btn-agendar-horario" title="Agendar Horário" 
                        data-horario="'.$horario["horario"].'" 
                        data-posto="1"
                        data-local="'.$request->localAgendamento.'">
                        Agendar
                    </button>
                </td>'; 
            }
            else{
                $tabelaHtml .= "<td>".$horario["posto_1"]."</td>";
            }

            // if($horario["posto_2"] == ""){
            //     $tabelaHtml .='<td>
            //         <button class="btn btn-success btn-sm btn-agendar-horario" title="Agendar Horário" 
            //             data-horario="'.$horario["horario"].'" 
            //             data-posto="2"
            //             data-local="'.$request->localAgendamento.'">
            //             Agendar
            //         </button>
            //     </td>'; 
            // }
            // else{
            //     $tabelaHtml .= "<td>".$horario["posto_2"]."</td>";
            // }

            // if ($request->localAgendamento == "CENTRO"){
            //     if($horario["posto_3"] == ""){
            //         $tabelaHtml .='<td>
            //             <button class="btn btn-success btn-sm btn-agendar-horario" title="Agendar Horário" 
            //                 data-horario="'.$horario["horario"].'" 
            //                 data-posto="3"
            //                 data-local="'.$request->localAgendamento.'">
            //                 Agendar
            //             </button>
            //         </td>'; 
            //     }
            //     else{
            //         $tabelaHtml .= "<td>".$horario["posto_3"]."</td>";
            //     }
            // }
            $tabelaHtml .= "</tr>";
        }
        $tabelaHtml .= "</tbody>";
        $tabelaHtml .= "</table>";
        $tabelaHtml .= "</div>";

        return $tabelaHtml;
    }

    public function fomentaConsultaDatasDisponiveis(Request $request){
        $dataHoje = date('d-m-Y'); 
        $arrayDatas = [];
        $totalDias = 1;
        
        $semana = array(
            0 => 'Domingo',
            1 => 'Segunda-Feira',
            2 => 'Terca-Feira',
            3 => 'Quarta-Feira',
            4 => 'Quinta-Feira',
            5 => 'Sexta-Feira',
            6 => 'Sábado'
        );

        $dataInicio = date('d-m-Y', strtotime("+1 days",strtotime($dataHoje)));

        if(strftime("%w", strtotime($dataInicio)) == '0'){
            $dataInicio = date('d-m-Y', strtotime("+2 days",strtotime($dataHoje)));
        }
        elseif(strftime("%w", strtotime($dataInicio)) == '6'){
            $dataInicio = date('d-m-Y', strtotime("+3 days",strtotime($dataHoje)));
        }

        $dataHoje = $dataInicio;
        
        while (count($arrayDatas) <= 2) {
            $diaAgendamento = date('d-m-Y', strtotime("+".$totalDias." days",strtotime($dataHoje))); 
            if(
                $diaAgendamento == '15-02-2021' ||
                $diaAgendamento == '16-02-2021' ||
                $diaAgendamento == '17-02-2021' ||
                strftime("%w", strtotime($diaAgendamento)) == '0' ||
                strftime("%w", strtotime($diaAgendamento)) == '6'
            ){
                $totalDias++;
            }
            else{
                $dataAgendamento = explode("-", $diaAgendamento);
                $diaUnix = mktime(0, 0, 0, $dataAgendamento[0], $dataAgendamento[1], $dataAgendamento[2]);

                $feriados = $this->dias_feriados($dataAgendamento[2]);

                if( in_array($diaUnix, $feriados) ){
                    $totalDias++;
                }
                else{
                    array_push($arrayDatas, [
                        'data' =>  date('d/m/Y', strtotime("+".$totalDias." days",strtotime($dataHoje))), 
                        'diaSemana' =>  $semana[strftime("%w", strtotime($diaAgendamento))], 
                    ]);
                    $totalDias++;
                }
            }
        }
        return $arrayDatas;
    }

    public function fomentaRecurso(Request $request){
        
        $totalRecursos = DB::connection('integracao')
        ->table('lecom_fomenta_recursos')
        ->where('cnpj', '=', $request->cnpj)
        ->where('processo', '=', $request->processo)
        ->where('fase', '=', $request->etapa)
        ->where('ciclo', '=', $request->ciclo)
        ->count();

        if($totalRecursos > 0){
            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>Já foi feita uma solicitação para Sanar Pendências para esse CNPJ.  Aguarde a próxima etapa',
                "rota" => "cadastrofomenta",
                "retorno" => "",
                "codigo" => "FOM_004",
            ]);
        }

        DB::beginTransaction();

        try{
            $idRecurso = DB::connection('integracao')
            ->table('lecom_fomenta_recursos')
            ->insertGetId(
                array_merge(
                    $this->dadosRecurso($request)
                )
            );

            $this->enviarDocumentosRecursoParaLecom(
                $request->cnpj,
                $idRecurso,
                $request->etapa,
                $request->ciclo
            );

            DB::commit();

            $this->removerDocumentosRecursoSessao();

            session()->forget('fomenta.cnpj');

           
        }
        catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => $request->cnpj, 
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "fomentaController",
                    'rotina'    => "fomenta_recurso",
                    'erro'      => $th, 
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "fomentaconsultarsituacao",
                "retorno" => "",
                "codigo" => "FOM_005",
                ]);
        }
            
        return view('messages.sucesso', [
            "icone" => "fa-lock",
            "titulo" => "Fomenta Maricá",
            "mensagem" => '<p>A sua solicitação para Sanar Pendências foi registrada com sucesso.',
            "rota" => "fomentaconsultarsituacao",
            "retorno" => "",
            ]);


    }
   
    public function fomentaCadastroSalvar(CadastroFomentaRequest $request){
        
        $totalCadastros = DB::connection('integracao')
        ->table('lecom_fomenta')
        ->where('cnpj', '=', $request->cnpj)
        ->count();

        if($totalCadastros > 0){
            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>O Benefício já foi solicitado para esse CNPJ',
                "rota" => "cadastrofomenta",
                "retorno" => "",
                "codigo" => "FOM_002",
            ]);
        }

        $arrCnpj = explode("-", $request->cnpj );

        $totalregistros = DB::connection('integracao')
        ->table('lecom_fomenta')
        ->count();

        $protocolo = $arrCnpj[1].str_pad($totalregistros+1, 6, '0', STR_PAD_LEFT).date('d');

        DB::beginTransaction();

        try{
            $idFomenta = DB::connection('integracao')
            ->table('lecom_fomenta')
            ->insertGetId(
                array_merge(
                    $this->dadosRequerente($request, $protocolo),
                    $this->dadosCreditoEmergencial($request),
                    $this->dadosEnderecoRequerente($request),
                    $this->dadosMercantis($request),
                    $this->dadosInformacoesContato($request),
                    $this->dadosEndereco($request)
                )
            );

            $this->enviarDocumentosParaLecom(
                $request->cnpj,
                $idFomenta
            );

            DB::commit();

            $this->removerDocumentosSessao();

            session()->forget('fomenta.cnpj');

           
        }
        catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => $request->cnpj, 
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "fomentaController",
                    'rotina'    => "fomentaCadastroSalvar",
                    'erro'      => $th, 
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-lock",
                "titulo" => "Fomenta Maricá",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "cadastrofomenta",
                "retorno" => "",
                "codigo" => "FOM_001",
                ]);
        }
        try{
            $destino = $request->email;
            
            Mail::to($destino)->send(new FomentaSucesso($request, $protocolo));
            
            return view('social.fomenta.fomenta_protocolo', [ 'pagina' => "Principal", 'razaoSocial'=> $request->razao_social, 'cnpj'=>$request->cnpj, 'protocolo'=>$protocolo]);
        }
        catch (\Throwable $th) {
            // Erro no envio do e-mail, mas apresenta a tela de protocolo.
            return view('social.fomenta.fomenta_protocolo', [ 'pagina' => "Principal", 'razaoSocial'=> $request->razao_social, 'cnpj'=>$request->cnpj, 'protocolo'=>$protocolo]);
        }


    }


    private function dadosRecurso(Request $request){ 
        return [
            'id_fomenta' => $request->id_fomenta,
            'cnpj' => $request->cnpj,
            'processo' => $request->processo,
            'fase' => $request->etapa,
            'ciclo' =>  $request->ciclo,
            'recurso' =>  $request->justificativa_recurso,
            'data' => date('Y-m-d H:i:s'),
        ];
    }

    private function dadosCreditoEmergencial(Request $request){ 
        $valorSolicitado = str_replace(".", "", $request->valorSolicitado);
        $valorSolicitado = str_replace(",", ".", $valorSolicitado);

        $valorParcela = str_replace(".", "", $request->valorParcela);
        $valorParcela = str_replace(",", ".", $valorParcela);

        return [
            'valor_solicitado' => $valorSolicitado,
            'carencia' => $request->tempoCarencia,
            'prazo' => $request->prazo,
            'parcela_solicitada' => $valorParcela,
            'motivo_beneficio' =>  $request->motivoSolicitacao,
        ];
    }

    private function dadosRequerente(Request $request, $protocolo){   
        return [
            'resp_preenchimento' => $request->responsavel,
            'rep_legal_nome' => $request->nome_requerente,
            'rep_legal_rg' => $request->identidade_requerente,
            'rep_legal_rg_orgao_expedidor' => implode('-', array_reverse(explode('/', $request->orgao_emissor_requerente))),
            'rep_legal_rg_data_expedicao' => implode('-', array_reverse(explode('/', $request->data_emissao_requerente))),
            'rep_legal_cpf' => $request->cpf_requerente,
            'rep_legal_email' => $request->email_requerente,
            'data_cadastro' => date('Y-m-d H:i:s'),
            'protocolo' => $protocolo
        ];
    }

    private function dadosEnderecoRequerente(Request $request){
        return [
            'rep_legal_cep' => $request->cep_requerente,
            'rep_legal_endereco' => $request->endereco_requerente,
            'rep_legal_numero' => $request->numero_requerente,
            'rep_legal_complemento' => $request->complemento_requerente,
            'rep_legal_bairro' => $request->bairro_requerente,
            'rep_legal_cidade' => $request->municipio_requerente,
            'rep_legal_uf' => $request->estado_requerente
        ];
    }
    
    private function dadosMercantis(Request $request){
        return [
            'cnpj' => $request->cnpj,
            'nome_empresarial' => $request->razao_social,
            'nome_fantasia' => $request->nome_fantasia,
            'natureza_juridica' => $request->natureza_juridica,
            'data_abertura' => implode('-', array_reverse(explode('/', $request->data_abertura_empresa))),
            'data_inicio_atividade' => implode('-', array_reverse(explode('/', $request->data_inicio_atividade))),
            'CNAE' => $request->cnae,
            'agencia_mumbuca' => $request->agPreferencia,
        ];
    }

    private function dadosInformacoesContato(Request $request){
        return [
            'email' => $request->email,
            'celular' => $request->celular,
            'telefone' => $request->telefone
        ];            
    }

    private function dadosEndereco(Request $request){
        return [
            'cep' => $request->cep,
            'endereco' => $request->endereco,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cidade' => $request->municipio,
            'uf' => $request->estado
        ];
    }

    private function enviarDocumentosRecursoParaLecom($cnpj, $idRecurso, $etapa, $ciclo){    
        $sessionDocumentos = [
            'documentos_recurso',
        ];

        foreach($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach(session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_fomenta_recursos_documentos')
                        ->insert([
                            'id_recurso' => $idRecurso,
                            'cnpj' => $cnpj,
                            'fase' => $etapa,
                            'ciclo' => $ciclo,
                            'documento' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    private function enviarDocumentosParaLecom($cnpj, $idFomenta){    
        $sessionDocumentos = [
            'documentos_requerente',
            'documento_endereco_requerente',
            'documentos_informacoes_pessoais',
            'documentos_dados_mercantis',
            'documento_endereco'
        ];

        foreach($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach(session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_fomenta_documentos')
                        ->insert([
                            'id_fomenta' => $idFomenta,
                            'cnpj' => $cnpj,
                            'documento' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    public function fomentaconsultarsituacao(){
        Log::create([ 'servico'=> 5009,  'descricao' => "Consulta de Crédito Fomenta", 'cpf' => isset(Auth::user()->cpf)?Auth::user()->cpf:"", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'], ]);

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt', 
            'dtAbertura', 
            'no_debug', 
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('social.fomenta.fomenta_consultaauxilio', [ 'pagina' => "Principal",'showRecaptcha'=> $showRecaptcha]);
    }

    public function fomentavalidasituacao(Request $request){

        // GoogleRecaptcha::verify('6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG', 'Por favor clique no checkbox do reCAPTCHA primeiro!');

        $validatedData = $request->validate([
            'cnpj'          => ['required', 'string'],
            'protocolo'     => ['required', 'string'],
            'dtAbertura'    => ['required', 'date_format:"d/m/Y"'],
        ]);
        
        if ( !$this->validaCNPJ($request->cnpj) ){
            return redirect()->back()->withInput()->withErrors( ["cnpj"=>"O CNPJ informado é inválido"] );
        }

        $usuarioCPF = DB::connection('integracao')
        ->table('lecom_fomenta')
        ->where('cnpj',         '=', $request->cnpj )
        ->where('protocolo',    '=', $request->protocolo )
        ->where('data_abertura','=', implode('-', array_reverse(explode('/', $request->dtAbertura))) )
        ->get();

        if (count($usuarioCPF) == 0 ){
            return redirect()->back()->withInput()->withErrors( ["cnpj"=>"Não encontramos nenhum pedido de Benefício com os dados informados "] );
        }
        
        if($usuarioCPF[0]->data_leitura_lecom == null || $usuarioCPF[0]->data_leitura_lecom == ""){
            return view('messages.sucesso', [
                'titulo' => 'PROGRAMA FOMENTA MARICÁ', 
                'icone' => 'fa-home', 
                "mensagem" => '<p><strong>SOLICITAÇÃO REGISTRADA</strong> </p>
            <p>A sua solcitação foi registrada e está sendo processada.</p>
            <p>Retorne em breve para verificar o andamento da sua solicitação.</p>
            ' 
            ]);
        }

        $processoLecom = DB::connection('lecom')
        ->table('v_consulta_servico_fomenta')
        ->where('Chamado',  '=', $usuarioCPF[0]->chamado_lecom )
        ->orderBy('abertura', 'desc')
        ->first();
        
        $recursoLecom = DB::connection('integracao')
            ->table('lecom_fomenta_recursos')
            ->where('cnpj', '=', $request->cnpj )
            ->where('processo', '=', $processoLecom->chamado )
            ->where('fase', '=', $processoLecom->etapa )
            ->where('ciclo', '=', $processoLecom->ciclo )
            ->first();

        $AgendamentoLecom = "";
        if($processoLecom->fase == "Realizar Agendamento"){
            $AgendamentoLecom = DB::connection('integracao')
                ->table('lecom_fomenta_agendamento')
                ->where('cnpj', '=', $request->cnpj )
                ->where('processo', '=', $processoLecom->chamado )
                ->where('fase', '=', $processoLecom->etapa )
                ->where('ciclo', '=', $processoLecom->ciclo )
                ->first();
        }

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt', 
            'recurso', 
            'no_debug', 
            'mt-4 mb-3 col-md-6 offset-md-4 aligncenter', 
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );
    
        return view('social.fomenta.fomenta_consultaauxiliostatus', [ 
            'pagina'        => "Principal",
            'usuario'       => $usuarioCPF[0], 
            'processo'      => $processoLecom, 
            'recurso'       => $recursoLecom, 
            'agendamento'   => $AgendamentoLecom,
            'showRecaptcha' => $showRecaptcha
        ]);

    }

    private function removerDocumentosRecursoSessao() {
        session()->forget("documentos_recurso");
    }

    private function removerDocumentosSessao() {
        session()->forget("documentos_requerente");
        session()->forget("documento_endereco_requerente");
        session()->forget("documentos_dados_mercantis");
        session()->forget("documento_endereco");
    }

    public function salvarDocumentoServidorFomenta(Request $request){

        $nome_original = $request->arquivo->getClientOriginalName();
        $extensao = $request->arquivo->extension();

        $nome_arquivo = remove_character_document($request->cpf_ou_cnpj) . "_" . (isset($request->descricao_martelada) && in_array($request->descricao_martelada) ? $request->descricao_martelada : $request->descricao) . "_" . uniqid() . "." . $extensao;
        $nome_arquivo = remove_accentuation(str_replace(" ", "_", $nome_arquivo));
        
        $upload = $request->arquivo->storeAs( $request->destino !="" ? $request->destino:'fomenta', $nome_arquivo, 'ftp');

        if ($upload) {

            if (!empty($request->matricula_imovel)) {
                session()->push($request->nome_sessao, (object) [
                    "matricula_imovel" => $request->matricula_imovel,
                    "tipo_vinculo" => $request->tipo_vinculo,
                    "tipo_documento" => $request->tipo_documento,
                    "nome_original" => $request->arquivo->getClientOriginalName(),
                    "nome_novo" => $nome_arquivo
                ]);
            } else {
                session()->push($request->nome_sessao, (object) [
                    "descricao" => $request->descricao,
                    "nome_original" => $request->arquivo->getClientOriginalName(),
                    "nome_novo" => $nome_arquivo
                ]);
            }

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }
            
            return view('social.fomenta.'. $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removerDocumentoSessaoFomenta(Request $request){

        if ($this->removerDocumentoServidor(($request->destino !="" ? $request->destino:'fomenta')."/".$request->nome_arquivo)) {
        
            $session = session()->get($request->nome_sessao);

            if (!empty($session)) {

                foreach($session as $key => $dados) {

                    if (in_array($request->nome_arquivo, (array) $dados)) {
                        unset($session[$key]);
                        session()->forget($request->nome_sessao);
                        session()->put($request->nome_sessao, $session);
                    }
                }
            }

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }

            return view('social.fomenta.'. $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }
    }

    private function removerDocumentoServidor(string $arquivo): bool {
        return Storage::disk('ftp')->delete($arquivo);
    }

    public function imprimirprotocolofomenta(Request $request){

        $usuarioCNPJ = DB::connection('integracao')
        ->table('lecom_fomenta')
        ->where('protocolo','=', $request->protocolo )
        ->first();

        // dd( $request, $usuarioCPF);

        return \PDF::loadView('social.fomenta.fomenta_protocoloimpressao', ['cnpj' => $usuarioCNPJ->cnpj, 'razaoSocial' => $usuarioCNPJ->nome_empresarial, 'data'=> $usuarioCNPJ->data_cadastro, 'protocolo'=> $usuarioCNPJ->protocolo])
        ->download('protocolo_fomenta.pdf')
       //->stream()
       ;

    }

    function valida_cpf( $cpf ) {
        // Verifica se um número foi informado
        if(empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if (
            $cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
        // Calcula os digitos verificadores para verificar se o
        // CPF é válido
        } else {   
            for ($t = 9; $t < 11; $t++) {
                
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf[$c] != $d) {
                    return false;
                }
            }
            return true;
        }
        
    }

    function validaCNPJ($cnpj = null) {

        // Verifica se um número foi informado
        if(empty($cnpj)) {
            return false;
        }
    
        // Elimina possivel mascara
        $cnpj = preg_replace("/[^0-9]/", "", $cnpj);
        $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);
        
        // Verifica se o numero de digitos informados é igual a 11 
        if (strlen($cnpj) != 14) {
            return false;
        }
        
        // Verifica se nenhuma das sequências invalidas abaixo 
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cnpj == '00000000000000' || 
            $cnpj == '11111111111111' || 
            $cnpj == '22222222222222' || 
            $cnpj == '33333333333333' || 
            $cnpj == '44444444444444' || 
            $cnpj == '55555555555555' || 
            $cnpj == '66666666666666' || 
            $cnpj == '77777777777777' || 
            $cnpj == '88888888888888' || 
            $cnpj == '99999999999999') {
            return false;
            
         // Calcula os digitos verificadores para verificar se o
         // CPF é válido
         } else {   
            $j = 5;
            $k = 6;
            $soma1 = 0;
            $soma2 = 0;
    
            for ($i = 0; $i < 13; $i++) {
    
                $j = $j == 1 ? 9 : $j;
                $k = $k == 1 ? 9 : $k;
    
                $soma2 += ($cnpj[$i] * $k);
    
                if ($i < 12) {
                    $soma1 += ($cnpj[$i] * $j);
                }
    
                $k--;
                $j--;
    
            }
    
            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;
    
            return (($cnpj[12] == $digito1) and ($cnpj[13] == $digito2));
         
        }
    }

    function validaCpfDep( $cpf ){

        if(!$this->valida_cpf($cpf)){
            return 1;
        }

        $usuariosCPF = DB::connection('mysql')
        ->table('auxilio_pat')
        ->where('cpf', '=', $cpf )
        ->get();

        // dd(count($usuariosCPF));
        if (count($usuariosCPF) > 0 ){
            
            $usuariosCPFLecom = DB::connection('lecom')
            ->table('v_consulta_servico_pat')
            ->where('cpf',      '=', $cpf )
            ->where('Serviço',  '=', "PAT - Programa de Amparo ao Trabalhador" )
            ->where('Fase',     '!=', "Solicitação Recusada" )
            ->get();

            if (count($usuariosCPFLecom) > 0 ){
                return 2;
            }
        }
        
        $dependentesCPF = DB::connection('lecom')
        ->table('v_cpf_dependentes')
        ->where('dependente', '=', $cpf )
        ->get();

        // dd(count($dependentesCPF));

        if (count($dependentesCPF) > 0 ){
            return 3;
        }    


        return 0;
    }

    public function verificarEnvioDocumentos(Request $request) {
        $documentos_em_falta = [];


        foreach($request->documentos_necessarios as $documento) {
            $achei = false;
            if (session()->has($documento['nome_sessao'])){
                foreach(session($documento['nome_sessao']) as $documento_sessao) {
                    if($documento_sessao->descricao == $documento["data_descricao"]){
                        $achei = true;
                        break;
                    }
                }
            }
            if(!$achei){
                $documentos_em_falta[] = [
                    "descricao" => $documento['descricao']
                ];
            }
        }

        return response()->json(["documentos_em_falta" => $documentos_em_falta]);
    }

    public function verificarEnvioDocumentosRecurso(Request $request) {

        $cont = 0;
        if (session()->has("documentos_recurso")){
            foreach(session("documentos_recurso") as $documento_sessao) {
                $cont++;
            }
        }

        return response()->json(["documentos_enviados" =>$cont]);
    }


    public function dias_feriados($ano = null){
        if(empty($ano))
        {
            $ano = intval(date('Y'));
        }
    
        $pascoa = easter_date($ano); // Limite de 1970 ou após 2037 da easter_date PHP consulta http://www.php.net/manual/pt_BR/function.easter-date.php
        $dia_pascoa = date('j', $pascoa);
        $mes_pascoa = date('n', $pascoa);
        $ano_pascoa = date('Y', $pascoa);
    
        $feriados = array(
            // Tatas Fixas dos feriados Nacionail Basileiras
            mktime(0, 0, 0, 1, 1, $ano), // Confraternização Universal - Lei nº 662, de 06/04/49
            mktime(0, 0, 0, 4, 21, $ano), // Tiradentes - Lei nº 662, de 06/04/49
            mktime(0, 0, 0, 5, 1, $ano), // Dia do Trabalhador - Lei nº 662, de 06/04/49
            mktime(0, 0, 0, 9, 7, $ano), // Dia da Independência - Lei nº 662, de 06/04/49
            mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida - Lei nº 6802, de 30/06/80
            mktime(0, 0, 0, 11, 2, $ano), // Todos os santos - Lei nº 662, de 06/04/49
            mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica - Lei nº 662, de 06/04/49
            mktime(0, 0, 0, 12, 25, $ano), // Natal - Lei nº 662, de 06/04/49
    
            // Essas Datas depem diretamente da data de Pascoa
            // mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 48, $ano_pascoa), //2ºferia Carnaval
            mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 47, $ano_pascoa), //3ºferia Carnaval
            mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 2, $ano_pascoa), //6ºfeira Santa
            mktime(0, 0, 0, $mes_pascoa, $dia_pascoa, $ano_pascoa), //Pascoa
            mktime(0, 0, 0, $mes_pascoa, $dia_pascoa + 60, $ano_pascoa), //Corpus Cirist
    
        );
    
        sort($feriados);
    
        return $feriados;
    }


}