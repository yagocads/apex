<option {{ old('banco_nome') == "Banco ABC Brasil S.A." ? "selected" : "" }} value="Banco ABC Brasil S.A.">Banco ABC Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco ABN AMRO S.A." ? "selected" : "" }} value="Banco ABN AMRO S.A.">Banco ABN AMRO S.A.</option>
<option {{ old('banco_nome') == "Banco Agibank S.A." ? "selected" : "" }} value="Banco Agibank S.A.">Banco Agibank S.A.</option>
<option {{ old('banco_nome') == "Banco Alfa S.A." ? "selected" : "" }} value="Banco Alfa S.A.">Banco Alfa S.A.</option>
<option {{ old('banco_nome') == "Banco Alvorada S.A." ? "selected" : "" }} value="Banco Alvorada S.A.">Banco Alvorada S.A.</option>
<option {{ old('banco_nome') == "Banco Andbank (Brasil) S.A." ? "selected" : "" }} value="Banco Andbank (Brasil) S.A.">Banco Andbank (Brasil) S.A.</option>
<option {{ old('banco_nome') == "Banco B3 S.A." ? "selected" : "" }} value="Banco B3 S.A.">Banco B3 S.A.</option>
<option {{ old('banco_nome') == "Banco BANDEPE S.A." ? "selected" : "" }} value="Banco BANDEPE S.A.">Banco BANDEPE S.A.</option>
<option {{ old('banco_nome') == "Banco BMG S.A." ? "selected" : "" }} value="Banco BMG S.A.">Banco BMG S.A.</option>
<option {{ old('banco_nome') == "Banco BNP Paribas Brasil S.A." ? "selected" : "" }} value="Banco BNP Paribas Brasil S.A.">Banco BNP Paribas Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco BOCOM BBM S.A." ? "selected" : "" }} value="Banco BOCOM BBM S.A.">Banco BOCOM BBM S.A.</option>
<option {{ old('banco_nome') == "Banco Bradescard S.A." ? "selected" : "" }} value="Banco Bradescard S.A.">Banco Bradescard S.A.</option>
<option {{ old('banco_nome') == "Banco Bradesco BBI S.A." ? "selected" : "" }} value="Banco Bradesco BBI S.A.">Banco Bradesco BBI S.A.</option>
<option {{ old('banco_nome') == "Banco Bradesco Cartões S.A." ? "selected" : "" }} value="Banco Bradesco Cartões S.A.">Banco Bradesco Cartões S.A.</option>
<option {{ old('banco_nome') == "Banco Bradesco Financiamentos S.A." ? "selected" : "" }} value="Banco Bradesco Financiamentos S.A.">Banco Bradesco Financiamentos S.A.</option>
<option {{ old('banco_nome') == "Banco Bradesco S.A." ? "selected" : "" }} value="Banco Bradesco S.A.">Banco Bradesco S.A.</option>
<option {{ old('banco_nome') == "Banco BS2 S.A." ? "selected" : "" }} value="Banco BS2 S.A.">Banco BS2 S.A.</option>
<option {{ old('banco_nome') == "Banco BTG Pactual S.A." ? "selected" : "" }} value="Banco BTG Pactual S.A.">Banco BTG Pactual S.A.</option>
<option {{ old('banco_nome') == "Banco C6 S.A." ? "selected" : "" }} value="Banco C6 S.A.">Banco C6 S.A.</option>
<option {{ old('banco_nome') == "Banco Caixa Geral - Brasil S.A." ? "selected" : "" }} value="Banco Caixa Geral - Brasil S.A.">Banco Caixa Geral - Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Cargill S.A" ? "selected" : "" }} value="Banco Cargill S.A">Banco Cargill S.A.</option>
<option {{ old('banco_nome') == "Banco Caterpillar S.A." ? "selected" : "" }} value="Banco Caterpillar S.A.">Banco Caterpillar S.A.</option>
<option {{ old('banco_nome') == "Banco Cetelem S.A." ? "selected" : "" }} value="Banco Cetelem S.A.">Banco Cetelem S.A.</option>
<option {{ old('banco_nome') == "Banco Cifra S.A." ? "selected" : "" }} value="Banco Cifra S.A.">Banco Cifra S.A.</option>
<option {{ old('banco_nome') == "Banco Citibank S.A." ? "selected" : "" }} value="Banco Citibank S.A.">Banco Citibank S.A.</option>
<option {{ old('banco_nome') == "Banco CNH Industrial Capital S.A." ? "selected" : "" }} value="Banco CNH Industrial Capital S.A.">Banco CNH Industrial Capital S.A.</option>
<option {{ old('banco_nome') == "Banco Cooperativo do Brasil S.A. - BANCOOB" ? "selected" : "" }} value="Banco Cooperativo do Brasil S.A. - BANCOOB">Banco Cooperativo do Brasil S.A. - BANCOOB</option>
<option {{ old('banco_nome') == "Banco Cooperativo Sicredi S.A." ? "selected" : "" }} value="Banco Cooperativo Sicredi S.A.">Banco Cooperativo Sicredi S.A.</option>
<option {{ old('banco_nome') == "Banco Credit Agricole Brasil S.A." ? "selected" : "" }} value="Banco Credit Agricole Brasil S.A.">Banco Credit Agricole Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Credit Suisse (Brasil) S.A." ? "selected" : "" }} value="Banco Credit Suisse (Brasil) S.A.">Banco Credit Suisse (Brasil) S.A.</option>
<option {{ old('banco_nome') == "Banco CSF S.A." ? "selected" : "" }} value="Banco CSF S.A.">Banco CSF S.A.</option>
<option {{ old('banco_nome') == "Banco da Amazônia S.A." ? "selected" : "" }} value="Banco da Amazônia S.A.">Banco da Amazônia S.A.</option>
<option {{ old('banco_nome') == "Banco da China Brasil S.A." ? "selected" : "" }} value="Banco da China Brasil S.A.">Banco da China Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Daycoval S.A." ? "selected" : "" }} value="Banco Daycoval S.A.">Banco Daycoval S.A.</option>
<option {{ old('banco_nome') == "Banco de Lage Landen Brasil S.A." ? "selected" : "" }} value="Banco de Lage Landen Brasil S.A.">Banco de Lage Landen Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Digio S.A." ? "selected" : "" }} value="Banco Digio S.A.">Banco Digio S.A.</option>
<option {{ old('banco_nome') == "Banco do Brasil S.A." ? "selected" : "" }} value="Banco do Brasil S.A.">Banco do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco do Estado de Sergipe S.A." ? "selected" : "" }} value="Banco do Estado de Sergipe S.A.">Banco do Estado de Sergipe S.A.</option>
<option {{ old('banco_nome') == "Banco do Estado do Pará S.A." ? "selected" : "" }} value="Banco do Estado do Pará S.A.">Banco do Estado do Pará S.A.</option>
<option {{ old('banco_nome') == "Banco do Estado do Rio Grande do Sul S.A." ? "selected" : "" }}value="Banco do Estado do Rio Grande do Sul S.A.">Banco do Estado do Rio Grande do Sul S.A.</option>
<option {{ old('banco_nome') == "Banco do Nordeste do Brasil S.A." ? "selected" : "" }} value="Banco do Nordeste do Brasil S.A.">Banco do Nordeste do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Fator S.A." ? "selected" : "" }} value="Banco Fator S.A.">Banco Fator S.A.</option>
<option {{ old('banco_nome') == "Banco Fibra S.A." ? "selected" : "" }} value="Banco Fibra S.A.">Banco Fibra S.A.</option>
<option {{ old('banco_nome') == "Banco Ficsa S.A." ? "selected" : "" }} value="Banco Ficsa S.A.">Banco Ficsa S.A.</option>
<option {{ old('banco_nome') == "Banco Fidis S.A." ? "selected" : "" }} value="Banco Fidis S.A.">Banco Fidis S.A.</option>
<option {{ old('banco_nome') == "Banco Finaxis S.A." ? "selected" : "" }} value="Banco Finaxis S.A.">Banco Finaxis S.A.</option>
<option {{ old('banco_nome') == "Banco Ford S.A." ? "selected" : "" }} value="Banco Ford S.A.">Banco Ford S.A.</option>
<option {{ old('banco_nome') == "Banco GMAC S.A." ? "selected" : "" }} value="Banco GMAC S.A.">Banco GMAC S.A.</option>
<option {{ old('banco_nome') == "Banco Guanabara S.A." ? "selected" : "" }} value="Banco Guanabara S.A.">Banco Guanabara S.A.</option>
<option {{ old('banco_nome') == "Banco Honda S.A." ? "selected" : "" }} value="Banco Honda S.A.">Banco Honda S.A.</option>
<option {{ old('banco_nome') == "Banco IBM S.A." ? "selected" : "" }} value="Banco IBM S.A.">Banco IBM S.A.</option>
<option {{ old('banco_nome') == "Banco Inbursa S.A." ? "selected" : "" }} value="Banco Inbursa S.A.">Banco Inbursa S.A.</option>
<option {{ old('banco_nome') == "Banco Industrial do Brasil S.A." ? "selected" : "" }} value="Banco Industrial do Brasil S.A.">Banco Industrial do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Indusval S.A." ? "selected" : "" }} value="Banco Indusval S.A.">Banco Indusval S.A.</option>
<option {{ old('banco_nome') == "Banco Inter S.A." ? "selected" : "" }} value="Banco Inter S.A.">Banco Inter S.A.</option>
<option {{ old('banco_nome') == "Banco Investcred Unibanco S.A." ? "selected" : "" }} value="Banco Investcred Unibanco S.A.">Banco Investcred Unibanco S.A.</option>
<option {{ old('banco_nome') == "Banco Itaú BBA S.A." ? "selected" : "" }} value="Banco Itaú BBA S.A.">Banco Itaú BBA S.A.</option>
<option {{ old('banco_nome') == "Banco Itaú Consignado S.A." ? "selected" : "" }} value="Banco Itaú Consignado S.A.">Banco Itaú Consignado S.A.</option>
<option {{ old('banco_nome') == "Banco Itaú Veículos S.A." ? "selected" : "" }} value="Banco Itaú Veículos S.A.">Banco Itaú Veículos S.A.</option>
<option {{ old('banco_nome') == "Banco ItauBank S.A" ? "selected" : "" }} value="Banco ItauBank S.A">Banco ItauBank S.A</option>
<option {{ old('banco_nome') == "Banco Itaucard S.A." ? "selected" : "" }} value="Banco Itaucard S.A.">Banco Itaucard S.A.</option>
<option {{ old('banco_nome') == "Banco J. P. Morgan S.A." ? "selected" : "" }} value="Banco J. P. Morgan S.A.">Banco J. P. Morgan S.A.</option>
<option {{ old('banco_nome') == "Banco J. Safra S.A." ? "selected" : "" }} value="Banco J. Safra S.A.">Banco J. Safra S.A.</option>
<option {{ old('banco_nome') == "Banco John Deere S.A." ? "selected" : "" }} value="Banco John Deere S.A.">Banco John Deere S.A.</option>
<option {{ old('banco_nome') == "Banco Luso Brasileiro S.A." ? "selected" : "" }} value="Banco Luso Brasileiro S.A.">Banco Luso Brasileiro S.A.</option>
<option {{ old('banco_nome') == "Banco Mumbuca" ? "selected" : "" }} value="Banco Mumbuca">Banco Mumbuca</option>
<option {{ old('banco_nome') == "Banco Mercantil do Brasil S.A." ? "selected" : "" }} value="Banco Mercantil do Brasil S.A.">Banco Mercantil do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Mizuho do Brasil S.A." ? "selected" : "" }} value="Banco Mizuho do Brasil S.A.">Banco Mizuho do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Modal S.A." ? "selected" : "" }} value="Banco Modal S.A.">Banco Modal S.A.</option>
<option {{ old('banco_nome') == "Banco Moneo S.A." ? "selected" : "" }} value="Banco Moneo S.A.">Banco Moneo S.A.</option>
<option {{ old('banco_nome') == "Banco MUFG Brasil S.A." ? "selected" : "" }} value="Banco MUFG Brasil S.A.">Banco MUFG Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Olé Bonsucesso Consignado S.A." ? "selected" : "" }} value="Banco Olé Bonsucesso Consignado S.A.">Banco Olé Bonsucesso Consignado S.A.</option>
<option {{ old('banco_nome') == "Banco Original S.A." ? "selected" : "" }} value="Banco Original S.A.">Banco Original S.A.</option>
<option {{ old('banco_nome') == "Banco PAN S.A." ? "selected" : "" }} value="Banco PAN S.A.">Banco PAN S.A.</option>
<option {{ old('banco_nome') == "Banco Paulista S.A." ? "selected" : "" }} value="Banco Paulista S.A.">Banco Paulista S.A.</option>
<option {{ old('banco_nome') == "Banco Pine S.A." ? "selected" : "" }} value="Banco Pine S.A.">Banco Pine S.A.</option>
<option {{ old('banco_nome') == "Banco Rabobank International Brasil S.A." ? "selected" : "" }} value="Banco Rabobank International Brasil S.A.">Banco Rabobank International Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco RCI Brasil S.A." ? "selected" : "" }} value="Banco RCI Brasil S.A.">Banco RCI Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Rendimento S.A." ? "selected" : "" }} value="Banco Rendimento S.A.">Banco Rendimento S.A.</option>
<option {{ old('banco_nome') == "Banco Rodobens S.A." ? "selected" : "" }} value="Banco Rodobens S.A.">Banco Rodobens S.A.</option>
<option {{ old('banco_nome') == "Banco Safra S.A." ? "selected" : "" }} value="Banco Safra S.A.">Banco Safra S.A.</option>
<option {{ old('banco_nome') == "Banco Santander  (Brasil)  S.A." ? "selected" : "" }} value="Banco Santander  (Brasil)  S.A.">Banco Santander  (Brasil)  S.A.</option>
<option {{ old('banco_nome') == "Banco Semear S.A." ? "selected" : "" }} value="Banco Semear S.A.">Banco Semear S.A.</option>
<option {{ old('banco_nome') == "Banco Smartbank S.A." ? "selected" : "" }} value="Banco Smartbank S.A.">Banco Smartbank S.A.</option>
<option {{ old('banco_nome') == "Banco Société Générale Brasil S.A." ? "selected" : "" }} value="Banco Société Générale Brasil S.A.">Banco Société Générale Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Stone Pagamentos S.A." ? "selected" : "" }} value="Banco Stone Pagamentos S.A.">Banco Stone Pagamentos S.A.</option>
<option {{ old('banco_nome') == "Banco Sumitomo Mitsui Brasileiro S.A." ? "selected" : "" }} value="Banco Sumitomo Mitsui Brasileiro S.A.">Banco Sumitomo Mitsui Brasileiro S.A.</option>
<option {{ old('banco_nome') == "Banco Topázio S.A." ? "selected" : "" }} value="Banco Topázio S.A.">Banco Topázio S.A.</option>
<option {{ old('banco_nome') == "Banco Toyota do Brasil S.A." ? "selected" : "" }} value="Banco Toyota do Brasil S.A.">Banco Toyota do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Triângulo S.A." ? "selected" : "" }} value="Banco Triângulo S.A.">Banco Triângulo S.A.</option>
<option {{ old('banco_nome') == "Banco Volvo Brasil S.A." ? "selected" : "" }} value="Banco Volvo Brasil S.A.">Banco Volvo Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco Votorantim S.A." ? "selected" : "" }} value="Banco Votorantim S.A.">Banco Votorantim S.A.</option>
<option {{ old('banco_nome') == "Banco VR S.A." ? "selected" : "" }} value="Banco VR S.A.">Banco VR S.A.</option>
<option {{ old('banco_nome') == "Banco Western Union do Brasil S.A." ? "selected" : "" }} value="Banco Western Union do Brasil S.A.">Banco Western Union do Brasil S.A.</option>
<option {{ old('banco_nome') == "Banco XP S.A." ? "selected" : "" }} value="Banco XP S.A.">Banco XP S.A.</option>
<option {{ old('banco_nome') == "Banco Yamaha Motor do Brasil S.A." ? "selected" : "" }} value="Banco Yamaha Motor do Brasil S.A.">Banco Yamaha Motor do Brasil S.A.</option>
<option {{ old('banco_nome') == "BancoSeguro S.A." ? "selected" : "" }} value="BancoSeguro S.A.">BancoSeguro S.A.</option>
<option {{ old('banco_nome') == "BANESTES S.A. Banco do Estado do Espírito Santo" ? "selected" : "" }} value="BANESTES S.A. Banco do Estado do Espírito Santo">BANESTES S.A. Banco do Estado do Espírito Santo</option>
<option {{ old('banco_nome') == "Bank of America Merrill Lynch Banco Múltiplo S.A." ? "selected" : "" }} value="Bank of America Merrill Lynch Banco Múltiplo S.A.">Bank of America Merrill Lynch Banco Múltiplo S.A.</option>
<option {{ old('banco_nome') == "BCV - Banco de Crédito e Varejo S.A." ? "selected" : "" }} value="BCV - Banco de Crédito e Varejo S.A.">BCV - Banco de Crédito e Varejo S.A.</option>
<option {{ old('banco_nome') == "BEXS Banco de Câmbio S.A." ? "selected" : "" }} value="BEXS Banco de Câmbio S.A.">BEXS Banco de Câmbio S.A.</option>
<option {{ old('banco_nome') == "BNY Mellon Banco S.A." ? "selected" : "" }} value="BNY Mellon Banco S.A.">BNY Mellon Banco S.A.</option>
<option {{ old('banco_nome') == "BRB - Banco de Brasília S.A." ? "selected" : "" }} value="BRB - Banco de Brasília S.A.">BRB - Banco de Brasília S.A.</option>
<option {{ old('banco_nome') == "Caixa Econômica Federal" ? "selected" : "" }} value="Caixa Econômica Federal">Caixa Econômica Federal</option>
<option {{ old('banco_nome') == "China Construction Bank (Brasil) Banco Múltiplo S.A." ? "selected" : "" }} value="China Construction Bank (Brasil) Banco Múltiplo S.A.">China Construction Bank (Brasil) Banco Múltiplo S.A.</option>
<option {{ old('banco_nome') == "Citibank N.A." ? "selected" : "" }} value="Citibank N.A.">Citibank N.A.</option>
<option {{ old('banco_nome') == "Deutsche Bank S.A. - Banco Alemão" ? "selected" : "" }} value="Deutsche Bank S.A. - Banco Alemão">Deutsche Bank S.A. - Banco Alemão</option>
<option {{ old('banco_nome') == "Goldman Sachs do Brasil Banco Múltiplo S.A." ? "selected" : "" }} value="Goldman Sachs do Brasil Banco Múltiplo S.A.">Goldman Sachs do Brasil Banco Múltiplo S.A.</option>
<option {{ old('banco_nome') == "Hipercard Banco Múltiplo S.A." ? "selected" : "" }} value="Hipercard Banco Múltiplo S.A.">Hipercard Banco Múltiplo S.A.</option>
<option {{ old('banco_nome') == "HSBC Brasil S.A. - Banco de Investimento" ? "selected" : "" }} value="HSBC Brasil S.A. - Banco de Investimento">HSBC Brasil S.A. - Banco de Investimento</option>
<option {{ old('banco_nome') == "ING Bank N.V." ? "selected" : "" }} value="ING Bank N.V.">ING Bank N.V.</option>
<option {{ old('banco_nome') == "Itaú Unibanco Holding S.A." ? "selected" : "" }} value="Itaú Unibanco Holding S.A.">Itaú Unibanco Holding S.A.</option>
<option {{ old('banco_nome') == "Itaú Unibanco S.A." ? "selected" : "" }} value="Itaú Unibanco S.A.">Itaú Unibanco S.A.</option>
<option {{ old('banco_nome') == "JPMorgan Chase Bank, National Association" ? "selected" : "" }} value="JPMorgan Chase Bank, National Association">JPMorgan Chase Bank, National Association</option>
<option {{ old('banco_nome') == "Kirton Bank S.A. - Banco Múltiplo" ? "selected" : "" }} value="Kirton Bank S.A. - Banco Múltiplo">Kirton Bank S.A. - Banco Múltiplo</option>
<option {{ old('banco_nome') == "MS Bank S.A. Banco de Câmbio" ? "selected" : "" }} value="MS Bank S.A. Banco de Câmbio">MS Bank S.A. Banco de Câmbio</option>
<option {{ old('banco_nome') == "Nu Pagamentos S.A." ? "selected" : "" }} value="Nu Pagamentos S.A.">Nu Pagamentos S.A.</option>
<option {{ old('banco_nome') == "Paraná Banco S.A." ? "selected" : "" }} value="Paraná Banco S.A.">Paraná Banco S.A.</option>
<option {{ old('banco_nome') == "Plural S.A. - Banco Múltiplo" ? "selected" : "" }} value="Plural S.A. - Banco Múltiplo">Plural S.A. - Banco Múltiplo</option>
<option {{ old('banco_nome') == "Scania Banco S.A." ? "selected" : "" }} value="Scania Banco S.A.">Scania Banco S.A.</option>
<option {{ old('banco_nome') == "Scotiabank Brasil S.A. Banco Múltiplo" ? "selected" : "" }} value="Scotiabank Brasil S.A. Banco Múltiplo">Scotiabank Brasil S.A. Banco Múltiplo</option>
<option {{ old('banco_nome') == "Travelex Banco de Câmbio S.A." ? "selected" : "" }} value="Travelex Banco de Câmbio S.A.">Travelex Banco de Câmbio S.A.</option>
<option {{ old('banco_nome') == "UBS Brasil Banco de Investimento S.A." ? "selected" : "" }} value="UBS Brasil Banco de Investimento S.A.">UBS Brasil Banco de Investimento S.A.</option>