@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h3>
                <i class="fa fa-home mb-3"></i> Fomenta Maricá
            </h3>

            @if (session()->has('success'))
                <div class="alert alert-success mt-3">
                    <i class="fa fa-check-square-o"></i> {{ session('success') }}
                </div>
            @endif

            <form method="POST" name="formSalvarFomenta" action="{{ route('fomentaCadastroSalvar') }}" autocomplete="off">
                @csrf
   
                <input type="hidden" name="verificar_cpf_cnpj" value="{{ mb_strlen(remove_character_document($cnpj)) == 11 ? 1 : 2 }}" />
                <input type="hidden" name="cpf_ou_cnpj" value="{{ $cnpj }}" />

                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <b>ATENÇÃO:</b><br><br>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            
                <div id="accordion">
                    
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#credito">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Crédito Emergencial
                            </div>
                        </a>
                        <div id="credito" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("social.fomenta.credito.credito_emergencial")
                            </div>
                        </div>
                    </div>
                    
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#requerente">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                            </div>
                        </a>
                        <div id="requerente" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("social.fomenta.requerente.requerente")
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#endereco_requerente_parte">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Endereço Requerente
                            </div>
                        </a>
                        <div id="endereco_requerente_parte" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("social.fomenta.endereco_requerente.endereco_requerente")
                            </div>
                        </div>
                    </div>

                    @if (mb_strlen(remove_character_document($cnpj)) == 14)
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados_mercantis">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados da Empresa
                                </div>
                            </a>
                            <div id="dados_mercantis" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("social.fomenta.dados_mercantis.dados_mercantis")
                                </div>
                            </div>
                        </div>
                    @endif
                
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#informacoes_contato">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Informações de Contato
                            </div>
                        </a>
                        <div id="informacoes_contato" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("social.fomenta.informacoes_contato.informacoes_contato")
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#endereco_cgm">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Endereço
                            </div>
                        </a>
                        <div id="endereco_cgm" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("social.fomenta.endereco.endereco")
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <p>
                                Declaro para fins de direito que faço jus ao Programa Fomenta Maricá MEI Emergencial Covid-19 pois exerço atividade econômica como Microempreendedor no Município de Maricá e desejo utilizar esta linha de crédito para suavizar os impactos do isolamento social necessário ao enfrentamento da pandemia do coronavírus. <span class="text-danger">*</span>
                            </p>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" name="autodeclaracao" 
                                        class="form-check-input" value="sim" required>
                                    Li e concordo.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <p>
                                Declaro que são VERDADEIRAS e EXATAS todas as informações que foram 
                                prestadas neste formulário. Declaro ainda estar ciente de que declaração 
                                falsa no presente cadastro constituirá crime de 
                                falsidade ideológica (art. 299 do Código Penal) e estará sujeita a sanções penais 
                                sem prejuízo de medidas administrativas e outras. <span class="text-danger">*</span>
                            </p>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" name="confirmacao_de_informacoes" 
                                        class="form-check-input" value="sim" required>
                                    Li e concordo.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ route('cadastrofomenta') }}" class="btn btn-primary mt-3">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                    
                        <button class="btn btn-success mt-3 enviar_informacoes">Enviar Informações <i class="fa fa-send"></i></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script>
    $(function() {

        // Spinner
        var load = $(".ajax_load");

        $("#valorSolicitado").mask("0.000,00", {reverse: true});
        $("#valorParcela").mask("0.000,00", {reverse: true});
        $("#tempoCarencia").mask("00");
        $("#prazo").mask("00");

        @if(empty($errors->all()))
            $("#credito").collapse('show');
        @endif

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#cpf_requerente").on("change", function() {
            let cpf = $(this).val();

            if (!validarCPF(cpf)) {
                alert("Atenção: CPF Inválido! Informe corretamente o número do seu CPF.");
                $(this).val("");
                $(this).focus();
                return;
            }
        });

        validarTodosCamposEmail();

        function validarTodosCamposEmail() {

            $("input[type=email]").on("change", function() {
                let email = $(this).val();

                if (!validaEmail(email)) {
                    alert("Atenção: O formato do E-mail é inválido.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        validarEmailsCorrespondem();

        function validarEmailsCorrespondem() {

            $("input[name=confirmacao_email]").on("change", function() {
                
                let email = $("input[name=email]").val();
                let confirmacao_email = $(this).val();

                if (email !== confirmacao_email) {
                    alert("Atenção: Os e-mails não correspondem.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $(this).data("descricao");
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");
            
            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);

            $.ajax({
                url: "{{ route('salvar_documento_servidor_fomenta') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-excluir-documento", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let nome_arquivo = $(this).data("nome_arquivo");
            let nome_sessao = $(this).data("nome_sessao");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = $(this).data("nome_tabela");
            let pasta = $(this).data("pasta");
            let conteudo_documentos = $(this).data("conteudo_documentos");

            $.ajax({
                url: "{{ route('excluir_documento_servidor_fomenta') }}",
                method: 'POST',
                data: {
                    "nome_arquivo": nome_arquivo,
                    "nome_sessao": nome_sessao,
                    "pasta": pasta,
                    "nome_tabela": nome_tabela,
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });
    });

    // $("#motivoSolicitacao").on("blur", function(e) {
    //     if($("#motivoSolicitacao").val() == ""){
    //         alert("Atenção: Informe o motivo da solicitação do empréstimo através do programa Fomenta Maricá - (Crédito Emergencial)"); 
    //         $("#credito").collapse('show');
    //         $("#motivoSolicitacao").focus();
    //         return false;
    //     }
    // });

    $("#valorSolicitado").on("blur", function(e) {
        $valorSolicitado = $("#valorSolicitado").val().replace(".", "");
        $valorSolicitado = $valorSolicitado.replace(",", ".");

        if(Number($valorSolicitado) > 0 && Number($valorSolicitado) < 300){
            alert("Atenção: O valor mínimo solicitado é de $ 300,00  - (Crédito Emergencial)"); 
            $("#valorSolicitado").val("");
            $("#credito").collapse('show');
            $("#valorSolicitado").focus();
            return false;
        }

        if(Number($valorSolicitado) > 5000){
            alert("Atenção: O valor máximo solicitado é de $ 5.000,00  - (Crédito Emergencial)"); 
            $("#valorSolicitado").val("");
            $("#credito").collapse('show');
            $("#valorSolicitado").focus();
            return false;
        }
    });

    $("#tempoCarencia").on("blur", function(e) {

        if(Number($("#tempoCarencia").val()) > 12){
            alert("Atenção: O tempo máximo de carência é de 12 meses - (Crédito Emergencial)");
            $("#tempoCarencia").val("");
            $("#credito").collapse('show');
            $("#tempoCarencia").focus(); 
            return false;
        }
    });

    $("#prazo").on("blur", function(e) {

        if($("#prazo").val().length > 0){
            if(Number($("#prazo").val()) == 0){
                alert("Atenção: informe o prazo - (Crédito Emergencial)");
                $("#prazo").val(""); 
                $("#credito").collapse('show');
                $("#prazo").focus(); 
                return false;
            }
        }
        if(Number($("#prazo").val()) > 18){
            alert("Atenção: O prazo máximo é de 18 meses - (Crédito Emergencial)");
            $("#prazo").val(""); 
            $("#credito").collapse('show');
            $("#prazo").focus(); 
            return false;
        }
    });

    $("#valorParcela").on("blur", function(e) {
        $valorParcela = $("#valorParcela").val().replace(".", "");
        $valorParcela = $valorParcela.replace(",", ".");
        
        $valorSolicitado = $("#valorSolicitado").val().replace(".", "");
        $valorSolicitado = $valorSolicitado.replace(",", ".");

        if(Number($valorParcela) > Number($valorSolicitado)){
            alert("Atenção: O valor da parcela não pode ser superior ao valor solicitado ($ " + $("#valorSolicitado").val() +  ")  - (Crédito Emergencial)"); 
            $("#valorParcela").val("");
            $("#credito").collapse('show');
            $("#valorParcela").focus();
            return false;
        }
    });

    function validaCreditoEmergencial(){

        if( $("#motivoSolicitacao").val() == "" ){
            alert("Atenção: Informe o motivo da solicitação do empréstimo através do programa Fomenta Maricá - (Crédito Emergencial)"); 
            $("#credito").collapse('show');
            $("#motivoSolicitacao").focus();
            return false;
        }

        $valorSolicitado = $("#valorSolicitado").val().replace(".", "");
        $valorSolicitado = $valorSolicitado.replace(",", ".");

        if(Number($valorSolicitado) == 0){
            alert("Atenção: Informe o valor solicitado para o benefício (de $ 300 a $ 5.000,00) - (Crédito Emergencial)"); 
            $("#credito").collapse('show');
            $("#valorSolicitado").val("");
            $("#valorSolicitado").focus();
            return false;
        }

        if(Number($valorSolicitado) < 300){
            alert("Atenção: O valor mínimo solicitado é de $ 300,00  - (Crédito Emergencial)"); 
            $("#credito").collapse('show');
            $("#valorSolicitado").val("");
            $("#valorSolicitado").focus();
            return false;
        }

        if(Number($valorSolicitado) > 5000){
            alert("Atenção: O valor máximo solicitado é de $ 5.000,00  - (Crédito Emergencial)"); 
            $("#credito").collapse('show');
            $("#valorSolicitado").val("");
            $("#valorSolicitado").focus();
            return false;
        }

        // if(Number($("#tempoCarencia").val()) == 0){
        //     alert("Atenção: Informe o tempo de carência do benefício (tempo máximo de 12 meses) - (Crédito Emergencial)");
        //     $("#credito").collapse('show');
        //     $("#tempoCarencia").val(""); 
        //     $("#tempoCarencia").focus(); 
        //     return false;
        // }

        if(Number($("#tempoCarencia").val()) > 12){
            alert("Atenção: O tempo máximo de carência é de 12 meses - (Crédito Emergencial)");
            $("#credito").collapse('show');
            $("#tempoCarencia").val(""); 
            $("#tempoCarencia").focus(); 
            return false;
        }

        if(Number($("#prazo").val()) == 0 ){
            alert("Atenção: Informe o prazo para o pagamento do benefício (tempo máximo de 18 meses) - (Crédito Emergencial)");
            $("#credito").collapse('show');
            $("#prazo").val(""); 
            $("#prazo").focus(); 
            return false;
        }

        if(Number($("#prazo").val()) > 18){
            alert("Atenção: O prazo máximo é de 18 meses - (Crédito Emergencial)");
            $("#credito").collapse('show');
            $("#prazo").val("");
            $("#prazo").focus(); 
            return false;
        }

        $valorParcela = $("#valorParcela").val().replace(".", "");
        $valorParcela = $valorParcela.replace(",", ".");
        
        if(Number($valorParcela) == 0){
            alert("Atenção: Informe o valor da parcela - (Crédito Emergencial)");
            $("#credito").collapse('show');
            $("#valorParcela").focus(); 
            return false;
        }

        if(Number($valorParcela) > Number($valorSolicitado)){
            alert("Atenção: O valor da parcela não pode ser superior ao valor solicitado ($ " + $("#valorSolicitado").val() +  ")  - (Crédito Emergencial)"); 
            $("#credito").collapse('show');
            $("#valorParcela").focus();
            return false;
        }


        return true;
    }

    verificarDocumentosNecessariosEcgmResideEmMarica();

    function verificarDocumentosNecessariosEcgmResideEmMarica() {
        $(".enviar_informacoes").on("click", function(e) {
            e.preventDefault();

            if(!validaCreditoEmergencial()){
                return false;
            }

            let documentos_necessarios = [];

            verificarNecessidadeAdicionarDocumentosRequerente(documentos_necessarios);
            adicionarDocumentosEnderecoRequerente(documentos_necessarios);
            adicionarDocumentosInformacoesPessoaisOuDadosMercantis(documentos_necessarios);
            adicionarDocumentosEndereco(documentos_necessarios);

            verificarEnvioDocumentosEcgmResideEmMarica(documentos_necessarios);
        });
    }

    function verificarNecessidadeAdicionarDocumentosRequerente(documentos_necessarios) {
        documentos_necessarios.push({
            "nome_sessao": "documentos_requerente",
            "data_descricao":"CPF Requerente",
            "descricao": "CPF Requerente em (Documentos do Requerente)",
            "qtd": 1
        });
        documentos_necessarios.push({
            "nome_sessao": "documentos_requerente",
            "data_descricao":"Identidade Requerente",
            "descricao": "Identidade Requerente em (Documentos do Requerente)",
            "qtd": 1
        });
        documentos_necessarios.push({
            "nome_sessao": "documentos_requerente",
            "data_descricao":"Selfie Requerente",
            "descricao": "Foto Selfie do Requerente em (Documentos do Requerente)",
            "qtd": 1
        });
    }

    function adicionarDocumentosEndereco(documentos_necessarios) {
        documentos_necessarios.push({
            "nome_sessao": "documento_endereco",
            "data_descricao":"Comprovante de Endereço",
            "descricao": "Comprovante de Residência em (Endereço)",
            "qtd": 1
        });
    }

    function adicionarDocumentosEnderecoRequerente(documentos_necessarios) {
        documentos_necessarios.push({
            "nome_sessao": "documento_endereco_requerente",
            "data_descricao":"Comprovante de Endereço Requerente",
            "descricao": "Comprovante de Endereço Requerente em (Endereço do Requerente)",
            "qtd": 1
        });
    }

    function adicionarDocumentosInformacoesPessoaisOuDadosMercantis(documentos_necessarios) {
        
        // documentos_necessarios.push({
        //     "nome_sessao": "documentos_dados_mercantis",
        //     "data_descricao":"Documento de Identidade",
        //     "descricao": "Documento de Identidade em (Dados da Empresa)",
        //     "qtd": 1
        // });
        // documentos_necessarios.push({
        //     "nome_sessao": "documentos_dados_mercantis",
        //     "data_descricao":"CPF",
        //     "descricao": "CPF do Sócio em (Dados da Empresa)",
        //     "qtd": 1
        // });
        documentos_necessarios.push({
            "nome_sessao": "documentos_dados_mercantis",
            "data_descricao":"Comprovante MEI",
            "descricao": "Comprovante MEI em (Dados da Empresa)",
            "qtd": 1
        });
        documentos_necessarios.push({
            "nome_sessao": "documentos_dados_mercantis",
            "data_descricao":"Documento Probatório da Atividade",
            "descricao": "Documento Probatório da Atividade em (Dados da Empresa)",
            "qtd": 1
        });
        documentos_necessarios.push({
            "nome_sessao": "documentos_dados_mercantis",
            "data_descricao":"Foto do Empreendimento",
            "descricao": "Foto do Empreendimento em (Dados da Empresa)",
            "qtd": 1
        });

    }

    function verificarEnvioDocumentosEcgmResideEmMarica(documentos_necessarios) {
        $.ajax({
            url: "{{ route('verificar_envio_documentos_fomenta') }}",
            method: 'POST',
            data: {
                "documentos_necessarios": documentos_necessarios
            },
            success(response) {

                if (response.documentos_em_falta.length > 0) {
                    for (let i = 0; i < response.documentos_em_falta.length; i++) {
                        alert("Atenção: Os seguintes documentos são necessários: " + response.documentos_em_falta[i].descricao);
                    }
                } 
                else {
                    $("form[name=formSalvarFomenta]").submit();
                }
            }
        });
    }

        
    function limpaFormularioCepRequerente() {
        document.getElementById('endereco_requerente').value = ("");
        document.getElementById('bairro_requerente').value = ("");
        document.getElementById('municipio_requerente').value = ("");
        document.getElementById('estado_requerente').value = ("");
    }
    
    function meuCallbackRequerente(conteudo) {

        if (!("erro" in conteudo)) {
            if(conteudo.localidade.toUpperCase() == "MARICÁ" || conteudo.localidade.toUpperCase() == "MARICA"  ){
                document.getElementById('endereco_requerente').value = (conteudo.logradouro);
                document.getElementById('bairro_requerente').value = (conteudo.bairro);
                document.getElementById('municipio_requerente').value = (conteudo.localidade);
                document.getElementById('estado_requerente').value = (conteudo.uf);
            } else {
                limpaFormularioCepRequerente();
                alert("Somente endereços do município de Maricá são permitidos");
            }
        } else {
            limpaFormularioCepRequerente();
            alert("CEP não encontrado.");
        }
    }

    function pesquisaCepRequerente(cep) {

        var cep = cep.replace(/\D/g, '');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $('.ajax_load').fadeIn(200);

                var script = document.createElement('script');

                script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meuCallbackRequerente';

                document.body.appendChild(script);

                $('.ajax_load').fadeOut(200);
            } else {
                limpaFormularioCepRequerente();
                alert("Formato de CEP inválido.");

            }
        } else {
            limpaFormularioCepRequerente();
        }
    }
       
</script>

@endsection