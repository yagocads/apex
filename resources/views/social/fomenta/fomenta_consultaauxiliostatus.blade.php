@extends('layouts.tema_principal')

@section('content')
<style>
    .fase {
        width: 100% !important;
        margin-bottom: 20px;
    }

    .alert {
        margin-top: 15px !important;
        margin-bottom: 15px !important;
    }

    .registro-fase {
        border-bottom: 1px solid #227DC7;
        padding-top: 5px;
        padding-bottom: 10px;
    }

    .registro-fase-topo {
        padding-top: 30px;
    }
</style>


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h2>
                <i class="fa fa-file-o mb-3"></i> Fomenta Maricá
            </h2>

            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <div class="card">
                <div class="card-header">
                    <h4>Consulta de Solicitação de Crédito</h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-2">
                            Chamado: <b>{{ $processo->chamado }}</b>
                        </div>
                        <div class="col-lg-2">
                            Data: <b>{{ date('d/m/Y', strtotime($processo->abertura)) }}</b>
                        </div>
                        <div class="col-lg-4">
                            Serviço: <b>Fomenta Maricá - Solicitação de Crédito</b>
                        </div>
                        <div class="col-lg-4">
                            Responsável: <b>{{ $processo->responsavel_consulta }}</b><br/>
                        </div>
                    </div>
                    <div class="row  align-items-center">
                        <div class="col-lg-12">
                            @switch($processo->fase)
                                @case('Revisão Documentação - PMM')
                                    <img src="{{ asset('img/fases_fomenta_01.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Sanar Pendências')
                                    <img src="{{ asset('img/fases_fomenta_02.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Análise de Crédito - AgeRio')
                                    <img src="{{ asset('img/fases_fomenta_03.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Solicitante Negativado')
                                    <img src="{{ asset('img/fases_fomenta_04.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Realizar Agendamento')
                                    <img src="{{ asset('img/fases_fomenta_05.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Elaboração do Contrato')
                                    <img src="{{ asset('img/fases_fomenta_06.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Assinatura da Documentação')
                                    <img src="{{ asset('img/fases_fomenta_07.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Regularização da Empresa')
                                    <img src="{{ asset('img/fases_fomenta_08.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Análise da Regularização')
                                    <img src="{{ asset('img/fases_fomenta_09.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Sanção Aplicada')
                                    <img src="{{ asset('img/fases_fomenta_10.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Débitos Regularizados')
                                    <img src="{{ asset('img/fases_fomenta_11.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Pagamento Realizado')
                                @case('Liberação do Pagamento')
                                    <img src="{{ asset('img/fases_fomenta_12.png') }}" alt="Fase" class="fase" />
                                    @break
                                @case('Solicitação Reprovada')
                                    <img src="{{ asset('img/fases_fomenta_13.png') }}" alt="Fase" class="fase" />
                                    @break
                            @endswitch
                        </div>
                    </div>

                    
                    <div class="row mt-3">
                        <div class="col-lg-2">
                            Nome do Solicitante:
                        </div>
                        <div class="col-lg-5">
                            <strong>{{$usuario->nome_empresarial}}</strong>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-2">
                            Data da Solicitação:
                        </div>
                        <div class="col-lg-5">
                            <strong>{{date('d/m/Y H:i:s', strtotime($usuario->data_cadastro))}}</strong>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-2">
                            CNPJ da Empresa:
                        </div>
                        <div class="col-lg-5">
                            <strong>{{$usuario->cnpj}}</strong>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-2">
                            Protocolo:
                        </div>
                        <div class="col-lg-5">
                            <strong>{{$usuario->protocolo}}</strong>
                        </div>
                    </div>

                    @if($processo->fase == 'Solicitação Reprovada' || $processo->fase == 'Sanar Pendências')
                        <div class="row mt-2">
                            <div class="col-lg-2">
                                Motivo:
                            </div>
                            <div class="col-lg-5">
                                <strong>{{$processo->motivo}}</strong>
                            </div>
                        </div>
                        <br/>
                        <br/>
                    @endif

                    @if(
                        $processo->fase == 'Pagamento Realizado' ||
                        $processo->fase == 'Solicitação Reprovada' ||
                        $processo->fase == 'Análise de Crédito - AgeRio' ||
                        $processo->fase == 'Solicitante Negativado' ||
                        $processo->fase == 'Liberação do Pagamento' ||
                        $processo->fase == 'Assinatura da Documentação' ||
                        $processo->fase == 'Elaboração do Contrato' ||
                        $processo->fase == 'Sanção Aplicada' ||
                        $processo->fase == 'Regularização da Empresa' ||
                        $processo->fase == 'Débitos Regularizados' ||
                        $processo->fase == 'Análise da Regularização' ||
                        $processo->fase == 'Revisão Documentação - PMM'
                    )
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{ route('fomentaconsultarsituacao') }}" class="btn btn-primary mt-3">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>
                            </div>
                        </div>
                    @endif

                </div>  
            </div>  
            <br/>

            @if(($processo->fase == 'Sanar Pendências') && ($processo->finalizado == 'andamento' || $processo->finalizado == 'reprovado') )
                <div class="card">
                    <div class="card-header">
                        <h4>Sanar Pendências</h4>
                    </div>

                    <div class="card-body">
                        @if( isset($recurso->cnpj) )
                            <div class="alert alert-info mt-3">
                                Já foi efetuada uma solicitação para Sanar Pendências em {{date('d/m/Y H:i:s', strtotime($recurso->data))}}
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{ route('fomentaconsultarsituacao') }}" class="btn btn-primary mt-3">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </a>
                                </div>
                            </div>
                        @else
                            <form method="POST" action="{{ route('fomenta_recurso') }}" id="fomenta_recurso" name="fomenta_recurso">
                                @csrf 
                                <input id="cnpj" type="hidden" name="cnpj" value="{{ $usuario->cnpj }}" />
                                <input id="processo" type="hidden" name="processo" value="{{ $processo->chamado }}" />
                                <input id="etapa" type="hidden" name="etapa" value="{{ $processo->etapa }}" />
                                <input id="ciclo" type="hidden" name="ciclo" value="{{ $processo->ciclo }}" />
                                <input id="id_fomenta" type="hidden" name="id_fomenta" value="{{ $usuario->id }}" />

                                @include("social.fomenta.recurso.recurso")
                                

                            </form>

                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{ route('fomentaconsultarsituacao') }}" class="btn btn-primary mt-3">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </a>
                                
                                    <button class="btn btn-success mt-3" id="btn_enviar">Enviar Solicitação <i class="fa fa-send"></i></button>
                                </div>
                            </div>
                            
                        @endif
                    </div>
                </div>
            @elseif(($processo->fase == 'Realizar Agendamento') && ($processo->finalizado == 'andamento' || $processo->finalizado == 'reprovado') )
                <div class="card">
                    <div class="card-header">
                        <h4>Realizar Agendamento</h4>
                    </div>

                    <div class="card-body">
                        @if( isset($agendamento->cnpj) )
                            <div class="alert alert-info mt-3">
                                <center>
                                    <h4>Já foi efetuado o agendamento:</h4><br>
                                </center>
                                 Local: <b>{{$agendamento->local_agendamento}}</b><br/>
                                 Data do Agendamento: <b>{{date('d/m/Y', strtotime($agendamento->data_agendamento))}}</b><br/>
                                 Hora do Agendamento: <b>{{$agendamento->hora_agendamento}}</b><br/><br/>
                                 Data de efetivação: <b>{{date('d/m/Y H:i:s', strtotime($agendamento->data))}}</b><br/>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{ route('fomentaconsultarsituacao') }}" class="btn btn-primary mt-3">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </a>
                                </div>
                            </div>
                        @else

                            @include("social.fomenta.agendamento.agendamento")
                                
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{ route('fomentaconsultarsituacao') }}" class="btn btn-primary mt-3">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </a>
                                
                                    {{-- <button class="btn btn-success mt-3" id="btn_agendar">Realizar Agendamento <i class="fa fa-send"></i></button> --}}
                                </div>
                            </div>
                            
                        @endif
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>


@endsection

@section('post-script')

<script type="text/javascript">
    $(function() {

        // Spinner
        var load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            if($("#DescrDoc").val() == ""){
                alert("Informe o tipo de documento que deseja enviar.");
                $("#DescrDoc").focus();
                return false;
            }

            let cpf_ou_cnpj = $("input[name=cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $("#DescrDoc").val();
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");
            let destino = "fomenta_recursos";
            
            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);
            formData.append("destino", destino);

            $.ajax({
                url: "{{ route('salvar_documento_servidor_fomenta') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });
        
        $("body").on("click", ".btn-agendar-horario", function(e) {
            e.preventDefault();
            
            if (
                !confirm("Confima o Agendamento?\n" + 
                "\nDia: " + $("#datas_disponiveis").val() + 
                "\nLocal: " + $("#local_agendamento").val() + 
                "\nHorário: " + $(this).data("horario") +
                "\nPosto: " + $(this).data("posto") ) 
                ){
                return false;
            }

            $("#local_agendamento1").val($("#local_agendamento").val());
            $("#data_agendamento").val($("#datas_disponiveis").val());
            $("#hora_agendamento").val($(this).data("horario"))
            $("#posto_agendamento").val( $(this).data("posto"))

            $("#fomenta_agendamento").submit();


        });

        $("body").on("click", ".btn-excluir-documento", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let nome_arquivo = $(this).data("nome_arquivo");
            let nome_sessao = $(this).data("nome_sessao");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = $(this).data("nome_tabela");
            let pasta = $(this).data("pasta");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let destino = "fomenta_recursos";

            $.ajax({
                url: "{{ route('excluir_documento_servidor_fomenta') }}",
                method: 'POST',
                data: {
                    "nome_arquivo": nome_arquivo,
                    "nome_sessao": nome_sessao,
                    "pasta": pasta,
                    "nome_tabela": nome_tabela,
                    "destino": destino
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("#btn_enviar").click(function(){

            if($("#justificativa_recurso").val() == ""){
                alert("Por favor preencha as informações para Sanar Pendências");
                $("#justificativa_recurso").focus();
                return false; 
            }

            let documentos_necessarios = [];

            documentos_necessarios.push({
                "nome_sessao": "documentos_recurso",
                "qtd": 1
            });

            verificarEnvioDocumentosRecurso(documentos_necessarios)
            
        });

        function verificarEnvioDocumentosRecurso(documentos_necessarios) {
            $.ajax({
                url: "{{ route('verificar_envio_documentos_fomenta_recurso') }}",
                method: 'POST',
                data: {
                    "documentos_necessarios": documentos_necessarios
                },
                success(response) {

                    if (response.documentos_enviados == 0) {
                        alert("Atenção: É necessário enviar o(s) documento(s) para Sanar Pendências.");
                    } 
                    else {
                        $("#fomenta_recurso").submit();
                    }
                }
            });
        }

        $("#local_agendamento").change(function(){
            $(".conteudo_horario").html("@include('social.fomenta.agendamento.tabela_horarios')");

            if ( $("#local_agendamento").val() == "" ){
                $("#datas_disponiveis").empty();
                $("#datas_disponiveis").html('<option value="">Selecione...</option>');
            }
            else{
                

                let $localAgendamento = $("#local_agendamento").val();

                let formData = new FormData();

                formData.append("localAgendamento", $localAgendamento);

                $.ajax({
                    url: "{{ route('fomenta_consulta_datas_disponiveis') }}",
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {

                        load.fadeOut(200);

                        if (response.error) {
                            alert("ATENÇÃO: " + response.error);
                            return false;
                        }
                        $("#datas_disponiveis").empty();
                        $.each(response, function( index, value ) {
                            $('#datas_disponiveis').append('<option value="' + value["data"] + '">' + value["data"] + ' (' + value["diaSemana"] + ')</option>');
                        });
                        $('#datas_disponiveis').focus();
                    }
                });



            }
        });

        $("#datas_disponiveis").change(function(){
            $(".conteudo_horario").html("@include('social.fomenta.agendamento.tabela_horarios')");
        });

        $("#btn_consultar_horário").click(function(){
            if($("#local_agendamento").val() == ""){
                alert("Por favor informe o Local em que deseja realizar o agendamento");
                $("#local_agendamento").focus();
                return false; 
            }

            if($("#datas_disponiveis").val() == ""){
                alert("Por favor escolha a data para realizar o agendamento");
                $("#datas_disponiveis").focus();
                return false; 
            }

          
            let $localAgendamento = $("#local_agendamento").val();
            let $diaAgendamento = $("#datas_disponiveis").val();

            let formData = new FormData();

            formData.append("localAgendamento", $localAgendamento);
            formData.append("diaAgendamento", $diaAgendamento);

            $.ajax({
                url: "{{ route('fomenta_consulta_horarios_disponiveis') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $(".conteudo_horario").html(response);
                    recalcularDataTable(null, false);
                    
                }
            });  

        });

        function teste(){
            console.log("teste");
            horarioAgendamento.columns.adjust().responsive.recalc();

        }


    });
</script>

@endsection