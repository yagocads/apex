<div class="card">
    <div class="card-header">
        <h5>Inputs (Formulário)</h5>
    </div>

    <div class="card-body">
        <div class="alert alert-warning">
            <p><b><i class="fa fa-volume-up"></i> Observação: </b> Seguir o padrão de separação envolvendo dados e documentos (anexos), sempre colocar dados do lado esquerdo e anexos do lado direito.</p>
            <p>O formulário não precisa ser igual, pois toda funcionalidade tem suas regras, mas o padrão para separação dos inputs sim.</p>
        </div>

        <h6><b>Padrão de seguimentação dos inputs:</b></h6>
        <hr>

        <div class="row">
            <div class="col-lg-6">
                <h5><i class="fa fa-user mt-3 mb-3"></i> Dados do cidadão:</h5>

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="nome_requerente">Nome Completo:</label>
                        <input type="text" name="nome_requerente" id="nome_requerente" class="form-control form-control-sm" value="" maxlength="100" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-4 form-group">
                        <label for="identidade_requerente">Identidade:</label>
                        <input type="text" name="identidade_requerente" id="identidade_requerente" class="form-control form-control-sm" value="" maxlength="20" />
                    </div>

                    <div class="col-lg-4 form-group">
                        <label for="orgao_emissor_requerente">Órgão Emissor:</label>
                        <input type="text" name="orgao_emissor_requerente" id="orgao_emissor_requerente" class="form-control form-control-sm" value="" maxlength="45" />
                    </div>

                    <div class="col-lg-4 form-group">
                        <label for="data_emissao_requerente">Data Emissão:</label>
                        <input type="date" name="data_emissao_requerente" id="data_emissao_requerente" max="{{ date('Y-m-d') }}" class="form-control form-control-sm" value="" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-4 form-group">
                        <label for="cpf_requerente">CPF:</label>
                        <input type="text" name="cpf_requerente" id="cpf_requerente" class="form-control form-control-sm mask-cpf" value="" maxlength="20" />
                    </div>

                    <div class="col-lg-8 form-group">
                        <label for="email_requerente">E-mail:</label>
                        <input type="email" name="email_requerente" id="email_requerente" class="form-control form-control-sm" value="" maxlength="100" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                        </div>
                    </div>

                    <div class="col-lg-12 form-group">
                        <label for="documento_representacao_legal"><i class="fa fa-paperclip"></i> Anexar Representação Legal:</label>
                        <input type="file" name="documento_representacao_legal" id="documento_representacao_legal" />
                    </div>

                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-enviar-documento">
                            Enviar Documento <i class="fa fa-send"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>