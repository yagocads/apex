<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests\CadastroCgmRequest;
use Illuminate\Http\Request;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Support\ImovelSupport;


class CadastroController extends Controller
{
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
        $this->properties = new ImovelSupport();
    }

    public function atualizaCGM()
    {
        if (Auth::user() !== null) {
            return redirect()->route(
                "cadastro_cgm",
                remove_character_document(Auth::user()->cpf)
            );
        } else {
            $showRecaptcha = GoogleRecaptcha::show(
                '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
                'cgm_cpf',
                'no_debug',
                'Por favor clique no checkbox do reCAPTCHA primeiro!'
            );

            return view('cadastro.cgmCpf', [
                'showRecaptcha' => $showRecaptcha,
                'vencimentoIPTU' => 0
            ]);
        }
    }

    public function atualizaCGMIPTU()
    {

        if (Auth::user() !== null) {
            return redirect()->route('cgm_validarCpfIPTU');
        } else {

            $showRecaptcha = GoogleRecaptcha::show(
                '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
                'cgm_cpf',
                'no_debug',
                'mt-4 mb-3',
                'Por favor clique no checkbox do reCAPTCHA primeiro!'
            );

            return view('cadastro.cgmCpf', ['pagina' => "Principal", 'showRecaptcha' => $showRecaptcha, 'vencimentoIPTU' => 1]);
        }
    }

    public function alteraIPTU()
    {
        if (Auth::user() !== null) {
            // usuário logado

            // $cgm = $this->recuperaCGM(Auth::user()->cpf);
            $cgm = $cgm = $this->properties->getDadosCGM(remove_character_document(Auth::user()->cpf));

            if ($cgm->iStatus != 1) {
                return view('site.usuario_falha_integracao');
            }

            // $cgmEnderecoPrimario = $this->recuperaEnderecoCGM(Auth::user()->cpf,"P");
            $cgmEnderecoPrimario =  $this->properties->getEnderecoCGM(remove_character_document(Auth::user()->cpf), "P");

            // dd($cgmEnderecoPrimario);

            if ($cgmEnderecoPrimario->iStatus != 1) {
                return view('site.usuario_falha_integracao');
            }

            // dd($cgm['cgm'],$cgmEnderecoPrimario['endereco']);
            $areaCulturalAtuacao = [
                "Artesanato",
                "Arte de Rua",
                "Artes Visuais",
                "Artes Plásticas",
                "Audiovisual",
                "Circo",
                "Cultura Popular",
                "Cultura Urbana",
                "Dança",
                "Design",
                "Fotografia",
                "Gestão Cultural",
                "Literatura",
                "Moda",
                "Museus",
                "Música",
                "Patrimônio Histórico e Cultural",
                "Teatro",
                "Outro"
            ];

            $atuacaoCultural = [
                "Artesão",
                "Artista Plástico",
                "Escritor",
                "Cultura Alimentar",
                "Cultura Urbana",
                "Cultura Popular",
                "Produção",
                "Artística",
                "Técnica",
                "Oficineiro/Instrutor/Professor",
                "Curadoria",
                "Pesquisa"
            ];

            $areaArtisticaCultural = [
                "Cursos livres",
                "Ensino superior",
                "Curso de especialização",
                "Cursos técnicos",
                "Auto-aprendizado"
            ];

            $tempoAtuacaoArea = [
                "Menos de 1 ano",
                "1 ano",
                "2 anos",
                "De 3 a 5 anos",
                "De 5 a 7 anos",
                "De 7 a 9 anos",
                "10 anos ou mais"
            ];

            $alcancePessoas = [
                "De 05 a 20 pessoas",
                "De 21 a 50 pessoas",
                "De 51 a 100 pessoas",
                "De 101 a 500 pessoas",
                "Acima de 500 pessoas",
                "Acima de 1000 pessoas"
            ];

            $frequenciaAtividadesArtisticas = [
                "Diariamente",
                "Semanalmente",
                "Apenas nos finais de semana",
                "de 15 em 15 dias",
                "Mensalmente",
                "Por demanda"
            ];

            $insercaoAtividadeArtisticoCultural = [
                "Apresentações em espaços culturais formais (cinema, teatro, casa de espetáculo, etc)",
                "Apresentações em outros espaços culturais (associação de bairro, colégios, etc)",
                "Espaço próprio",
                "Espaço Público",
                "Espaço Virtual",
                "Exposição coletivas de arte",
                "Exposição individuais de arte",
                "Feiras de artesanato",
                "Feiras de livro",
                "Feiras de moda",
                "Feiras gastronômicas",
                "Lojas (livros, CDs, telas, artesanatos, etc)",
                "Shows em bares e restaurantes",
                "Shows/apresentações em espaços abertos"
            ];

            $verificaCadastroCgmNaCultura = DB::connection('integracao')
                ->table('lecom_cgm_cultura')
                ->where('cpf_cnpj', '=', formatCnpjCpf(Auth::user()->cpf))
                ->count();

            session(['vencimentoIPTU' => 1]);

            // dd($cgm, $cgmEnderecoPrimario);
            return view('cadastro.cgm.cgm_formulario', [
                'pagina' => "Principal", 'vencimentoIPTU' => 1,
                'cgm' => $cgm,
                'endereco' => $cgmEnderecoPrimario->endereco,
                'cpfOuCnpj' => formatCnpjCpf(Auth::user()->cpf),
                "areaCulturalAtuacao" => $areaCulturalAtuacao,
                "atuacaoCultural" => $atuacaoCultural,
                "areaArtisticaCultural" => $areaArtisticaCultural,
                "tempoAtuacaoArea" => $tempoAtuacaoArea,
                "alcancePessoas" => $alcancePessoas,
                "frequenciaAtividadesArtisticas" => $frequenciaAtividadesArtisticas,
                "insercaoAtividadeArtisticoCultural" => $insercaoAtividadeArtisticoCultural,
                "estados" => getEstados(),
                'verificaCadastroCgmNaCultura' => $verificaCadastroCgmNaCultura
            ]);
        } else {
            return redirect()->route('atualizaCGMIPTU');
        }
    }

    public function cgm_validarCpf(Request $request)
    {

        session(['vencimentoIPTU' => $request->vencimentoIPTU]);
        if (Auth::user() !== null) {

            return redirect()->route(
                "cadastro_cgm",
                remove_character_document(Auth::user()->cpf)
            );
        } else {

            // GoogleRecaptcha::verify(
            //     '6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG',
            //     'Por favor clique no checkbox do reCAPTCHA primeiro!'
            // );

            return redirect()->route(
                "cadastro_cgm",
                remove_character_document($request->cgm_cpf)
            );
        }
    }

    public function cadastroCgm($cpfOuCnpj)
    {
        $areaCulturalAtuacao = [
            "Artesanato",
            "Arte de Rua",
            "Artes Visuais",
            "Artes Plásticas",
            "Audiovisual",
            "Circo",
            "Cultura Popular",
            "Cultura Urbana",
            "Dança",
            "Design",
            "Fotografia",
            "Gestão Cultural",
            "Literatura",
            "Moda",
            "Museus",
            "Música",
            "Patrimônio Histórico e Cultural",
            "Teatro",
            "Outro"
        ];

        $atuacaoCultural = [
            "Artesão",
            "Artista Plástico",
            "Escritor",
            "Cultura Alimentar",
            "Cultura Urbana",
            "Cultura Popular",
            "Produção",
            "Artística",
            "Técnica",
            "Oficineiro/Instrutor/Professor",
            "Curadoria",
            "Pesquisa"
        ];

        $areaArtisticaCultural = [
            "Cursos livres",
            "Ensino superior",
            "Curso de especialização",
            "Cursos técnicos",
            "Auto-aprendizado"
        ];

        $tempoAtuacaoArea = [
            "Menos de 1 ano",
            "1 ano",
            "2 anos",
            "De 3 a 5 anos",
            "De 5 a 7 anos",
            "De 7 a 9 anos",
            "10 anos ou mais"
        ];

        $alcancePessoas = [
            "De 05 a 20 pessoas",
            "De 21 a 50 pessoas",
            "De 51 a 100 pessoas",
            "De 101 a 500 pessoas",
            "Acima de 500 pessoas",
            "Acima de 1000 pessoas"
        ];

        $frequenciaAtividadesArtisticas = [
            "Diariamente",
            "Semanalmente",
            "Apenas nos finais de semana",
            "de 15 em 15 dias",
            "Mensalmente",
            "Por demanda"
        ];

        $insercaoAtividadeArtisticoCultural = [
            "Apresentações em espaços culturais formais (cinema, teatro, casa de espetáculo, etc)",
            "Apresentações em outros espaços culturais (associação de bairro, colégios, etc)",
            "Espaço próprio",
            "Espaço Público",
            "Espaço Virtual",
            "Exposição coletivas de arte",
            "Exposição individuais de arte",
            "Feiras de artesanato",
            "Feiras de livro",
            "Feiras de moda",
            "Feiras gastronômicas",
            "Lojas (livros, CDs, telas, artesanatos, etc)",
            "Shows em bares e restaurantes",
            "Shows/apresentações em espaços abertos"
        ];

        $verificaCadastroCgmNaCultura = DB::connection('integracao')
            ->table('lecom_cgm_cultura')
            ->where('cpf_cnpj', '=', formatCnpjCpf($cpfOuCnpj))
            ->count();

        return view('cadastro.cgm.cgm_formulario', [
            'cpfOuCnpj' => formatCnpjCpf($cpfOuCnpj),
            "areaCulturalAtuacao" => $areaCulturalAtuacao,
            "atuacaoCultural" => $atuacaoCultural,
            "areaArtisticaCultural" => $areaArtisticaCultural,
            "tempoAtuacaoArea" => $tempoAtuacaoArea,
            "alcancePessoas" => $alcancePessoas,
            "frequenciaAtividadesArtisticas" => $frequenciaAtividadesArtisticas,
            "insercaoAtividadeArtisticoCultural" => $insercaoAtividadeArtisticoCultural,
            "estados" => getEstados(),
            'verificaCadastroCgmNaCultura' => $verificaCadastroCgmNaCultura
        ]);
    }

    public function ConsultarSituacaoMrc(Request $request)
    {
        session()->forget("documentos_consulta_mrc");

        if (isset($request->protocolo) && isset($request->cpf_cnpj)) {

            $consultaBeneficio = DB::connection('lecom')
                ->table('v_consulta_servico_mrc')
                ->where('protocolo', '=', $request->protocolo)
                ->where('cpf_cnpj', '=', $request->cpf_cnpj)
                ->orWhereRaw('(data_nasc_abert = ? OR DATE(abertura) = ?)', [
                    date("d/m/Y", strtotime($request->data_nascimento)),
                    $request->data_abertura
                ])
                //->where(\DB::raw("DATE(abertura)"), '=', $request->data_abertura)
                ->first();

            if (isset($consultaBeneficio->fase)) {
                $situacao = $this->situacaoBeneficio($consultaBeneficio->fase);

                if ($situacao === "beneficio_concedido") {

                    $dadosBancarios = DB::connection('lecom')
                        ->table('v_mrc_dados_banco')
                        ->where('cpf_cnpj', '=', $consultaBeneficio->cpf_cnpj)
                        ->first();
                }

                if ($situacao === "solicitacao_nao_aprovada") {

                    $verificaEnvioRecurso = DB::connection('integracao')
                        ->table('lecom_cgm_cultura_recursos')
                        ->where('cpf_cnpj', '=', $consultaBeneficio->cpf_cnpj)
                        ->count();
                }
            } else {

                $consultaCgmCultura = DB::connection('integracao')
                    ->table('lecom_cgm_cultura')
                    ->where('cpf_cnpj', '=', $request->cpf_cnpj)
                    ->count();

                if ($consultaCgmCultura) {

                    return redirect()
                        ->route('consultarsituacaomrc')
                        ->withInput()
                        ->with(
                            'solicitacao_em_abertura',
                            'Sua solicitação está em processo de abertura. Continue acompanhando o andamento desta solicitação.'
                        );
                }

                return redirect()
                    ->route('consultarsituacaomrc')
                    ->withInput()
                    ->with('solicitacao_nao_encontrada', 'Solicitação não encontrada.');
            }
        }

        return view('cadastro.cgm.consulta_mrc.cgm_consulta_mrc', [
            'dadosConsulta' => isset($consultaBeneficio) ? $consultaBeneficio : null,
            'situacao' => isset($situacao) ? $situacao : null,
            'verificaEnvioRecurso' => isset($verificaEnvioRecurso) ? $verificaEnvioRecurso : null,
            'dadosBancarios' => isset($dadosBancarios) ? $dadosBancarios : null
        ]);
    }

    private function situacaoBeneficio($fase)
    {
        switch ($fase) {
            case "Abertura de Solicitação":
                $result = "abertura_de_solicitacao";
                break;
            case "Solicitação em Análise":
                $result = "solicitacao_em_analise";
                break;
            case "Benefício Concedido":
                $result = "beneficio_concedido";
                break;
            case "Solicitação Não Aprovada":
                $result = "solicitacao_nao_aprovada";
                break;
            case "Solicitação Recusada":
                $result = "solicitacao_recusada";
                break;
        }

        return $result;
    }

    public function enviarRecurso(Request $request)
    {
        $cgmCultura = DB::connection('integracao')
            ->table('lecom_cgm_cultura')
            ->where('cpf_cnpj', '=', $request->cpf_ou_cnpj)
            ->first();

        if (!isset($cgmCultura->cpf_cnpj)) {
            return response()->json(['error' => "true"]);
        }

        $cgmCulturaRecurso = DB::connection('integracao')
            ->table('lecom_cgm_cultura_recursos')
            ->insertGetId(
                [
                    'id_cgm' => $cgmCultura->id_cgm,
                    'id_cultura' => $cgmCultura->id,
                    'cpf_cnpj' => $cgmCultura->cpf_cnpj,
                    'processo' => $cgmCultura->chamado_lecom,
                    'recurso' => $request->justificativa_recurso,
                    'data' => date('Y-m-d H:i:s')
                ]
            );

        if (session()->has('documentos_consulta_mrc')) {
            foreach (session('documentos_consulta_mrc') as $dados) {
                DB::connection('integracao')
                    ->table('lecom_cgm_cultura_recurso_arquivos')
                    ->insert([
                        'id_recurso' => $cgmCulturaRecurso,
                        'cpf_cnpj' => $cgmCultura->cpf_cnpj,
                        'descricao' => $dados->descricao,
                        'documento' => $dados->nome_novo
                    ]);
            }
        }
    }

    public function salvarCgm(CadastroCgmRequest $request)
    {
        $idCgm = DB::connection('integracao')
            ->table('lecom_cgm')
            ->insertGetId(
                array_merge(
                    $this->dadosRequerente($request),
                    (mb_strlen(remove_character_document($request->cpf_ou_cnpj)) == 11 ? $this->dadosInformacoesPessoais($request) : $this->dadosMercantis($request)),
                    $this->dadosInformacoesContato($request),
                    $this->dadosEndereco($request)
                )
            );

        $this->enviarDocumentosParaLecom(
            $request->cpf_ou_cnpj,
            $idCgm
        );

        $this->enviarDocumentosDoImovelParaLecom(
            $request->cpf_ou_cnpj,
            $idCgm
        );

        if ($request->artista == "sim") {

            $request->protocolo = remove_character_document($request->cpf_ou_cnpj) . str_pad($idCgm + 1, 6, '0', STR_PAD_LEFT) . date('d');

            $idCultura = DB::connection('integracao')
                ->table('lecom_cgm_cultura')
                ->insertGetId(
                    $this->dadosInformacoesCulturais($request, $idCgm)
                );

            $this->enviarDocumentosParaCultura(
                $idCgm,
                $idCultura,
                $request->cpf_ou_cnpj
            );

            $this->removerDocumentosSessao();

            return view('cadastro.cgm.protocolo', [
                'identificacao' => (mb_strlen(remove_character_document($request->cpf_ou_cnpj)) == 11 ? $request->nome : $request->razao_social),
                'cpfOuCnpj' => $request->cpf_ou_cnpj,
                'vencimentoIptu' => $request->vencimento_iptu,
                'numeroProtocolo' => $request->protocolo
            ]);
        }

        $this->removerDocumentosSessao();
        session()->forget('vencimentoIPTU');

        return view('messages.sucesso', [
            'titulo' => $request->vencimento_iptu == 1 ? "Alterar Vencimento do IPTU" : "Cadastrar/Atualizar CGM",
            'mensagem' => 'Prezado contribuinte, seu cadastro foi gravado com sucesso. Prazo de 48 horas úteis para a efetivação. Após este prazo, o acesso ao boleto do IPTU 2021 estará liberado.',
            'icone' => 'fa-home',
            'rota' => 'servicos',
            'retorno' => 'imovel'

        ]);

        // return redirect()
        //     ->route('cadastro_cgm', remove_character_document($request->cpf_ou_cnpj))
        //     ->with('success', 'Seus dados foram enviados com sucesso!');
    }

    private function removerDocumentosSessao()
    {
        session()->forget("documentos_requerente");
        session()->forget("documentos_informacoes_pessoais");
        session()->forget("documentos_dados_mercantis");
        session()->forget("documento_endereco");
        session()->forget("documentos_imovel");
        session()->forget("documentos_informacoes_culturais_portfolio");
        session()->forget("documentos_informacoes_culturais_familia_monoparental");
    }

    private function dadosRequerente(Request $request)
    {
        return [
            'resp_preenchimento' => $request->responsavel,
            'nome_rep_legal' => $request->nome_requerente,
            'rg_rep_legalIdentiadade' => $request->identidade_requerente,
            'rg_rep_legalIdOrgEmis' => $request->orgao_emissor_requerente,
            'cpf_rep_legalcpf' => $request->cpf_requerente,
            'rg_rep_legalemail' => $request->email_requerente,
            'data_cadastro' => date('Y-m-d H:i:s'),
            'iptu_idoso' => $request->vencimentoIPTU
        ];
    }

    private function dadosInformacoesPessoais(Request $request)
    {
        return [
            'cpf' => $request->cpf,
            'data_nasc' => $request->data_nascimento,
            'sexo' => $request->sexo,
            'nome' => $request->nome,
            'nome_social' => $request->nome_social,
            'filiacao1' => $request->nome_mae,
            'filiacao2' => $request->nome_pai,
            'identidade' => $request->identidade,
            'orgao_expeditor' => $request->orgao_emissor,
            'data_expedicao' => $request->data_emissao,
            'estado_civil' => $request->estado_civil,
            'naturalidade' => $request->naturalidade,
            'nacionalidade' => $request->nacionalidade,
            'tipo_pessoa' => 'F',
            'validade_iptu' => $request->vencimentoIPTU != 0 ? "SIM" : "NÃO"
        ];
    }

    private function dadosMercantis(Request $request)
    {
        return [
            'cnpj' => $request->cnpj,
            'razao_social' => $request->nome_requerente,
            'nome_fantasia' => $request->identidade_requerente,
            'natureza_juridica' => $request->natureza_juridica,
            'data_abertura' => $request->data_abertura_empresa,
            'inscricao_estadual' => $request->inscricao_estadual,
            'tipo_pessoa' => 'J'
        ];
    }

    private function dadosInformacoesContato(Request $request)
    {
        return [
            'email' => $request->email,
            'celular' => $request->celular,
            'telefone' => $request->telefone
        ];
    }

    private function dadosEndereco(Request $request)
    {
        return [
            'cep' => $request->cep,
            'endereco' => $request->endereco,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cidade' => $request->municipio,
            'uf' => $request->estado
        ];
    }

    private function dadosInformacoesCulturais(Request $request, $idCgm)
    {
        return [
            'protocolo' => $request->protocolo,
            'id_cgm' => $idCgm,
            'cpf_cnpj' => $request->cpf_ou_cnpj,
            'area_cultural' => !empty($request->area_cultural) ? implode(',', $request->area_cultural) : '',
            'area_cultural_principal' => $request->area_cultural_principal,
            'atuacao_cultural' => !empty($request->atuacao_cultural) ? implode(',', $request->atuacao_cultural) : '',
            'descricao_atividades' => $request->descricao_atividades,
            'formacao_area_artistica_cultural' => !empty($request->formacao_area_artistica_cultural) ? implode(',', $request->formacao_area_artistica_cultural) : '',
            'registro_profissional' => $request->registro_profissional,
            'registro_profissional_descricao' => $request->registro_profissional_descricao,
            'espaco_cultural_coletivo' => $request->espaco_cultural_coletivo,
            'numero_doc_espaco_cultural_coletivo' => $request->numero_doc_espaco_cultural_coletivo,
            'tempo_atuacao_area' => $request->tempo_atuacao_area,
            'alcance_pessoas' => $request->alcance_pessoas,
            'frequencia_atividade' => $request->frequencia_atividade,
            'insercao_atividade_artistico_cultural' => !empty($request->insercao_atividade_artistico_cultural) ? implode(',', $request->insercao_atividade_artistico_cultural) : '',
            'fonte_renda' => $request->fonte_renda,
            'pandemia_covid' => $request->pandemia_covid,
            'recebe_auxilio_governo' => $request->recebe_auxilio_governo,
            'emprego_formal_ativo' => $request->emprego_formal_ativo,
            'redes_sociais_divulgacao' => $request->redes_sociais_divulgacao,
            'lei_emergencia_cultural' => $request->lei_emergencia_cultural,
            'enquadramento_lei_emergencial' => $request->enquadramento_lei_emergencial,
            'ciente_que_lei_nao_garante_beneficio' => $request->ciente_que_lei_nao_garante_beneficio,
            'beneficio_previdenciario' => $request->beneficio_previdenciario,
            'nomes_beneficios_previdenciarios' => $request->nomes_beneficios_previdenciarios,
            'beneficio_emergencial_lei_13982' => $request->beneficio_emergencial_lei_13982,
            'mulher_familia_monoparental' => $request->mulher_familia_monoparental,
            'confirmacao_de_informacoes' => $request->confirmacao_de_informacoes == "sim" ? 1 : 0,
            'banco_nome' => $request->banco_nome,
            'banco_tipo_conta' => $request->banco_tipo_conta,
            'banco_agencia' => $request->banco_agencia,
            'banco_numero_conta' => $request->banco_numero_conta,
            'banco_conta_operacao' => $request->banco_conta_operacao
        ];
    }

    private function enviarDocumentosParaLecom($cpf, $idCgm)
    {
        $sessionDocumentos = [
            'documentos_requerente',
            'documentos_informacoes_pessoais',
            'documentos_dados_mercantis',
            'documento_endereco'
        ];

        foreach ($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach (session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_cgm_documentos')
                        ->insert([
                            'id_cgm' => $idCgm,
                            'cpf' => $cpf,
                            'documento' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    private function enviarDocumentosDoImovelParaLecom($cpf, $idCgm)
    {
        if (session()->has('documentos_imovel')) {
            foreach (session('documentos_imovel') as $dados) {
                DB::connection('integracao')
                    ->table('lecom_cgm_imoveis')
                    ->insert([
                        'id_cgm' => $idCgm,
                        'cpf' => $cpf,
                        'matricula_imovel' => $dados->matricula_imovel,
                        'tipo_vinculo' => $dados->tipo_vinculo,
                        'descricao_documento' => $dados->tipo_documento,
                        'documento' => $dados->nome_novo
                    ]);
            }
        }
    }

    private function enviarDocumentosParaCultura($idCgm, $idCultura, $cpfOuCnpj)
    {
        $sessionDocumentos = [
            "documentos_informacoes_culturais_portfolio",
            "documentos_informacoes_culturais_familia_monoparental"
        ];

        foreach ($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach (session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_cgm_cultura_arquivos')
                        ->insert([
                            'id_cgm' => $idCgm,
                            'id_cultura' => $idCultura,
                            'cpf_cnpj' => $cpfOuCnpj,
                            'descricao' => $dados->descricao,
                            'documento' => $dados->nome_novo
                        ]);
                }
            }
        }
    }

    public function salvarDocumentoServidor(Request $request)
    {
        $nome_original = $request->arquivo->getClientOriginalName();
        $extensao = $request->arquivo->extension();

        $nomesDocumentosMartelados = [
            "outro",
            "portfolio",
            "monoparental"
        ];

        $nome_arquivo = remove_character_document($request->cpf_ou_cnpj) . "_" . (isset($request->descricao_martelada) && in_array($request->descricao_martelada, $nomesDocumentosMartelados) ? $request->descricao_martelada : $request->descricao) . "_" . uniqid() . "." . $extensao;
        $nome_arquivo = remove_accentuation(str_replace(" ", "_", $nome_arquivo));

        $upload = $request->arquivo->storeAs('cgm', $nome_arquivo, 'ftp');

        if ($upload) {

            if (!empty($request->matricula_imovel)) {
                session()->push($request->nome_sessao, (object) [
                    "matricula_imovel" => $request->matricula_imovel,
                    "tipo_vinculo" => $request->tipo_vinculo,
                    "tipo_documento" => $request->tipo_documento,
                    "nome_original" => $request->arquivo->getClientOriginalName(),
                    "nome_novo" => $nome_arquivo
                ]);
            } else {
                session()->push($request->nome_sessao, (object) [
                    "descricao" => $request->descricao,
                    "nome_original" => $request->arquivo->getClientOriginalName(),
                    "nome_novo" => $nome_arquivo
                ]);
            }

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }

            return view('cadastro.cgm.' . $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removerDocumentoSessao(Request $request)
    {
        if ($this->removerDocumentoServidor($request->nome_arquivo)) {

            $session = session()->get($request->nome_sessao);

            if (!empty($session)) {

                foreach ($session as $key => $dados) {

                    if (in_array($request->nome_arquivo, (array) $dados)) {
                        unset($session[$key]);
                        session()->forget($request->nome_sessao);
                        session()->put($request->nome_sessao, $session);
                    }
                }
            }

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }

            return view('cadastro.cgm.' . $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }
    }

    private function removerDocumentoServidor(string $arquivo): bool
    {
        return Storage::disk('ftp')->delete('cgm/' . $arquivo);
    }

    public function imprimirProtocoloMrc(Request $request)
    {
        return \PDF::loadView('cadastro.cgm.protocolo_impressao_mrc', [
            'cpfOuCnpj' => $request->cpf_cnpj,
            'identificacao' => $request->identificacao,
            'protocolo' => $request->protocolo
        ])->download('protocolo_mrc.pdf');
    }

    public function verificarEnvioDocumentos(Request $request)
    {
        $documentos_em_falta = [];

        foreach ($request->documentos_necessarios as $documento) {

            if (
                !session()->has($documento['nome_sessao'])
                || count(session($documento['nome_sessao'])) < $documento['qtd']
            ) {

                $documentos_em_falta[] = [
                    "descricao" => $documento['descricao']
                ];
            }
        }

        return response()->json(["documentos_em_falta" => $documentos_em_falta]);
    }
}
