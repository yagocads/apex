@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Prestação de Contas - PAE</h2>
            </div>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col d-flex justify-content-center">
        <div class="col-lg-12 ">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <b>ATENÇÃO:</b><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session("mensagem"))
            <div class="alert alert-success">
                {{session("mensagem")}}
            </div>
            <br>
            @endif

            <form method="POST" action="gravar_prestacao_contas" autocomplete="off" id="prestacao_contas">
                @csrf
                <input type="hidden" value="{{Auth::user()->cpf}}" name="cnpj">
                <input type="hidden" value="{{Auth::user()->name}}" name="nome">
            
                <div id="accordion">
                    
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados_usuario">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Suas Informações
                            </div>
                        </a>
                        <div id="dados_usuario" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("financeiro.social.usuario")
                            </div>
                        </div>
                    </div>

                    @if(count($mesesPendentes) == 0 )
                        <div class="alert alert-info ">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <b>ATENÇÃO:</b> Todos os arquivos já foram enviados.
                            
                        </div>
                    @else
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#funcionarios">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Documentos
                                </div>
                            </a>
                            <div id="funcionarios" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("financeiro.social.funcionarios")
                                </div>
                            </div>
                        </div>
                        
                        {{-- <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#termoAdesao">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Termo de Adesão
                                </div>
                            </a>
                            <div id="termoAdesao" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("financeiro.social.termo_adesao")
                                </div>
                            </div>
                        </div> --}}
                    @endif
                </div>
                

                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'financeiro']) }}" class="btn btn-primary mt-3">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                        @if(count($mesesPendentes) > 0 )
                            <button class="btn btn-success mt-3" id="enviar-informacoes">Enviar Informações <i class="fa fa-send"></i></button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>

@include('modals.modal_footer')
@endsection

@section('post-script')

<script>
    $(function() {

        $("#dados_usuario").collapse('show');
        
        // var documentosFuncionarios = $('#documentos_funcionarios').DataTable({
        //     destroy: true,
        //     "oLanguage": {
        //         "sProcessing": "Processando...",
        //         "sLengthMenu": "_MENU_ resultados por página",
        //         "sZeroRecords": "Não foram encontrados resultados",
        //         "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        //         "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
        //         "sInfoFiltered": "",
        //         "sInfoPostFix": "",
        //         "sSearch": "Pesquisar:",
        //         "sUrl": "",
        //         "oPaginate": {
        //             "sFirst": "Primeiro",
        //             "sPrevious": "Anterior",
        //             "sNext": "Próximo",
        //             "sLast": "Último"
        //         }
        //     },
        //     "bLengthChange": true,
        //     "ordering": true,
        //     "info": true,
        //     "searching": true,
        //     "paginate": true
        // }).columns.adjust().responsive.recalc();

        // Spinner
        let load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#descricao").on("change", function(e) {
            if($("#descricao").val() == "SEFIP da Empresa"){
                $("#doc_rescisao").hide()
                $("#doc_outros").hide()
            }
            if($("#descricao").val() == "Comprovante de Rescisão"){
                $("#doc_rescisao").show()
                $("#doc_outros").hide()
            }
            if($("#descricao").val() == "Outros"){
                $("#doc_rescisao").hide()
                $("#doc_outros").show()
            }
        });


        $("#enviar-informacoes").on("click", function(e) {


            var documentosFuncionarios = $('#documentos_funcionarios').DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Pesquisar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "bLengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "paginate": true
            }).columns.adjust().responsive.recalc();

            if ( ! documentosFuncionarios.data().count() ) {
                // alert( 'ATENÇÃO: É necessário incluir ao menos um documento' );

                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: É necessário incluir ao menos um documento.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");
                return false;
            }


            $achouDocumento = false;
            documentosFuncionarios.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 3){
                        if(column == "SEFIP da Empresa"){
                            $achouDocumento = true
                        }
                    }
                });
            });

            if(!$achouDocumento){
                // alert( 'ATENÇÃO: É necessário incluir a SEFIP do Mês selecionado.' );

                
                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: É necessário incluir a SEFIP do Mês selecionado.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");

                return false; 
            }

           load.fadeOut(200);
           $("#prestacao_contas").submit();

        });

        /**
         * (Agrupamento: Documentação em Anexo)
         * 
         * # Salvar Documento na sessão
         * 
         */

        $("body").on("click", ".salvar_documento", function(e) {

            e.preventDefault();

            if($("#mes").val() == ""){
                // alert("ATENÇÃO: Informe o mês de referência para a prestação de contas.");
                                
                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO:  Informe o mês de referência para a prestação de contas.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");
                
                $("#funcionarios").collapse('show');
                $("#mes").focus();
                return false;    
            }

            if($("#descricao").val() == ""){
                // alert("ATENÇÃO: Informe o tipo de documento.");

                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO:  Informe o tipo de documento.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");

                $("#funcionarios").collapse('show');
                $("#descricao").focus();
                return false;    
            }

            if($("#descricao").val() == "Comprovante de Rescisão"){
                if($("#nome_funcionario").val() == ""){
                    // alert("ATENÇÃO: Informe o funcionário.");


                    $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                    $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: Informe o funcionário.</p>')
                    $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                    
                    $('#footer-modal').modal("show");
                                        
                    $("#funcionarios").collapse('show');
                    $("#nome_funcionario").focus();
                    return false;    
                }
            }

            if($("#descricao").val() == "Outros"){
                if($("#descricao_doc").val() == ""){
                    // alert("ATENÇÃO: Informe a descrição do documento.");


                    $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                    $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: Informe a descrição do documento.</p>')
                    $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                    
                    $('#footer-modal').modal("show");

                    $("#funcionarios").collapse('show');
                    $("#descricao_doc").focus();
                    return false;    
                }
            }


            let arquivo = $('#arquivo')[0].files[0];

            if (!arquivo) {
                // alert("ATENÇÃO: Escolha um arquivo para enviar.");


                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: Escolha um arquivo para enviar.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");
                                
                return false;
            }

            $input = document.getElementById('arquivo');
            file = $input.files[0];
            
            $ext = file.name.split('.');
            $ext = "." +  $ext[$ext.length-1];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'], 8400000)) {
                return false;
            }

            let formData = new FormData();
            formData.append("mes", $("#mes").val());
            formData.append("nome", $( "#nome_funcionario" ).val());
            formData.append("arquivo", arquivo);
            formData.append("descricao", $("#descricao").val());   
            formData.append("descricao_doc", $("#descricao_doc").val());   
            formData.append("comentario", $("#comentario").val());   
            formData.append('outro_anexo',$("#outro_anexo").val());  

            let url = "salvar_documento_funcionario_pae";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("#arquivo").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        // alert("ATENÇÃO: " + response.error);

                        $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                        $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: ' + response.error + '</p>')
                        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                        
                        $('#footer-modal').modal("show");

                        return false;
                    }
                    else{
                        $("#nome_funcionario").val("");
                        $("#descricao").val("");
                        $("#descricao_doc").val("");
                        // $("#mes").val("");
                        $("#comentario").val("");

                        $(".conteudo_documentos_funcionarios").html(response);
                        recalcularDataTable("documentos_funcionarios");

                        var documentosFuncionarios = $('#documentos_funcionarios').DataTable({
                            destroy: true,
                            "oLanguage": {
                                "sProcessing": "Processando...",
                                "sLengthMenu": "_MENU_ resultados por página",
                                "sZeroRecords": "Não foram encontrados resultados",
                                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                                "sInfoFiltered": "",
                                "sInfoPostFix": "",
                                "sSearch": "Pesquisar:",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Primeiro",
                                    "sPrevious": "Anterior",
                                    "sNext": "Próximo",
                                    "sLast": "Último"
                                }
                            },
                            "bLengthChange": true,
                            "ordering": true,
                            "info": true,
                            "searching": true,
                            "paginate": true
                        }).columns.adjust().responsive.recalc();
                        
                        if ( documentosFuncionarios.data().count() ) {
                            $("#mes").attr("readonly", true);
                        }
                    }
                }
            });
        });


        $("body").on("click", ".salvar_documento", function(e) {

            e.preventDefault();

            if ( $('#descricao_termo_adesao').val() == "") {
                // alert("ATENÇÃO: informe a descrição do documento.");

                
                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: informe a descrição do documento.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");

                return false;
            }

            let arquivo = $('#arquivo_termo_adesao')[0].files[0];

            if (!arquivo) {
                // alert("ATENÇÃO: Escolha um arquivo para enviar.");

                
                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: Escolha um arquivo para enviar.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");

                return false;
            }

            $input = document.getElementById('arquivo_termo_adesao');
            file = $input.files[0];
            
            $ext = file.name.split('.');
            $ext = "." +  $ext[$ext.length-1];

            if (file.size > 2048000) {
                // alert("O arquivo deve ter no máximo 2Mb");

                                
                $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: O arquivo deve ter no máximo 2Mb</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");

                return false;
            }

            let formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("descricao", $("#descricao_termo_adesao").val());   
            formData.append('outro_anexo',$("#outro_anexo").val());  

            let url = "salvar_termo_adesao";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("#arquivo_termo_adesao").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        // alert("ATENÇÃO: " + response.error);

                        $('#footer-modal').find('.modal-title').html('<h3>Prestação de Contas PAE</h3>');
                        $('#footer-modal').find('.modal-body').html('<p>ATENÇÃO: ' + response.error + '</p>')
                        $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                        
                        $('#footer-modal').modal("show");
                                                
                        return false;
                    }

                    $(".conteudo_termo_adesao").html(response);
                    recalcularDataTable("termo_adesao");
                }
            });
        });

        /**
        * (Agrupamento: Documentação em Anexo)
        * 
        * # Remover Documento na sessão
        * 
        */
        $("body").on("click", ".remover_documento_funcionario", function(e) {

            e.preventDefault();

            if (!confirm("Deseja realmente remover o documento?")) {
                return false;
            }

            let documento = $(this).data("documento");

            let url = "remover_documento_funcionario_sessao/" + documento;

            $.ajax({
                url: url,
                method: 'GET',
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    $(".conteudo_documentos_funcionarios").html(response);
                    recalcularDataTable("documentos_funcionarios");

                    var documentosFuncionarios = $('#documentos_funcionarios').DataTable({
                            destroy: true,
                            "oLanguage": {
                                "sProcessing": "Processando...",
                                "sLengthMenu": "_MENU_ resultados por página",
                                "sZeroRecords": "Não foram encontrados resultados",
                                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                                "sInfoFiltered": "",
                                "sInfoPostFix": "",
                                "sSearch": "Pesquisar:",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Primeiro",
                                    "sPrevious": "Anterior",
                                    "sNext": "Próximo",
                                    "sLast": "Último"
                                }
                            },
                            "bLengthChange": true,
                            "ordering": true,
                            "info": true,
                            "searching": true,
                            "paginate": true
                        }).columns.adjust().responsive.recalc();
                        
                        if ( ! documentosFuncionarios.data().count() ) {
                            $("#mes").attr("readonly", false);
                        }
                }
            });
        });

        $("body").on("click", ".remover_termo_adesao", function(e) {

            e.preventDefault();

            if (!confirm("Deseja realmente remover o documento?")) {
                return false;
            }

            let documento = $(this).data("documento");

            let url = "remover_termo_adesao_sessao/" + documento;

            $.ajax({
                url: url,
                method: 'GET',
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    $(".conteudo_termo_adesao").html(response);
                    recalcularDataTable("termo_adesao");
                }
            });
        });
    });
</script>

@endsection