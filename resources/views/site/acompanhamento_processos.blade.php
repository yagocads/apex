@if(isset($processos))
    <div class="card mt-2">
        <div class="card-header">
            <h5><i class="fa fa-arrow-down arrow-red"></i> Processos abertos ({{ $processos[0]->Serviço }})</h5>
        </div>
        <div class="card-body">
            @foreach ($processos as $processo)
                <div class="callout-red shadow-sm mb-4">
                    <div class="row processo-fase-topo">
                        <div class="col-lg-2">
                            Nº do Protocolo: <b>{{ $processo->Chamado }}</b>
                        </div>
                        <div class="col-lg-2">
                            Data: <b>{{ date('d/m/Y', strtotime($processo->abertura)) }}</b>
                        </div>
                        <div class="col-lg-4">
                            Serviço: <b>{{ $processo->Serviço }}</b>
                        </div>
                        <div class="col-lg-4">
                            Responsável: <b>{{ $processo->responsavel_consulta }}</b><br>
                        </div>
                    </div>
                    <div class="scrollspy-container">
                        <div data-spy="scroll" data-target="#navbar-example2" data-offset="0" class="scrollspy-example">
                            <div class="col-md-12 mt-5" style="height: 150px;">
                                <div id="stepper1" class="bs-stepper">
                                    <div class="bs-stepper-header">
                                        <input type="hidden" value="{{ $processo->Etapa }}" id='etapa_atual' />
                                        @php
                                            $val = 1;
                                            $qtd = count($etapas);
                                        @endphp

                                        @foreach ($etapas as $key => $etapa)

                                        <div class="{{ $processo->Fase == $key ? 'step active' : 'step' }}">
                                            <button type="button" class="btn step-trigger">
                                                <span class="bs-stepper-circle">{{ $val++ }}</span>
                                                <span class="bs-stepper-label">{{ $key }}</span>
                                            </button>
                                        </div>

                                        <div class="bs-stepper-line"></div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                    <div class="bs-stepper-content">
                        <div class="row">
                            <div class="col-lg-12">
                                @isset($pass)
                                    @if($pass > 0)
                                        <div class="alert alert-error">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <strong>Atenção!</strong> Você já tem uma solicitação para este serviço. Verifique o status deste
                                            serviço:
                                        </div>
                                    @endif
                                @endisset

                                @php
                                    $servicosSemSegundoPasso = [
                                        'ITBI Online',
                                        'Alteração Cadastro Geral do Município - PF',
                                        'Alteração Cadastro Geral do Município - PJ',
                                        'Defesa Prévia de Autuação'
                                    ];
                                @endphp

                                @if (!in_array($processo->Serviço, $servicosSemSegundoPasso))
                                    @include('site.processos.'. str_replace('-','_',remove_accentuation($processo->Serviço)))
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
