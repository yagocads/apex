@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <i class="fa fa-home"></i> 
                            Fomenta Maricá
                        </h4>
                    </div>

                    <div class="card-body">
                        <br>
                        <div class="alert alert-info" style="text-align:center; margin: auto; border-radius: 7px; border: 1px solid #82a5c7; color: #ae7410;">
                                <b>
                                    Encerramento das inscrições:<br> 
                                    04/09/2020 às 18:00:00
                                </b>
                                <br>
                        </div>
                        <br>
                        <br>
                        <form method="POST" action="{{ route('fomentaValidaCpfCnpj') }}" id="infoCpf">
                            @csrf

                            <input id="vencimentoIPTU" type="hidden" name="vencimentoIPTU" value="{{ isset($vencimentoIPTU) ? $vencimentoIPTU : 0}}" />
                            
                            <div class="form-group">
                                <label for="fomenta_cpfcnpj">Digite seu CNPJ</label>
                                
                                <input type="text" name="fomenta_cpfcnpj" id="fomenta_cpfcnpj" 
                                    class="form-control form-control-sm col-lg-6 cpfOuCnpj 
                                    {{ $errors->has('fomenta_cpfcnpj') ? 'is-invalid' : '' }}" maxlength="20" required="true" />
                                
                                @if ($errors->has('fomenta_cpfcnpj'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('fomenta_cpfcnpj') }}
                                    </div>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <a href="{{ route('fomenta') }}" class="btn btn-primary mt-3">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>
                            
                                <button type="submit" id="validar_CPF" class="btn btn-success mt-3">
                                    Validar CNPJ
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#validar_CPF").prop('disabled', true);
   
    function onReCaptchaTimeOut() {
        $("#validar_CPF").prop('disabled', true);
    }

    function onReCaptchaSuccess() {
        $("#validar_CPF").prop('disabled', false);
    }

    $('#fomenta_cpfcnpj').change(function() {

        if ($("#fomenta_cpfcnpj").val().length <= 14) {
            if(!validarCPF($("#fomenta_cpfcnpj").val())) {
                alert("CPF Inválido!");
                $("#fomenta_cpfcnpj").focus();
                return false;
            }
        } else{
            if(!validarCNPJ($("#fomenta_cpfcnpj").val())) {
                alert("CNPJ Inválido!");
                $("#fomenta_cpfcnpj").focus();
                return false;
            }
        }
    });

</script>

@endsection