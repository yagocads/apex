@if( $processo->Serviço == 'Legalização de Empresa')
    <div class="justfy-content-center processo-fase">

        @if($processo->Fase == "Cumprir Pendências")
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;
                    <a href="{{ route('exigencias_mei', ['chamado' => $processo->Chamado, 'ciclo' => $processo->Ciclo , 'fase' => $processo->Etapa]) }}" class="btn btn-blue">Sanar Pendências</a>
                    <div class="col-lg-6">
                        Motivo: {{ $processo->motivo }}
                    </div>
                </div>
            </div>
        @endif

        @if($processo->motivo != "" &&  $processo->FINALIZADO != "andamento")
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Atenção!</strong><br>
                Esta solicitação foi finalizada e teve como conclusão:<br>
                Conclusão: <strong>{{$processo->Fase}}</strong><br>
                @if(trim($processo->motivo) != "")
                    Motivo: {{$processo->motivo}}<br>
                @endif
            </div>
        @endif
    </div>
@endif