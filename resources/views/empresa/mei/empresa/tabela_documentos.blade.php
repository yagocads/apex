
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="documentos_empresa">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Nome Original</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_empresa"))
                @foreach(session("documentos_empresa") as $documento)
                    <tr>
                        <td class="td-descricao">{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento" title="Remover Documento" 
                                data-nome_arquivo="{{ $documento->nome_novo }}"
                                data-pasta="empresa"
                                data-nome_sessao="documentos_empresa"
                                data-id_tabela="documentos_empresa"
                                data-conteudo_documentos="conteudo_documentos_empresa">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>