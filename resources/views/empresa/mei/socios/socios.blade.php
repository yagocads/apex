
<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-user mt-3 mb-3"></i> Sócios:</h5>
        <input type="hidden" name="tipo_socio" id="tipo_socio" value="Responsável MEI" />
        
        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="cpf_socio">CPF: <span class="text-danger">*</span></label>
                <input type="text" name="cpf_socio" id="cpf_socio" 
                onfocusin="limparCamposSocio()"
                onChange="pesquisaCPFSocios(this.value);"
                    class="form-control form-control-sm mask-cpf {{ ($errors->has('cpf_socio') ? 'is-invalid' : '') }}"
                    value="{{ old('cpf_socio') }}" />

                    @if ($errors->has('cpf_socio'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpf_socio') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="nome_socio">Nome do Sócio: <span class="text-danger">*</span></label>
                <input type="text" name="nome_socio" id="nome_socio"
                    class="form-control form-control-sm {{ ($errors->has('nome_socio') ? 'is-invalid' : '') }}"
                    value="{{ old('nome_socio') }}" />

                    @if ($errors->has('nome_socio'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_socio') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="data_nascimento">Data de Nascimento: <span class="text-danger">*</span></label>
                <input type="date" name="data_nascimento" id="data_nascimento"
                    class="form-control form-control-sm {{ ($errors->has('data_nascimento') ? 'is-invalid' : '') }}"
                    value="{{ old('data_nascimento') }}" />

                    @if ($errors->has('data_nascimento'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_nascimento') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-8 form-group">
                <label for="email_socio">E-mail: <span class="text-danger">*</span></label>
                <input type="text" name="email_socio" id="email_socio"
                    class="form-control form-control-sm {{ ($errors->has('email_socio') ? 'is-invalid' : '') }}"
                    value="{{ old('email_socio') }}" />

                    @if ($errors->has('email_socio'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email_socio') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-10 form-group">
                <label for="mae_socio">Nome da Mãe do Sócio: <span class="text-danger">*</span></label>
                <input type="text" name="mae_socio" id="mae_socio"
                    class="form-control form-control-sm {{ ($errors->has('mae_socio') ? 'is-invalid' : '') }}"
                    value="{{ old('mae_socio') }}" />

                    @if ($errors->has('mae_socio'))
                        <div class="invalid-feedback">
                            {{ $errors->first('mae_socio') }}
                        </div>
                    @endif
            </div>
            <div class="col-lg-2 form-group">
                <label for="percentual_socio">Percentual: <span class="text-danger">*</span></label>
                <input type="text" name="percentual_socio" id="percentual_socio"
                    class="form-control form-control-sm {{ ($errors->has('percentual_socio') ? 'is-invalid' : '') }}"
                    value="{{ old('percentual_socio') }}" />

                    @if ($errors->has('percentual_socio'))
                        <div class="invalid-feedback">
                            {{ $errors->first('percentual_socio') }}
                        </div>
                    @endif
            </div>
        </div>

        <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Endereço:</h5>
        
        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="cep_socios">CEP: <span class="text-danger">*</span></label>
                <input type="text" name="cep_socios" id="cep_socios"
                    onChange="pesquisaCepSocios(this.value);" 
                    class="form-control form-control-sm mask-cep {{ ($errors->has('cep_socios') ? 'is-invalid' : '') }}"
                    value="{{ old('cep_socios') }}" />

                    @if ($errors->has('cep_socios'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cep_socios') }}
                        </div>
                    @endif
            </div>
                    
            <div class="col-lg-8 form-group">
                <label for="endereco_socios">Endereço: <span class="text-danger">*</span></label>
                <input type="text" name="endereco_socios" id="endereco_socios" 
                    class="form-control form-control-sm {{ ($errors->has('endereco_socios') ? 'is-invalid' : '') }}"
                    value="{{ old('endereco_socios') }}" maxlength="100" readonly/>

                    @if ($errors->has('endereco_socios'))
                        <div class="invalid-feedback">
                            {{ $errors->first('endereco_socios') }}
                        </div>
                    @endif
            </div>

        </div>
        
        <div class="form-row">
            <div class="col-lg-2 form-group">
                <label for="numero_socios">Número: <span class="text-danger">*</span></label>
                <input type="text" name="numero_socios" id="numero_socios" 
                    class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('numero_socios') ? 'is-invalid' : '') }}"
                    value="{{ old('numero_socios') }}" />

                    @if ($errors->has('numero_socios'))
                        <div class="invalid-feedback">
                            {{ $errors->first('numero_socios') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-6 form-group">
                <label for="complemento_socios">Complemento:</label>
                <input type="text" name="complemento_socios" id="complemento_socios" 
                    class="form-control form-control-sm {{ ($errors->has('complemento_socios') ? 'is-invalid' : '') }}"
                    value="{{ old('complemento_socios') }}" maxlength="50" />

                    @if ($errors->has('complemento_socios'))
                        <div class="invalid-feedback">
                            {{ $errors->first('complemento_socios') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-4 form-group">
                <label for="bairro_socios">Bairro: <span class="text-danger">*</span></label>
                <input type="text" name="bairro_socios" id="bairro_socios" 
                    class="form-control form-control-sm {{ ($errors->has('bairro_socios') ? 'is-invalid' : '') }}" readonly="true"
                    value="{{ old('bairro_socios') }}" maxlength="40" />

                    @if ($errors->has('bairro_socios'))
                        <div class="invalid-feedback">
                            {{ $errors->first('bairro_socios') }}
                        </div>
                    @endif
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="municipio_socios">Município: <span class="text-danger">*</span></label>
                <input type="text" name="municipio_socios" id="municipio_socios" 
                    class="form-control form-control-sm {{ ($errors->has('municipio_socios') ? 'is-invalid' : '') }}" readonly="true"
                    value="{{ old('municipio_socios') }}" maxlength="40" />

                    @if ($errors->has('municipio_socios'))
                        <div class="invalid-feedback">
                            {{ $errors->first('municipio_socios') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="estado_socios">Estado: <span class="text-danger">*</span></label>
                <select name="estado_socios" id="estado_socios" class="form-control form-control-sm {{ ($errors->has('estado_socios') ? 'is-invalid' : '') }}" 
                    readonly="true">
                    <option></option>

                    @foreach($estados->geonames as $estado)
                        <option value="{{ $estado->adminCodes1->ISO3166_2 }}" {{ old('estado_socios') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}>
                            {{ $estado->name }}
                        </option>
                    @endforeach
                </select>

                @if ($errors->has('estado_socios'))
                    <div class="invalid-feedback">
                        {{ $errors->first('estado_socios') }}
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-row">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>

            <div class="col-lg-12 form-group">
                <label for="documento_socio_RG"><i class="fa fa-paperclip"></i> Anexar RG do Sócio: <span class="text-danger">*</span></label><br>
                <input type="file" name="documento_socio_RG" 
                    id="documento_socio_RG" />
            </div>

            <div class="col-lg-12">
                <hr/>
            </div>

            <div class="col-lg-12 form-group">
                <label for="documento_socio_cpf"><i class="fa fa-paperclip"></i> Anexar CPF do Sócio: <span class="text-danger">*</span></label><br>
                <input type="file" name="documento_socio_cpf" 
                    id="documento_socio_cpf" />
            </div>
                

            <div class="col-lg-12">
                <hr/>
            </div>

            <div class="col-lg-12 form-group">
                <label for="documento_socio_comprovante_residencia"><i class="fa fa-paperclip"></i> Anexar Comprovante de Endereço (mês atual ou mês anterior): <span class="text-danger">*</span></label><br>
                <input type="file" name="documento_socio_comprovante_residencia" 
                    id="documento_socio_comprovante_residencia" />
            </div>


            
        </div>    
    </div>

    <div class="col-lg-12">
        <div class="form-row">
            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento-socio"
                    data-nome_input="dados_socio" 
                    data-descricao="Socios"
                    data-nome_sessao="Socios"
                    data-conteudo_documentos="conteudo_socios"
                    data-id_tabela="dados_socios"
                    data-pasta="socios">
                    Incluir Dados do Sócio <i class="fa fa-send"></i>
                </button>
            </div>
        </div>
    </div>

</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_socios">
            @include('empresa.mei.socios.tabela_documentos')
        </div>
    </div>
</div>