@if( $processo->Serviço == 'Cadastro Geral do Município - PJ')
    <div class="justfy-content-center processo-fase">
        @if($processo->Fase != "Finalizado" && $processo->Fase != 'Concluído')
            @if($processo->responsavel == "Portal")
            <br>
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                <a href="{{ route('formularioEdita', $processo->Chamado) }}" class="btn btn-blue">Editar
                    Solicitação</a>
            </div>
            @endif
        @endif

        @if($processo->motivo != "" || $processo->Fase == "Cadastro Aprovado")
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Atenção!</strong><br>
            Esta solicitação foi finalizada e teve como conclusão:<br>
            Conclusão: <strong>{{$processo->Fase}}</strong><br>
            @if($processo->motivo !== "")
                Motivo: {{$processo->motivo}}<br>
            @endif
        </div>
        @endif
    </div>
@endif