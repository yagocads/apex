<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Apex Brasil</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- DataTables -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bs-stepper/dist/css/bs-stepper.min.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/header.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css" rel="stylesheet">
    <!-- Font-Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- SELECT2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>
<body>
    <div id="app">
        @include('modals.modal_footer')
        @include('layouts.header')

        <main class="pt-4 pb-4" style="background-color: #F4F4F4">

            <div class="ajax_load">
                <div class="ajax_load_box">
                    <div class="ajax_load_box_circle"></div>
                    <p class="ajax_load_box_title">Aguarde, carregando...</p>
                </div>
            </div>

            @yield('content')
        </main>

        @include('modals.modal_login')
        @include('layouts.footer')
    </div>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>

    <!-- Masks -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>

    <!-- SELECT2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- Javascript -->
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

    @if(session("localex") == "en")
        <!-- Inglês -->
        <script src="//code.jivosite.com/widget/KPfEMLgrC7" async></script>
    @elseif(session("localex") == "es")
        <!-- Espanhol -->
        <script src="//code.jivosite.com/widget/10hYrjHSB8" async></script>
    @else
        <!-- Português -->
        <script src="//code.jivosite.com/widget/26p39bG8YS" async></script>
    @endif

    <script>
        $(document).ready(function () {

            /*
             * Init DataTable
            */
            var table = $('.dataTable').DataTable({
                "oLanguage": {
                    "sProcessing": "{{__('Processando...')}}",
                    "sLengthMenu": "{{__('_MENU_ resultados por página')}}",
                    "sZeroRecords": "{{__('Não foram encontrados resultados')}}",
                    "sInfo": "{{__('Mostrando de _START_ até _END_ de _TOTAL_ registros')}}",
                    "sInfoEmpty": "{{__('Mostrando de 0 até 0 de 0 registros')}}",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "{{__('Pesquisar')}}:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "{{__('Primeiro')}}",
                        "sPrevious": "{{__('Anterior')}}",
                        "sNext": "{{__('Próximo')}}",
                        "sLast": "{{__('Último')}}"
                    }
                }
            });

            var tableParametrosOff = $('.dataTableParametrosOff').DataTable({
                "oLanguage": {
                    "sProcessing": "{{__('Processando...')}}",
                    "sLengthMenu": "{{__('_MENU_ resultados por página')}}",
                    "sZeroRecords": "{{__('Não foram encontrados resultados')}}",
                    "sInfo": "{{__('Mostrando de _START_ até _END_ de _TOTAL_ registros')}}",
                    "sInfoEmpty": "{{__('Mostrando de 0 até 0 de 0 registros')}}",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "{{__('Pesquisar')}}:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "{{__('Primeiro')}}",
                        "sPrevious": "{{__('Anterior')}}",
                        "sNext": "{{__('Próximo')}}",
                        "sLast": "{{__('Último')}}"
                    }
                },
                "bLengthChange": false,
                "ordering": false,
                "info": false,
                "searching": false,
                "paginate": false
            });

            $('.nav-link').on('shown.bs.tab', function() {
                table.columns.adjust().responsive.recalc();
                tableParametrosOff.columns.adjust().responsive.recalc();
            });

            /*
             * Accordion (Agrupamento Deslizante)
            */
            $('#accordion').on('shown.bs.collapse', function (e) {

                table.columns.adjust().responsive.recalc();

                var panelHeadingHeight = $(this).height();
                var animationSpeed = 900;
                var currentScrollbarPosition = $(document).scrollTop();
                var topOfPanelContent = $(e.target).offset().top - 142;

                if (currentScrollbarPosition > topOfPanelContent - panelHeadingHeight) {
                    $("html, body").animate({ scrollTop: topOfPanelContent}, animationSpeed);
                }
            });

            $("#btnAbrirITBIMenu").click(function(){
                $(".modal-title").html("ITBI OnLine");

                $(".modal-body").html("<p>Para solicitar o lançamento do ITBI, siga os passos abaixo:</p><p>1. Faça o download do FORMULÁRIO DE SOLICITAÇÃO DE ITBI, preencha preferencialmente de forma eletrônica e anexe-o na solicitação do processo.<br></p><p class='text-center'><a href='{{  url('fileDownload/1') }}' class='btn btn-primary'><i class='icon-download-alt'></i> FORMULÁRIO DE SOLICITAÇÃO DE ITBI</a></p><p>&nbsp;</p><p>2. Veja as observações e os documentos necessários para a solicitação do ITBI <br></p><p class='text-center'><a href='{{  url('fileDownload/2') }}' class='btn btn-primary'><i class='icon-download-alt'></i> DOCUMENTOS PARA SOLICITAÇÃO DE ITBI</a></p><p>&nbsp;</p>");
                $(".modal-footer").html("<button data-dismiss='modal' class='btn btn-danger' type='button'>Cancelar</button><a href='{{ route('itbionline') }}'><button id='btn_contato' class='btn btn-primary' type='button'>Continuar</button></a>");

                $("#footer-modal").modal('show');
            });

            if (navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
            ) {
                $('[data-toggle="tooltip"]').tooltip('dispose');
                $(".texto_servico").removeClass('d-none');
            } else {
                $('[data-toggle="tooltip"]').tooltip();
                $(".texto_servico").addClass('d-none');
            }
        });

        function recalcularDataTable(id = null, parametros = true) {
            let dataTable = "";

            if (id) {
                dataTable = "#" + id;
            } else {
                dataTable = ".dataTable";
            }

            $(dataTable).DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "{{__('Processando...')}}",
                    "sLengthMenu": "{{__('_MENU_ resultados por página')}}",
                    "sZeroRecords": "{{__('Não foram encontrados resultados')}}",
                    "sInfo": "{{__('Mostrando de _START_ até _END_ de _TOTAL_ registros')}}",
                    "sInfoEmpty": "{{__('Mostrando de 0 até 0 de 0 registros')}}",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "{{__('Pesquisar')}}:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "{{__('Primeiro')}}",
                        "sPrevious": "{{__('Anterior')}}",
                        "sNext": "{{__('Próximo')}}",
                        "sLast": "{{__('Último')}}"
                    }
                },
                "bLengthChange": parametros,
                "ordering": parametros,
                "info": parametros,
                "searching": parametros,
                "paginate": parametros,
                "bAutoWidth": false
            }).columns.adjust().responsive.recalc();
        }
    </script>

    @yield('post-script')
</body>
</html>
