<div class="accordion-inner">
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="cep" class="col-md-4 col-form-label text-md-right">
                {{ __('CEP') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input id="cep" type="text" class="form-input" name="cep" value="{{$empresa->cep}}"
            @if($empresa->cep != "")
            readonly
            @endif
            >

        </div>
        {{-- <div class="span2 align-left">
            <button type="button" class="btn e_wiggle align-right" id="btn_consultar">
                {{ __('Buscar Cep') }}
            </button>
        </div> --}}
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="uf" class="col-md-4 col-form-label text-md-right">
                {{ __('Estado') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <select id="uf" class="form-input" name="uf" disabled>
                <option value="AC" {{ $empresa->uf == "AC" ? "selected" : "" }}>Acre</option>
                <option value="AL" {{ $empresa->uf == "AL" ? "selected" : "" }} >Alagoas</option>
                <option value="AP" {{ $empresa->uf == "AP" ? "selected" : "" }}>Amapá</option>
                <option value="AM" {{ $empresa->uf == "AM" ? "selected" : "" }}>Amazonas</option>
                <option value="BA" {{ $empresa->uf == "BA" ? "selected" : "" }}>Bahia</option>
                <option value="CE" {{ $empresa->uf == "CE" ? "selected" : "" }}>Ceará</option>
                <option value="DF" {{ $empresa->uf == "DF" ? "selected" : "" }}>Distrito Federal</option>
                <option value="ES" {{ $empresa->uf == "ES" ? "selected" : "" }}>Espírito Santo</option>
                <option value="GO" {{ $empresa->uf == "GO" ? "selected" : "" }}>Goiás</option>
                <option value="MA" {{ $empresa->uf == "MA" ? "selected" : "" }}>Maranhão</option>
                <option value="MT" {{ $empresa->uf == "MT" ? "selected" : "" }}>Mato Grosso</option>
                <option value="MS" {{ $empresa->uf == "MS" ? "selected" : "" }}>Mato Grosso do Sul</option>
                <option value="MG" {{ $empresa->uf == "MG" ? "selected" : "" }}>Minas Gerais</option>
                <option value="PA" {{ $empresa->uf == "PA" ? "selected" : "" }}>Pará</option>
                <option value="PB" {{ $empresa->uf == "PB" ? "selected" : "" }}>Paraíba</option>
                <option value="PR" {{ $empresa->uf == "PR" ? "selected" : "" }}>Paraná</option>
                <option value="PE" {{ $empresa->uf == "PE" ? "selected" : "" }}>Pernambuco</option>
                <option value="PI" {{ $empresa->uf == "PI" ? "selected" : "" }}>Piauí</option>
                <option value="RJ" {{ $empresa->uf == "RJ" ? "selected" : "" }}>Rio de Janeiro</option>
                <option value="RN" {{ $empresa->uf == "RN" ? "selected" : "" }}>Rio Grande do Norte</option>
                <option value="RS" {{ $empresa->uf == "RS" ? "selected" : "" }}>Rio Grande do Su</option>
                <option value="RO" {{ $empresa->uf == "RO" ? "selected" : "" }}>Rondônia</option>
                <option value="RR" {{ $empresa->uf == "RR" ? "selected" : "" }}>Roraima</option>
                <option value="SC" {{ $empresa->uf == "SC" ? "selected" : "" }}>Santa Catarina</option>
                <option value="SP" {{ $empresa->uf == "SP" ? "selected" : "" }}>São Paulo</option>
                <option value="SE" {{ $empresa->uf == "SE" ? "selected" : "" }}>Sergipe</option>
                <option value="TO" {{ $empresa->uf == "TO" ? "selected" : "" }}>Tocantins</option>
            </select>
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="cidade" class="col-md-4 col-form-label text-md-right">
                {{ __('Município') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input readonly maxlength="40" id="cidade" type="text" class="form-input" name="cidade" value="{{$empresa->municipio}}">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="bairro" class="col-md-4 col-form-label text-md-right">
                {{ __('Bairro') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input readonly maxlength="40" id="bairro" type="text" class="form-input" name="bairro" value="{{$empresa->bairro}}">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="logradouro" class="col-md-4 col-form-label text-md-right">
                {{ __('Logradouro') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="logradouro" type="text" class="form-input" name="logradouro" value="{{$empresa->logradouro}}"
            @if($empresa->logradouro != "")
            readonly
            @endif
            >

        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="numero" class="col-md-4 col-form-label text-md-right">
                {{ __('Número') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input maxlength="4" id="numero" type="text" class="form-input" name="numero" style="margin-bottom: 1px;" value="{{$empresa->numero}}"
            @if((((int)$empresa->numero)*1) != 0)
            readonly
            @endif
            >
            <span style="font-size: 10px;"><b>Se não tiver Número, colocar 0</b></span>
        </div>

        <div class="controls span2 form-label">
            <label for="complemento" class="col-md-4 col-form-label text-md-right">
                {{ __('Complemento') }}
            </label>
        </div>

        <div class="span3">
            <input maxlength="50" id="complemento" type="text" class="form-input" name="complemento" value="{{$empresa->complemento}}"
            @if($empresa->complemento != "")
            readonly
            @endif
            >
        </div>
    </div>


    <form method="POST" action="" id="formEndereco" name="formEndereco" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Comprovante de Endereço') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Comprovante de Endereço" />
                <input type="hidden" name="tipoDoc" value="ComprovanteEndereco" />
                <input type="hidden" name="idCpf" id="idEndereco" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoEndereco" /><br>
                Obs.: O tamanho máximo dos arquivos é de 2Mb.
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoEndereco">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <div class="row formrow">
        <div class="span11">
            <h4 class="heading">Documentos Lançados<span></span></h4>

            <table id="documentosEnderecoTbl" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th width='20%'>Descrição</th>
                        <th>Nome Original</th>
                        <th>Nome Final</th>
                        <th width='20%'>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
<br>
<br>
</div>