<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroFomentaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Crédito Emergencial
            'motivoSolicitacao' => 'required',
            'valorSolicitado' => 'required|max:5000',
            // 'tempoCarencia' => 'max:6',
            'prazo' => 'required',
            'valorParcela' => 'required',

            // Dados do Requerente
            'nome_requerente' => 'required_if:responsavel,2',
            'identidade_requerente' => 'required_if:responsavel,2',
            'orgao_emissor_requerente' => 'required_if:responsavel,2',
            'data_emissao_requerente' => 'required_if:responsavel,2',
            'data_emissao_requerente' => 'before:today',
            'cpf_requerente' => 'required_if:responsavel,2',
            'email_requerente' => 'required_if:responsavel,2',

            // Endereço
            'cep_requerente' => 'required|string',
            'endereco_requerente' => 'required|string|max:191',
            'bairro_requerente' => 'required|max:40',
            'numero_requerente' => 'required',
            'municipio_requerente' => 'required|max:40',
            'estado_requerente' => 'required',

            // Dados da Empresa
            'razao_social' => 'sometimes|required',
            'nome_fantasia' => 'sometimes',
            'natureza_juridica' => 'sometimes|required',
            'data_abertura_empresa' => 'sometimes|required',
            'data_inicio_atividade' => 'sometimes|required',
            'confirmacao_mumbuca' => 'required',
            'agPreferencia' => 'required',

            // Informações de Contato
            'email' => 'required',
            'confirmacao_email' => 'required',
            'celular' => 'required',

            // Endereço
            'cep' => 'required|string',
            'endereco' => 'required|string|max:191',
            'bairro' => 'required|max:40',
            'municipio' => 'required|max:40',
            'estado' => 'required',

            'autodeclaracao' => 'required',
            'confirmacao_de_informacoes' => 'required'
        ];
    }

    public function messages()
    {  
        return [
            // Crédito Emergencial
            'motivoSolicitacao.required' => 'Preencha o campo: Por qual motivo está solicitando o empréstimo através do programa Fomenta Maricá? (Crédito Emergencial)',
            'valorSolicitado.required' => 'Preencha o campo: De quanto você precisa? (Máximo R$ 5.000,00) (Crédito Emergencial)',
            'valorSolicitado.max' => 'O valor máximo para o benefício é R$ 5.000,00 (Crédito Emergencial)',
            'tempoCarencia.required' => 'Preencha o campo: Carência (de 0 a 6 meses) (Crédito Emergencial)',
            'tempoCarencia.max' => 'O tempo máximo de carência é de 6 meses (Crédito Emergencial)',
            'prazo.required' => 'Preencha o campo: Prazo (até 18 meses) (Crédito Emergencial)',
            'valorParcela.required' => 'Preencha o campo: Qual é a parcela em que você se sente confortável para pagar mensalmente? (Crédito Emergencial)',

            // Dados do Requerente
            'nome_requerente.required_if' => 'Preencha o campo: Nome (Dados do Requerente)',
            'identidade_requerente.required_if' => 'Preencha o campo: Identidade (Dados do Requerente)',
            'orgao_emissor_requerente.required_if' => 'Preencha o campo: Órgão Emissor (Dados do Requerente)',
            'data_emissao_requerente.required_if' => 'Preencha o campo: Data Emissão (Dados do Requerente)',
            'data_emissao_requerente.before' => 'A Data Emissão deve ser anterior a data atual (Dados do Requerente)',
            'cpf_requerente.required_if' => 'Preencha o campo: CPF (Dados do Requerente)',
            'email_requerente.required_if' => 'Preencha o campo: E-mail (Dados do Requerente)',
            
            // Endereço do Requerente
            'cep_requerente.required' => 'Preencha o campo: CEP (Endereço do Requerente)',
            'endereco_requerente.required' => 'Preencha o campo: Endereço (Endereço do Requerente)',
            'numero_requerente.required' => 'Preencha o campo: Número (Endereço do Requerente)',
            'complemento_requerente.required' => 'Preencha o campo: Complemento (Endereço do Requerente)',
            'bairro_requerente.required' => 'Preencha o campo: Bairro (Endereço do Requerente)',
            'municipio_requerente.required' => 'Preencha o campo: Município (Endereço do Requerente)',
            'estado_requerente.required' => 'Preencha o campo: Estado (Endereço do Requerente)',
            
            // Dados da Empresa
            'razao_social.required' => 'Preencha o campo: Razão Social (Dados da Empresa)',
            'nome_fantasia.required' => 'Preencha o campo: Nome Fantasia (Dados da Empresa)',
            'natureza_juridica.required' => 'Preencha o campo: Natureza Jurídica (Dados da Empresa)',
            'data_abertura_empresa.required' => 'Preencha o campo: Data de Abertura (Dados da Empresa)',
            'agPreferencia.required' => 'Preencha o campo: Agência de preferência do banco Mumbuca (Dados da Empresa)',
            'confirmacao_mumbuca.required' => 'Preencha o campo: Declaro que estou ciente e de acordo com a abertura de conta bancária em meu nome junto ao Banco Mumbuca caso minha solicitação de crédito ao Programa Fomenta Maricá MEI Emergencial Covid-19 seja aprovada. (Dados da Empresa)',
            'data_inicio_atividade.required' => 'Preencha o campo: Data de início da Atividade (Dados da Empresa)',

            // Informações de Contato
            'email.required' => 'Preencha o campo: E-mail (Informações de Contato)',
            'confirmacao_email.required' => 'Preencha o campo: Confirmação de E-mail (Informações de Contato)',
            'celular.required' => 'Preencha o campo: Celular (Informações de Contato)',

            // Endereço
            'cep.required' => 'Preencha o campo: CEP (Endereço)',
            'endereco.required' => 'Preencha o campo: Endereço (Endereço)',
            'numero.required' => 'Preencha o campo: Número (Endereço)',
            'complemento.required' => 'Preencha o campo: Complemento (Endereço)',
            'bairro.required' => 'Preencha o campo: Bairro (Endereço)',
            'municipio.required' => 'Preencha o campo: Município (Endereço)',
            'estado.required' => 'Preencha o campo: Estado (Endereço)',

            'autodeclaracao.required' => 'Preencha o campo: Declaro para fins de direito que faço jus ao Programa Fomenta Maricá MEI Emergencial Covid-19 pois exerço atividade econômica como Microempreendedor no Município de Maricá e desejo utilizar esta linha de crédito para suavizar os impactos do isolamento social necessário ao enfrentamento da pandemia do coronavírus.',
            'confirmacao_de_informacoes.required' => 'Preencha o campo: Declaro que são VERDADEIRAS e EXATAS todas as informações que foram prestadas neste formulário. Declaro ainda estar ciente de que declaração falsa no presente cadastro constituirá crime de falsidade ideológica (art. 299 do Código Penal) e estará sujeita a sanções penais sem prejuízo de medidas administrativas e outras'
        ];
    }
}
