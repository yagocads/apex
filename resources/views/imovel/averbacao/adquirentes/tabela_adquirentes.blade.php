
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela_adquirentes">
        <thead>
            <tr>
                <th>Nome/Razão Social</th>
                <th>CPF/CNPJ</th>
                <th>CGM</th>
                <th>Principal</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("adquirentes"))
                @foreach(session("adquirentes") as $adquirente)
                    <tr>
                        <td>{{ convert_accentuation($adquirente->nome) }}</td>
                        <td>{{ $adquirente->cgccpf }}</td>
                        <td>{{ $adquirente->numcgm }}</td>
                        <td>{{ ($adquirente->princ == "Sim" ? "Sim" : "Não") }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm remover_adquirente" 
                                title="Remover Adquirente" data-document="{{ $adquirente->cgccpf }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
