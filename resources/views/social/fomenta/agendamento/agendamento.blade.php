<form method="POST" action="{{ route('fomenta_agendamento') }}" id="fomenta_agendamento" name="fomenta_agendamento">
    @csrf 
    <input id="id_fomenta" type="hidden" name="id_fomenta" value="{{ $usuario->id }}" />
    <input id="cnpj" type="hidden" name="cnpj" value="{{ $usuario->cnpj }}" />
    <input id="processo" type="hidden" name="processo" value="{{ $processo->chamado }}" />
    <input id="etapa" type="hidden" name="etapa" value="{{ $processo->etapa }}" />
    <input id="ciclo" type="hidden" name="ciclo" value="{{ $processo->ciclo }}" />
    <input id="hora_agendamento" type="hidden" name="hora_agendamento" value="" />
    <input id="posto_agendamento" type="hidden" name="posto_agendamento" value="" />
    <input id="local_agendamento1" type="hidden" name="local_agendamento" value="" />
    <input id="data_agendamento" type="hidden" name="data_agendamento" value="" />
</form>

<div class="requerente_prosseguimento">
    <div class="row">
        <div class="col-lg-6">
            <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Local do Agendamento:</h5>

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="local_agendamento">Escolha o Local de agendamento:</label>
                    <select name="local_agendamento" id="local_agendamento" class="form-control form-control-sm {{ ($errors->has('local_agendamento') ? 'is-invalid' : '') }}">
                        <option value="">Selecione...</option>
                        <option value="CENTRO">CENTRO – SEDE SECRETARIA DE DESENVOLVIMENTO</option>
                        <option value="ITAIPUAÇU">ITAIPUAÇU – PRÉDIO NOVO DO SIM</option>
                    </select>

                    @if ($errors->has('local_agendamento'))
                        <div class="invalid-feedback">
                            {{ $errors->first('local_agendamento') }}
                        </div>
                    @endif

                </div>
            </div>
            <br/>
            <h5><i class="fa fa-calendar mt-6 mb-3"></i> Data do Agendamento:</h5>
            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="datas_disponiveis">Escolha as datas disponíveis:</label>
                    <select name="datas_disponiveis" id="datas_disponiveis" class="form-control form-control-sm {{ ($errors->has('datas_disponiveis') ? 'is-invalid' : '') }}">
                        <option value="">Selecione...</option>
                    </select>

                    @if ($errors->has('datas_disponiveis'))
                        <div class="invalid-feedback">
                            {{ $errors->first('datas_disponiveis') }}
                        </div>
                    @endif

                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-12 form-group text-right">
                    <button class="btn btn-success mt-3" id="btn_consultar_horário"><i class="fa fa-calendar"></i> Consultar Horários disponíveis</button>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6">
            <h5><i class="fa fa-clock-o mt-3 mb-3"></i> Horários Disponíveis:</h5>

            <div class="conteudo_horario" id="conteudo_horarios">
                @include('social.fomenta.agendamento.tabela_horarios')
            </div>
        </div>
    </div>
</div>
