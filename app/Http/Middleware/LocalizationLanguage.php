<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class LocalizationLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(session('localex'));
        $locale = session("localex");

        // Define os locales da aplicação
        $languages = ['pt-br', 'en', 'es'];

        // Verifica se o locale passado na URL é válido
        // Se sim, então muda o idioma da aplicação
        // Se não, então deixa o idioma padrão
        if (in_array($locale, $languages)) {
            // App::setLocale('pt-br');
            App::setLocale($locale);
        }

        return $next($request);
    }
}
