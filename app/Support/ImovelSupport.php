<?php

namespace App\Support;

class ImovelSupport extends CurlSupport
{
    /**
     * Constructor of ImovelSupport
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Buscar ITBI’s quitados referente ao imóvel informado
     *
     * @param string $registration (matricula do imóvel)
     * @return mixed
     */
    public function listItbiImmobile($registration)
    {
        $this->url = API_URL . API_ITBI;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao'    => API_TOKEN,
                'sExec'              => API_LISTA_ITBI,
                's_limit'            => "0",
                'i_matricula_imovel' => $registration
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Lista de imoveis
     *
     * @param mixed $pagination
     * @param string $document
     * @return mixed
     */
    public function listProperties($pagination = null, $document, $sExec = null)
    {
        // $this->url = 'http://vds3010x5.startdedicated.com:8090/api-ecidade-online-integracao-cerdidoes-paginacao/integracaoCerdidoes.RPC.php';
        $this->url = API_URL . API_CERTIDAO;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'           => empty($sExec) ? API_MATRICULA_CONSULTA : $sExec,
                'z01_cgccpf'      => $document,
                'sTokeValiadacao' => API_TOKEN,
                'i_paginacao'     => !empty($pagination) ? $pagination : 0,
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Lista informações do imóvel apartir da matricula
     *
     * @param string $registration
     * @param string $document
     * @return mixed
     */
    public function getPropertie($registration, $document)
    {
        $this->url = API_URL . API_IMOVEL;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'             => API_CONSULTA_IMOVEL,
                'i_cpfcnpj'         => $document,
                'sTokeValiadacao'   => API_TOKEN,
                'i_codigo_maticula' => $registration
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Informações do Imóvel ITBI
     *
     * @param string $registration
     * @return mixed
     */
    public function getInfoPropertieITBI($registration)
    {
        $this->url = API_URL . API_ITBI;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'             => API_IMOVEL_ITBI,
                'sTokeValiadacao'   => API_TOKEN,
                'i_codigo_maticula' => $registration
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Confere se o imóvel foi baixado ou não
     *
     * @param string $registration
     * @param string $document
     * @return mixed
     */
    public function getImovelBaixado($registration, $document)
    {
        $this->url = API_URL . API_CERTIDAO;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'             => API_IMOVEL_CERTIDAO,
                'z01_cgccpf'         => $document,
                'sTokeValiadacao'   => API_TOKEN,
                'i_codigo_maticula' => $registration
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * searchPropertie
     *
     * @param  mixed $registration
     * @param  mixed $document
     * @return void
     */
    public function searchPropertie($data, $document, $page = null)
    {
        // $this->url = 'http://vds3010x5.startdedicated.com:8090/api-ecidade-online-integracao-cerdidoes-paginacao/integracaoCerdidoes.RPC.php';
        $this->url = API_URL . API_CERTIDAO;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'             => 'integracaolistacertidoesimoveis',
                'z01_cgccpf'        => $document,
                'sTokeValiadacao'   => API_TOKEN,
                "i_paginacao"       => $page ?? 1,
                "filtro_matricula"  => $data->matricula,
                "filtro_logradouro" => empty($data->logradouro) ? null : $data->logradouro,
                "filtro_planta"     => empty($data->planta) ? null : $data->planta,
                "filtro_quadra"     => empty($data->quadra) ? null : $data->quadra,
                "filtro_lote"       => empty($data->lote) ? null : $data->lote,
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Recupera os adquirentes de um imóvel
     *
     * @param string $guia
     * @return mixed
     */
    public function getAdquirentesImovel(string $guia)
    {
        $this->url = API_URL . API_AVERBACAO;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao' => API_TOKEN,
                'sExec'           => API_LISTA_ADQUIRENTES_ITBI,
                'i_codigo_guia'   => $guia,
                's_tipo'          => "C",
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Tipo de Averbação
     *
     * @return mixed
     */
    public function getTipoAverbacao()
    {
        $this->url = API_URL . API_AVERBACAO;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao' => API_TOKEN,
                'sExec'           => API_TIPO_AVERBACAO
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Recuperando dados do CGM
     *
     * @param string $document
     * @return mixed
     */
    public function getDadosCGM(string $document)
    {
        $this->url = API_URL . API_CGM;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'         => API_CONSULTA_CGM,
                'z01_cgccpf'    => $document,
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Recuperando dados do CGM
     *
     * @param string $document
     * @param string $tipo
     *
     * @return mixed
     */
    public function getEnderecoCGM(string $document, string $tipo)
    {
        $this->url = API_URL . API_CGM;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'         => API_CONSULTA_ENDERECO_CGM,
                'sCpfcnpj'      => $document,
                'sTipoEndereco' => $tipo
            ])
        ];

        return $this->request()->callback();
    }

    public function getDebitosImovel(string $matriculaImovel)
    {
        $this->url = API_URL . 'integracaoGeralFinanceiraDebitos.RPC.php';
        // $this->url = 'https://10.135.16.85:8090/api-ecidade-online/integracaoGeralFinanceiraDebitos.RPC.php';
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec' => 'integracaopesquisadebitoscgmsolicitante',
                'i_matricula' => $matriculaImovel,
                'sTokeValiadacao' => API_TOKEN
                //'sTokeValiadacao' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLWVjaWRhZGUtb25saW5lLmxvY2FsIiwiYXVkIjoiaHR0cDpcL1wvbGVjb20ubG9jYWwiLCJpYXQiOjE1ODA3MjU0NTgsIm5iZiI6MTU4MDcyNTQ1OCwidWlkIjoyLCJ1bmFtZSI6ImxlY29tIiwidXJsIjoiaHR0cDpcL1wvYnBtLm1hcmljYS5yai5nb3YuYnJcLyJ9.VPu-wEhh7Btm_xLYbcTMj9jdAGRDU3X5_dKRziADqg4',
            ])
        ];

        return $this->request()->callback();
    }
}
