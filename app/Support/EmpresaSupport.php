<?php

namespace App\Support;

class EmpresaSupport extends CurlSupport
{
    /**
     * Constructor of EmpresaSupport
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Capturar informações da Receita
     *
     * @param string $document
     * @return mixed
     */
    public function empresa($document){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.receitaws.com.br/v1/cnpj/". $document ."/days/30",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "Authorization: Bearer b21989b535bde93f103ef1254ca087f703d704615e13bff434282c6abee13d97"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }
}
