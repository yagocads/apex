@extends('layouts.tema_principal')

@section('content')

    @include('layouts.slide')
    @include('layouts.servicos')

@endsection
@isset($mensagem)
    @if($mensagem == "respostafaleconosco")
        @section('post-script')
            <script>
                $(".modal-title").html("{{__('Fale Conosco')}}");

                $(".modal-body").html("<p></p><b>{{__('Sucesso')}}!</b></p><p>{{__('Enviamos a sua mensagem com sucesso')}}.</p><p>{{__('Em breve retornaremos o contato')}}.</p>");
                $(".modal-footer").html("<button data-dismiss='modal' class='btn btn-primary' type='button'>OK</button>");

                $("#footer-modal").modal('show');
            </script>
        @endsection
    @endif
@endisset
