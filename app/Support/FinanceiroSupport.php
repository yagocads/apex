<?php

namespace App\Support;

class FinanceiroSupport extends CurlSupport
{
    /**
     * Constructor of ImovelSupport
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Busca os anos de pagamentos
     *
     * @param string $registration (matricula do imóvel)
     * @return mixed
     */

    public function verificaAnosPg($matInsc, $document, $tipoPesquisa)
    {
        $this->url = API_URL_GFD . API_GFD;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'         => 'integracaopesquisapagamentosefetuadosano',
                'i_cpfcnpj'    => $document,
                'i_matricula_inscricao' => $matInsc,
                'c_tipo_pesquisa' => $tipoPesquisa,
                'sTokeValiadacao'   => API_TOKEN,
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * verifica se data informada pode gerar o boleto da divida
     *
     * @param string $date
     * @return mixed
     */

    public function verificaDiaUtil($date)
    {
        $this->url = API_URL . 'integracaoTributarioArrecadacaoCad.RPC.php';
        // $this->url = 'https://10.135.16.85:8090/api-ecidade-online/integracaoTributarioArrecadacaoCad.RPC.php';
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sExec'         => 'integracaocaledariodiautil',
                'dDataVerificar'    => $date,
                'sTokeValiadacao'   => API_TOKEN,
            ])
        ];

        return $this->request()->callback();
    }
}
