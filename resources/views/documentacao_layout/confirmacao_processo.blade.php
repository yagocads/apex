<div class="card">
    <div class="card-header">
        <h5>Confirmação do Processo</h5>
    </div>

    <div class="card-body">
        <h6><b>Alerta para confirmação de processo logado no sistema:</b></h6>
        <hr>

        <div class="alert alert-success py-5 mb-5">
            <h3 class="mb-4"><i class="fa fa-check-square-o"></i> Cadastro realizado com sucesso!</h3>

            <p><b>Identificação: </b> (Nome do usuário que requisitou)</p>

            <p><b>CPF/CNPJ:</b> <span class="cpfOuCnpj">14537207743</span></p>

            <p><b>ATENÇÃO:</b> Para consultar o processo, acesse a página de consulta no link abaixo:</p>
            <a href="{{ route('acompanhamento') }}">Clique aqui para consultar o processo</a>
        </div>

        <h6><b>Alerta para confirmação de processo sem precisar logar no sistema:</b></h6>
        <hr>

        <div class="alert alert-success py-5">
            <h3 class="mb-4"><i class="fa fa-check-square-o"></i> Cadastro realizado com sucesso!</h3>

            <p><b>ATENÇÃO:</b> Guarde o número de protocolo, 
            ele será solicitado para fazer a consulta do andamento desta solicitação.</p>

            <p><b>Identificação: </b> (Nome do usuário que requisitou)</p>

            <p><b>CPF/CNPJ:</b> <span class="cpfOuCnpj">14537207743</span></p>
            <p><b>Protocolo:</b> (Número de protocolo)</p>

            <button class="btn btn-success">
                Imprimir Protocolo <i class="fa fa-print"></i>
            </button>            
        </div>
    </div>
</div>