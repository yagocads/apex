{{-- <div class="form-row">
    <div class="col-lg-12 form-group">
        <div class="alert alert-info">
            <i class="fa fa-volume-up"></i> OBS: O e-mail será vinculado ao CPF e todo o contato da prefeitura com o 
            cidadão será feito por meio deste.
        </div>
    </div>
</div> --}}

<div class="form-row">
    <div class="col-lg-4 form-group">
        <label for="residencial">Residencial:</label>
        <input type="text" name="residencial" id="residencial" 
            class="form-control form-control-sm mask-tel {{ ($errors->has('residencial') ? 'is-invalid' : '') }}" 
            value="{{ old('residencial') }}" />

            @if ($errors->has('residencial'))
                <div class="invalid-feedback">
                    {{ $errors->first('residencial') }}
                </div>
            @endif
    </div>

    <div class="col-lg-4 form-group">
        <label for="comercial">Comercial:</label>
        <input type="text" name="comercial" id="comercial" 
            class="form-control form-control-sm mask-tel {{ ($errors->has('comercial') ? 'is-invalid' : '') }}" 
            value="{{ old('comercial') }}" />

            @if ($errors->has('comercial'))
                <div class="invalid-feedback">
                    {{ $errors->first('comercial') }}
                </div>
            @endif
    </div>

    <div class="col-lg-4 form-group">
        <label for="celular">Celular: <span class="text-danger">*</span></label>
        <input type="text" name="celular" id="celular" 
            class="form-control form-control-sm mask-cel {{ ($errors->has('celular') ? 'is-invalid' : '') }}" 
            value="{{ old('celular') }}" />

            @if ($errors->has('celular'))
                <div class="invalid-feedback">
                    {{ $errors->first('celular') }}
                </div>
            @endif
    </div>
</div> 
