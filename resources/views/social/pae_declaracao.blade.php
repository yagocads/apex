<div class="accordion-inner">



    <div class="row formrow">
        <div class="controls span3 form-label">
        </div>

        <div class="span8">
            <b></b>Declaro, para os fins no disposto da Lei Municipal nº2929/2020 que:</b>
        </div>
    </div>
    <br>
    {{-- <div class="row formrow">
        <div class="controls span3 form-label">
            <label class="col-md-4 col-form-label text-md-right">
                
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                possuo ciência acerca das sanções administrativas, cíveis e penais decorrentes da apresentação de informações falsas ou inverídicas, para acesso ao PROGRAMA DE AMPARO AO TRABALHADOR – PAT.
                <br><br>
<b>Código Penal<br>
Art. 299</b> - Omitir, em documento público ou particular, declaração que dele devia constar, ou nele inserir ou fazer inserir declaração falsa ou diversa da que devia ser escrita, com o fim de prejudicar direito, criar obrigação ou alterar a verdade sobre fato juridicamente relevante: <br>
Pena - reclusão, de um a cinco anos, e multa, se o documento é público, e reclusão de um a três anos, e multa, de quinhentos mil réis a cinco contos de réis, se o documento é particular. 
<br><br>
<b>Código Civil</b><br>
<b>Art. 927.</b> Aquele que, por ato ilícito ( arts. 186 e 187 ), causar dano a outrem, fica obrigado a repará-lo.<br>
<b>Art. 186.</b> Aquele que, por ação ou omissão voluntária, negligência ou imprudência, violar direito e causar dano a outrem, ainda que exclusivamente moral, comete ato ilícito.<br>
<b>Art. 187.</b> Também comete ato ilícito o titular de um direito que, ao exercê-lo, excede manifestamente os limites impostos pelo seu fim econômico ou social, pela boa-fé ou pelos bons costumes.<br>
<br><br>

<b>De acordo com a Lei 2920/2020, modificada pela Lei 2922/2020</b><br>
<b>Art. 6º</b> A apresentação de declaração ou documento em desconformidade com o ordenamento jurídico poderá sujeitar às sanções administrativas, cíveis e penais correspondentes.<br>
<b>§1º</b> O disposto no caput deste artigo poderá ainda importar em descredenciamento e impossibilidade de credenciamento nos programas em âmbito municipal, pelo período de 3 (três) anos.<br>
<b>§2º</b> A atuação de servidor que possibilite a circunstância descrita no caput deste artigo ocasionará a instauração de processo administrativo disciplinar, passível de todas as sanções em âmbito administrativo, cível e penal constantes no ordenamento jurídico. <br>
<br>
            </div>
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span3">
            <input type="checkbox" id="declaracao1" name="declaracao1" class="form-input"> Li e declaro estar ciente dos termos. <span class="required">*</span>
            <br>
            @if ($errors->has('declaracao1'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('declaracao1') }}</strong>
            </span>
            @endif 
        </div>
    </div> 

    <br><br>       --}}
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label class="col-md-4 col-form-label text-md-right">
                
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                A empresa está localizada no Município de Maricá
            </div>
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span3">
            <input type="checkbox" id="declaracao1" name="declaracao1" class="form-input"> Li e declaro estar ciente dos termos. <span class="required">*</span>
            <br>
        </div>
    </div>  

    <br><br>     
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label class="col-md-4 col-form-label text-md-right">
                
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                Se enquadra como microempreendedor individual (MEI), microempresa ou empresa de pequeno porte, nos termos da Lei Complementar nº 123, de 14 de dezembro de 2006.
            </div>
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span3">
            <input type="checkbox" id="declaracao2" name="declaracao2" class="form-input"> Li e declaro estar ciente dos termos. <span class="required">*</span>
            <br>
        </div>
    </div>   
    
    <br><br>     
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label class="col-md-4 col-form-label text-md-right">
                
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                Teve suas atividades suspensas, mesmo que parcialmente, por consequência das determinações da Prefeitura Municipal de Maricá para o isolamento social com o propósito de diminuir a disseminação da COVID 19.
            </div>
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span3">
            <input type="checkbox" id="declaracao3" name="declaracao3" class="form-input"> Li e declaro estar ciente dos termos. <span class="required">*</span>
            <br>
        </div>
    </div>   
    
    <br><br>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label class="col-md-4 col-form-label text-md-right">
                
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                Manterá período de estabilidade do emprego e o valor integral dos salários de seus funcionários, compreendendo período idêntico ao de recebimento do benefício, contados após o pagamento da última parcela do benefício, excetuando-se os casos de demissão por justa causa ou pedido de demissão.
            </div>
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span3">
            <input type="checkbox" id="declaracao4" name="declaracao4" class="form-input"> Li e declaro estar ciente dos termos. <span class="required">*</span>
            <br>
            @if ($errors->has('declaracao4'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('declaracao4') }}</strong>
            </span>
            @endif 
        </div>
    </div>   
    
    <br><br>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label class="col-md-4 col-form-label text-md-right">
                
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                No caso de eventual pendência junto ao fisco Municipal, referentes os anos de 2017, 2018, e 2019, se compromete a regularizar a sua situação fiscal no prazo de até 12 meses após o recebimento da primeira parcela do benefício.
            </div>
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span3">
            <input type="checkbox" id="declaracao5" name="declaracao5" class="form-input"> Li e declaro estar ciente dos termos. <span class="required">*</span>
            <br>
            @if ($errors->has('declaracao5'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('declaracao5') }}</strong>
            </span>
            @endif 
        </div>
    </div>
    <br>
    <br>
</div>