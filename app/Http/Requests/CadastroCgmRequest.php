<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroCgmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Dados do Requerente
            'nome_requerente' => 'required_if:responsavel,2',
            'identidade_requerente' => 'required_if:responsavel,2',
            'orgao_emissor_requerente' => 'required_if:responsavel,2',
            'data_emissao_requerente' => 'required_if:responsavel,2',
            'cpf_requerente' => 'required_if:responsavel,2',
            'email_requerente' => 'required_if:responsavel,2',

            // Informações Pessoais
            'cpf' => 'sometimes|required|cpf',
            'data_nascimento' => 'sometimes|required',
            'sexo' => 'sometimes|required|in:M,F',
            'nome' => 'sometimes|required|string|max:191',
            'nome_mae' => 'sometimes|required|string|max:191',
            'nome_pai' => 'sometimes|required|string|max:191',
            'identidade' => 'sometimes|required|string',
            'orgao_emissor' => 'sometimes|required|string',
            'data_emissao' => 'sometimes|required',
            'estado_civil' => 'sometimes|required',
            'naturalidade' => 'sometimes|required',
            'nacionalidade' => 'sometimes|required|in:brasileira,estrangeira',

            // Dados Mercantis
            'razao_social' => 'sometimes|required',
            'nome_fantasia' => 'sometimes|required',
            'natureza_juridica' => 'sometimes|required',
            'data_abertura_empresa' => 'sometimes|required',
            'inscricao_estadual' => 'sometimes|required',

            // Informações de Contato
            'email' => 'required',
            'confirmacao_email' => 'required',
            'celular' => 'required',

            // Endereço
            'cep' => 'required|string',
            'endereco' => 'required|string|max:191',
            'numero' => 'required|string|max:6',
            'complemento' => 'required|string|max:50',
            'bairro' => 'required|max:40',
            'municipio' => 'required|max:40',
            'estado' => 'required',

            // Informações Culturais
            'artista' => 'required_if:verificar_cgm_na_cultura,0',
            'area_cultural' => 'required_if:artista,sim',
            'area_cultural_principal' => 'required_if:artista,sim',
            'atuacao_cultural' => 'required_if:artista,sim',
            'descricao_atividades' => 'required_if:artista,sim',
            'formacao_area_artistica_cultural' => 'required_if:artista,sim',
            'registro_profissional' => 'required_if:artista,sim',
            'registro_profissional_descricao' => 'required_if:registro_profissional,sim',
            'espaco_cultural_coletivo' => request("artista") == "sim" && request("verificar_cpf_cnpj") == 1 ? "required" : "",
            'numero_doc_espaco_cultural_coletivo' => 'required_if:espaco_cultural_coletivo,sim',
            'tempo_atuacao_area' => 'required_if:artista,sim',
            'alcance_pessoas' => 'required_if:artista,sim',
            'frequencia_atividade' => 'required_if:artista,sim',
            'insercao_atividade_artistico_cultural' => 'required_if:artista,sim',
            'fonte_renda' => 'required_if:artista,sim',
            'pandemia_covid' => 'required_if:artista,sim',
            'recebe_auxilio_governo' => 'required_if:artista,sim',
            'emprego_formal_ativo' => 'required_if:artista,sim',
            'redes_sociais_divulgacao' => 'required_if:artista,sim',
            'lei_emergencia_cultural' => 'required_if:artista,sim',
            'enquadramento_lei_emergencial' => 'required_if:lei_emergencia_cultural,sim',
            'ciente_que_lei_nao_garante_beneficio' => 'required_if:lei_emergencia_cultural,sim',
            'beneficio_previdenciario' => 'required_if:lei_emergencia_cultural,sim',
            'nomes_beneficios_previdenciarios' => 'required_if:beneficio_previdenciario,sim',
            'beneficio_emergencial_lei_13982' => 'required_if:lei_emergencia_cultural,sim',
            'mulher_familia_monoparental' => 'required_if:lei_emergencia_cultural,sim',
            'banco_nome' => 'required_if:artista,sim',
            'banco_tipo_conta' => 'required_if:artista,sim',
            'banco_agencia' => 'required_if:artista,sim',
            'banco_numero_conta' => 'required_if:artista,sim',
            'banco_conta_operacao' => 'required_if:banco_nome,104 - Caixa Econômica Federal',

            'confirmacao_de_informacoes' => 'required'
        ];
    }

    public function messages()
    {  
        return [
            // Dados do Requerente
            'nome_requerente.required_if' => 'Preencha o campo Nome (Dados do Requerente)',
            'identidade_requerente.required_if' => 'Preencha o campo Identidade (Dados do Requerente)',
            'orgao_emissor_requerente.required_if' => 'Preencha o campo Órgão Emissor (Dados do Requerente)',
            'data_emissao_requerente.required_if' => 'Preencha o campo Data Emissão (Dados do Requerente)',
            'cpf_requerente.required_if' => 'Preencha o campo CPF (Dados do Requerente)',
            'email_requerente.required_if' => 'Preencha o campo E-mail (Dados do Requerente)',
            
            // Informações Pessoais
            'cpf.required' => 'Preencha o campo CPF (Informações Pessoais)',
            'data_nascimento.required' => 'Preencha o campo Data de Nascimento (Informações Pessoais)',
            'sexo.required' => 'Preencha o campo Sexo (Informações Pessoais)',
            'nome.required' => 'Preencha o campo Nome (Informações Pessoais)',
            'nome_mae.required' => 'Preencha o campo Nome da Mãe (Informações Pessoais)',
            'nome_pai.required' => 'Preencha o campo Nome do Pai (Informações Pessoais)',
            'identidade.required' => 'Preencha o campo Identidade (Informações Pessoais)',
            'orgao_emissor.required' => 'Preencha o campo Órgão Emissor (Informações Pessoais)',
            'data_emissao.required' => 'Preencha o campo Data Emissão (Informações Pessoais)',
            'estado_civil.required' => 'Preencha o campo Estado Civil (Informações Pessoais)',
            'naturalidade.required' => 'Preencha o campo Naturalidade (Informações Pessoais)',
            'nacionalidade.required' => 'Preencha o campo Nacionalidade (Informações Pessoais)',
            
            // Dados Mercantis
            'razao_social.required' => 'Preencha o campo Razão Social (Dados Mercantis)',
            'nome_fantasia.required' => 'Preencha o campo Nome Fantasia (Dados Mercantis)',
            'natureza_juridica.required' => 'Preencha o campo Natureza Jurídica (Dados Mercantis)',
            'data_abertura_empresa.required' => 'Preencha o campo Data de Abertura (Dados Mercantis)',
            'inscricao_estadual.required' => 'Preencha o campo Inscrição Estadual (Dados Mercantis)',

            // Informações de Contato
            'email.required' => 'Preencha o campo E-mail (Informações de Contato)',
            'confirmacao_email.required' => 'Preencha o campo Confirmação de E-mail (Informações de Contato)',
            'celular.required' => 'Preencha o campo Celular (Informações de Contato)',

            // Endereço
            'cep.required' => 'Preencha o campo CEP (Endereço)',
            'endereco.required' => 'Preencha o campo Endereço (Endereço)',
            'numero.required' => 'Preencha o campo Número (Endereço)',
            'complemento.required' => 'Preencha o campo Complemento (Endereço)',
            'bairro.required' => 'Preencha o campo Bairro (Endereço)',
            'municipio.required' => 'Preencha o campo Município (Endereço)',
            'estado.required' => 'Preencha o campo Estado (Endereço)',

            // Informações Culturais - Liberadas e revisadas
            'artista.required_if' => 'Preencha o campo Você é um artista (Perfil do Cidadão)',
            'area_cultural.required_if' => 'Preencha o campo Área Cultural de Atuação (Informações Culturais)',
            'area_cultural_principal.required_if' => 'Preencha o campo Escolha sua área cultural de atuação principal (Informações Culturais)',
            'atuacao_cultural.required_if' => 'Preencha o campo Atuação Cultural (Informações Culturais)',
            'descricao_atividades.required_if' => 'Preencha o campo Atividades artísticas/culturais desenvolvidas por você (Informações Culturais)',
            'formacao_area_artistica_cultural.required_if' => 'Preencha o campo Formação na área Artística/Cultural (Informações Culturais)',
            'registro_profissional.required_if' => 'Preencha o campo Registro Profissional (Informações Culturais)',
            'registro_profissional_descricao.required_if' => 'Preencha o campo Quais registros profissionais você tem (Informações Culturais)',
            'espaco_cultural_coletivo.required' => 'Preencha o campo Você representa um espaço cultural/coletivo (Informações Culturais)',
            'numero_doc_espaco_cultural_coletivo.required_if' => 'Preencha o campo Número do DOC (Espaço Cultural Coletivo - Informações Culturais)',
            'tempo_atuacao_area.required_if' => 'Preencha o campo Tempo de atuação na área (Informações Culturais)',
            'alcance_pessoas.required_if' => 'Preencha o campo Quantas pessoas você alcança com o seu trabalho (Informações Culturais)',
            'frequencia_atividade.required_if' => 'Preencha o campo Com que frequência você realiza atividades artísticas (Informações Culturais)',
            'insercao_atividade_artistico_cultural.required_if' => 'Preencha o campo Formas de inserção da atividade artístico-cultural (Informações Culturais)',
            'fonte_renda.required_if' => 'Preencha o campo Vive exclusivamente da sua atividade artística ou possui outra fonte de renda (Informações Culturais)',
            'pandemia_covid.required_if' => 'Preencha o campo Executa alguma atividade cultural que foi interrompida devido a Covid-19 (Informações Culturais)',
            'recebe_auxilio_governo.required_if' => 'Preencha o campo Recebe algum auxílio do Governo (Informações Culturais)',
            'emprego_formal_ativo.required_if' => 'Preencha o campo Possui emprego formal ativo (Informações Culturais)',
            'redes_sociais_divulgacao.required_if' => 'Preencha o campo Informe suas redes sociais de divulgação (Informações Culturais)',
            'lei_emergencia_cultural.required_if' => 'Preencha o campo Caso haja interesse em prosseguir para o cadastro para recebimento dos benefícios previstos na Lei de Emergência Cultural Aldir Blanc (Informações Culturais)',
            'enquadramento_lei_emergencial.required_if' => 'Preencha o campo Declaro que li e tenho ciência que me enquadro em todas as condições acima (Informações Culturais)',
            'ciente_que_lei_nao_garante_beneficio.required_if' => 'Preencha o campo Declaro ter ciência que o preenchimento deste formulário NÃO me garante os benefícios previstos na Lei Nº 14.017/2020. (Informações Culturais)',
            'beneficio_previdenciario.required_if' => 'Preencha o campo Você é titular de benefício previdênciário ou assistencial ou beneficiário do seguro-desemprego ou de programa de transferência de renda federal ressalvado o Bolsa Família (Informações Culturais)',
            'nomes_beneficios_previdenciarios.required_if' => 'Preencha o campo Informe os beneficio(s) (Informações Culturais)',
            'beneficio_emergencial_lei_13982.required_if' => 'Preencha o campo Você é/foi beneficiário do auxílio emergêncial previsto pela Lei Nº 13.982, de 2 de abril de 2020 (Informações Culturais)',
            'mulher_familia_monoparental.required_if' => 'Preencha o campo A mulher provedora de família monoparental terá direito a 2(duas) cotas da renda emergêncial. Você se enquadra nesse requisito (Informações Culturais)',
            'banco_nome.required_if' => 'Preencha o campo Nome do banco (Informações Culturais)',
            'banco_tipo_conta.required_if' => 'Preencha o campo Tipo de conta (Informações Culturais)',
            'banco_agencia.required_if' => 'Preencha o campo Nome da agência (Informações Culturais)',
            'banco_numero_conta.required_if' => 'Preencha o campo Numero da conta (Informações Culturais)',
            'banco_conta_operacao.required_if' => 'Preencha o campo Conta Operação (Informações Culturais)',

            'confirmacao_de_informacoes.required' => 'Preencha o campo Declaro que são VERDADEIRAS e EXATAS todas as informações que foram prestadas neste formulário. Declaro ainda estar ciente de que declaração falsa no presente cadastro constituirá crime de falsidade ideológica (art. 299 do Código Penal) e estará sujeita a sanções penais sem prejuízo de medidas administrativas e outras'
        ];
    }
}
