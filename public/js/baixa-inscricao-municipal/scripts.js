let load = $(".ajax_load");

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

$("#inscricao_municipal").mask("000000000");
$("#inscricao_estadual").mask("00000000");
$("#iptu").mask("000000");

getDadosContribuinte();
function getDadosContribuinte() {
    $("#cpfOuCnpj_contribuinte").on("change", function () {
        let cpfOuCnpj = $(this);

        if (formatDocument(cpfOuCnpj.val()).length === 14) {
            if (!validarCNPJ(cpfOuCnpj.val())) {
                alert(`ATENÇÃO: O CNPJ ${cpfOuCnpj.val()} não é válido.`);
                limparCampos();
                cpfOuCnpj.val("").trigger("focus");
                return;
            }
        } else {
            if (!validarCPF(cpfOuCnpj.val())) {
                alert(`ATENÇÃO: O CPF ${cpfOuCnpj.val()} não é válido.`);
                limparCampos();
                cpfOuCnpj.val("").trigger("focus");
                return;
            }
        }

        $.ajax({
            url: "dados-contribuinte",
            method: "POST",
            data: { cpfOuCnpj: cpfOuCnpj.val() },
            dataType: "json",
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                load.fadeOut(200);

                if (response.solicitacaoJaEmProcessamento) {
                    alert(
                        "Atenção: Já existe uma solicitação em andamento para esse CPF/CNPJ."
                    );
                    cpfOuCnpj.val("").trigger("focus");
                    limparCampos();
                    return;
                }

                if (response) {
                    $("#razao_social_contribuinte").val(response.razao_social);
                    $("#inscricao_municipal").val(response.inscricao_municipal);
                    $("#telefone_contribuinte").val(response.telefone);
                    $("#celular_contribuinte").val(response.celular);
                    $("#email_contribuinte").val(response.email);
                    $("#inscricao_estadual").val(response.inscricao_estadual);
                    $("#iptu").val(response.iptu);
                } else {
                    limparCampos();
                }
            },
        });

        function limparCampos() {
            $("#razao_social_contribuinte").val('');
            $("#inscricao_municipal").val('');
            $("#telefone_contribuinte").val('');
            $("#celular_contribuinte").val('');
            $("#email_contribuinte").val('');
            $("#inscricao_estadual").val('');
            $("#iptu").val('');
        }
    });
}

carregarObservacaoAutonomo();
function carregarObservacaoAutonomo() {
    carregarObservacaoOnload();
    function carregarObservacaoOnload() {
        let tipoBaixaInscricao = $("select[name=tipo_baixa_inscricao]").val();

        if (tipoBaixaInscricao === "Autônomo") {
            $(".observacao_autonomo").removeClass("d-none");
        } else {
            $(".observacao_autonomo").addClass("d-none");
        }
    }

    carregarObservacaoOnChange();
    function carregarObservacaoOnChange() {
        $("select[name=tipo_baixa_inscricao]").on("change", function () {
            let tipoBaixaInscricao = $(this).val();

            if (tipoBaixaInscricao === "Autônomo") {
                $(".observacao_autonomo").removeClass("d-none");
            } else {
                $(".observacao_autonomo").addClass("d-none");
            }
        });
    }
}

carregarDocumentacaoPorTiposDeBaixaDeInscricao();
function carregarDocumentacaoPorTiposDeBaixaDeInscricao() {
    carregarDocumentosOnLoad();

    function carregarDocumentosOnLoad() {
        let documentacaoPorTipo = obterDocumentacaoPorTipo(
            $("select[name=tipo_baixa_inscricao]").val()
        );
        popularListaDocumentacaoObrigatoria(documentacaoPorTipo);
        popularDescricaoDocumento(documentacaoPorTipo);
    }

    carregarDocumentosOnChange();

    function carregarDocumentosOnChange() {
        $("select[name=tipo_baixa_inscricao]").on("change", function () {
            mostrarAgrupamentoDocumentacaoDoProcesso();

            let documentacaoPorTipo = obterDocumentacaoPorTipo($(this).val());
            popularListaDocumentacaoObrigatoria(documentacaoPorTipo);
            popularDescricaoDocumento(documentacaoPorTipo);
            removerDocumentosDaSessao();
        });

        function removerDocumentosDaSessao() {
            $.post(
                "remover-documentos-sessao-baixa-inscricao-municipal",
                function (response) {
                    $(".conteudo_documentacao").html(response);
                    recalcularDataTable("tabela_documentacao");
                }
            );
        }
    }
}

function obterDocumentacaoPorTipo(tipo) {
    let documentacao = [];

    switch (tipo) {
        case "Distrato Social":
            documentacao = [
                "1. CNPJ Baixada na receita",
                "2. Inscrição Estadual baixada (documento opcional)",
                "3. Representação Legal",
                "4. Documento de Identidade do Representante Legal",
                "5. CPF do Representante Legal",
            ];
            break;
        case "Transferência de Endereço":
            documentacao = [
                "1. Alteração contratual com transferência de endereço registrada",
                "2. CNPJ com endereço atualizado",
                "3. Inscrição Estadual com endereço atualizado (documento opcional)",
                "4. RG do Sócio (Incluir de todos os sócios)",
                "5. CPF do Sócio (Incluir de todos os sócios)"
            ];
            break;
        case "Autônomo":
            documentacao = [
                "1. Identidade",
                "2. Comprovante de Endereço",
                "3. CPF",
                "4. Taxa de Requerimento",
            ];
            break;
        case "MEI - Encerramento":
            documentacao = [
                "1. Certificado de MEI Baixado",
                "2. CNPJ Baixado",
                "3. RG do Titular",
                "4. CPF do Titular",
                "5. Representação Legal (documento opcional)",
                "6. CPF Representante Legal (documento opcional)",
                "7. RG Representante Legal (documento opcional)"
            ];
            break;
        case "MEI - Transferência de Endereço":
            documentacao = [
                "1. Certificado de MEI Atualizado",
                "2. CNPJ Atualizado",
                "3. RG do Titular",
                "4. CPF do Titular",
                "5. Representaçao Legal (documento opcional)",
                "6. CPF Representante Legal (documento opcional)",
                "7. RG Representante Legal (documento opcional)"
            ];
            break;
    }

    return documentacao;
}

function mostrarAgrupamentoDocumentacaoDoProcesso() {
    let tipo = $("select[name=tipo_baixa_inscricao]").val();

    if (tipo !== "") {
        $(".documentacao").removeClass("d-none");
        $(".enviar_informacoes").removeAttr("disabled", false);
    } else {
        $(".documentacao").addClass("d-none");
        $(".enviar_informacoes").attr("disabled", true);
    }
}

function popularListaDocumentacaoObrigatoria(documentacao) {
    let lista = document.querySelector(".lista_documentacao_obrigatoria");
    lista.innerHTML = "";

    documentacao.forEach((documento) => {
        let elementLi = document.createElement("li");
        elementLi.append(documento);
        lista.appendChild(elementLi);
    });
}

function popularDescricaoDocumento(documentacao) {
    let select = document.querySelector("select[name=descricao_documento]");

    select.innerHTML = "";
    select.append(document.createElement("option"));

    documentacao.forEach((documento) => {
        let elementOption = document.createElement("option");
        elementOption.append(documento);
        select.appendChild(elementOption);
    });
}

enviarDocumento();
function enviarDocumento() {
    $("body").on("click", ".btn-enviar-documento", function (e) {
        e.preventDefault();

        let descricao = $("select[name=descricao_documento]").val();
        let arquivo = $("input[name=documento]")[0].files[0];

        if (!descricao) {
            alert("Atenção: Selecione a descrição do documento.");
            $("select[name=descricao_documento]").focus();
            return false;
        }

        if (!validarAnexo(arquivo, ["jpg", "jpeg", "png", "pdf"], 5048000)) {
            return false;
        }

        const formData = new FormData();
        formData.append("arquivo", arquivo);
        formData.append("descricao", descricao);
        formData.append("servidor", "ftp");
        formData.append("pastaServidor", "baixa_inscricao_municipal");
        formData.append("nomeSessao", "documentacao_baixa_inscricao_municipal");
        formData.append(
            "caminhoTabela",
            "empreendedorismo.baixa_inscricao_municipal.tabela_documentacao"
        );

        if (descricao === '4. RG do Sócio (Incluir de todos os sócios)'
            || descricao === '5. CPF do Sócio (Incluir de todos os sócios)') {

            formData.append('verificarDescricaoArquivoJaIncluidoNaSessao', false);
        }

        $.ajax({
            url: "salvar-documento-baixa-inscricao-municipal",
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                $("input[name=documento]").val("");
                $("select[name=descricao_documento]").val("");

                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $(".conteudo_documentacao").html(response);
                recalcularDataTable("tabela_documentacao");
            },
        });
    });
}

removerDocumento();
function removerDocumento() {
    $("body").on("click", ".btn-remover-documento", function (e) {
        e.preventDefault();

        if (!confirm("Deseja realmente excluir?")) {
            return false;
        }

        let descricao = $(this).data("descricao");
        let caminho = $(this).data("caminho");

        $.ajax({
            url: "remover-documento-baixa-inscricao-municipal",
            method: "POST",
            data: {
                descricao,
                caminho,
                servidor: "ftp",
                pastaServidor: "baixa_inscricao_municipal",
                nomeSessao: "documentacao_baixa_inscricao_municipal",
                caminhoTabela:
                    "empreendedorismo.baixa_inscricao_municipal.tabela_documentacao",
            },
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $(".conteudo_documentacao").html(response);
                recalcularDataTable("tabela_documentacao");
            },
        });
    });
}

enviarInformacoes();
function enviarInformacoes() {
    $(".enviar_informacoes").on("click", function (e) {
        e.preventDefault();

        verificarDocumentosExigidos();
    });
}

function verificarDocumentosExigidos() {
    let documentacao = obterDocumentacaoPorTipo(
        $("select[name=tipo_baixa_inscricao]").val()
    );
    let documentosExigidos = removerDocumentoOpcional(documentacao);

    function removerDocumentoOpcional(documentacao) {
        let documentosExigidos = [];
        documentacao.forEach((documento) => {
            if (documento.indexOf("(documento opcional)") === -1) {
                documentosExigidos.push(documento);
            }
        });

        return documentosExigidos;
    }

    $.ajax({
        url: "verificar-documentos-baixa-inscricao-municipal",
        method: "POST",
        data: {
            nomeSessao: "documentacao_baixa_inscricao_municipal",
            documentosExigidos,
        },
        dataType: "json",
        success(response) {
            if (response.length > 0) {
                alert(
                    `Atenção: Os seguintes documentos são obrigatórios: ${response.join(", ")}.`
                );
                return false;
            }

            $("#formBaixaInscricaoMunicipal").trigger("submit");
        },
    });
}
