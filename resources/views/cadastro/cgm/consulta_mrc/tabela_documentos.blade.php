
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-bordered dataTable dt-responsive w-100" id="tabela_documentos_consulta_mrc">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Arquivo</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_consulta_mrc"))
                @foreach(session("documentos_consulta_mrc") as $documento)
                    <tr>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento" 
                                title="Remover Documento"
                                data-nome_arquivo="{{ $documento->nome_novo }}"
                                data-pasta="consulta_mrc"
                                data-nome_sessao="documentos_consulta_mrc"
                                data-id_tabela="tabela_documentos_consulta_mrc"
                                data-conteudo_documentos="conteudo_consulta_mrc_recurso">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>