<div class="form-row">
    <div class="col-lg-12 form-group">
        <div class="alert alert-info">
            <i class="fa fa-volume-up"></i> O Programa Fomenta Maricá MEI Emergencial é voltado para os Microempreendedores do Município de Maricá que desejam créditos em Moeda Mumbuca a juros 0 e carência de até 12 meses para início do pagamento. O programa integra o conjunto de medidas emergenciais adotadas pela cidade para combater as consequências do isolamento social causadas pelo coronavírus.
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-lg-7 form-group">
        <label for="motivoSolicitacao">Por qual motivo está solicitando o empréstimo através do programa Fomenta Maricá?</label>

        <select id="motivoSolicitacao" class="form-control form-control-sm {{ ($errors->has('motivoSolicitacao') ? 'is-invalid' : '') }}" name="motivoSolicitacao">
                <option value=""></option>
                <option {{ old('motivoSolicitacao')=="Para ampliar meu estabelecimento"?"selected":"" }}>Para ampliar meu estabelecimento</option>
                <option {{ old('motivoSolicitacao')=="Para investir em produtos ligados a minha atividade"?"selected":"" }}>Para investir em produtos ligados a minha atividade</option>
                <option {{ old('motivoSolicitacao')=="Para investir em cursos ligados a minha atividade"?"selected":"" }}>Para investir em cursos ligados a minha atividade</option>
                <option {{ old('motivoSolicitacao')=="Para investir em serviços ligados a minha atividade"?"selected":"" }}>Para investir em serviços ligados a minha atividade</option>
                <option {{ old('motivoSolicitacao')=="Para aumentar meu capital de giro"?"selected":"" }}>Para aumentar meu capital de giro</option>
        </select> 
        @if ($errors->has('motivoSolicitacao'))
            <div class="invalid-feedback">
                {{ $errors->first('motivoSolicitacao') }}
            </div>
        @endif

    </div>
    <div class="col-lg-5 form-group">
        <label for="valorSolicitado">De quanto você precisa? (de $ 300,00 a $ 5.000,00)</label>
        <input type="text" name="valorSolicitado" id="valorSolicitado" 
            class="form-control form-control-sm {{ ($errors->has('valorSolicitado') ? 'is-invalid' : '') }}" 
            value="{{ old('valorSolicitado') }}" maxlength="100" autofocus/>

            @if ($errors->has('valorSolicitado'))
                <div class="invalid-feedback">
                    {{ $errors->first('valorSolicitado') }}
                </div>
            @endif
    </div>
</div>
<div class="form-row">
    <div class="col-lg-3 form-group">
        <label for="tempoCarencia">Carência (de 0 a 12 meses):</label>
        <input type="text" name="tempoCarencia" id="tempoCarencia" 
            class="form-control form-control-sm {{ ($errors->has('tempoCarencia') ? 'is-invalid' : '') }}"
            value="{{ old('tempoCarencia') }}" maxlength="100" />

            @if ($errors->has('tempoCarencia'))
                <div class="invalid-feedback">
                    {{ $errors->first('tempoCarencia') }}
                </div>
            @endif
    </div>

    <div class="col-lg-3 form-group">
        <label for="prazo">Prazo (até 18 meses):</label>
        <input type="text" name="prazo" id="prazo" 
            class="form-control form-control-sm mask-cel {{ ($errors->has('prazo') ? 'is-invalid' : '') }}" 
            value="{{ old('prazo') }}" />

            @if ($errors->has('prazo'))
                <div class="invalid-feedback">
                    {{ $errors->first('prazo') }}
                </div>
            @endif
    </div>

    <div class="col-lg-6 form-group">
        <label for="valorParcela">Qual é a parcela em que você se sente confortável para pagar mensalmente?</label>
        <input type="text" name="valorParcela" id="valorParcela" 
            class="form-control form-control-sm mask-tel {{ ($errors->has('valorParcela') ? 'is-invalid' : '') }}" 
            value="{{ old('valorParcela') }}" />

            @if ($errors->has('valorParcela'))
                <div class="invalid-feedback">
                    {{ $errors->first('valorParcela') }}
                </div>
            @endif
    </div>

</div> 
