
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela_documentacao_defesa_previa_autuacao">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Documento</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentacao_defesa_previa_autuacao"))
                @foreach(session("documentacao_defesa_previa_autuacao") as $dados)
                    <tr>
                        <td>{{ $dados->descricao }}</td>
                        <td>{{ $dados->arquivo }}</td>
                        <td class="text-center" width="10%">
                            <button
                                class="btn btn-sm btn-danger btn-remover-documento"
                                data-descricao="{{ $dados->descricao }}"
                                data-caminho="{{ $dados->caminho }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
