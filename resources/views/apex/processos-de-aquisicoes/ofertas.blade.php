<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive" style="padding: 4px !important; margin-bottom: 8px;">
            <table class="table table-striped table-bordered dataTable dt-responsive w-100"
                data-ordering="false" id="tabela-ofertas">

                <thead>
                    <tr>
                        <th>{{__('Ordem')}}</th>
                        <th>{{__('Fornecedor')}}</th>
                        <th>{{__('Valor Total')}}</th>
                        <th>{{__('Data')}}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($ofertas as $ordem => $oferta)
                        <tr>
                            <td>{{ $ordem + 1 }}</td>
                            <td>{{ $oferta->razao_social }}</td>
                            <td>{{ format_currency_brl($oferta->valor_total) }}</td>
                            <td>{{ date('d/m/Y H:i:s', strtotime($oferta->data)) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
