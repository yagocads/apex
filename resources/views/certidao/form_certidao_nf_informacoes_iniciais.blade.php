<div class="row">
    <?php if ($titulo === 'Cancelamento de Nota Fiscal'): ?>
        <div class="col-lg-12">
            <div class="alert alert-info">
                Se emissão de nota fiscal física/avulsa, é necessário o comparecimento na casa do Empreendedor
                para abertura do processo.
            </div>
        </div>
    <?php endif; ?>

    <div class="col-lg-12 form-group">
        <label for="tipo_prestador">1 - Selecione o tipo de Prestador:</label>
        <select name="tipo_prestador" id="tipo_prestador" class="form-control form-control-sm col-lg-3">
            <option></option>

            <?php if ($titulo === 'Cancelamento de Nota Fiscal'): ?>
                <option>MEI</option>
                <option>Outros</option>
            <?php else: ?>
                <option>PF</option>
                <option>MEI</option>
                <option>Outras Constituições</option>
            <?php endif; ?>

        </select>
    </div>

    <?php if ($titulo === 'Cancelamento de Nota Fiscal'): ?>
        <div class="col-lg-12 form-group">
            <label for="tipo_tomador">2 - Selecione o tipo de Tomador:</label>
            <select name="tipo_tomador" id="tipo_tomador" class="form-control form-control-sm col-lg-3">
                <option></option>
                <option>PF</option>
                <option>MEI</option>
                <option>Ente Público</option>
                <option>Outras Instituições</option>
                <option>Não Declarado</option>
            </select>
        </div>

        <div class="col-lg-12 form-group">
            <label for="assinatura_carta_anuencia">3 - Quem está assinando a carta de anuência? (Não se aplica para "Não Declarado"):</label>
            <select name="assinatura_carta_anuencia" id="assinatura_carta_anuencia" class="form-control form-control-sm col-lg-3">
                <option></option>
                <option>Tomador</option>
                <option>Procurador</option>
            </select>
        </div>

        <div class="col-lg-12 form-group">
            <label for="justificativa_cancelamento">4 - Justificativa de Cancelamento:</label>
            <select name="justificativa_cancelamento" id="justificativa_cancelamento" class="form-control form-control-sm col-lg-3">
                <option></option>
                <option>Erro de Preenchimento</option>
                <option>Serviço não foi prestado</option>
                <option>Ainda se foi emitida em duplicidade</option>
            </select>
        </div>

        <div class="col-lg-12 form-group pergunta_emissao_guia_pagamento d-none">
            <label for="emissao_guia_pagamento">4.1 - Já emitiu a guia de pagamento?</label>
            <select name="emissao_guia_pagamento" id="emissao_guia_pagamento" class="form-control form-control-sm col-lg-3">
                <option></option>
                <option>Sim</option>
                <option>Não</option>
            </select>
        </div>

        <div class="col-lg-12 form-group pergunta_competencia_nota d-none">
            <label for="competencia_nota">4.2 - Qual a competência da nota?</label>
            <select name="competencia_nota" id="competencia_nota" class="form-control form-control-sm col-lg-5">
                <option></option>
                <option>Solicitação do cancelamento até dia 10 do mês seguinte</option>
                <option>Solicitação do cancelamento após dia 10 do mês seguinte</option>
            </select>
        </div>

        <div class="col-lg-12 d-none aviso_nota_legal">
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Aviso: Acessar o Nota Legal e substituir a nota preenchida incorretamente.
            </div>
        </div>
    <?php endif; ?>
</div>
