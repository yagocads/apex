@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">

        @if (session('success'))
            @include('confirmacao.confirmacao_processo')
        @else
            <div class="col-lg-12">
                <h3 class="mb-3">
                    <i class="fa fa-home"></i> Solicitação de Defesa Prévia de Autuação
                </h3>

                @if (!empty(session('solicitacaoJaEmProcessamento')))
                    <div class="alert alert-danger">
                        {{ session('solicitacaoJaEmProcessamento') }}
                    </div>
                @endif

                <form method="POST" id="formDefesaPreviaAutuacao"
                    action="{{ route('defesa-previa-autuacao') }}" autocomplete="off">

                    @csrf

                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <span class="d-block">* {{ $error }}</span>
                            @endforeach
                        </div>
                    @endif

                    <div id="accordion">

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados-requerente">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                                </div>
                            </a>
                            <div id="dados-requerente" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cidadao.transito.defesa_previa_autuacao.dados_requerente")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#solicitacao-defesa-previa">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados da Solicitação de Defesa Prévia
                                </div>
                            </a>
                            <div id="solicitacao-defesa-previa" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cidadao.transito.defesa_previa_autuacao.solicitacao_defesa_previa")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3 documentacao">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#documentacao">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Documentação do Processo
                                </div>
                            </a>
                            <div id="documentacao" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cidadao.transito.defesa_previa_autuacao.documentacao_processo")
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-info">
                                <p>
                                    Declaro que são VERDADEIRAS e EXATAS todas as informações que foram
                                    prestadas neste formulário. Declaro ainda estar ciente de que declaração
                                    falsa no presente cadastro constituirá crime de
                                    falsidade ideológica (art. 299 do Código Penal) e estará sujeita a sanções penais
                                    sem prejuízo de medidas administrativas e outras. <span class="text-danger">*</span>
                                </p>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="confirmacao_de_informacoes"
                                            class="form-check-input" value="sim" {{ old('confirmacao_de_informacoes') ? 'checked' : '' }} required>
                                        Li e concordo.
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'transito']) }}" class="btn btn-primary mt-2">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>

                            <button class="btn btn-success mt-2 enviar_informacoes">Enviar Informações <i class="fa fa-send"></i></button>
                        </div>
                    </div>

                </form>
            </div>
        @endif
    </div>
</div>

@endsection

@section('post-script')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/handleArquivos/scripts.js') }}"></script>
    <script src="{{ asset('js/defesa-previa-autuacao/scripts.js') }}"></script>
@endsection
