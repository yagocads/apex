<div class="row">
    <div class="col-lg-12">
        @if (count($notificacoes) > 0)
            <ul class="list-group">
                @foreach($notificacoes as $notificacao)
                    <li class="list-group-item">
                        <span>
                            {{__('Número do Processo')}}: <h5 class="d-inline"><span class="badge badge-secondary">{{ $notificacao->processo_lecom }}</span></h5>
                            <br />
                            {{__('Resultado')}}: {{ $notificacao->resultado }}
                            <br />
                            {{__('Motivo')}}: {{ $notificacao->motivo }}
                        </span>

                        <h5 class="mt-1"><span class="badge badge-primary p-1 marcar-como-lida" data-processo="{{ $notificacao->processo_lecom }}" style="cursor: pointer"><i class="fa fa-check"></i> {{__('Marcar como lida')}}</span></h5>
                    </li>
                @endforeach
            </ul>
        @else
            <p>{{__('Não há notificações')}}.</p>
        @endif
    </div>
</div>
