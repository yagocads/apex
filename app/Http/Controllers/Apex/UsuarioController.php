<?php

namespace App\Http\Controllers\Apex;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('apex.cadastro-usuario.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->only(
            'nome',
            'telefone',
            'email',
            'cpfUsuario',
            'senha'
        ), $this->rules(), $this->messages());

        if ($validate->fails()) {
            return redirect()
                ->route('cadastro-usuario')
                ->withInput()
                ->withErrors($validate);
        }

        DB::table('users')->insert([
            'name' => $request->nome,
            'telefone' => $request->telefone,
            'email' => $request->email,
            'cpf' => $request->cpfUsuario,
            'password' => bcrypt($request->senha)
        ]);

        Log::criarLog($request->cpfUsuario, 'Usuário com CPF: ' . $request->cpfUsuario . ' cadastrado com sucesso');

        return redirect()
            ->route('login')
            ->with('usuarioCadastradoComSucesso', 'Usuário cadastrado com sucesso!')
            ->with('cpf', $request->cpfUsuario);
    }

    /**
     * Validation rules
     * @return array
     */
    private function rules()
    {
        return [
            'nome' => 'required',
            'telefone' => 'required',
            'email' => 'required|unique:users,email',
            'cpfUsuario' => 'required|unique:users,cpf',
            'senha' => 'required'
        ];
    }

    /**
     * Validation rules messages
     * @return array
     */
    private function messages()
    {
        return [
            'nome.required' => 'O campo nome é obrigatório',
            'telefone.required' => 'O campo telefone é obrigatório',
            'email.required' => 'O campo email é obrigatório',
            'cpfUsuario.required' => 'O campo CPF é obrigatório',
            'senha.required' => 'O campo senha é obrigatório',
            'email.unique' => 'O campo E-mail já está sendo utilizado',
            'cpfUsuario.unique' => 'O campo CPF já está sendo utilizado'
        ];
    }
}
