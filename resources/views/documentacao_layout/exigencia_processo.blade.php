<div class="card">
    <div class="card-header">
        <h5>Exigência</h5>
    </div>

    <div class="card-body">
        <p>Para visualizar a tela de exigências, <a href="{{ route('documentacao-layout-exigencia') }}" target="_blank"><i class="fa fa-link"></i> clique aqui</a>.</p>
    </div>
</div>
