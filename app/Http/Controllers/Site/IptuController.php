<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Notice;
use App\Models\Contact;
use App\Models\Log;
use App\Mail\FaleConosco;
use App\Mail\ResetPassword;
use App\Support\FinanceiroSupport;
use App\Support\ImovelSupport;
use App\User;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\DB;

class IptuController extends Controller
{

    public function __construct()
    {
        $this->financial = new FinanceiroSupport;
        $this->cpf = "042.671.914-09";

        // Definição das contantes para acesso à API e-cidade
        if (!defined('API_GFD'))   define('API_GFD',  '/integracaoGeralFinanceiraDebitos.RPC.php');
        if (!defined('API_URL_GFD'))   define('API_URL_GFD',  API_URL);
    }

    public function index()
    {

        return view('site.welcome', ['pagina' => "Principal"]);
    }

    public function cadastroIptuIdoso()
    {

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'cpf_ou_cnpj',
            'no_debug',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('iptu.cadastro.identificacao', ['pagina' => "Principal", 'showRecaptcha' => $showRecaptcha, 'idoso' => 1]);
    }

    public function cadastroIptu()
    {

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'cpf_ou_cnpj',
            'no_debug',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('iptu.cadastro.identificacao', ['pagina' => "Principal", 'showRecaptcha' => $showRecaptcha, 'idoso' => 0]);
    }

    public function consultaIptu()
    {

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'data_solicitacao',
            'no_debug',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('iptu.cadastro.consulta', ['pagina' => "Principal", 'showRecaptcha' => $showRecaptcha]);
    }

    public function consultarIptu(Request $request)
    {

        $chamadoIptu = DB::connection('integracao')
            ->table('lecom_iptu')
            ->where('cpf', '=', $request->cpf_ou_cnpj)
            ->where('protocolo', '=', $request->protocolo)
            ->where(DB::raw('DATE(data_cadastro)'), '=', $request->data_solicitacao)
            ->first();

        if ($chamadoIptu) {

            $registros = DB::connection('lecom')
                ->table("v_consulta_iptu_imovel")
                ->where('cpf', '=', $request->cpf_ou_cnpj)
                ->where('Chamado', '=', $chamadoIptu->chamado_lecom)
                ->get();

            if (count($registros) > 0) {
                $etapas = $this->getEtapas($registros[0]->COD_FORM);
                $data = $this->getTermoAceite($request->cpf_ou_cnpj);

                return view('iptu.cadastro.acompanhamento_iptu', ['processos' => $registros, 'etapas' => $etapas]);
            } else {
                return view('messages.sucesso', [
                    'titulo' => 'Cadastro de IPTU', 
                    'icone' => 'fa-home', 
                    "mensagem" => '<p><strong>SOLICITAÇÃO REGISTRADA</strong> </p>
                <p>A sua solcitação foi registrada e está sendo processada.</p>
                <p>Retorne em breve para verificar o andamento da sua solicitação.</p>
                ' 
                ]);
                // return view('errors.erro_geral', [
                //     "icone" => "fa-home",
                //     "titulo" => "Cadastro de IPTU",
                //     "mensagem" => '<p>Não encontramos o chamado com os dados informados.<br>Por favor entre em contato com o SIM.',
                //     "rota" => "consulta_iptu",
                //     "retorno" => "",
                //     "codigo" => "IPTU_006",
                // ]);
            }
        } else {
            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Cadastro de IPTU",
                "mensagem" => '<p>Não encontramos nenhuma solicitação com os dados informados.<br>Por favor confirme os dados e tente novamente.',
                "rota" => "consulta_iptu",
                "retorno" => "",
                "codigo" => "IPTU_005",
            ]);
        }
    }

    public function getEtapas($codForm)
    {
        $items = DB::connection('lecom')
            ->table('v_fases_processos')
            ->where('COD_FORM', '=', $codForm)
            ->orderBy('POSICAO')
            ->get();
        $values = collect($items->all());

        $result = array();

        foreach ($values as $key => $data) {

            $id = $data->NOME;
            if (isset($result[$id])) {
                $result[$id][] = $data->COD_ETAPA;
            } else {
                $result[$id] = array($data->COD_ETAPA);
            }
        }

        return $result;
    }

    public function getTermoAceite($documento)
    {
        return DB::connection('integracao')
            ->table('lecom_da_termo_aceite')
            ->where('cpf_cnpj', '=', $documento)
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function sanarPendenciasIptu($chamado, $ciclo, $etapa)
    {

        $pendenciaIptu = DB::connection('integracao')
            ->table('lecom_iptu_exigencias')
            ->where('processo', '=', $chamado)
            ->where('ciclo', '=', $ciclo)
            ->where('fase', '=', $etapa)
            ->first();

        if (!$pendenciaIptu) {

            $processoIptu = DB::connection('lecom')
                ->table('v_consulta_iptu_imovel')
                ->where('Chamado', '=', $chamado)
                ->first();

            return view('iptu.cadastro.iptu_pendencias', ['processo' => $processoIptu]);
        } else {
            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Cadastro IPTU",
                "mensagem" => '<p>Já foi feita uma interação para esta solicitação.  Aguarde o próximo ciclo',
                "rota" => "cadastro_iptu",
                "retorno" => "",
                "codigo" => "IPTU_007",
            ]);
        }
    }

    public function verificarDocumentosPendenciasIptu()
    {

        $session = session()->get("documentos_dados_pendencias");

        if (!empty($session)) {
            return json_encode(["documentosEnviados" => true]);
        } else {
            return json_encode(["documentosEnviados" => false]);
        }
    }

    public function validaCpfCnpjIptu(Request $request)
    {

        return view('iptu.cadastro.cadastro', ['pagina' => "Principal", 'request' => $request, 'estados' => getEstados(),]);
    }

    public function formPendenciasIptuSalvar(Request $request)
    {

        DB::beginTransaction();

        try {

            $chamadoIptu = DB::connection('integracao')
                ->table('lecom_iptu')
                ->where('cpf', '=', $request->cpf_ou_cnpj)
                ->where('chamado_lecom', '=', $request->chamado)
                ->first();

            $idpendenciaIptu = DB::connection('integracao')
                ->table('lecom_iptu_exigencias')
                ->insertGetId(
                    [
                        'id_iptu'   => $chamadoIptu->id,
                        'cpf_cnpj'  => $request->cpf_ou_cnpj,
                        'fase'      => $request->fase,
                        'ciclo'     => $request->ciclo,
                        'processo'  => $request->chamado,
                        'recurso'   => $request->justificativa_recurso,
                        'data'      => date("Y-m-d H:i:s"),
                    ]
                );

            $this->enviarDocumentosRecursoParaLecom(
                $request->cpf_ou_cnpj,
                $idpendenciaIptu,
                $request->chamado
            );

            DB::commit();

            $this->removerDocumentosPendenciaSessao();

            $rota = "consulta_iptu";
            $mensagem = '<p><b>ATENÇÃO:</b> As suas informações foram enviadas com sucesso.</p>';
            $mensagem .= '<p>Aguarde a análise do processo.</p>';
            $mensagem .= '<p><a href="' . route($rota) . '">Clique aqui para consultar o processo</a></p>';

            return view('messages.confirmacao', [
                "icone" => "fa-home",
                "titulo" => "Cadastro IPTU",
                "mensagem" => $mensagem,
                "rota" => "consulta_iptu",
                "retorno" => "",
            ]);
        } catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => $request->cpf_ou_cnpj,
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "IptuController",
                    'rotina'    => "formPendenciasIptuSalvar",
                    'erro'      => $th,
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Cadastro IPTU",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "cadastro_iptu",
                "retorno" => "",
                "codigo" => "IPTU_008",
            ]);
        }
    }

    public function salvarIptu(Request $request)
    {
        // dd($request);

        DB::beginTransaction();

        try {
            $arrCnpj = explode("-", $request->cpf_ou_cnpj);
            $protocolo = $arrCnpj[1] . str_pad(rand(), 6, '0', STR_PAD_LEFT) . date('d');

            $idIptu = DB::connection('integracao')
                ->table('lecom_iptu')
                ->insertGetId(
                    array_merge(
                        $this->dadosRequerente($request, $protocolo),
                        $this->dadosEmpresa($request),
                        $this->dadosContato($request),
                        $this->dadosEndereco($request)
                    )
                );

            $this->enviarDocumentosParaLecom(
                $request->cpf_ou_cnpj,
                $idIptu
            );

            $this->enviarImoveisParaLecom(
                $request->cpf_ou_cnpj,
                $idIptu
            );

            DB::commit();

            $this->removerDocumentosSessao();

            $rota = "consulta_iptu";
            $mensagem  = '<p><b>Identificação:</b> ' . $request->nome_solicitante . '</p>';
            $mensagem .= '<p><b>CPF/CNPJ:</b> ' . $request->cpf_ou_cnpj . '</p>';
            $mensagem .= '<p><b>Protocolo:</b> ' . $protocolo . '</p>';
            $mensagem .= '<p><b>Data:</b> ' . date('d/m/Y') . '</p>';
            $mensagem .= '<p><b>O prazo de envio do IPTU para o e-mail cadastrado é de 3 dias úteis.</b></p>';
            $mensagem .= '<p><b>ATENÇÃO:</b> Para consultar o andamento do processo, acesse a página de consulta no link abaixo.</p>';
            $mensagem .= '<p><a href="' . route($rota) . '">Clique aqui para consultar o processo</a></p>';

            return view('messages.confirmacao', [
                "icone" => "fa-home",
                "titulo" => "Solicitação de IPTU",
                "mensagem" => $mensagem,
                "rota" => "cadastro_iptu",
                "retorno" => "",
            ]);
        } catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => $request->cpf_ou_cnpj,
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "IptuController",
                    'rotina'    => "salvarIptu",
                    'erro'      => $th,
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Cadastro IPTU",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "cadastro_iptu",
                "retorno" => "",
                "codigo" => "IPTU_002",
            ]);
        }
    }

    private function dadosRequerente(Request $request, $protocolo)
    {
        return [
            'idoso' => $request->idoso,
            'cpf' => $request->cpf_ou_cnpj,
            'resp_preenchimento' => $request->responsavel,
            'req_nome' => $request->nome_requerente,
            'req_id' => $request->identidade_requerente,
            'req_id_orgao' => $request->orgao_emissor_requerente,
            'req_id_dataEmissao' => $request->data_emissao_requerente,
            'req_cpf' => $request->cpf_requerente,

            'protocolo' => $protocolo,
            'data_cadastro' => date("Y-m-d H:i:s"),
        ];
    }

    private function dadosEmpresa(Request $request)
    {
        return [
            'cnpj' => $request->cnpj,
            'razao_social' => $request->razao_social,
            'nome_fantasia' => $request->nome_fantasia,
            'natureza_juridica' => $request->natureza_juridica,
            'data_abertura' => $request->data_abertura_empresa,
            'inscricao_estadual' => $request->inscricao_estadual,
            'cresci' => $request->cresci,
        ];
    }

    private function dadosContato(Request $request)
    {
        return [
            'telefone' => $request->telefone,
            'celular' => $request->celular,
            'email' => $request->email,
        ];
    }

    private function dadosEndereco(Request $request)
    {
        return [
            'cep' => $request->cep,
            'uf' => $request->estado,
            'cidade' => $request->municipio,
            'bairro' => $request->bairro,
            'endereco' => $request->endereco,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
        ];
    }

    private function enviarDocumentosParaLecom($cpf, $idIptu)
    {
        $sessionDocumentos = [
            'documento_endereco',
            'documentos_requerente',
        ];

        foreach ($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach (session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_iptu_documentos')
                        ->insert([
                            'id_iptu' => $idIptu,
                            'cpf' => $cpf,
                            'documento' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    private function enviarDocumentosRecursoParaLecom($cpf, $idIptu, $processo)
    {
        $sessionDocumentos = [
            'documentos_dados_pendencias',
        ];

        foreach ($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach (session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_iptu_exigencias_arquivos')
                        ->insert([
                            'id_exigencia' => $idIptu,
                            'cpf_cnpj' => $cpf,
                            'processo' => $processo,
                            'nome_arquivo' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    private function enviarImoveisParaLecom($cpf, $idIptu)
    {
        $sessionDocumentos = [
            'documentos_imovel',
        ];

        foreach ($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach (session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_iptu_imoveis')
                        ->insert([
                            'id_iptu' => $idIptu,
                            'cpf' => $cpf,
                            'matricula_imovel' => $dados->matricula_imovel,
                            'forma_pagamento' => $dados->forma_pagamento,
                            'cep' => $dados->cep,
                            'uf' => $dados->estado,
                            'cidade' => $dados->municipio,
                            'bairro' => $dados->bairro,
                            'endereco' => $dados->endereco,
                            'numero' => $dados->numero,
                            'complemento' => $dados->complemento,
                            'nome_loteamento' => $dados->nome_loteamento,
                            'quadra' => $dados->quadra,
                            'lote' => $dados->lote,
                            'ponto_referencia' => $dados->referencia,
                            'descricao_documento' => $dados->descricao,
                            'documento' => $dados->nome_novo
                        ]);
                }
            }
        }
    }
    private function removerDocumentosPendenciaSessao()
    {
        session()->forget("documentos_dados_pendencias");
    }

    private function removerDocumentosSessao()
    {
        session()->forget("documentos_imovel");
        session()->forget("documento_endereco");
        session()->forget("documentos_requerente");
    }



    public function salvarDocumentoServidor(Request $request)
    {

        if ($request->nome_sessao == "documentos_imovel" && $request->possui_matricula == "SIM") {
            $matriculaCadastrada = DB::connection('integracao')
                ->table('lecom_iptu_imoveis')
                ->where('matricula_imovel', '=', $request->matricula_imovel)
                ->count();

            if ($matriculaCadastrada > 0) {
                return ['error' => 'O número de matrícula desse imóvel já foi solicitado.'];
            }
        }


        if ($request->arquivo != "undefined") {
            $nome_original = $request->arquivo->getClientOriginalName();
            $extensao = $request->arquivo->extension();

            $nome_arquivo = remove_character_document($request->cpf_ou_cnpj) . "_" .  $request->descricao . "_" . uniqid() . "." . $extensao;
            $nome_arquivo = remove_accentuation(str_replace(" ", "_", $nome_arquivo));

            $upload = $request->arquivo->storeAs('cadastro_iptu', $nome_arquivo, 'ftp');

            if (!$upload) {
                return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
            }
        }

        session()->push($request->nome_sessao, (object) [
            "id" => uniqid(),
            "matricula_imovel" =>  $request->matricula_imovel != "undefined" ? $request->matricula_imovel : "",
            "possui_matricula" => $request->possui_matricula,
            "forma_pagamento" =>  $request->forma_pagamento != "undefined" ? $request->forma_pagamento : "",
            "cep" =>  $request->cep != "undefined" ? $request->cep : "",
            "endereco" =>  $request->endereco != "undefined" ? $request->endereco : "",
            "numero" =>  $request->numero != "undefined" ? $request->numero : "",
            "complemento" =>  $request->complemento != "undefined" ? $request->complemento : "",
            "bairro" =>  $request->bairro != "undefined" ? $request->bairro : "",
            "municipio" =>  $request->municipio != "undefined" ? $request->municipio : "",
            "estado" =>  $request->estado != "undefined" ? $request->estado : "",
            "referencia" =>  $request->referencia != "undefined" ? $request->referencia : "",
            "nome_loteamento" =>  $request->nome_loteamento != "undefined" ? $request->nome_loteamento : "",
            "quadra" =>  $request->quadra != "undefined" ? $request->quadra : "",
            "lote" =>  $request->lote != "undefined" ? $request->lote : "",
            "ponto_referencia" =>  $request->ponto_referencia != "undefined" ? $request->ponto_referencia : "",

            "nome_original" => $request->arquivo != "undefined" ? $request->arquivo->getClientOriginalName() : "",
            "nome_novo" => $request->arquivo != "undefined" ? $nome_arquivo : "",
            "descricao" => $request->descricao,
        ]);

        if (empty($request->nome_tabela)) {
            $nome_tabela = "tabela_documentos";
        } else {
            $nome_tabela = $request->nome_tabela;
        }

        return view('iptu.cadastro.' . $request->pasta . '.' . $nome_tabela);
    }

    public function salvarDocumentoPendenciaIptu(Request $request)
    {

        if ($request->arquivo != "undefined") {
            $nome_original = $request->arquivo->getClientOriginalName();
            $extensao = $request->arquivo->extension();

            $nome_arquivo = remove_character_document($request->cpf_ou_cnpj) . "_" .  $request->descricao . "_" . uniqid() . "." . $extensao;
            $nome_arquivo = remove_accentuation(str_replace(" ", "_", $nome_arquivo));

            $upload = $request->arquivo->storeAs('cadastro_iptu_exigencias', $nome_arquivo, 'ftp');

            if (!$upload) {
                return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
            }
        }

        session()->push($request->nome_sessao, (object) [

            "nome_original" => $request->arquivo != "undefined" ? $request->arquivo->getClientOriginalName() : "",
            "nome_novo" => $request->arquivo != "undefined" ? $nome_arquivo : "",
            "descricao" => $request->descricao,
        ]);

        if (empty($request->nome_tabela)) {
            $nome_tabela = "tabela_documentos";
        } else {
            $nome_tabela = $request->nome_tabela;
        }

        return view('iptu.cadastro.' . $nome_tabela);
    }

    public function removerDocumentoPendencia(Request $request)
    {

        if ($request->nome_arquivo != "") {
            if (!$this->removerDocumentoPendenciaServidor($request->nome_arquivo)) {
                return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
            }
        }

        $session = session()->get($request->nome_sessao);

        if (!empty($session)) {

            foreach ($session as $key => $dados) {

                if (in_array($request->nome_arquivo, (array) $dados)) {
                    unset($session[$key]);
                    session()->forget($request->nome_sessao);
                    session()->put($request->nome_sessao, $session);
                }
            }
        }

        if (empty($request->nome_tabela)) {
            $nome_tabela = "tabela_documentos";
        } else {
            $nome_tabela = $request->nome_tabela;
        }

        return view('iptu.cadastro.' . $nome_tabela);
    }

    private function removerDocumentoPendenciaServidor(string $arquivo): bool
    {
        return Storage::disk('ftp')->delete('cadastro_iptu_exigencias/' . $arquivo);
    }

    public function removerDocumentoSessao(Request $request)
    {

        if ($request->nome_arquivo != "") {
            if (!$this->removerDocumentoServidor($request->nome_arquivo)) {
                return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
            }
        }

        $session = session()->get($request->nome_sessao);

        if (!empty($session)) {

            foreach ($session as $key => $dados) {

                if (in_array($request->id, (array) $dados)) {
                    unset($session[$key]);
                    session()->forget($request->nome_sessao);
                    session()->put($request->nome_sessao, $session);
                }
            }
        }

        if (empty($request->nome_tabela)) {
            $nome_tabela = "tabela_documentos";
        } else {
            $nome_tabela = $request->nome_tabela;
        }

        return view('iptu.cadastro.' . $request->pasta . '.' . $nome_tabela);
    }

    private function removerDocumentoServidor(string $arquivo): bool
    {
        return Storage::disk('ftp')->delete('cadastro_iptu/' . $arquivo);
    }










    public function emitirIptu()
    {

        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'matricula',
            'no_debug',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('iptu.matricula_imovel', ['pagina' => "Principal", 'showRecaptcha' => $showRecaptcha]);
    }

    public function pesquisaMatriculaImovel2()
    {
        $info = [
            "tipo_pesquisa" => "m",
            "cpf" => "04.267.191-409",
            "ro_cpf" => "042.671.914-09",
            "nome" => "FERNANDO DA SILVA PEREIRA",
            "ro_nome" => "FERNANDO DA SILVA PEREIRA",
            "matricula" => "75490",
            "tipo_debito" => "86"
        ];

        $this->relatorioDebitosAbertos((object) $info);
    }

    public function pesquisaMatriculaImovel(Request $request)
    {
        if ($request->matricula != "") {
            $matricula_inscricao = $request->matricula;
            $request->tipo_pesquisa = 'm';
        }

        $imovelSupport = new ImovelSupport();
        $registros = (object) $imovelSupport->getDebitosImovel($request->matricula);

        if ($registros->iStatus == 2) {

            $apiErros = DB::connection('mysql')
                ->table('erros_api')
                ->select('mensagem', 'codigo_portal')
                ->where('codigo', '=', $registros->eErro)
                ->first();

            if ($apiErros) {
                $mensagem_erro = [
                    'codigo' => $apiErros->codigo_portal,
                    'mensagem' => $apiErros->mensagem
                ];
            } else {
                $mensagem_erro = [
                    'codigo' => 'API_0001',
                    'mensagem' => "Erro na emissão dos boletos para o IPTU. Favor entrar com contato com o SIM!"
                ];
            }

            return view('errors.erro_default', [
                'titulo' => 'Emissão do IPTU',
                'mensagem' => $mensagem_erro['mensagem'],
                'codigo' => $mensagem_erro['codigo']
            ]);
        }

        $request->tipo_debito = 86;

        // $registros = $this->integracaoPesquisaDebitos($request);

        // dd($registros);

        if (isset($registros->aDebitosNormaisEncontrados) && isset($registros->aDebitosUnicosEncontrados)) {

            if (is_array($registros->aDebitosNormaisEncontrados) && is_array($registros->aDebitosUnicosEncontrados)) {
                return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "registros" => $registros->aDebitosNormaisEncontrados, "registros_unicos" => $registros->aDebitosUnicosEncontrados, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
            } else {
                if (!is_array($registros->aDebitosNormaisEncontrados) && is_array($registros->aDebitosUnicosEncontrados)) {
                    return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "registros_unicos" => $registros->aDebitosUnicosEncontrados, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
                }
                if (is_array($registros->aDebitosNormaisEncontrados) && !is_array($registros->aDebitosUnicosEncontrados)) {
                    return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "registros" => $registros->aDebitosNormaisEncontrados, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
                }
                if (!is_array($registros->aDebitosNormaisEncontrados) && !is_array($registros->aDebitosUnicosEncontrados)) {
                    return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
                }
            }
        }

        if (isset($registros->aDebitosNormaisEncontrados) && !isset($registros->aDebitosUnicosEncontrados)) {
            if (is_array($registros->aDebitosNormaisEncontrados)) {
                return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "registros" => $registros->aDebitosNormaisEncontrados, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
            } else {
                return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
            }
        }

        if (!isset($registros->aDebitosNormaisEncontrados) && isset($registros->aDebitosUnicosEncontrados)) {
            if (is_array($registros->aDebitosUnicosEncontrados)) {
                return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "registros_unicos" => $registros->aDebitosUnicosEncontrados, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
            } else {
                return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
            }
        }

        if (!isset($registros->aDebitosNormaisEncontrados) && !isset($registros->aDebitosUnicosEncontrados)) {
            return view('iptu.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf_cnpj" => $request->cpf, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao]);
        }
    }


    public function diaUtilEcidade(Request $request)
    {
        $result = $this->financial->verificaDiaUtil($request->date);
        return response()->json($result->bDiaUtil, 200);
    }

    public function integracaoReciboDebitos(Request $request)
    {
        $cpfUsuario = remove_character_document($this->cpf);

        if (isset($request->registros_unicos)) {

            $data1 = [
                'sExec'         => 'integracaorecibodebitos',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_matricula_inscricao' => $request->matricula_inscricao,
                'c_tipo_pesquisa' => $request->tipo_pesquisa,
                'i_tipo_debito' => $request->tipo_debito,
                'a_lista_debitos' => [['i_numpre' => '', 'i_numpar' => '']],
                'i_numpre_unica' => $request->numpre_unica,
                'i_numpar_unica' => $request->numpar_unica,
                'd_data_vencimento' => $request->d_data_vencimento,
                'sTokeValiadacao'   => API_TOKEN,
            ];
        }

        if (isset($request->registros_normais)) {
            $i = 0;
            $i_numpre = array();
            $i_numpar = array();

            if ($request->tipo_debito == 34) {

                foreach ($request->registros_normais as $registros_normais) {
                    $dados = explode("_", $registros_normais);
                    $i_inicial[$i] = $dados[0];
                    $i++;
                }

                $a_lista_inicial = [];
                $i = 0;
                foreach ($i_inicial as $value) {
                    $a_lista_inicial[$i]['i_inicial'] = $value;
                    $i++;
                }


                $data1 = [
                    'sExec'         => 'integracaorecibodebitos',
                    'i_cpfcnpj'    => $cpfUsuario,
                    'i_matricula' => $request->matricula_inscricao,
                    'c_tipo_pesquisa' => $request->tipo_pesquisa,
                    'i_tipo_debito' => $request->tipo_debito,
                    'a_lista_inicial' => $a_lista_inicial,
                    'a_lista_debitos' => $a_lista_inicial,
                    'd_data_vencimento' => $request->dataVencimento,
                    'sTokeValiadacao'   => API_TOKEN,
                ];
            } else {
                foreach ($request->registros_normais as $registros_normais) {
                    $dados = explode("_", $registros_normais);
                    $i_numpre[$i] = $dados[0];
                    $i_numpar[$i] = $dados[1];
                    $i++;
                }

                $numpre_numpar = array();
                $i = 0;
                foreach ($i_numpre as $numpre) {
                    $numpre_numpar[$i]['i_numpre'] = $numpre;
                    $i++;
                }
                $i = 0;
                foreach ($i_numpar as $numpar) {
                    $numpre_numpar[$i]['i_numpar'] = $numpar;
                    $i++;
                }

                if ($request->tipo_debito == 86 && $request->desconto_total != '0') {
                    $data1 = [
                        'sExec'         => 'integracaorecibodebitoscgmsolicitante',
                        'i_cpfcnpj'    => $cpfUsuario,
                        'i_matricula' => $request->matricula_inscricao,
                        'c_tipo_pesquisa' => $request->tipo_pesquisa,
                        'i_tipo_debito' => $request->tipo_debito,
                        'a_lista_inicial' => [],
                        'a_lista_debitos' => [],
                        'i_numpre_unica' => $dados[0],
                        'i_numpar_unica' => $dados[1],
                        'd_data_vencimento' => $request->dataVencimento,
                        'sTokeValiadacao'   => API_TOKEN,
                    ];
                } else {
                    $data1 = [
                        'sExec'         => 'integracaorecibodebitoscgmsolicitante',
                        'i_cpfcnpj'    => $cpfUsuario,
                        'i_matricula' => $request->matricula_inscricao,
                        'c_tipo_pesquisa' => $request->tipo_pesquisa,
                        'i_tipo_debito' => $request->tipo_debito,
                        'a_lista_debitos' => $numpre_numpar,
                        'i_numpre_unica' => '',
                        'i_numpar_unica' => '',
                        'd_data_vencimento' => $request->dataVencimento,
                        'sTokeValiadacao'   => API_TOKEN,
                    ];
                }
            }
        }
        $json_data = json_encode($data1);
        $data_string = ['json' => $json_data];

        $ch = curl_init();
        curl_setopt_array($ch, array(
            // CURLOPT_URL => 'https://10.135.16.85:8090/api-ecidade-online/integracaoGeralFinanceiraDebitos.RPC.php',
            CURLOPT_URL => API_URL . 'integracaoGeralFinanceiraDebitos.RPC.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query($data_string),
            CURLOPT_HTTPHEADER => array(
                'Content-Length: ' . strlen(http_build_query($data_string))
            ),
        ));


        if (USAR_SSL) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
            curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
        }

        $response = json_decode(curl_exec($ch));
        $err = curl_error($ch);
        curl_close($ch);

        // dd($response);

        if ($response->iStatus == 2) {
            return view('errors.erro_default', [
                'titulo' => 'Emissão do IPTU',
                'mensagem' => '',
                'codigo' => 'API_0006'
            ]);
        }

        $tipo = "application/pdf";
        $arquivo = $response->sUrl;
        $vet_arquivo = explode("/", $arquivo);
        $novoNome = "DebitosAbertos.pdf";

        ini_set('max_execution_time', 60);
        stream_context_set_default([
            'ssl' => [
                'verify_peer' => SSL_API_VERIFICA,
                'verify_peer_name' => SSL_API_VERIFICA,
            ]
        ]);

        $tempImage = tempnam(sys_get_temp_dir(), $response->sUrl);
        copy($response->sUrl, $tempImage);

        //Log IPTU
        // $data = DB::connection('mysql')->table('log_iptu')->insertGetId([
        //     'cpf_cnpj'  => $this->cpf,
        //     'matricula' => $request->matricula_inscricao,
        //     'data'      => date("Y-m-d H:i:s"),
        // ]);

        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($tempImage, $novoNome, $headers);
    }

    public function integracaoPesquisaDebitos($request)
    {
        if (isset($request->cpf)) {
            $cpfUsuario = remove_character_document($request->cpf);

            if ($request->tipo_pesquisa == 'i') {
                $i_matricula_inscricao = $request->inscricao;
            }
            if ($request->tipo_pesquisa == 'm') {
                $i_matricula_inscricao = $request->matricula;
            }

            $data1 = [
                'sExec'         => 'integracaopesquisadebitos',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_matricula_inscricao' => $i_matricula_inscricao,
                'c_tipo_pesquisa' => $request->tipo_pesquisa,
                'i_tipo_debito' => $request->tipo_debito,
                'sTokeValiadacao'   => API_TOKEN,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json' => $json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));


            if (USAR_SSL) {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd() . '/cert/' . SSL_API);
            }

            $response = json_decode(curl_exec($ch));

            $err = curl_error($ch);
            curl_close($ch);

            if (isset($response->aDebitosUnicosEncontrados) && isset($response->aDebitosNormaisEncontrados)) {
                if ($request->tipo_debito == '86') {

                    if ($response->aDebitosUnicosEncontrados != '') {
                        $list = [];
                        foreach ($response->aDebitosUnicosEncontrados as $parcelas) {
                            array_push($list, $parcelas->k00_numpar);
                        }

                        if (is_array($list)) {
                            $parcela = $list[0];
                            $collection = collect($response->aDebitosNormaisEncontrados);
                            $result = $collection->filter(function ($value, $key)  use ($parcela) {
                                return $value->k00_numpar < $parcela;
                            });
                            $response->aDebitosNormaisEncontrados = json_decode($result);
                        }
                    }
                }

                return view('financeiro.relatorio_debitos_abertos', ['pagina' => 'Consulta Geral Financeira - Débitos Abertos', 'registros_unicos' => $response->aDebitosUnicosEncontrados, 'registros' => $response->aDebitosNormaisEncontrados]);
            }
            if (isset($response->aDebitosUnicosEncontrados) && !isset($response->aDebitosNormaisEncontrados)) {
                return view('financeiro.relatorio_debitos_abertos', ['pagina' => 'Consulta Geral Financeira - Débitos Abertos', 'registros_unicos' => $response->aDebitosUnicosEncontrados]);
            }
            if (!isset($response->aDebitosUnicosEncontrados) && isset($response->aDebitosNormaisEncontrados)) {
                return view('financeiro.relatorio_debitos_abertos', ['pagina' => 'Consulta Geral Financeira - Débitos Abertos', 'registros' => $response->aDebitosNormaisEncontrados]);
            }
            if (!isset($response->aDebitosUnicosEncontrados) && !isset($response->aDebitosNormaisEncontrados)) {
                return view('financeiro.relatorio_debitos_abertos', ['pagina' => 'Consulta Geral Financeira - Débitos Abertos']);
            }
        }
    }
}
