<div class="col-lg-12">
    <div class="alert alert-success py-5">
        <h3 class="mb-4"><i class="fa fa-check-square-o"></i> {{ session('success')['title'] }}</h3>

        <p><b>Identificação: </b> {{ Auth::user()->name }}</p>

        <p><b>CPF/CNPJ:</b> <span class="{{ mb_strlen(str_only_numbers(Auth::user()->cpf)) == 14 ? 'mask-cnpj' : 'mask-cpf' }}">{{ Auth::user()->cpf }}</span></p>
    
        <p><b>ATENÇÃO: </b> Para consultar o processo, acesse a página de consulta no link abaixo:</p>
        <a href="{{ route('acompanhamento') }}">Clique aqui para consultar o processo</a>
    </div>
</div>