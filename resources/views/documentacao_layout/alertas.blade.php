<div class="card">
    <div class="card-header">
        <h5>Alertas</h5>
    </div>

    <div class="card-body">
        <h6><b>Alertas:</b></h6>
        <hr>

        <div class="alert alert-info mb-3">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>Utilizado para alguma informação que queira passar ao usuário.</p>
        </div>

        <div class="alert alert-warning mb-3">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>Utilizado para algum aviso mais importante que queira passar ao usuário.</p>
        </div>

        <div class="alert alert-success mb-3">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>Utilizado para algum aviso positivo que queira passar ao usuário.</p>
        </div>

        <div class="alert alert-danger mb-5">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p>Utilizado para algum aviso negativo que queira passar ao usuário.</p>
        </div>
    </div>
</div>