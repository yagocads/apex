<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive nowrap w-100">
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
                <th>Extn.</th>
                <th data-priority="2">E-mail</th> <!--data-priority="2" coloca na 3ª posicão a coluna no reponsivo -->
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger</td>
                <td>Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
                <td>5421</td>
                <td>t.nixon@datatables.net</td>
            </tr>
        </tbody>
    </table>
</div>

<!--
    Inicialização datatable no modal
    OBS: Sempre inicializar dentro da section('post-script')
-->
<script>
    $('.modal').on('shown.bs.modal', function() {
        $('#id_da_tabela').DataTable({
            // "paging:" false,    <- Removendo Paginação
            // "info:" false       <- Removendo Informações da Tabela
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            }
        }).columns.adjust().responsive.recalc(); // <- Recalcula a tabela para manter responsivo
    });
</script>
