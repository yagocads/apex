@extends('layouts.tema_principal')

@section('content')
    <div class="container-fluid">
        <input type="hidden" name="cpf_ou_cnpj" value="{{ isset($dadosConsulta->cpf_cnpj) ? $dadosConsulta->cpf_cnpj : '' }}" />

        @include('cadastro.cgm.consulta_mrc.consulta')

        @if (!empty($dadosConsulta))
            @include('cadastro.cgm.consulta_mrc.situacao')

            @if ($situacao == "solicitacao_nao_aprovada")
                @include('cadastro.cgm.consulta_mrc.recurso')
            @endif

            @if ($situacao == "beneficio_concedido")
                @include('cadastro.cgm.consulta_mrc.beneficio_concedido')
            @endif
        @endif
    </div>
@endsection

@section('post-script')

<script>
    // Spinner
    let load = $(".ajax_load");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("body").on("change", "select[name=recurso_tipo_documento]", function() {
        let value = $(this).val();

        if (value === "Outro") {
            $(".recurso_descricao_documento").removeClass("d-none");
        } else {
            $(".recurso_descricao_documento").addClass("d-none");
        }
    });

    $("body").on("click", ".btn-enviar-documento-recurso", function(e) {
        e.preventDefault();    

        let tipo_documento = $("select[name=recurso_tipo_documento]").val();

        if (tipo_documento === "Outro") {
            tipo_documento = $("textarea[name=recurso_descricao_documento]").val();

            if (!tipo_documento) {
                alert("ATENÇÃO: Preencha a descrição do documento para continuar.");
                return false;
            }
        }

        if (!tipo_documento) {
            alert("ATENÇÃO: Preencha o tipo de documento para continuar.");
            return false;
        }

        let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();
        let nome_input = $(this).data("nome_input");
        let nome_sessao = $(this).data("nome_sessao");
        let conteudo_documentos = $(this).data("conteudo_documentos");
        let id_tabela = $(this).data("id_tabela");
        let pasta = $(this).data("pasta");
        let recurso_tipo_documento = $("select[name=recurso_tipo_documento]").val();
        
        let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

        if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
            return false;
        }

        let formData = new FormData();

        formData.append("arquivo", arquivo);
        formData.append("descricao", tipo_documento);
        formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
        formData.append("nome_sessao", nome_sessao);
        formData.append("pasta", pasta);

        if (recurso_tipo_documento == "Outro") {
            formData.append("descricao_martelada", "outro");
        }

        $.ajax({
            url: "{{ route('salvar_documento_servidor') }}",
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {

                $("input[name=" + nome_input + "]").val("");
                $("select[name=recurso_tipo_documento]").val("");
                $("textarea[name=recurso_descricao_documento]").val("");
                $(".recurso_descricao_documento").addClass("d-none");

                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $("." + conteudo_documentos).html(response);
                recalcularDataTable(id_tabela);
            }
        });
    });

    $("body").on("click", ".btn-excluir-documento", function(e) {
        e.preventDefault();
        
        if (!confirm("Deseja realmente excluir?")) {
            return false;
        }

        let nome_arquivo = $(this).data("nome_arquivo");
        let nome_sessao = $(this).data("nome_sessao");
        let id_tabela = $(this).data("id_tabela");
        let nome_tabela = $(this).data("nome_tabela");
        let pasta = $(this).data("pasta");
        let conteudo_documentos = $(this).data("conteudo_documentos");

        $.ajax({
            url: "{{ route('excluir_documento_servidor') }}",
            method: 'POST',
            data: {
                "nome_arquivo": nome_arquivo,
                "nome_sessao": nome_sessao,
                "pasta": pasta,
                "nome_tabela": nome_tabela,
            },
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                
                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $("." + conteudo_documentos).html(response);
                recalcularDataTable(id_tabela);
            }
        });
    });

    $("body").on("click", ".btn-solicitar-recurso", function(e) {
        e.preventDefault();

        let justificativa_recurso = $("textarea[name=justificativa_recurso]").val();
        let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

        if (!justificativa_recurso) {
            alert("Atenção: O campo Motivo do Recurso é obrigatório.");
            $("textarea[name=justificativa_recurso]").focus();
            return;
        }

        $.ajax({
            url: "{{ route('enviar_recurso') }}",
            method: 'POST',
            data: {
                "justificativa_recurso": justificativa_recurso,
                "cpf_ou_cnpj": cpf_ou_cnpj                
            },
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                
                load.fadeOut(200);

                if (response.error === "true") {
                    alert("ERRO ao enviar o recurso, entre em contato com a administração.");
                    return;
                }

                alert("Recurso solicitado com sucesso!");
                location.reload();
            }
        });
    })
</script>

@endsection