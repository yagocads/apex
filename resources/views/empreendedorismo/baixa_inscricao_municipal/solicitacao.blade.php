<div class="row">
    <div class="col-lg-12">
        <div class="form-row">
            <div class="col-lg-3 form-group">
                <label for="tipo_baixa_inscricao">Tipos de Baixa de Inscrição:</label>
                <select name="tipo_baixa_inscricao" id="tipo_baixa_inscricao"
                    class="form-control form-control-sm">
                    <option></option>

                    <option {{ old('tipo_baixa_inscricao') == 'Distrato Social' ? 'selected' : '' }} >Distrato Social</option>
                    <option {{ old('tipo_baixa_inscricao') == 'Transferência de Endereço' ? 'selected' : '' }}>Transferência de Endereço</option>
                    <option {{ old('tipo_baixa_inscricao') == 'Autônomo' ? 'selected' : '' }}>Autônomo</option>
                    <option {{ old('tipo_baixa_inscricao') == 'MEI - Encerramento' ? 'selected' : '' }}>MEI - Encerramento</option>
                    <option {{ old('tipo_baixa_inscricao') == 'MEI - Transferência de Endereço' ? 'selected' : '' }}>MEI - Transferência de Endereço</option>
                </select>
            </div>

            <div class="col-lg-12 observacao_autonomo d-none">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> <b>Observação:</b> No campo "Observação Adicionais" deverá ser escrita a justificativa da baixa de inscrição.
                </div>
            </div>

            <div class="col-lg-12 form-group">
                <label for="observacoes_adicionais">Observações Adicionais:</label>
                <textarea class="form-control form-control-sm {{ ($errors->has('observacoes_adicionais') ? 'is-invalid' : '') }}"
                    cols="2" rows="6" maxlength="1024" id="observacoes_adicionais"
                    name="observacoes_adicionais">{{ old('observacoes_adicionais') }}</textarea>

                @if ($errors->has('observacoes_adicionais'))
                    <div class="invalid-feedback">
                        {{ $errors->first('observacoes_adicionais') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
