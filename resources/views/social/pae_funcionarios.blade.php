<div class="accordion-inner">
    
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="cpfFuncionario" class="col-md-4 col-form-label text-md-right">
                {{ __('CPF do Funcionário') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span3">
            <input maxlength="14" id="cpfFuncionario" type="text" class="form-input"  name="cpfFuncionario" value="">
        </div>
    </div>
    
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="nomeFuncionario" class="col-md-4 col-form-label text-md-right">
                    {{ __('Nome do Funcionário') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="nomeFuncionario" type="text" class="form-input"  name="nomeFuncionario" value="">
        </div>

    </div>
    
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="percentualReducao" class="col-md-4 col-form-label text-md-right">
                    {{ __('Percentual de redução') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span1">
            <select name="percentualReducao" id="percentualReducao" style="width:100%;">
                <option value=""></option>
                <option value="0"> 0%</option>
                <option value="25">25%</option>
                <option value="50">50%</option>
                <option value="70">70%</option>
            </select>
        </div>
    </div>


    <form method="POST" action="" enctype="multipart/form-data">
        @csrf
    </form>
    <form method="POST" action="" id="formFuncionario" name="formFuncionario" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Acordo de Redução') }} <span id="exigeAcordo" class="required" style="display: none;">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="file" name="arquivoAcordoReducao" id="arquivoAcordoReducao" /><br>
            </div>
            <div class="span2">
            </div>
        </div>
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('SEFIP da Empresa') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="SEFIP" />
                <input type="hidden" name="tipoDoc" value="SEFIP" />
                <input type="hidden" name="idCpf" id="idFuncionario" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoFuncionario" /><br>
                Obs I: O tamanho máximo dos arquivos é de 2Mb.<br>
                Obs II: Limite máximo de <b>49</b> funcionários <br>
                Obs III: Caso tenha aderido ao Programa Emergencial de Manutenção do Emprego e da Renda instituído pela Medida Provisória nº 936/2020 (Governo Federal), informar o percentual de redução salarial de cada funcionário. 

            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarFuncionario">
                    {{ __('Lançar Funcionário') }}
                </button>
            </div>
        </div>
    </form>

    <div class="row formrow">
        <div class="span11">
            <h4 class="heading">Funcionários Lançados<span></span></h4>

            <table id="fucionariosLancados" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>CPF</th>
                        <th>Nome</th>
                        <th>% Redução</th>
                        <th>Acordo</th>
                        <th>SEFIP</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
    <br>
    <br>
</div>