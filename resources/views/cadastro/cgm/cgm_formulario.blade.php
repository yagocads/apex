@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h3>
                <i class="fa fa-home mb-3"></i> {{session('vencimentoIPTU')==1?"IPTU Solicitar Recebimento em Casa - Idoso":"Cadastrar/Atualizar CGM"}}
            </h3>

            @if (session()->has('success'))
                <div class="alert alert-success mt-3">
                    <i class="fa fa-check-square-o"></i> {{ session('success') }}
                </div>
            @endif
        
            <form method="POST" name="formSalvarCgm" action="{{ route('salvar_cgm') }}" autocomplete="off">
                @csrf
                <input type="hidden" name="verificar_cpf_cnpj" value="{{ mb_strlen(remove_character_document($cpfOuCnpj)) == 11 ? 1 : 2 }}" />
                <input type="hidden" name="cpf_ou_cnpj" value="{{ $cpfOuCnpj }}" />
                <input type="hidden" name="verificar_cgm_na_cultura" value="{{ $verificaCadastroCgmNaCultura > 0 ? 1 : 0 }}" />
                <input type="hidden" name="vencimentoIPTU" value="{{ session()->has('vencimentoIPTU')?session('vencimentoIPTU'):0 }}" />

                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <span class="d-block">* {{ $error }}</span>
                        @endforeach
                    </div>
                @endif
            
                <div id="accordion">
                    
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#requerente">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                            </div>
                        </a>
                        <div id="requerente" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("cadastro.cgm.requerente.requerente")
                            </div>
                        </div>
                    </div>

                    @if (mb_strlen(remove_character_document($cpfOuCnpj)) == 14)
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados_mercantis">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados Mercantis
                                </div>
                            </a>
                            <div id="dados_mercantis" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cadastro.cgm.dados_mercantis.dados_mercantis")
                                </div>
                            </div>
                        </div>
                    @endif
                
                    @if (mb_strlen(remove_character_document($cpfOuCnpj)) == 11)
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#informacoes_pessoais">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Informações Pessoais
                                </div>
                            </a>
                            <div id="informacoes_pessoais" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cadastro.cgm.informacoes_pessoais.informacoes_pessoais")
                                </div>
                            </div>
                        </div>
                    @endif
                
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#informacoes_contato">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Informações de Contato
                            </div>
                        </a>
                        <div id="informacoes_contato" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("cadastro.cgm.informacoes_contato")
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#endereco_cgm">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Endereço do Requerente
                            </div>
                        </a>
                        <div id="endereco_cgm" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("cadastro.cgm.endereco.endereco")
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#cadastro_imoveis">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> 
                                Cadastro de Imóveis (Preencher se for proprietário ou possuidor de imóvel no município)
                            </div>
                        </a>
                        <div id="cadastro_imoveis" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("cadastro.cgm.cadastro_imovel.cadastro_imovel")
                            </div>
                        </div>
                    </div>

                    @if ($verificaCadastroCgmNaCultura == 0)
                        @if(session('vencimentoIPTU')==0)
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#perfil_cidadao">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> 
                                    Perfil do Cidadão
                                </div>
                            </a>
                            <div id="perfil_cidadao" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cadastro.cgm.perfil_cidadao")
                                </div>
                            </div>
                        </div>
                        @endif
                        
                        <div class="card informacoes_culturais {{ old('artista') == null || old('artista') == 'nao' ? 'd-none' : '' }} mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#informacoes_culturais">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> 
                                    Informações Culturais
                                </div>
                            </a>
                            <div id="informacoes_culturais" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("cadastro.cgm.informacoes_culturais.informacoes_culturais")
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <p>
                                Declaro que são VERDADEIRAS e EXATAS todas as informações que foram 
                                prestadas neste formulário. Declaro ainda estar ciente de que declaração 
                                falsa no presente cadastro constituirá crime de 
                                falsidade ideológica (art. 299 do Código Penal) e estará sujeita a sanções penais 
                                sem prejuízo de medidas administrativas e outras. <span class="text-danger">*</span>
                            </p>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" name="confirmacao_de_informacoes" 
                                        class="form-check-input" value="sim" required>
                                    Li e concordo.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @auth
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'imovel'])  }}" class="btn btn-primary btn-sm mt-3">
                        @else
                            @if(session('vencimentoIPTU')==1)
                                <a href="{{ route('atualizaCGMIPTU') }}" class="btn btn-primary btn-sm mt-3">
                            @else
                                <a href="{{ route('atualizaCGM') }}" class="btn btn-primary btn-sm mt-3">
                            @endif	
                        @endauth
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                    
                        <button class="btn btn-success btn-sm mt-3 enviar_informacoes">Enviar Informações <i class="fa fa-send"></i></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script>
    $(function() {

        @if(empty($errors->all()))
            @auth
                $("#informacoes_pessoais").collapse();
            @else
                $("#requerente").collapse();
            @endauth
        @endif


        // Spinner
        var load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        responsavelPeloPreenchimento();

        function responsavelPeloPreenchimento() {

            $("select[name=responsavel]").change(function() {
                let value = $(this).val();

                if (value === "1") {
                    $(".requerente_prosseguimento").addClass("d-none");
                } else {
                    $(".requerente_prosseguimento").removeClass("d-none");
                }
            });
        }

        contaOperacaoCaixaEconomicaFederal();

        function contaOperacaoCaixaEconomicaFederal() {

            $("select[name=banco_nome]").change(function() {
                let value = $(this).val();

                if (value === "Caixa Econômica Federal") {
                    $(".banco_conta_operacao").removeClass("d-none");
                } else {
                    $(".banco_conta_operacao").addClass("d-none");
                }
            });
        }

        aparecerConteudoAoClicarSim(
            "input[name=artista]", 
            ".informacoes_culturais"
        );

        aparecerConteudoAoClicarSim(
            "input[name=registro_profissional]", 
            ".nome_registros_profissionais"
        );

        aparecerConteudoAoClicarSim(
            "input[name=lei_emergencia_cultural]",
            ".lei_emergencia_cultural_prosseguimento"
        );

        aparecerConteudoAoClicarSim(
            "input[name=beneficio_previdenciario]",
            ".beneficio_previdenciario_prosseguimento"
        );

        aparecerConteudoAoClicarSim(
            "input[name=mulher_familia_monoparental]",
            ".mulher_familia_monoparental_prosseguimento"
        );

        aparecerConteudoAoClicarSim(
            "input[name=espaco_cultural_coletivo]",
            ".espaco_cultural_coletivo_prosseguimento"
        );

        function aparecerConteudoAoClicarSim(input, conteudoQueDeveAparecer) {

            $(input).change(function() {
                let value = $(this).val();

                if (value === "sim") {
                    $(conteudoQueDeveAparecer).removeClass("d-none");
                } else {
                    $(conteudoQueDeveAparecer).addClass("d-none");
                }
            });
        }

        limitarCheckboxMarcado(5, "insercao_atividade_artistico_cultural");

        function limitarCheckboxMarcado(maxInputs, inputName) {
            $("body").on("change", "." + inputName, function() {
                
                let inputs = $("." + inputName + ":checked").length;

                if (inputs > maxInputs) {
                    alert("Atenção: Você só pode marcar até 5 opções.");
                    $(this).prop('checked', false);
                }
            });
        }

        $("#cpf_requerente").on("change", function() {
            let cpf = $(this).val();

            if (!validarCPF(cpf)) {
                alert("Atenção: CPF Inválido! Informe corretamente o número do seu CPF.");
                $(this).val("");
                $(this).focus();
                return;
            }
        });

        validarTodosCamposEmail();

        function validarTodosCamposEmail() {

            $("input[type=email]").on("change", function() {
                let email = $(this).val();

                if (!validaEmail(email)) {
                    alert("Atenção: O formato do E-mail é inválido.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        validarEmailsCorrespondem();

        function validarEmailsCorrespondem() {

            $("input[name=confirmacao_email]").on("change", function() {
                
                let email = $("input[name=email]").val();
                let confirmacao_email = $(this).val();

                if (email !== confirmacao_email) {
                    alert("Atenção: Os e-mails não correspondem.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        $("body").on("click", ".btn-enviar-documento-informacoes-culturais", function(e) {
            e.preventDefault();

            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");

            let nome_descricao_input = $(this).data("nome_descricao_input");
            let descricao = $("textarea[name=" + nome_descricao_input + "]").val();

            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = $(this).data("nome_tabela");
            let pasta = $(this).data("pasta");

            if (!descricao) {
                alert("ATENÇÃO: Preencha o campo descrição para continuar.");
                return false;
            }

            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);
            formData.append("nome_tabela", nome_tabela);
        
            if (nome_input == "portfolio") {
                formData.append("descricao_martelada", "portfolio");
            } else if (nome_input == "documento_mulher_familia_monoparental") {
                formData.append("descricao_martelada", "monoparental");
            }

            $.ajax({
                url: "{{ route('salvar_documento_servidor') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");
                    $("textarea[name=" + nome_descricao_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $(this).data("descricao");
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");
            
            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);

            $.ajax({
                url: "{{ route('salvar_documento_servidor') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-enviar-dados-imovel", function(e) {
            e.preventDefault();
            
            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $(this).data("descricao");
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");
            
            let matricula_imovel = $("input[name=matricula_imovel]");
            let tipo_vinculo = $("select[name=tipo_vinculo]");
            let tipo_documento = $("select[name=tipo_documento]");

            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            if (!matricula_imovel.val()) {
                alert("ATENÇÃO: Preencha o campo matrícula para continuar.");
                matricula_imovel.focus();
                return false;
            }

            if (!tipo_vinculo.val()) {
                alert("ATENÇÃO: Preencha o campo tipo de vínculo para continuar.");
                tipo_vinculo.focus();
                return false;
            }

            if (!tipo_documento.val()) {
                alert("ATENÇÃO: Preencha o campo tipo de documento para continuar.");
                tipo_documento.focus();
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);
            formData.append("matricula_imovel", matricula_imovel.val());
            formData.append("tipo_vinculo", tipo_vinculo.val());
            formData.append("tipo_documento", tipo_documento.val());
            
            $.ajax({
                url: "{{ route('salvar_documento_servidor') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");
                    matricula_imovel.val("");
                    tipo_vinculo.val("");
                    tipo_documento.val("");
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-excluir-documento", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let nome_arquivo = $(this).data("nome_arquivo");
            let nome_sessao = $(this).data("nome_sessao");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = $(this).data("nome_tabela");
            let pasta = $(this).data("pasta");
            let conteudo_documentos = $(this).data("conteudo_documentos");

            $.ajax({
                url: "{{ route('excluir_documento_servidor') }}",
                method: 'POST',
                data: {
                    "nome_arquivo": nome_arquivo,
                    "nome_sessao": nome_sessao,
                    "pasta": pasta,
                    "nome_tabela": nome_tabela,
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });
    });

    verificarDocumentosNecessariosEcgmResideEmMarica();

    function verificarDocumentosNecessariosEcgmResideEmMarica() {
        $(".enviar_informacoes").on("click", function(e) {
            e.preventDefault();

            if (!verificarCampoProsseguirLeiEmergenciaCultural()) {
                return;
            }

            let documentos_necessarios = [
                {
                    "nome_sessao": "documento_endereco",
                    "descricao": "Comprovante de Residência em (Endereço)",
                    "qtd": 1
                }
            ];

            verificarNecessidadeAdicionarDocumentosRequerente(documentos_necessarios);
            adicionarDocumentosInformacoesPessoaisOuDadosMercantis(documentos_necessarios);
            verificarNecessidadeAdicionarDocumentosPortfolio(documentos_necessarios);
            verificarNecessidadeAdicionarDocumentosMulherFamiliaMonoparental(documentos_necessarios);

            verificarEnvioDocumentosEcgmResideEmMarica(documentos_necessarios);
        });
    }

    function verificarCampoProsseguirLeiEmergenciaCultural() {

        let lei_emergencia_cultural = $("input[name=lei_emergencia_cultural]:checked").val();
        let artista = $("input[name=artista]:checked").val();

        if (artista === "sim") {

            if (lei_emergencia_cultural !== "sim") {
                alert("Atenção: Para continuar com o cadastro da cultura é necessário clicar em Prosseguir em (Lei de Emergência Cultural Aldir Blanc)");
                return false;
            }
        }

        return true;
    }

    function verificarNecessidadeAdicionarDocumentosRequerente(documentos_necessarios) {
        let responsavel_preenchimento = $("select[name=responsavel]").val();
        let responsavel_legal = "2";

        if (responsavel_preenchimento === responsavel_legal) {
            documentos_necessarios.push({
                "nome_sessao": "documentos_requerente",
                "descricao": "Representação Legal em (Documentos do Requerente)",
                "qtd": 1
            });
        }
    }

    function adicionarDocumentosInformacoesPessoaisOuDadosMercantis(documentos_necessarios) {
        let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val().length;
        
        if (cpf_ou_cnpj === 14) {
            @if(session('vencimentoIPTU')==0)
                documentos_necessarios.push({
                    "nome_sessao": "documentos_informacoes_pessoais",
                    "descricao": "Identidade, CPF, Sélfie de Identificação em (Informações Pessoais)",
                    "qtd": 3
                });
            @else
                documentos_necessarios.push({
                    "nome_sessao": "documentos_informacoes_pessoais",
                    "descricao": "Identidade e CPF em (Informações Pessoais)",
                    "qtd": 2
                });
            @endif
        } else if (cpf_ou_cnpj === 18) {
            documentos_necessarios.push({
                "nome_sessao": "documentos_dados_mercantis",
                "descricao": "Contrato Social, Identidade do Sócio, CPF do Sócio em (Dados Mercantis)",
                "qtd": 3
            });
        }
    }
    
    function verificarNecessidadeAdicionarDocumentosPortfolio(documentos_necessarios) {
        let artista = $("input[name=artista]:checked").val();

        if (artista === "sim") {
            documentos_necessarios.push({
                "nome_sessao": "documentos_informacoes_culturais_portfolio",
                "descricao": "Portfólio em (Anexos - Informações Culturais)",
                "qtd": 1
            });
        }
    }

    function verificarNecessidadeAdicionarDocumentosMulherFamiliaMonoparental(documentos_necessarios) {
        let mulher_familia_monoparental = $("input[name=mulher_familia_monoparental]:checked").val();

        if (mulher_familia_monoparental === "sim") {
            documentos_necessarios.push({
                "nome_sessao": "documentos_informacoes_culturais_familia_monoparental",
                "descricao": "Certidão de Nascimento ou CPF em (Mulher Provedora de Família Monoparental - Informações Culturais)",
                "qtd": 1
            });
        }
    }

    function verificarEnvioDocumentosEcgmResideEmMarica(documentos_necessarios) {

        $.ajax({
            url: "{{ route('verificar_envio_documentos') }}",
            method: 'POST',
            data: {
                "documentos_necessarios": documentos_necessarios
            },
            success(response) {

                if (response.documentos_em_falta.length > 0) {
                    for (let i = 0; i < response.documentos_em_falta.length; i++) {
                        alert("Atenção: Os seguintes documentos são necessários: " + response.documentos_em_falta[i].descricao);
                    }
                } else {

                    if (verificarCgmResideMunicipioMarica()) {
                        $("form[name=formSalvarCgm]").submit();
                    }
                }
            }
        });
    }

    function verificarCgmResideMunicipioMarica() {
        let artista = $("input[name=artista]:checked").val();
        let municipio = $("input[name=municipio]").val();

        if (artista == "sim" && municipio != "Maricá") {
            alert("Atenção: O Benefício da Cultura só está disponível para o município de Maricá.");
            return false;
        }

        return true;
    }
    
</script>

@endsection