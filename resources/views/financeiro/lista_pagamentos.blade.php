@extends('layouts.tema_principal')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Consulta Geral Financeira</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ Route('integracaoRelatorioPagamentosEfetuados') }}" method="post">
            @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="container">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        @if(strlen(Auth::user()->cpf ) == 14)
                                            <label for="cpf">{{ __('CPF') }}</label>
                                            <input type="hidden" id="hd_cpf" value="{{ Auth::user()->cpf }}">
                                        @else
                                            <label for="cpf">{{ __('CNPJ') }}</label>
                                        @endif
                                        <input type="text" class="form-control form-control-sm" name="cpf" value="{{ Auth::user()->cpf }}" readonly="readonly" />
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        @if(isset($request->inscricao))
                                            <label for="inscricao">{{ __('Inscrição Municipal') }}</label>
                                            <input type="text" class="form-control form-control-sm" name="inscricao" value="{{ $request->inscricao }}" readonly="readonly" />
                                        @else
                                            <label for="matricula">{{ __('Matrícula do Imóvel') }}</label>
                                            <input type="text" class="form-control form-control-sm" name="matricula" value="{{ $request->matricula }}" readonly="readonly" />
                                        @endif
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label for="nome">{{ __('Nome') }}</label>
                                        <input type="text" class="form-control form-control-sm" name="nome" value="{{ Auth::user()->name }}" readonly="readonly" />
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4>{{ $pagina }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table table-striped table-bordered dataTable dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>Cod. Arrecadação</th>
                                        <th>Operação</th>
                                        <th>Parcela</th>
                                        <th>Vencimento</th>
                                        <th>Receita</th>
                                        <th>Valor</th>
                                        <th>Pagamento</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data->aPagamentosEfetuadoss as $item)
                                    <tr>
                                        <td>{{ $item->k00_numpre }}</td>
                                        <td>{{ date('d/m/Y', strtotime($item->k00_dtoper)) }}</td>
                                        <td>{{ $item->k00_numpar }}</td>
                                        <td>{{ date('d/m/Y', strtotime($item->k00_dtvenc)) }}</td>
                                        <td>{{ str_replace('%2F','/',str_replace('+',' ',convert_accentuation($item->k02_drecei))) }}</td>
                                        <td>
                                            @if($item->k00_valor < 0)
                                                <span style="color:#FF0000;">R$ {{ number_format($item->k00_valor, 2, ',', '.') }}</span>
                                            @else
                                                <span>R$ {{ number_format($item->k00_valor, 2, ',', '.') }}</span>
                                            @endif
                                        </td>
                                        <td>{{ date('d/m/Y', strtotime($item->efetpagto)) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="total">{{ __('Total Pago') }}</label>
                                <input type="text" class="form-control" name="total" value="R$ {{ number_format($total, 2, ',', '.') }}" readonly />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{ route('pagamentosEfetuados') }}"
                                    class="btn btn-primary">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>&nbsp;
                                <button id="btn_imprimir" type="submit" class="btn btn-success">Imprimir <i class="fa fa-print"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
