@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">        
            <h3>
                <i class="fa fa-home mb-3"></i> Averbação de Imóveis
            </h3>
        
            <form method="POST" action="" autocomplete="off">
                @csrf

                <input type="hidden" name="adquierente_existente" value="false" />
                <input type="hidden" name="tipo_solicitacao" value="3" />
                <input type="hidden" name="matricula" value="{{ $matricula }}" />
                <input type="hidden" name="guia_itbi" value="{{ $guia }}" />

                <div class="alert alert-info">
                    <h4 class="mb-3">Formulário destinado a transações por meio de ITBI (Inter vivos).</h4>
                    <p>
                        <a href="{{ url('/download/RGI_fundo_branco.pdf') }}" target="_blank">
                            Clique aqui
                        </a> 
                        
                        para identificar no Registro do Imóvel no Cartório (RGI) onde se encontram as informações para preenchimento do formulário abaixo.
                    </p>

                    <p class="alert alert-danger">
                        <b>IMPORTANTE:</b> 
                    
                        Preencher os dados com cuidado, pois quaisquer divergências entre os dados cadastrados e o 
                        documento apresentado serão analisadas e apontadas.
                    </p>
                </div>
            
                <div id="accordion">
                    
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#averbacao">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Averbação
                            </div>
                        </a>
                        <div id="averbacao" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include('imovel.averbacao.form_averbacao')
                            </div>
                        </div>
                    </div>
                
                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#adquirentes">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Adquirente
                            </div>
                        </a>
                        <div id="adquirentes" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include('imovel.averbacao.adquirentes.form_adquirentes')
                            </div>
                        </div>
                    </div>
                
                    <div class="card">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#documentos">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Documentação em Anexo
                            </div>
                        </a>
                        <div id="documentos" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include('imovel.averbacao.documentos.form_documentos')
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'imovel']) }}" class="btn btn-primary mt-3">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                    
                        <button class="btn btn-success mt-3 solicitar">Solicitar <i class="fa fa-send"></i></button>
                    </div>
                </div>

            </form> 
        </div>
    </div>
</div>

@include('modals.modal_default')

@endsection

@section('post-script')

<script>

    $(function() {

        // Spinner
        let load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        /**
         * (Agrupamento: Averbação)
         * 
         * # Validação Data de Registro do Imóvel no Cartório
         * 
         * - Não pode ser maior que o dia de hoje
         * - Não pode ter mais que 90 dias no passado
         * 
         */
        $("#data_imovel_registro_cartorio").change(function() {

            let parts_data = $(this).val().split("-");
            let data_hoje = new Date();
            let data_imovel_registro_cartorio = new Date(parts_data[0], parts_data[1] - 1, parts_data[2]);

            if (data_imovel_registro_cartorio > data_hoje) {
                alert("ATENÇÃO: A data não pode ser maior que o dia de hoje.");
                $(this).val("");
                return;
            }

            let data_anterior_90_dias = new Date();
            data_anterior_90_dias.setDate(data_hoje.getDate() - 90);

            if (data_imovel_registro_cartorio < data_anterior_90_dias) {
                alert("ATENÇÃO: A data não pode ser anterior a 90 dias.");
                $(this).val("");
                return;
            }
        });

         /**
         * (Agrupamento: Adquirentes)
         * 
         * # Busca por Cadastro no CGM
         * 
         * - Caso exista, mostrar apenas Nome/Razão Social
         * - Caso não exista, habilitar todos os campos e bloquear o campo CGM
         * 
         */
        $(".pesquisar_cgm").click(function(e) {
            
            e.preventDefault();
            
            let document = $('input[name=document]').val();
            let url = "recupera_dados_cgm/" + document; 

            if (document === "") {
                alert("ATENÇÃO: Por favor preencha o CPF/CNPJ antes de pesquisar.");
                $("input[name=document]").focus();
                return;
            }

            if (document.length == 14) {
                if (!validarCPF(document)) {
                    desabilitaCamposAdquirentes();
                    alert("ATENÇÃO: CPF inválido! Favor informar um CPF válido.");
                    return false;
                }
            } else if (document.length == 18) {
                if (!validarCNPJ(document)) {
                    desabilitaCamposAdquirentes();
                    alert("ATENÇÃO: CNPJ inválido! Favor informar um CNPJ válido.");
                    return false;
                }
            } else {
                desabilitaCamposAdquirentes();
                alert("ATENÇÃO: Informe corretamente o número do seu CPF ou CNPJ.");
                return false;
            }

            $.ajax({
                url: url,
                method: 'get',
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    if (response.adquirente_principal) {
                        $("input[name=adquirente_principal]").attr("disabled", true);
                    } else {
                        $("input[name=adquirente_principal]")
                            .prop("checked", true)
                            .attr("disabled", false);
                    }
                    
                    if (response.exists) {
                        $("input[name=adquierente_existente]").val("true");
                        preencheCamposAdquirentes(response);
                        escondeCamposAdquirentes();
                    } else {
                        alert("Dados não encontrados.");
                        habilitaCamposAdquirentes();
                    }

                    load.fadeOut(200);
                }
            });
        });

         /**
         * (Agrupamento: Adquirentes)
         * 
         * # Salvar Adquirente na sessão
         * 
         */
         $("body").on("click", ".salvar_adquirente", function(e) {
            
            e.preventDefault();

            let document = $("input[name=document]").val();

            if (document === "") {
                alert("ATENÇÃO: Por favor preencha o CPF/CNPJ antes de salvar.");
                $("input[name=document]").focus();
                return;
            }

            let adquierente_existente = $("input[name=adquierente_existente]").val();
            let adquirente_principal = $("input[name=adquirente_principal]").is(":checked");
            let razao_social = $("input[name=razao_social]").val();
            let cgm = $("input[name=cgm]").val();
            let telefone = $("input[name=telefone]").val();
            let celular = $("input[name=celular]").val();
            let email = $("input[name=email]").val();

            let cep = $("input[name=cep]").val();
            let endereco = $("input[name=endereco]").val();
            let numero = $("input[name=numero]").val();
            let complemento = $("input[name=complemento]").val();
            let bairro = $("input[name=bairro]").val();
            let municipio = $("input[name=municipio]").val();
            let estado = $("select[name=estado]").val();

            let url = "{{ route('salvar_adquirente_sessao') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    "adquierente_existente": adquierente_existente,
                    "adquirente_principal": adquirente_principal,
                    "document": document,
                    "razao_social": razao_social,
                    "cgm": cgm,
                    "telefone": telefone,
                    "celular": celular,
                    "email": email,

                    "cep": cep,
                    "endereco": endereco,
                    "numero": numero,
                    "complemento" : complemento,
                    "bairro": bairro, 
                    "municipio": municipio, 
                    "estado": estado 
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: Por favor preencha todos os campos para enviar.");
                        return;
                    }

                    if (response.exists) {
                        alert("ATENÇÃO: Adquirente já cadastrado.");
                        return;
                    }

                    $('.conteudo_adquirentes').html(response);

                    desabilitaCamposAdquirentes();
                    recalcularDataTable("tabela_adquirentes");
                }
            });

        });

        /**
         * (Agrupamento: Adquirentes)
         * 
         * # Remover Adquirente na sessão
         * 
         */
        $("body").on("click", ".remover_adquirente", function(e) {

            e.preventDefault();

            if (!confirm("Deseja realmente remover o adquirente?")) {
                return false;
            }

            let document = $(this).data("document");

            let url = "remover_adquirente_sessao/" + document;

            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    document: document
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    $('.conteudo_adquirentes').html(response);

                    recalcularDataTable("tabela_adquirentes");
                }
            });
        });

        /**
         * (Agrupamento: Documentação em Anexo)
         * 
         * # Salvar Documento na sessão
         * 
         */
         $("body").on("click", ".salvar_documento", function(e) {

            e.preventDefault();

            let arquivo = $('input[name=arquivo]')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            let formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("descricao", $("#descricao").val());   
            formData.append('outro_anexo',$("#outro_anexo").val());  

            let url = "salvar_documento_averbacao";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=arquivo]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $(".conteudo_documentos_adquirentes").html(response);
                    recalcularDataTable("documentos_adquirentes");
                }
            });
        });

        /**
         * (Agrupamento: Documentação em Anexo)
         * 
         * # Remover Documento na sessão
         * 
         */
        $("body").on("click", ".remover_documento_adquirente", function(e) {

            e.preventDefault();

            if (!confirm("Deseja realmente remover o documento?")) {
                return false;
            }

            let documento = $(this).data("documento");

            let url = "remover_documento_adquirente_sessao/" + documento;

            $.ajax({
                url: url,
                method: 'POST',
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    $(".conteudo_documentos_adquirentes").html(response);
                    recalcularDataTable("documentos_adquirentes");
                }
            });
        });

        function habilitaCamposAdquirentes() {

            $("input[name=razao_social]").val("").attr("readonly", false);
            $("input[name=telefone]").val("").attr("readonly", false);
            $("input[name=celular]").val("").attr("readonly", false);
            $("input[name=email]").val("").attr("readonly", false);
            $("input[name=cgm]").val("").attr("readonly", true);

            $("input[name=cep]").val("").attr("readonly", false);
            $("input[name=endereco]").val("");
            $("input[name=numero]").val("").attr("readonly", false);
            $("input[name=complemento]").val("").attr("readonly", false);
            $("input[name=bairro]").val("");
            $("input[name=municipio]").val("");
            $("select[name=estado]").val("");

            $(".endereco_adquirente").show();
            $(".telefone").show();
            $(".celular").show();
            $(".email").show();
            $(".cgm").show();
        }

        function desabilitaCamposAdquirentes() {

            $("input[name=adquirente_principal]").prop("checked", false).attr("disabled", true);
            $("input[name=document]").val("");
            $("input[name=razao_social]").val("").attr("readonly", true);
            $("input[name=telefone]").val("").attr("readonly", true);
            $("input[name=celular]").val("").attr("readonly", true);
            $("input[name=email]").val("").attr("readonly", true);
            $("input[name=cgm]").val("").attr("readonly", true);

            $("input[name=cep]").val("").attr("readonly", true);
            $("input[name=endereco]").val("").attr("readonly", true);
            $("input[name=numero]").val("").attr("readonly", true);
            $("input[name=complemento]").val("").attr("readonly", true);
            $("input[name=bairro]").val("").attr("readonly", true);
            $("input[name=municipio]").val("").attr("readonly", true);
            $("select[name=estado]").val("").attr("readonly", true);

            $(".endereco_adquirente").show();
            $(".telefone").show();
            $(".celular").show();
            $(".email").show();
            $(".cgm").show();
        }

        function preencheCamposAdquirentes(response) {

            $("input[name=razao_social]").val(response.cgm.nome);
            $("input[name=telefone]").val(response.cgm.telefone);
            $("input[name=celular]").val(response.cgm.celular);
            $("input[name=email]").val(response.cgm.email);
            $("input[name=cgm]").val(response.cgm.cgm);

            $("input[name=cep]").val(response.cgm.cep);
            $("input[name=endereco]").val(response.cgm.endereco);
            $("input[name=numero]").val(response.cgm.numero);
            $("input[name=complemento]").val(response.cgm.complemento);
            $("input[name=bairro]").val(response.cgm.bairro);
            $("input[name=municipio]").val(response.cgm.municipio);
            $("select[name=estado]").val(response.cgm.estado);
        }

        function escondeCamposAdquirentes() {

            $(".endereco_adquirente").hide();

            $(".telefone").hide();
            $(".celular").hide();
            $(".email").hide();
            $(".cgm").hide();
        }

        /**
         * 
         * # Solicitação de Averbação
         * 
         */
        $(".solicitar").on("click", function(e) {
            e.preventDefault();

            let dataRegistroImovelCartorio = $("input[name=data_imovel_registro_cartorio]").val();
            let numeroRegistro = $("input[name=numero_registro]").val();
            let protocoloReg = $("input[name=protocolo_reg]").val();
            let tipoSolicitacao = $("input[name=tipo_solicitacao]").val();
            let matricula = $("input[name=matricula]").val();
            let guiaItbi = $("input[name=guia_itbi]").val();

            if (!dataRegistroImovelCartorio) {
                alert("A data do Registro do imóvel não pode ser vazio.");
                return false;
            }

            if (!numeroRegistro) {
                alert("O número do Registro não pode ser vazio.");
                return false;
            }

            if (!protocoloReg) {
                alert("O protocolo Reg não pode ser vazio.");
                return false;
            }

            $.ajax({
                url: "gravar_averbacao",
                method: 'POST',
                data: {
                    "dataRegistroImovelCartorio": dataRegistroImovelCartorio,
                    "numeroRegistro": numeroRegistro,
                    "protocoloReg": protocoloReg,
                    "tipoSolicitacao": tipoSolicitacao,
                    "matricula": matricula,
                    "guiaItbi": guiaItbi
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $('#default-modal').find('.modal-title').html(response.titulo);
                    $('#default-modal').find('.modal-body').html(response.mensagem);
                    $('#default-modal').modal('show');

                    $("#default-modal").on('hidden.bs.modal', function() {
                        if (response.reload) {
                            location.reload();
                        }
                    });

                    load.fadeOut(200);
                }
            });
        });

        /**
         * 
         * # MASKs 
         * - Número do Registro
         * - Protocolo Reg
         * 
         */
        $('.maskNumeroRegistro').mask("999999999");
        $('.maskProtocoloReg').mask("99999999999999999999");
    });
</script>

@endsection