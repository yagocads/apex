
<div class="row">
    <div class="col-lg-6">
        <label for="mes"><b>Mês de Referência: </b></label>
        <select name="mes" id="mes" class="form-control form-control-sm">
            <option></option>
            @foreach ($mesesPendentes as $mespendente)
            <option>{{$mespendente}}</option>
            @endforeach
        </select>
    </div>  
</div>
<br>
@foreach ($funcionarios as $funcionario)
<form method="POST" action="" autocomplete="off" id="form-{{$loop->iteration}}">
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <label for="nome_funcionario"><b>{{$loop->iteration}}) Nome: </b></label>
            <input type="text" name="nome_funcionario" id="nome_funcionario-{{$loop->iteration}}" value="{{$funcionario->NOME}}" class="form-control form-control-sm" readonly/>
        </div>
        <div class="col-lg-6">
            <label for="cpf_funcionario"><b>CPF: </b></label>
            <input type="text" name="cpf_funcionario" id="cpf_funcionario-{{$loop->iteration}}" value="{{$funcionario->CPF}}" class="form-control form-control-sm" readonly />
        </div>
        <br>
        <div class="col-lg-6 form-group">
            <label for="descricao">Descrição:</label>
            <select name="descricao" id="descricao-{{$loop->iteration}}" class="form-control form-control-sm">
                <option>SEFIP da Empresa</option>
            </select>
        </div>

        <div class="col-lg-6 form-group">
            <label for="arquivo">Documento:</label><br>
            <input type="file" name="arquivo" id="arquivo-{{$loop->iteration}}" />
        </div>

        <div class="col-lg-12">
            <button class="btn btn-primary btn-sm salvar_documento-{{$loop->iteration}}" id="btn-envia-form-{{$loop->iteration}}"><i class="fa fa-save"></i> Salvar Documento</button>
        </div>
    </div>
</form>
<hr />
@endforeach

<div class="row col-lg-12 mb-3 mt-3">
    <h5><i class="fa fa-file-o"></i> Documentos Enviados:</h5>
</div>

<div class="row">
    <div class="col-lg-12 conteudo_documentos_funcionarios">
        @include('financeiro.social.tabela_documentos_funcionarios')
    </div>
</div>
