
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-bordered dataTable dt-responsive w-100" id="tabela_documentos_exigencia">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Arquivo</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_exigencia"))
                @foreach(session("documentos_exigencia") as $documento)
                    <tr>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->arquivo }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-remover-documento" 
                                title="Remover Documento"
                                data-caminho="{{ $documento->caminho }}"
                                data-descricao="{{ $documento->descricao }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>