@extends('layouts.tema_principal')

@section('content')


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h3>
                <i class="fa fa-home mb-3"></i> Fomenta Maricá
            </h3>
            <br>
            <br>
            <div class="row justify-content-center"> 
                <form method="POST" action="{{ route('imprimirprotocolofomenta') }}" id="frmConsultarAuxilio">
                    @csrf
                    <input id="protocolo" type="hidden" name="protocolo" value="{{ $protocolo }}">
                </form>
                <div class="col-lg-8">
                    <div class="alert alert-info">
                        <center>
                            <h2>Crédito Solicitado</h2>
                        </center>
                        <p>
                            As análises documentais somente serão iniciadas no início de setembro de 2020, acompanhe sua solicitação utilizando-se deste protocolo para mais informações dentro do próprio sistema. O Programa Fomenta Maricá MEI Emergencial dará prioridade ao atendimento de Microempreendedores NÃO beneficiados pelo Programa de Amparo ao Trabalhador – PAT e pelo Programa de Amparo ao Emprego – PAE e que sejam mais antigos. A inscrição no Programa não garante que a sua solicitação será aprovada ficando sujeita, além dos itens acima, ao limite de recurso destinado ao Programa Fomenta Maricá Emergencial MEI.
                        </p>
                        <br>
                        <p>
                            <strong>Atenção:</strong><br>
                            Guarde o número do protocolo, ele será solicitado para fazer a consulta do andamento desta solicitação.<br>
                            <br>
                            <br>
                            Nome Empresarial: <b>{{$razaoSocial}}</b><br>
                            CNPJ: <b>{{$cnpj}}</b><br><br>
                            <b>Protocolo:</b>
                            <center>
                            <h3>{{$protocolo}}</h3>
                            </center>
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <a href="{{ route('cadastrofomenta', 1 )  }}">
                        <button type="button" class="btn btn-primary mt-3">
                            {{ __('Finalizar') }}
                        </button>
                    <a>
                    
                    <button type="button" class="btn btn-success mt-3" id="imprimirProtocolo">
                        {{ __('Imprimir Protocolo') }}
                    </button>

                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</section>
@endsection

@section('post-script')

<script type="text/javascript">
    jQuery(document).ready(function(){
        $("#imprimirProtocolo").on("click", function(e) {
            $("#frmConsultarAuxilio").submit();
        })
    });
    
</script>
@endsection