
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela_documentos_imovel">
        <thead>
            <tr>
                <th>Possui Matricula</th>
                <th>Matricula</th>
                <th>Cep</th>
                <th>Descrição</th>
                <th>Arquivo</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_imovel"))
                @foreach(session("documentos_imovel") as $documento)
                    <tr>
                        <td>{{ $documento->possui_matricula }}</td>
                        <td>{{ $documento->matricula_imovel }}</td>
                        <td>{{ $documento->cep }}</td>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento" title="Remover Documento" 
                                data-id="{{ $documento->id }}"
                                data-nome_arquivo="{{ $documento->nome_novo }}"
                                data-pasta="cadastro_imovel"
                                data-nome_sessao="documentos_imovel"
                                data-id_tabela="tabela_documentos_imovel"
                                data-conteudo_documentos="conteudo_documentos_imovel">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>