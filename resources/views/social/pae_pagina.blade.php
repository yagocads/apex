@extends('layouts.tema_principal')

@section('content')
    <br>
    <style>
        .texto {
            text-align:justify; 
            font-size:18px;
            font-weight:400;
            padding-left:30px; 
            padding-top: 5px;
            line-height: 30px;
            color: #075c7b;
        }
        .img-topo{
            width: 100%;
        }
        .img-titulo{
            width: 100%;
        }
        .img-botao{
            width: 100%;
        }
    </style>

    <div class="container-fluid">
        <div class="col d-flex justify-content-center">
            <div class="row">
                <div class="col-lg-12">
                    <img src="img/pae/topo.png" class="img-topo" alt="">
                    <p></p>
                </div>
                <div class="col-lg-6">
                    <img src="img/pae/oquee.png" class="img-titulo" alt=""/>
                </div>
                <div class="col-lg-12">
                    <p class="texto">Iniciativa de apoio ao empregador MEI, Micro ou Pequena empresa, com efetivo de até 49 empregados, afetadas pela pandemia do COVID-19, visando a manutenção dos empregos formais no município, através do repasse de 01 (um) salário mínimo mensal, por 03 meses a fim de subsidiar o salário de seus funcionários.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
         <div class="col-lg-6">
            <img src="img/pae/contato.png" class="img-botao" alt=""/>
         </div>
         <div class="col-lg-6">
            <br>
            <br>
            <br>
            
            <a href="{{ route('consultarsituacaopae') }}">
                <img src="img/pae/consulta.png" class="img-botao" alt=""/>
            </a>
            <br>
            <br>
            <a href="{{ route('prestacaoContasPAE') }}">
                <img src="img/pae/prestacao_contas.png" class="img-botao" alt=""/>
            </a>
          
         </div>
    </div>
</div>
<br>
<br>
<br>
@endsection
