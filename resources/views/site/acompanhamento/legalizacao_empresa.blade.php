<div class="row registro-fase-topo">
    <div class="col-lg-2">
        Nº do Protocolo: <b>{{ $registro->Chamado }}</b>
    </div>
    <div class="col-lg-2">
        Data: <b>{{ date('d/m/Y', strtotime($registro->abertura)) }}</b>
    </div>
    <div class="col-lg-4">
        Serviço: <b>{{ $registro->Serviço }}</b>
    </div>
    <div class="col-lg-4">
        Responsável: <b>{{ $registro->responsavel_consulta }}</b><br>
    </div>

    <div class="justfy-content-center registro-fase">
        <center>
            @switch($registro->Fase)
                @case('Abertura do Processo')
                    <img src="{{ asset('img/fases_mei_01.png') }}" alt="Fase" class="fase" />
                    @break
                @case('Cadastro Empresarial')
                    <img src="{{ asset('img/fases_mei_02.png') }}" alt="Fase" class="fase" />
                    @break
                @case('Análise Empresarial')
                    <img src="{{ asset('img/fases_mei_03.png') }}" alt="Fase" class="fase" />
                    @break
                @case('Cumprir Pendências')
                    <img src="{{ asset('img/fases_mei_04.png') }}" alt="Fase" class="fase" />
                    @break
                @case('Emissão do Alvará')
                    <img src="{{ asset('img/fases_mei_05.png') }}" alt="Fase" class="fase" />
                    @break
                @case('Encerramento do Processo')
                    <img src="{{ asset('img/fases_mei_06.png') }}" alt="Fase" class="fase" />
                    @break
            @endswitch
        </center>
        @if($registro->Fase == "Cumprir Pendências")
            @if( strtolower($registro->responsavel) == "portal")
            <br>
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                <a href="{{ route('exigencias_mei', $registro->Chamado) }}" class="btn btn-blue">Editar
                    Solicitação</a>
            </div>
            @endif
        @endif

        @if( $registro->FINALIZADO != "andamento")
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Atenção!</strong><br>
            Esta solicitação foi finalizada e teve como conclusão:<br>
            Conclusão: <strong>{{$registro->Fase}}</strong><br>
            @if($registro->motivo != "")
                Motivo: {{$registro->motivo}}<br>
            @endif
        </div>
        @endif
    </div>
</div>