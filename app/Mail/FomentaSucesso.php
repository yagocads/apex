<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FomentaSucesso extends Mailable
{
    use Queueable, SerializesModels;
    protected $mensagem;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensagem, $protocolo)
    {
        $this->mensagem = $mensagem;
        $this->protocolo = $protocolo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('PROTOCOLO FOMENTA MARICÁ MEI EMERGENCIAL')
                    ->markdown('emails.fomenta_sucesso', [
                        'url' => url(route('fomenta')),
                        'mensagem' => $this->mensagem,
                        'protocolo' => $this->protocolo
                    ]);
    }
}