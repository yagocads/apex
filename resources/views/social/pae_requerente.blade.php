<div class="accordion-inner">

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="respPreenchimento" class="col-md-4 col-form-label text-md-right">
                {{ __('Responsável pelo Preenchimento') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <select name="respPreenchimento" id="respPreenchimento" style="width:100%;">
                <option value=""></option>
                <option value="Representante Legal">Representante Legal</option>
                <option value="Sócio Administrador">Sócio Administrador</option>
                <option value="Sócio Cotista">Sócio Cotista</option>
            </select>
        </div> 
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="nomeRespLegal" class="col-md-4 col-form-label text-md-right">
                    {{ __('Nome Completo') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="nomeRespLegal" type="text" class="form-input"  name="nomeRespLegal" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="nacionalidadeRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Nacionalidade') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <select name="nacionalidadeRespLegal" id="nacionalidadeRespLegal" style="width:100%;">
                <option value=""></option>
                <option value="Brasileira">Brasileira</option>
                <option value="Estrangeira">Estrangeira</option>
            </select>
        </div>

        {{-- Separação --}}
        <div class="controls span1 form-label">
            <label for="" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="controls span2 form-label">
            <label for="estCivilRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Estado Civil') }} <span class="required">*</span>
            </label>
        </div>
    
        <div class="span2">
            <select name="estCivilRespLegal" id="estCivilRespLegal" style="width:100%; ">
                <option value=""></option>
                <option value="Solteiro">Solteiro</option>
                <option value="Casado">Casado</option>
                <option value="Viúvo">Viúvo</option>
                <option value="Divorciado">Divorciado</option>
                <option value="Separado Consensual">Separado Consensual</option>
                <option value="Separado Judicial">Separado Judicial</option>
                <option value="União Estavel">União Estavel</option>
            </select>
        </div>
    </div>




    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="idRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Identidade') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input maxlength="20" id="idRespLegal" type="text" class="form-input" name="idRespLegal" value="">
        </div>

        {{-- Separação --}}
        <div class="controls span1 form-label">
            <label for="" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="controls span2 form-label">
            <label for="orgEmRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Órgão Emissor') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input maxlength="30" id="orgEmRespLegal" type="text" class="form-input" name="orgEmRespLegal" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="dtEmissRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Data Emissão') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input id="dtEmissRespLegal" type="text" class="form-input" name="dtEmissRespLegal" value="">
        </div>

        {{-- Separação --}}
        <div class="controls span1 form-label">
            <label for="" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="controls span2 form-label">
            <label for="cpfRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('CPF') }} <span class="required">*</span>
            </label>
        </div>
        <div class="span2">
            <input maxlength="20" id="cpfRespLegal" type="text" class="form-input" name="cpfRespLegal" value="">
        </div>        
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="emailRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('E-mail') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="emailRespLegal" type="text" class="form-input" name="emailRespLegal" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="confEmailRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Confirmação do E-mail') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="confEmailRespLegal" type="text" class="form-input" name="confEmailRespLegal" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="profissaoRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Profissão') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <select id="profissaoRespLegal" class="form-input" name="profissaoRespLegal">
                <option value="" ></option>
                @foreach ($cbos as $cbo)
                    <option value= "{{$cbo}}">
                        {{$cbo}}
                    </option>
                @endforeach
            </select>

        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="cepRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('CEP') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input id="cepRespLegal" type="text" class="form-input" name="cepRespLegal" value="">

        </div>
        <div class="span2 align-left">
            <button type="button" class="btn e_wiggle align-right" id="btn_consultarCepRespLegal" style="margin: 0;">
                {{ __('Buscar Cep') }}
            </button>
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="ufRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Estado') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <select id="ufRespLegal" class="form-input" name="ufRespLegal" disabled>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Su</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
            </select>
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="cidadeRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Município') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input readonly maxlength="30" id="cidadeRespLegal" type="text" class="form-input" name="cidadeRespLegal" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="bairroRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Bairro') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input readonly maxlength="30" id="bairroRespLegal" type="text" class="form-input" name="bairroRespLegal" value="">
         </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="logradouroRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Logradouro') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="logradouroRespLegal" type="text" class="form-input" name="logradouroRespLegal" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="numeroRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Número') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input maxlength="9" id="numeroRespLegal" type="text" class="form-input" name="numeroRespLegal" style="margin-bottom: 1px;" value="">
            <span style="font-size: 10px;"><b>Se não tiver Número, colocar 0</b></span>
        </div>

        <div class="controls span2 form-label">
            <label for="complementoRespLegal" class="col-md-4 col-form-label text-md-right">
                {{ __('Complemento') }}
            </label>
        </div>

        <div class="span3">
            <input maxlength="30" id="complementoRespLegal" type="text" class="form-input" name="complementoRespLegal" value="">
        </div>
    </div>


    <form method="POST">
        @csrf
    </form>


    <form method="POST" action="" id="formCPFRespLegal" name="formCPFRespLegal" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('CPF do Representante') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="CPF Representante" />
                <input type="hidden" name="tipoDoc" value="CPFRespLegal" />
                <input type="hidden" name="idCpf" id="idCPFRespLegal" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoCPFRespLegal" /><br>
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCPFRespLegal" >
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formEnderecoRespLegal" name="formEnderecoRespLegal" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Comprovante de Residência') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Comprovante de Endereço Representante" />
                <input type="hidden" name="tipoDoc" value="ComprovanteEnderecoRespLegal" />
                <input type="hidden" name="idCpf" id="idEnderecoRespLegal" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoEnderecoRespLegal" /><br>
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoEnderecoRespLegal" >
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formDocRespLegal" name="formDocRespLegal" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Anexar Representação Legal') }} <span class="required" id="ExigeRepresentacao" style="display: none;">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Representação Legal" />
                <input type="hidden" name="tipoDoc" value="Representacao_Legal" />
                <input type="hidden" name="idCpf" id="idCpf" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoRespLegal" /><br>
                Obs.: O tamanho máximo dos arquivos é de 2Mb.
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoRespLegal">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <div class="row formrow">
        <div class="span11">
            <h4 class="heading">Documentos Lançados<span></span></h4>

            <table id="documentosLancadosReq" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th width='20%'>Descrição</th>
                        <th>Nome Original</th>
                        <th>Nome Final</th>
                        <th width='20%'>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
    <br>
    <br>














</div>
