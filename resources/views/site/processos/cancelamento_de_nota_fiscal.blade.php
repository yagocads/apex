<div class="row">
    @if($processo->Fase != "Finalizado" && $processo->Fase != 'Concluído')
        @if($processo->Etapa == 4)
            <div class="col-lg-6">
                <div class="alert alert-info">
                    <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;
                    <a href="{{ route('formulario-nf-certidao', ['chamado' => $processo->Chamado, 'ciclo' => $processo->Ciclo, 'tipo' => 3]) }}" class="btn btn-blue">Editar Solicitação</a>
                    <div class="col-lg-6">
                        Motivo: {{ $processo->motivo }}
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                @if($processo->motivo == '')
                    <h5>Aceite do Parcelamento</h5>
                    <p>
                        Para registrar o parcelamento é necessário anexar o Termo de Compromisso assinado. Você encontra este termo clicando
                        <a href="{{ route('termo_aceite') }}" id="termo">aqui <i class="fa fa-download"></i></a>
                    </p>
                @else
                    <h5>Exigência Documental</h5>
                    <p>
                        Para andamento do seu processo, foram exigidas as seguintes documentações:
                        {{$processo->motivo}}
                    </p>
                @endif
            </div>
        @endif
    @endif
    @if($processo->motivo != "" && $processo->FINALIZADO != "andamento")
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Atenção!</strong><br>
        Esta solicitação foi finalizada e teve como conclusão:<br>
        Conclusão: <strong>{{$processo->Fase}}</strong><br>
        @if($processo->motivo != "")
            Motivo: {{$processo->motivo}}<br>
        @endif
    </div>
    @endif
</div>
