
<div class="form-row">
    <div class="col-lg-3 form-group">
        <label for="data_imovel_registro_cartorio">Data de Registro do Imóvel no Cartório:</label>
        <input type="date" name="data_imovel_registro_cartorio" 
            id="data_imovel_registro_cartorio" class="form-control form-control-sm" required />
    </div>

    <div class="col-lg-2 form-group">
        <label for="numero_registro">Número do Registro:</label>
        <input type="text" name="numero_registro" id="numero_registro"
            class="form-control form-control-sm maskNumeroRegistro" required />
    </div>

    <div class="col-lg-2 form-group">
        <label for="protocolo_reg">Protocolo Reg:</label>
        <input type="text" name="protocolo_reg" id="protocolo_reg" 
            class="form-control form-control-sm maskProtocoloReg" required />
    </div>
</div>
