<?php

namespace App\Support;

class CidadaoSupport extends CurlSupport
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Recuperando dados do CGM
     * 
     * @param string $document
     * @return mixed
     */
    public function getDadosCGM(string $document)
    {
        $this->url = API_URL . API_CGM;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'         => API_CONSULTA_CGM,
                'z01_cgccpf'    => $document,
            ])
        ];

        return $this->request()->callback();
    }

    /**
     * Recuperando endereço do CGM
     * 
     * @param string $document
     * @param string $tipo
     * 
     * @return mixed
     */
    public function getEnderecoCGM(string $document, string $tipo = "P")
    {
        $this->url = API_URL . API_CGM;
        $this->customRequest = "POST";

        $this->build = [
            'json' => json_encode([
                'sTokeValiadacao'   => API_TOKEN,
                'sExec'         => API_CONSULTA_ENDERECO_CGM,
                'sCpfcnpj'      => $document,
                'sTipoEndereco' => $tipo
            ])
        ];

        return $this->request()->callback();
    }
}
