<div class="card">
    <div class="card-header">
        <h5>Tabelas</h5>
    </div>

    <div class="card-body">
        <h6><b>DataTable Responsivo:</b></h6>
        <hr>

        <div class="alert alert-warning">
            <p><b><i class="fa fa-volume-up"></i> Observação: </b> Sempre que a datable estiver escondida no HTML, você deve recalcular a datable para o responsivo funcionar.</p>
        </div>

        <div class="table-responsive py-1 pl-1 pr-1 mb-4">
            <table class="table table-striped table-bordered dataTable dt-responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Salary</th>
                        <th>Extn.</th>
                        <th data-priority="2">E-mail</th>
                        <!--data-priority="2" coloca na 3ª posicão a coluna no reponsivo -->
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Tiger</td>
                        <td>Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>5421</td>
                        <td>t.nixon@datatables.net</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <h6><b>DataTable Responsivo (Sem Recursos):</b></h6>
        <hr>

        <div class="table-responsive py-1 pl-1 pr-1">
            <table class="table table-striped table-bordered dataTableParametrosOff dt-responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Salary</th>
                        <th>Extn.</th>
                        <th data-priority="2">E-mail</th>
                        <!--data-priority="2" coloca na 3ª posicão a coluna no reponsivo -->
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Tiger</td>
                        <td>Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>5421</td>
                        <td>t.nixon@datatables.net</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>