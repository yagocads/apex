@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-success py-5">
                <h3 class="mb-4"><i class="fa fa-check-square-o"></i> Cadastro realizado com sucesso!</h3>

                <p><b>ATENÇÃO:</b> Guarde o número de protocolo, 
                ele será solicitado para fazer a consulta do andamento desta solicitação.</p>

                <p><b>Identificação: </b> {{ $identificacao }}</p>

                <p><b>CPF/CNPJ:</b> <span class="cpfOuCnpj">{{ $cpfOuCnpj }}</span></p>
                <p><b>Protocolo:</b> {{ $numeroProtocolo }}</p>

                <form method="POST" action="{{ route('imprimir_protocolo_mrc') }}">
                    @csrf
                    
                    <input type="hidden" name="cpf_cnpj" value="{{ $cpfOuCnpj }}" />
                    <input type="hidden" name="identificacao" value="{{ $identificacao }}" />
                    <input type="hidden" name="protocolo" value="{{ $numeroProtocolo }}" />
                    
                    <button class="btn btn-success">
                        Imprimir Protocolo <i class="fa fa-print"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection