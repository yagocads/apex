
<div class="solicitante_prosseguimento">
    <div class="row">
        <div class="col-lg-6">
            <h5><i class="fa fa-user mt-3 mb-3"></i> Dados do Solicitante:</h5>

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="nome_solicitante">Nome Completo: <span class="text-danger">*</span></label>
                    <input type="text" name="nome_solicitante" id="nome_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('nome_solicitante') ? 'is-invalid' : '') }}"
                        value="{{Auth::user()->name}}" maxlength="100" readonly/>

                        @if ($errors->has('nome_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('nome_solicitante') }}
                            </div>
                        @endif
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="cpf_solicitante">CPF: <span class="text-danger">*</span></label>
                    <input type="text" name="cpf_solicitante" id="cpf_solicitante" 
                        class="form-control form-control-sm mask-cpf {{ ($errors->has('cpf_solicitante') ? 'is-invalid' : '') }}" 
                        value="{{Auth::user()->cpf}}" maxlength="20" readonly/>

                        @if ($errors->has('cpf_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('cpf_solicitante') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-8 form-group">
                    <label for="email_solicitante">E-mail: <span class="text-danger">*</span></label>
                    <input type="email" name="email_solicitante" id="email_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('email_solicitante') ? 'is-invalid' : '') }}" 
                        value="{{Auth::user()->email}}" maxlength="100" readonly/>

                        @if ($errors->has('email_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email_solicitante') }}
                            </div>
                        @endif
                </div>
            </div>
            
            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="identidade_solicitante">Identidade: <span class="text-danger">*</span></label>
                    <input type="text" name="identidade_solicitante" id="identidade_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('identidade_solicitante') ? 'is-invalid' : '') }}" 
                        value="{{ old('identidade_solicitante') }}" maxlength="20" />

                        @if ($errors->has('identidade_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('identidade_solicitante') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-4 form-group">
                    <label for="orgao_emissor_solicitante">Órgão Emissor: <span class="text-danger">*</span></label>
                    <input type="text" name="orgao_emissor_solicitante" id="orgao_emissor_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('orgao_emissor_solicitante') ? 'is-invalid' : '') }}" 
                        value="{{ old('orgao_emissor_solicitante') }}" maxlength="45" />

                        @if ($errors->has('orgao_emissor_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('orgao_emissor_solicitante') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-4 form-group">
                    <label for="data_emissao_solicitante">Data Emissão: <span class="text-danger">*</span></label>
                    <input type="date" name="data_emissao_solicitante" 
                        id="data_emissao_solicitante"
                        max="{{ date('Y-m-d') }}" 
                        class="form-control form-control-sm {{ ($errors->has('data_emissao_solicitante') ? 'is-invalid' : '') }}"
                        value="{{ old('data_emissao_solicitante') }}" />

                        @if ($errors->has('data_emissao_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('data_emissao_solicitante') }}
                            </div>
                        @endif
                </div>
            </div> 



            
            <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Endereço:</h5>
            
            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="cep_solicitante">CEP: <span class="text-danger">*</span></label>
                    <input type="text" name="cep_solicitante" id="cep_solicitante"
                        onChange="pesquisaCepSolicitante(this.value);" 
                        class="form-control form-control-sm mask-cep {{ ($errors->has('cep_solicitante') ? 'is-invalid' : '') }}"
                        value="{{ old('cep_solicitante') }}" />

                        @if ($errors->has('cep_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('cep_solicitante') }}
                            </div>
                        @endif
                </div>
                        
                <div class="col-lg-8 form-group">
                    <label for="endereco_solicitante">Endereço: <span class="text-danger">*</span></label>
                    <input type="text" name="endereco_solicitante" id="endereco_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('endereco_solicitante') ? 'is-invalid' : '') }}"
                        value="{{ old('endereco_solicitante') }}" maxlength="100" readonly/>

                        @if ($errors->has('endereco_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('endereco_solicitante') }}
                            </div>
                        @endif
                </div>

            </div>
            
            <div class="form-row">
                <div class="col-lg-2 form-group">
                    <label for="numero_solicitante">Número: <span class="text-danger">*</span></label>
                    <input type="text" name="numero_solicitante" id="numero_solicitante" 
                        class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('numero_solicitante') ? 'is-invalid' : '') }}"
                        value="{{ old('numero_solicitante') }}" />

                        @if ($errors->has('numero_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('numero_solicitante') }}
                            </div>
                        @endif
                </div>
            
                <div class="col-lg-6 form-group">
                    <label for="complemento_solicitante">Complemento:</label>
                    <input type="text" name="complemento_solicitante" id="complemento_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('complemento_solicitante') ? 'is-invalid' : '') }}"
                        value="{{ old('complemento_solicitante') }}" maxlength="50" />

                        @if ($errors->has('complemento_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('complemento_solicitante') }}
                            </div>
                        @endif
                </div>
            
                <div class="col-lg-4 form-group">
                    <label for="bairro_solicitante">Bairro: <span class="text-danger">*</span></label>
                    <input type="text" name="bairro_solicitante" id="bairro_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('bairro_solicitante') ? 'is-invalid' : '') }}" readonly="true"
                        value="{{ old('bairro_solicitante') }}" maxlength="40" />

                        @if ($errors->has('bairro_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('bairro_solicitante') }}
                            </div>
                        @endif
                </div>
            </div>
            
            <div class="form-row">
                <div class="col-lg-6 form-group">
                    <label for="municipio_solicitante">Município: <span class="text-danger">*</span></label>
                    <input type="text" name="municipio_solicitante" id="municipio_solicitante" 
                        class="form-control form-control-sm {{ ($errors->has('municipio_solicitante') ? 'is-invalid' : '') }}" readonly="true"
                        value="{{ old('municipio_solicitante') }}" maxlength="40" />

                        @if ($errors->has('municipio_solicitante'))
                            <div class="invalid-feedback">
                                {{ $errors->first('municipio_solicitante') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-6 form-group">
                    <label for="estado_solicitante">Estado: <span class="text-danger">*</span></label>
                    <select name="estado_solicitante" id="estado_solicitante" class="form-control form-control-sm {{ ($errors->has('estado_solicitante') ? 'is-invalid' : '') }}" 
                        readonly="true">
                        <option></option>

                        @foreach($estados->geonames as $estado)
                            <option value="{{ $estado->adminCodes1->ISO3166_2 }}" {{ old('estado_solicitante') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}>
                                {{ $estado->name }}
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('estado_solicitante'))
                        <div class="invalid-feedback">
                            {{ $errors->first('estado_solicitante') }}
                        </div>
                    @endif
                </div>
            </div>
            
        </div>
        
        <div class="col-lg-6">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

            <div class="form-row">
                <div class="col-lg-12">
                    <div class="alert alert-info">
                        <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                    </div>
                </div>
                
                <div class="col-lg-12 form-group">
                    <label for="documento_cpf_solicitante"><i class="fa fa-paperclip"></i> Anexar CPF do solicitante:</label><br>
                    <input type="file" name="documento_cpf_solicitante" 
                        id="documento_cpf_solicitante" />
                </div>

                <div class="col-lg-12">
                    <button class="btn btn-primary btn-enviar-documento" 
                        data-nome_input="documento_cpf_solicitante" 
                        data-descricao="CPF Solicitante"
                        data-nome_sessao="documentos_solicitante"
                        data-conteudo_documentos="conteudo_documentos_solicitante"
                        data-id_tabela="documentos_solicitante"
                        data-pasta="solicitante">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
            </div>    
            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <hr/>
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="documento_rg_solicitante"><i class="fa fa-paperclip"></i> Anexar RG do solicitante:</label><br>
                    <input type="file" name="documento_rg_solicitante" 
                        id="documento_rg_solicitante" />
                </div>

                <div class="col-lg-12">
                    <button class="btn btn-primary btn-enviar-documento" 
                        data-nome_input="documento_rg_solicitante" 
                        data-descricao="RG Solicitante"
                        data-nome_sessao="documentos_solicitante"
                        data-conteudo_documentos="conteudo_documentos_solicitante"
                        data-id_tabela="documentos_solicitante"
                        data-pasta="solicitante">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
            </div>    
            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <hr/>
                </div>
            </div>

            <div class="form-row">
                
                <div class="col-lg-12 form-group">
                    <label for="documento_cres_solicitante"><i class="fa fa-paperclip"></i> Anexar Comprovante de Residência do solicitante:</label><br>
                    <input type="file" name="documento_cres_solicitante" 
                        id="documento_cres_solicitante" />
                </div>

                <div class="col-lg-12">
                    <button class="btn btn-primary btn-enviar-documento" 
                        data-nome_input="documento_cres_solicitante" 
                        data-descricao="Comprovante Residência Solicitante"
                        data-nome_sessao="documentos_solicitante"
                        data-conteudo_documentos="conteudo_documentos_solicitante"
                        data-id_tabela="documentos_solicitante"
                        data-pasta="solicitante">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
            </div>  

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <hr/>
                </div>
            </div>

            <div class="form-row">
                
                <div class="col-lg-12 form-group">
                    <label for="documento_procuracao_solicitante"><i class="fa fa-paperclip"></i> Procuração (Necessário quando o Solicitante não é um dos sócios):</label><br>
                    <input type="file" name="documento_procuracao_solicitante" id="documento_procuracao_solicitante" />
                </div>

                <div class="col-lg-12">
                    <button class="btn btn-primary btn-enviar-documento" 
                        data-nome_input="documento_procuracao_solicitante" 
                        data-descricao="Procuração Solicitante"
                        data-nome_sessao="documentos_solicitante"
                        data-conteudo_documentos="conteudo_documentos_solicitante"
                        data-id_tabela="documentos_solicitante"
                        data-pasta="solicitante">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
            </div>    
        </div>  
    </div>

    <hr/>

    <div class="row">
        <div class="col-lg-12 form-group">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

            <div class="conteudo_documentos_solicitante">
                @include('empresa.mei.solicitante.tabela_documentos')
            </div>
        </div>
    </div>
</div>
