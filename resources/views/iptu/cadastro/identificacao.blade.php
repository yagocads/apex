@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">

                @if($idoso == 1)
                    <div class="alert alert-info">
                        <h4>Observações Idoso</h3>
                        <p>
                            
                        </p>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-home"></i> Solicitar IPTU</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('valida_cpf_cnpj_iptu') }}" id="FormIptu">
                            @csrf
                            <input type="hidden" name="idoso" id="idoso" value="{{$idoso}}">
                            <div class="form-group">
                                <label for="cpf_ou_cnpj" class="control-label">Digite seu CPF</label>
                                <input maxlength="20" id="cpf_ou_cnpj" type="text" class="form-control {{ $errors->has('cpf_ou_cnpj') ? ' is-invalid' : '' }} mask-cpf " 
                                    name="cpf_ou_cnpj" 
                                    value="" 
                                    required>
            
                                @if ($errors->has('cpf_ou_cnpj'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cpf_ou_cnpj') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>
        
                            <a href="{{ route('principal')  }}">
                                <button type="button" class="btn btn-primary form-group">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </button>
                            <a>

                            <button type="button" class="btn btn-success form-group" id="emitir_iptu">
                                Solicitar IPTU
                            </button>

                            <a href="{{ route('consulta_iptu')  }}">
                                <button type="button" class="btn btn-primary form-group">
                                    <i class="fa fa-search"></i> Consultar Solitação de IPTU
                                </button>
                            <a>
                        </form>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#emitir_iptu").prop('disabled', true);

    function onReCaptchaTimeOut(){
        $("#emitir_iptu").prop('disabled', true);
    }
    function onReCaptchaSuccess(){
        $("#emitir_iptu").prop('disabled', false);
    }


    $("#emitir_iptu").click(function() {

        if ($("#cpf_ou_cnpj").val() == "") {
            alert("Por favor informe o seu CPF ou CNPJ");
            $("#cpf_ou_cnpj").val("");
            $("#cpf_ou_cnpj").focus();
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        $("#FormIptu").submit();
        $("#cpf_ou_cnpj").val("");
        grecaptcha.reset();

        setTimeout(function(){ $("#mdlAguarde").modal('toggle'); }, 5000);
    });

    $('#cpf_ou_cnpj').change(function() {

        if ($("#cpf_ou_cnpj").val().length <= 14) {
            if(!validarCPF($("#cpf_ou_cnpj").val())) {
                alert("CPF Inválido!");
                $("#cpf_ou_cnpj").val("");
                $("#cpf_ou_cnpj").focus();
                return false;
            }
        } else{
            if(!validarCNPJ($("#cpf_ou_cnpj").val())) {
                alert("CNPJ Inválido!");
                $("#cpf_ou_cnpj").val("");
                $("#cpf_ou_cnpj").focus();
                return false;
            }
        }
    });

</script>
@endsection