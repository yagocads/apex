<div class="form-row">
    <div class="col-lg-6">
        <h5><i class="fa fa-user mt-3 mb-3"></i> Informações:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <label for="mes"><b>Mês de Referência: </b></label>
                <select name="mes" id="mes" class="form-control form-control-sm">
                    <option></option>
                    @foreach ($mesesPendentes as $mespendente)
                    <option>{{$mespendente}}</option>
                    @endforeach
                </select>
            </div>  
        </div>
        <br>
    
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="descricao"><b>Tipo de Documento:</b></label>
                <select name="descricao" id="descricao" class="form-control form-control-sm">
                    <option></option>
                    <option value="SEFIP da Empresa">SEFIP da Empresa</option>
                    <option value="Comprovante de Rescisão">Comprovante de Rescisão</option>
                    <option value="Outros">Outros</option>
                </select>
            </div>
        </div>
        <section id="doc_rescisao" style="display: none;" >
            <div class="form-row">
                <div class="col-lg-12">
                    <label for="nome_funcionario"><b>Funcionário: </b></label>
                    <select name="nome_funcionario" id="nome_funcionario" class="form-control form-control-sm">
                        <option></option>
                        @foreach ($funcionarios as $funcionario)
                        <option value="{{$funcionario->CPF}}|{{$funcionario->NOME}}">{{$funcionario->CPF}} | {{$funcionario->NOME}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </section>

        <section id="doc_outros" style="display: none;">
            <div class="form-row">
                <div class="col-lg-12">
                    <label for="descricao_doc"><b>Descrição: </b></label>
                    <input type="text" name="descricao_doc" id="descricao_doc" value="" class="form-control form-control-sm" maxlength="20" />
                </div>
            </div>
        </section>

        <div class="form-row">
            <div class="col-lg-12">
                <label for="comentario"><b>Comentários: </b></label>
                <input type="text" name="comentario" id="comentario" value="" class="form-control form-control-sm" maxlength="50" />
            </div>
        </div>
        <br>
    </div>
            
    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 8Mb.
                </div>
            </div>

            <div class="col-lg-12 form-group">
                <label for="arquivo">Documento:</label><br>
                <input type="file" name="arquivo" id="arquivo" style="width: 100%;"/>
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-sm salvar_documento" id="btn-envia-form"> Enviar Documento <i class="fa fa-send"></i></button>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<hr />

<div class="row col-lg-12 mb-3 mt-3">
    <h5><i class="fa fa-file-o"></i> Documentos Enviados:</h5>
</div>

<div class="row">
    <div class="col-lg-12 conteudo_documentos_funcionarios">
        @include('financeiro.social.tabela_documentos_funcionarios')
    </div>
</div>
