@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-lock"></i> Primeiro Acesso ao Portal SIM</h4>
                    </div>

                    <div class="card-body">
                        <form class="" method="POST" action="{{ route('valida_cpf_login') }}" id="frmCpf" autocomplete="off">
                            @csrf
                            
                            <div class="form-group">
                                <label for="cpf_login">Digite seu CPF/CNPJ</label>
                                <input type="text" name="cpf_login" id="cpf_login" class="form-control {{ $errors->has('cpf_login') ? ' is-invalid' : '' }}" value="" required="true" autofocus />
                                
                                @if ($errors->has('cpf_login'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('cpf_login') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" id="valida_cpf_login" class="btn btn-danger"><i class="fa fa-lock"></i> Validar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>

@endsection


@section('post-script')

<script type="text/javascript">

    var tmpcpf = $("#cpf").val()
    $("#btn_submit").prop('disabled', true);

    console.log("Registrando Funções do captcha");
    function onReCaptchaTimeOut() {
        console.log("Bloquear Botão");
        $("#btn_submit").prop('disabled', true);
    }
    function onReCaptchaSuccess() {
        console.log("Liberar Botão");
        $("#btn_submit").prop('disabled', false);
    }
    function onReCaptchaError() {
        console.log("Erro no captcha");
        $("#btn_submit").prop('disabled', true);
    }
    console.log("Fim do registro");


    jQuery(document).ready(function () {

        $('#cpf').mask('00.000.000/0000-00')
        var cpf_opt = {
            onKeyPress: function (cpf, e, field, cpf_opt) {
                console.log("Keypress: " + cpf.length)
                var masks = ['00.000.000/0000-00', '000.000.000-00'];
                var mask = (cpf.length > 14 || cpf.length == 0) ? masks[0] : masks[1];
                $('#cpf').mask(mask, cpf_opt);
            }
        };

        if (typeof ($("#cpf")) !== "undefined") {
            var masks = ['00.000.000/0000-00', '000.000.000-00'];
            var mask = ($("#cpf").val().length > 14 || $("#cpf").val().length == 0) ? masks[0] : masks[1];
            $("#cpf").mask(mask, cpf_opt);
        }

        $("#cpf").val(tmpcpf);

    });

</script>

@endsection
