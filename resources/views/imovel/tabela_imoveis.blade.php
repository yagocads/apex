<div class="table-responsive py-1 pl-1 pr-1">
    <table class='table table-striped table-bordered dataTable dt-responsive nowrap w-100' id="tabela_imoveis">
        <thead>
            <tr>
                <th>Matrícula</th>
                <th>Tipo</th>
                <th>Planta</th>
                <th>Quadra</th>
                <th>Lote</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($registros as $item)
                <tr>
                    <td>{{ $item->matricula }}</td>
                    <td>{{ $item->tipo }}</td>
                    <td>{{ convert_accentuation($item->planta) }}</td>
                    <td>{{ $item->quadra }}</td>
                    <td>{{ $item->lote }}</td>
                    <td class="text-center">
                        <button class="btn btn-primary" onClick='selectionaPesquisa("{{ ($item->matricula) }}");'>
                            Selecionar
                        </button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="text-center">
            {!! $paginator !!}
    </div>
</div>

<script type="text/javascript">
    $('.modal').on('shown.bs.modal', function() {
        $('#tabela_imoveis').DataTable({
            "stateSave": true,
            "bDestroy": true,
            "bPaginate": false,
            "ordering": true,
            "info": false,
            "searching": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            }
        }).columns.adjust().responsive.recalc();
    });
</script>
@yield('post-script')

