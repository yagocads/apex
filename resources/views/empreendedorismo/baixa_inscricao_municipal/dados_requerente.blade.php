<div class="row">
    <div class="col-lg-12">
        <div class="form-row">
            <div class="col-lg-3 form-group">
                <label for="cpf_requerente">CPF/CNPJ do Requerente:</label>
                <input type="text" name="cpf_requerente" id="cpf_requerente"
                    class="form-control form-control-sm {{ ($errors->has('cpf_requerente') ? 'is-invalid' : '') }}"
                    value="{{ $user->cpf }}" readonly />

                    @if ($errors->has('cpf_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpf_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-9 form-group">
                <label for="nome_requerente">Requerente:</label>
                <input type="text" name="nome_requerente" id="nome_requerente"
                    class="form-control form-control-sm {{ ($errors->has('nome_requerente') ? 'is-invalid' : '') }}"
                    value="{{ $user->name }}" maxlength="100" readonly />

                    @if ($errors->has('nome_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="email_requerente">E-mail do Requerente:</label>
                <input type="text" name="email_requerente" id="email_requerente"
                    class="form-control form-control-sm {{ ($errors->has('email_requerente') ? 'is-invalid' : '') }}"
                    value="{{ $user->email }}" maxlength="100" readonly />

                    @if ($errors->has('email_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="celular_requerente">Celular do Requerente:</label>
                <input type="text" name="celular_requerente" id="celular_requerente"
                    class="form-control form-control-sm mask-cel {{ ($errors->has('celular_requerente') ? 'is-invalid' : '') }}"
                    value="{{ $user->celphone }}" readonly />

                    @if ($errors->has('celular_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('celular_requerente') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div>
