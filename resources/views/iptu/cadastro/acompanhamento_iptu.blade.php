@extends('layouts.tema_principal')

<style type="text/css">
    .scrollspy-container {
        overflow: auto;
    }
    .scrollspy-example {
        width: 100%;
    }
    .bd-step {
        border-bottom: 1px solid #227DC7;
        padding-top: 5px;
        padding-bottom: 10px;
    }
    .registro-fase-topo {
        padding-top: 30px;
    }
    .busca {
        padding-bottom: 10px;
    }
    .servicos p {
        margin: 30px 0 0 0;
    }
    .servicos {
        padding: 20px;
        background-color: #FFF;
        border-radius: 10px;
        height: 200px;
        width: 100%;
        cursor:pointer;
    }
    @media (min-width: 768px) and (max-width: 991px) {
        /* Show 4th slide on md if col-md-4*/
        .carousel-inner .active.col-md-4.carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -33.3333%;  /*change this with javascript in the future*/
            z-index: -1;
            display: block;
            visibility: visible;
        }
    }
    @media (min-width: 576px) and (max-width: 768px) {
        /* Show 3rd slide on sm if col-sm-6*/
        .carousel-inner .active.col-sm-6.carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -50%;  /*change this with javascript in the future*/
            z-index: -1;
            display: block;
            visibility: visible;
        }
    }
    @media (min-width: 576px) {
        .carousel-item {
            margin-right: 0;
        }
        /* show 2 items */
        .carousel-inner .active + .carousel-item {
            display: block;
        }
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item {
            transition: none;
        }
        .carousel-inner .carousel-item-next {
            position: relative;
            transform: translate3d(0, 0, 0);
        }
        /* left or forward direction */
        .active.carousel-item-left + .carousel-item-next.carousel-item-left,
        .carousel-item-next.carousel-item-left + .carousel-item,
        .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }
        /* farthest right hidden item must be also positioned for animations */
        .carousel-inner .carousel-item-prev.carousel-item-right {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            display: block;
            visibility: visible;
        }
        /* right or prev direction */
        .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
        .carousel-item-prev.carousel-item-right + .carousel-item,
        .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }
    }
    /* MD */
    @media (min-width: 768px) {
        /* show 3rd of 3 item slide */
        .carousel-inner .active + .carousel-item + .carousel-item {
            display: block;
        }
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
            transition: none;
        }
        .carousel-inner .carousel-item-next {
            position: relative;
            transform: translate3d(0, 0, 0);
        }
        /* left or forward direction */
        .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }
        /* right or prev direction */
        .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }
    }
    /* LG */
    @media (min-width: 991px) {
        /* show 4th item */
        .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item {
            display: block;
        }
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
            transition: none;
        }
        /* Show 5th slide on lg if col-lg-3 */
        .carousel-inner .active.col-lg-3.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -25%;  /*change this with javascript in the future*/
            z-index: -1;
            display: block;
            visibility: visible;
        }
        /* left or forward direction */
        .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }
        /* right or prev direction //t - previous slide direction last item animation fix */
        .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }
    }
    #carousel-example{
        width: 100%;
    }
 </style>

@section('content')

<div class="container-fluid">
    <div class="row" id="processos_abertos">
        <div class="col-lg-12" id="conteudo">
            @include('site.acompanhamento_processos')
        </div>
    </div>
    <div class="row">
        <div <div class="col-lg-12 mt-3">
            <a href="{{ route('consulta_iptu')  }}">
                <button type="button" class="btn btn-primary form-group">
                    <i class="fa fa-arrow-left"></i> Voltar
                </button>
            <a>
        </div>
    </div>
</div>
@include('modals.modal_msg')
@endsection
@section('post-script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.servicos').click(function(){

            let load = $(".ajax_load");
            const view = $(this).data("registro");
            const url = "consulta_processo/" + view;

            $.ajax({
                url: url,
                method: 'GET',
                timeout: 20000,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    load.fadeOut(200);
                    alert('Favor tentar novamente!');
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        $('.modal-body>p').html(response.error);
                        $("#divida-modal").modal('show');
                        return false;
                    } else {
                        $("#conteudo").html(response);
                        efeito_ancora('#processos_abertos');
                    }

                }
            });
        });

        $('#carousel-example').on('slide.bs.carousel', function (e) {
            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 5;
            var totalItems = $('.carousel-item').length;

            if (idx >= totalItems-(itemsPerSlide-1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i=0; i<it; i++) {
                    // append slides to end
                    if (e.direction=="left") {
                        $('.carousel-item').eq(i).appendTo('.carousel-inner');
                    }
                    else {
                        $('.carousel-item').eq(0).appendTo('.carousel-inner');
                    }
                }
            }
        });
    });
</script>
@endsection
