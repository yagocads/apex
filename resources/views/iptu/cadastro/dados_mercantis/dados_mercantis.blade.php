<div class="row">
    <div class="col-lg-12">
        <h5><i class="fa fa-home mt-3 mb-3"></i> Dados Mercantis:</h5>

        <section id="camposCorretor" class="d-none">
            
            <div class="form-row">
                <div class="col-lg-6 form-group">
                    <label for="cresci">CRESCI:</label> <span class="asterisc">*</span>
                    <input type="text" name="cresci" id="cresci" 
                        class="form-control form-control-sm {{ ($errors->has('cresci') ? 'is-invalid' : '') }}" 
                        value=""
                        required />

                        @if ($errors->has('cresci'))
                            <div class="invalid-feedback">
                                {{ $errors->first('cresci') }}
                            </div>
                        @endif
                </div>
            </div>
        </section>

        <section id="camposEmpresa" class="d-none">
            <div class="form-row">
                <div class="col-lg-6 form-group">
                    <label for="cnpj">CNPJ:</label> <span class="asterisc">*</span>
                    <input type="text" name="cnpj" id="cnpj" 
                        class="form-control form-control-sm mask-cnpj {{ ($errors->has('cnpj') ? 'is-invalid' : '') }}" 
                        value=""
                        required />

                        @if ($errors->has('cnpj'))
                            <div class="invalid-feedback">
                                {{ $errors->first('cnpj') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-12 form-group">
                    <label for="razao_social">Razão Social:</label>  <span class="asterisc">*</span>
                    <input type="text" name="razao_social" id="razao_social" 
                        class="form-control form-control-sm {{ ($errors->has('razao_social') ? 'is-invalid' : '') }}" 
                        value="{{ old('razao_social') }}" maxlength="100" 
                        required />

                        @if ($errors->has('razao_social'))
                            <div class="invalid-feedback">
                                {{ $errors->first('razao_social') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-12 form-group">
                    <label for="nome_fantasia">Nome Fantasia:</label>
                    <input type="text" name="nome_fantasia" id="nome_fantasia" 
                        class="form-control form-control-sm {{ ($errors->has('nome_fantasia') ? 'is-invalid' : '') }}" 
                        value="{{ old('nome_fantasia') }}" maxlength="100" />

                        @if ($errors->has('nome_fantasia'))
                            <div class="invalid-feedback">
                                {{ $errors->first('nome_fantasia') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-12 form-group">
                    <label for="natureza_juridica">Natureza Jurídica:</label>
                    <input type="text" name="natureza_juridica" id="natureza_juridica" 
                        class="form-control form-control-sm {{ ($errors->has('natureza_juridica') ? 'is-invalid' : '') }}"
                        value="{{ old('natureza_juridica') }}" maxlength="40" />

                        @if ($errors->has('natureza_juridica'))
                            <div class="invalid-feedback">
                                {{ $errors->first('natureza_juridica') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-6 form-group">
                    <label for="data_abertura_empresa">Data de Abertura:</label>
                    <input type="date" name="data_abertura_empresa" id="data_abertura_empresa" 
                        class="form-control form-control-sm {{ ($errors->has('data_abertura_empresa') ? 'is-invalid' : '') }}" 
                        value="{{ old('data_abertura_empresa') }}"/>

                        @if ($errors->has('data_abertura_empresa'))
                            <div class="invalid-feedback">
                                {{ $errors->first('data_abertura_empresa') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-6 form-group">
                    <label for="inscricao_estadual">Inscrição Estadual:</label>
                    <input type="text" name="inscricao_estadual" id="inscricao_estadual" 
                        class="form-control form-control-sm mask-inscricao_estadual {{ ($errors->has('inscricao_estadual') ? 'is-invalid' : '') }}" 
                        value="{{ old('inscricao_estadual') }}" />

                        @if ($errors->has('inscricao_estadual'))
                            <div class="invalid-feedback">
                                {{ $errors->first('inscricao_estadual') }}
                            </div>
                        @endif
                </div>
            </div>
        </section>
    </div>
    
</div>