<div id="informacoes_culturais_accordion">

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-area-cultural">
            <div class="card-header"> 
                Área Cultural de Atuação: <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-area-cultural" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-group">
                    <label for="area_cultural_principal">Escolha a sua área principal:</label>
                    <select name="area_cultural_principal" id="area_cultural_principal"
                        class="form-control form-control-sm col-lg-4 {{ ($errors->has('area_cultural_principal') ? 'is-invalid' : '') }}">
                        <option></option>

                        @foreach($areaCulturalAtuacao as $area)
                            @if ($area != "Outro")
                                <option {{ old('area_cultural_principal') == $area ? 'selected' : '' }}>
                                    {{ $area }}
                                </option>
                            @endif
                        @endforeach
                    </select>

                    @if ($errors->has('area_cultural_principal'))
                        <div class="invalid-feedback">
                            {{ $errors->first('area_cultural_principal') }}
                        </div>
                    @endif
                </div>

                <p>Escolha as áreas que você atua:</p>

                <div class="form-check">
                    <input type="hidden" name="area_cultural" 
                        class="form-check-input {{ ($errors->has('area_cultural') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('area_cultural'))
                        <div class="invalid-feedback">
                            {{ $errors->first('area_cultural') }}
                        </div>
                    @endif
                </div>

                @foreach($areaCulturalAtuacao as $area)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="area_cultural[]" class="form-check-input" 
                                value="{{ $area }}" 
                                {{ (is_array(old('area_cultural')) && in_array($area, old('area_cultural'))) ? 'checked' : '' }}>
                            {{ $area }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-atuacao-cultural">
            <div class="card-header"> 
                Atuação Cultural: <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-atuacao-cultural" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <input type="hidden" name="atuacao_cultural" 
                        class="form-check-input {{ ($errors->has('atuacao_cultural') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('atuacao_cultural'))
                        <div class="invalid-feedback">
                            {{ $errors->first('atuacao_cultural') }}
                        </div>
                    @endif
                </div>

                @foreach($atuacaoCultural as $atuacao)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="atuacao_cultural[]" class="form-check-input" 
                                value="{{ $atuacao }}" 
                                {{ (is_array(old('atuacao_cultural')) && in_array($atuacao, old('atuacao_cultural'))) ? 'checked' : '' }}>
                            {{ $atuacao }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-descricao-atividades">
            <div class="card-header"> 
                Descreva, como em um release, as atividades artisticas/culturais desenvolvidas
                por você. <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-descricao-atividades" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                <textarea name="descricao_atividades" 
                    class="form-control form-control-sm {{ ($errors->has('descricao_atividades') ? 'is-invalid' : '') }}" 
                    placeholder="Sua resposta" 
                    cols="5" rows="6" 
                    maxlength="1200">{{ old('descricao_atividades') }}</textarea>

                    @if ($errors->has('descricao_atividades'))
                        <div class="invalid-feedback">
                            {{ $errors->first('descricao_atividades') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-area-artistica-cultural">
            <div class="card-header"> 
                Possui formação na área Artística/Cultural: <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-area-artistica-cultural" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <input type="hidden" name="formacao_area_artistica_cultural" 
                        class="form-check-input {{ ($errors->has('formacao_area_artistica_cultural') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('formacao_area_artistica_cultural'))
                        <div class="invalid-feedback">
                            {{ $errors->first('formacao_area_artistica_cultural') }}
                        </div>
                    @endif
                </div>

                @foreach($areaArtisticaCultural as $area)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="formacao_area_artistica_cultural[]" class="form-check-input" 
                                value="{{ $area }}"
                                {{ (is_array(old('formacao_area_artistica_cultural')) && in_array($area, old('formacao_area_artistica_cultural'))) ? 'checked' : '' }}>
                            {{ $area }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-registro-profissional">
            <div class="card-header"> 
                Possui Registro Profissional <small>(Registro como ator, músico etc.)</small>? <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-registro-profissional" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="registro_profissional" value="sim"
                        {{ old('registro_profissional') == 'sim' ? 'checked' : ''}}>
                        Sim
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="registro_profissional" value="nao"
                        {{ old('registro_profissional') == 'nao' ? 'checked' : ''}}>
                        Não
                    </label>
                </div>

                <div class="form-check">
                    <input type="hidden" name="registro_profissional" 
                        class="form-check-input {{ ($errors->has('registro_profissional') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('registro_profissional'))
                        <div class="invalid-feedback">
                            {{ $errors->first('registro_profissional') }}
                        </div>
                    @endif
                </div>

                <div class="nome_registros_profissionais {{ old('registro_profissional') == null || old('registro_profissional') == 'nao' ? 'd-none' : '' }} mt-3">
                    <p>Qual o/os registro(s) você tem? <span class="text-danger">*</span></p>
                    
                    <div class="row">
                        <div class="col-lg-6">
                            <textarea class="form-control form-control-sm {{ ($errors->has('registro_profissional_descricao') ? 'is-invalid' : '') }}" 
                            placeholder="Sua resposta" cols="" rows="6" maxlength="255"
                            name="registro_profissional_descricao">{{ old('registro_profissional_descricao') }}</textarea>

                            @if ($errors->has('registro_profissional_descricao'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('registro_profissional_descricao') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (mb_strlen(remove_character_document($cpfOuCnpj)) == 11)
        <div class="card mb-3">
            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-espaco-cultural-coletivo">
                <div class="card-header"> 
                    Você representa um espaço cultural/coletivo? <span class="text-danger">*</span>
                </div>
            </a>
            <div id="card-espaco-cultural-coletivo" class="collapse" data-parent="#informacoes_culturais_accordion">
                <div class="card-body">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="espaco_cultural_coletivo" value="sim"
                            {{ old('espaco_cultural_coletivo') == 'sim' ? 'checked' : ''}}>
                            Sim
                        </label>
                    </div>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="espaco_cultural_coletivo" value="nao"
                            {{ old('espaco_cultural_coletivo') == 'nao' ? 'checked' : ''}}>
                            Não
                        </label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="espaco_cultural_coletivo" 
                            class="form-check-input {{ ($errors->has('espaco_cultural_coletivo') ? 'is-invalid' : '') }}"
                            disabled />
            
                        @if ($errors->has('espaco_cultural_coletivo'))
                            <div class="invalid-feedback">
                                {{ $errors->first('espaco_cultural_coletivo') }}
                            </div>
                        @endif
                    </div>

                    <div class="espaco_cultural_coletivo_prosseguimento {{ old('espaco_cultural_coletivo') == null || old('espaco_cultural_coletivo') == 'nao' ? 'd-none' : '' }} mt-3">
                        <p>Informe o número do DOC: <span class="text-danger">*</span></p>
                        <input type="text" class="form-control form-control-sm col-lg-3 {{ ($errors->has('numero_doc_espaco_cultural_coletivo') ? 'is-invalid' : '') }}" 
                            placeholder="Digite o número" maxlength="15"
                            name="numero_doc_espaco_cultural_coletivo"
                            value="{{ old('numero_doc_espaco_cultural_coletivo') }}" />

                        @if ($errors->has('numero_doc_espaco_cultural_coletivo'))
                            <div class="invalid-feedback">
                                {{ $errors->first('numero_doc_espaco_cultural_coletivo') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-tempo-atuacao-area">
            <div class="card-header"> 
                Tempo de atuação na área? <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-tempo-atuacao-area" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                
                <div class="form-check">
                    <input type="hidden" name="tempo_atuacao_area" 
                        class="form-check-input {{ ($errors->has('tempo_atuacao_area') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('tempo_atuacao_area'))
                        <div class="invalid-feedback">
                            {{ $errors->first('tempo_atuacao_area') }}
                        </div>
                    @endif
                </div>

                @foreach($tempoAtuacaoArea as $tempo)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="tempo_atuacao_area" value="{{ $tempo }}"
                                {{ old('tempo_atuacao_area') == $tempo ? 'checked' : ''}}>
                            {{ $tempo }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-alcance-pessoas">
            <div class="card-header"> 
                Quantas pessoas você alcança aproximadamente com o seu trabalho direta ou indiretamente? 
                <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-alcance-pessoas" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <input type="hidden" name="alcance_pessoas" 
                        class="form-check-input {{ ($errors->has('alcance_pessoas') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('alcance_pessoas'))
                        <div class="invalid-feedback">
                            {{ $errors->first('alcance_pessoas') }}
                        </div>
                    @endif
                </div>

                @foreach($alcancePessoas as $alcance)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="alcance_pessoas" value="{{ $alcance }}"
                                {{ old('alcance_pessoas') == $alcance ? 'checked' : ''}}>
                            {{ $alcance }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-frequencia-atividades-artisticas">
            <div class="card-header"> 
                Com que frequência você realiza as suas atividades artísticas?
                <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-frequencia-atividades-artisticas" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <input type="hidden" name="frequencia_atividade" 
                        class="form-check-input {{ ($errors->has('frequencia_atividade') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('frequencia_atividade'))
                        <div class="invalid-feedback">
                            {{ $errors->first('frequencia_atividade') }}
                        </div>
                    @endif
                </div>

                @foreach($frequenciaAtividadesArtisticas as $frequenciaAtividade)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="frequencia_atividade" value="{{ $frequenciaAtividade }}"
                                {{ old('frequencia_atividade') == $frequenciaAtividade ? 'checked' : ''}}>
                            {{ $frequenciaAtividade }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-insercao-atividade-artistico-cultural">
            <div class="card-header"> 
                Formas de inserção da atividade artístico-cultural (Marque até 5 opções): <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-insercao-atividade-artistico-cultural" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <input type="hidden" name="insercao_atividade_artistico_cultural" 
                        class="form-check-input {{ ($errors->has('insercao_atividade_artistico_cultural') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('insercao_atividade_artistico_cultural'))
                        <div class="invalid-feedback">
                            {{ $errors->first('insercao_atividade_artistico_cultural') }}
                        </div>
                    @endif
                </div>

                @foreach($insercaoAtividadeArtisticoCultural as $atividadeArtisticoCultural)
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="insercao_atividade_artistico_cultural[]" class="form-check-input insercao_atividade_artistico_cultural" 
                                value="{{ $atividadeArtisticoCultural }}"
                                {{ (is_array(old('insercao_atividade_artistico_cultural')) && in_array($atividadeArtisticoCultural, old('insercao_atividade_artistico_cultural'))) ? 'checked' : '' }}>
                            {{ $atividadeArtisticoCultural }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-fonte-renda">
            <div class="card-header"> 
                Vive exclusivamente da sua atividade artística/cultural, ou 
                possui outra fonte de renda? <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-fonte-renda" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="fonte_renda" 
                        value="Vivo exclusivamente da minha atividade artística/cultural"
                        {{ old('fonte_renda') == 'Vivo exclusivamente da minha atividade artística/cultural' ? 'checked' : '' }}>
                        Vivo exclusivamente da minha atividade artística/cultural
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="fonte_renda" 
                    value="Possuo outra fonte de renda"
                    {{ old('fonte_renda') == 'Possuo outra fonte de renda' ? 'checked' : '' }}>
                    Possuo outra fonte de renda
                    </label>
                </div>

                <div class="form-check">
                    <input type="hidden" name="fonte_renda" 
                        class="form-check-input {{ ($errors->has('fonte_renda') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('fonte_renda'))
                        <div class="invalid-feedback">
                            {{ $errors->first('fonte_renda') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-pandemia-covid">
            <div class="card-header"> 
                Você executa alguma atividade cultural que foi interrompida devido a pandemia do Covid-19? <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-pandemia-covid" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" 
                            name="pandemia_covid" value="sim"
                            {{ old('pandemia_covid') == 'sim' ? 'checked' : '' }}>
                        Sim
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                    <input type="radio" class="form-check-input" 
                        name="pandemia_covid" value="nao"
                        {{ old('pandemia_covid') == 'nao' ? 'checked' : '' }}>
                    Não
                    </label>
                </div>

                <div class="form-check">
                    <input type="hidden" name="pandemia_covid" 
                        class="form-check-input {{ ($errors->has('pandemia_covid') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('pandemia_covid'))
                        <div class="invalid-feedback">
                            {{ $errors->first('pandemia_covid') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-auxilio-governo">
            <div class="card-header"> 
                Você recebe algum auxílio do Governo? <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-auxilio-governo" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" 
                            name="recebe_auxilio_governo" value="sim"
                            {{ old('recebe_auxilio_governo') == 'sim' ? 'checked' : '' }}>
                        Sim
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                    <input type="radio" class="form-check-input" 
                        name="recebe_auxilio_governo" value="nao"
                        {{ old('recebe_auxilio_governo') == 'nao' ? 'checked' : '' }}>
                    Não
                    </label>
                </div>

                <div class="form-check">
                    <input type="hidden" name="recebe_auxilio_governo" 
                        class="form-check-input {{ ($errors->has('recebe_auxilio_governo') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('recebe_auxilio_governo'))
                        <div class="invalid-feedback">
                            {{ $errors->first('recebe_auxilio_governo') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-emprego-formal-ativo">
            <div class="card-header"> 
                Atualmente você possui emprego formal ativo? <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-emprego-formal-ativo" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" 
                            name="emprego_formal_ativo" value="sim"
                            {{ old('emprego_formal_ativo') == 'sim' ? 'checked' : '' }}>
                        Sim
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                    <input type="radio" class="form-check-input" 
                        name="emprego_formal_ativo" value="nao"
                        {{ old('emprego_formal_ativo') == 'nao' ? 'checked' : '' }}>
                    Não
                    </label>
                </div>

                <div class="form-check">
                    <input type="hidden" name="emprego_formal_ativo" 
                        class="form-check-input {{ ($errors->has('emprego_formal_ativo') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('emprego_formal_ativo'))
                        <div class="invalid-feedback">
                            {{ $errors->first('emprego_formal_ativo') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-anexos">
            <div class="card-header"> 
                <i class="fa fa-paperclip"></i> Anexos
            </div>
        </a>
        <div id="card-anexos" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
               <label for="portfolio">Portfólio: <span class="text-danger">*</span></label>
                <div class="alert alert-info">
                   Anexe aqui o portfólio que comprove atuação na área artística/cultural há pelo menos 
                   24(vinte e quatro) meses. Podem ser fotos, reportagens, prints de redes sociais que
                   conste datas, currículo, certificados, etc. Importante o informe de datas na 
                   documentação enviada. (Somente PDF, PNG e JPG)
                </div>

                <div class="form-row">
                    <div class="col-lg-6 form-group">
                        <label for="descricao_portfolio">Descrição do anexo:</label>
                        <textarea name="descricao_portfolio" 
                            maxlength="40"
                            class="form-control form-control-sm" 
                            id="descricao_portfolio" rows="3" cols="2"></textarea>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-6 form-group">
                        <input type="file" name="portfolio" id="portfolio" class="d-block" />
                    </div>
                </div>

                <button class="btn btn-primary btn-enviar-documento-informacoes-culturais" 
                    data-nome_input="portfolio"
                    data-nome_descricao_input="descricao_portfolio"
                    data-nome_sessao="documentos_informacoes_culturais_portfolio"
                    data-conteudo_documentos="conteudo_documentos_informacoes_culturais_portfolio"
                    data-id_tabela="documentos_portfolio"
                    data-nome_tabela="tabela_documentos_portfolio"
                    data-pasta="informacoes_culturais">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>

                <hr/>

                <div class="row">
                    <div class="col-lg-12 form-group">
                        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>
                
                        <div class="conteudo_documentos_informacoes_culturais_portfolio">
                            @include('cadastro.cgm.informacoes_culturais.tabela_documentos_portfolio')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-redes-sociais-divulgacao">
            <div class="card-header"> 
                Informe suas redes sociais de divulgação: <span class="text-danger">*</span>
                <p>
                    <small>
                        Ex: Site: https://www.artista.com.br | Facebook: https://www.facebook.com/artista | 
                        Instagram: @artista | Twitter: @artista, etc...
                    </small>
                </p>
            </div>
        </a>
        <div id="card-redes-sociais-divulgacao" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                <textarea name="redes_sociais_divulgacao" 
                    class="form-control form-control-sm {{ ($errors->has('redes_sociais_divulgacao') ? 'is-invalid' : '') }}" 
                    placeholder="Sua resposta" cols="5" rows="6" maxlength="255">{{ old('redes_sociais_divulgacao') }}</textarea>

                    @if ($errors->has('redes_sociais_divulgacao'))
                        <div class="invalid-feedback">
                            {{ $errors->first('redes_sociais_divulgacao') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-lei-emergencia-cultural">
            <div class="card-header"> 
                Lei de Emergência Cultural Aldir Blanc: <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-lei-emergencia-cultural" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                <div class="alert alert-info">
                    <p>
                        <i class="fa fa-volume-up"></i> 
                        Lei nº 14.017/2020, de 29 de junho de 2020 - Lei de Emergência Cultural Aldir Blanc
                        para expediente de execução da referida Lei, está previsto o repasse da União para os Estados, 
                        ao Distrito Federal e aos Municípios, em parcela única, no exercício de 2020, o valor de 
                        R$ 3.000.000.000,00 (três bilhões de reais) para aplicação, pelos Poderes Executivos locais, 
                        em ações emergenciais de apoio ao setor cultural por meio de:
                    </p>

                    I - renda emergencial mensal aos trabalhadores e trabalhadoras da cultura;<br>
                    
                    II - subsídios mensais para manutenção de espaços artísticos e culturais, micro e pequenas 
                    empresas culturais, cooperativas, instituições e organizações culturais comunitárias que 
                    tiveram as suas atividades interrompidas por força das medidas de isolamento social;<br>
                    
                    III - editais, chamadas públicas, prêmios, aquisição de bens e serviços vinculados ao setor 
                    cultural e outros instrumentos voltados à manutenção de agentes, espaços, iniciativas, cursos, 
                    produções, desenvolvimento de atividades de economia criativa e economia solidária, produções audiovisuais, 
                    manifestações culturais, bem como para a realização de 2 atividades artísticas e culturais que possam ser 
                    transmitidas pela internet ou disponibilizadas por meio de redes sociais e outras 
                    plataformas digitais.
                </div>

                <p>
                    Caso haja interesse em prosseguir para o cadastro para recebimento dos
                    benefícios previstos na Lei de Emergência Cultural Aldir Blanc, clique em 
                    Prosseguir: <span class="text-danger">*</span>
                </p>

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" 
                            name="lei_emergencia_cultural" value="sim"
                            {{ old('lei_emergencia_cultural') == 'sim' ? 'checked' : '' }}>
                        Prosseguir
                    </label>
                </div>

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" 
                            name="lei_emergencia_cultural" value="nao"
                            {{ old('lei_emergencia_cultural') == 'nao' ? 'checked' : '' }}>
                        Não tenho interesse, quero encerrar o cadastro.
                    </label>
                </div>

                <div class="form-check">
                    <input type="hidden" name="lei_emergencia_cultural" 
                        class="form-check-input {{ ($errors->has('lei_emergencia_cultural') ? 'is-invalid' : '') }}"
                        disabled />
        
                    @if ($errors->has('lei_emergencia_cultural'))
                        <div class="invalid-feedback">
                            {{ $errors->first('lei_emergencia_cultural') }}
                        </div>
                    @endif
                </div>

                <!-- Prosseguimento do cadastro -->
                <div class="lei_emergencia_cultural_prosseguimento mt-3 {{ old('lei_emergencia_cultural') == null || old('lei_emergencia_cultural') == 'nao' ? 'd-none' : '' }}">
                    
                    <hr/>

                    <div class="alert alert-info">
                        <p><i class="fa fa-volume-up"></i> Lei de Emergência Cultural - Condições para recebimento do subsídio mensal.</p>

                        <p>
                            Farão jus ao benefício de subsídio mensal os espaços culturais e artísticos, 
                            micro e pequenas empresas culturais, organizações culturais comunitárias, 
                            cooperativas e instituições culturais com atividades interrompidas, 
                            devendo comprovar sua inscrição e respectiva homologação em, pelo menos, 
                            um dos seguintes cadastros:
                        </p>

                        I - Cadastros Estaduais de Cultura;<br>
                        II - Cadastros Municipais de Cultura;<br>
                        III - Cadastro Distrital de Cultura;<br>
                        IV - Cadastro Nacional de Pontos e Pontões de Cultura;<br>
                        V - Cadastros Estaduais de Pontos e Pontões de Cultura;<br>
                        VI - Sistema Nacional de Informações e Indicadores Culturais (Sniic);<br>
                        VII - Sistema de Informações Cadastrais do Artesanato Brasileiro (Sicab);<br>
                        VIII - Outros cadastros referentes a atividades culturais existentes na Unidade da 
                        Federação, bem como projetos culturais apoiados nos termos da Lei n° 8.313, de 23 de 
                        dezembro de 1991, nos 24 (vinte e quatro) meses imediatamente anteriores à data de 
                        publicação desta Lei.
                    </div>

                    <p>
                        Declaro que li e tenho ciência que me enquadro em todas as 
                        condições acima: <span class="text-danger">*</span>
                    </p>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="enquadramento_lei_emergencial" value="sim"
                                {{ old('enquadramento_lei_emergencial') == 'sim' ? 'checked' : '' }}>
                            Sim
                        </label>
                    </div>
    
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="enquadramento_lei_emergencial" value="nao"
                                {{ old('enquadramento_lei_emergencial') == 'nao' ? 'checked' : '' }}>
                            Não
                        </label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="enquadramento_lei_emergencial" 
                            class="form-check-input {{ ($errors->has('enquadramento_lei_emergencial') ? 'is-invalid' : '') }}"
                            disabled />
            
                        @if ($errors->has('enquadramento_lei_emergencial'))
                            <div class="invalid-feedback">
                                {{ $errors->first('enquadramento_lei_emergencial') }}
                            </div>
                        @endif
                    </div>

                    <hr/>

                    <p class="mt-3">
                        Declaro ter ciência que o preenchimento deste formulário NÃO me garante 
                        os benefícios previstos na Lei Nº 14.017/2020. <span class="text-danger">*</span>
                    </p>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="ciente_que_lei_nao_garante_beneficio" value="sim"
                                {{ old('ciente_que_lei_nao_garante_beneficio') == 'sim' ? 'checked' : '' }}>
                            Sim
                        </label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="ciente_que_lei_nao_garante_beneficio" 
                            class="form-check-input {{ ($errors->has('ciente_que_lei_nao_garante_beneficio') ? 'is-invalid' : '') }}"
                            disabled />
            
                        @if ($errors->has('ciente_que_lei_nao_garante_beneficio'))
                            <div class="invalid-feedback">
                                {{ $errors->first('ciente_que_lei_nao_garante_beneficio') }}
                            </div>
                        @endif
                    </div>

                    <hr/>

                    <p class="mt-3">
                        Você é titular de benefício previdênciário ou assistencial ou beneficiário 
                        do seguro-desemprego ou de programa de transferência de renda federal
                        ressalvado o Bolsa Família? <span class="text-danger">*</span>
                    </p>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="beneficio_previdenciario" value="sim"
                                {{ old('beneficio_previdenciario') == 'sim' ? 'checked' : '' }}>
                            Sim
                        </label>
                    </div>
    
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="beneficio_previdenciario" value="nao"
                                {{ old('beneficio_previdenciario') == 'nao' ? 'checked' : '' }}>
                            Não
                        </label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="beneficio_previdenciario" 
                            class="form-check-input {{ ($errors->has('beneficio_previdenciario') ? 'is-invalid' : '') }}"
                            disabled />
            
                        @if ($errors->has('beneficio_previdenciario'))
                            <div class="invalid-feedback">
                                {{ $errors->first('beneficio_previdenciario') }}
                            </div>
                        @endif
                    </div>

                    <div class="beneficio_previdenciario_prosseguimento mt-3 {{ old('beneficio_previdenciario') == null || old('beneficio_previdenciario') == 'nao' ? 'd-none' : '' }}">
                        <p>Informe os beneficio(s):</p>
                        <textarea name="nomes_beneficios_previdenciarios" 
                            class="form-control form-control-sm {{ ($errors->has('nomes_beneficios_previdenciarios') ? 'is-invalid' : '') }}" 
                            placeholder="Sua resposta" cols="5" rows="6" maxlength="255">{{ old('nomes_beneficios_previdenciarios') }}</textarea>

                            @if ($errors->has('nomes_beneficios_previdenciarios'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('nomes_beneficios_previdenciarios') }}
                                </div>
                            @endif
                    </div>

                    <hr/>

                    <p class="mt-3">
                        Você é/foi beneficiário do auxílio emergêncial previsto pela
                        Lei Nº 13.982, de 2 de abril de 2020? <span class="text-danger">*</span>
                    </p>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="beneficio_emergencial_lei_13982" value="sim"
                                {{ old('beneficio_emergencial_lei_13982') == 'sim' ? 'checked' : '' }}>
                            Sim
                        </label>
                    </div>
    
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="beneficio_emergencial_lei_13982" value="nao"
                                {{ old('beneficio_emergencial_lei_13982') == 'nao' ? 'checked' : '' }}>
                            Não
                        </label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="beneficio_emergencial_lei_13982" 
                            class="form-check-input {{ ($errors->has('beneficio_emergencial_lei_13982') ? 'is-invalid' : '') }}"
                            disabled />
            
                        @if ($errors->has('beneficio_emergencial_lei_13982'))
                            <div class="invalid-feedback">
                                {{ $errors->first('beneficio_emergencial_lei_13982') }}
                            </div>
                        @endif
                    </div>

                    <hr/>

                    <p class="mt-3">
                        A mulher provedora de família monoparental terá direito a 2(duas)
                        cotas da renda emergêncial. Você se enquadra nesse requisito? <span class="text-danger">*</span>
                    </p>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="mulher_familia_monoparental" value="sim"
                                {{ old('mulher_familia_monoparental') == 'sim' ? 'checked' : '' }}>
                            Sim
                        </label>
                    </div>
    
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" 
                                name="mulher_familia_monoparental" value="nao"
                                {{ old('mulher_familia_monoparental') == 'nao' ? 'checked' : '' }}>
                            Não
                        </label>
                    </div>

                    <div class="form-check">
                        <input type="hidden" name="mulher_familia_monoparental" 
                            class="form-check-input {{ ($errors->has('mulher_familia_monoparental') ? 'is-invalid' : '') }}"
                            disabled />
            
                        @if ($errors->has('mulher_familia_monoparental'))
                            <div class="invalid-feedback">
                                {{ $errors->first('mulher_familia_monoparental') }}
                            </div>
                        @endif
                    </div>

                    <div class="mulher_familia_monoparental_prosseguimento mt-3 {{ old('mulher_familia_monoparental') == null || old('mulher_familia_monoparental') == 'nao' ? 'd-none' : '' }}">
                        <p>
                            Anexe uma foto ou xerox da certidão de nascimento ou CPF dos dependentes: 
                            (Somente PDF, PNG e JPG)
                        </p>

                        <div class="form-row">
                            <div class="col-lg-6 form-group">
                                <label for="descricao_mulher_familia_monoparental">Descrição do anexo:</label>
                                <textarea name="descricao_mulher_familia_monoparental" 
                                    class="form-control form-control-sm" 
                                    maxlength="40"
                                    id="descricao_mulher_familia_monoparental" 
                                    rows="3" cols="2"></textarea>
                            </div>
                        </div>
        
                        <div class="form-row">
                            <div class="col-lg-6 form-group">
                                <input type="file" name="documento_mulher_familia_monoparental" 
                                    id="documento_mulher_familia_monoparental" class="d-block" />
                            </div>
                        </div>
        
                        <button class="btn btn-primary btn-enviar-documento-informacoes-culturais" 
                            data-nome_input="documento_mulher_familia_monoparental" 
                            data-nome_descricao_input="descricao_mulher_familia_monoparental"
                            data-nome_sessao="documentos_informacoes_culturais_familia_monoparental"
                            data-conteudo_documentos="conteudo_documentos_informacoes_culturais_familia_monoparental"
                            data-id_tabela="documentos_familia_monoparental"
                            data-nome_tabela="tabela_documentos_familia_monoparental"
                            data-pasta="informacoes_culturais">
                            Enviar Documento <i class="fa fa-send"></i>
                        </button>
        
                        <hr/>
        
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>
                        
                                <div class="conteudo_documentos_informacoes_culturais_familia_monoparental">
                                    @include('cadastro.cgm.informacoes_culturais.tabela_documentos_familia_monoparental')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#card-informacoes-bancarias">
            <div class="card-header"> 
                Informações Bancárias: <span class="text-danger">*</span>
            </div>
        </a>
        <div id="card-informacoes-bancarias" class="collapse" data-parent="#informacoes_culturais_accordion">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-lg-4 form-group">
                        <label for="banco_nome">Nome do Banco:</label>
                        <select name="banco_nome" id="banco_nome" 
                            class="form-control form-control-sm {{ ($errors->has('banco_nome') ? 'is-invalid' : '') }}">
                            <option></option>
                            @include("cadastro.cgm.informacoes_culturais.bancos")
                        </select>

                        @if ($errors->has('banco_nome'))
                            <div class="invalid-feedback">
                                {{ $errors->first('banco_nome') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-row banco_conta_operacao {{ old('banco_nome') == null || old('banco_nome') !== "104 - Caixa Econômica Federal" ? 'd-none' : '' }}">
                    <div class="col-lg-4 form-group">
                        <label for="banco_conta_operacao">Conta Operação:</label>
                        <select name="banco_conta_operacao" id="banco_conta_operacao"
                            class="form-control form-control-sm {{ ($errors->has('banco_conta_operacao') ? 'is-invalid' : '') }}">
                            <option></option>
                            <option value="001" {{ old('banco_conta_operacao') == '001' ? 'selected' : '' }}>001 - Conta Corrente PF</option>
                            <option value="002" {{ old('banco_conta_operacao') == '002' ? 'selected' : '' }}>002 - Conta Simples PF</option>
                            <option value="003" {{ old('banco_conta_operacao') == '003' ? 'selected' : '' }}>003 - Conta Corrente PJ</option>
                            <option value="006" {{ old('banco_conta_operacao') == '006' ? 'selected' : '' }}>006 - Entidades Públicas</option>
                            <option value="007" {{ old('banco_conta_operacao') == '007' ? 'selected' : '' }}>007 - Depósitos Instituições Financeiras</option>
                            <option value="013" {{ old('banco_conta_operacao') == '013' ? 'selected' : '' }}>013 - Poupança PF</option>
                            <option value="022" {{ old('banco_conta_operacao') == '022' ? 'selected' : '' }}>022 - Poupança PJ</option>
                            <option value="023" {{ old('banco_conta_operacao') == '023' ? 'selected' : '' }}>023 - Conta Caixa Fácil</option>
                            <option value="028" {{ old('banco_conta_operacao') == '028' ? 'selected' : '' }}>028 - Poupança Crédito Imobiliário</option>
                            <option value="043" {{ old('banco_conta_operacao') == '043' ? 'selected' : '' }}>043 - Depósitos Lotéricos</option>
                        </select>

                        @if ($errors->has('banco_conta_operacao'))
                            <div class="invalid-feedback">
                                {{ $errors->first('banco_conta_operacao') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-4 form-group">
                        <label for="banco_tipo_conta">Tipo de Conta:</label>
                        <select name="banco_tipo_conta" id="banco_tipo_conta" 
                            class="form-control form-control-sm {{ ($errors->has('banco_tipo_conta') ? 'is-invalid' : '') }}">
                            <option></option>
                            <option {{ old('banco_tipo_conta') == 'Conta Corrente' ? 'selected' : '' }}>Conta Corrente</option>
                            <option {{ old('banco_tipo_conta') == 'Poupança' ? 'selected' : '' }}>Poupança</option>
                        </select>

                        @if ($errors->has('banco_tipo_conta'))
                            <div class="invalid-feedback">
                                {{ $errors->first('banco_tipo_conta') }}
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-2 form-group">
                        <label for="banco_agencia">Agência:</label>
                        <input type="text" name="banco_agencia" 
                            id="banco_agencia" 
                            class="form-control form-control-sm mask-agencia {{ ($errors->has('banco_agencia') ? 'is-invalid' : '') }}" 
                            maxlength="10" value="{{ old('banco_agencia') }}" />


                        @if ($errors->has('banco_agencia'))
                            <div class="invalid-feedback">
                                {{ $errors->first('banco_agencia') }}
                            </div>
                        @endif
                    </div>

                    <div class="col-lg-2 form-group">
                        <label for="banco_numero_conta">Número da Conta:</label>
                        <input type="text" name="banco_numero_conta" 
                            id="banco_numero_conta" 
                            class="form-control form-control-sm mask-numero-conta {{ ($errors->has('banco_numero_conta') ? 'is-invalid' : '') }}" 
                            maxlength="10" value="{{ old('banco_numero_conta') }}" />

                        @if ($errors->has('banco_numero_conta'))
                            <div class="invalid-feedback">
                                {{ $errors->first('banco_numero_conta') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
