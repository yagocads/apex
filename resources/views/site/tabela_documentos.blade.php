
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="documentos_processo">
        <thead>
            <tr>
                <th>Processo</th>
                <th>Documento</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_processo"))
                @foreach(session("documentos_processo") as $documento)
                    <tr>
                        <td>{{ $documento->processo }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger" title="Remover Documento" data-documento="{{ $documento->id }}" id="remover_documento">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){

    $("#remover_documento").click(function(e){
        e.preventDefault();

        if (!confirm("Deseja realmente remover o documento?")) {
            return false;
        }
        let load = $(".ajax_load");
        let documento = $(this).data("documento");
        let url = "/remove_doc_sessao/" + documento;

        $.ajax({
            url: url,
            method: 'POST',
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                load.fadeOut(200);

                $("#conteudo_documentos").html(response);
                $('#documentos_processo').DataTable({
                                "stateSave": true,
                                "bDestroy": true,
                                "bPaginate": false,
                                "ordering": true,
                                "info": false,
                                "searching": true,
                                "language": {
                                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                                }
                }).columns.adjust().responsive.recalc();
            }
        });
    });
});
</script>
