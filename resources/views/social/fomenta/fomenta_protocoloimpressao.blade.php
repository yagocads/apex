<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>FOMENTA MARICÁ</title>

        <!--Custon CSS-->
        <link rel="stylesheet" href="{{ getcwd().'/css/certidao.css' }}">

        <!--Favicon-->
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    </head>
    <body>

        <div class="header">
            <div class="div-logo">
                <img src="{{ getcwd().'/img/brasao.png'  }}" class="logo" alt="Prefeitura de Maricá">
            </div>
            <div class="titulo">
                ESTADO DO RIO DE JANEIRO<br>
                PREFEITURA MUNICIPAL DE MARICÁ<br>
                FOMENTA MARICÁ
            </div>
        </div>
            <div>
                <h1 class="titulo-autenticacao">
                    PROTOCOLO DE SOLICITAÇÃO<br>
                    DE CRÉDITO
                </h1>

                <p class="texto-autenticacao">
                    A sua solicitação do crédito FOMENTA MARICÁ foi realizada com sucesso.<br>
                </p>
                <p>
                    As análises documentais somente serão iniciadas no início de setembro de 2020, acompanhe sua solicitação utilizando-se deste protocolo para mais informações dentro do próprio sistema. O Programa Fomenta Maricá MEI Emergencial dará prioridade ao atendimento de Microempreendedores NÃO beneficiados pelo Programa de Amparo ao Trabalhador – PAT e pelo Programa de Amparo ao Emprego – PAE e que sejam mais antigos. A inscrição no Programa não garante que a sua solicitação será aprovada ficando sujeita, além dos itens acima, ao limite de recurso destinado ao Programa Fomenta Maricá Emergencial MEI.
                </p>
                <br>
                <p class="texto-autenticacao">
                    Número do protocolo:<br>
                </p>

                <h2 class="status">{{$protocolo}}</h2>

                <p class="dados-certidao"><b>CNPJ: </b>{{$cnpj}} </p>
                <p class="dados-certidao"><b>Nome Empresarial: </b> {{$razaoSocial}}</p>
                <p class="dados-certidao"><b>Data de Emissão:</b> {{ \Carbon\Carbon::parse()->format('d/m/Y H:i:s') }}</p>
                <br>
                <br>
                <br>
                <p class="data-autenticacao">
                    
                    Maricá, {{ \Carbon\Carbon::parse(date("Y-m-d"))->locale("pt-BR")->format('d/m/Y') }}.
                </p>
            </div>


    </body>
</html>
