<form action="{{ route('buscar', $url_servico) }}" method="POST">
    @csrf
    <div class="form-row">
        <div class="col-lg-2 form-group">
            <label for="matricula">Matrícula:</label>
            <input type="text" class="form-control form-control-sm" name="matricula" id="matricula" value="{{ $_REQUEST['matricula'] ?? '' }}"  />
        </div>

        <div class="col-lg-3 form-group">
            <label for="logradouro">Logradouro:</label>
            <input type="text" name="logradouro" id="logradouro" class="form-control form-control-sm" value="{{ $_REQUEST['logradouro'] ?? '' }}" />
        </div>

        <div class="col-lg-3 form-group">
            <label for="planta">Planta:</label>
        <input type="text" name="planta" id="planta" class="form-control form-control-sm" value="{{ $_REQUEST['planta'] ?? '' }}" />
        </div>

        <div class="col-lg-2 form-group">
            <label for="quadra">Quadra:</label>
            <input type="text" name="quadra" id="quadra" class="form-control form-control-sm" value="{{ $_REQUEST['quadra'] ?? '' }}" />
        </div>

        <div class="col-lg-2 form-group">
            <label for="lote">Lote:</label>
            <input type="text" name="lote" id="lote" class="form-control form-control-sm" value="{{ $_REQUEST['lote'] ?? '' }}" />
        </div>

        <div class="col-lg-4 form-group">
            <button class="btn btn-primary">
                <i class="fa fa-search"></i> Buscar Imóvel
            </button>
        </div>
    </div>
</form>
