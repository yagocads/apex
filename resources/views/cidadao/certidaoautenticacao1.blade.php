@extends('layouts.tema_principal')

@section('content')
<style>
    div.g-recaptcha {
      margin: 0 0;
      width: 304px;
      padding: 0;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-left">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <i class="fa fa-certificate"></i> Verifica Autenticidade das Certidões
                        </h3>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('validaautenticidade2') }}" id="validaFormCertidao">
                            @csrf
                            <div class="form-row justify-content-left">
                                <div class="col-lg-12 form-group">
                                    <div  class="col-lg-5 p-0">
                                        <label for="data_imovel_registro_cartorio">CÓDIGO DE AUTENTICIDADE:</label>
                                        <input type="text" name="codigo" id="codigo" class="form-control form-control-sm" />
                                    </div>
                                </div>
                            </div>
                            <br>
                            {!! $showRecaptcha !!}
                            <div class="row">
								<div class="col-lg-12">
                                    <a href="{{ route('servicos', ['guia' => get_guia_services()==""?"1":get_guia_services(), 'aba' => 'certidoes']) }}">
                                        <button type="button" class="btn btn-primary btn-sm  mt-3">
                                            <i class="fa fa-arrow-left"></i> {{ __('Voltar') }}
                                        </button>
                                    <a>

                                    <button type="button" class="btn btn-sm btn-success mt-3" id="autenticar" disabled>
                                        <i class="fa fa-certificate"></i> {{ __('Verificar Autenticidade') }} 
                                    </button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>


@endsection


@section('post-script')
<script type="text/javascript">

    $("#autenticar").prop('disabled', true);

    jQuery(document).ready(function(){
            
        $("#autenticar").click(function(){
            if( $("#codigo").val() === ""  ){
                alert("Por favor, informe o CÓDIGO DE AUTENTICAÇÃO para verificar.");
                $("#codigo").focus();
            }
            else{
                
                $("#validaFormCertidao").submit();
            }
        });

        jQuery('.g-recaptcha').attr('data-callback', 'onReCaptchaSuccess');
        jQuery('.g-recaptcha').attr('data-expired-callback', 'onReCaptchaTimeOut');
    });
    function onReCaptchaTimeOut(){
        $("#autenticar").prop('disabled', true);
    }
    function onReCaptchaSuccess(){
        $("#autenticar").prop('disabled', false);
    }
</script>
@endsection