<div class="accordion-inner">

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="banco" class="col-md-4 col-form-label text-md-right">
                {{ __('Banco') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span8">
            <select id="banco" class="form-input" name="banco">
                <option value=""></option>
                <option value="Banco ABC Brasil S.A.">Banco ABC Brasil S.A.</option>
                <option value="Banco ABN AMRO S.A.">Banco ABN AMRO S.A.</option>
                <option value="Banco Agibank S.A.">Banco Agibank S.A.</option>
                <option value="Banco Alfa S.A.">Banco Alfa S.A.</option>
                <option value="Banco Alvorada S.A.">Banco Alvorada S.A.</option>
                <option value="Banco Andbank (Brasil) S.A.">Banco Andbank (Brasil) S.A.</option>
                <option value="Banco B3 S.A.">Banco B3 S.A.</option>
                <option value="Banco BANDEPE S.A.">Banco BANDEPE S.A.</option>
                <option value="Banco BMG S.A.">Banco BMG S.A.</option>
                <option value="Banco BNP Paribas Brasil S.A.">Banco BNP Paribas Brasil S.A.</option>
                <option value="Banco BOCOM BBM S.A.">Banco BOCOM BBM S.A.</option>
                <option value="Banco Bradescard S.A.">Banco Bradescard S.A.</option>
                <option value="Banco Bradesco BBI S.A.">Banco Bradesco BBI S.A.</option>
                <option value="Banco Bradesco Cartões S.A.">Banco Bradesco Cartões S.A.</option>
                <option value="Banco Bradesco Financiamentos S.A.">Banco Bradesco Financiamentos S.A.</option>
                <option value="Banco Bradesco S.A.">Banco Bradesco S.A.</option>
                <option value="Banco BS2 S.A.">Banco BS2 S.A.</option>
                <option value="Banco BTG Pactual S.A.">Banco BTG Pactual S.A.</option>
                <option value="Banco C6 S.A.">Banco C6 S.A.</option>
                <option value="Banco Caixa Geral - Brasil S.A.">Banco Caixa Geral - Brasil S.A.</option>
                <option value="Banco Cargill S.A">Banco Cargill S.A.</option>
                <option value="Banco Caterpillar S.A.">Banco Caterpillar S.A.</option>
                <option value="Banco Cetelem S.A.">Banco Cetelem S.A.</option>
                <option value="Banco Cifra S.A.">Banco Cifra S.A.</option>
                <option value="Banco Citibank S.A.">Banco Citibank S.A.</option>
                <option value="Banco CNH Industrial Capital S.A.">Banco CNH Industrial Capital S.A.</option>
                <option value="Banco Cooperativo do Brasil S.A. - BANCOOB">Banco Cooperativo do Brasil S.A. - BANCOOB</option>
                <option value="Banco Cooperativo Sicredi S.A.">Banco Cooperativo Sicredi S.A.</option>
                <option value="Banco Credit Agricole Brasil S.A.">Banco Credit Agricole Brasil S.A.</option>
                <option value="Banco Credit Suisse (Brasil) S.A.">Banco Credit Suisse (Brasil) S.A.</option>
                <option value="Banco CSF S.A.">Banco CSF S.A.</option>
                <option value="Banco da Amazônia S.A.">Banco da Amazônia S.A.</option>
                <option value="Banco da China Brasil S.A.">Banco da China Brasil S.A.</option>
                <option value="Banco Daycoval S.A.">Banco Daycoval S.A.</option>
                <option value="Banco de Lage Landen Brasil S.A.">Banco de Lage Landen Brasil S.A.</option>
                <option value="Banco Digio S.A.">Banco Digio S.A.</option>
                <option value="Banco do Brasil S.A.">Banco do Brasil S.A.</option>
                <option value="Banco do Estado de Sergipe S.A.">Banco do Estado de Sergipe S.A.</option>
                <option value="Banco do Estado do Pará S.A.">Banco do Estado do Pará S.A.</option>
                <option value="Banco do Estado do Rio Grande do Sul S.A.">Banco do Estado do Rio Grande do Sul S.A.</option>
                <option value="Banco do Nordeste do Brasil S.A.">Banco do Nordeste do Brasil S.A.</option>
                <option value="Banco Fator S.A.">Banco Fator S.A.</option>
                <option value="Banco Fibra S.A.">Banco Fibra S.A.</option>
                <option value="Banco Ficsa S.A.">Banco Ficsa S.A.</option>
                <option value="Banco Fidis S.A.">Banco Fidis S.A.</option>
                <option value="Banco Finaxis S.A.">Banco Finaxis S.A.</option>
                <option value="Banco Ford S.A.">Banco Ford S.A.</option>
                <option value="Banco GMAC S.A.">Banco GMAC S.A.</option>
                <option value="Banco Guanabara S.A.">Banco Guanabara S.A.</option>
                <option value="Banco Honda S.A.">Banco Honda S.A.</option>
                <option value="Banco IBM S.A.">Banco IBM S.A.</option>
                <option value="Banco Inbursa S.A.">Banco Inbursa S.A.</option>
                <option value="Banco Industrial do Brasil S.A.">Banco Industrial do Brasil S.A.</option>
                <option value="Banco Indusval S.A.">Banco Indusval S.A.</option>
                <option value="Banco Inter S.A.">Banco Inter S.A.</option>
                <option value="Banco Investcred Unibanco S.A.">Banco Investcred Unibanco S.A.</option>
                <option value="Banco Itaú BBA S.A.">Banco Itaú BBA S.A.</option>
                <option value="Banco Itaú Consignado S.A.">Banco Itaú Consignado S.A.</option>
                <option value="Banco Itaú Veículos S.A.">Banco Itaú Veículos S.A.</option>
                <option value="Banco ItauBank S.A">Banco ItauBank S.A</option>
                <option value="Banco Itaucard S.A.">Banco Itaucard S.A.</option>
                <option value="Banco J. P. Morgan S.A.">Banco J. P. Morgan S.A.</option>
                <option value="Banco J. Safra S.A.">Banco J. Safra S.A.</option>
                <option value="Banco John Deere S.A.">Banco John Deere S.A.</option>
                <option value="Banco Luso Brasileiro S.A.">Banco Luso Brasileiro S.A.</option>
                <option value="Banco Mercantil do Brasil S.A.">Banco Mercantil do Brasil S.A.</option>
                <option value="Banco Mizuho do Brasil S.A.">Banco Mizuho do Brasil S.A.</option>
                <option value="Banco Modal S.A.">Banco Modal S.A.</option>
                <option value="Banco Moneo S.A.">Banco Moneo S.A.</option>
                <option value="Banco MUFG Brasil S.A.">Banco MUFG Brasil S.A.</option>
                <option value="Banco Olé Bonsucesso Consignado S.A.">Banco Olé Bonsucesso Consignado S.A.</option>
                <option value="Banco Original S.A.">Banco Original S.A.</option>
                <option value="Banco PAN S.A.">Banco PAN S.A.</option>
                <option value="Banco Paulista S.A.">Banco Paulista S.A.</option>
                <option value="Banco Pine S.A.">Banco Pine S.A.</option>
                <option value="Banco Rabobank International Brasil S.A.">Banco Rabobank International Brasil S.A.</option>
                <option value="Banco RCI Brasil S.A.">Banco RCI Brasil S.A.</option>
                <option value="Banco Rendimento S.A.">Banco Rendimento S.A.</option>
                <option value="Banco Rodobens S.A.">Banco Rodobens S.A.</option>
                <option value="Banco Safra S.A.">Banco Safra S.A.</option>
                <option value="Banco Santander  (Brasil)  S.A.">Banco Santander  (Brasil)  S.A.</option>
                <option value="Banco Semear S.A.">Banco Semear S.A.</option>
                <option value="Banco Smartbank S.A.">Banco Smartbank S.A.</option>
                <option value="Banco Société Générale Brasil S.A.">Banco Société Générale Brasil S.A.</option>
                <option value="Banco Stone Pagamentos S.A.">Banco Stone Pagamentos S.A.</option>
                <option value="Banco Sumitomo Mitsui Brasileiro S.A.">Banco Sumitomo Mitsui Brasileiro S.A.</option>
                <option value="Banco Topázio S.A.">Banco Topázio S.A.</option>
                <option value="Banco Toyota do Brasil S.A.">Banco Toyota do Brasil S.A.</option>
                <option value="Banco Triângulo S.A.">Banco Triângulo S.A.</option>
                <option value="Banco Volvo Brasil S.A.">Banco Volvo Brasil S.A.</option>
                <option value="Banco Votorantim S.A.">Banco Votorantim S.A.</option>
                <option value="Banco VR S.A.">Banco VR S.A.</option>
                <option value="Banco Western Union do Brasil S.A.">Banco Western Union do Brasil S.A.</option>
                <option value="Banco XP S.A.">Banco XP S.A.</option>
                <option value="Banco Yamaha Motor do Brasil S.A.">Banco Yamaha Motor do Brasil S.A.</option>
                <option value="BancoSeguro S.A.">BancoSeguro S.A.</option>
                <option value="BANESTES S.A. Banco do Estado do Espírito Santo">BANESTES S.A. Banco do Estado do Espírito Santo</option>
                <option value="Bank of America Merrill Lynch Banco Múltiplo S.A.">Bank of America Merrill Lynch Banco Múltiplo S.A.</option>
                <option value="BCV - Banco de Crédito e Varejo S.A.">BCV - Banco de Crédito e Varejo S.A.</option>
                <option value="BEXS Banco de Câmbio S.A.">BEXS Banco de Câmbio S.A.</option>
                <option value="BNY Mellon Banco S.A.">BNY Mellon Banco S.A.</option>
                <option value="BRB - Banco de Brasília S.A.">BRB - Banco de Brasília S.A.</option>
                <option value="Caixa Econômica Federal">Caixa Econômica Federal</option>
                <option value="China Construction Bank (Brasil) Banco Múltiplo S.A.">China Construction Bank (Brasil) Banco Múltiplo S.A.</option>
                <option value="Citibank N.A.">Citibank N.A.</option>
                <option value="Deutsche Bank S.A. - Banco Alemão">Deutsche Bank S.A. - Banco Alemão</option>
                <option value="Goldman Sachs do Brasil Banco Múltiplo S.A.">Goldman Sachs do Brasil Banco Múltiplo S.A.</option>
                <option value="Hipercard Banco Múltiplo S.A.">Hipercard Banco Múltiplo S.A.</option>
                <option value="HSBC Brasil S.A. - Banco de Investimento">HSBC Brasil S.A. - Banco de Investimento</option>
                <option value="ING Bank N.V.">ING Bank N.V.</option>
                <option value="Itaú Unibanco Holding S.A.">Itaú Unibanco Holding S.A.</option>
                <option value="Itaú Unibanco S.A.">Itaú Unibanco S.A.</option>
                <option value="JPMorgan Chase Bank, National Association">JPMorgan Chase Bank, National Association</option>
                <option value="Kirton Bank S.A. - Banco Múltiplo">Kirton Bank S.A. - Banco Múltiplo</option>
                <option value="MS Bank S.A. Banco de Câmbio">MS Bank S.A. Banco de Câmbio</option>
                <option value="Nu Pagamentos S.A.">Nu Pagamentos S.A.</option>
                <option value="Paraná Banco S.A.">Paraná Banco S.A.</option>
                <option value="Plural S.A. - Banco Múltiplo">Plural S.A. - Banco Múltiplo</option>
                <option value="Scania Banco S.A.">Scania Banco S.A.</option>
                <option value="Scotiabank Brasil S.A. Banco Múltiplo">Scotiabank Brasil S.A. Banco Múltiplo</option>
                <option value="Travelex Banco de Câmbio S.A.">Travelex Banco de Câmbio S.A.</option>
                <option value="UBS Brasil Banco de Investimento S.A.">UBS Brasil Banco de Investimento S.A.</option>
            </select>              
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="tipoConta" class="col-md-4 col-form-label text-md-right">
                {{ __('Tipo de Conta') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <select id="tipoConta" class="form-input" name="tipoConta">
                <option value="" ></option>
                <option value="C">Conta Corrente</option>
                <option value="P">Conta Poupança</option>
            </select>
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="agencia" class="col-md-4 col-form-label text-md-right">
                {{ __('Agência') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input maxlength="10" id="agencia" type="text" class="form-input" name="agencia" value="" >
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="contaOperacao" class="col-md-4 col-form-label text-md-right">
                {{ __('Operação') }} <span class="required" id="exigeOperacao" style="display: none;">*</span>
            </label>
        </div>

        <div class="span3">
            <select name="contaOperacao" id="contaOperacao" style="width:100%;">
                <option value=""></option>
                <option value="001">001 - Conta Corrente PF</option>
                <option value="002">002 - Conta Simples PF</option>
                <option value="003">003 - Conta Corrente PJ</option>
                <option value="006">006 - Entidades Públicas</option>
                <option value="007">007 - Depósitos Instituições Financeiras</option>
                <option value="013">013 - Poupança PF</option>
                <option value="022">022 - Poupança PJ</option>
                <option value="023">023 - Conta Caixa Fácil</option>
                <option value="028">028 - Poupança Crédito Imobiliário</option>
                <option value="043">043 - Depósitos Lotéricos</option>
            </select>
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="conta" class="col-md-4 col-form-label text-md-right">
                {{ __('Número da Conta') }} <span class="required">*</span><br>
                <span "><b>com o dígito&nbsp;&nbsp;&nbsp;</b></span>
            </label>
        </div>

        <div class="span2">
            <input maxlength="10" id="conta" type="text" class="form-input" name="conta" value="">
        </div>
    </div>
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="declaracao" class="col-md-4 col-form-label text-md-right">
                &nbsp;
            </label>
        </div>

        <div class="span8">
            <div class="alert alert-info">
                <input type="checkbox" id="declaracaoBanco" name="declaracaoBanco" class="form-input"> 
                Concordo com a portabilidade automática do valor depositado na conta do banco mumbuca para a conta informada acima. 
                <span class="required">*</span>
                <br>
            </div>
        </div>
    </div> 


    <form method="POST" action="" id="formComprovanteBancario" name="formComprovanteBancario" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Comprovante de Conta') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Comprovante Bancário" />
                <input type="hidden" name="tipoDoc" value="ComprovanteBancario" />
                <input type="hidden" name="idCpf" id="idCompBancario" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoCompBancario" /><br>
                Obs I: O tamanho máximo dos arquivos é de 2Mb.<br>
                Obs II: Foto do cartão do banco ou extrato do banco com conta e agência.
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCompBancario">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <div class="row formrow">
        <div class="span11">
            <h4 class="heading">Documentos Lançados<span></span></h4>

            <table id="documentosComprovanteBancario" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th width='20%'>Descrição</th>
                        <th>Nome Original</th>
                        <th>Nome Final</th>
                        <th width='20%'>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>


</div>
