@if (!$verificaEnvioRecurso)
    <div class="alert alert-info">
        <div class="row">
            <div class="col-lg-12 mt-2">
                <h3>Recurso</h3>
            </div>

            <div class="col-lg-8">
                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="justificativa_recurso">Justifique o motivo do recurso:</label>
                        <textarea name="justificativa_recurso" 
                            class="form-control form-control-sm" 
                            id="justificativa_recurso"
                            rows="9" cols="5" maxlength="255"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="recurso_tipo_documento">Tipo de Documento:</label>
                        <select name="recurso_tipo_documento" 
                            class="form-control form-control-sm" id="recurso_tipo_documento">
                            <option></option>
                            <option>Identidade</option>
                            <option>CPF</option>
                            <option>Comprovante de Residência</option>
                            <option>Outro</option>
                        </select>
                    </div>

                    <div class="col-lg-12 form-group recurso_descricao_documento d-none">
                        <label for="recurso_descricao_documento">Descrição do Documento:</label>
                        <textarea name="recurso_descricao_documento" 
                            id="recurso_descricao_documento" 
                            maxlength="40"
                            class="form-control form-control-sm"></textarea>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="documento_recurso" class="d-block"><i class="fa fa-paperclip"></i> Anexar Arquivos:</label>
                        <input type="file" name="documento_recurso" 
                            id="documento_recurso" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <button class="btn btn-primary btn-enviar-documento-recurso"
                            data-nome_input="documento_recurso" 
                            data-nome_sessao="documentos_consulta_mrc"
                            data-conteudo_documentos="conteudo_consulta_mrc_recurso"
                            data-id_tabela="tabela_documentos_consulta_mrc"
                            data-pasta="consulta_mrc">
                            Enviar Documento <i class="fa fa-send"></i>
                        </button>
                    </div>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

                <div class="conteudo_consulta_mrc_recurso">
                    @include('cadastro.cgm.consulta_mrc.tabela_documentos')
                </div>
            </div>
            
            <div class="col-lg-12 mt-3">
                <button class="btn btn-success btn-solicitar-recurso">Solicitar Recurso <i class="fa fa-send"></i></button>
            </div>
        </div>
    </div>
@else

<div class="alert alert-info text-center">
    <h5><i class="fa fa-volume-up"></i> Atenção: O seu recurso foi solicitado. Continue acompanhando o andamento desta solicitação.</h5>
</div>

@endif


