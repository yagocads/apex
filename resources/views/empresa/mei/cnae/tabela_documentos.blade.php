
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="dados_cnae">
        <thead>
            <tr>
                <th>Tipo CNAE</th>
                <th>Atividade CNAE</th>
                <th>Código CNAE</th>
                <th>Grupo CNAE</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("dados_cnae"))
                @foreach(session("dados_cnae") as $documento)
                    <tr>
                        <td class="td-descricao">{{ $documento->tipo }}</td>
                        <td>{{ $documento->atividade }}</td>
                        <td>{{ $documento->codigo }}</td>
                        <td>{{ $documento->grupo }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento-cnae" title="Remover CNAE" 
                                data-codigo="{{ $documento->codigo }}"
                                data-pasta="cnae"
                                data-nome_sessao="dados_cnae"
                                data-id_tabela="dados_cnae"
                                data-conteudo_documentos="conteudo_dados_cnae">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>