@extends('layouts.tema_principal')

@section('content')
@include('modals.modal_msg')
@include('modals.modal_error')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Financeiro</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            Parcelamento de dívida ativa
                        </h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="javascript:void(0)" id="form_pd">
                            @csrf
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6 form-group">
                                        @if(get_guia_services() == 1)
                                            <label for="ro_cpf">CPF</label>
                                        @else
                                            <label for="ro_cpf">CNPJ</label>
                                        @endif
                                        <input type="text" class="form-control form-control-sm" name="documento" id="documento" value="{{ Auth::user()->cpf }}" readonly="readonly" />
                                    </div>

                                    <div class="col-lg-6 form-group">
                                        <label for="name">Nome:</label>
                                        <input type="text" class="form-control form-control-sm" name="nome" id="nome" value="{{ Auth::user()->name }}" readonly="readonly" />
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="name">E-mail:</label>
                                        <input type="text" class="form-control form-control-sm" name="email" id="email" value="{{ Auth::user()->email }}" readonly="readonly" />
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label for="name">Telefone:</label>
                                        <input type="text" class="form-control form-control-sm" name="telefone" id="telefone" value="{{ Auth::user()->phone }}" readonly="readonly" />
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label for="name">Celular:</label>
                                        <input type="text" class="form-control form-control-sm" name="celular" id="celular" value="{{ Auth::user()->celphone }}" readonly="readonly" />
                                    </div>
                                    @if(get_guia_services() == 2)
                                        <div class="col-lg-12 form-group">
                                            <label for="name">Nome do Representante Legal:</label>
                                            <input type="text" class="form-control form-control-sm" name="representante" id="representante" maxlength="40" />
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label for="name">CPF - Representante Legal:</label>
                                            <input type="text" class="form-control form-control-sm" name="cpf_rep" id="cpf_rep" />
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label for="name">CPF - Representante Legal: ( Somente PDF )</label>
                                            <input type="file" class="form-control form-control-sm" name="cpf_rep_doc" id="cpf_rep_doc" />
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label for="name">RG - Representante Legal: ( Enviar frente e verso no mesmo arquivo PDF )</label>
                                            <input type="file" class="form-control form-control-sm" name="rg_rep_doc" id="rg_rep_doc" />
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label for="name">Documento de Representação Legal: ( Somente PDF )</label>
                                            <input type="file" class="form-control form-control-sm" name="doc_rep" id="doc_rep" />
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label for="name">Telefone - Representante Legal:</label>
                                            <input type="text" class="form-control form-control-sm" name="tel_rep" id="tel_rep" />
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label for="name">Celular - Representante Legal:</label>
                                            <input type="text" class="form-control form-control-sm" name="cel_rep" id="cel_rep" />
                                        </div>
                                        <div class="col-lg-12 form-group">
                                            <label for="name">E-mail - Representante Legal:</label>
                                            <input type="email" class="form-control form-control-sm" name="email_rep" id="email_rep" maxlength="100" />
                                        </div>
                                    @endif
                                    <div class="col-lg-6 form-group">
                                        <label for="name">Tipo de Dívida:</label>
                                        <select class="form-control form-control-sm" name="tipo_tributo" id="tipo_tributo">
                                            <option value="">Selecione</option>
                                            <option value="Dívida Executada">Dívida Executada</option>
                                            <option value="Dívida Não Executada">Dívida Não Executada</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label for="name">Parcelas:</label>
                                        <input type="number" class="form-control form-control-sm" name="parcela" onKeyDown="if(this.value.length==2 && event.keyCode!=8) return false;" onkeypress='return onlyNumber(event)' id="parcela" max="36" min="1" />
                                        <label>Quantidade mínima de cada parcela: 2 UFIMAS</label><br>
                                        <label>Quantidade máxima de parcelas: 36 parcelas</label>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label for="name">Dívida Ativa: ( Informar dívida ativa a ser parcelada )</label>
                                        <input type="text" class="form-control form-control-sm" maxlength="100" name="div_ativa" id="div_ativa" />
                                    </div>
                                </div>

                                <div class="card mb-3">
                                    <a class="card-link no-underline text-dark">
                                        <div class="card-header">
                                            <i class="fa fa-arrow-down arrow-red"></i> Documentação obrigatória
                                        </div>
                                    </a>
                                    <div>
                                        <div class="card-body">
                                            @include('financeiro.documentos_divida')
                                        </div>
                                    </div>
                                </div>
                        </form>
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'financeiro']) }}"
                                    class="btn btn-primary">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </a>
                                <button type="submit" id="salvar" class="btn btn-success">Enviar Solicitação</button>
                            </div>
                        </div>
                        <div id="msg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('post-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
<script>
    $(function() {

        $('#cpf_rep').mask("000.000.000-00");
        $('#celular').mask("(99) 99999-9999");
        $('#telefone').mask("(99) 9999-9999");
        $('#tel_rep').mask("(99) 9999-9999");
        $('#cel_rep').mask("(99) 99999-9999");

    });
</script>


<script>
    $(function() {

        var load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

         $("#form_pd").validate({

         rules: {
            representante: { required: true },
            doc_rep: { required: true },
            cpf_rep: { required: true },
            email_rep: { required: true },
            cel_rep: { required: true },
            processo: { required: true },
            documento: { required: true },
            celular: { required: true },
            parcela: { required: true },
            tipo_tributo: { required: true },
            div_ativa: { required: true },
            cpf_rep_doc: { required: true },
            rg_rep_doc: { required: true },
         },
         messages: {
            representante: { required: "Informe o Nome do representante" },
            doc_rep: { required: "Informe o Documento de Representação Legal " },
            cpf_rep_doc: { required: "Enviar a cópia do CPF de Representação Legal " },
            rg_rep_doc: { required: "Enviar a cópia do RG Representação Legal " },
            email_rep: { required: "Informe E-mail do representante", email: "E-mail inválido" },
            cpf_rep: { required: "Informe o CPF do Representante" },
            cel_rep: { required: "Informe o Celular do Representante" },
            celular: { required: "Informe o Celular" },
            parcela: { required: "" },
            tipo_tributo: { required: "Informe o Tipo de dívida" },
            div_ativa: { required: "Informe a Dívida Ativa" },
            processo: { required: "Informe o Documento do processo" },

         },
         submitHandler: function(form) {
            let url = "{{ route('salvarParcelarDivida') }}";
            let form_data = new FormData();
            var documento = $("#documento").val();
            let file =  $('#doc_rep').prop('files')[0];
            let file_cpf =  $('#cpf_rep_doc').prop('files')[0];
            let file_rg =  $('#rg_rep_doc').prop('files')[0];

            if(!validarCPF($("#cpf_rep").val())){
                alert("CPF Inválido!");
                return false;
            }

            if (!validarAnexo(file, ["pdf"],'10485760') || !validarAnexo(file_cpf, ["pdf"],'10485760') || !validarAnexo(file_rg, ["pdf"],'10485760')) {
                return false;
            }

            if (documento.length != 14) {
                form_data.append('documento', $('#doc_rep').prop('files')[0]);
                form_data.append('cpf_rep_doc', $('#cpf_rep_doc').prop('files')[0]);
                form_data.append('rg_rep_doc', $('#rg_rep_doc').prop('files')[0]);
                form_data.append('cpf_rep', $("#cpf_rep").val());
                form_data.append('email_rep', $("#email_rep").val());
                form_data.append('tel_rep', $("#tel_rep").val());
                form_data.append('cel_rep', $("#cel_rep").val());
                form_data.append('representante', $("#representante").val());
            }

            form_data.append('document', $("#documento").val());
            form_data.append('nome', $("#nome").val());
            form_data.append('email', $("#email").val());
            form_data.append('telefone', $("#telefone").val());
            form_data.append('celular', $("#celular").val());
            form_data.append('parcela', $("#parcela").val());
            form_data.append('tipo_tributo', $("#tipo_tributo").val());
            form_data.append('div_ativa', $("#div_ativa").val());

            $.ajax({
                url: url,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                data: form_data,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    var resposta = jQuery.parseJSON(response);
                    $('.modal-body>p').empty();
                    if (resposta.error) {
                        $('.modal-body>p').append(resposta.error);
                        $("#error").modal('show');
                        return false;
                    }

                    $('#form_pd').each(function () {
                        this.reset();
                    });
                    $('#documentos_processo').remove();
                    $('.modal-body>p').append(response.split('"').join(''));
                    $("#divida-modal").modal('show');
                }
            });
         }
       });
    });
</script>

@endsection
