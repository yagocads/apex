@extends('layouts.tema_principal')
@section('content')
<div class="container-fluid">
    @include('modals.modal_default')
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Consulta Geral Financeira</h2>
            </div>
        </div>
    </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h4>{{ $pagina }}</h4></div>
                    <div class="card-body">

                        <div class="form-row">
                            {{-- <div class="col-md-6 mb-3">
                                @if(strlen($cpf_cnpj) == 14)
                                <label for="cpf">{{ __('CPF') }}</label>
                                @else
                                <label for="cpf">{{ __('CNPJ') }}</label>
                                @endif
                                <input type="text" class="form-control form-control-sm" name="cpf" value="{{ $cpf_cnpj }}" readonly="readonly" />
                            </div> --}}
                            <div class="col-md-2">
                                <label>{{ $tipo_pesquisa == 'm' ? 'Matrícula do Imóvel' : 'Inscrição Municipal' }}</label>
                                <input type="text" class="form-control form-control-sm" name="matricula" value="{{ $matricula_inscricao }}" readonly="readonly" />
                            </div>
                        </div>

                        <hr />

                        <form id="formulario" action="{{ route('integracaoReciboDebitosIptu') }}" method="post">
                            @csrf
                            <input type="hidden" name="tipo_pesquisa" id="tipo_pesquisa" name="tipo_pesquisa" value="{{ $tipo_pesquisa }}">
                            <input type="hidden" name="matricula_inscricao" id="matricula_inscricao" name="matricula_inscricao" value="{{ $matricula_inscricao }}">
                            <input type="hidden" name="tipo_debito" id="tipo_debito" value="{{ $tipo_debito }}">
                            <input type="hidden" name="numpre_unica" id="numpre_unica" value="">
                            <input type="hidden" name="numpar_unica" id="numpar_unica" value="">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if($tipo_debito == 86)
                                        @include('financeiro.relatorio_deb_abertos_iptu')
                                    @else
                                        <table class="table table-striped table-bordered dataTable dt-responsive nowrap w-100" id="debitosAbertos">
                                            <thead>
                                                <tr>
                                                    <th data-priority="1">{{ $tipo_debito == 34 ? 'Inicial' : 'Arrecadação' }}</th>
                                                    @if($tipo_debito == 6)
                                                        <th>Parcelamento</th>
                                                    @endif
                                                    <th data-priority="2">Parcela</th>
                                                    <th>Vencimento</th>
                                                    <th>{{ $tipo_debito == 34 ? 'Processo do Foro' : 'Receita' }}</th>
                                                    <th>Valor</th>
                                                    <th>Corrigido</th>
                                                    <th>Juros</th>
                                                    <th>Multa</th>
                                                    <th>Desconto</th>
                                                    <th>Total</th>
                                                    <th data-priority="3"><input type="checkbox" name="checkAll" class="checkAll" id="checkAll" {{ $tipo_debito == 34 ? 'disabled' : '' }} ></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($registros_unicos))
                                                    @foreach($registros_unicos as $ru)
                                                        @if($ru->k00_receit !== '')
                                                            @php
                                                            $k00_receit = str_replace('+',' ',$ru->k00_receit);
                                                            @endphp
                                                        @else
                                                            @php
                                                            $k00_receit = $ru->k00_receit;
                                                            @endphp
                                                        @endif
                                                        <tr>
                                                            <td>{{ $ru->k00_numpre }}</td>
                                                            <td>{{ $ru->k00_numpar }}</td>
                                                            @if(date("Ymd",strtotime($ru->dtvencunic)) < date("Ymd"))
                                                                <td><span style="color:#FF0000;">{{ date("d/m/Y",strtotime($ru->dtvencunic)) }}</span></td>
                                                            @else
                                                                @if(date("m",strtotime($ru->dtvencunic)) == date("m"))
                                                                    <td><span style="color:#FFA500;">{{ date("d/m/Y",strtotime($ru->dtvencunic)) }}</span></td>
                                                                @else
                                                                    <td>{{ date("d/m/Y",strtotime($ru->dtvencunic)) }}</td>
                                                                @endif
                                                            @endif
                                                            <td>{{ $k00_receit }}</td>
                                                            <td>R$ {{ format_currency_brl($ru->uvlrhis) }}</td>
                                                            <td>R$ {{ format_currency_brl($ru->uvlrcor) }}</td>
                                                            <td>R$ {{ format_currency_brl($ru->uvlrjuros) }}</td>
                                                            <td>R$ {{ format_currency_brl($ru->uvlrmulta) }}</td>
                                                            <td>R$ {{ format_currency_brl($ru->uvlrdesconto) }}</td>
                                                            <td>R$ {{ format_currency_brl($ru->utotal) }}</td>
                                                            <td>&nbsp;<input type="radio" name="registros_unicos" class="registros_unicos" value="{{ $ru->k00_numpre }}_{{ $ru->k00_numpar }}_{{ $ru->dtvencunic }}_{{ str_replace('+',' ',$ru->k00_receit) }}_{{ format_currency_brl($ru->uvlrhis) }}_{{ format_currency_brl($ru->uvlrcor) }}_{{ $ru->uvlrjuros }}_{{ $ru->uvlrmulta }}_{{ format_currency_brl($ru->uvlrdesconto) }}_{{ format_currency_brl($ru->utotal) }}"></td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                @if(isset($registros))
                                                    @foreach($registros as $registro)
                                                        @if($registro->k00_receit !== '')
                                                            @php
                                                            $k00_receit = str_replace('+',' ',$registro->k00_receit);
                                                            @endphp
                                                        @else
                                                            @php
                                                            $k00_receit = $registro->k00_receit;
                                                            @endphp
                                                        @endif
                                                        <tr>
                                                            <td>
                                                                @if($tipo_debito == 34)
                                                                    {{ $registro->k00_inicial }}
                                                                @else
                                                                    {{ $registro->k00_numpre }}
                                                                @endif
                                                            </td>
                                                            @if($tipo_debito == 6)
                                                                <td>
                                                                    {{ $registro->k00_num_parc }}
                                                                </td>
                                                            @endif
                                                            <td>
                                                                @if($tipo_debito == 34)
                                                                    1
                                                                @else
                                                                    {{ $registro->k00_numpar }}
                                                                @endif

                                                            </td>
                                                            @if($tipo_debito == 34)
                                                                @php
                                                                $dataVenc = $registro->k00_dtinicial;
                                                                @endphp

                                                                @if(date("Ymd",strtotime($registro->k00_dtinicial)) < date("Ymd"))
                                                                    <td><span style="color:#FF0000;">{{ date("d/m/Y",strtotime($registro->k00_dtinicial)) }}</span></td>
                                                                @else
                                                                    @if(date("m",strtotime($registro->k00_dtinicial)) == date("m"))
                                                                        <td><span style="color:#FFA500;">{{ date("d/m/Y",strtotime($registro->k00_dtinicial)) }}</span></td>
                                                                    @else
                                                                        <td>{{ date("d/m/Y",strtotime($registro->k00_dtinicial)) }}<br /></td>
                                                                    @endif
                                                                @endif
                                                            @else
                                                                @php
                                                                $dataVenc = $registro->k00_dtvenc;
                                                                @endphp

                                                                @if(date("Ymd",strtotime($registro->k00_dtvenc)) < date("Ymd"))
                                                                    <td><span style="color:#FF0000;">{{ date("d/m/Y",strtotime($registro->k00_dtvenc)) }}</span></td>
                                                                @else
                                                                    @if(date("m",strtotime($registro->k00_dtvenc)) == date("m"))
                                                                        <td><span style="color:#FFA500;">{{ date("d/m/Y",strtotime($registro->k00_dtvenc)) }}</span></td>
                                                                    @else
                                                                        <td>{{ date("d/m/Y",strtotime($registro->k00_dtvenc)) }}</td>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                            <td>{{ $tipo_debito == 34 ? $registro->k00_codforo : $k00_receit }}</td>
                                                            <td>R$ {{ format_currency_brl($registro->valor) }}</td>
                                                            <td>R$ {{ format_currency_brl($registro->valorcorr) }}</td>
                                                            <td>R$ {{ format_currency_brl($registro->juros) }}</td>
                                                            <td>R$ {{ format_currency_brl($registro->multa) }}</td>
                                                            <td>R$ {{ format_currency_brl($registro->desconto) }}</td>
                                                            <td>R$ {{ format_currency_brl($registro->total) }}</td>
                                                            <td>
                                                                @if($registro->valor == '0.00')
                                                                    &nbsp;
                                                                @elseif($tipo_debito == 34)
                                                                    @if(empty($registro->k00_codforo))
                                                                        &nbsp;
                                                                    @else
                                                                        &nbsp;<input type="radio" name="registros_normais[]" id="registros_normais" class="inicial_foro" value="{{ $registro->k00_inicial }}_{{ $registro->k00_numpre }}_{{ $registro->k00_numpar }}_{{ $dataVenc }}_{{ str_replace('+',' ',$registro->k00_receit) }}_{{ $registro->valor }}_{{ $registro->valorcorr }}_{{ $registro->juros }}_{{ $registro->multa }}_{{ $registro->desconto }}_{{ $registro->total }}" />
                                                                    @endif
                                                                @else
                                                                    &nbsp;<input type="checkbox" name="registros_normais[]" class="registros_normais" value="{{ $registro->k00_numpre }}_{{ $registro->k00_numpar }}_{{ $dataVenc }}_{{ str_replace('+',' ',$registro->k00_receit) }}_{{ $registro->valor }}_{{ $registro->valorcorr }}_{{ $registro->juros }}_{{ $registro->multa }}_{{ $registro->desconto }}_{{ $registro->total }}" />
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Valores Atualizados</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="callout-red shadow-sm mb-4">
                                                <div class="row">
                                                    <div class="col-lg-6" id="linha_totais">
                                                        Nenhum débito marcado!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3 m4">
                                                    <label>Data de vencimento do boleto:</label>
                                                    <input type="date" class="form-control form-control-sm" min="{{ date('Y-m-d') }}"  id="dataVencimento" name="dataVencimento" disabled required />
                                                </div>
                                            </div>
                                            <div class="row" id="mensagem">
                                                <div class="col-md-12 mb-12">
                                                    <label>&nbsp;</label>
                                                    <div class="alert alert-primary">
                                                        Emissão do boleto fora do horário permitido (7h até 19h).
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 mb-12">
                                                    <label>&nbsp;</label>
                                                    <div class="alert alert-primary">
                                                        Prezado Contribuinte, informamos que o IPTU 2021 só será emitido com a data de vencimento conforme determinado no CATRIMA DECRETO 598 de 14 de outubro de 2020 publicado no JOM Edição especial 294 de 16/10/2020 . Sendo possível  alteração de datas somente após o vencimento das cotas.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-3 mb-3">
                                    <a href="{{ route('iptu') }}"
                                        class="btn btn-primary">
                                        <i class="fa fa-arrow-left"></i> Voltar
                                    </a>&nbsp;
                                    <button disabled id="btn_emitir_boleto" class="btn btn-success">Emitir Boleto <i class="fa fa-print"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



</div>

@include('modals.modal_footer')
@endsection

@section('post-script')
<script type="text/javascript">
    $(document).ready(function(){

        $(':checkbox[name^=checkAll]').click(function(){
            $('#linha_totais').html('');
            if($('#checkAll').is(':checked')){
                var checkbox = $('input:checkbox[name^=registros_normais]');
                checkbox.each(function(){
                    checkbox.prop('checked',true);
                });
                var checkbox_checked = $('input:checkbox[name^=registros_normais]:checked');
                if(checkbox_checked.length > 0){
                    var val = [];
                    var valor = 0;
                    var corrigido = 0;
                    var juros = 0;
                    var multa = 0;
                    var desconto = 0;
                    var total = 0;
                    checkbox_checked.each(function(){
                        val.push($(this).val());
                        var colunas = $(this).val().split("_");
                        valor += Number(colunas[4].replace(",",""));
                        corrigido += Number(colunas[5].replace(",",""));
                        juros += Number(colunas[6].replace(",",""));
                        multa += Number(colunas[7].replace(",",""));
                        desconto += Number(colunas[8].replace(",",""));
                        total += Number(colunas[9].replace(",",""));
                    });
                    $('#linha_totais').append("<p><b>Valor: R$ "+valor.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='valor_total' id='valor_total' value='"+valor+"'></b></p><p><b>Corrigido: R$ "+corrigido.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='corrigido_total' id='corrigido_total' value='"+corrigido+"'></b></p><p><b>Juros: R$ "+juros.toLocaleString('pt-BR', {style: 'currency', currency:'BRL'})+"<input type='hidden' name='juros_total' id='juros_total' value='"+juros+"'></b></p><p><b>Multa: R$ "+multa.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='multa_total' id='multa_total' value='"+multa+"'></b></p><p><b>Desconto: R$ "+desconto.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='desconto_total' id='desconto_total' value='"+desconto+"'></b></p><p><b>Total: R$ "+total.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='total_total' id='total_total' value='"+total+"'></b></p>");

                }
            } else {
                var checkbox = $('input:checkbox[name^=registros_normais]');
                checkbox.each(function(){
                    checkbox.prop('checked',false);
                });
            }
        });

        $('.registros_unicos').click(function(){
   			if($('.registros_unicos').is(':checked')){
        		$('.registros_normais').prop('checked',false);
   			}
		});
        $('.registros_normais').click(function(){
        	if($('.registros_normais').is(':checked')){
        		$('.registros_unicos').prop('checked',false);
        	}
		});
        $('#checkAll').click(function(){
            if($('#checkAll').is(':checked')){
                $('.registros_unicos').prop('checked',false);
            }
        });
    });
</script>
@endsection
