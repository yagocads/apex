<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-warning">
            <p><i class="fa fa-volume-up"></i> Ciente de que a falta de documentos solicitados prejudica o julgamento do referido pedido.</p>

            <ul>
                <li>Cópia do CRLV.</li>
                <li>
                    Cópia do CNH do proprietário ou do condutor identificado. O proprietário
                    (ou seu representante legal), quando não habilitado, deverá apresentar cópia do documento
                    de identidade e CPF.
                </li>
                <li>
                    A representação legal do requerente poderá ser realizada por procuração simples para o advogado,
                    acompanhada da Carteira da Ordem dos Advogados do Brasil - OAB ou por procuração,
                    com firma reconhecida para terceiros, acompanhada da cópia de identidade do representante.
                </li>
                <li>
                    Quando o proprietário notificado for pessoa jurídica, deverá apresentar cópia do CNPJ na
                    validade dos documentos constitutivos da empresa e dos documentos de identidade e
                    CPF do sócio / representante que solicita o serviço.
                </li>
                <li>Original ou cópia de Notificação de Autuação, ou do Auto de Infração.</li>
            </ul>
        </div>
    </div>

    <div class="col-lg-6 form-group">
        <div class="alert alert-info documentacao_obrigatoria">
            <p><i class="fa fa-folder-open"></i> Documentação Obrigatória:</p>

            <ul class="lista_documentacao_obrigatoria" style="list-style: none;">
            </ul>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <div class="alert alert-info">
                <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 5Mb.
            </div>
        </div>

        <div class="form-group">
            <label for="descricao_documento">Descrição do documento:</label>
            <select name="descricao_documento" id="descricao_documento"
                class="form-control form-control-sm">
                <option>1. teste</option>
            </select>
        </div>

        <div class="form-group">
            <label for="documento" class="d-block"><i class="fa fa-paperclip"></i> Anexar Documento:</label>
            <input type="file" name="documento"
                id="documento" />
        </div>

        <div class="form-group">
            <button class="btn btn-primary btn-enviar-documento"><i class="fa fa-plus"></i> Adicionar Documento</button>
        </div>
    </div>
</div>

<hr />

<div class="row col-lg-12 mb-3 mt-3">
    <h5><i class="fa fa-file-o"></i> Documentos Adicionados:</h5>
</div>

<div class="row">
    <div class="col-lg-12 form-group">
        <div class="conteudo_documentacao">
            @include('cidadao.transito.defesa_previa_autuacao.tabela_documentacao')
        </div>
    </div>
</div>
