<div class="row">

    <div class="col-lg-12 form-group">
        <label for="responsavel_preenchimento">Responsável pelo preenchimento: <span class="asterisc">*</span></label>
        <select name="responsavel_preenchimento" id="responsavel_preenchimento"
            class="form-control form-control-sm col-lg-3 {{ ($errors->has('responsavel_preenchimento') ? 'is-invalid' : '') }}">
            <option></option>
            <option {{ old('responsavel_preenchimento') === 'Representação Legal' ? 'selected' : '' }}>Representação Legal</option>
            <option {{ old('responsavel_preenchimento') === 'Próprio' ? 'selected' : '' }}>Próprio</option>
        </select>

        @if ($errors->has('responsavel_preenchimento'))
            <div class="invalid-feedback">
                {{ $errors->first('responsavel_preenchimento') }}
            </div>
        @endif

        <hr/>
    </div>

    <div class="col-lg-6">
        <h5><i class="fa fa-user"></i> Dados do Requerente:</h5>

        <div class="form-row mt-3">
            <div class="col-lg-12 form-group">
                <label for="nome_requerente">Nome do Requerente: <span class="asterisc">*</span></label>
                <input type="text" name="nome_requerente" id="nome_requerente"
                    class="form-control form-control-sm {{ ($errors->has('nome_requerente') ? 'is-invalid' : '') }}"
                    maxlength="40"
                    value="{{ old('nome_requerente') ?? convert_accentuation($dadosEcidade->aCgmPessoais->z01_nome) }}"
                    readonly />

                    @if ($errors->has('nome_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_requerente') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="cpf_cnpj_requerente">CPF/CNPJ: <span class="asterisc">*</span></label>
                <input type="text" name="cpf_cnpj_requerente" id="cpf_cnpj_requerente"
                    class="form-control form-control-sm
                    {{ mb_strlen($dadosEcidade->aCgmPessoais->z01_cgccpf) === 11 ? 'mask-cpf' : 'mask-cnpj' }}
                    {{ ($errors->has('cpf_cnpj_requerente') ? 'is-invalid' : '') }}"
                    maxlength="18"
                    value="{{ old('cpf_cnpj_requerente') ?? formatCnpjCpf($dadosEcidade->aCgmPessoais->z01_cgccpf) }}"
                    readonly />

                    @if ($errors->has('cpf_cnpj_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpf_cnpj_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="data_nasc_requerente">Data de Nascimento: <span class="asterisc">*</span></label>
                <input type="date" name="data_nasc_requerente" id="data_nasc_requerente"
                    class="form-control form-control-sm {{ ($errors->has('data_nasc_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('data_nasc_requerente') ?? $dadosEcidade->aCgmPessoais->z01_nasc }}" />

                    @if ($errors->has('data_nasc_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_nasc_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="cnh_requerente">CNH: <span class="asterisc">*</span></label>
                <input type="text" name="cnh_requerente" id="cnh_requerente"
                    class="form-control form-control-sm mask_cnh_requerente {{ ($errors->has('cnh_requerente') ? 'is-invalid' : '') }}"
                    maxlength="11"
                    value="{{ old('cnh_requerente') }}" />

                    @if ($errors->has('cnh_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cnh_requerente') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="identidade_requerente">Doc. Identidade: <span class="asterisc">*</span></label>
                <input type="text" name="identidade_requerente" id="identidade_requerente"
                    class="form-control form-control-sm {{ ($errors->has('identidade_requerente') ? 'is-invalid' : '') }}"
                    maxlength="20"
                    value="{{ old('identidade_requerente') ?? $dadosEcidade->aCgmAdicionais->z01_ident }}" />

                    @if ($errors->has('identidade_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('identidade_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="orgao_exp_requerente">Órgão Expeditor: <span class="asterisc">*</span></label>
                <input type="text" name="orgao_exp_requerente" id="orgao_exp_requerente"
                    class="form-control form-control-sm {{ ($errors->has('orgao_exp_requerente') ? 'is-invalid' : '') }}"
                    maxlength="50"
                    value="{{ old('orgao_exp_requerente') ?? $dadosEcidade->aCgmAdicionais->z01_identorgao }}" />

                    @if ($errors->has('orgao_exp_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('orgao_exp_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="nacionalidade_requerente">Nacionalidade: <span class="asterisc">*</span></label>
                <input type="text" name="nacionalidade_requerente" id="nacionalidade_requerente"
                    class="form-control form-control-sm {{ ($errors->has('nacionalidade_requerente') ? 'is-invalid' : '') }}"
                    maxlength="100"
                    value="{{ old('nacionalidade_requerente') }}" />

                    @if ($errors->has('nacionalidade_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nacionalidade_requerente') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="naturalidade_requerente">Naturalidade: <span class="asterisc">*</span></label>
                <input type="text" name="naturalidade_requerente" id="naturalidade_requerente"
                    class="form-control form-control-sm {{ ($errors->has('naturalidade_requerente') ? 'is-invalid' : '') }}"
                    maxlength="100"
                    value="{{ old('naturalidade_requerente') }}" />

                    @if ($errors->has('naturalidade_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('naturalidade_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="telefone_requerente">Telefone:</label>
                <input type="text" name="telefone_requerente" id="telefone_requerente"
                    class="form-control form-control-sm mask-tel {{ ($errors->has('telefone_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('telefone_requerente') ?? $dadosEcidade->aCgmContato->z01_telef }}" />

                    @if ($errors->has('telefone_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('telefone_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="celular_requerente">Celular: <span class="asterisc">*</span></label>
                <input type="text" name="celular_requerente" id="celular_requerente"
                    class="form-control form-control-sm mask-cel {{ ($errors->has('celular_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('celular_requerente') ?? $dadosEcidade->aCgmContato->z01_telcel }}" />

                    @if ($errors->has('celular_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('celular_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-8 form-group">
                <label for="email_requerente">E-mail: <span class="asterisc">*</span></label>
                <input type="email" name="email_requerente" id="email_requerente"
                    class="form-control form-control-sm {{ ($errors->has('email_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('email_requerente') ?? convert_accentuation($dadosEcidade->aCgmContato->z01_email) }}" />

                    @if ($errors->has('email_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email_requerente') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <h5><i class="fa fa-map-marker"></i> Endereço:</h5>

        <div class="form-row mt-3">
            <div class="col-lg-4 form-group">
                <label for="cep">CEP: <span class="asterisc">*</span></label>
                <input type="text" name="cep" id="cep"
                    onchange="pesquisaCep(this.value);"
                    class="form-control form-control-sm mask-cep"
                    value="{{ old('cep') ?? $dadosEcidade->endereco->iCep }}" />
            </div>

            <div class="col-lg-8 form-group">
                <label for="endereco">Endereço: <span class="asterisc">*</span></label>
                <input type="text" name="endereco" id="endereco"
                class="form-control form-control-sm" readonly="true"
                value="{{ old('endereco') ?? convert_accentuation($dadosEcidade->endereco->sRua) }}" />
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-2 form-group">
                <label for="numero">Número: <span class="asterisc">*</span></label>
                <input type="text" name="numero" id="numero"
                    class="form-control form-control-sm {{ ($errors->has('numero') ? 'is-invalid' : '') }}"
                    value="{{ old('numero') }}" maxlength="4" />

                @if ($errors->has('numero'))
                    <div class="invalid-feedback">
                        {{ $errors->first('numero') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-5 form-group">
                <label for="complemento">Complemento:</label>
                <input type="text" name="complemento" id="complemento" maxlength="50"
                    class="form-control form-control-sm"
                    value="{{ old('complemento') ?? convert_accentuation($dadosEcidade->endereco->sComplemento) }}" />
            </div>

            <div class="col-lg-5 form-group">
                <label for="bairro">Bairro: <span class="asterisc">*</span></label>
                <input type="text" name="bairro" id="bairro"
                    class="form-control form-control-sm" readonly="true"
                    value="{{ old('bairro') ?? convert_accentuation($dadosEcidade->endereco->sBairro) }}"/>
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="municipio">Município: <span class="asterisc">*</span></label>
                <input type="text" name="municipio" id="municipio"
                    class="form-control form-control-sm" readonly="true"
                    value="{{ old('municipio') ?? convert_accentuation($dadosEcidade->endereco->sMunicipio) }}"/>
            </div>

            <div class="col-lg-2 form-group">
                <label for="estado">Estado: <span class="asterisc">*</span></label>
                <input type="text" name="estado" id="estado"
                    class="form-control form-control-sm" readonly="true"
                    value="{{ old('estado') ?? $dadosEcidade->endereco->sUf }}" />
            </div>
        </div>
    </div>
</div>
