<div class="row">
    <div class="col-lg-12">
        <label>O que você deseja fazer? <span class="text-danger">*</span></label>
        
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" 
                    name="oquefazer" value="Alterar uma inscrição municipal existente"
                    {{ old('oquefazer') == 'Alterar uma inscrição municipal existente' ? 'checked' : '' }}>
                    Alterar uma inscrição municipal existente
            </label>
        </div>

        <div class="form-check">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" 
                name="oquefazer" value="Abrir uma inscrição municipal"
                {{ old('oquefazer') == 'Abrir uma inscrição municipal' ? 'checked' : '' }}>
                Abrir uma inscrição municipal
            </label>
        </div>

        <div class="form-check">
            @if ($errors->has('oquefazer'))
                <div class="invalid-feedback">
                    {{ $errors->first('oquefazer') }}
                </div>
            @endif
        </div>
    </div>
</div>

<div class="row mt-3" id="perguntaCNPJ" style="display: none;">
    <div class="col-lg-12">
        <label>Possui CNPJ? <span class="text-danger">*</span></label>
        
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" 
                name="possuiCNPJ" id="possuiCNPJ" value="Sim"
                    {{ old('possuiCNPJ') == 'Sim' ? 'checked' : '' }}>
                    Sim
            </label>
        </div>

        <div class="form-check">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" 
                name="possuiCNPJ" value="Não"
                {{ old('possuiCNPJ') == 'Não' ? 'checked' : '' }}>
                Não
            </label>
        </div>

        <div class="form-check">
            @if ($errors->has('possuiCNPJ'))
                <div class="invalid-feedback">
                    {{ $errors->first('possuiCNPJ') }}
                </div>
            @endif
        </div>
    </div>
</div>
<div class="row mt-3" id="mensagemAlteracao" style="display: none;">
    <div class="col-lg-12">
        <div class="alert alert-danger">
            <b>ATENÇÃO:</b><br>
            <p>
                Para este serviço, é necessário comparecer a casa do empreendedor para abertura do processo.
            </p>
        </div>
    </div>
</div>
