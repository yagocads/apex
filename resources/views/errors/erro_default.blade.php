@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i>
                    @isset($titulo)
                    {{ $titulo }}
                    @endisset
                </h2>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="alert alert-danger">
                @isset($mensagem)
                    @if ($codigo === 'API_0006')
                        <p><strong>Atenção!</strong>
                        A emissão do boleto só será possível após a atualização do seu cadastro, disponível neste link:
                        <a href="{{ route('primeiroAcesso') }}">Clique aqui</a>
                        </p>

                        <p>Prazo de 48 horas úteis para a efetivação. Após este prazo, o acesso ao boleto do IPTU 2021 estará liberado.</p>

                        <p>Este procedimento diz respeito à atualização cadastral dos dados do proprietário do imóvel. Se você é responsável tributário, possuidor, corretor, procurador, entre outros, solicite o boleto do IPTU no link:
                        <a href="{{ route('cadastro_iptu')}}">Clique aqui</a></p>
                    @else
                        <p><strong>Atenção!</strong>
                        {{ $mensagem }}
                        </p>
                        <p>Tente Novamente em alguns instantes.</p>
                    @endif
                @endisset
                <p>
                    <b>Código:</b>
                    @isset($codigo)
                    {{ $codigo }}
                    @endisset
                </p>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>

@endsection


@section('post-script')
<script type="text/javascript">
    jQuery(document).ready(function(){

        });
</script>
@endsection
