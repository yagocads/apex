@extends('layouts.tema_principal')

@section('content')
@include('modals.modal_msg')
<div class="container-fluid">
    <h3>Processo: {{ $chamado }}</h3>
    <form method="POST" id="form_pd">
        <input type="hidden" name="chamado" id="chamado" value="{{ $chamado }}" class="form-control" />
        <input type="hidden" name="ciclo" id="ciclo" value="{{ $ciclo }}" class="form-control" />
        <div class="row">
            <div class="col-lg-12 form-group">
                <label for="name">Observação:</label>
                <textarea class="form-control" name="obs" id="obs" rows="3" required></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 form-group">
                <label for="name">Documento Exigido:</label>
                <select class="form-control form-control-sm" id="processo">
                    <option value="">Selecione</option>
                    <option value="Termo de aceite">Termo de aceite</option>
                    <option value="Documento de Identidade">Documento de Identidade</option>
                    <option value="Cadastro de Pessoa Física-CPF">Cadastro de Pessoa Física-CPF</option>
                    <option value="Comprovante de Endereço da Empresa">Comprovante de Endereço da Empresa</option>
                    <option value="Cartão CNPJ">Cartão CNPJ</option>
                    <option value="Contrato Social">Contrato Social</option>
                    <option value="Ata de Constituição ou Estatuto Social">Ata de Constituição ou Estatuto Social</option>
                    <option value="Procuração">Procuração</option>
                    <option value="Contrato de Prestação de Serviços (contador)">Contrato de Prestação de Serviços (contador)</option>
                    <option value="0">Outros</option>
                </select>
            </div>
            <div class="col-lg-12 form-group" id="inputOculto">
                <input type="text" name="outro" id="outro" class="form-control" />
            </div>
            <div class="col-lg-12 form-group">
                <label for="name">Anexar documentos: ( Somente PDF )</label>
                <div class="input-group control-group increment" >
                    <input type="file" name="doc_processo" id="doc_processo" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-primary" id="salvar_documento"><i class="fa fa-save"></i> Salvar Documento</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 form-group" id="conteudo_documentos"></div>
        </div>
    </form>
    <div class="row">
        <div class="col-lg-12">
            <a href="{{ route('acompanhamento') }}"
                class="btn btn-primary">
                <i class="fa fa-arrow-left"></i> Voltar
            </a>
            <button type="submit" id="salvar" class="btn btn-success">Enviar Documentos</button>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        $('#inputOculto').hide();
        $('#processo').change(function() {
            $('#processo').val() == '0' ? $('#inputOculto').show() : $('#inputOculto').hide();
        });

        $("#salvar").click(function(e){

            if (!$("#remover_documento").length){
                alert("Favor enviar os documentos solicitados");
                return false;
            }

            if ($("#obs").val() == ''){
                alert("Favor digite alguma observação");
                return false;
            }

            $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            let load = $(".ajax_load");
            let obs = $('#obs').val();
            let ciclo = $("#ciclo").val();
            let chamado = $("#chamado").val();

            let formData = new FormData();
            formData.append("obs", obs);
            formData.append("ciclo", ciclo);
            formData.append("chamado", chamado);

            let url = "{{ route('salvar_exigencia') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        load.fadeOut(200);
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }
                    $('#form_pd').each(function () {
                        this.reset();
                    });
                    $('#documentos_processo').remove();
                    $('.modal-body>p').append(response.msg);
                    $("#divida-modal").modal('show');
                }
            });
        });

        $("#salvar_documento").click(function(e){
            $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            let load = $(".ajax_load");
            let arquivo = $('#doc_processo').prop('files')[0];
            let processo = $("#processo").val();
            let chamado = $("#chamado").val();

            if (!validarAnexo(arquivo, ["pdf"],'10485760')) {
                return false;
            }
            let formData = new FormData();

            if (processo == '0') {
                processo = $("#outro").val();
            }

            formData.append("arquivo", arquivo);
            formData.append("processo", processo);
            formData.append("chamado", chamado);

            let url = "{{ route('enviar_doc_divida') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        load.fadeOut(200);
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }
                    $("#conteudo_documentos").html(response);
                }
            });
        });
    });
</script>
@endsection
