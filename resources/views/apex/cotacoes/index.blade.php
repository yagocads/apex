@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>{{__('Cotações')}}</h4>
                </div>

                <div class="card-body">
                    <button class="btn btn-primary text-white mb-3 btn-notificacoes">
                        {{__('Notificações')}}
                        <span class="badge badge-light qtd-notificacoes">{{ !empty($notificacoes) ? count($notificacoes) : 0 }}</span>
                    </button>

                    <div class="table-responsive py-1 pl-1 pr-1">
                        <table class="table table-striped table-bordered dataTable dt-responsive w-100">
                            <thead>
                                <tr>
                                    <th width="5%">{{__('Nº Processo Aquisição')}}</th>
                                    <th width="5%">{{__('Nº Processo Cotação')}}</th>
                                    <th width="15%">CNPJ</th>
                                    <th>{{__('Razão Social')}}</th>
                                    <th>{{__('Objeto')}}</th>
                                    <th>{{__('Moeda')}}</th>
                                    <th>{{__('Resultado')}}</th>
                                    <th>{{__('Situação')}}</th>
                                    <th data-priority="1">{{__('Ações')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($cotacoes)
                                    @foreach($cotacoes as $cotacao)
                                        <tr>
                                            <td>{{ $cotacao->processo_lecom_pai }}</td>
                                            <td>{{ $cotacao->processo_lecom }}</td>
                                            <td>{{ $cotacao->cnpj }}</td>
                                            <td>{{ $cotacao->razao_social }}</td>
                                            <td>{{ $cotacao->objeto }}</td>
                                            <td>{{ $cotacao->moedas }}</td>
                                            <td>{{ $cotacao->resultado }}</td>
                                            <td>
                                                {{
                                                    !empty($cotacoesJaEnviadas[$cotacao->processo_lecom]) ?
                                                    __("Enviado")
                                                    : __("Não Enviado")
                                                }}
                                            </td>
                                            <td>
                                                @if ($cotacao->resultado === 'Cotação aberta')
                                                    <button class="btn btn-sm btn-primary btn-ofertar-valor mb-2 w-100"
                                                        data-processo="{{ $cotacao->processo_lecom }}"
                                                        data-moeda="{{ $cotacao->moedas }}"
                                                        data-fornecedor="{{ $fornecedor->id }}">
                                                        {{__('Ofertar Valor')}}
                                                    </button>

                                                    <button class="btn btn-sm btn-primary btn-abrir-modal-enviar-documento mb-2 w-100"
                                                        data-processo="{{ $cotacao->processo_lecom }}">
                                                        {{__('Enviar Documento')}}
                                                    </button>

                                                    <button class="btn btn-sm btn-primary btn-abrir-modal-esclarecimento w-100"
                                                        data-processo="{{ $cotacao->processo_lecom }}"
                                                        data-processopai={{ $cotacao->processo_lecom_pai }}
                                                        data-fornecedor="{{ $fornecedor->id }}">
                                                        {{__('Esclarecimento')}}
                                                    </button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('apex.cotacoes.modal-produtos-cotacao')
    @include('apex.cotacoes.modal-envio-documentos')
    @include('apex.cotacoes.modal-notificacoes')
    @include('apex.cotacoes.modal-esclarecimento')
</div>

@endsection

@section('post-script')
    <script src="{{ asset('js/handleArquivos/scripts.js') }}"></script>

    <script>
        $(function() {
            let load = $(".ajax_load");

            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
                },
            });

            $('body').on('click', '.btn-ofertar-valor', function() {

                const numeroProcesso = $(this).data('processo');
                const idFornecedor = $(this).data('fornecedor');
                const moeda = $(this).data('moeda');

                $.ajax({
                    url: "carregar-produtos-cotacao",
                    method: "POST",
                    data: { numeroProcesso, idFornecedor},
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-produtos-cotacao')
                            .find('.modal-header .numero-processo')
                            .html(numeroProcesso);

                        $('#modal-produtos-cotacao')
                            .find('.modal-body')
                            .html(response);

                        if (moeda === 'BRL') {
                            $('.input-cotacoes').mask('000.000.000.000.000,00', { reverse: true });
                        } else {
                            $('.input-cotacoes').mask('000000000000000.00', { reverse: true });
                        }

                        recalcularDataTable('tabela-produtos-servicos');
                        $('#modal-produtos-cotacao').modal('show');
                    },
                });
            });

            $('body').on('click', '.salvar-cotacao', function() {

                let numeroProcesso = $('#modal-produtos-cotacao')
                    .find('.modal-header .numero-processo')
                    .html();

                let cotacoes = [];

                $("input[name='cotacoes[]']").each(function() {
                    cotacoes.push({
                        numeroProcessoPai: $(this).data('processopai'),
                        item: $(this).data('item'),
                        quantidade: $(this).data('quantidade'),
                        unidade: $(this).data('unidade'),
                        moeda: $(this).data('moeda'),
                        valor: $(this).val(),
                        observacoes: $('#observacoes').val()
                    });
                });

                $.ajax({
                    url: "{{ route('salvar-cotacoes-produtos') }}",
                    method: "POST",
                    data: {
                        numeroProcesso,
                        cotacoes,
                        idFornecedor: "{{ !empty($fornecedor->id) ? $fornecedor->id : '' }}"
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            location.reload();
                        }
                    },
                });
            });

            $('body').on('click', '.btn-abrir-modal-enviar-documento', function() {
                const numeroProcesso = $(this).data('processo');

                $('body .btn-salvar-documentos').attr('data-processo', numeroProcesso);

                $.ajax({
                    url: "{{ route('carregar-documentos-cotacoes') }}",
                    method: "POST",
                    data: { numeroProcesso },
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {

                        load.fadeOut(200);

                        $('.conteudo_documentacao').html(response);
                        recalcularDataTable('documentacao_cotacoes');

                        $('#modal-envio-documentos-cotacao').modal('show');
                    },
                });
            });

            enviarDocumento({
                tamanhoAnexoPermitido: 5048000,
                servidor: 'ftp',
                pastaServidor: 'documentos_cotacoes',
                nomeSessao: 'documentacao_cotacoes',
                caminhoTabela: 'apex.cotacoes.tabela-documentos-enviados',
                rota: 'salvar-documentos-cotacoes',
                idTabela: 'documentacao_cotacoes'
            });

            removerDocumento({
                servidor: 'ftp',
                pastaServidor: 'documentos_cotacoes',
                nomeSessao: 'documentacao_cotacoes',
                caminhoTabela: 'apex.cotacoes.tabela-documentos-enviados',
                rota: 'remover-documentos-cotacoes',
                idTabela: 'documentacao_cotacoes'
            });

            $('body').on('click', '.btn-salvar-documentos', function(e) {
                e.preventDefault();

                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "{{ route('salvar-documentos-cotacoes-na-base-de-dados') }}",
                    method: "POST",
                    data: { numeroProcesso },
                    dataType: 'json',
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            $('#modal-envio-documentos-cotacao').modal('hide');
                            location.reload();
                        }
                    },
                });
            });

            $('body').on('click', '.btn-notificacoes', function(e) {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('carregar-notificacoes') }}",
                    method: "POST",
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('#modal-notificacoes').find('.modal-body').html(response);

                        $('#modal-notificacoes').modal('show');
                    },
                });
            });

            $('body').on('click', '.marcar-como-lida', function() {
                const numeroProcesso = $(this).data('processo');

                $.ajax({
                    url: "{{ route('marcar-notificacao-como-lida') }}",
                    method: "POST",
                    data: { numeroProcesso},
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        let qtdNotificacoes = $('.qtd-notificacoes').html();

                        $('.qtd-notificacoes').html(Number(qtdNotificacoes) - 1);
                        $('#modal-notificacoes').find('.modal-body').html(response);
                    },
                });
            });

            $('body').on('click', '.btn-abrir-modal-esclarecimento', function() {
                const numeroProcessoPai = $(this).data('processopai');
                const numeroProcesso = $(this).data('processo');
                const idFornecedor = $(this).data('fornecedor');

                $('body .enviar-esclarecimento').attr('data-processopai', numeroProcessoPai);
                $('body .enviar-esclarecimento').attr('data-processo', numeroProcesso);
                $('body .enviar-esclarecimento').attr('data-fornecedor', idFornecedor);

                $('#modal-esclarecimento')
                    .find('.modal-header .numero-processo')
                    .html(numeroProcesso);

                $('#modal-esclarecimento').modal('show');
            });

            $('body').on('click', '.enviar-esclarecimento', function() {

                const numeroProcessoPai = $(this).data('processopai');
                const numeroProcesso = $(this).data('processo');
                const idFornecedor = $(this).data('fornecedor');

                const esclarecimento = $('#esclarecimento').val();

                if (!esclarecimento) {
                    alert('Atenção: O campo esclarecimento não pode ser vazio.');
                    $('#esclarecimento').focus();
                    return;
                }

                $.ajax({
                    url: "{{ route('salvar-esclarecimento-cotacao') }}",
                    method: "POST",
                    data: { idFornecedor, numeroProcessoPai, numeroProcesso, esclarecimento},
                    dataType: 'json',
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        if (response) {
                            location.reload();
                        }
                    },
                });
            });
        });
    </script>
@endsection
