
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="dados_socios">
        <thead>
            <tr>
                <th>CPF</th>
                <th>Nome</th>
                <th>Data Nasc.</th>
                <th>Participação</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("Socios"))
                @foreach(session("Socios") as $documento)
                    <tr>
                        <td>{{ $documento->cpf }}</td>
                        <td>{{ $documento->nome }}</td>
                        <td>{{ $documento->dataNasc }}</td>
                        <td>{{ $documento->percentual }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento-socio" title="Remover Documento" 
                                data-cpf="{{ $documento->cpf }}"
                                data-nome_arquivoRG="{{ $documento->nomeRGDepois }}"
                                data-nome_arquivoCPF="{{ $documento->nomeCPFDepois }}"
                                data-nome_arquivoCE="{{ $documento->nomeCEDepois }}"
                                data-pasta="socios"
                                data-nome_sessao="Socios"
                                data-id_tabela="dados_socios"
                                data-conteudo_documentos="conteudo_socios">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>