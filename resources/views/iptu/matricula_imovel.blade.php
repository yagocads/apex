@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-home"></i> Emissão do IPTU</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('pesquisa_matricula_imovel') }}" id="FormIptu">
                            @csrf

                            <div class="form-group">
                                <label for="matricula" class="control-label">Informe a Matrícula do Imóvel</label>
                                <input maxlength="20" id="matricula" type="text" class="form-control {{ $errors->has('matricula') ? ' is-invalid' : '' }}" name="matricula" 
                                    value="" 
                                    required>
            
                                @if ($errors->has('matricula'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('matricula') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>
        
                            <a href="{{ route('principal')  }}">
                                <button type="button" class="btn btn-primary form-group">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </button>
                            <a>

                            <button type="button" class="btn btn-success form-group" id="emitir_iptu">
                                Emitir IPTU
                            </button>
                        </form>
                        <p>Consulte <a href="{{ route('consulta_iptu')  }}">aqui</a> se você fez a solicitação Manual</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#emitir_iptu").prop('disabled', true);
    $("#matricula").mask('00000000');

    function onReCaptchaTimeOut(){
        $("#emitir_iptu").prop('disabled', true);
    }
    function onReCaptchaSuccess(){
        $("#emitir_iptu").prop('disabled', false);
    }


    $("#emitir_iptu").click(function() {

        if ($("#matricula").val() == "") {
            alert("Por favor informe a Matrícula do Imóvel");
            $("#matricula").val("");
            $("#matricula").focus();
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        $("#FormIptu").submit();
        $("#matricula").val("");
        grecaptcha.reset();

        setTimeout(function(){ $("#mdlAguarde").modal('toggle'); }, 5000);
    });

</script>
@endsection