@extends('layouts.tema01')

@section('content')

<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span8">
                <div class="inner-heading">
                    <h2>{{ __('PROGRAMA DE AMPARO AO EMPREGO') }}</h2>
                </div>
            </div>
            <div class="span4">
                <ul class="breadcrumb">
                    <li><a href="{{  url('/') }}"><i class="fa fa-home"></i></a><i class="fa fa-angle-right"></i></li>
                    <li class="active">{{ __('PROGRAMA DE AMPARO AO EMPREGO') }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row justify-content-left">
            <div class="col-md-12">
                @if(strtotime(now()) > strtotime("2020-06-11 08:00:00"))
                    <div class="alert alert-info" style="width: 580px; text-align:center; margin: auto; border-radius: 7px; border: 1px solid #82a5c7; color: #ae7410;">
                            <br>
                            <b>
                                Encerramento das inscrições:<br> 
                                11/06/2020 às 08:00:00
                            </b>
                            <br>
                            <br>
                            <h3>INSCRIÇÕES ENCERRADAS</h3>
                    </div>
                @else
                    <form method="POST" action="{{ route('validacnpjpae') }}" id="formPaeCnpj">
                        @csrf
                        <div class="row formrow">
                            <div class="controls span5 form-label">
                                <label for="pae_cnpj" class="col-md-4 col-form-label text-md-right">
                                {{ __('Informe o CNPJ') }}
                                </label>
                            </div>
                            <div class="span2">
                                <input maxlength="20" id="pae_cnpj" type="text" class="form-input{{ $errors->has('pae_cnpj') ? ' is-invalid' : '' }}" name="pae_cnpj" required>
                                @if ($errors->has('pae_cnpj'))
                                <script>
                                    jQuery(document).ready(function(){
                                        $("#erro").css({
                                            "opacity": "0"
                                        });
                                    });
                                </script>
                                <span class="invalid-feedback" role="alert" id="erro" style="transition: 10s; opacity: 1;">
                                    <strong>{{ $errors->first('pae_cnpj') }}</strong>
                                </span>
                                <br>
                                @endif
                            </div>

                        </div>

                        <div class="row formrow">
                            <div class="controls span5 form-label">
                            </div>
                            <div class="span3 align-left">
                                {!! $showRecaptcha !!}
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>

                        <div class="row formrow">
                            <div class="controls span5 form-label">
                            </div>
                            <div class="span2 align-left">
                                <button type="button" class="btn btn-theme e_wiggle align-right" id="validar_CPF">
                                    {{ __('Solicitar') }}
                                </button>
                            </div>                              
                        </div>
                        <br>

                    </form>
                @endif
                <div class="row">
                    <div class="span1 align-left">
                    </div>
                    <div class="span8 align-left">
                        <a href="{{ route('PAE_PAGINA' )  }}">
                            <button type="button" class="btn btn-theme e_wiggle">
                                {{ __('Voltar') }}
                            </button>
                            <a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</section>
@endsection

@section('post-script')

<script type="text/javascript">

    $("#validar_CPF").prop('disabled', true);
    $("#pae_cnpj").mask('00.000.000/0000-00');
    

    function onReCaptchaTimeOut(){
        $("#validar_CPF").prop('disabled', true);
    }
    function onReCaptchaSuccess(){
        $("#validar_CPF").prop('disabled', false);
    }


    $("#validar_CPF").click(function(){
        if($("#pae_cnpj").val() == ""){
            alert("Por favor informe o CNPJ");
            $("#pae_cnpj").focus();
            return false;
        }

        if(!validarCNPJ($("#pae_cnpj").val())){
            alert("CNPJ Inválido!");
            $("#pae_cnpj").focus();
            return false;
        }

        $("#formPaeCnpj").submit();
    });

    function validarCNPJ(cnpj) {
        cnpj = cnpj.replace(/[^\d]+/g, '');

        if (cnpj == '') return false;

        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")
            return false;

        // Valida DVs
        var tamanho = cnpj.length - 2
        var numeros = cnpj.substring(0, tamanho);
        var digitos = cnpj.substring(tamanho);
        var soma = 0;
        var pos = tamanho - 7;
        for (var i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (var i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;
    }

    function validarCPF(cpf) {
        var add;
        var rev;
        cpf = cpf.replace(/[^\d]+/g, '');
        if (cpf == '') return false;
        // Elimina CPFs invalidos conhecidos	
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return false;
        // Valida 1o digito	
        add = 0;
        for (var i = 0; i < 9; i++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito	
        add = 0;
        for (i = 0; i < 10; i++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }


</script>
@endsection