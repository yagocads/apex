<!-- The Modal -->
<div class="modal fade" id="footer-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Pesquisar Imóvel</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>

            <div class="modal-footer justify-content-center">
                <!-- footer content -->

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('body').on('click', '.text-center a', function(e) {

            e.preventDefault();

            let page = $(this).attr('href');
            let load = $(".ajax_load");

            $.ajax({
                url: 'imoveis/' + page,
                method: 'get',
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    if (response) {
                            load.fadeOut(200);
                            $(".modal-body").html(response);
                            $("#footer-modal").modal('show');
                            $('#tabela_imoveis').DataTable({
                                "stateSave": true,
                                "bDestroy": true,
                                "bPaginate": false,
                                "ordering": true,
                                "info": false,
                                "searching": false,
                                "language": {
                                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
                                }
                            }).columns.adjust().responsive.recalc();
                    } else {
                            alert("Dados não encontrados.");
                    }

                    load.fadeOut(200);
                }
            });

        });
    });
</script>

