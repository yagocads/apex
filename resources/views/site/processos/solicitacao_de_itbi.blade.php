@if( $processo->Serviço == 'Solicitação de ITBI' )
    <div class="justfy-content-center processo-fase">
        @if($processo->Fase != "Finalizado" && $processo->Fase != 'Concluído')
            @if($processo->responsavel == 'Portal')
                <br>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                    <a href="{{ route('formularioEdita', $processo->Chamado) }}" class="btn btn-blue">Editar
                        Solicitação</a>
                </div>
            @endif
        @endif

        @if( $processo->Serviço == 'Solicitação de ITBI' && $processo->FINALIZADO == "andamento")
            @if( $processo->Etapa == "6")
                <br>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> A sua Guia de ITBI já está disponível. Clique no botão para baixar a guia: &nbsp;<br><br>
                    <a href="{{ route('baixarguiaitbi', $processo->Chamado) }}" class="btn btn-blue">Baixar Guia de ITBI</a>
                </div>
            @elseif( $processo->Etapa == "7")
                <br>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;<br><br>
                    <strong>Pendências:</strong> {{$processo->motivo}}<br><br>

                    <a href="{{ route('editarItbi', $processo->Chamado) }}" class="btn btn-blue">Corrigir pendências</a>
                </div>
            @endif
        @endif

        @if($processo->motivo != "" && $processo->FINALIZADO != "andamento")
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Atenção!</strong><br>
            Esta solicitação foi finalizada e teve como conclusão:<br>
            Conclusão: <strong>{{$processo->Fase}}</strong><br>
            @if($processo->motivo != "")
                Motivo: {{$processo->motivo}}<br>
            @endif
        </div>
        @endif
    </div>
@endif