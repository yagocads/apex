<div class="row">
    <div class="col-lg-12">
        <p style="margin-left: 4px;">
            <b>{{__('Objeto')}}: </b> {{ $produtos[0]->objeto }}
        </p>

        <div class="table-responsive" style="padding: 4px !important; margin-bottom: 8px;">
            <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="tabela-produtos-servicos">
                <thead>
                    <tr>
                        <th>{{__('Discriminação')}}</th>
                        <th>{{__('Quantidade')}}</th>
                        <th>{{__('Unidade')}}</th>
                        <th>{{__('Moeda')}}</th>
                        <th width="20%">{{__('Valor')}}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($produtos as $produto)
                        <tr>
                            <td>{{ $produto->discriminacao_item }}</td>
                            <td>{{ $produto->quantidade }}</td>
                            <td>{{ $produto->unidade_fornecimento }}</td>
                            <td>{{ $produto->moedas }}</td>
                            <td>
                                <input type="text" name="cotacoes[]"
                                    class="form-control form-control-sm input-cotacoes"
                                    data-processopai={{ $produto->processo_lecom_pai }}
                                    data-processo="{{ $produto->processo_lecom }}"
                                    data-item="{{ $produto->item }}"
                                    data-quantidade="{{ $produto->quantidade }}"
                                    data-unidade="{{ $produto->unidade_fornecimento }}"
                                    data-moeda={{ $produto->moedas }}
                                    value="{{
                                        !empty($cotacoesComValores[$produto->item]) ?
                                        $cotacoesComValores[$produto->item]
                                        : ''
                                    }}" />
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <label for="observacoes">Observações:</label>
        <textarea name="observacoes" id="observacoes"
            class="form-control form-control-sm mb-2"
            maxlength="255" cols="5" rows="5">{{ !empty($observacoes) ? $observacoes : '' }}</textarea>

        <button type="button" class="btn btn-success salvar-cotacao mt-2">
            {{__('Enviar Cotação')}} <i class="fa fa-send"></i>
        </button>
    </div>
</div>
