@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <div class="card card-servicos">
                    <div class="card-header card-header-title">{{__('Fale Conosco - Formulário para Contato com a Coordenação de Aquisições')}}</div>
                    <div class="card-body py-4">
                        <form id="contactForm" action="{{ route('enviarmensagem') }}" method="POST" role="form" class="contactForm">

                            @csrf
                            <div id="errormessage"></div>

                            <div class="form-row">
                                <div class="col-lg-6 form-group">
                                    <label for="nome_campo1">{{__('Nome')}}</label>
                                    <input maxlength="40" type="text" name="name" class="form-control form-control-sm" id="nameFC" placeholder="{{__('Seu nome')}}"
                                    @if (!(empty($name)))
                                    value="{{ $name }}" readonly
                                    @endif
                                    />
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="nome_campo1">{{__('E-mail')}}</label>
                                    <input maxlength="100" type="email" class="form-control form-control-sm" name="email" id="emailFC" placeholder="{{__('Seu email')}}"
                                    @if (!(empty($email)))
                                    value="{{ $email }}" readonly
                                    @endif
                                    />
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-4 form-group">
                                    <label for="nome_campo1">{{__('Telefone')}}</label>
                                    <input type="text" class="form-control form-control-sm" name="phone" id="phoneFC" placeholder="{{__('Telefone')}}"
                                    @if (!(empty($phone)))
                                    value="{{ $phone }}" readonly
                                    @endif
                                    />
                                </div>

                                <div class="col-lg-4 form-group">
                                    <label for="nome_campo1">{{__('Assunto')}}</label>
                                    <select id="assunto" name="assunto" class="form-control form-control-sm">
                                        <option value="">{{__('Selecione')}}</option>
                                        <option value="Elogio">{{__('Elogio')}}</option>
                                        <option value="Dúvida">{{__('Dúvida')}}</option>
                                        <option value="Reclamação">{{__('Reclamação')}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-12 form-group">
                                    <label for="nome_campo1">{{__('Mensagem')}}</label>
                                    <textarea class="form-control"
                                    id="mensagemfc"
                                    name="message"
                                    rows="4"
                                    placeholder="{{__('Por favor, escreva a sua mensagem')}}"></textarea>
                                </div>

                                <div class="col-lg-12">
                                    {!! $showRecaptcha !!}
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-12 form-group">
                                    <button id="btn_contato" class="btn btn-large btn-primary" type="button" disabled>
                                        <i class="fa fa-envelope"></i> {{__('Enviar Mensagem')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('post-script')
    <script type="text/javascript">

        $("#btn_contato").prop('disabled', true);

        jQuery(document).ready(function(){
            jQuery('.g-recaptcha').attr('data-callback', 'onReCaptchaSuccess');
            jQuery('.g-recaptcha').attr('data-expired-callback', 'onReCaptchaTimeOut');
        });
        function onReCaptchaTimeOut(){
            $("#btn_contato").prop('disabled', true);
        }
        function onReCaptchaSuccess(){
            $("#btn_contato").prop('disabled', false);
        }
    </script>
@endsection
