<div class="form-row">
    <div class="col-lg-5 form-group">
        <label for="categoria_processo">Categoria do processo:</label>
        <select name="categoria_processo" id="categoria_processo" 
            class="form-control form-control-sm {{ ($errors->has('categoria_processo') ? 'is-invalid' : '') }}">
            
            <option></option>
            @foreach($dadosProcessos as $processo)
                <option {{ old('categoria_processo') == $processo->categoria_processo ? 'selected' : '' }}>{{ $processo->categoria_processo }}</option>
            @endforeach

        </select>

        @if ($errors->has('categoria_processo'))
            <div class="invalid-feedback">
                {{ $errors->first('categoria_processo') }}
            </div>
        @endif
    </div>
</div>

<div class="form-row">
    <div class="col-lg-12 form-group">
        <div class="descricao_processo alert alert-info d-none">
            <p>Descrição do processo:</p>
            <p class="descricao_processo_conteudo"></p>
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-lg-12 form-group">
        <label for="objeto_processo">Objeto:</label>
        <textarea name="objeto_processo" id="objeto_processo"
        class="form-control form-control-sm {{ ($errors->has('objeto_processo') ? 'is-invalid' : '') }}" 
        placeholder="Digite aqui" cols="5" rows="6" maxlength="1200">{{ old('objeto_processo') }}</textarea>

        @if ($errors->has('objeto_processo'))
            <div class="invalid-feedback">
                {{ $errors->first('objeto_processo') }}
            </div>
        @endif
    </div>
</div>

<h5><i class="fa fa-home mt-3 mb-3"></i> Dados do Imóvel:</h5>

<div class="form-row">
    <div class="col-lg-3 form-group">
        <label for="matricula_imovel">Matrícula:</label>
        <input type="text" name="matricula_imovel" id="matricula_imovel" 
        class="form-control form-control-sm mask-matricula_imovel {{ ($errors->has('matricula_imovel') ? 'is-invalid' : '') }}"
            value="{{ old('matricula_imovel') }}" />

            @if ($errors->has('matricula_imovel'))
                <div class="invalid-feedback">
                    {{ $errors->first('matricula_imovel') }}
                </div>
            @endif
    </div>

    <div class="col-lg-3 form-group">
        <label for="setor_imovel">Setor:</label>
        <input type="text" name="setor_imovel" id="setor_imovel" 
        class="form-control form-control-sm {{ ($errors->has('setor_imovel') ? 'is-invalid' : '') }}"
            value="{{ old('setor_imovel') }}" maxlength="20" readonly="true" />

            @if ($errors->has('setor_imovel'))
                <div class="invalid-feedback">
                    {{ $errors->first('setor_imovel') }}
                </div>
            @endif
    </div>

    <div class="col-lg-3 form-group">
        <label for="quadra_imovel">Quadra:</label>
        <input type="text" name="quadra_imovel" id="quadra_imovel" 
        class="form-control form-control-sm {{ ($errors->has('quadra_imovel') ? 'is-invalid' : '') }}"
            value="{{ old('quadra_imovel') }}" maxlength="20" readonly="true" />

            @if ($errors->has('quadra_imovel'))
                <div class="invalid-feedback">
                    {{ $errors->first('quadra_imovel') }}
                </div>
            @endif
    </div>

    <div class="col-lg-3 form-group">
        <label for="lote_imovel">Lote:</label>
        <input type="text" name="lote_imovel" id="lote_imovel" 
        class="form-control form-control-sm {{ ($errors->has('lote_imovel') ? 'is-invalid' : '') }}"
            value="{{ old('lote_imovel') }}" maxlength="20" readonly="true" />

            @if ($errors->has('lote_imovel'))
                <div class="invalid-feedback">
                    {{ $errors->first('lote_imovel') }}
                </div>
            @endif
    </div>

    <div class="col-lg-12 imovel_error_mensagem d-none">
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> Matrícula não encontrada.
        </div>
    </div>
</div>

<div class="row d-none">
    <div class="col-lg-12 form-group">
        <div class="conteudo_imoveis_adicionados">
            @include('imovel.tramitacao_imoveis.dados_processo.tabela_imoveis')
        </div>
    </div>
</div>