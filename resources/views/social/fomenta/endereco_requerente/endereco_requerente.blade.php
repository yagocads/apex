
<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Endereço:</h5>
        
        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="cep_requerente">CEP:</label>
                <input type="text" name="cep_requerente" id="cep_requerente" onChange="pesquisaCepRequerente(this.value);" 
                    class="form-control form-control-sm mask-cep {{ ($errors->has('cep_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('cep_requerente') }}" />

                    @if ($errors->has('cep_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cep_requerente') }}
                        </div>
                    @endif
            </div>
                    
            <div class="col-lg-8 form-group">
                <label for="endereco_requerente">Endereço:</label>
                <input type="text" name="endereco_requerente" id="endereco_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('endereco_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('endereco_requerente') }}" maxlength="100" readonly/>

                    @if ($errors->has('endereco_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('endereco_requerente') }}
                        </div>
                    @endif
            </div>

        </div>
        
        <div class="form-row">
            <div class="col-lg-2 form-group">
                <label for="numero_requerente">Número:</label>
                <input type="text" name="numero_requerente" id="numero_requerente" 
                    class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('numero_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('numero_requerente') }}" />

                    @if ($errors->has('numero_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('numero_requerente') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-6 form-group">
                <label for="complemento_requerente">Complemento:</label>
                <input type="text" name="complemento_requerente" id="complemento_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('complemento_requerente') ? 'is-invalid' : '') }}"
                    value="{{ old('complemento_requerente') }}" maxlength="50" />

                    @if ($errors->has('complemento_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('complemento_requerente') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-4 form-group">
                <label for="bairro_requerente">Bairro:</label>
                <input type="text" name="bairro_requerente" id="bairro_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('bairro_requerente') ? 'is-invalid' : '') }}" readonly="true"
                    value="{{ old('bairro_requerente') }}" maxlength="40" />

                    @if ($errors->has('bairro_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('bairro_requerente') }}
                        </div>
                    @endif
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="municipio_requerente">Município:</label>
                <input type="text" name="municipio_requerente" id="municipio_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('municipio_requerente') ? 'is-invalid' : '') }}" readonly="true"
                    value="{{ old('municipio_requerente') }}" maxlength="40" />

                    @if ($errors->has('municipio_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('municipio_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="estado_requerente">Estado:</label>
                <select name="estado_requerente" id="estado_requerente" class="form-control form-control-sm {{ ($errors->has('estado_requerente') ? 'is-invalid' : '') }}" 
                    readonly="true">
                    <option></option>

                    @foreach($estados->geonames as $estado)
                        <option value="{{ $estado->adminCodes1->ISO3166_2 }}" {{ old('estado_requerente') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}>
                            {{ $estado->name }}
                        </option>
                    @endforeach
                </select>

                @if ($errors->has('estado_requerente'))
                    <div class="invalid-feedback">
                        {{ $errors->first('estado_requerente') }}
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-row">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>

            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> As documentações aceitas são conta de luz, água, telefone, internet, correspondência bancária ou boletos de consumo que sejam emitidos em nome do solicitante, em nome dos genitores do solicitante, em nome do cônjuge/companheiro do solicitante (enviando nesse caso a certidão de casamento ou escritura de união estável), em nome de pessoa que more no mesmo endereço desde que emita declaração acompanhada da identidade do declarante ou Declaração do Posto de Saúde.
                </div>
            </div>


            <div class="col-lg-12 form-group">
                <label for="documento_comprovante_residencia_requerente"><i class="fa fa-paperclip"></i> Anexar Comprovante de Residência (mês atual ou mês anterior):</label><br>
                <input type="file" name="documento_comprovante_residencia_requerente" 
                    id="documento_comprovante_residencia_requerente" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_comprovante_residencia_requerente" 
                    data-descricao="Comprovante de Endereço Requerente"
                    data-nome_sessao="documento_endereco_requerente"
                    data-conteudo_documentos="conteudo_endereco_requerente_documento"
                    data-id_tabela="documento_endereco_requerente"
                    data-pasta="endereco_requerente">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
        </div>    
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_endereco_requerente_documento">
            @include('social.fomenta.endereco_requerente.tabela_documentos')
        </div>
    </div>
</div>