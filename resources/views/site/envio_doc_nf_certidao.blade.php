@extends('layouts.tema_principal')

@section('content')
@include('modals.modal_msg')

<div class="container-fluid">

    <div class="alert alert-info">
        <form method="POST" id='form_nfc' action="{{ route('salvar-exigencia-nf-certidao') }}">
            @csrf
        <div class="row">
            <div class="col-lg-12 mt-2">
                <h3><i class="fa fa-exclamation-triangle"></i> Exigência</h3>
                <hr/>
            </div>

            <input type="hidden" name="resp_anexo" id="resp_anexo" value="{{ Auth::user()->name }}" />
            <input type="hidden" name="data_cad" id="data_cad" value="{{ date('d/m/Y', strtotime(now())) }}" />
            <input type="hidden" name="processo" id="processo" value="{{ $chamado }}" />
            <input type="hidden" name="ciclo" id="ciclo" value="{{ $ciclo }}" />

            <div class="col-lg-4">
                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="exigencia">Exigência:</label>
                        <textarea name="exigencia"
                            class="form-control form-control-sm"
                            id="exigencia"
                            rows="8" cols="5" maxlength="255"
                            readonly>{{ $motivo }}</textarea>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="cumprimento_exigencia">Cumprimento de exigência:</label>
                        <textarea name="obs"
                            class="form-control form-control-sm"
                            id="obs"
                            rows="8" cols="5" maxlength="255" required></textarea>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="descricao">Descrição do Documento:<span class="asterisc">*</span></label>
                        <input type="text" name="descricao" id="descricao" class="form-control form-control-sm" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <label for="documento_exigencia" class="d-block"><i class="fa fa-paperclip"></i> Anexar Arquivos:</label>
                        <input type="file" name="documento_exigencia"
                            id="documento_exigencia" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-lg-12 form-group">
                        <button class="btn btn-primary btn-enviar-documento">
                            <i class="fa fa-plus"></i> Adicionar Documento
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

                <div class="conteudo_documentos">
                    @include('certidao.tabela_certidao_nf')
                </div>
            </div>

            <div class="col-lg-12 mt-3">
                <button type="submit" id="salvar" class="btn btn-success btn-enviar-exigencia">Enviar Exigência <i class="fa fa-send"></i></button>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#add").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){
            $(this).parents(".control-group").remove();
        });

        $(".btn-enviar-documento").click(function(e){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            let load = $(".ajax_load");
            let arquivo = $('#documento_exigencia').prop('files')[0];
            let descricao = $("#descricao").val();
            let resp_anexo =  $("#resp_anexo").val();
            let data_cad =  $("#data_cad").val();
            let obs = $("#obs").val();
            let processo = $("#processo").val();

            if (!validarAnexo(arquivo, ["pdf"],'10485760')) {
                return false;
            }

            if (!arquivo) {
                alert("Escolha um arquivo para continuar");
                return false;
            }

            if (descricao == '') {
                alert("A descrição do arquivo não pode ser vazia.");
                $('#descricao').focus();
                return false;
            }

            let formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("resp_anexo", resp_anexo);
            formData.append("data_cad", data_cad);
            formData.append("obs", obs);
            formData.append("exigencia", 1);
            formData.append("processo", processo);

            let url = "{{ route('salvar-doc-cn') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }
                    $('#descricao').val('');
                    $('#documento_exigencia').val('');
                    $(".conteudo_documentos").html(response);
                }
            });
        });

        $("#salvar").click(function(e){
            e.preventDefault();
            if (!$(".remover_documento").length){
                alert("Favor enviar os documentos solicitados");
                return false;
            }
            if ($("#obs").val() == ''){
                alert("O campo cumprimento da exigência não pode ser vazio");
                $("#obs").focus();
                return false;
            }
            $('#form_nfc').submit();
        });
    });
</script>
@endsection
