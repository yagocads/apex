
<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-user mt-3 mb-3"></i> Informações Pessoais:</h5>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="cpf_informacoes_pessoais">CPF:</label>
                <input type="text" name="cpf" id="cpf_informacoes_pessoais" 
                    class="form-control form-control-sm mask-cpf {{ ($errors->has('cpf') ? 'is-invalid' : '') }}" 
                    value="{{ $cpfOuCnpj }}" 
                    readonly="true" />

                    @if ($errors->has('cpf'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpf') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="data_nascimento">Data de Nascimento:</label>
                <input type="date" name="data_nascimento" id="data_nascimento"
                    max="{{ date('Y-m-d') }}" 
                    class="form-control form-control-sm {{ ($errors->has('data_nascimento') ? 'is-invalid' : '') }}" 
                    @auth
                        value="{{$cgm->aCgmPessoais->z01_nasc}}" readonly
                    @else
                        value="{{ old('data_nascimento') }}"
                    @endif
                    />

                    @if ($errors->has('data_nascimento'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_nascimento') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="sexo">Sexo:</label>
                <select name="sexo" id="sexo" 
                    class="form-control form-control-sm {{ ($errors->has('sexo') ? 'is-invalid' : '') }}"
                    @auth
                        readonly
                    @endauth
                    >
                    <option></option>
                    @auth
                        <option value="M" {{ $cgm->aCgmPessoais->z01_sexo == 'M' ? 'selected' : '' }}>Masculino</option>
                        <option value="F" {{ $cgm->aCgmPessoais->z01_sexo == 'F' ? 'selected' : '' }}>Feminino</option>
                    @else
                        <option value="M" {{ old('sexo') == 'M' ? 'selected' : '' }}>Masculino</option>
                        <option value="F" {{ old('sexo') == 'F' ? 'selected' : '' }}>Feminino</option>
                    @endauth
                </select>

                @if ($errors->has('sexo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sexo') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="nome">Nome:</label>
                <input type="text" name="nome" id="nome" 
                    class="form-control form-control-sm {{ ($errors->has('nome') ? 'is-invalid' : '') }}"
                    maxlength="40"
                    @auth
                        value="{{convert_accentuation($cgm->aCgmPessoais->z01_nome)}}" readonly
                    @else
                        value="{{ old('nome') }}"
                    @endauth
                    />

                    @if ($errors->has('nome'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="nome_social">Nome Social:</label>
                <input type="text" name="nome_social" id="nome_social" 
                    class="form-control form-control-sm {{ ($errors->has('nome_social') ? 'is-invalid' : '') }}"
                    value="{{ old('nome_social') }}" maxlength="40" />

                    @if ($errors->has('nome_social'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_social') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="nome_mae">Nome da Mãe:</label>
                <input type="text" name="nome_mae" id="nome_mae" 
                    class="form-control form-control-sm {{ ($errors->has('nome_mae') ? 'is-invalid' : '') }}"
                    maxlength="40" 
                    @auth
                        @if($cgm->aCgmPessoais->z01_mae != "")
                            value="{{ convert_accentuation($cgm->aCgmPessoais->z01_mae) }}" readonly
                        @else
                            value="{{ old('nome_mae') }}"
                        @endif
                    @else
                        value="{{ old('nome_mae') }}"
                    @endauth
                    />

                    @if ($errors->has('nome_mae'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_mae') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="nome_pai">Nome do Pai:</label>
                <input type="text" name="nome_pai" id="nome_pai" 
                    class="form-control form-control-sm {{ ($errors->has('nome_pai') ? 'is-invalid' : '') }}" 
                    maxlength="40" 
                    @auth
                        @if($cgm->aCgmPessoais->z01_pai != "")
                            value="{{ convert_accentuation($cgm->aCgmPessoais->z01_pai) }}" readonly
                        @else
                            value="{{ old('nome_pai') }}"
                        @endif
                    @else
                        value="{{ old('nome_pai') }}"
                    @endauth
                   />

                    @if ($errors->has('nome_pai'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_pai') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="identidade">Identidade:</label>
                <input type="text" name="identidade" id="identidade" 
                    class="form-control form-control-sm {{ ($errors->has('identidade') ? 'is-invalid' : '') }}" 
                    maxlength="20" 
                    @auth
                        @if($cgm->aCgmAdicionais->z01_ident != "")
                            value="{{ $cgm->aCgmAdicionais->z01_ident }}" readonly
                        @else
                            value="{{ old('identidade') }}"
                        @endif
                    @else
                        value="{{ old('identidade') }}"
                    @endauth
                    />

                    @if ($errors->has('identidade'))
                        <div class="invalid-feedback">
                            {{ $errors->first('identidade') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="orgao_emissor">Órgão Emissor:</label>
                <input type="text" name="orgao_emissor" id="orgao_emissor" 
                    class="form-control form-control-sm {{ ($errors->has('orgao_emissor') ? 'is-invalid' : '') }}" 
                    maxlength="45" 
                    @auth
                        @if($cgm->aCgmAdicionais->z01_identorgao != "")
                            value="{{ $cgm->aCgmAdicionais->z01_identorgao }}" readonly
                        @else
                            value="{{ old('orgao_emissor') }}"
                        @endif
                    @else
                        value="{{ old('orgao_emissor') }}"
                    @endauth
                    />

                    @if ($errors->has('orgao_emissor'))
                        <div class="invalid-feedback">
                            {{ $errors->first('orgao_emissor') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="data_emissao">Data Emissão:</label>
                <input type="date" name="data_emissao" id="data_emissao" 
                    max="{{ date('Y-m-d') }}"
                    class="form-control form-control-sm {{ ($errors->has('data_emissao') ? 'is-invalid' : '') }}" 
                    @auth
                        @if($cgm->aCgmAdicionais->z01_identdtexp != "")
                            value="{{$cgm->aCgmAdicionais->z01_identdtexp}}" readonly
                        @else
                            value="{{ old('data_emissao') }}"
                        @endif
                    @else
                        value="{{ old('data_emissao') }}"
                    @endauth
                />

                    @if ($errors->has('data_emissao'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_emissao') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="estado_civil">Estado Civil:</label>
                <select name="estado_civil" id="estado_civil" 
                    class="form-control form-control-sm {{ ($errors->has('estado_civil') ? 'is-invalid' : '') }}"
                    @auth
                        readonly
                    @endauth
                >
                    <option></option>
                    @auth
                        <option value="solteiro" {{ $cgm->aCgmPessoais->z01_estciv == "1" ? "selected" : "" }} >Solteiro</option>
                        <option value="casado" {{ $cgm->aCgmPessoais->z01_estciv == "2" ? "selected" : "" }}  >Casado</option>
                        <option value="viúvo" {{ $cgm->aCgmPessoais->z01_estciv == "3" ? "selected" : "" }}>Viúvo</option>
                        <option value="divorciado" {{ $cgm->aCgmPessoais->z01_estciv == "4" ? "selected" : "" }}>Divorciado</option>
                        <option value="separado_consensual" {{ $cgm->aCgmPessoais->z01_estciv == "5" ? "selected" : "" }}>Separado Consensual</option>
                        <option value="separado_judicial" {{ $cgm->aCgmPessoais->z01_estciv == "6" ? "selected" : "" }} >Separado Judicial</option>   
                        <option value="uniao_estavel" {{ $cgm->aCgmPessoais->z01_estciv == "7" ? "selected" : "" }} >União Estável</option>   
                    @else
                        <option value="solteiro" {{ old('estado_civil') == 'solteiro' ? 'selected' : '' }}>Solteiro</option>
                        <option value="casado" {{ old('estado_civil') == 'casado' ? 'selected' : '' }}>Casado</option>
                        <option value="viúvo" {{ old('estado_civil') == 'viúvo' ? 'selected' : '' }}>Viúvo</option>
                        <option value="divorciado" {{ old('estado_civil') == 'divorciado' ? 'selected' : '' }}>Divorciado</option>
                        <option value="separado_consensual" {{ old('estado_civil') == 'separado_consensual' ? 'selected' : '' }}>Separado Consensual</option>
                        <option value="separado_judicial" {{ old('estado_civil') == 'separado_judicial' ? 'selected' : '' }}>Separado Judicial</option>
                        <option value="uniao_estavel" {{ old('estado_civil') == 'uniao_estavel' ? 'selected' : '' }}>União Estável</option>
                    @endauth
                </select>

                @if ($errors->has('estado_civil'))
                    <div class="invalid-feedback">
                        {{ $errors->first('estado_civil') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="naturalidade">Naturalidade:</label>
                <input type="text" name="naturalidade" id="naturalidade" 
                    class="form-control form-control-sm {{ ($errors->has('naturalidade') ? 'is-invalid' : '') }}" 
                    value="{{ old('naturalidade') }}" maxlength="100" />

                    @if ($errors->has('naturalidade'))
                        <div class="invalid-feedback">
                            {{ $errors->first('naturalidade') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="nacionalidade">Nacionalidade:</label>
                <select name="nacionalidade" id="nacionalidade" 
                    class="form-control form-control-sm {{ ($errors->has('nacionalidade') ? 'is-invalid' : '') }}"
                    @auth
                        readonly
                    @endauth
                >
                    <option></option>
                    @auth
                        <option value="brasileira" {{ $cgm->aCgmPessoais->z01_nacion == "1" ? "selected" : "" }}  >Brasileira</option>
                        <option value="estrangeira" {{ $cgm->aCgmPessoais->z01_nacion == "2" ? "selected" : "" }}  >Estrangeira</option>
                    @else
                        <option value="brasileira" {{ old('nacionalidade') == 'brasileira' ? 'selected' : '' }}>Brasileira</option>
                        <option value="estrangeira" {{ old('nacionalidade') == 'estrangeira' ? 'selected' : '' }}>Estrangeira</option>
                    @endauth
                </select>

                @if ($errors->has('nacionalidade'))
                    <div class="invalid-feedback">
                        {{ $errors->first('nacionalidade') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>
            
            <div class="col-lg-6 form-group">
                <label for="documento_identidade"><i class="fa fa-paperclip"></i> Anexar Documento de Identidade:</label>
                <input type="file" name="documento_identidade" 
                    id="documento_identidade" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary mb-3 btn-enviar-documento"
                    data-nome_input="documento_identidade" 
                    data-descricao="Identidade"
                    data-nome_sessao="documentos_informacoes_pessoais"
                    data-conteudo_documentos="conteudo_informacoes_pessoais_documentos"
                    data-id_tabela="documentos_informacoes_pessoais"
                    data-pasta="informacoes_pessoais">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>

            <div class="col-lg-6 form-group">
                <label for="documento_cpf"><i class="fa fa-paperclip"></i> Anexar Documento CPF:</label>
                <input type="file" name="documento_cpf" 
                    id="documento_cpf" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_cpf" 
                    data-descricao="CPF"
                    data-nome_sessao="documentos_informacoes_pessoais"
                    data-conteudo_documentos="conteudo_informacoes_pessoais_documentos"
                    data-id_tabela="documentos_informacoes_pessoais"
                    data-pasta="informacoes_pessoais">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>

            @if(session('vencimentoIPTU')==0)
                <div class="col-lg-12 mt-3">
                    <div class="alert alert-info">
                        <i class="fa fa-volume-up"></i> OBS: Faça também uma selfie segurando, ao lado de sua face, 
                        o documento oficial de identificação, com o lado que contém a 
                        foto voltado para a câmera. É proibida a utilização de qualquer adereço,
                        vestimenta ou aparato que impossibilite a completa visão de sua face, 
                        tais como óculos, bonés, gorros, entre outros. Não tire fotos de fotos, 
                        precisamos de uma foto de você mesmo(a).
                    </div>
                </div>

                <div class="col-lg-6 form-group">
                    <label for="documento_selfie_identificacao"><i class="fa fa-paperclip"></i> Anexar Selfie de Identificação:</label>
                    <input type="file" name="documento_selfie_identificacao" 
                        id="documento_selfie_identificacao" />
                </div>

                <div class="col-lg-12 mb-3">
                    <button class="btn btn-primary btn-enviar-documento"
                        data-nome_input="documento_selfie_identificacao" 
                        data-descricao="Selfie de Identificação"
                        data-nome_sessao="documentos_informacoes_pessoais"
                        data-conteudo_documentos="conteudo_informacoes_pessoais_documentos"
                        data-id_tabela="documentos_informacoes_pessoais"
                        data-pasta="informacoes_pessoais">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
            @endif
        </div>    
    </div>  
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_informacoes_pessoais_documentos">
            @include('cadastro.cgm.informacoes_pessoais.tabela_documentos')
        </div>
    </div>
</div>
