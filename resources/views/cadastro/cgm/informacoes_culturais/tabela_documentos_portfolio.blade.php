
<div class="table-responsive py-1 pl-1 pr-1">
    <table class="table table-striped table-bordered dataTable dt-responsive w-100" id="documentos_portfolio">
        <thead>
            <tr>
                <th>Descrição</th>
                <th>Arquivo</th>
                <th data-priority="1">Ações</th>
            </tr>
        </thead>
        <tbody>
            @if (session()->has("documentos_informacoes_culturais_portfolio"))
                @foreach(session("documentos_informacoes_culturais_portfolio") as $documento)
                    <tr>
                        <td>{{ $documento->descricao }}</td>
                        <td>{{ $documento->nome_original }}</td>
                        <td class="text-center">
                            <button class="btn btn-danger btn-sm btn-excluir-documento" title="Remover Documento" 
                                data-nome_arquivo="{{ $documento->nome_novo }}"
                                data-pasta="informacoes_culturais"
                                data-nome_sessao="documentos_informacoes_culturais_portfolio"
                                data-id_tabela="documentos_portfolio"
                                data-nome_tabela="tabela_documentos_portfolio"
                                data-conteudo_documentos="conteudo_documentos_informacoes_culturais_portfolio">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>