<!-- The Modal -->
<div class="modal fade" id="modal-motivo-exclusao">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">
                    {{__('Motivo da Exclusão')}}
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <form method="POST">
                    <div class="form-group">
                        <label for="motivo">{{__('Motivo')}}:</label>
                        <textarea name="motivo" id="motivo" class="form-control form-control-sm"></textarea>
                    </div>

                    <button class="btn btn-danger btn-excluir-fornecedor">{{__('Excluir')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
