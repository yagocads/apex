<div class="row">
    <div class="col-lg-12">
        <label>Você é um artista ou trabalhador da Cultura? <span class="text-danger">*</span></label>
        
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" 
                    name="artista" value="sim"
                    {{ old('artista') == 'sim' ? 'checked' : '' }}>
                Sim
            </label>
        </div>

        <div class="form-check">
            <label class="form-check-label">
            <input type="radio" class="form-check-input" 
                name="artista" value="nao"
                {{ old('artista') == 'nao' ? 'checked' : '' }}>
            Não
            </label>
        </div>

        <div class="form-check">
            <input type="hidden" name="artista" 
                class="form-check-input {{ ($errors->has('artista') ? 'is-invalid' : '') }}"
                disabled />

            @if ($errors->has('artista'))
                <div class="invalid-feedback">
                    {{ $errors->first('artista') }}
                </div>
            @endif
        </div>
    </div>
</div>
