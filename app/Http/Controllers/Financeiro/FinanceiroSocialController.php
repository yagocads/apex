<?php

namespace App\Http\Controllers\Financeiro;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Cidadao\CgmController;
use App\Models\DividaAtiva;
use App\Models\DividaAtivaDocumento;
use App\Support\FinanceiroSupport;
use Auth;
use DB;

class FinanceiroSocialController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

	public function prestacaoContasPAE(){
        
        $empresaAprovada = DB::connection('lecom')
        ->table('v_consulta_servico_pae')
        ->where('CNPJ', '=', Auth::user()->cpf )
        ->where('Fase', '=', "Benefício Concedido" )
        ->first();

        // dd($empresaAprovada);

        if(!is_null($empresaAprovada)){
            if($empresaAprovada->Fase == "Benefício Concedido"){

                $empresaFuncionarios = DB::connection('lecom')
                ->table('v_consulta_funcionarios_pae')
                ->where('CNPJ', '=', Auth::user()->cpf )
                ->orderby('NOME')
                ->get();

                $mesesEnviados = DB::connection('integracao')
                ->table('lecom_pae_prestacao_contas')
                ->where('CNPJ', '=', Auth::user()->cpf )
                ->get();

                $achouMaio = false;
                $achouJunho = false;
                $listaMeses = [
                    "Maio/2020",
                    "Junho/2020",
                    "Julho/2020",
                    "Agosto/2020",
                    "Setembro/2020",
                    "Outubro/2020",
                    "Novembro/2020",
                    "Dezembro/2020",
                    "Janeiro/2021",
                    "Fevereiro/2021",
                    "Março/2021",
                    "Abril/2021",
                    "Maio/2021",
                    "Junho/2021",
                    "Julho/2021",
                    "Agosto/2021",
                    "Setembro/2021",
                    "Outubro/2021",
                    "Novembro/2021",
                    "Dezembro/2021",                    
                ];

                foreach($mesesEnviados as $mesEnviado){
                    foreach($listaMeses as $chaveMes => $mesListado){
                        if($mesEnviado->mes == $mesListado){
                            unset($listaMeses[$chaveMes]); 
                        }
                    }
                }

                return view('financeiro.social.prestacao_contas_pae', ["funcionarios" => $empresaFuncionarios, 'mesesPendentes' => $listaMeses ] );

            }
            else{
                return view('errors.erro_geral', [
                    "icone" => "fa-home",
                    "titulo" => "Prestação de Contas PAE",
                    "mensagem" => "O Benefício solicitado ainda não está aprovado.<br>",
                    "codigo" => "PAE_005",
                    "rota" => "servicos",
                    "retorno" => "financeiro",
                    ]);
            }
        }
        else{
            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Prestação de Contas PAE",
                "mensagem" => "Não encontramos nenhum benefício cadastrado para este CNPJ.<br>",
                "codigo" => "PAE_006",
                "rota" => "servicos",
                "retorno" => "financeiro",
                ]);
        }


	}

    public function gravarPrestacaoContas(Request $request){

        $errors = array();

        if (!session()->has("documentos_funcionarios")) {
            array_push($errors, 'É necessário anexar o SEFIP da Empresa');
            return back()->withErrors($errors);
        }

        
        foreach(session("documentos_funcionarios") as $documento){

            $mesEnviado = DB::connection('integracao')
            ->table('lecom_pae_prestacao_contas')
            ->where('CNPJ', '=', Auth::user()->cpf )
            ->where('mes', '=', $documento->mes )
            ->get();

            if(count($mesEnviado) == 0){
                $id = DB::connection('integracao')
                ->table('lecom_pae_prestacao_contas')->insertGetId(
                    [
                        'cnpj' => $request->cnpj,
                        'mes' => $documento->mes,
                        'comentario' =>  $documento->comentario,
                        'status' => 1,
                    ]
                );
            } 
            
            $id_doc = DB::connection('integracao')
            ->table('lecom_pae_prestacao_contas_documentos')->insertGetId(
                [
                    'id_prestacao_contas' => $id,
                    'cnpj' => $request->cnpj,
                    'tipo_documento' => $documento->tipo,
                    'descricao_documento' => $documento->descricao,
                    'mes' => $documento->mes,
                    'cpf_funcionario' => $documento->cpf,
                    'nome_arquivo' =>  $documento->nome_novo,
                    'data_cadastro' =>  date("Y-m-d H:i:s"),
                ]
            );
        }

        session()->forget("documentos_funcionarios");

        return redirect()->route('prestacaoContasPAE')->with( 'mensagem', 'SUCESSO: As informações foram gravadas com sucesso!' );;

    }

    public function salvarDocumentoFuncionarioPae(Request $request){

        // dd( $request );

        $extensoesPermitidas = ["jpeg", "png", "jpg", "gif", "pdf","JPEG", "PNG", "JPG", "GIF", "PDF"];
        $extensao = $request->arquivo->extension();

        if (!in_array($extensao, $extensoesPermitidas)) {
            return ['error' => 'O arquivo deve ser uma imagem ou um pdf com as seguintes extensões (jpeg, png, jpg, gif, pdf).'];
        }

        $tamanhoArquivo = $request->arquivo->getClientSize();

        if ($tamanhoArquivo > 8400000) {
            return ['error' => 'O arquivo deve ter no máximo 8MB.'];
        }

        if($request->cpf != "" ){
            if (!empty(session("documentos_funcionarios"))) {
                foreach(session("documentos_funcionarios") as $documento){
                    if( $documento->cpf == $request->cpf ){
                        return ['error' => "O documento para este funcionário já foi incluído. Caso deseje alterar o documento, remova-o da tabela de documentos."];
                    }
                }
            }
        }

        $cpf = remove_character_document(Auth::user()->cpf);

        $array_cpf = explode("|", $request->nome);

        $nomeNovoArquivo =  $cpf . "_" . $request->descricao . "_" . date('His') . "." . $extensao;
        $nomeNovoArquivo = remove_accentuation(str_replace(" ", "_", $nomeNovoArquivo));

        $upload = $request->arquivo->storeAs('pae_prestacao_contas', $nomeNovoArquivo, 'ftp');

        if ($upload) {

            session()->push("documentos_funcionarios", (object) [
                "mes" =>  $request->mes,
                "cpf" =>  isset($array_cpf[0])?$array_cpf[0]:"",
                "nome" => isset($array_cpf[1])?$array_cpf[1]:"",
                "tipo" => $request->descricao,
                "descricao" => $request->descricao_doc,
                "comentario" => $request->comentario,
                "nome_original" => $request->arquivo->getClientOriginalName(),
                "nome_novo" => $nomeNovoArquivo
            ]);

            return view('financeiro.social.tabela_documentos_funcionarios');
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function salvarTermoAdesao(Request $request){
        $extensoesPermitidas = ["jpeg", "png", "jpg", "gif", "pdf","JPEG", "PNG", "JPG", "GIF", "PDF"];
        $extensao = $request->arquivo->extension();

        if (!in_array($extensao, $extensoesPermitidas)) {
            return ['error' => 'O arquivo deve ser uma imagem ou um pdf com as seguintes extensões (jpeg, png, jpg, gif, pdf).'];
        }

        $tamanhoArquivo = $request->arquivo->getClientSize();

        if ($tamanhoArquivo > 2048000) {
            return ['error' => 'O arquivo deve ter no máximo 2MB.'];
        }

        $cpf = remove_character_document(Auth::user()->cpf);

        $descricao = "TERMO";

        $nomeNovoArquivo =  $cpf . "_" . $descricao . "_" . uniqid() . "." . $extensao;
        $nomeNovoArquivo = remove_accentuation(str_replace(" ", "_", $nomeNovoArquivo));

        $upload = $request->arquivo->storeAs('pae_termo_adesao', $nomeNovoArquivo, 'ftp');

        if ($upload) {

            session()->push("termo_adesao", (object) [
                "descricao" => $descricao,
                "nome_original" => $request->arquivo->getClientOriginalName(),
                "nome_novo" => $nomeNovoArquivo
            ]);

            return view('financeiro.social.tabela_termo_adesao');
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removerDocumentoFuncionarioSessao($document)
    {
        $funcionarios = session()->get("documentos_funcionarios");

        if (!empty($funcionarios)) {

            foreach($funcionarios as $key => $funcionario) {

                if (in_array($document, (array) $funcionario)) {
                    unset($funcionarios[$key]);
                    session()->forget("documentos_funcionarios");
                    session()->put('documentos_funcionarios', $funcionarios);

                    $retorno = Storage::disk('ftp')->delete('pae_prestacao_contas/' . $document);
                }
            }
        }

        return view('financeiro.social.tabela_documentos_funcionarios');
    }

    public function removerTermoAdesaoSessao($document)
    {
        $funcionarios = session()->get("termo_adesao");

        if (!empty($funcionarios)) {

            foreach($funcionarios as $key => $funcionario) {

                if (in_array($document, (array) $funcionario)) {
                    unset($funcionarios[$key]);
                    session()->forget("termo_adesao");
                    session()->put('termo_adesao', $funcionarios);

                    $retorno = Storage::disk('ftp')->delete('pae_termo_adesao/' . $document);
                }
            }
        }

        return view('financeiro.social.tabela_termo_adesao');
    }
}
