@extends('layouts.tema_principal')

@section('content')
    <br>
    <style>
        .texto {
            text-align:justify; 
            font-size:18px;
            font-weight:400;
            padding-left:0; 
            padding-top: 5px;
            line-height: 30px;
            color: #075c7b;
        }
        .img-topo{
            width: 100%;
        }
        .img-titulo{
            margin-top: 20px;
            width: 100%;
        }
        .img-botao{
            width: 100%;
        }
        .fonte_tabela{
            font-size: 24px;
            font-weight: bold;

        }
        a.fonte_tabela{
            color: red;
            text-decoration: none;
        }
        a.fonte_tabela:hover{
            color: gold;
        }
    </style>

<div class="container-fluid">
    <div class="row justify-content-md-center">
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <img src="img/fomenta/BANNER_TOPO.png" class="img-topo" alt=""/>
                </div>
                <div class="col-lg-12">
                    <p>
                        <h3><strong>Leia atentamente as informações e  depois faça os 03 primeiros passos para realizar sua inscrição.</strong></h3>
                    </p>
                </div>
                <div class="col-lg-6">
                <img src="img/fomenta/o_que_e.png" class="img-titulo" alt=""/>
                    <p class="texto">É uma ação da Prefeitura de Maricá que disponibilizará linhas de crédito emergenciais para os microempreendedores do Município de Maricá. Devido à pandemia a ação assumiu caráter emergencial e irá ofertar linha de crédito de até $ 5.000,00 (cinco mil) Mumbucas para MEIs inscritos no município até 31/03/2020.</p>
                </div>

                <div class="col-lg-6">
                    <img src="img/fomenta/condicoes.png" class="img-titulo" alt=""/>
                    <p class="texto">A linha de crédito emergencial para MEIs oferecerá taxa de juros de 0,00% a.a. (zero por cento ao ano), podendo ter carência de até 12 (doze) meses para início do pagamento do financiamento, o qual poderá alcançar até 18 (dezoito) parcelas mensais.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <img src="img/fomenta/quem_pode.png" class="img-titulo" alt=""/>
                    <p class="texto">A ação é destinada aos Microempreendedores Individuais – MEIs residentes e com atuação de sua atividade econômica no Município de Maricá, inscritos até 31/03/2020 que sejam aprovados na análise de crédito da AgeRio.  Serão priorizados os MEIs que não foram beneficiados pelos Programas do PAT e do PAE, e com maior tempo de abertura.</p>
                </div>
                <div class="col-lg-6">
                    <img src="img/fomenta/inscrver.png" class="img-titulo" alt=""/>
                    <p class="texto">A inscrição se dará unicamente através do site do Programa Fomenta Maricá no Portal do SIM sendo certo que deverá estar munido dos documentos necessários para enviá-los para análise. Ao finalizar a sua inscrição tome nota do seu protocolo, pois este será utilizado para acompanhamento de todo o processo.</p>
                    <p class="texto">A inscrição poderá ser feita pelo computador e via celular.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <img src="img/fomenta/documentos.png" class="img-titulo" alt=""/>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped table-bordered dataTable dt-responsive w-100 fonte_tabela" id="tabela_documentos">
                        <tbody>
                            <tr>
                                <td>
                                    a) Documento de Identidade e CPF do solicitante;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b) Comprovante de residência do solicitante situado no Município de Maricá emitido nos últimos 90 (noventa) dias, o qual poderá ser representado por conta de luz, água, telefone, internet, correspondência bancária ou boletos de consumo que sejam emitidos:<br>
                                    b.1) em nome do solicitante;<br>
                                    b.2) em nome dos genitores do solicitante;<br>
                                    b.3) em nome do cônjuge/companheiro do solicitante desde que acompanhado do envio da certidão de casamento ou escritura de união estável ;<br>
                                    b.4) em nome de pessoa que more no mesmo endereço desde que emita declaração acerca da ciência das repercussões criminais da declaração falsa e acompanhada de cópia de documento de identidade do declarante; 
                                    <a href="{{ asset('/docs/DECLARACAO_ENDEREÇO.docx') }}" class="fonte_tabela" download="Declaração de Endereço">(LINK PARA DOWNLOAD)</a>
                                    <br>
                                    b.5) Declaração do Posto de Saúde. <br>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c) Foto do solicitante segurando o seu documento de identificação com a foto voltada para a câmera;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d) Certificado da Condição de Microempreendedor Individual (CCMEI) ou Cartão do Cadastro Nacional de Pessoas Jurídicas (CNPJ);
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e) Comprovação do endereço do local de exercício da atividade econômica neste Município de Maricá, a qual também obedecerá aos ditames previstos na letra “b” deste item;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    f) Comprovação do exercício da atividade econômica que deverá ser demonstrada de forma inequívoca, podendo ser utilizado como meios os seguintes elementos, dentre outros igualmente convincentes:<br>
                                        f.1)      Foto do estabelecimento comercial, se houver;<br>
                                        f.2)      Foto de rede social ativa antes de março de 2020 anunciando o desempenho de atividade empreendedora;<br>
                                        f.3)      Foto de cartão de visitas/divulgação contendo a atividade e número de contato do solicitante;<br>
                                        f.4)      Foto de anúncio de vendas/ oferta do serviço;<br>
                                        f.5)       Declaração de tomador de serviço desde que contenha cláusula acerca da ciência das repercussões criminais da declaração falsa e esteja acompanhada de cópia de documento de identidade do declarante; 
                                        <a href="{{ asset('/docs/DECLARACAO_TOMADOR_SERVIÇO.docx') }}" class="fonte_tabela" download="Declaração de Tomador de Serviços">(LINK PARA DOWNLOAD)</a><br>
                                        f.6)      Outros sites que informem acerca da atividade desempenhada próprio ou de terceiros;<br>
                                        f.7)      Nota fiscal de faturamento;<br>
                                        f.8) Alvará de funcionamento do MEI dentro do prazo de validade.<br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <img src="img/fomenta/analises.png" class="img-titulo" alt=""/>
                    <p  class="texto">As análises de crédito ficarão a cargo da Agência de Fomento do Estado do Rio de Janeiro (AgeRio), instituição financeira governamental que conta com 16 anos de experiência e um corpo  funcional altamente qualificado para realização das ações, obedecendo para tanto a ordem de chegada, a partir da data do envio de todas as documentações estabelecidas.</p>
                </div>

                <div class="col-lg-6">
                    <img src="img/fomenta/quanto.png" class="img-titulo" alt=""/>
                    <p  class="texto">Será feita uma análise do valor solicitado (de $300,00 a $5.000,00 Mumbucas) pelo valor liberado pela instituição financeira a partir da análise de crédito e das documentações realizadas pela Agerio, variando conforme a análise realizada. Pode-se obter o valor total ou parcial da linha de crédito o qual será concedido em Moeda Mumbuca.</p>
                    <p  class="texto">Depois da análise aprovada, o pagamento do valor solicitado será depositado, mediante contrato assinado, em uma conta aberta automaticamente no Banco Mumbuca. Todas as transações de utilização da verba financiada serão feitas através do App E-DINHEIRO. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <img src="img/fomenta/disponivel.png" class="img-titulo" alt=""/>
                    <p  class="texto">As inscrições poderão ser realizadas de 28/08 até o dia 04/09. As análises começarão a ser feitas após finalizadas as inscrições. Os valores serão disponibilizados de acordo com o correto envio das documentações e das análises realizadas pela instituição financeira, respeitando os critérios de prioridades estabelecidos. Todo o acompanhamento poderá ser realizado através do Portal do SIM com a utilização do CNPJ, data de abertura do MEI e do Protocolo de solicitação gerado. </p>
                </div>
                <div class="col-lg-6">
                    <img src="img/fomenta/quantidade.png" class="img-titulo" alt=""/>
                    <p  class="texto">Serão atendidos tantos microempreendedores quantos forem possíveis, até atingir o valor total disponibilizado o qual inicialmente, é de 7 (sete) milhões de Mumbucas.</p>
                    <img src="img/fomenta/resposta.png" class="img-titulo" alt=""/>
                    <p  class="texto">Os prazos serão variáveis respeitando todas as determinações legais de análise de crédito a fim de garantir a lisura do processo e das garantias estabelecidas da responsabilidade da análise,  terão um limite de até 120 dias, a contar da entrega dos documentos.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p>
                        <h3><strong>Os 04 passos para o Fomenta Maricá Mei Emergencial: Simule, cadastre, inscreva-se e acompanhe!</strong></h3>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <a href="{{ asset('/docs/Simulador_Marica.xls') }}" download="Simulador Maricá">
                        <img src="img/fomenta/1passo_simulacao.png" class="img-topo" alt=""/>
                    </a>
                </div>
                <div class="col-lg-3">
                    @if(strtotime(now()) < strtotime("2020-09-04 18:00:00"))
                    <a href="https://forms.gle/wRnyNsMU8ShAz3Wg8" target="_blank">
                    @endif
                        <img src="img/fomenta/2passo_cadastro.png" class="img-topo" alt="" id="passo2"/>
                    @if(strtotime(now()) < strtotime("2020-09-04 18:00:00"))
                    </a>
                    @endif
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('cadastrofomenta') }}" >
                        <img src="img/fomenta/3passo_solicite.png" class="img-topo" alt=""/>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="{{ route('fomentaconsultarsituacao') }}" >
                        <img src="img/fomenta/4passo_consulta.png" class="img-topo" alt=""/>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <img src="img/fomenta/quadro_informacoes.png" class="img-topo" alt=""/>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                    <img src="img/fomenta/contato.png" class="img-topo" alt=""/>
                    <br>
                    <a href="mailto:sacfomentamarica@marica.rj.gov.br" target="_blank">
                        <img src="img/fomenta/sac.png" class="img-topo" alt=""/>
                    </a>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
            <br>

        </div>
    </div>
</div>
<br>
<br>
<br>
@endsection

@section('post-script')

<script type="text/javascript">

    @if(strtotime(now()) >= strtotime("2020-09-04 18:00:00"))
        $('#passo2').click(function() {
            alert("INSCRIÇÕES ENCERRADAS");
        });
    @endif

</script>

@endsection