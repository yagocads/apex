<footer style="background-color: #FFF">
    <div class="container" style="margin-top: 0px !important">
        <div class="row">
            <div class="col-lg-6" style="background: #FFF; margin-top: 140px;">
                <img src="{{ asset('img/apex/footer_background.png') }}" class="footer-logo-apex-brasil" alt="logo footer" />
            </div>

            <div class="col-lg-6 mt-5">
                <a href="{{ route('faleconosco') }}" style="text-decoration: none">
                    <h3 class="text-white mb-3 ml-4" style="color: #084B7D !important;">
                        <i class="fa fa-envelope"></i> {{__('Fale Conosco - Formulário para Contato com a Coordenação de Aquisições')}}
                    </h3>
                </a>

                <div class="text-white" style="color: #084B7D !important;">
                    <p><a href="tel:556120270202"><i class="fa fa-phone mr-2 ml-4"></i> +55 61 2027-0202</a></p>
                    <p class="mr-2 ml-4">SAUN, Quadra 5, Lote C, Torre B, 12º a 18º andar</p>
                    <p class="mr-2 ml-4">Centro Empresarial CNC</p>
                    <p class="mr-2 ml-4">Asa Norte, Brasília - DF, 70040-250</p>
                </div>

                <div class="social_network">
                    <a href="https://www.facebook.com/apexbrasil" target="_blank"
                       data-placement="bottom" title="Facebook">
                        <span class="fa-stack fa-lg e_tada ml-4">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>

                    <a href="https://twitter.com/apexbrasil" target="_blank"
                       title="Twitter">
                        <span class="fa-stack fa-lg e_tada">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>

                    <a href="https://www.youtube.com/user/ApexBrasil" target="_blank"
                       data-placement="bottom" title="Youtube">
                        <span class="fa-stack fa-lg e_tada">
                            <i class="fa fa-square fa-stack-2x"></i>
                            <i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i>
                        </span>
                    </a>
                </div>

                <p class="mt-3 ml-3" style="color: #084B7D !important;">
                    © {{__('Todos os Direitos Reservados')}} - Apex-Brasil
                </p>
            </div>
        </div>
    </div>
</footer>

