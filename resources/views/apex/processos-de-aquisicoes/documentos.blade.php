<div class="row">
    <div class="col-lg-12">
        <p>
            <b>{{__('Objeto')}}:</b> {{ $objeto }}
        </p>

        <ul class="list-group">
            @foreach($documentos as $documento)
                <li class="list-group-item">
                    <a href="{{ route('download-documento-processos-de-aquisicoes', ['documento' => $documento->anexo_doc_just, 'numeroProcesso' => $documento->numero]) }}" style="text-decoration: none">
                        <i class="fa fa-download"></i>
                        {{ $documento->tipo_doc_just }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
