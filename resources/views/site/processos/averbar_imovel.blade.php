@if( $processo->Serviço == 'Averbar Imóvel' )
    <div class="justfy-content-center processo-fase">
        @if($processo->Fase != "Finalizado" && $processo->Fase != 'Concluído')
            @if($processo->responsavel == "Portal")
                <br>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> Esta solicitação requer que você faça uma interação: &nbsp;
                    <a href="{{ route('formularioEdita', $processo->Chamado) }}" class="btn btn-blue">Editar
                        Solicitação</a>
                </div>
            @endif
        @endif

        @if($processo->motivo != "" &&  $processo->FINALIZADO != "andamento")
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Atenção!</strong><br>
                Esta solicitação foi finalizada e teve como conclusão:<br>
                Conclusão: <strong>{{$processo->Fase}}</strong><br>
                @if($processo->motivo != "")
                    Motivo: {{$processo->motivo}}<br>
                @endif
            </div>
        @endif

        @if( $processo->Serviço == 'Averbar Imóvel' )
            @if( $processo->Etapa == "11")
                <br>
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Atenção!</strong> Existem pendências na sua solicitação: &nbsp;<br><br>
                    <strong>Pendências:</strong> {{$processo->motivo}}<br><br>

                    <a href="{{ route('averbacaoImovel', $processo->Chamado) }}" class="btn btn-blue">Corrigir pendências</a>
                </div>
            @endif
        @endif

    </div>
@endif