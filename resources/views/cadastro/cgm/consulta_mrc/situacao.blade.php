<div class="row mt-4">
    <div class="col-lg-12">
        <img src="img/{{ $situacao }}.png"
            width="100%" />
    </div>

    <div class="col-lg-6 mt-3">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><i class="fa fa-user"></i> Nome do Solicitante</th>
                    <td>{{ $dadosConsulta->solicitante }}</td>
                </tr>

                <tr>
                    <th><i class="fa fa-calendar"></i> Data da Solicitação</th>
                    <td>{{ date('d/m/Y H:i:s', strtotime($dadosConsulta->abertura)) }}</td>
                </tr>

                <tr>
                    <th><i class="fa fa-user"></i> CPF/CNPJ do Solicitante</th>
                    <td>{{ $dadosConsulta->cpf_cnpj }}</td>
                </tr>
            </thead>
        </table>
    </div>
</div>