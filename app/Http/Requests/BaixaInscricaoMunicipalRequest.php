<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaixaInscricaoMunicipalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razao_social_contribuinte' => 'required',
            'inscricao_municipal' => 'required',
            'celular_contribuinte' => 'required',
            'cpfOuCnpj_contribuinte' => 'required',

            // Dados do Requerente
            'nome_requerente' => 'required',
            'cpf_requerente' => 'required',
            'email_requerente' => 'email|required',
            'celular_requerente' => 'required',

            'tipo_baixa_inscricao' => 'required',
            'observacoes_adicionais' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'razao_social_contribuinte.required' => 'Preencha o campo Razão Social (Dados do Contribuinte)',
            'inscricao_municipal.required' => 'Preencha o campo Inscrição Municipal (Dados do Contribuinte)',
            'celular_contribuinte.required' => 'Preencha o campo Celular (Dados do Contribuinte)',
            'cpfOuCnpj_contribuinte.required' => 'Preencha o campo CPF/CNPJ (Dados do Contribuinte)',

            // Dados do Requerente
            'nome_requerente.required' => 'Preencha o campo Requerente (Dados do Requerente)',
            'cpf_requerente.required' => 'Preencha o campo CPF do Requerente (Dados do Requerente)',
            'email_requerente.required' => 'Preencha o campo E-mail (Dados do Requerente)',
            'celular_requerente.required' => 'Preencha o campo Celular do Requerente (Dados do Requerente)',

            'tipo_baixa_inscricao.required' => 'Selecione o campo Tipos de Baixa de Inscrição (Solicitação)',
            'observacoes_adicionais.required' => 'Preencha o campo Observações Adicionais (Solicitação)'
        ];
    }
}
