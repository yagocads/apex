@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{__('Produtos e serviços')}}</h4>
                </div>

                <div class="card-body">
                    @include('apex.produtos-servicos.tabela_produtos_servicos')
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
    <script>
        let load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            }
        });

        $(document).ready(function() {
            carregarProdutosEservicos();
            function carregarProdutosEservicos() {
                $.ajax({
                    url: "carregar-produtos-servicos",
                    method: "POST",
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        load.fadeOut(200);

                        $('.card').find('.card-body').html(response);
                        recalcularDataTable();
                    },
                });
            }
        });
    </script>
@endsection
