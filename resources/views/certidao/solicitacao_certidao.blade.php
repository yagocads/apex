@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        @if (session('success'))
            @include('confirmacao.confirmacao_processo')
        @else
            <div class="col-lg-12">
                <h3>
                    <i class="fa fa-home mb-3"></i> {{ $titulo }}
                </h3>

                    <form method="POST" action="{{ route('salvar-certidoes-notas') }}" id="form">
                    @csrf
                    <input type="hidden" name="tipo_solicitacao" id="tipo_solicitacao" value="{{ $tipo }}" />
                    <input type="hidden" name="titulo" id="titulo" value="{{ $titulo }}" />
                    <input type="hidden" name="representante" id="representante" value="0" />
                    <div id="accordion">
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Informações Iniciais
                                </div>
                            </a>
                            <div class="collapse show" data-parent="#accordion">
                                <div class="card-body">
                                    @include('certidao.form_certidao_nf_informacoes_iniciais')
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" >
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                                </div>
                            </a>
                            <div class="collapse show" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="form-row align-items-center">
                                        <div class="col-lg-6 form-group col-auto">
                                            <label for="ro_cpf">{{(get_guia_services() == 1) ? 'CPF' : 'CNPJ' }}</label><span class="asterisc">*</span></label>
                                            <input id="documento" type="text" class="form-control form-control-sm"  name="documento" value="{{ Auth::user()->cpf }}" readonly />
                                        </div>
                                        <div class="col-auto">
                                            <div class="form-check mb-6">
                                                <input class="form-check-input" type="checkbox" id="rep_legal" name="rep_legal" />
                                                <label class="form-check-label" for="rep_legal">
                                                Representação Legal
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 form-group">
                                            <label for="nome" class="control-label">Nome <span class="asterisc">*</span></label>
                                            <input id="nome" type="text" class="form-control form-control-sm"  name="nome" value="{{ Auth::user()->name }}" readonly />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="celular" class="control-label">Celular <span class="asterisc">*</span></label>
                                            <input id="celular" type="text" class="form-control form-control-sm mask-cel" value="{{ Auth::user()->celphone }}" readonly  name="celular" />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="telefone" class="control-label">Telefone</label>
                                            <input id="telefone" type="text" class="form-control form-control-sm mask-tel" value="{{ Auth::user()->phone }}" readonly name="telefone" />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="email" class="control-label">E-mail <span class="asterisc">*</span></label>
                                            <input id="email" type="text" class="form-control form-control-sm"  name="email" value="{{ Auth::user()->email }}" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Contribuinte
                                </div>
                            </a>
                            <div class="collapse show" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="form-row align-items-center contribuinte">
                                        <div class="col-lg-4 form-group col-auto">
                                            <label for="cpf_req" class="control-label">CPF / CNPJ <span class="asterisc">*</span></label>
                                            <input id="cpf_req" type="text" class="form-control form-control-sm"  name="cpf_req" value={{ Auth::user()->cpf }} readonly />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="comp_rep" class="control-label">Nome/Razão Social Contribuinte<span class="asterisc">*</span></label>
                                            <input id="nome_rep" type="text" class="form-control form-control-sm"  name="nome_rep" value="{{ Auth::user()->name }}" maxlength="40" readonly />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="cel_cont" class="control-label">Celular <span class="asterisc">*</span></label>
                                            <input id="cel_cont" type="text" class="form-control form-control-sm mask-cel"  name="cel_cont" value="{{ Auth::user()->celphone }}" readonly />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="tel_cont" class="control-label">Telefone</label>
                                            <input id="tel_cont" type="text" class="form-control form-control-sm mask-tel"  name="tel_cont" value="{{ Auth::user()->phone }}" readonly />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="email_cont" class="control-label">E-mail Contribuinte <span class="asterisc">*</span></label>
                                            <input id="email_cont" type="email" class="form-control form-control-sm"  name="email_cont" value="{{ Auth::user()->email }}" maxlength="100" readonly />
                                        </div>
                                        <div class="col-lg-4 form-group">
                                            <label for="insc_cont" class="control-label">Inscrição Municipal</label>
                                            <input id="insc_cont" type="text" class="form-control form-control-sm" maxlength="9"  name="insc_cont" onkeypress='return onlyNumber(event)' />
                                        </div>
                                        <div class="col-lg-12">
                                            <h5><i class="fa fa-map-marker mt-3 mb-3"></i> Endereço:</h5>

                                            <div class="form-row">
                                                <div class="col-lg-4 form-group">
                                                    <label for="cep">CEP:</label>
                                                    <input type="text" name="cep" id="cep" onChange="pesquisaCep(this.value);"
                                                        class="form-control form-control-sm mask-cep {{ ($errors->has('cep') ? 'is-invalid' : '') }}"
                                                        value="{{ old('cep') }}" />

                                                        @if ($errors->has('cep'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('cep') }}
                                                            </div>
                                                        @endif
                                                </div>

                                                <div class="col-lg-8 form-group">
                                                    <label for="endereco">Endereço:</label>
                                                    <input type="text" name="endereco" id="endereco"
                                                        class="form-control form-control-sm {{ ($errors->has('endereco') ? 'is-invalid' : '') }}"
                                                        value="{{ old('endereco') }}" maxlength="100" />

                                                        @if ($errors->has('endereco'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('endereco') }}
                                                            </div>
                                                        @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="col-lg-2 form-group">
                                                    <label for="numero">Número:</label>
                                                    <input type="text" name="numero" id="numero"
                                                        class="form-control form-control-sm mask-numero_endereco {{ ($errors->has('numero') ? 'is-invalid' : '') }}"
                                                        value="{{ old('numero') }}" />

                                                        @if ($errors->has('numero'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('numero') }}
                                                            </div>
                                                        @endif
                                                </div>

                                                <div class="col-lg-6 form-group">
                                                    <label for="complemento">Complemento:</label>
                                                    <input type="text" name="complemento" id="complemento"
                                                        class="form-control form-control-sm {{ ($errors->has('complemento') ? 'is-invalid' : '') }}"
                                                        value="{{ old('complemento') }}" maxlength="50" />

                                                        @if ($errors->has('complemento'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('complemento') }}
                                                            </div>
                                                        @endif
                                                </div>

                                                <div class="col-lg-4 form-group">
                                                    <label for="bairro">Bairro:</label>
                                                    <input type="text" name="bairro" id="bairro"
                                                        class="form-control form-control-sm {{ ($errors->has('bairro') ? 'is-invalid' : '') }}" readonly="true"
                                                        value="{{ old('bairro') }}" maxlength="40" />

                                                        @if ($errors->has('bairro'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('bairro') }}
                                                            </div>
                                                        @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="col-lg-6 form-group">
                                                    <label for="municipio">Município:</label>
                                                    <input type="text" name="municipio" id="municipio"
                                                        class="form-control form-control-sm {{ ($errors->has('municipio') ? 'is-invalid' : '') }}" readonly="true"
                                                        value="{{ old('municipio') }}" maxlength="40" />

                                                        @if ($errors->has('municipio'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('municipio') }}
                                                            </div>
                                                        @endif
                                                </div>
                                                <div class="col-lg-6 form-group">
                                                    <label for="estado">Estado:</label>
                                                    <select name="estado" id="estado" class="form-control form-control-sm {{ ($errors->has('estado') ? 'is-invalid' : '') }}"
                                                        readonly="true">
                                                        <option></option>

                                                        @foreach($estados->geonames as $estado)
                                                            <option value="{{ $estado->adminCodes1->ISO3166_2 }}" {{ old('estado') == $estado->adminCodes1->ISO3166_2 ? 'selected' : '' }}>
                                                                {{ $estado->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                    @if ($errors->has('estado'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('estado') }}
                                                        </div>
                                                    @endif
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <a class="card-link no-underline text-dark" data-toggle="collapse">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Documentação do Processo
                                </div>
                            </a>
                            <div class="collapse show" data-parent="#accordion">
                                <div class="card-body">
                                    @include('certidao.form_certidao_nf')
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'imovel']) }}" class="btn btn-primary mt-3">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>

                            <button class="btn btn-success mt-3 solicitar" id="enviar">Solicitar <i class="fa fa-send"></i></button>
                        </div>
                    </div>

                </form>
            </div>
            @endif
    </div>
</div>

@endsection

@section('post-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
<script type="text/javascript">
    $(function(e) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $('#rep_legal').click(function() {
            $('#representante').val('1');
            if ($(this).is(':checked')) {
                $('.contribuinte input').attr('readonly', false).val("");

                $('#cpf_req').mask('000.000.000-00', {
                    onKeyPress : function(cpfcnpj, e, field, options) {
                        const masks = ['000.000.000-000', '00.000.000/0000-00'];
                        const mask = (cpfcnpj.length > 14) ? masks[1] : masks[0];
                        $('#cpf_req').mask(mask, options);
                    }
                });
            } else {
                $('#representante').val('0');
                $('#cpf_req').attr('readonly', true).val($('#documento').val());
                $('#nome_rep').attr('readonly', true).val($('#nome').val());
                $('#cel_cont').attr('readonly', true).val($('#celular').val());
                $('#tel_cont').attr('readonly', true).val($('#telefone').val());
                $('#email_cont').attr('readonly', true).val($('#email').val());
            }
        });

        popularDocumentacao();

        function popularDocumentacao() {

            const titulo = $('input[name=titulo]').val();

            let documentacaoExigida = [];

            if (titulo === 'Cancelamento de Nota Fiscal') {
                documentacaoExigida = [
                    'Última alteração do Contrato Social',
                    'Alvará de localização e funcionamento',
                    'Identidade',
                    'CPF',
                    'Comprovante Residência do procurador com firma reconhecida',
                    'Pagamento da taxa de requerimento',
                ];
            } else {
                documentacaoExigida = [
                    'Identidade do Responsável',
                    'CPF do Responsável',
                ];
            }

            carregarDocumentacaoPorRepresentacaoLegal();

            function carregarDocumentacaoPorRepresentacaoLegal() {
                $('#rep_legal').on('change', function() {

                    const representanteLegalChecked = $(this).is(':checked');

                    if (representanteLegalChecked) {
                        adicionarDocumento('CPF do Representante Legal');
                        adicionarDocumento('RG do Representante Legal');
                        adicionarDocumento('Documento de Representação Legal');
                    } else {
                        removerDocumento('CPF do Representante Legal');
                        removerDocumento('RG do Representante Legal');
                        removerDocumento('Documento de Representação Legal');
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });
            }

            carregarDocumentacaoPorTipoDePrestador();

            function carregarDocumentacaoPorTipoDePrestador() {
                $('#tipo_prestador').on('change', function() {
                    const tipo = $(this).val();

                    if (titulo === 'Cancelamento de Nota Fiscal') {
                        switch(tipo) {
                            case 'MEI':
                                adicionarDocumento('Certificado de MEI do prestador');
                                removerDocumento('Última alteração do Contrato Social');
                                removerDocumento('Alvará de localização e funcionamento');
                                removerDocumento('Última alteração do Contrato Social do Prestador');
                                break;
                            case 'Outros':
                                removerDocumento('Certificado de MEI do prestador');
                                adicionarDocumento('Última alteração do Contrato Social');
                                adicionarDocumento('Alvará de localização e funcionamento');
                                adicionarDocumento('Última alteração do Contrato Social do Prestador');
                                break;
                            default:
                                adicionarDocumento('Última alteração do Contrato Social');
                                adicionarDocumento('Alvará de localização e funcionamento');
                                removerDocumento('Certificado de MEI do prestador');
                                removerDocumento('Última alteração do Contrato Social do Prestador');
                                break;
                        }
                    } else {
                        switch(tipo) {
                            case 'PF':
                                adicionarDocumento('Comprovante Residência');

                                removerDocumento('Certificado de MEI - CEMEI');
                                removerDocumento('Cartão CNPJ');
                                removerDocumento('Última alteração do Contrato Social');
                                removerDocumento('Comprovante de vínculo');
                                break;
                            case 'MEI':
                                adicionarDocumento('Certificado de MEI - CEMEI');
                                adicionarDocumento('Cartão CNPJ');

                                removerDocumento('Comprovante Residência');
                                removerDocumento('Última alteração do Contrato Social');
                                removerDocumento('Comprovante de vínculo');
                                break;

                            case 'Outras Constituições':
                                adicionarDocumento('Cartão CNPJ');
                                adicionarDocumento('Última alteração do Contrato Social');
                                adicionarDocumento('Comprovante de vínculo');

                                removerDocumento('Certificado de MEI - CEMEI');
                                removerDocumento('Comprovante Residência');
                                break;

                            default:
                                removerDocumento('Comprovante Residência');
                                removerDocumento('Certificado de MEI - CEMEI');
                                removerDocumento('Cartão CNPJ');
                                removerDocumento('Última alteração do Contrato Social');
                                removerDocumento('Comprovante de vínculo');
                                break;
                        }
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });
            }

            carregarDocumentacaoPorTipoDeTomador();

            function carregarDocumentacaoPorTipoDeTomador() {
                $('#tipo_tomador').on('change', function() {
                    const tipo = $(this).val();

                    switch(tipo) {
                        case 'PF':
                            adicionarDocumento('Carta de anuência assinada concordando com o cancelamento');
                            adicionarDocumento('Tomador/Procurador legalmente constituído');
                            removerDocumento('Certificado de MEI do Tomador');
                            removerDocumento('Última alteração do contrato social do Tomador');
                            break;
                        case 'MEI':
                            adicionarDocumento('Certificado de MEI do Tomador');
                            removerDocumento('Carta de anuência assinada concordando com o cancelamento');
                            removerDocumento('Tomador/Procurador legalmente constituído');
                            removerDocumento('Última alteração do contrato social do Tomador');
                            break;
                        case 'Outras Instituições':
                            adicionarDocumento('Última alteração do contrato social do Tomador');
                            removerDocumento('Carta de anuência assinada concordando com o cancelamento');
                            removerDocumento('Tomador/Procurador legalmente constituído');
                            removerDocumento('Certificado de MEI do Tomador');
                            break;
                        default:
                            removerDocumento('Carta de anuência assinada concordando com o cancelamento');
                            removerDocumento('Tomador/Procurador legalmente constituído');
                            removerDocumento('Certificado de MEI do Tomador');
                            removerDocumento('Última alteração do contrato social do Tomador');
                            break;
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });
            }

            carregarDocumentacaoQuemAssinouCartaAnuencia();

            function carregarDocumentacaoQuemAssinouCartaAnuencia() {
                $('#assinatura_carta_anuencia').on('change', function() {
                    const tipo = $(this).val();

                    switch(tipo) {
                        case 'Tomador':
                            adicionarDocumento('CPF Tomador');
                            adicionarDocumento('Identidade Tomador');
                            removerDocumento('Procuração');
                            removerDocumento('CPF Procurador');
                            removerDocumento('Identidade Procurador');
                            break;
                        case 'Procurador':
                            adicionarDocumento('Procuração');
                            adicionarDocumento('CPF Tomador');
                            adicionarDocumento('Identidade Tomador');
                            adicionarDocumento('CPF Procurador');
                            adicionarDocumento('Identidade Procurador');
                            break;
                        default:
                            removerDocumento('Procuração');
                            removerDocumento('CPF Tomador');
                            removerDocumento('Identidade Tomador');
                            removerDocumento('CPF Procurador');
                            removerDocumento('Identidade Procurador');
                            break;
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });
            }

            carregarDocumentacaoJustificativaCancelamento();

            function carregarDocumentacaoJustificativaCancelamento() {
                $('#justificativa_cancelamento').on('change', function() {
                    const status = $(this).val();

                    switch(status) {
                        case 'Erro de Preenchimento':
                            $('.pergunta_emissao_guia_pagamento').removeClass('d-none');
                            break;
                        case 'Serviço não foi prestado':
                            removerDocumentosEmissaoGuiaPagamento();

                            adicionarDocumento('Nota a ser cancelada');
                            removerDocumento('Nota Original');
                            removerDocumento('Nota duplicada a ser cancelada');
                            $('.pergunta_emissao_guia_pagamento').addClass('d-none');
                            break;
                        case 'Ainda se foi emitida em duplicidade':
                            adicionarDocumento('Nota Original');
                            adicionarDocumento('Nota duplicada a ser cancelada');
                            removerDocumento('Nota a ser cancelada');
                            $('.pergunta_emissao_guia_pagamento').addClass('d-none');

                            removerDocumentosEmissaoGuiaPagamento();
                            break;
                        default:
                            removerDocumento('Nota Original');
                            removerDocumento('Nota duplicada a ser cancelada');
                            removerDocumento('Nota a ser cancelada');
                            $('.pergunta_emissao_guia_pagamento').addClass('d-none');

                            removerDocumentosEmissaoGuiaPagamento();
                            break;
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });

                function removerDocumentosEmissaoGuiaPagamento() {
                    removerDocumento('Nova nota emitida com os dados corretos');
                    removerDocumento('Nota a ser cancelada');
                    $('.pergunta_competencia_nota').addClass('d-none');
                    $('#emissao_guia_pagamento').val('');
                }

                $('#emissao_guia_pagamento').on('change', function() {
                    const status = $(this).val();

                    switch(status) {
                        case 'Sim':
                            adicionarDocumento('Nova nota emitida com os dados corretos');
                            adicionarDocumento('Nota a ser cancelada');
                            $('.pergunta_competencia_nota').addClass('d-none');

                            $('.aviso_nota_legal').addClass('d-none');
                            $('#enviar').attr('disabled', false);
                            break;
                        case 'Não':
                            $('.pergunta_competencia_nota').removeClass('d-none');
                            removerDocumento('Nova nota emitida com os dados corretos');
                            removerDocumento('Nota a ser cancelada');

                            removerDocumentosCompetenciaNota();
                            break;
                        default:
                            removerDocumento('Nova nota emitida com os dados corretos');
                            removerDocumento('Nota a ser cancelada');
                            $('.pergunta_competencia_nota').addClass('d-none');

                            removerDocumentosCompetenciaNota();
                            break;
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });

                function removerDocumentosCompetenciaNota() {
                    removerDocumento('Nova nota emitida com os dados corretos');
                    removerDocumento('Nota a ser cancelada');
                    $('.aviso_nota_legal').addClass('d-none');
                    $('#enviar').attr('disabled', false);
                    $('#competencia_nota').val('');
                }

                $('#competencia_nota').on('change', function() {
                    const status = $(this).val();

                    switch(status) {
                        case 'Solicitação do cancelamento até dia 10 do mês seguinte':
                            removerDocumento('Nova nota emitida com os dados corretos');
                            removerDocumento('Nota a ser cancelada');
                            $('.aviso_nota_legal').removeClass('d-none');
                            $('#enviar').attr('disabled', true);
                            break;
                        case 'Solicitação do cancelamento após dia 10 do mês seguinte':
                            adicionarDocumento('Nova nota emitida com os dados corretos');
                            adicionarDocumento('Nota a ser cancelada');
                            $('.aviso_nota_legal').addClass('d-none');
                            $('#enviar').attr('disabled', false);
                            break;
                        default:
                            removerDocumento('Nova nota emitida com os dados corretos');
                            removerDocumento('Nota a ser cancelada');
                            $('.aviso_nota_legal').addClass('d-none');
                            $('#enviar').attr('disabled', false);
                            break;
                    }

                    popularDescricaoDocumento(documentacaoExigida);
                    popularListaDocumentacaoObrigatoria(documentacaoExigida);
                });

            }

            function adicionarDocumento(documento) {
                if (documentacaoExigida.indexOf(documento) === -1) {
                    documentacaoExigida.push(documento);
                }
            }

            function removerDocumento(documento) {
                if (documentacaoExigida.indexOf(documento) !== -1) {
                    documentacaoExigida.splice(documentacaoExigida.indexOf(documento), 1);
                }
            }

            popularDescricaoDocumento(documentacaoExigida);

            function popularDescricaoDocumento(documentacaoExigida) {
                let select = document.querySelector("select[name=descricao]");

                select.innerHTML = "";
                select.append(document.createElement("option"));

                documentacaoExigida.sort();

                documentacaoExigida.forEach((documento) => {
                    let elementOption = document.createElement("option");
                    elementOption.append(documento);
                    select.appendChild(elementOption);
                });

                let titulo = $('input[name=titulo]').val();

                if (titulo !== 'Certidão de Não Inscrito') {
                    // const option = document.createElement('option');
                    // option.append('Cópia da Nota Fiscal');
                    // select.append(option);
                }
            }

            popularListaDocumentacaoObrigatoria(documentacaoExigida);

            function popularListaDocumentacaoObrigatoria(documentacaoExigida) {
                let lista = document.querySelector(".doc_obrigatorio");
                lista.innerHTML = "";

                documentacaoExigida.sort();

                documentacaoExigida.forEach((documento) => {
                    let elementLi = document.createElement("li");
                    elementLi.append(documento);
                    lista.appendChild(elementLi);
                });

                let titulo = $('input[name=titulo]').val();

                if (titulo !== 'Certidão de Não Inscrito') {
                    // const li = document.createElement('li');
                    // li.append('Cópia da Nota Fiscal');
                    // lista.append(li);
                }
            }
        }
    });

    $("#form").validate({
        rules: {
            cep: { required: true },
            endereco: { required: true },
            numero: { required: true },
            bairro: { required: true },
            municipio: { required: true },
            estado: { required: true },
            cpf_req: { required: true },
            nome_rep: { required: true },
            cel_cont: { required: true },
            email_cont: { required: true },
            tipo_prestador: { required: true },
            // tipo_tomador: { required: true },
            // assinatura_carta_anuencia: { required: true },
            // justificativa_cancelamento: { required: true },
        },
        messages: {
            cep: { required: "Informe o CEP do endereço" },
            endereco: { required: "Informe o endereço" },
            numero: { required: "Informe o número" },
            municipio: { required: "Informe o municipio " },
            bairro: { required: "Informe o bairro"},
            estado: { required: "Informe o estado" },
            cpf_req: { required: "Informe o CPF do representante" },
            nome_rep: { required: "Informe o nome do do representante" },
            cel_cont: { required: "Informe o Celular" },
            email_cont: { required: "Informe E-mail do representante", email: "E-mail inválido"},
            tipo_prestador: { required: "Selecione o tipo de prestador" },
            // tipo_tomador: { required: "Selecione o tipo de tomador" },
            // assinatura_carta_anuencia: { required: "Selecione quem está assinando a carta de anuência" },
            // justificativa_cancelamento: { required: "Selecione a justificativa de cancelamento" },
        },
        submitHandler: function(form) {

            var load = $(".ajax_load");

            if ($('#cpf_req').val().length <= 14) {
                if(!validarCPF($("#cpf_req").val())){
                    alert("CPF Inválido!");
                    return false;
                }
            }

            if($("#cpf_req").val().length == 18) {
                if (!validarCNPJ($("#cpf_req").val())) {
                    alert("CNPJ Inválido!");
                    return false;
                }
            }

            let url = "{{ route('verifica-documentacao') }}";

            const documentosExigidos = [];

            $("select[name=descricao]").find('option').each(function () {

                if ($(this).text() != '') {
                    documentosExigidos.push($(this).text());
                }
            });

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: {documentosExigidos},
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);

                    if (response.length > 0) {
                        alert(`Atenção: Os seguintes documentos são obrigatórios: ${response.join(', ')}.`);
                        return false;
                    }

                    form.submit();
                }
            });
        }
    });

</script>
@endsection
