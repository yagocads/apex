<div class="row">
    <div class="col-lg-6">
        <label for="cpf">{{ (strlen(Auth::user()->cpf) > 14 ? 'CNPJ:' : 'CPF:') }}</label>
        <input type="text" class="form-control form-control-sm" name="cpf" value="{{ Auth::user()->cpf }}" readonly="readonly" />
    </div>
    <div class="col-lg-6">
        <label for="nome">{{ __('Nome') }}</label>
        <input type="text" class="form-control form-control-sm" name="nome" value="{{ Auth::user()->name }}" readonly="readonly" />
    </div>
</div>