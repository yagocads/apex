<div class="form-row">
    <div class="col-lg-12 form-group">
        <div class="alert alert-info">
            <i class="fa fa-volume-up"></i> OBS: O e-mail será vinculado ao CPF e todo o contato da prefeitura com o 
            cidadão será feito por meio deste.
        </div>
    </div>
</div>

<div class="form-row">
    <div class="col-lg-4 form-group">
        <label for="email">E-mail:</label>
        <input type="email" name="email" id="email" 
            class="form-control form-control-sm {{ ($errors->has('email') ? 'is-invalid' : '') }}" 
            value="{{ old('email') }}" maxlength="100" />

            @if ($errors->has('email'))
                <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                </div>
            @endif
    </div>

    <div class="col-lg-4 form-group">
        <label for="confirmacao_email">Confirmação do E-mail:</label>
        <input type="email" name="confirmacao_email" id="confirmacao_email" 
            class="form-control form-control-sm {{ ($errors->has('confirmacao_email') ? 'is-invalid' : '') }}"
            value="{{ old('confirmacao_email') }}" maxlength="100" />

            @if ($errors->has('confirmacao_email'))
                <div class="invalid-feedback">
                    {{ $errors->first('confirmacao_email') }}
                </div>
            @endif
    </div>

    <div class="col-lg-2 form-group">
        <label for="celular">Celular:</label>
        <input type="text" name="celular" id="celular" 
            class="form-control form-control-sm mask-cel {{ ($errors->has('celular') ? 'is-invalid' : '') }}" 
            value="{{ old('celular') }}" />

            @if ($errors->has('celular'))
                <div class="invalid-feedback">
                    {{ $errors->first('celular') }}
                </div>
            @endif
    </div>

    <div class="col-lg-2 form-group">
        <label for="telefone">Telefone de Contato:</label>
        <input type="text" name="telefone" id="telefone" 
            class="form-control form-control-sm mask-tel {{ ($errors->has('telefone') ? 'is-invalid' : '') }}" 
            value="{{ old('telefone') }}" />

            @if ($errors->has('telefone'))
                <div class="invalid-feedback">
                    {{ $errors->first('telefone') }}
                </div>
            @endif
    </div>
</div> 
