@extends('layouts.tema_principal')

<style type="text/css">
    .scrollspy-container {
        overflow: auto;
    }
    .scrollspy-example {
        width: 100%;
    }
    .bd-step {
        border-bottom: 1px solid #227DC7;
        padding-top: 5px;
        padding-bottom: 10px;
    }
    .registro-fase-topo {
        padding-top: 30px;
    }
    .busca {
        padding-bottom: 10px;
    }
    .servicos p {
        margin: 30px 0 0 0;
    }
    .servicos {
        padding: 20px;
        background-color: #FFF;
        border-radius: 10px;
        height: 200px;
        width: 100%;
        cursor:pointer;
    }
    @media (min-width: 768px) and (max-width: 991px) {
        /* Show 4th slide on md if col-md-4*/
        .carousel-inner .active.col-md-4.carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -33.3333%;  /*change this with javascript in the future*/
            z-index: -1;
            display: block;
            visibility: visible;
        }
    }
    @media (min-width: 576px) and (max-width: 768px) {
        /* Show 3rd slide on sm if col-sm-6*/
        .carousel-inner .active.col-sm-6.carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -50%;  /*change this with javascript in the future*/
            z-index: -1;
            display: block;
            visibility: visible;
        }
    }
    @media (min-width: 576px) {
        .carousel-item {
            margin-right: 0;
        }
        /* show 2 items */
        .carousel-inner .active + .carousel-item {
            display: block;
        }
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item {
            transition: none;
        }
        .carousel-inner .carousel-item-next {
            position: relative;
            transform: translate3d(0, 0, 0);
        }
        /* left or forward direction */
        .active.carousel-item-left + .carousel-item-next.carousel-item-left,
        .carousel-item-next.carousel-item-left + .carousel-item,
        .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }
        /* farthest right hidden item must be also positioned for animations */
        .carousel-inner .carousel-item-prev.carousel-item-right {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            display: block;
            visibility: visible;
        }
        /* right or prev direction */
        .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
        .carousel-item-prev.carousel-item-right + .carousel-item,
        .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }
    }
    /* MD */
    @media (min-width: 768px) {
        /* show 3rd of 3 item slide */
        .carousel-inner .active + .carousel-item + .carousel-item {
            display: block;
        }
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
            transition: none;
        }
        .carousel-inner .carousel-item-next {
            position: relative;
            transform: translate3d(0, 0, 0);
        }
        /* left or forward direction */
        .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }
        /* right or prev direction */
        .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }
    }
    /* LG */
    @media (min-width: 991px) {
        /* show 4th item */
        .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item {
            display: block;
        }
        .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
            transition: none;
        }
        /* Show 5th slide on lg if col-lg-3 */
        .carousel-inner .active.col-lg-3.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -25%;  /*change this with javascript in the future*/
            z-index: -1;
            display: block;
            visibility: visible;
        }
        /* left or forward direction */
        .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }
        /* right or prev direction //t - previous slide direction last item animation fix */
        .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }
    }
    #carousel-example{
        width: 100%;
    }
 </style>

@section('content')

<div class="container-fluid">
    <div class="row busca">
        <div class="col-lg-12">
            <div class="card">
                <a data-toggle="collapse" class="no-underline text-dark" href="#busca" aria-controls="busca">
                    <div class="card-header">
                        <h5><i class="fa fa-arrow-down arrow-red"></i> Buscar Solicitação</h5>
                    </div>
                </a>
                <div class="collapse" id="busca">
                    <div class="card-body" >
                        @include('site.acompanhamento_form')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <a data-toggle="collapse" class="no-underline text-dark" href="#processos" aria-controls="processos">
                    <div class="card-header">
                        <h5><i class="fa fa-arrow-down arrow-red"></i> Seus Serviços Abertos</h5>
                    </div>
                </a>
                <div class="collapse" id="processos">
                    <div class="card-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div id="carousel-example" class="carousel slide" data-interval="0">
                                    <div class="carousel-inner row w-100 mx-auto">
                                        @foreach ($registros as $key => $registro)
                                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 {{$key == 0 ? 'active' : ''}}">
                                                <div class="servicos text-center shadow-sm mb-5" data-registro="{{$registro->view}}">
                                                    <i class="fa fa-search-plus fa-3x" aria-hidden="true"></i>
                                                    <p>{{$registro->servico}}</p>
                                                    <a href="#" class="text-dark no-underline">
                                                        <div class="btn-acessar btn btn-danger shadow-sm" data-registro="{{$registro->view}}">
                                                            Ver Processos
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev" style="width: 0px;">
                                        <span class="carousel-control-prev-icon" aria-hidden="true" style="margin-top: -172px; margin-left: -21px;"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next" style="width: 0px;">
                                        <span class="carousel-control-next-icon" aria-hidden="true" style="margin-top: -172px; margin-right: -21px;"></span>
                                        <span class="sr-only">Próximo</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="processos_abertos">
        <div class="col-lg-12" id="conteudo">
            @include('site.acompanhamento_processos')
        </div>
    </div>
</div>
@include('modals.modal_msg')
@endsection
@section('post-script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.servicos').click(function(){

            let load = $(".ajax_load");
            const view = $(this).data("registro");
            const url = "consulta_processo/" + view;

            $.ajax({
                url: url,
                method: 'GET',
                timeout: 20000,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    load.fadeOut(200);
                    alert('Favor tentar novamente!');
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        $('.modal-body>p').html(response.error);
                        $("#divida-modal").modal('show');
                        return false;
                    } else {
                        $("#conteudo").html(response);
                        efeito_ancora('#processos_abertos');
                    }

                }
            });
        });

        $('#carousel-example').on('slide.bs.carousel', function (e) {
            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 5;
            var totalItems = $('.carousel-item').length;

            if (idx >= totalItems-(itemsPerSlide-1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i=0; i<it; i++) {
                    // append slides to end
                    if (e.direction=="left") {
                        $('.carousel-item').eq(i).appendTo('.carousel-inner');
                    }
                    else {
                        $('.carousel-item').eq(0).appendTo('.carousel-inner');
                    }
                }
            }
        });
    });
</script>
@endsection
