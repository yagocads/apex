@if ($processo->Serviço == 'Baixa de Inscrição Municipal')
    @if ($processo->Fase == 'Anexar Exigências')
        <div class="justfy-content-center registro-fase">
            <div class="alert alert-info">
                <strong>Atenção!</strong> Existem exigências na sua solicitação:
                <a href="{{ route('baixa-inscricao-municipal-exigencia', $processo->Chamado) }}" class="ml-2">Editar Solicitação <i class="fa fa-edit"></i></a>
                
                <div class="col-lg-12">
                    <div>Resp: {{ $processo->responsavel }}</div>
                    <div>Motivo: {{ $processo->motivo }}</div>
                </div>
            </div>
        </div>
    @endif
@endif