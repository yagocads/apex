@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h3>
                <i class="fa fa-home mb-3"></i> Solicitar IPTU
            </h3>

            @if($request->idoso == 1)
                <div class="alert alert-info">
                    <h4>Observações Idoso</h3>
                    <p>

                    </p>
                </div>
            @endif

            <form method="POST" name="formSalvarIptu" action="{{ route('salvar_iptu') }}" autocomplete="off">
                @csrf
                <input type="hidden" name="cpf_ou_cnpj" value="{{isset($request->cpf_ou_cnpj)?$request->cpf_ou_cnpj:''}}" />
                <input type="hidden" name="idoso" value="{{isset($request->idoso)?$request->idoso:0}}" />

                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <span class="d-block">* {{ $error }}</span>
                        @endforeach
                    </div>
                @endif

                <div id="accordion">

                    <div class="card mb-3">
                        <a class="card-link no-underline text-dark" data-toggle="collapse" href="#requerente">
                            <div class="card-header">
                                <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                            </div>
                        </a>
                        <div id="requerente" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                @include("iptu.cadastro.requerente.requerente")
                            </div>
                        </div>
                    </div>

                    <section id="abaEmpresa" class="d-none">
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados_mercantis">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados Mercantis
                                </div>
                            </a>
                            <div id="dados_mercantis" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("iptu.cadastro.dados_mercantis.dados_mercantis")
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="abasGeral" class="d-none">
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#informacoes_contato">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Informações de Contato
                                </div>
                            </a>
                            <div id="informacoes_contato" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("iptu.cadastro.informacoes_contato")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dadosEndereco">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Endereço
                                </div>
                            </a>
                            <div id="dadosEndereco" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("iptu.cadastro.endereco.endereco")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#cadastro_imoveis">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i>
                                    Cadastro de Imóveis
                                </div>
                            </a>
                            <div id="cadastro_imoveis" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("iptu.cadastro.cadastro_imovel.cadastro_imovel")
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info">
                            <p>
                                Declaro que são VERDADEIRAS e EXATAS todas as informações que foram
                                prestadas neste formulário. Declaro ainda estar ciente de que declaração
                                falsa no presente cadastro constituirá crime de
                                falsidade ideológica (art. 299 do Código Penal) e estará sujeita a sanções penais
                                sem prejuízo de medidas administrativas e outras. <span class="text-danger">*</span>
                            </p>

                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" name="confirmacao_de_informacoes"
                                        class="form-check-input" value="sim" required>
                                    Li e concordo.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @auth
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'imovel'])  }}" class="btn btn-primary btn-sm mt-3">
                        @else
                            @if(session('vencimentoIPTU')==1)
                                <a href="{{ route('atualizaCGMIPTU') }}" class="btn btn-primary btn-sm mt-3">
                            @else
                                <a href="{{ route('atualizaCGM') }}" class="btn btn-primary btn-sm mt-3">
                            @endif
                        @endauth
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>

                        <button class="btn btn-success btn-sm mt-3 enviar_informacoes">Enviar Informações <i class="fa fa-send"></i></button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

@endsection

@section('post-script')

<script>
    $(function() {

        $("#cresci").mask("00000000")

        @if(empty($errors->all()))
            @auth
                $("#informacoes_pessoais").collapse();
            @else
                $("#requerente").collapse();
            @endauth
        @endif

        // $('#cadastro_imoveis').on('shown.bs.collapse', function () {
        //     console.log("Recalculando tabela");
        //     // recalcularDataTable("tabela_documentos_imovel");

        //     tabelaImovel = $("#tabela_documentos_imovel").DataTable({
        //         destroy: true,
        //         "oLanguage": {
        //             "sProcessing": "Processando...",
        //             "sLengthMenu": "_MENU_ resultados por página",
        //             "sZeroRecords": "Não foram encontrados resultados",
        //             "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        //             "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
        //             "sInfoFiltered": "",
        //             "sInfoPostFix": "",
        //             "sSearch": "Pesquisar:",
        //             "sUrl": "",
        //             "oPaginate": {
        //                 "sFirst": "Primeiro",
        //                 "sPrevious": "Anterior",
        //                 "sNext": "Próximo",
        //                 "sLast": "Último"
        //             }
        //         },
        //         "bLengthChange": true,
        //         "ordering": true,
        //         "info": true,
        //         "searching": true,
        //         "paginate": true
        //     }).columns.adjust().responsive.recalc();
        // });

        // Spinner
        var load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        responsavelPeloPreenchimento();

        function responsavelPeloPreenchimento() {

            $("select[name=responsavel]").change(function() {
                let value = $(this).val();

                if (value === "3" || value === "7") {
                    $(".requerente_prosseguimento").removeClass("d-none");
                    $("#abaEmpresa").addClass("d-none");
                    $("#abasGeral").removeClass("d-none");
                } else {
                    $(".requerente_prosseguimento").removeClass("d-none");
                    $("#abaEmpresa").removeClass("d-none");
                    $("#abasGeral").removeClass("d-none");
                }

                if (value === "6") {
                    $("#camposCorretor").removeClass("d-none");
                    $("#camposEmpresa").addClass("d-none");
                }
                else{
                    $("#camposCorretor").addClass("d-none");
                    $("#camposEmpresa").removeClass("d-none");
                }
            });
        }

        clientePossuiImoveis();

        function clientePossuiImoveis() {

            $("select[name=possui_matricula]").change(function() {
                let value = $(this).val();

                if (value === "SIM") {
                    $("#dadosImovel").removeClass("d-none");
                    $("#enderecoImovel").addClass("d-none");
                } else if(value === "NÃO") {
                    $("#dadosImovel").addClass("d-none");
                    $("#enderecoImovel").removeClass("d-none");
                } else {
                    $("#dadosImovel").addClass("d-none");
                    $("#enderecoImovel").addClass("d-none");
                }
            });
        }

        $("#cpf_requerente").on("change", function() {
            let cpf = $(this).val();

            if (!validarCPF(cpf)) {
                alert("Atenção: CPF Inválido! Informe corretamente o número do seu CPF.");
                $(this).val("");
                $(this).focus();
                return;
            }
        });

        validarTodosCamposEmail();

        function validarTodosCamposEmail() {

            $("input[type=email]").on("change", function() {
                let email = $(this).val();

                if (!validaEmail(email)) {
                    alert("Atenção: O formato do E-mail é inválido.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        validarEmailsCorrespondem();

        function validarEmailsCorrespondem() {

            $("input[name=confirmacao_email]").on("change", function() {

                let email = $("input[name=email]").val();
                let confirmacao_email = $(this).val();

                if (email !== confirmacao_email) {
                    alert("Atenção: Os e-mails não correspondem.");
                    $(this).val("");
                    $(this).focus();
                    return;
                }
            });
        }

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $(this).data("descricao");
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");

            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'], 8400000)) {
                return false;
            }

            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);

            $.ajax({
                url: "{{ route('salvar_documento_servidor_iptu') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("body").on("click", ".btn-enviar-dados-imovel", function(e) {
            e.preventDefault();

            if($("#possui_matricula").val() == ""){
                alert("ATENÇÃO: É necessário informar se você possui o número de matrícula do imóvel.");
                $("#possui_matricula").focus();
                return false;
            }

            let cpf_ou_cnpj = $("input[name=cpf_ou_cnpj]").val();

            let nome_input = $(this).data("nome_input");
            let descricao = $("#tipo_documento").val();
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");

            let possui_matricula = $("#possui_matricula");

            let matricula_imovel = $("input[name=matricula_imovel]");
            let forma_pagamento = $("select[name=formaPagamento]");

            let cep = $("#i_cep");
            let endereco = $("#i_endereco");
            let numero = $("#i_numero");
            let complemento = $("#i_complemento");
            let bairro = $("#i_bairro");
            let municipio = $("#i_municipio");
            let estado = $("#i_estado");
            let referencia = $("#i_referencia");
            let nome_loteamento = $("#i_nome_loteamento");
            let quadra = $("#i_quadra");
            let lote = $("#i_lote");

            let tipo_documento = $("#tipo_documento");


            if ($("#possui_matricula").val() == "SIM") {
                if (!matricula_imovel.val()) {
                    alert("ATENÇÃO: Preencha o campo matrícula para continuar.");
                    matricula_imovel.focus();
                    return false;
                }

                if ($("#formaPagamento").val() == "") {
                    alert("ATENÇÃO: Preencha o campo Forma de Pagamento para continuar.");
                    $("#formaPagamento").focus();
                    return false;
                }
            } else {
                if ($("#i_cep").val() == "") {
                    alert("ATENÇÃO: Preencha o campo CEP.");
                    $("#i_cep").focus();
                    return false;
                }

                if (nome_loteamento.val() == "") {
                    alert("ATENÇÃO: Preencha o campo Nome do Loteamento.");
                    $("#i_nome_loteamento").focus();
                    return false;
                }

                if (quadra.val() == "") {
                    alert("ATENÇÃO: Preencha o campo Quadra.");
                    quadra.focus();
                    return false;
                }

                if (lote.val() == "") {
                    alert("ATENÇÃO: Preencha o campo Lote.");
                    lote.focus();
                    return false;
                }
            }

            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (arquivo) {
                if (!tipo_documento.val()) {
                    alert("ATENÇÃO: Preencha o campo tipo de documento para continuar.");
                    tipo_documento.focus();
                    return false;
                }

                if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'], 8400000)) {
                    return false;
                }
            }


            let formData = new FormData();

            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("cpf_ou_cnpj", cpf_ou_cnpj);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);

            formData.append("matricula_imovel", matricula_imovel.val());
            formData.append("possui_matricula", possui_matricula.val());
            formData.append("forma_pagamento", forma_pagamento.val());
            formData.append("cep", cep.val());
            formData.append("endereco", endereco.val());
            formData.append("numero", numero.val());
            formData.append("complemento", complemento.val());
            formData.append("bairro", bairro.val());
            formData.append("municipio", municipio.val());
            formData.append("estado", estado.val());
            formData.append("referencia", referencia.val());
            formData.append("nome_loteamento", nome_loteamento.val());
            formData.append("quadra", quadra.val());
            formData.append("lote", lote.val());

            $.ajax({
                url: "{{ route('salvar_documento_servidor_iptu') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");
                    matricula_imovel.val("");
                    tipo_documento.val("");

                    possui_matricula.val("");

                    forma_pagamento.val("");

                    cep.val("");
                    endereco.val("");
                    numero.val("");
                    complemento.val("");
                    bairro.val("");
                    municipio.val("");
                    estado.val("");
                    referencia.val("");
                    nome_loteamento.val("");
                    quadra.val("");
                    lote.val("");

                    tipo_documento.val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);

                    $("#possui_matricula").focus();
                    $("#enderecoImovel").addClass("d-none");
                    $("#dadosImovel").addClass("d-none");
                }
            });
        });

        $("body").on("click", ".btn-excluir-documento", function(e) {
            e.preventDefault();

            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let nome_arquivo = $(this).data("nome_arquivo");
            let nome_sessao = $(this).data("nome_sessao");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = $(this).data("nome_tabela");
            let pasta = $(this).data("pasta");
            let id = $(this).data("id");
            let conteudo_documentos = $(this).data("conteudo_documentos");

            $.ajax({
                url: "{{ route('excluir_documento_servidor_iptu') }}",
                method: 'POST',
                data: {
                    "nome_arquivo": nome_arquivo,
                    "nome_sessao": nome_sessao,
                    "pasta": pasta,
                    "nome_tabela": nome_tabela,
                    "id": id,
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });
    });

    verificarDocumentosNecessarios();

    function verificarDocumentosNecessarios() {
        $(".enviar_informacoes").on("click", function(e) {
            e.preventDefault();

            if( $("#responsavel").val() == "" ){
                alert("Por favor informe o responsável pelo preenchimento");
                $("#requerente").collapse('show');
                $("#responsavel").focus();
                return false;
            }

            if( $("#nome_requerente").val() == "" ){
                alert("Por favor informe o Nome do responsável pelo preenchimento");
                $("#requerente").collapse('show');
                $("#nome_requerente").focus();
                return false;
            }

            if( $("#identidade_requerente").val() == "" ){
                alert("Por favor informe a Identidade do responsável pelo preenchimento");
                $("#requerente").collapse('show');
                $("#identidade_requerente").focus();
                return false;
            }

            if( $("#orgao_emissor_requerente").val() == "" ){
                alert("Por favor informe o Órgão Emissor da Identidade do responsável pelo preenchimento");
                $("#requerente").collapse('show');
                $("#orgao_emissor_requerente").focus();
                return false;
            }

            if( $("#data_emissao_requerente").val() == "" ){
                alert("Por favor informe a Data de Emissão da Identidade do responsável pelo preenchimento");
                $("#requerente").collapse('show');
                $("#data_emissao_requerente").focus();
                return false;
            }

            if( $("#cpf_requerente").val() == "" ){
                alert("Por favor informe o CPF do responsável pelo preenchimento");
                $("#requerente").collapse('show');
                $("#cpf_requerente").focus();
                return false;
            }

            tabelaDocumentosRequerente = $("#documentos_requerente").DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Pesquisar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "bLengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "paginate": true
            }).columns.adjust().responsive.recalc();

            $achouDocumento = false;
            tabelaDocumentosRequerente.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 0){
                        if(column == "CPF do Requerente"){
                            $achouDocumento = true
                        }
                    }
                });
            });
            if(!$achouDocumento){
                alert("Por Favor é necessário anexar o CPF do Requerente");
                $("#requerente").collapse('show');
                $("#documento_cpf_requerente").focus();
                return false;
            }

            $achouDocumento = false;
            tabelaDocumentosRequerente.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 0){
                        if(column == "RG do Requerente"){
                            $achouDocumento = true
                        }
                    }
                });
            });
            if(!$achouDocumento){
                alert("Por Favor é necessário anexar o RG do Requerente");
                $("#requerente").collapse('show');
                $("#documento_rg_requerente").focus();
                return false;
            }

            if( $("#responsavel").val() == "4" || $("#responsavel").val() == "5" ){
                if( $("#cnpj").val() == "" ){
                    alert("Por favor informe o CNPJ");
                    $("#dados_mercantis").collapse('show');
                    $("#cnpj").focus();
                    return false;
                }
                if( $("#razao_social").val() == "" ){
                    alert("Por favor informe a Razão Social");
                    $("#dados_mercantis").collapse('show');
                    $("#razao_social").focus();
                    return false;
                }
            }

            if( $("#responsavel").val() == "6" ){
                if( $("#cresci").val() == "" ){
                    alert("Por favor informe o CRESCI");
                    $("#dados_mercantis").collapse('show');
                    $("#cresci").focus();
                    return false;
                }
            }

            if( $("#email").val() == "" ){
                alert("Por favor informe o E-Mail");
                $("#informacoes_contato").collapse('show');
                $("#email").focus();
                return false;
            }

            if( $("#confirmacao_email").val() == "" ){
                alert("Por favor informe a Confirmação do E-Mail");
                $("#informacoes_contato").collapse('show');
                $("#confirmacao_email").focus();
                return false;
            }

            if( $("#celular").val() == "" && $("#telefone").val() == "" ){
                alert("Por favor informe o número do Celular ou do Telefone ");
                $("#informacoes_contato").collapse('show');
                $("#celular").focus();
                return false;
            }

            if( $("#cep").val() == "" ){
                alert("Por favor informe o CEP");
                $("#dadosEndereco").collapse('show');
                $("#cep").focus();
                return false;
            }

            if( $("#numero").val() == "" ){
                alert("Por favor informe o Número de endereço");
                $("#dadosEndereco").collapse('show');
                $("#numero").focus();
                return false;
            }

            tabelaDocumentosEndereco = $("#documento_endereco").DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Pesquisar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "bLengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "paginate": true
            }).columns.adjust().responsive.recalc();

            $achouDocumento = false;
            tabelaDocumentosEndereco.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 0){
                        if(column == "Comprovante de Endereço"){
                            $achouDocumento = true
                        }
                    }
                });
            });
            if(!$achouDocumento){
                alert("Por Favor é necessário anexar o Comprovante de Endereço");
                $("#dadosEndereco").collapse('show');
                $("#documento_comprovante_residencia").focus();
                return false;
            }

            tabelaImovel = $("#tabela_documentos_imovel").DataTable({
                destroy: true,
                "oLanguage": {
                    "sProcessing": "Processando...",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Pesquisar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "bLengthChange": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "paginate": true
            }).columns.adjust().responsive.recalc();

            if(  !tabelaImovel.data().count() ){
                alert("É necessário incluir ao menos 1 imóvel para solicitar o IPTU ");
                $("#cadastro_imoveis").collapse('show');
                $("#possui_matricula").focus();
                return false;
            }

            console.log($('input[name="confirmacao_de_informacoes"]:checked').val());

            if($('input[name="confirmacao_de_informacoes"]:checked').val() != "sim"){
                alert("É necessário concordar com os termos de ciência");
                $("#confirmacao_de_informacoes").focus();
                return false;
            }



            $("form[name=formSalvarIptu]").submit();
        });

        $('#cnpj').change(function() {

            if(!validarCNPJ($("#cnpj").val())) {
                alert("CNPJ Inválido!");
                $("#cnpj").val("");
                $("#cnpj").focus();
                return false;
            }
        });
    }

    function verificarNecessidadeAdicionarDocumentosRequerente(documentos_necessarios) {
        let responsavel_preenchimento = $("select[name=responsavel]").val();
        let responsavel_legal = "2";

        // if (responsavel_preenchimento === responsavel_legal) {
            documentos_necessarios.push({
                "nome_sessao": "documentos_requerente",
                "descricao": "CPF do Requerente em (Documentos do Requerente)",
                "qtd": 2
            });
            documentos_necessarios.push({
                "nome_sessao": "documentos_requerente",
                "descricao": "RG do Requerente em (Documentos do Requerente)",
                "qtd": 2
            });
        // }
    }

    function verificarEnvioDocumentos(documentos_necessarios) {

        $.ajax({
            url: "{{ route('verificar_envio_documentos') }}",
            method: 'POST',
            data: {
                "documentos_necessarios": documentos_necessarios
            },
            success(response) {

                if (response.documentos_em_falta.length > 0) {
                    for (let i = 0; i < response.documentos_em_falta.length; i++) {
                        alert("Atenção: Os seguintes documentos são necessários: " + response.documentos_em_falta[i].descricao);
                    }
                } else {
                    $("form[name=formSalvarIptu]").submit();
                }
            }
        });
    }

    function pesquisaCep(cep) {

        var cep = cep.replace(/\D/g, '');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $('.ajax_load').fadeIn(200);

                var script = document.createElement('script');

                script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meuCallback';

                document.body.appendChild(script);

                $('.ajax_load').fadeOut(200);
            } else {
                limpaFormularioCepImovel();
                alert("Formato de CEP inválido.");

            }
        } else {
            limpaFormularioCep();
        }
    }

    function pesquisaCepImovel(cep) {

        var cep = cep.replace(/\D/g, '');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {

                $('.ajax_load').fadeIn(200);

                var script = document.createElement('script');

                script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meuCallbackImovel';

                document.body.appendChild(script);

                $('.ajax_load').fadeOut(200);
            } else {
                limpaFormularioCepImovel();
                alert("Formato de CEP inválido.");

            }
        } else {
            limpaFormularioCepImovel();
        }
    }

    function limpaFormularioCepImovel() {
        document.getElementById('i_endereco').value = ("");
        $("#i_endereco").attr('readonly', true);
        document.getElementById('i_bairro').value = ("");
        $("#i_bairro").attr('readonly', true);
        document.getElementById('i_municipio').value = ("");
        $("#i_municipio").attr('readonly', true);
        document.getElementById('i_estado').value = ("");
        $("#i_estado").attr('readonly', true);
    }

    function meuCallbackImovel(conteudo) {
        if (!("erro" in conteudo)) {
            document.getElementById('i_endereco').value = (conteudo.logradouro);
            if(conteudo.logradouro==""){
                $("#i_endereco").attr('readonly', false);
            }
            else{
                $("#i_endereco").attr('readonly', true);
            }
            document.getElementById('i_bairro').value = (conteudo.bairro);
            if(conteudo.bairro==""){
                $("#i_bairro").attr('readonly', false);
            }
            else{
                $("#i_bairro").attr('readonly', true);
            }
            document.getElementById('i_municipio').value = (conteudo.localidade);
            if(conteudo.localidade==""){
                $("#i_municipio").attr('readonly', false);
            }
            else{
                $("#i_municipio").attr('readonly', true);
            }
            document.getElementById('i_estado').value = (conteudo.uf);
            if(conteudo.uf==""){
                $("#i_estado").attr('readonly', false);
            }
            else{
                $("#i_estado").attr('readonly', true);
            }
        } else {
            limpaFormularioCepImovel();
            alert("CEP não encontrado.");
        }
    }

    function limpaFormularioCep() {
        document.getElementById('endereco').value = ("");
        $("#endereco").attr('readonly', true);
        document.getElementById('bairro').value = ("");
        $("#bairro").attr('readonly', true);
        document.getElementById('municipio').value = ("");
        $("#municipio").attr('readonly', true);
        document.getElementById('estado').value = ("");
        $("#estado").attr('readonly', true);
    }

    function meuCallback(conteudo) {
        if (!("erro" in conteudo)) {
            document.getElementById('endereco').value = (conteudo.logradouro);
            if(conteudo.logradouro==""){
                $("#endereco").attr('readonly', false);
            }
            else{
                $("#endereco").attr('readonly', true);
            }
            document.getElementById('bairro').value = (conteudo.bairro);
            if(conteudo.bairro==""){
                $("#bairro").attr('readonly', false);
            }
            else{
                $("#bairro").attr('readonly', true);
            }
            document.getElementById('municipio').value = (conteudo.localidade);
            if(conteudo.localidade==""){
                $("#municipio").attr('readonly', false);
            }
            else{
                $("#municipio").attr('readonly', true);
            }
            document.getElementById('estado').value = (conteudo.uf);
            if(conteudo.uf==""){
                $("#estado").attr('readonly', false);
            }
            else{
                $("#estado").attr('readonly', true);
            }
        } else {
            limpaFormularioCep();
            alert("CEP não encontrado.");
        }
    }

</script>

@endsection
