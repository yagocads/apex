@extends('layouts.tema_principal')

@section('content')
    <div class="container-fluid">

        @if ($exigenciaNaoRespondida)
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger p-5">
                        <b><i class="fa fa-volume-up"></i> Atençao!</b> Exigência já enviada, por favor aguarde a resposta.
                    </div>
                </div>
            </div>
        @else
            <div class="alert alert-info">
                <div class="row">
                    <div class="col-lg-12 mt-2">
                        <h3><i class="fa fa-exclamation-triangle"></i> Exigência <small>(Baixa Inscrição Municipal)</small></h3>
                        <hr/>
                    </div>

                    <input type="hidden" name="id_chamado" value="{{ $idChamado }}" />

                    <div class="col-lg-4">
                        <div class="form-row">
                            <div class="col-lg-12 form-group">
                                <label for="exigencia">Exigência:</label>
                                <textarea name="exigencia" 
                                    class="form-control form-control-sm" 
                                    id="exigencia"
                                    rows="8" cols="5" maxlength="255" 
                                    readonly>{{ $exigencia }}</textarea>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-lg-4">
                        <div class="form-row">
                            <div class="col-lg-12 form-group">
                                <label for="cumprimento_exigencia">Cumprimento de exigência:</label>
                                <textarea name="cumprimento_exigencia" 
                                    class="form-control form-control-sm" 
                                    id="cumprimento_exigencia"
                                    rows="8" cols="5" maxlength="255"></textarea>
                            </div>
                        </div>
                    </div>
        
                    <div class="col-lg-4">
                        <div class="form-row">
                            <div class="col-lg-12 form-group">
                                <label for="descricao_documento">Descrição do Documento:</label>
                                <input type="text" name="descricao_documento" 
                                    id="descricao_documento" 
                                    maxlength="40"
                                    class="form-control form-control-sm">
                            </div>
                        </div>
        
                        <div class="form-row">
                            <div class="col-lg-12 form-group">
                                <label for="documento_exigencia" class="d-block"><i class="fa fa-paperclip"></i> Anexar Arquivos:</label>
                                <input type="file" name="documento_exigencia" 
                                    id="documento_exigencia" />
                            </div>
                        </div>
        
                        <div class="form-row">
                            <div class="col-lg-12 form-group">
                                <button class="btn btn-primary btn-enviar-documento">
                                    <i class="fa fa-plus"></i> Adicionar Documento
                                </button>
                            </div>
                        </div> 
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-lg-12">
                        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>
        
                        <div class="conteudo_documentos_exigencia">
                            @include('empreendedorismo.baixa_inscricao_municipal.exigencia.tabela_documentos')
                        </div>
                    </div>
                    
                    <div class="col-lg-12 mt-3">
                        <button class="btn btn-success btn-enviar-exigencia">Enviar Exigência <i class="fa fa-send"></i></button>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('post-script')

<script>
    // Spinner
    let load = $(".ajax_load");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("body").on("click", ".btn-enviar-documento", function(e) {
        e.preventDefault();

        let descricao = $("input[name=descricao_documento]").val();
        let arquivo = $("input[name=documento_exigencia]")[0].files[0];

        if (!descricao) {
            alert("Atenção: Preencha a descrição do documento.");
            $("input[name=descricao_documento]").focus();
            return false;
        }

        if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'], 5048000)) {
            return false;
        }

        const formData = new FormData();
        formData.append("arquivo", arquivo);
        formData.append("descricao", descricao);
        formData.append("servidor", "ftp");
        formData.append("pastaServidor", "baixa_inscricao_municipal_exigencias");
        formData.append("nomeSessao", "documentos_exigencia");     
        formData.append("caminhoTabela", "empreendedorismo.baixa_inscricao_municipal.exigencia.tabela_documentos"); 

        $.ajax({
            url: "{{ route('salvar-documento-exigencia-baixa-inscricao-municipal') }}",
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {

                $("input[name=documento_exigencia]").val("");
                $("input[name=descricao_documento]").val("");

                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $(".conteudo_documentos_exigencia").html(response);
                recalcularDataTable("tabela_documentos_exigencia");
            }
        });
    });

    $("body").on("click", ".btn-remover-documento", function(e) {
        e.preventDefault();
        
        if (!confirm("Deseja realmente excluir?")) {
            return false;
        }

        let descricao = $(this).data("descricao");
        let caminho = $(this).data("caminho");

        $.ajax({
            url: "{{ route('remover-documento-exigencia-baixa-inscricao-municipal') }}",
            method: 'POST',
            data: {
                descricao,
                caminho,
                "servidor": "ftp",
                "pastaServidor": "baixa_inscricao_municipal_exigencias",
                "nomeSessao": "documentos_exigencia"  ,
                "caminhoTabela": "empreendedorismo.baixa_inscricao_municipal.exigencia.tabela_documentos"  
            },
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                
                load.fadeOut(200);

                if (response.error) {
                    alert("ATENÇÃO: " + response.error);
                    return false;
                }

                $(".conteudo_documentos_exigencia").html(response);
                recalcularDataTable("tabela_documentos_exigencia");
            }
        });
    });

    $("body").on("click", ".btn-enviar-exigencia", function(e) {
        e.preventDefault();

        const idChamado = $("input[name=id_chamado]").val();
        const exigencia = $("textarea[name=exigencia]").val();
        const cumprimentoExigencia = $("textarea[name=cumprimento_exigencia]").val();

        if (!exigencia) {
            alert("Atenção: O campo exigência é obrigatório.");
            return;
        }

        if (!cumprimentoExigencia) {
            alert("Atenção: O campo cumprimento da exigência é obrigatório.");
            return;
        }

        if ($(".dataTables_empty").html()) {
            let resposta = confirm("Atenção: Nenhum arquivo foi anexado, deseja continuar?");
            if (!resposta) {
                return;
            }
        }

        $.ajax({
            url: "{{ route('enviar-exigencia-baixa-inscricao-municipal') }}",
            method: 'POST',
            data: {
                "idChamado": idChamado,
                "exigencia": exigencia,
                "cumprimentoExigencia": cumprimentoExigencia
            },
            beforeSend: function () {
                load.fadeIn(200).css("display", "flex");
            },
            success(response) {
                
                load.fadeOut(200);

                if (response.error === "true") {
                    alert("ERRO ao enviar a exigência, entre em contato com a administração.");
                    return;
                }

                alert("Exigência enviada com sucesso!");
                location.href = "{{ route('acompanhamento') }}";
            }
        });
    });
</script>

@endsection