@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">

                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-home"></i> Consultar Solicitação de IPTU</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('consultar_iptu') }}" id="FormIptu">
                            @csrf
                            <div class="form-group">
                                <label for="cpf_ou_cnpj" class="control-label">Digite seu CPF/CNPJ</label>
                                <input maxlength="20" id="cpf_ou_cnpj" type="text" class="form-control {{ $errors->has('cpf_ou_cnpj') ? ' is-invalid' : '' }} cpfOuCnpj" 
                                    name="cpf_ou_cnpj" 
                                    value="" 
                                    required>
            
                                @if ($errors->has('cpf_ou_cnpj'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cpf_ou_cnpj') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="protocolo" class="control-label">Digite o Número do Protocolo</label>
                                <input maxlength="20" id="protocolo" type="text" class="form-control {{ $errors->has('protocolo') ? ' is-invalid' : '' }}" 
                                    name="protocolo" 
                                    value="" 
                                    required>
            
                                @if ($errors->has('protocolo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('protocolo') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="data_solicitacao" class="control-label">Digite Data da Solicitação</label>
                                <input maxlength="20" id="data_solicitacao" type="date" class="form-control {{ $errors->has('data_solicitacao') ? ' is-invalid' : '' }}" 
                                    name="data_solicitacao" 
                                    value="" 
                                    required>
            
                                @if ($errors->has('data_solicitacao'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('data_solicitacao') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                {!! $showRecaptcha !!}
                                
                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>
        
                            <a href="{{ route('iptu')  }}">
                                <button type="button" class="btn btn-primary form-group">
                                    <i class="fa fa-arrow-left"></i> Voltar
                                </button>
                            <a>

                            <button type="button" class="btn btn-success form-group" id="emitir_iptu">
                                Consultar Solicitação do IPTU
                            </button>
                        </form>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('post-script')

<script type="text/javascript">

    $("#emitir_iptu").prop('disabled', true);

    function onReCaptchaTimeOut(){
        $("#emitir_iptu").prop('disabled', true);
    }
    function onReCaptchaSuccess(){
        $("#emitir_iptu").prop('disabled', false);
    }


    $("#emitir_iptu").click(function() {

        if ($("#cpf_ou_cnpj").val() == "") {
            alert("Por favor informe a Matrícula do Imóvel");
            $("#cpf_ou_cnpj").focus();
            return false;
        }

        if ($("#protocolo").val() == "") {
            alert("Por favor informe o Protocolo da Solicitação");
            $("#protocolo").focus();
            return false;
        }

        if ($("#data_solicitacao").val() == "") {
            alert("Por favor informe a Data da Solicitação");
            $("#data_solicitacao").focus();
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        $("#FormIptu").submit();
        $("#cpf_ou_cnpj").val("");
        grecaptcha.reset();

        setTimeout(function(){ $("#mdlAguarde").modal('toggle'); }, 5000);
    });

    $('#cpf_ou_cnpj').change(function() {

        if ($("#cpf_ou_cnpj").val().length <= 14) {
            if(!validarCPF($("#cpf_ou_cnpj").val())) {
                alert("CPF Inválido!");
                $("#cpf_ou_cnpj").val("");
                $("#cpf_ou_cnpj").focus();
                return false;
            }
        } else{
            if(!validarCNPJ($("#cpf_ou_cnpj").val())) {
                alert("CNPJ Inválido!");
                $("#cpf_ou_cnpj").val("");
                $("#cpf_ou_cnpj").focus();
                return false;
            }
        }
    });

</script>
@endsection