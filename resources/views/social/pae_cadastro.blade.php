@extends('layouts.tema01')

@section('content')

<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="span8">
                <div class="inner-heading">
                    <h2>{{ __('PROGRAMA DE AMPARO AO EMPREGO ') }}</h2>
                </div>
            </div>
            <div class="span4">
                <ul class="breadcrumb">
                    <li><a href="{{  url('/') }}"><i class="fa fa-home"></i></a><i class="fa fa-angle-right"></i></li>
                    <li class="active">{{ __('PROGRAMA DE AMPARO AO EMPREGO ') }}</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row justify-content-left">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                <form method="POST" action="{{ route('pae_cadastro') }}" id="pae_cadastro">
                    @csrf
                    <input id="recurso" type="hidden" name="recurso" value="{{$recurso}}">
                    <input id="docEmpresa" type="hidden" name="docEmpresa" value="">
                    <input id="docReq" type="hidden" name="docReq" value="">
                    <input id="docEndereco" type="hidden" name="docEndereco" value="">
                    <input id="docBanco" type="hidden" name="docBanco" value="">
                    <input id="docFuncionarios" type="hidden" name="docFuncionarios" value="">
                    <input id="atividades" type="hidden" name="atividades" value="">
                    
                    <div class="card">
                        <div class="card-body">
                            <p>
                                <p>
                                    <a href='https://www.marica.rj.gov.br/2020/05/13/jom-especial-269/' target="_blank"><b>CONSULTE AQUI A LEI DO PROGRAMA PAE - PROGRAMA DE AMPARO AO EMPREGO</b></a>
                                </p>
                                <p>
                                    <a href='https://www.marica.rj.gov.br/2020/05/22/jom-especial-271/' target="_blank"><b>CONSULTE AQUI O DECRETO DO PROGRAMA PAE - PROGRAMA DE AMPARO AO EMPREGO</b></a>
                                </p>
                            </p>
                            <br>
                            <div class="accordion" id="accordion3">

                                {{-- 
                                ***********************************
                                *** Dados Mercantis  
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3"
                                            href="#collapse1">
                                            <i class="icon-plus"></i>
                                            Dados Mercantis 
                                        </a>
                                    </div>
                                    <div id="collapse1" class="accordion-body collapse">
                                        @include('social.pae_pessoa_juridica')
                                        {{-- @include('social.emconstrucao') --}}
                                    </div>
                                </div>

                                {{-- 
                                ***********************************
                                *** Dados do Requerente 
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseReq">
                                            <i class="icon-plus"></i>
                                            Dados do Requerente
                                        </a>
                                    </div>
                                    <div id="collapseReq" class="accordion-body collapse">
                                        @include('social.pae_requerente')
                                        {{-- @include('cadastro.emconstrucao') --}}
                                    </div>
                                </div>

                                {{-- 
                                ***********************************
                                *** Endereço
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3"
                                            href="#collapse2">
                                            <i class="icon-plus"></i> Endereço
                                        </a>
                                    </div>
                                    <div id="collapse2" class="accordion-body collapse">
                                        @include('social.pae_endereco')
                                        {{-- @include('social.emconstrucao') --}}
                                    </div>
                                </div>


                                {{-- 
                                ***********************************
                                *** Informações de Contato
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3"
                                            href="#collapse3">
                                            <i class="icon-plus"></i> Informações de Contato da Empresa
                                        </a>
                                    </div>
                                    <div id="collapse3" class="accordion-body collapse">
                                        @include('social.pae_informacoes_contato')
                                        {{-- @include('social.emconstrucao') --}}
                                    </div>
                                </div>

                                {{-- 
                                ***********************************
                                *** Dados Bancários
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse4">
                                            <i class="icon-plus"></i> Dados Bancários
                                        </a>
                                    </div>
                                    <div id="collapse4" class="accordion-body collapse">
                                        @include('social.pae_dados_bancarios')
                                        {{-- @include('social.emconstrucao') --}}
                                    </div>
                                </div>

                                {{-- 
                                ***********************************
                                *** Relação de Funcionários
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse5">
                                            <i class="icon-plus"></i> Relação de Funcionários
                                        </a>
                                    </div>
                                    <div id="collapse5" class="accordion-body collapse">
                                        @include('social.pae_funcionarios')
                                        {{-- @include('social.emconstrucao') --}}
                                    </div>
                                </div>
                                    
                                {{-- 
                                ***********************************
                                *** Auto declaração
                                ***********************************
                                --}}
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse8">
                                            <i class="icon-plus"></i> Declaração
                                        </a>
                                    </div>
                                    <div id="collapse8" class="accordion-body collapse">
                                        @include('social.pae_declaracao')
                                    </div>
                                </div>
                            </div>
                                <div class="span12 align-left">
                                    <a href="{{ route('PAE_CNPJ')}}">
                                        <button type="button" class="btn btn-theme e_wiggle">
                                            {{ __('Voltar') }}
                                        </button>
                                    <a>
                                
                                    <button type="button" class="btn btn-warning e_wiggle align-right" id="solicitarAuxilio">
                                        {{ __('Solicitar Auxílio') }}
                                    </button>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
</section>
@endsection

@section('post-script')

<script type="text/javascript">

    jQuery(document).ready(function(){

        $("#collapse1").collapse('show');

        $("#cnpj").mask("00.000.000/0000-00");
        $("#cpfFuncionario").mask("000.000.000-00");
        $("#dtAbertura").mask("00/00/0000");
        $("#celular").mask("(00)00000-0000");
        $("#telefone").mask("(00)0000-0000");
        $("#cep").mask("00.000-000");
        $("#numero").mask("000000");
        $("#inscrMunicipal").mask("00000");

        $("#dtEmissRespLegal").mask("00/00/0000");
        $("#cepRespLegal").mask("00.000-000");
        $("#cpfRespLegal").mask("000.000.000-00");
        $("#numeroRespLegal").mask("000000");
    });
    
    var AtividadesTbl = $('#AtividadesTbl').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 5,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 150,
        "info"          : true,
    });
    
    var documentosPJTbl = $('#documentosPJTbl').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 5,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 250,
        "info"          : false,
    });

    var documentosEnderecoTbl = $('#documentosEnderecoTbl').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 5,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 150,
        "info"          : false,
    });

    var fucionariosLancados = $('#fucionariosLancados').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 10,
        "searching"     : false,
        "ordering"      : true,
        "scrollY"       : 350,
        "info"          : true,
    });

    var documentosLancadosReq = $('#documentosLancadosReq').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 10,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 150,
        "info"          : false,
    });

    var documentosComprovanteBancario = $('#documentosComprovanteBancario').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "bLengthChange" : false,
        "pageLength"    : 10,
        "searching"     : false,
        "ordering"      : false,
        "scrollY"       : 150,
        "info"          : false,
    });

    $("#tipoInscricao").change(function(){
        if($("#tipoInscricao").val() == "Inscrição Municipal" ){
            $("#exigeCND").show();
            $("#inscrMunicipal").unmask();
            $("#inscrMunicipal").mask("00000");
        }
        else{
            $("#exigeCND").hide();
            $("#inscrMunicipal").unmask();
            $("#inscrMunicipal").mask("AAAAAAAAAAAAA");
        }
    });

    $("#banco").change(function(){
        if($("#banco").val() == "Caixa Econômica Federal" ){
            $("#exigeOperacao").show();
        }
        else{
            $("#exigeOperacao").hide();
        }
    });

    $("#percentualReducao").change(function(){
        if($("#percentualReducao").val() != "0" && $("#percentualReducao").val() != ""){
            $("#exigeAcordo").show();
        }
        else{
            $("#exigeAcordo").hide();
        }
    });

    $("#respPreenchimento").change(function(){
        if($("#respPreenchimento").val() == "Representante Legal" || $("#respPreenchimento").val() == "Sócio Cotista"  ){
            $("#ExigeRepresentacao").show();
        }
        else{
            $("#ExigeRepresentacao").hide();
        }
    });

    $("#btn_consultarCepRespLegal").click(function(){
        if ($("#cepRespLegal").val() == ""){
            alert("Informe o CEP");
            $("#cepRespLegal").focus();
            return false;
        }
        else{
            $("#mdlAguarde").modal('toggle');
            var cep = $("#cepRespLegal").val().replace(/[^0-9]/, '');
            var cep = $("#cepRespLegal").val().replace(/[^0-9]/g, '');
            if(cep){
                var url = 'https://viacep.com.br/ws/' + cep + '/json/';
                
                $.get( url,  function(obj){
                    // var obj = jQuery.parseJSON(resposta);
                    if(obj.logradouro){
                        // if (obj.localidade.toUpperCase() == "MARICÁ" || obj.localidade.toUpperCase() == "MARICA"){
                            $("#numeroRespLegal").focus();
    
                            $("#logradouroRespLegal").val(obj.logradouro);
                            $("#bairroRespLegal").val(obj.bairro);
                            $("#cidadeRespLegal").val(obj.localidade);
                            $("#ufRespLegal").val(obj.uf);
                            
                            $("#logradouroRespLegal").prop('readonly', false);
                            $("#bairroRespLegal").prop('readonly', true);
                            $("#cidadeRespLegal").prop('readonly', true);
                            $("#ufRespLegal").prop('disabled', true);
                        // }
                        // else{
                        //     $("#logradouro").val("");
                        //     $("#bairro").val("");
                        //     $("#cidade").val("");
                        //     $("#uf").val("");

                        //     alert("Só é possível cadastrar CEP de Maricá!");
                        // }
                    }
                    else if(obj.erro){
                        alert("CEP não encontrado ou inválido!");
                    }
                })
                .fail(function() {
                    alert("CEP não encontrado ou inválido!");
                })
                .always(function() {
                    $("#mdlAguarde").modal('toggle');
                });
               
            }					
        }
    });	

    $("#btn_consultar").click(function(){
        if ($("#cep").val() == ""){
            alert("Informe o CEP");
            $("#cep").focus();
            return false;
        }
        else{
            $("#mdlAguarde").modal('toggle');
            var cep = $("#cep").val().replace(/[^0-9]/, '');
            var cep = $("#cep").val().replace(/[^0-9]/g, '');
            if(cep){
                var url = 'https://viacep.com.br/ws/' + cep + '/json/';
                
                $.get( url,  function(obj){
                    // var obj = jQuery.parseJSON(resposta);
                    if(obj.logradouro){
                        if (obj.localidade.toUpperCase() == "MARICÁ" || obj.localidade.toUpperCase() == "MARICA"){
                            $("#numero").focus();
    
                            $("#logradouro").val(obj.logradouro);
                            $("#bairro").val(obj.bairro);
                            $("#cidade").val(obj.localidade);
                            $("#uf").val(obj.uf);
                            
                            $("#logradouro").prop('readonly', false);
                            $("#bairro").prop('readonly', true);
                            $("#cidade").prop('readonly', true);
                            $("#uf").prop('disabled', true);
                        }
                        else{
                            $("#logradouro").val("");
                            $("#bairro").val("");
                            $("#cidade").val("");
                            $("#uf").val("");

                            alert("Só é possível cadastrar CEP de Maricá!");
                        }
                    }
                    else if(obj.erro){
                        alert("CEP não encontrado ou inválido!");
                    }
                })
                .fail(function() {
                    alert("CEP não encontrado ou inválido!");
                })
                .always(function() {
                    $("#mdlAguarde").modal('toggle');
                });
               
            }					
        }
    });	
    
    $("#solicitarAuxilio").click(function () {


        // Validação Empresa
        // **************************
        if( $("#razaoSocial").val() == "" ){
            alert("Informe a Razão Social da Empresa");
            $("#collapse1").collapse('show');
            $("#razaoSocial").focus();
            return false;
        }

        if( $("#nomeFantasia").val() == "" ){
            alert("Informe o Nome Fantasia da Empresa");
            $("#collapse1").collapse('show');
            $("#nomeFantasia").focus();
            return false;
        }

        if( $("#NatJur").val().length == "" ){
            alert("Informe a Natureza Jurídica da Empresa");
            $("#collapse1").collapse('show');
            $("#NatJur").focus();
            return false;
        }

        if( $("#dtAbertura").val() == "" ){
            alert("Informe a Data de Abertura da Empresa");
            $("#collapse1").collapse('show');
            $("#dtAbertura").focus();
            return false;
        }

        if( !validadataNascimento3( $("#dtAbertura").val() ) ){
            alert("Data de Abertura inválida");
            $("#collapse1").collapse('show');
            $("#dtAbertura").focus();
            return false;
        }

        if( $("#tipoInscricao").val() == "" ){
            alert("Informe o TIPO DE INSCRIÇÃO MUNICIPAL que a Empresa possui.");
            $("#collapse1").collapse('show');
            $("#tipoInscricao").focus();
            return false;
        }

        if( $("#inscrMunicipal").val() == "" ){
            alert("Informe a Inscrição Municipal");
            $("#collapse1").collapse('show');
            $("#inscrMunicipal").focus();
            return false;
        }

        if( $("#porteEmpresa").val() == "" ){
            alert("Informe o Porte da Empresa");
            $("#collapse1").collapse('show');
            $("#porteEmpresa").focus();
            return false;
        }  

        if( $("#possuilAlvara").val() == "" ){
            alert("Informe se a Empresa POSSUI ALVARÁ");
            $("#collapse1").collapse('show');
            $("#possuilAlvara").focus();
            return false;
        }  


        var $achou = false;
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "Inscrição Municipal"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o COMPROVANTE DA INSCRIÇÃO MUNICIPAL em formato digital");
            $("#collapse1").collapse('show');
            $("#arquivoInscricaoMunicipal").focus();
            return false;
        }      

        var $achou = false;
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "Folha Salarial"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o COMPROVANTE DA FOLHA SALARIAL em formato digital");
            $("#collapse1").collapse('show');
            $("#arquivoFolhaSalarial").focus();
            return false;
        }      

        var $achou = false;
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "Contrato Social"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o COMPROVANTE DO CONTRATO SOCIAL em formato digital");
            $("#collapse1").collapse('show');
            $("#arquivoCSocial").focus();
            return false;
        }      

        var $achou = false;
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "Cartão CNPJ"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir O CARTÃO CNPJ em formato digital");
            $("#collapse1").collapse('show');
            $("#arquivoCartaoCnpj").focus();
            return false;
        }      

        if($("#tipoInscricao").val() == "Inscrição Municipal" ){
            var $achou = false;
            documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 0){
                        console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                        if(column == "Certidão Negativa"){
                            $achou = true
                        }
                    }
                });
            });
            if(!$achou){
                alert("É necessário incluir a CERTIDÃO NEGATIVA DE DÉBITOS COM O MUNICÍPIO em formato digital");
                $("#collapse1").collapse('show');
                $("#arquivoCertNegativa").focus();
                return false;
            }      
        }      

        var $achou = false;
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "Documento de Identidade do Sócio"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o DOCUMENTO DE IDENTIDADE DO SÓCIO em formato digital");
            $("#collapse1").collapse('show');
            $("#arquivoRGSocio").focus();
            return false;
        }      

        var $achou = false;
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "CPF do Sócio"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o CPF DO SÓCIO em formato digital");
            $("#collapse1").collapse('show');
            $("#arquivoCPFSocio").focus();
            return false;
        }      


        // Validação Resposável Legal
        // **************************
        if( $("#respPreenchimento").val() == "" ){
            alert("Informe o Responsável pelo Preenchimento");
            $("#collapseReq").collapse('show');
            $("#respPreenchimento").focus();
            return false;
        }

        if( $("#nomeRespLegal").val() == "" ){
            alert("Informe o nome do Responsável");
            $("#collapseReq").collapse('show');
            $("#nomeRespLegal").focus();
            return false;
        }

        if( $("#nacionalidadeRespLegal").val() == "" ){
            alert("Informe a Nacionalidade do Responsável");
            $("#collapseReq").collapse('show');
            $("#nacionalidadeRespLegal").focus();
            return false;
        }

        if( $("#estCivilRespLegal").val() == "" ){
            alert("Informe o Estado Civil do Responsável");
            $("#collapseReq").collapse('show');
            $("#estCivilRespLegal").focus();
            return false;
        }

        if( $("#idRespLegal").val() == "" ){
            alert("Informe a Identidade do Responsável");
            $("#collapseReq").collapse('show');
            $("#idRespLegal").focus();
            return false;
        }

        if( $("#orgEmRespLegal").val() == "" ){
            alert("Informe o Órgão Emissor da Identidade do Responsável");
            $("#collapseReq").collapse('show');
            $("#orgEmRespLegal").focus();
            return false;
        }

        if( $("#dtEmissRespLegal").val() == "" ){
            alert("Informe a Data de Emissão da Identidade do Responsável");
            $("#collapseReq").collapse('show');
            $("#dtEmissRespLegal").focus();
            return false;
        }

        if( !validadataNascimento3( $("#dtEmissRespLegal").val() ) ){
            alert("Data de Emissão da Identidade inválida");
            $("#collapseReq").collapse('show');
            $("#dtEmissRespLegal").focus();
            return false;
        }

        if( $("#cpfRespLegal").val() == "" ){
            alert("Informe o CPF do Responsável");
            $("#collapseReq").collapse('show');
            $("#cpfRespLegal").focus();
            return false;
        }

        if (!validarCPF($("#cpfRespLegal").val())) {
            alert("CPF do Responsável inválido");
            $("#collapseReq").collapse('show');
            $("#cpfRespLegal").focus();
            return false;
        }

        if( $("#emailRespLegal").val() == "" ){
            alert("Informe o EMAIL do Responsável");
            $("#collapseReq").collapse('show');
            $("#emailRespLegal").focus();
            return false;
        }

        if( !checkMail($("#emailRespLegal").val()) ){
            alert("EMAIL Inválido");
            $("#collapseReq").collapse('show');
            $("#emailRespLegal").focus();
            return false;
        }

        if( $("#emailRespLegal").val() != $("#confEmailRespLegal").val() ){
            alert("Os E-MAILS não são iguais");
            $("#collapseReq").collapse('show');
            $("#confEmailRespLegal").focus();
            return false;
        }

        if( $("#profissaoRespLegal").val() == "" ){
            alert("Informe a Profissão do Responsável");
            $("#collapseReq").collapse('show');
            $("#profissaoRespLegal").focus();
            return false;
        }

        if( $("#cepRespLegal").val() == "" ){
            alert("Informe o CEP do Responsável");
            $("#collapseReq").collapse('show');
            $("#cepRespLegal").focus();
            return false;
        }

        if( $("#ufRespLegal").val() == "" ){
            alert("Informe a UF do Responsável");
            $("#collapseReq").collapse('show');
            $("#ufRespLegal").focus();
            return false;
        }

        if( $("#cidadeRespLegal").val() == "" ){
            alert("Informe a Cidade do Responsável");
            $("#collapseReq").collapse('show');
            $("#cidadeRespLegal").focus();
            return false;
        }

        if( $("#bairroRespLegal").val() == "" ){
            alert("Informe o Bairro do Responsável");
            $("#collapseReq").collapse('show');
            $("#bairroRespLegal").focus();
            return false;
        }

        if( $("#logradouroRespLegal").val() == "" ){
            alert("Informe o Logradouro do Responsável");
            $("#collapseReq").collapse('show');
            $("#logradouroRespLegal").focus();
            return false;
        }

        if( $("#numeroRespLegal").val() == "" ){
            alert("Informe o Número do Endereço do Responsável");
            $("#collapseReq").collapse('show');
            $("#numeroRespLegal").focus();
            return false;
        }

        var $achou = false;
        documentosLancadosReq.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "CPF Representante"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o CPF DO RESPONSÁVEL em formato digital");
            $("#collapseReq").collapse('show');
            $("#arquivoCPFRespLegal").focus();
            return false;
        }  

        var $achou = false;
        documentosLancadosReq.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == "Comprovante de Endereço Representante"){
                        $achou = true
                    }
                }
            });
        });
        if(!$achou){
            alert("É necessário incluir o COMPROVANTE DE ENDEREÇO REPRESENTANTE em formato digital");
            $("#collapseReq").collapse('show');
            $("#arquivoEnderecoRespLegal").focus();
            return false;
        }  

        if($("#respPreenchimento").val() == "Representante Legal" || $("#respPreenchimento").val() == "Sócio Cotista"){
            var $achou = false;
            documentosLancadosReq.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                this.data().forEach(function(column, $posicao) {
                    if ($posicao == 0){
                        console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                        if(column == "Representação Legal"){
                            $achou = true
                        }
                    }
                });
            });
            if(!$achou){
                alert("É necessário incluir o COMPROVANTE DE REPRESENTAÇÃO LEGAL em formato digital");
                $("#collapseReq").collapse('show');
                $("#arquivoRespLegal").focus();
                return false;
            }  
        }

    
    
        // Validação Endereço
        // **************************
        if( $("#cep").val() == "" ){
            alert("Informe o CEP da Empresa");
            $("#collapse2").collapse('show');
            $("#cep").focus();
            return false;
        }

        if( $("#uf").val() == "" ){
            alert("Informe a UF da Empresa");
            $("#collapse2").collapse('show');
            $("#uf").focus();
            return false;
        }

        if( $("#cidade").val() == "" ){
            alert("Informe a Cidade da Empresa");
            $("#collapse2").collapse('show');
            $("#cidade").focus();
            return false;
        }

        if( $("#cidade").val().toUpperCase() != "MARICÁ" && $("#cidade").val().toUpperCase() != "MARICA" ){
            alert("Este benefício é destinado somente às empresas da Cidade de Maricá.");
            $("#collapse2").collapse('show');
            $("#cidade").focus();
            return false;
        }

        if( $("#bairro").val() == "" ){
            alert("Informe o Bairro da Empresa");
            $("#collapse2").collapse('show');
            $("#bairro").focus();
            return false;
        }

        if( $("#logradouro").val() == "" ){
            alert("Informe o Logradouro da Empresa");
            $("#collapse2").collapse('show');
            $("#logradouro").focus();
            return false;
        }

        if( $("#numero").val() == "" ){
            alert("Informe o Número do Endereço da Empresa");
            $("#collapse2").collapse('show');
            $("#numero").focus();
            return false;
        }

        if( documentosEnderecoTbl.rows().count()  == 0 ){
            alert("É necessário incluir o COMPROVANTE DE ENDEREÇO em formato digital");
            $("#collapse2").collapse('show');
            $("#arquivoEndereco").focus();
            return false;
        }  
    
        // Validação Contato
        // **************************
        if( $("#email").val() == "" ){
            alert("Informe o EMAIL da Empresa");
            $("#collapse3").collapse('show');
            $("#email").focus();
            return false;
        }

        if( !checkMail($("#email").val()) ){
            alert("EMAIL Inválido");
            $("#collapse3").collapse('show');
            $("#email").focus();
            return false;
        }

        if( $("#email").val() != $("#email-confirm").val() ){
            alert("Os E-MAILS não são iguais");
            $("#collapse3").collapse('show');
            $("#email-confirm").focus();
            return false;
        }

        if( $("#celular").val() == "" ){
            alert("Informe o Celular da Empresa");
            $("#collapse3").collapse('show');
            $("#celular").focus();
            return false;
        }


    
        // Validação Dados Bancários
        // **************************
        if( $("#banco").val() == "" ){
            alert("Informe o Nome do Banco da Empresa");
            $("#collapse4").collapse('show');
            $("#banco").focus();
            return false;
        }  
              
        if( $("#tipoConta").val() == "" ){
            alert("Informe o Tipo de Conta da Empresa");
            $("#collapse4").collapse('show');
            $("#tipoConta").focus();
            return false;
        }        

        if( $("#agencia").val() == "" ){
            alert("Informe a Agência");
            $("#collapse4").collapse('show');
            $("#agencia").focus();
            return false;
        }  

        if( $("#banco").val() == "Caixa Econômica Federal" ){
            if( $("#contaOperacao").val() == "" ){
                alert("Informe a Operação");
                $("#collapse4").collapse('show');
                $("#contaOperacao").focus();
                return false;
            }        
        }        

        if( $("#conta").val() == "" ){
            alert("Informe o Número da Conta");
            $("#collapse4").collapse('show');
            $("#conta").focus();
            return false;
        }    
        
        if(!$("#declaracaoBanco").is(':checked')){
            alert("É necessário concordar com os termos do programa");
            $("#collapse4").collapse('show');
            $("#declaracaoBanco").focus();
            return false;       
        }

        if( documentosComprovanteBancario.rows().count()  == 0 ){
            alert("É necessário incluir o COMPROVANTE DE CONTA em formato digital");
            $("#arquivoCompBancario").focus();
            return false;
        }  

        if( fucionariosLancados.rows().count()  == 0 ){
            alert("É necessário incluir os FUNCIONÁRIOS DA EMPRESA");
            $("#collapse5").collapse('show');
            $("#cpfFuncionario").focus();
            return false;
        }  

    
        // Validação Declarações
        // **************************

        if(!$("#declaracao1").is(':checked')){
            alert("É necessário concordar com os termos do programa");
            $("#collapse8").collapse('show');
            $("#declaracao1").focus();
            return false;       
        }

        if(!$("#declaracao2").is(':checked')){
            alert("É necessário concordar com os termos do programa");
            $("#collapse8").collapse('show');
            $("#declaracao2").focus();
            return false;       
        }

        if(!$("#declaracao3").is(':checked')){
            alert("É necessário concordar com os termos do programa");
            $("#collapse8").collapse('show');
            $("#declaracao3").focus();
            return false;       
        }

        if(!$("#declaracao4").is(':checked')){
            alert("É necessário concordar com os termos do programa");
            $("#collapse8").collapse('show');
            $("#declaracao4").focus();
            return false;       
        }

        if(!$("#declaracao5").is(':checked')){
            alert("É necessário concordar com os termos do programa");
            $("#collapse8").collapse('show');
            $("#declaracao5").focus();
            return false;       
        }

        $("#mdlAguarde").modal('toggle');

        var $docEmpresa = "";
        documentosPJTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($docEmpresa.length > 0 ){
                $docEmpresa += "|"
            }
            $docEmpresa += JSON.stringify(this.data());
        });
        $("#docEmpresa").val($docEmpresa);


        var $docRequerente = "";
        documentosLancadosReq.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($docRequerente.length > 0 ){
                $docRequerente += "|"
            }
            $docRequerente += JSON.stringify(this.data());
        });
        $("#docReq").val($docRequerente);

        var $docEndereco = "";
        documentosEnderecoTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($docEndereco.length > 0 ){
                $docEndereco += "|"
            }
            $docEndereco += JSON.stringify(this.data());
        });
        $("#docEndereco").val($docEndereco);

        var $docBanco = "";
        documentosComprovanteBancario.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($docBanco.length > 0 ){
                $docBanco += "|"
            }
            $docBanco += JSON.stringify(this.data());
        });
        $("#docBanco").val($docBanco);

        var $docFuncionarios = "";
        fucionariosLancados.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($docFuncionarios.length > 0 ){
                $docFuncionarios += "|"
            }
            $docFuncionarios += JSON.stringify(this.data());
        });
        $("#docFuncionarios").val($docFuncionarios);

        var $atividades = "";
        AtividadesTbl.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            if($atividades.length > 0 ){
                $atividades += "|"
            }
            $atividades += JSON.stringify(this.data());
        });
        $("#atividades").val($atividades);

        $("#uf").prop('disabled', false);
        $("#ufRespLegal").prop('disabled', false);

        $("#pae_cadastro").submit();
    });

    $("#lancarArquivoCPFRespLegal").click(function () {
        if ($("#arquivoCPFRespLegal").val() == "") {
            alert("Por favor escolha o CPF do Representante Legal para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCPFRespLegal');
        file = $input.files[0];
        // $ext = /\..*$/g.exec(file.name);

        
        $ext = file.name.split('.');

        $ext = "." +  $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formCPFRespLegal']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosLancadosReq.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosLancadosReq.columns.adjust().draw();
                $("#arquivoCPFRespLegal").val("");
                $arq01 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoEnderecoRespLegal").click(function () {
        if ($("#arquivoEnderecoRespLegal").val() == "") {
            alert("Por favor escolha o COMPROVANTE DE RESIDÊNCIA do Representante Legal para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoEnderecoRespLegal');
        file = $input.files[0];
        // $ext = /\..*$/g.exec(file.name);

        
        $ext = file.name.split('.');

        $ext = "." +  $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formEnderecoRespLegal']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosLancadosReq.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosLancadosReq.columns.adjust().draw();
                $("#arquivoEnderecoRespLegal").val("");
                $arq01 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoRespLegal").click(function () {
        if ($("#arquivoRespLegal").val() == "") {
            alert("Por favor escolha um documento de REPRESENTAÇÃO LEGAL para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoRespLegal');
        file = $input.files[0];
        // $ext = /\..*$/g.exec(file.name);
        
        $ext = file.name.split('.');

        $ext ="." +  $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formDocRespLegal']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosLancadosReq.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosLancadosReq.columns.adjust().draw();
                $("#arquivoRespLegal").val("");
                $arq02 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoEndereco").click(function () {
        if ($("#arquivoEndereco").val() == "") {
            alert("Por favor escolha um documento para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoEndereco');
        file = $input.files[0];
        // $ext = /\..*$/g.exec(file.name);

        $ext = file.name.split('.');

        $ext ="." +  $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formEndereco']")[0]);
        var $descricao = dados.get("DescrDoc");
        
        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosEnderecoTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosEnderecoTbl.columns.adjust().draw();
                $("#arquivoEndereco").val("");
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoCompBancario").click(function () {
        if ($("#arquivoCompBancario").val() == "") {
            alert("Por favor escolha o COMPROVANTE BANCÁRIO para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCompBancario');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formComprovanteBancario']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosComprovanteBancario.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosComprovanteBancario.columns.adjust().draw();
                $("#arquivoCompBancario").val("");
                $arq06 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoInscricaoMunicipal").click(function () {
        if ($("#arquivoInscricaoMunicipal").val() == "") {
            alert("Por favor escolha o Comprovante de INSCRIÇÃO MUNICIPAL para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoInscricaoMunicipal');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formInscricaoMunicipal']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoInscricaoMunicipal").val("");
                $arq03 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoFolhaSalarial").click(function () {
        if ($("#arquivoFolhaSalarial").val() == "") {
            alert("Por favor escolha o DEMONSTRATIVO DE FOLHA SALARIAL para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoFolhaSalarial');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formFolhaSalarial']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoFolhaSalarial").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoCSocial").click(function () {
        if ($("#arquivoCSocial").val() == "") {
            alert("Por favor escolha o CONTRATO SOCIAL para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCSocial');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formCSocial']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoCSocial").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoCartaoCnpj").click(function () {
        if ($("#arquivoCartaoCnpj").val() == "") {
            alert("Por favor escolha o CARTÃO DE CNPJ para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCartaoCnpj');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formCartaoCnpj']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoCartaoCnpj").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoCertNegativa").click(function () {
        if ($("#arquivoCertNegativa").val() == "") {
            alert("Por favor escolha CERTIDÃO DE DÉBITOS COM O MUNICÍPIO para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCertNegativa');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formCertNegativa']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoCertNegativa").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoRGSocio").click(function () {
        if ($("#arquivoRGSocio").val() == "") {
            alert("Por favor escolha o DOCUMENTO DE IDENTIDADE DO(S) SÓCIO(S) para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoRGSocio');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formRGSocio']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoRGSocio").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarArquivoCPFSocio").click(function () {
        if ($("#arquivoCPFSocio").val() == "") {
            alert("Por favor escolha o CPF DO(S) SÓCIO(S) para enviar");
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        $input = document.getElementById('arquivoCPFSocio');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formCPFSocio']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                documentosPJTbl.row.add([
                    $descricao,
                    resposta.nome_original,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                documentosPJTbl.columns.adjust().draw();
                $("#arquivoCPFSocio").val("");
                $arq04 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $("#lancarFuncionario").click(function () {

        if( fucionariosLancados.rows().count()  >= 49 ){
            alert("Só é possível incluir no máximo 49 funcionários.");
            $("#cpfFuncionario").focus();
            return false;
        }

        if ($("#cpfFuncionario").val() == "") {
            alert("Por favor informe o CPF do Funcionário");
            $("#cpfFuncionario").focus();
            return false;
        }

        if (!validarCPF($("#cpfFuncionario").val())) {
            alert("CPF Inválido");
            $("#cpfFuncionario").focus();
            return false;
        }

        if ($("#nomeFuncionario").val() == "") {
            alert("Por favor informe o Nome do Funcionário");
            $("#nomeFuncionario").focus();
            return false;
        }        

        if ($("#percentualReducao").val() == "") {
            alert("Por favor informe o Percentual de Redução do Salário do Funcionário");
            $("#percentualReducao").focus();
            return false;
        }        

        if ($("#percentualReducao").val() != 0 && $("#arquivoAcordoReducao").val() == "" ) {
            alert("Por favor informe o selecione o arquivo do Acordo de Redução do Salário do Funcionário");
            $("#arquivoAcordoReducao").focus();
            return false;
        }        

        if ($("#arquivoFuncionario").val() == "") {
            alert("Por favor selecione o arquivo SEFIP da Empresa para enviar");
            $("#arquivoFuncionario").focus();
            return false;
        }

        var $achou = false;
        fucionariosLancados.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            this.data().forEach(function(column, $posicao) {
                if ($posicao == 0){
                    console.log('row ' + rowIdx + ' column ' + $posicao + ' value ' + column);
                    if(column == $("#cpfFuncionario").val()){
                        alert("Esse CPF já foi incluído na listagem")
                        $("#cpfFuncionario").focus();
                        $achou = true
                        return false;
                    }
                }
            });
        });

        if($achou){
            return false;
        }

        var $input, file, $arquivo, $tamanho, $ext;
        
        if (!window.FileReader) {
            return false;
        }
        
        if($("#arquivoAcordoReducao").val() != ""){
            $inputAcordo = document.getElementById('arquivoAcordoReducao');
            file = $inputAcordo.files[0];
            
            $ext = file.name.split('.');
            $ext = "." + $ext[$ext.length-1];

            if (file.size > 2048000) {
                alert("O arquivo deve ter no máximo 2Mb");
                $("#arquivoAcordoReducao").focus();
                return false;
            }

            if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
                alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
                $("#arquivoAcordoReducao").focus();
                return false;
            }
        }
        
        $input = document.getElementById('arquivoFuncionario');
        file = $input.files[0];
        
        $ext = file.name.split('.');
        $ext = "." + $ext[$ext.length-1];

        if (file.size > 2048000) {
            alert("O arquivo deve ter no máximo 2Mb");
            $("#arquivoFuncionario").focus();
            return false;
        }

        if ($ext != '.jpeg' && $ext != '.jpg' && $ext != '.gif' && $ext != '.png' && $ext != '.pdf'  && $ext != '.JPEG' && $ext != '.JPG' && $ext != '.GIF' && $ext != '.PNG' && $ext != '.PDF') {
            alert("O arquivo deve ser do tipo 'jpeg', 'jpg', 'png', 'gif' ou 'pdf'");
            $("#arquivoFuncionario").focus();
            return false;
        }

        $("#mdlAguarde").modal('toggle');
        var dados = new FormData($("form[name='formFuncionario']")[0]);
        var $descricao = dados.get("DescrDoc");

        $.ajax({
            type: 'POST',
            url: "{{ url('/pae_documentos') }}",
            data: dados,
            processData: false,
            contentType: false

        }).done(function (resposta) {
            $("#mdlAguarde").modal('toggle');

            if (resposta.statusArquivoTransacao == 1) {
                fucionariosLancados.row.add([
                    $("#cpfFuncionario").val(),
                    $("#nomeFuncionario").val(),
                    $("#percentualReducao").val(),
                    resposta.arquivoReducao,
                    resposta.nome_novo,
                    '<button type="button" class="btn btn-red e_wiggle" >Remover</button>'
                ]).draw(false);
                fucionariosLancados.columns.adjust().draw();
                $("#cpfFuncionario").val("");
                $("#nomeFuncionario").val("");
                $("#percentualReducao").val("");
                $("#arquivoAcordoReducao").val("");
                $("#arquivoFuncionario").val("");
                $arq06 = true;
            }
            else {
                alert("Não foi possivel fazer o upload do seu arquivo");
                return false;
            }
        })
        .fail(function ( data ) {
            if( data.status === 422 ) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    console.log(key+ " " +value);
                    $('#response').addClass("alert alert-danger");

                    if($.isPlainObject(value)) {
                        $.each(value, function (key, value) {                       
                            console.log(key+ " " +value);
                        $('#response').show().append(value+"<br/>");

                        });
                    }else{
                    $('#response').show().append(value+"<br/>"); //this is my div with messages
                    }
                });
            }
        })
    });

    $('#documentosPJTbl').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = documentosPJTbl.row($(this).parents('tr')).data()[0];
        var nomeArquivo = documentosPJTbl.row($(this).parents('tr')).data()[2];
        documentosPJTbl.row($(this).parents('tr')).remove().draw(false);

        switch(tipo) {
            case "Inscrição Municipal":
                $arq03=false;
                break;
            case "Folha Salarial":
                $arq04=false;
                break;
            case "Contrato Social":
                $arq05=false;
                break;
            case "Cartão CNPJ":
                $arq06=false;
                break;
            case "Certidão Negativa":
                $arq07=false;
                break;
            case "Documento de Identidade do Sócio":
                $arq08=false;
                break;
            case "CPF do Sócio":
                $arq09=false;
                break;
        }
        $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivo, function (resposta) {

            $("#mdlAguarde").modal('toggle');

        })
    });

    $('#documentosLancadosReq').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = documentosLancadosReq.row($(this).parents('tr')).data()[0];
        var nomeArquivo = documentosLancadosReq.row($(this).parents('tr')).data()[2];
        documentosLancadosReq.row($(this).parents('tr')).remove().draw(false);

        switch(tipo) {
            case "Comprovante de Endereço Representante":
                $arq01=false;
                break;
            case "Representação Legal":
                $arq02=false;
                break;
        }
        $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivo, function (resposta) {

            $("#mdlAguarde").modal('toggle');

        })
    });

    $('#documentosEnderecoTbl').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = documentosEnderecoTbl.row($(this).parents('tr')).data()[0];
        var nomeArquivo = documentosEnderecoTbl.row($(this).parents('tr')).data()[2];
        documentosEnderecoTbl.row($(this).parents('tr')).remove().draw(false);

        $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivo, function (resposta) {

            $("#mdlAguarde").modal('toggle');

        })
    });

    $('#documentosComprovanteBancario').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = documentosComprovanteBancario.row($(this).parents('tr')).data()[0];
        var nomeArquivo = documentosComprovanteBancario.row($(this).parents('tr')).data()[2];
        documentosComprovanteBancario.row($(this).parents('tr')).remove().draw(false);

        $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivo, function (resposta) {

            $("#mdlAguarde").modal('toggle');

        })
    });

    $('#fucionariosLancados').on("click", "button", function () {
        $("#mdlAguarde").modal('toggle');

        var row = $(this).parents('tr').data();
        var tipo = fucionariosLancados.row($(this).parents('tr')).data()[0];
        var nomeArquivoAcordo = fucionariosLancados.row($(this).parents('tr')).data()[3];
        var nomeArquivo = fucionariosLancados.row($(this).parents('tr')).data()[4];
        fucionariosLancados.row($(this).parents('tr')).remove().draw(false);

        if(nomeArquivoAcordo != "" ){
            $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivoAcordo, function (resposta) {
            });
        }

        $.get("{{ url('/pae_documentos_remove') }}/"  + nomeArquivo, function (resposta) {
            $("#mdlAguarde").modal('toggle');
        })
    });

    function validadataNascimento3(valor){
        var date=valor;
        var ardt=new Array;
        var ExpReg=new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
        ardt=date.split("/");
        erro=false;
        if ( date.search(ExpReg)==-1){
            erro = true;
            }
        else if (((ardt[1]==4)||(ardt[1]==6)||(ardt[1]==9)||(ardt[1]==11))&&(ardt[0]>30))
            erro = true;
        else if ( ardt[1]==2) {
            if ((ardt[0]>28)&&((ardt[2]%4)!=0))
                erro = true;
            if ((ardt[0]>29)&&((ardt[2]%4)==0))
                erro = true;
        }
        if (erro) {
            return false;
        }
        return true;
    }

    // function validadataNascimento2(data){
    //     data = data.replace(/\//g, "-"); // substitui eventuais barras (ex. IE) "/" por hífen "-"
    //     var data_array = data.split("-"); // quebra a data em array
        
    //     // para o IE onde será inserido no formato dd/MM/yyyy
    //     if(data_array[0].length != 4){
    //         data = data_array[2]+"-"+data_array[1]+"-"+data_array[0]; // remonto a data no formato yyyy/MM/dd
    //     }
        
    //     // comparo as datas e calculo a idade
    //     var hoje = new Date();
    //     var nasc  = new Date(data);
    //     var idade = hoje.getFullYear() - nasc.getFullYear();
    //     var m = hoje.getMonth() - nasc.getMonth();
    //     if (m < 0 || (m === 0 && hoje.getDate() < nasc.getDate())) idade--;
        
    //     if(idade < 10){
    //         return false;
    //     }

    //     if(idade >= 10 && idade <= 120){
    //         return true;
    //     }
        
    //     // se for maior que 120!
    //     return false;
    // }

    // function validadataNascimento(data){
    //     data = data.replace(/\//g, "-"); // substitui eventuais barras (ex. IE) "/" por hífen "-"
    //     var data_array = data.split("-"); // quebra a data em array
        
    //     // para o IE onde será inserido no formato dd/MM/yyyy
    //     if(data_array[0].length != 4){
    //         data = data_array[2]+"-"+data_array[1]+"-"+data_array[0]; // remonto a data no formato yyyy/MM/dd
    //     }
        
    //     // comparo as datas e calculo a idade
    //     var hoje = new Date();
    //     var nasc  = new Date(data);
    //     var idade = hoje.getFullYear() - nasc.getFullYear();
    //     var m = hoje.getMonth() - nasc.getMonth();
    //     if (m < 0 || (m === 0 && hoje.getDate() < nasc.getDate())) idade--;
        
    //     if(idade < 16){
    //         return false;
    //     }

    //     if(idade >= 16 && idade <= 120){
    //         return true;
    //     }
        
    //     // se for maior que 120!
    //     return false;
    // }
    
    function validarCNPJ(cnpj) {

        cnpj = cnpj.replace(/[^\d]+/g, '');

        if (cnpj == '') return false;

        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" ||
            cnpj == "11111111111111" ||
            cnpj == "22222222222222" ||
            cnpj == "33333333333333" ||
            cnpj == "44444444444444" ||
            cnpj == "55555555555555" ||
            cnpj == "66666666666666" ||
            cnpj == "77777777777777" ||
            cnpj == "88888888888888" ||
            cnpj == "99999999999999")
            return false;

        // Valida DVs
        var tamanho = cnpj.length - 2
        var numeros = cnpj.substring(0, tamanho);
        var digitos = cnpj.substring(tamanho);
        var soma = 0;
        var pos = tamanho - 7;
        for (var i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (var i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;
    }


    function validarCPF(cpf) {
        var add;
        var rev;
        cpf = cpf.replace(/[^\d]+/g, '');
        if (cpf == '') return false;
        // Elimina CPFs invalidos conhecidos	
        if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
            return false;
        // Valida 1o digito	
        add = 0;
        for (var i = 0; i < 9; i++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito	
        add = 0;
        for (i = 0; i < 10; i++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }


    function checkMail(mail){	
        var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);	
        if(typeof(mail) == "string"){		
            if(er.test(mail)){ return true; }	
        }else if(typeof(mail) == "object"){		
            if(er.test(mail.value)){ 					
                return true; 				
            }	
        }else{		
            return false;		
        }
    }

</script>
@endsection