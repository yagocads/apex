<table class="table table-striped table-bordered dataTable dt-responsive nowrap w-100" id="debitosAbertos">
    <thead>
        <tr>
            <th data-priority="1">Arrecadação</th>
            <th data-priority="2">Parcela</th>
            <th>Vencimento</th>
            <th>Receita</th>
            <th>Valor</th>
            <th>Corrigido</th>
            <th>Juros</th>
            <th>Multa</th>
            <th>Desconto</th>
            <th>Total</th>
            <th data-priority="3"><input type="checkbox" name="checkAll" class="checkAll" id="checkAll"></th>
        </tr>
    </thead>
    <tbody>

        @php $parcelasJaMostradas = [] @endphp

        @if(isset($registros_unicos))
            @foreach($registros_unicos as $desconto)
                @if ($desconto->uvlrdesconto !== 0.00)
                    <tr>
                        <td>{{ $desconto->k00_numpre }}</td>
                        <td>
                            @if($desconto->k00_numpar == 0)
                                Cota Única
                            @else
                                {{ $desconto->k00_numpar }}
                            @endif
                        </td>
                        @if(date("Ymd",strtotime($desconto->dtvencunic)) < date("Ymd"))
                            <td><span style="color:#FF0000;">{{ date("d/m/Y",strtotime($desconto->dtvencunic)) }}</span></td>
                        @else
                            @if(date("m",strtotime($desconto->dtvencunic)) == date("m"))
                                <td><span style="color:#FFA500;">{{ date("d/m/Y",strtotime($desconto->dtvencunic)) }}</span></td>
                            @else
                                <td>{{ date("d/m/Y",strtotime($desconto->dtvencunic)) }}</td>
                            @endif
                        @endif
                        <td>{{ str_replace('+','',$desconto->k00_receit) }}</td>
                        <td>R$ {{ format_currency_brl($desconto->uvlrhis) }}</td>
                        <td>R$ {{ format_currency_brl($desconto->uvlrcor) }}</td>
                        <td>R$ {{ $desconto->uvlrjuros == '0' ? '0,00' : format_currency_brl($desconto->uvlrjuros) }}</td>
                        <td>R$ {{ $desconto->uvlrmulta == '0' ? '0,00' : format_currency_brl($desconto->uvlrmulta) }}</td>
                        <td>R$ {{ format_currency_brl($desconto->uvlrdesconto) }}</td>
                        <td>R$ {{ format_currency_brl($desconto->utotal) }}</td>
                        <td>&nbsp;<input type="radio" name="registros_normais[]" id="registros_normais" class="iptu_desconto" value="{{ $desconto->k00_numpre }}_{{ $desconto->k00_numpar }}_{{ $desconto->dtvencunic }}_{{ str_replace('+',' ',$desconto->k00_receit) }}_{{ format_currency_brl($desconto->uvlrhis) }}_{{ str_replace('.',',',$desconto->uvlrcor) }}_{{ $desconto->uvlrjuros }}_{{ $desconto->uvlrmulta }}_{{ format_currency_brl($desconto->uvlrdesconto) }}_{{ format_currency_brl($desconto->utotal) }}" /></td>
                    </tr>

                    @php $parcelasJaMostradas[] = $desconto->k00_numpar @endphp
                @endif
            @endforeach
        @endif

        @if(isset($registros))
        @foreach($registros as $registro)

            @if (in_array($registro->k00_numpar, $parcelasJaMostradas))
                @php continue; @endphp
            @endif

            <tr>
                <td>{{ $registro->k00_numpre }}</td>
                <td>{{ $registro->k00_numpar }}</td>
                @if(date("Ymd",strtotime($registro->k00_dtvenc)) < date("Ymd"))
                    <td><span style="color:#FF0000;">{{ date("d/m/Y",strtotime($registro->k00_dtvenc)) }}</span></td>
                @else
                    @if(date("m",strtotime($registro->k00_dtvenc)) == date("m"))
                        <td><span style="color:#FFA500;">{{ date("d/m/Y",strtotime($registro->k00_dtvenc)) }}</span></td>
                    @else
                        <td>{{ date("d/m/Y",strtotime($registro->k00_dtvenc)) }}</td>
                    @endif
                @endif
                <td>{{ str_replace('+','',$registro->k00_receit) }}</td>
                <td>R$ {{ format_currency_brl($registro->valor) }}</td>
                <td>R$ {{ format_currency_brl($registro->valorcorr) }}</td>
                <td>R$ {{ format_currency_brl($registro->juros) }}</td>
                <td>R$ {{ format_currency_brl($registro->multa) }}</td>
                <td>R$ {{ format_currency_brl($registro->desconto) }}</td>
                <td>R$ {{ format_currency_brl($registro->total) }}</td>
                <td>
                    @if($registro->valor == '0.00')
                        &nbsp;
                    @else
                        &nbsp;<input type="checkbox" name="registros_normais[]" id="registros_normais" class="registros_normais" value="{{ $registro->k00_numpre }}_{{ $registro->k00_numpar }}_{{ $registro->k00_dtvenc }}_{{ str_replace('+',' ',$registro->k00_receit) }}_{{ $registro->valor }}_{{ $registro->valorcorr }}_{{ $registro->juros }}_{{ $registro->multa }}_{{ $registro->desconto }}_{{ $registro->total }}" />
                    @endif
                </td>
            </tr>

        @endforeach
        @endif
    </tbody>
</table>

@section('post-script')
<script type="text/javascript">
    $(document).ready(function(){
        $(':checkbox[name^=checkAll]').click(function(){
            $('body #registros_normais').prop('checked',false);
            $('#linha_totais').html('');
            if($('#checkAll').is(':checked')){
                var checkbox = $('input:checkbox[name^=registros_normais]');
                checkbox.each(function(){
                    checkbox.prop('checked',true);
                });
                var checkbox_checked = $('input:checkbox[name^=registros_normais]:checked');
                if(checkbox_checked.length > 0){
                    var val = [];
                    var valor = 0;
                    var corrigido = 0;
                    var juros = 0;
                    var multa = 0;
                    var desconto = 0;
                    var total = 0;
                    checkbox_checked.each(function(){
                        val.push($(this).val());
                        var colunas = $(this).val().split("_");
                        valor += Number(colunas[4].replace(",",""));
                        corrigido += Number(colunas[5].replace(",",""));
                        juros += Number(colunas[6].replace(",",""));
                        multa += Number(colunas[7].replace(",",""));
                        desconto += Number(colunas[8].replace(",",""));
                        total += Number(colunas[9].replace(",",""));
                    });
                    $('#linha_totais').append("<p><b>Valor: R$ "+valor.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='valor_total' id='valor_total' value='"+valor+"'></b></p><p><b>Corrigido: R$ "+corrigido.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='corrigido_total' id='corrigido_total' value='"+corrigido+"'></b></p><p><b>Juros: R$ "+juros.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='juros_total' id='juros_total' value='"+juros+"'></b></p><p><b>Multa: R$ "+multa.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='multa_total' id='multa_total' value='"+multa+"'></b></p><p><b>Desconto: R$ "+desconto.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='desconto_total' id='desconto_total' value='"+desconto+"'></b></p><p><b>Total: R$ "+total.toLocaleString('pt-BR',{style: 'currency', currency:'BRL'})+"<input type='hidden' name='total_total' id='total_total' value='"+total+"'></b></p>");

                }
            } else {
                var checkbox = $('input:checkbox[name^=registros_normais]');
                checkbox.each(function(){
                    checkbox.prop('checked',false);
                });
            }
        });

        $('#debitosAbertos').DataTable({
            "stateSave": true,
            "bDestroy": true,
            "bPaginate": false,
            "ordering": true,
            "info": false,
            "searching": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
            }
        });
    });
</script>
@endsection
