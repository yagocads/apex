@extends('layouts.tema_principal')
@section('content')
<div class="container-fluid">
    @include('modals.modal_financeiro')
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-12">
                <h2 class="mb-4"><i class="fa fa-home"></i> Consulta Geral Financeira</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h4>{{ $pagina }}</h4></div>
                <div class="card-body">
                    <form method="post" id="formulario">
                        @csrf
                        <input type="hidden" name="tipo_pesquisa" id="tipo_pesquisa">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    @if(strlen(Auth::user()->cpf ) == 14)
                                    <label for="ro_cpf">CPF</label>
                                    @else
                                    <label for="ro_cpf">CNPJ</label>
                                    @endif
                                    <input type="hidden"name="cpf" id="cpf" value="{{ $cpf }}">
                                    <input type="text" class="form-control form-control-sm" name="ro_cpf" id="ro_cpf" value="{{ $cpf }}" readonly="readonly" />
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="ro_nome">Nome:</label>
                                    <input type="hidden"name="nome" id="nome" value="{{ $nome }}">
                                    <input type="text" class="form-control form-control-sm" name="ro_nome" id="ro_nome" value="{{ $nome }}" readonly="readonly" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <h5>Informe o tipo</h5>
                                    <span id="matriculaB" class="btn btn-primary btn-sm">Matrícula do Imóvel</span>
                                    &nbsp;
                                    <span id="inscricaoB" class="btn btn-success btn-sm">Inscrição Municipal</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 form-group">
                                    <label for="matricula">Matrícula do Imóvel:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="matricula" id="matricula" maxlength="8" disabled onkeypress='return onlyNumber(event)' />
                                        <div class="input-group-append">
                                            <input type="button" id="btn_pesquisar_matricula" class="btn btn-primary btn-sm" value="Pesquisar" disabled />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="inscricao">Inscrição Municipal:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control form-control-sm" name="inscricao" id="inscricao" maxlength="8" disabled onkeypress='return onlyNumber(event)' />
                                        <div class="input-group-append">
                                            <input type="button" id="btn_pesquisar_inscricao" class="btn btn-primary btn-sm" value="Pesquisar" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 form-group" id="linha_debitos" style="visibility: hidden;">
                                    <label for="tipo_debito">Tipo de Débito:</label>
                                    <select class="form-control form-control-sm" id="tipo_debito" name="tipo_debito"><option value="0">Selecione o tipo de débito</option></select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'financeiro']) }}"
                                class="btn btn-primary">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>
                            <button id="btn_avancar" class="btn btn-success" style="visibility: hidden;" >Avançar <i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('post-script')
    <script type="text/javascript">
        $(document).ready(function(){
            if($("#cpf").val().length == 14){
                $('#btn_voltar').click(function(){
                    window.location.replace("servicos/1/financeiro");
                });
            }
            else{
                $('#btn_voltar').click(function(){
                    window.location.replace("servicos/2/financeiro");
                });
            }
            if($('#cpf').val().length == 14){
                var cpf1 = $('#cpf').val();
                var cpf2 = cpf1.replace("/","-");
                $('#cpf').val(cpf2);
            }

            $('#btn_pesquisar_inscricao').click(function(){
                let load = $(".ajax_load");
                load.fadeIn(200).css("display", "flex");

                $(".modal-title").html("Pesquisar Inscrição");
                $(".modal-body").html("<div class='callout-white'><div class'table_head'><div class='row'><div class='col-lg-3'><p><strong>Inscrição</strong></p></div><div class='col-lg-3'><p><strong>Nome</strong></p></div><div class='col-lg-3'><p><strong>Endereço</strong></p></div><div class='col-lg-3'><p><strong>Ações</strong></p></div></div></div><div class='table_body'></div></div>");
                $('.table_body').html('');

                let cpf = $('#ro_cpf').val();
                let insc = $('#inscricao').val();
                let document = formatDocument(cpf);
                var url = 'taxasconsultainscricao/' + document +'/';

                $.ajax({
                    url: url,
                    method: 'GET',
                    beforeSend: function () {
                        load.fadeIn(200).css("display", "flex");
                    },
                    success(response) {
                        var obj = jQuery.parseJSON(response);

                        if(obj.iStatus == 1) {
                            load.fadeOut(200);
                            if (obj.aInscBase[0].q02_inscr == insc || insc == '') {
                                $('.table_body').append(
                                    "<div class='row'>"+
                                        "<div class='col-lg-3'>"+obj.aInscBase[0].q02_inscr+"</div>"+
                                        "<div class='col-lg-3'>"+unescape(obj.aInscBase[0].z01_nome).replace(/[+]/g, " ")+"</div>"+
                                        "<div class='col-lg-3'>"+unescape(obj.aInscBase[0].z01_ender).replace(/[+]/g, " ")+"</div>"+
                                        "<div class='col-lg-3'><button type='button' class='btn btn-primary' onClick='selecionaPesquisaInscricao(" + obj.aInscBase[0].q02_inscr + ");'  >Selecionar</button></div>"+
                                    "</div>");
                                $("#footer-modal").modal('show');
                            } else {
                                load.fadeOut(200);
                                alert("Não foi encontrada a inscrição informada.");
                                return false;
                            }
                        } else {
                            load.fadeOut(200);
                            alert("Nenhuma inscrição foi encontrada para este usuário.");
                            return false;
                        }
                        load.fadeOut(200);
                    }
                });
            });
        });

        function selectionaPesquisa(matricula){
            let load = $(".ajax_load");
            load.fadeIn(200).css("display", "flex");

            $('#tipo_pesquisa').val('m');
            $('#inscricao').val('');
            $.post("{{ route('integracaoRelatorioTotalDebitos') }}",{
                '_token':'{{ csrf_token() }}',
                'cpf': $('#ro_cpf').val(),
                'nome':$('#ro_nome').val(),
                'matricula': matricula,
                'tipo': 'm',
                'tipo_relatorio': 'r' },function(data){
                if(data.error){
                    load.fadeOut(200);
                    alert(data.error);
                } else {

                    $("#matricula").val(matricula);
                    $('#hd_matricula').val(matricula);
                    $("#pesquisa").modal('hide');
                    $('#tipo_debito').html("");
                    $('#tipo_debito').append("<option value='0'>Selecione o tipo de débito</option>");

                    $('#linha_debitos').css("visibility","visible");
                    var cpf = $('#cpf').val();
                    if(cpf.length == 14){
                        var cpf_new = cpf.replace("/","-");
                        cpf = cpf_new;
                    }

                    $.post("{{ route('integracaoPesquisaTipoDebitos') }}",{
                        '_token':'{{ csrf_token() }}',
                        'cpf': cpf,
                        'matricula': $('#matricula').val(),
                        'tipo': 'm' },function(data){
                        if(data == ''){
                            alert("Esse imóvel não possui Débitos em Aberto.");
                            load.fadeOut(200);
                        } else {
                            load.fadeOut(200);
                            var obj = jQuery.parseJSON(data);
                            for(var i=0; i < obj.aTiposDebitosEncontradas.length; i++){
                                var descricao_bebito = obj.aTiposDebitosEncontradas[i].sDescricaoDeb;
                                var new_descricao_bebito = descricao_bebito.replace("+"," ");
                                $('#tipo_debito').append("<option value='"+obj.aTiposDebitosEncontradas[i].iTipoDeb+"'>"+new_descricao_bebito.replace("+"," ")+"</option>")
                            }
                            $("#footer-modal").modal('hide');
                        }
                    });
                }
            });
        }

        function selecionaPesquisaInscricao(inscricao){
            let load = $(".ajax_load");
            load.fadeIn(200).css("display", "flex");

            $('#tipo_pesquisa').val('i');
            $('#matricula').val('');
            $.post("{{ route('integracaoRelatorioTotalDebitos') }}",{
                '_token':'{{ csrf_token() }}',
                'cpf': $('#ro_cpf').val(),
                'nome':$('#ro_nome').val(),
                'inscricao': inscricao,
                'tipo': 'i',
                'tipo_relatorio': 'r' },function(data){

                if(data.error){
                    load.fadeOut(200);
                    alert(data.error);
                } else {
                    load.fadeOut(200);
                    $("#inscricao").val(inscricao);
                    $('hd_inscricao').val(inscricao);
                    $("#pesquisaInscricaoTaxa").modal('hide');
                    $('#tipo_debito').html("");
                    $('#tipo_debito').append("<option value='0'>Selecione o tipo de débito</option>");

                    $('#linha_debitos').css("visibility","visible");
                    var cpf = $('#cpf').val();
                    if(cpf.length == 14){
                        var cpf_new = cpf.replace("/","-");
                        cpf = cpf_new;
                    }
                    $.post("{{ route('integracaoPesquisaTipoDebitos') }}",{
                        '_token':'{{ csrf_token() }}',
                        'cpf': cpf,
                        'inscricao': inscricao,
                        'tipo': 'i' },function(data){
                        if(data == ''){
                            load.fadeOut(200);
                            alert("Essa Inscrição Municipal não possui débitos em aberto.");
                        } else {
                            $('#inscricao').prop('readonly',true);
                            load.fadeOut(200);
                            var obj = jQuery.parseJSON(data);
                            for(var i=0; i < obj.aTiposDebitosEncontradas.length; i++){
                                var descricao_bebito = obj.aTiposDebitosEncontradas[i].sDescricaoDeb;
                                var new_descricao_bebito = descricao_bebito.replace("+"," ");
                                $('#tipo_debito').append("<option value='"+obj.aTiposDebitosEncontradas[i].iTipoDeb+"'>"+new_descricao_bebito.replace("+"," ")+"</option>")
                            }
                            $("#footer-modal").modal('hide');
                        }
                    });
                }
            });
        }

        $('#tipo_debito').change(function(){
            if ( $('#tipo_debito').val() == 5 || $('#tipo_debito').val() == 34){
                $('#footer-modal').find('.modal-title').html('<h3>Dívida Ativa</h3>');
                $('#footer-modal').find('.modal-body').html('<p>Prezado Contribuinte, verificamos que constam débitos em dívida ativa para este imóvel. Para realizar a quitação ou parcelamento dos mesmos, basta acessar o link abaixo e agendar um atendimento:<br><br><b>Link:</b> <a href="https://maricadigital.com.br/agendar-servico">https://maricadigital.com.br/agendar-servico</a><br><b>Grupo:</b> Procuradoria Geral do Município<br><b>Serviço:</b> dívida ativa<br><br>Escolher o Pólo de Atendimento, dia e horário.</p>')
                $('#footer-modal').find('.modal-footer').html('<button class="btn btn-success" id="msg_ok" data-dismiss="modal">OK</button>')
                
                $('#footer-modal').modal("show");
            }
            else{
                $('#formulario').attr("action","{{ Route('relatorioDebitosAbertos') }}");
                $('#formulario').submit();
            }
        });
    </script>
@endsection
