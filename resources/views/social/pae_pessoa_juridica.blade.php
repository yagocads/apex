<div class="accordion-inner">
    {{-- CPF/CNPJ --}}
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="cpf" class="col-md-4 col-form-label text-md-right">
                {{ __('CNPJ') }} 
            </label>
        </div>
        <div class="span2">
            <input maxlength="20" id="cpfPJ" type="text" class="form-input" name="cpf" value="{{$cpf}}" readonly>
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="razaoSocial" class="col-md-4 col-form-label text-md-right">
                    {{ __('Razão Social') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="razaoSocial" type="text" class="form-input"  name="razaoSocial" value="{{$empresa->nome}}"
            @if($empresa->nome != "")
            readonly
            @endif
            >
        </div>
    </div>
    
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="nomeFantasia" class="col-md-4 col-form-label text-md-right">
                    {{ __('Nome Fantasia') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span7">
            <input maxlength="100" id="nomeFantasia" type="text" class="form-input"  name="nomeFantasia" value="{{$empresa->fantasia}}"
            @if($empresa->fantasia != "")
            readonly
            @endif
            >
        </div>
    </div>
    
    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="NatJur" class="col-md-4 col-form-label text-md-right">
                {{ __('Natureza Jurídica') }}
            </label>
        </div>

        <div class="span7">
            <input maxlength="40" id="NatJur" type="text" class="form-input" name="NatJur" value="{{$empresa->natureza_juridica}}"
            @if($empresa->natureza_juridica != "")
            readonly
            @endif
            >

        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="dtAbertura" class="col-md-4 col-form-label text-md-right">
                {{ __('Data de Abertura') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span2">
            <input id="dtAbertura" type="text" class="form-input" name="dtAbertura" value="{{ $empresa->abertura }}"
            @if($empresa->abertura != "")
            readonly
            @endif
            >
        </div>
        {{-- Separação --}}

        <div class="controls span2 form-label">
            <label for="inscrEstadual" class="col-md-4 col-form-label text-md-right">
                {{ __('Inscrição Estadual') }} 
            </label>
        </div>
    
        <div class="span3">
            <input maxlength="15" id="inscrEstadual" type="text" class="form-input"  name="inscrEstadual" value="">
        </div>
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="tipoInscricao" class="col-md-4 col-form-label text-md-right">
                {{ __('Tipo Inscrição') }} <span class="required">*</span>
            </label>
        </div>
    
        <div class="span2">
            <select name="tipoInscricao" id="tipoInscricao" style="width:100%;">
                <option value=""></option>
                <option value="Inscrição Municipal">Inscrição Municipal </option>
                <option value="Protocolo de Solicitação">Protocolo de Solicitação</option>
            </select>
        </div>

        <div class="controls span2 form-label">
            <label for="inscrMunicipal" class="col-md-4 col-form-label text-md-right">
                {{ __('Inscrição Municipal') }} <span class="required">*</span>
            </label>
        </div>

        <div class="span3">
            <input id="inscrMunicipal" type="text" class="form-input" name="inscrMunicipal" value="">
        </div>

    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="porteEmpresa" class="col-md-4 col-form-label text-md-right">
                {{ __('Porte') }} <span class="required">*</span>
            </label>
        </div>
    
        <div class="span3">
            <select name="porteEmpresa" id="porteEmpresa" style="width:100%;">
                <option value=""></option>
                <option value="MEI">MEI</option>
                <option value="Microempresa">Microempresa</option>
                <option value="Empresa de Pequeno Porte">Empresa de Pequeno Porte</option>
            </select>
        </div>
        
        <div class="controls span3 form-label">
            <label for="possuilAlvara" class="col-md-4 col-form-label text-md-right">
                {{ __('Posssui Alvará?') }} <span class="required">*</span>
            </label>
        </div>
    
        <div class="span1">
            <select name="possuilAlvara" id="possuilAlvara" style="width:100%;">
                <option value=""></option>
                <option value="SIM">SIM</option>
                <option value="NÃO">NÃO</option>
            </select>
        </div>        
    </div>

    <div class="row formrow">
        <div class="controls span3 form-label">
            <label for="porteEmpresa" class="col-md-4 col-form-label text-md-right">
                {{ __('Atividades') }} 
            </label>
        </div>
    
        <div class="span7">
            <table id="AtividadesTbl" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Código</th>
                        <th>Descrição</th>
                    </tr>
                </thead>
                @foreach ($empresa->atividade_principal as $atividade)
                    <tr>
                        <td>Principal</td>
                        <td>{{$atividade->code }}</td>
                        <td>{{$atividade->text }}</td>
                    </tr>
                @endforeach
                @foreach ($empresa->atividades_secundarias as $atividade)
                    <tr>
                        <td>Secundária</td>
                        <td>{{$atividade->code }}</td>
                        <td>{{$atividade->text }}</td>
                    </tr>
                @endforeach
                <tbody>
                </tbody>
            </table>

        </div>
    </div>
    <br>
    <br>





    <form method="POST" action="" enctype="multipart/form-data">
        @csrf
    </form>

    <form method="POST" action="" id="formInscricaoMunicipal" name="formInscricaoMunicipal" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Comprovação e/ou solicitação de Inscrição Municipal') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Inscrição Municipal" />
                <input type="hidden" name="tipoDoc" value="inscricaoMunicipal" />
                <input type="hidden" name="idCpf" id="idInscricaoMunicipal" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoInscricaoMunicipal" />
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoInscricaoMunicipal">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formFolhaSalarial" name="formFolhaSalarial" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Demonstrativo de Folha Salarial (Abril/2020)') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Folha Salarial" />
                <input type="hidden" name="tipoDoc" value="FolhaSalarial" />
                <input type="hidden" name="idCpf" id="idFolhaSalarial" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoFolhaSalarial" />
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoFolhaSalarial">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formCSocial" name="formCSocial" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Contrato Social') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Contrato Social" />
                <input type="hidden" name="tipoDoc" value="ContratoSocial" />
                <input type="hidden" name="idCpf" id="idCSocial" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoCSocial" />
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCSocial">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formCartaoCnpj" name="formCartaoCnpj" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Cartão de CNPJ') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Cartão CNPJ" />
                <input type="hidden" name="tipoDoc" value="CartaoCnpj" />
                <input type="hidden" name="idCpf" id="idCartaoCnpj" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoCartaoCnpj" />
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCartaoCnpj">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formCertNegativa" name="formCertNegativa" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Certidão de débitos com o Município') }} <span id="exigeCND" class="required" style="display: none;">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Certidão Negativa" />
                <input type="hidden" name="tipoDoc" value="CertNegativa" />
                <input type="hidden" name="idCpf" id="idCertNegativa" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoCertNegativa" />
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCertNegativa">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formRGSocio" name="formRGSocio" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('Documento de Identidade do(s) Sócio(s)') }} <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="Documento de Identidade do Sócio" />
                <input type="hidden" name="tipoDoc" value="RGSocio" />
                <input type="hidden" name="idCpf" id="idRGSocio" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoRGSocio" />
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoRGSocio">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <form method="POST" action="" id="formCPFSocio" name="formCPFSocio" enctype="multipart/form-data">
        @csrf
        <div class="row formrow">
            <div class="controls span3 form-label">
                <label for="documento" class="col-md-4 col-form-label text-md-right">
                    {{ __('CPF do(s) Sócio(s)') }}  <span class="required">*</span>
                </label>
            </div>

            <div class="span6">
                <input type="hidden" name="DescrDoc" value="CPF do Sócio" />
                <input type="hidden" name="tipoDoc" value="CPFSocio" />
                <input type="hidden" name="idCpf" id="idCPFSocio" value="{{ $cpf }}" />
                <input type="file" name="arquivo" id="arquivoCPFSocio" /><br>
                Obs.: O tamanho máximo dos arquivos é de 2Mb.
            </div>
            <div class="span2">
                <button type="button" class="btn btn-small btn-blue e_wiggle align-left" id="lancarArquivoCPFSocio">
                    {{ __('Lançar') }}
                </button>
            </div>
        </div>
    </form>

    <div class="row formrow">
        <div class="span11">
            <h4 class="heading">Documentos Lançados<span></span></h4>

            <table id="documentosPJTbl" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th width='20%'>Descrição</th>
                        <th>Nome Original</th>
                        <th>Nome Final</th>
                        <th width='20%'>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>



</div>
