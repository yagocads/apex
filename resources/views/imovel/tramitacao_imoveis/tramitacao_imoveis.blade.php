@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">

        @if (session('success'))
            @include('confirmacao.confirmacao_processo')
        @else
            <div class="col-lg-12">
                <h3 class="mb-3">
                    <i class="fa fa-home"></i> Tramitação de Processos de Legalização de Imóveis
                </h3>

                @if (session('tramitacaoJaExiste'))
                    <div class="alert alert-danger">
                        {!! session('tramitacaoJaExiste') !!}
                    </div>
                @endif
            
                <form method="POST" id="formTramitacao" action="{{ route('salvar-tramitacao-processos-legalizacao-imoveis') }}" autocomplete="off">
                    @csrf
    
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <span class="d-block">* {{ $error }}</span>
                            @endforeach
                        </div>
                    @endif
                
                    <div id="accordion">
                        
                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#requerente">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Requerente
                                </div>
                            </a>
                            <div id="requerente" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("imovel.tramitacao_imoveis.requerente")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3 d-none">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#requisitante">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Requisitante
                                </div>
                            </a>
                            <div id="requisitante" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("imovel.tramitacao_imoveis.requisitante")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados_processo">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Processo
                                </div>
                            </a>
                            <div id="dados_processo" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("imovel.tramitacao_imoveis.dados_processo.dados_processo")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3 documentacao {{ old('categoria_processo') == null ? 'd-none' : '' }}">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#documentacao">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Documentação
                                </div>
                            </a>
                            <div id="documentacao" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("imovel.tramitacao_imoveis.documentacao.documentacao")
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3 d-none">
                            <a class="card-link no-underline text-dark" data-toggle="collapse" href="#dados_despacho">
                                <div class="card-header">
                                    <i class="fa fa-arrow-down arrow-red"></i> Dados do Despacho
                                </div>
                            </a>
                            <div id="dados_despacho" class="collapse" data-parent="#accordion">
                                <div class="card-body">
                                    @include("imovel.tramitacao_imoveis.dados_despacho")
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <a href="{{ route('servicos', ['guia' => get_guia_services(), 'aba' => 'imovel']) }}" class="btn btn-primary mt-2">
                                <i class="fa fa-arrow-left"></i> Voltar
                            </a>
                        
                            <button class="btn btn-success mt-2 enviar_informacoes" disabled="true">Enviar Informações <i class="fa fa-send"></i></button>
                        </div>
                    </div>

                </form>
            </div>
        @endif
        
    </div>
</div>

@endsection

@section('post-script')

<script>

    // Spinner
    let load = $(".ajax_load");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    primeiroCarregamentoDadosProcesso();

    function primeiroCarregamentoDadosProcesso() {

        let categoriaProcesso = $("select[name=categoria_processo]").val();
        popularDadosDoProcessoEDocumentacao(categoriaProcesso);
    }

    carregarDadosProcessoConformeCategoria();
    
    function carregarDadosProcessoConformeCategoria() {

        $("select[name=categoria_processo]").change(function() {
            let categoriaProcesso = $(this).val();
            let forgetSession = true;

            popularDadosDoProcessoEDocumentacao(categoriaProcesso, forgetSession);
        });
    }

    function popularDadosDoProcessoEDocumentacao(categoriaProcesso, forgetSession = null) {

        $.ajax({
            url: "{{ route('tramitacao-imoveis-dados-processo-json') }}",
            method: 'POST',
            data: {categoriaProcesso, forgetSession},
            dataType: 'json',
            success(response) {
            
                if (Object.keys(response).length > 0) {

                    let documentacao = requisitarDocumentoConformeTipoRequerente(response.documentacao_processo);

                    $(".descricao_processo").removeClass("d-none");
                    $(".documentacao_obrigatoria").removeClass("d-none");
                    $(".documentacao").removeClass("d-none");
                    $(".enviar_informacoes").attr("disabled", false);

                    $(".descricao_processo_conteudo").html(response.descricao_processo);

                    renderTabelaDocumentacao();
                    popularListaDocumentacaoObrigatoria(documentacao);
                    popularDescricaoDocumento(documentacao);

                } else {
                    $(".descricao_processo").addClass("d-none");
                    $(".documentacao_obrigatoria").addClass("d-none");
                    $(".documentacao").addClass("d-none");
                    $(".enviar_informacoes").attr("disabled", true);
                }
            }
        });
    }

    function renderTabelaDocumentacao() {
        $.ajax({
            url: "{{ route('tramitacao-imoveis-tabela-documentacao') }}",
            method: 'GET',
            success(response) {
                $(".conteudo_documentacao").html(response);
                recalcularDataTable("tabela_documentacao");
            }
        });
    }

    function requisitarDocumentoConformeTipoRequerente(documentacao) {

        let tipoRequerente = $("select[name=tipo_requerente]").val();

        if (tipoRequerente === "Responsável Legal" || tipoRequerente === "Procurador") {
            documentacao += ',Representação Legal';
            documentacao += ',Identidade do Representante Legal';
        }

        return documentacao;
    }

    adicionarOuRemoverDocumentosConformeTipoRequerente();

    function adicionarOuRemoverDocumentosConformeTipoRequerente() {

        $("select[name=tipo_requerente]").change(function() {

            let tipoRequerente = $(this).val();
            let categoriaProcesso = $("select[name=categoria_processo]").val();

            if (tipoRequerente !== "Responsável Legal") {
                
                let forgetSession = true;
                popularDadosDoProcessoEDocumentacao(categoriaProcesso, forgetSession);
            } else {

                let documentacao = popularDadosDoProcessoEDocumentacao(categoriaProcesso);
                documentacao = requisitarDocumentoConformeTipoRequerente(documentacao);
                popularListaDocumentacaoObrigatoria(documentacao);
                popularDescricaoDocumento(documentacao);
            }
        });
    }

    function popularListaDocumentacaoObrigatoria(documentacao) {

        let documentos = documentacao.split(",");
        let lista = document.querySelector(".lista_documentacao_obrigatoria");

        lista.innerHTML = "";

        documentos.forEach(documento => {
            let elementLi = document.createElement("li");
            elementLi.append(documento);
            lista.appendChild(elementLi);
        });
    }

    function popularDescricaoDocumento(documentacao) {

        let documentos = documentacao.split(",");
        let select = document.querySelector("select[name=descricao_documento]");

        select.innerHTML = "";
        select.append(document.createElement("option"));

        let ordem = 1;

        documentos.forEach(documento => {
            let elementOption = document.createElement("option");
            elementOption.append(`${ordem}. ${documento}`);
            select.appendChild(elementOption);
            ordem++;
        });
    }

    populandoDadosDoImovel();

    function populandoDadosDoImovel() {
        $("input[name=matricula_imovel]").change(function() {

            let matriculaImovel = $(this).val();

            $.ajax({
                url: "{{ route('tramitacao-imoveis-dados-imovel-json') }}",
                method: 'POST',
                data: {matriculaImovel},
                dataType: 'json',
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);

                    if (response.bSqlerro) {
                        $(".imovel_error_mensagem").removeClass('d-none');

                        $("input[name=matricula_imovel]").focus();
                        $("input[name=setor_imovel]").val("");
                        $("input[name=quadra_imovel]").val("");
                        $("input[name=lote_imovel]").val("");
                        return;
                    }

                    $(".imovel_error_mensagem").addClass('d-none');

                    $("input[name=setor_imovel]").val(response.oDadosLocalInserir.it22_setor);
                    $("input[name=quadra_imovel]").val(response.oDadosLocalInserir.it22_quadra);
                    $("input[name=lote_imovel]").val(response.oDadosLocalInserir.it22_lote);
                }
            });
        });
    }

    enviarDocumento();

    function enviarDocumento() {

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            let descricao = $("select[name=descricao_documento]").val();
            let arquivo = $("input[name=documento]")[0].files[0];

            if (!descricao) {
                alert("Atenção: Selecione a descrição do documento.");
                $("select[name=descricao_documento]").focus();
                return false;
            }

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'], 5048000)) {
                return false;
            }

            const formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("servidor", "ftp");
            formData.append("pastaServidor", "tramitacao");
            formData.append("nomeSessao", "documentacao");     

            $.ajax({
                url: "{{ route('salvar-documento-tramitacao-imoveis') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=documento]").val("");
                    $("select[name=descricao_documento]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $(".conteudo_documentacao").html(response);
                    recalcularDataTable("tabela_documentacao");
                }
            });
        });
    }

    removerDocumento();

    function removerDocumento() {

        $("body").on("click", ".btn-remover-documento", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let descricao = $(this).data("descricao");
            let caminho = $(this).data("caminho");

            $.ajax({
                url: "{{ route('remover-documento-tramitacao-imoveis') }}",
                method: 'POST',
                data: {
                    descricao,
                    caminho,
                    "servidor": "ftp",
                    "pastaServidor": "tramitacao",
                    "nomeSessao": "documentacao"
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $(".conteudo_documentacao").html(response);
                    recalcularDataTable("tabela_documentacao");
                }
            });
        });
    }

    enviarInformacoes();
    
    function enviarInformacoes() {

        $(".enviar_informacoes").on("click", function(e) {
            e.preventDefault();

            verificarDocumentosExigidos();
        });
    }

    function verificarDocumentosExigidos() {

        let categoriaProcesso = $("select[name=categoria_processo]").val();

        $.ajax({
            url: "{{ route('tramitacao-imoveis-dados-processo-json') }}",
            method: 'POST',
            data: {categoriaProcesso},
            dataType: 'json',
            success(response) {

                let documentacao = requisitarDocumentoConformeTipoRequerente(response.documentacao_processo).split(',');

                let ordem = 1;
                const documentosExigidos = [];

                documentacao.forEach((documento) => {
                    documentosExigidos.push(`${ordem}. ${documento}`);
                    ordem++;
                });

                $.ajax({
                    url: "{{ route('tramitacao-imoveis-verificar-documentos-exigidos') }}",
                    method: 'POST',
                    data: {documentosExigidos},
                    dataType: 'json',
                    success(response) {
                        
                        if (response.length > 0) {
                            alert(`Atenção: Os seguintes documentos são obrigatórios: ${response.join(', ')}.`);
                            return false;
                        }

                        $("#formTramitacao").submit();
                    }
                });
            }
        });
    }

</script>

@endsection
