<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/',                                     'Site\SiteController@index')->name('principal');

// Definição de rotas fake para o módulo financeiro

Route::get('geralFinanceira',                        'Site\SiteController@index')->name('geralFinanceira');
Route::get('debitosAnaliticos',                      'Site\SiteController@index')->name('debitosAnaliticos');
Route::get('pagamentosEfetuados',                    'Site\SiteController@index')->name('pagamentosEfetuados');
Route::get('selecaoDebito',                          'Site\SiteController@index')->name('selecaoDebito');
Route::get('cartaoGratuidadeEstacionamento',         'Site\SiteController@index')->name('cartaoGratuidadeEstacionamento');
Route::get('extratoFinanceiro',                      'Site\SiteController@index')->name('extratoFinanceiro');
// ************************************************

Route::get('/cartaServicos',                         'Site\SiteController@cartaServicos')->name('cartaServicos');
Route::get('/fomenta',                               'Site\SiteController@fomenta')->name('fomenta');
Route::get('/info',                                  'Site\SiteController@info')->name('info');
Route::post('/esqueceuSenha',                        'Site\SiteController@esqueceuSenha')->name('esqueceuSenha');
Route::post('/resetPassword',                        'Site\SiteController@resetPassword')->name('resetPassword');
Route::get('/primeiroAcesso',                       'Site\SiteController@primeiroAcesso')->name('primeiroAcesso');

Route::get('/iptu',                                 'Site\IptuController@emitirIptu')->name('iptu');
Route::post('/pesquisa_matricula_imovel',           'Site\IptuController@pesquisaMatriculaImovel')->name('pesquisa_matricula_imovel');
Route::get('/pesquisa_matricula_imovel',           'Site\IptuController@pesquisaMatriculaImovel2')->name('pesquisa_matricula_imovel');
Route::post('dia_util_iptu',                        'Site\IptuController@diaUtilEcidade')->name('dia_util_iptu');
Route::post('/integracaoReciboDebitosIptu',         'Site\IptuController@integracaoReciboDebitos')->name('integracaoReciboDebitosIptu');

Route::get('/iptu_idoso',                           'Site\IptuController@cadastroIptuIdoso')->name('iptu_idoso');
Route::get('/cadastro_iptu',                        'Site\IptuController@emitirIptu')->name('cadastro_iptu');
Route::get('/consulta_iptu',                        'Site\IptuController@consultaIptu')->name('consulta_iptu');
Route::get('/sanar_pendencias_iptu/{chamado}/{ciclo}/{etapa}', 'Site\IptuController@sanarPendenciasIptu')->name('sanar_pendencias_iptu');
Route::post('/consultar_iptu',                      'Site\IptuController@consultarIptu')->name('consultar_iptu');
Route::post('/valida_cpf_cnpj_iptu',                'Site\IptuController@validaCpfCnpjIptu')->name('valida_cpf_cnpj_iptu');
Route::post('/salvar_documento_servidor_iptu',      'Site\IptuController@salvarDocumentoServidor')->name('salvar_documento_servidor_iptu');
Route::post('/excluir_documento_servidor_iptu',     'Site\IptuController@removerDocumentoSessao')->name('excluir_documento_servidor_iptu');
Route::post('/salvar_iptu',                         'Site\IptuController@salvarIptu')->name('salvar_iptu');
Route::post('/formPendenciasIptuSalvar',            'Site\IptuController@formPendenciasIptuSalvar')->name('formPendenciasIptuSalvar');
Route::post('/verificar_documentos_pendencias_iptu', 'Site\IptuController@verificarDocumentosPendenciasIptu')->name('verificar_documentos_pendencias_iptu');
Route::post('/salvar_documento_pendencia_iptu',     'Site\IptuController@salvarDocumentoPendenciaIptu')->name('salvar_documento_pendencia_iptu');
Route::post('/excluir_documento_pendencia_iptu',     'Site\IptuController@removerDocumentoPendencia')->name('excluir_documento_pendencia_iptu');

Route::get('/register/consultaCEP/{id}',            'Auth\RegisterController@consultaCEP');
Route::get('/noticias',                             'Site\SiteController@noticias')->name('noticias');
Route::post('/enviarmensagem',                      'Site\SiteController@enviarmensagem')->name('enviarmensagem');
Route::post('/valida_cpf_login',                    'Site\SiteController@valida_cpf_login')->name('valida_cpf_login');
Route::get('/valida_cpf_cadastro/{id}',             'Site\SiteController@valida_cpf_cadastro')->name('valida_cpf_cadastro');
Route::get('/contato',                              'Site\SiteController@contato')->name('contato');
// Route::get('/faleconosco/assunto/{solicitacao}',    'Site\SiteController@pesquisa_solicitacao');
Route::get('/faleconosco',                          'Site\SiteController@faleconosco')->name('faleconosco');
Route::get('/servicos/{guia}/{aba?}',               'Site\SiteController@servicos')->name('servicos');
Route::get('/validaUsuario',                        'Site\SiteController@validaUsuario')->name('validaUsuario');
Route::get('/outrosservicos',                       'Site\SiteController@outrosservicos')->name('outrosservicos');
Route::get('/fileDownload/{id}',                    'Site\SiteController@fileDownload')->name('fileDownload');

Route::get('/certidaoAutenticacao',                 'Site\SiteController@certidaoAutenticacao')->name('certidaoAutenticacao');
Route::post('/validaautenticidade2',                'Site\SiteController@validaAutenticidade')->name('validaautenticidade2');
Route::get('/lecomAtualizaCgm/{chamado?}/{status?}',    'Site\SiteController@lecomAtualizaCgm')->name('lecomAtualizaCgm');

Route::get('/PAE_PAGINA',                           'Site\SocialController@pae_pagina')->name('PAE_PAGINA');
Route::get('/PAE/{guia?}/{aba?}',                   'Site\SocialController@pae')->name('PAE');
Route::get('/PAE_CNPJ',                             'Site\SocialController@pae_cnpj')->name('PAE_CNPJ');
Route::post('/validacnpjpae',                       'Site\SocialController@validacnpjpae')->name('validacnpjpae');
Route::get('/consultarsituacaopae',                 'Site\SocialController@consultarsituacaopae')->name('consultarsituacaopae');
Route::get('/pae_documentos_remove/{arquivo}',      'Site\SocialController@pae_documentos_remove')->name('pae_documentos_remove');
Route::post('/pae_documentos',                      'Site\SocialController@pae_documentos')->name('pae_documentos');
Route::post('/pae_cadastro',                        'Site\SocialController@pae_cadastro')->name('pae_cadastro');
Route::post('/imprimirprotocolopae',                'Site\SocialController@imprimirprotocolopae')->name('imprimirprotocolopae');
Route::post('/validaconsultaauxiliopae',            'Site\SocialController@validaconsultaauxiliopae')->name('validaconsultaauxiliopae');
Route::post('/pae_recurso',                         'Site\SocialController@pae_recurso')->name('pae_recurso');
Route::post('/pae_recurso_pc',                      'Site\SocialController@pae_recurso_pc')->name('pae_recurso_pc');
Route::post('/pae_documentos_recurso_pc',           'Site\SocialController@pae_documentos_recurso_pc')->name('pae_documentos_recurso_pc');
Route::get('/pae_documentos_PC_remove/{arquivo}',      'Site\SocialController@pae_documentos_PC_remove')->name('pae_documentos_PC_remove');

Route::get('/PAT/{guia?}/{aba?}',                   'Site\SocialController@servicossociais')->name('PAT');
Route::get('/PAT/{guia?}/{aba?}',                   'Site\SocialController@servicossociais')->name('PAT');
Route::get('/PAT_CADASTRO',                         'Site\SocialController@pat')->name('PAT_CADASTRO');
Route::get('/consultarsituacao',                    'Site\SocialController@consultarsituacao')->name('consultarsituacao');
Route::get('/PAT_CADASTRO',                         'Site\SocialController@pat')->name('PAT_CADASTRO');
Route::get('/solicitarauxilio',                     'Site\SocialController@solicitarauxilio')->name('solicitarauxilio');
Route::get('/pat_cadastro/{cpf}',                   'Site\SocialController@pat_cadastro')->name('pat_cadastro');
Route::get('/validaCpfDep/{cpf}',                   'Site\SocialController@validaCpfDep')->name('validaCpfDep');
Route::post('/pat_documentos',                      'Site\SocialController@pat_documentos')->name('pat_documentos');
Route::post('/pat_validarCpf',                      'Site\SocialController@pat_validarCpf')->name('pat_validarCpf');
Route::post('/pat_informacoes',                     'Site\SocialController@pat_informacoes')->name('pat_informacoes');
Route::post('/pat_recurso',                         'Site\SocialController@pat_recurso')->name('pat_recurso');
Route::post('/validaconsultaauxilio',               'Site\SocialController@pat3')->name('validaconsultaauxilio');
Route::post('/imprimirprotocolo',                   'Site\SocialController@imprimirprotocolo')->name('imprimirprotocolo');
Route::get('/pat_documentos_remove/{arquivo}',      'Site\SocialController@pat_documentos_remove')->name('pat_documentos_remove');
Route::get('/patConsultaCid10',                     'Site\SocialController@patConsultaCid10')->name('patConsultaCid10');

Route::get('/certidaoNegativaPositiva',             'Site\SocialController@certidaoNegativaPositiva')->name('certidaoNegativaPositiva');
Route::post('/certnegativa_validarCpf',             'Site\SocialController@certnegativa_validarCpf')->name('certnegativa_validarCpf');

Route::get('/fomentaconsultarsituacao',             'Site\FomentaController@fomentaconsultarsituacao')->name('fomentaconsultarsituacao');
Route::post('/fomentavalidasituacao',               'Site\FomentaController@fomentavalidasituacao')->name('fomentavalidasituacao');
Route::post('/fomenta_recurso',                     'Site\FomentaController@fomentaRecurso')->name('fomenta_recurso');
Route::post('/fomenta_consulta_datas_disponiveis',  'Site\FomentaController@fomentaConsultaDatasDisponiveis')->name('fomenta_consulta_datas_disponiveis');
Route::post('/fomenta_consulta_horarios_disponiveis',  'Site\FomentaController@fomentaConsultaHorariosDisponiveis')->name('fomenta_consulta_horarios_disponiveis');
Route::post('/fomenta_agendamento',                 'Site\FomentaController@fomentaAgendamento')->name('fomenta_agendamento');
Route::get('/fomentaCadastrarMEI',                  'Site\FomentaController@fomentaCadastrarMEI')->name('fomentaCadastrarMEI');
Route::post('/fomentaCadastroSalvar',               'Site\FomentaController@fomentaCadastroSalvar')->name('fomentaCadastroSalvar');
Route::post('/fomentaValidaCpfCnpj',                ['uses' => 'Site\FomentaController@fomentaValidaCpfCnpj'])->name('fomentaValidaCpfCnpj');
Route::post('/salvar_documento_servidor_fomenta',   'Site\FomentaController@salvarDocumentoServidorFomenta')->name('salvar_documento_servidor_fomenta');
Route::post('/excluir_documento_servidor_fomenta',  'Site\FomentaController@removerDocumentoSessaoFomenta')->name('excluir_documento_servidor_fomenta');
Route::get('/cadastrofomenta',                      'Site\FomentaController@cadastrofomenta')->name('cadastrofomenta');
Route::post('/verificar_envio_documentos_fomenta',  'Site\FomentaController@verificarEnvioDocumentos')->name('verificar_envio_documentos_fomenta');
Route::post('/verificar_envio_documentos_fomenta_recurso',  'Site\FomentaController@verificarEnvioDocumentosRecurso')->name('verificar_envio_documentos_fomenta_recurso');
Route::post('/imprimirprotocolofomenta',            'Site\FomentaController@imprimirprotocolofomenta')->name('imprimirprotocolofomenta');


Auth::routes(['verify' => true]);

Route::post('/login',                                 'Site\SiteController@login')->name('login');

Route::group([
    'namespace' => 'Site',
    'middleware' => 'verified'
], function () {
    Route::get('/home',                     'HomeController@index')->name('home');

    // Route::get('/servicos/cidadao',                         ['uses' => 'HomeController@servicos'])->name('servicos');
    Route::get('/detectaEtapa/{chamado}',                   ['uses' => 'HomeController@detectaEtapa'])->name('detectaEtapa');
    Route::get('/formulario/{guia}/{assunto}/{servico}',    ['uses' => 'HomeController@formulario'])->name('formulario');
    Route::get('/formularioEdita/{chamado}',                ['uses' => 'HomeController@formularioEdita'])->name('formularioEdita');
    Route::get('/formularioEditaCGM',                       ['uses' => 'HomeController@formularioEditaCGM'])->name('formularioEditaCGM');
    Route::get('/servicos_documentos',                      ['uses' => 'HomeController@servicos_documentos'])->name('servicos_documentos');
    Route::get('/servicos_tributos',                        ['uses' => 'HomeController@servicos_tributos'])->name('servicos_tributos');

    Route::get('/alvara_consulta',                          ['uses' => 'HomeController@alvara_consulta'])->name('alvara_consulta');
    Route::get('/alvara_certidao',                          ['uses' => 'HomeController@alvara_certidao'])->name('alvara_certidao');
    Route::get('/certidao/{id}',                            ['uses' => 'HomeController@certidao'])->name('certidao');
    Route::get('/iptu_abatimento',                          ['uses' => 'HomeController@iptu_abatimento'])->name('iptu_abatimento');
    Route::get('/iptu_cadastramento',                       ['uses' => 'HomeController@iptu_cadastramento'])->name('iptu_cadastramento');
    Route::get('/iss_guia',                                 ['uses' => 'HomeController@iss_guia'])->name('iss_guia');
    Route::get('/iss_pagamento',                            ['uses' => 'HomeController@iss_pagamento'])->name('iss_pagamento');
    Route::get('/acompanhamento',                           ['uses' => 'HomeController@consultaServicos'])->name('acompanhamento');
    Route::get('consulta_processo/{view}',                       ['uses' => 'HomeController@consultaProcesso'])->name('consulta_processo');
    Route::post('/acompanhamento',                          ['uses' => 'HomeController@acompanhamento'])->name('acompanhamento');
    Route::get('/termo_aceite',                             ['uses' => 'HomeController@converterArquivo'])->name('termo_aceite');
    Route::post('/enviar_doc_divida',                       ['uses' => 'HomeController@salvarDocDivida'])->name('enviar_doc_divida');
    Route::post('/salvar_exigencia',                        ['uses' => 'HomeController@salvarExigencia'])->name('salvar_exigencia');
    Route::get('/formularioDivida/{chamado}/{ciclo}',       ['uses' => 'HomeController@enviarDoc'])->name('formularioDivida');
    Route::get('/formulario-nf-certidao/{chamado}/{ciclo}/{tipo}', ['uses' => 'HomeController@enviarDocNfCertidao'])->name('formulario-nf-certidao');
    Route::get('/formularioBPM',                            ['uses' => 'HomeController@formularioBPM'])->name('formularioBPM');
    Route::post('remove_doc_sessao/{documento}',            ['uses' => 'HomeController@removeDocSessao'])->name('remove_doc_sessao');
    Route::get('/cadAvisos',                                ['uses' => 'HomeController@cadAvisos'])->name('cadAvisos');
    Route::get('/avisoEdit/{id}',                           ['uses' => 'HomeController@avisoEdit'])->name('avisoEdit');
    Route::get('/avisoNovo',                                ['uses' => 'HomeController@avisoNovo'])->name('avisoNovo');
    Route::post('/avisoSave',                               ['uses' => 'HomeController@avisoSave'])->name('avisoSave');

    Route::get('/alterarSenha',                             ['uses' => 'HomeController@alterarSenha'])->name('alterarSenha');
    Route::post('/alterarSenha',                            ['uses' => 'HomeController@alterarSenhaSave'])->name('alterarSenha');
    Route::get('/itbionline',                               ['uses' => 'HomeController@itbionline'])->name('itbionline');
    Route::get('/sigelu',                                   ['uses' => 'SigeluController@abrirServico'])->name('sigelu');
});

Route::group(['namespace' => 'Empresa',], function () {

    // *** Legalização MEI
    Route::get('/legalizacaoMEI',                           ['uses' => 'EmpresaController@legalizacaoMEI'])->name('legalizacaoMEI');
    Route::get('/verificar_atividade_cnae',                 ['uses' => 'EmpresaController@verificar_atividade_cnae'])->name('verificar_atividade_cnae');
    Route::get('/verificar_socios_cadastrados',             ['uses' => 'EmpresaController@verificar_socios_cadastrados'])->name('verificar_socios_cadastrados');
    Route::post('/limpar_arquivos_sessao',                  ['uses' => 'EmpresaController@limpar_arquivos_sessao'])->name('limpar_arquivos_sessao');
    Route::post('/formLegalizacaoMeiSalvar',                ['uses' => 'EmpresaController@formLegalizacaoMeiSalvar'])->name('formLegalizacaoMeiSalvar');
    Route::post('/salvar_documento_pendencia',              ['uses' => 'EmpresaController@salvarDocumentoPendencia'])->name('salvar_documento_pendencia');
    Route::post('/salvar_documento_servidor_legalizaMEI',   ['uses' => 'EmpresaController@salvarDocumentoServidor'])->name('salvar_documento_servidor_legalizaMEI');
    Route::post('/excluir_documento_pendencia',             ['uses' => 'EmpresaController@removerDocumentoPendencia'])->name('excluir_documento_pendencia');
    Route::post('/excluir_documento_servidor_legalizaMEI',  ['uses' => 'EmpresaController@removerDocumentoSessao'])->name('excluir_documento_servidor_legalizaMEI');
    Route::post('/validaCnpjECidade',                       ['uses' => 'EmpresaController@validaCnpjECidade'])->name('validaCnpjECidade');
    Route::post('/incluirCNAE',                             ['uses' => 'EmpresaController@incluirCNAE'])->name('incluirCNAE');
    Route::post('/validaCpfECidade',                        ['uses' => 'EmpresaController@validaCpfECidade'])->name('validaCpfECidade');
    Route::post('/salvar_dados_socio',                      ['uses' => 'EmpresaController@salvar_dados_socio'])->name('salvar_dados_socio');
    Route::post('/excluir_documento_servidor_SocioMEI',     ['uses' => 'EmpresaController@removerDocumentoSocio'])->name('excluir_documento_servidor_SocioMEI');
    Route::post('/salvar_dados_cnae',                       ['uses' => 'EmpresaController@salvar_dados_cnae'])->name('salvar_dados_cnae');
    Route::post('/excluir_dados_cnae',                      ['uses' => 'EmpresaController@excluir_dados_cnae'])->name('excluir_dados_cnae');
    Route::post('/verificar_envio_documentos_mei',          ['uses' => 'EmpresaController@verificarEnvioDocumentos'])->name('verificar_envio_documentos_mei');
    Route::post('/verificar_documentos_pendencias',         ['uses' => 'EmpresaController@verificarDocumentosPendencias'])->name('verificar_documentos_pendencias');
    Route::post('/formPendenciasMeiSalvar',                 ['uses' => 'EmpresaController@formPendenciasMeiSalvar'])->name('formPendenciasMeiSalvar');
    Route::get('/exigencias_mei/{chamado}',                 ['uses' => 'EmpresaController@exigenciasMei'])->name('exigencias_mei');
    Route::get('/sanar_pendencias_mei/{chamado}/{ciclo}/{etapa}', ['uses' => 'EmpresaController@sanarPendenciasMei'])->name('sanar_pendencias_mei');
});


Route::group(['namespace' => 'Cadastro',], function () {

    // *** Novo Cadastro do CGM
    Route::get('/atualizaCGM',                          ['uses' => 'CadastroController@atualizaCGM'])->name('atualizaCGM');
    Route::get('/consultarsituacaomrc',                 ['uses' => 'CadastroController@consultarSituacaoMrc'])->name('consultarsituacaomrc');
    Route::post('/consultarsituacaomrc',                ['uses' => 'CadastroController@consultarSituacaoMrc'])->name('consultarsituacaomrc');
    Route::post('/enviar_recurso',                      ['uses' => 'CadastroController@enviarRecurso'])->name('enviar_recurso');
    Route::get('/atualizaCGMIPTU',                      ['uses' => 'CadastroController@atualizaCGMIPTU'])->name('atualizaCGMIPTU');
    Route::get('/cgm_validarCpf',                       ['uses' => 'CadastroController@cgm_direcionarForm'])->name('cgm_validarCpf');
    Route::post('/cgm_validarCpf',                      ['uses' => 'CadastroController@cgm_validarCpf'])->name('cgm_validarCpf');
    Route::get('/alteraIPTU',                           ['uses' => 'CadastroController@alteraIPTU'])->name('alteraIPTU');
    Route::post('/salvar_cgm',                          ['uses' => 'CadastroController@salvarCgm'])->name('salvar_cgm');
    Route::post('/salvar_documento_servidor',           ['uses' => 'CadastroController@salvarDocumentoServidor'])->name('salvar_documento_servidor');
    Route::post('/excluir_documento_servidor',          ['uses' => 'CadastroController@removerDocumentoSessao'])->name('excluir_documento_servidor');
    Route::post('/verificar_envio_documentos',          ['uses' => 'CadastroController@verificarEnvioDocumentos'])->name('verificar_envio_documentos');
    Route::post('/imprimir_protocolo_mrc',              ['uses' => 'CadastroController@imprimirProtocoloMrc'])->name('imprimir_protocolo_mrc');
    Route::get('/cadastro_cgm/{cpfOuCnpj}',             ['uses' => 'CadastroController@cadastroCgm'])->name('cadastro_cgm');
});

Route::group([
    'namespace' => 'Cidadao',
    'middleware' => 'verified'
], function () {

    // Route::get('/fracionamento',                    ['uses' => 'CadastroController@fracionamento'])->name('fracionamento');
    Route::post('/verifica-documentacao',           ['uses' => 'CertidaoController@verificaDocumentacao'])->name('verifica-documentacao');
    Route::get('/certidoes-cancelamento-notas/{id?}', ['uses' => 'CertidaoController@solicitaCertidaoNota'])->name('certidoes-cancelamento-notas');
    Route::post('/salvar-certidoes-notas',           ['uses' => 'CertidaoController@salvarCertidaoNota'])->name('salvar-certidoes-notas');
    Route::post('/salvar-doc-cn',                   ['uses' => 'CertidaoController@salvarDocSessao'])->name('salvar-doc-cn');
    Route::post('/salvar-exigencia-nf-certidao',    ['uses' => 'CertidaoController@salvarExigenciaNfCertidao'])->name('salvar-exigencia-nf-certidao');
    Route::post('remove_doc_cn',                   ['uses' => 'CertidaoController@removeDocSessao'])->name('remove_doc_cn');

    Route::get('/certidaoAmbiental',                ['uses' => 'CertidaoController@certidaoAmbiental'])->name('certidaoAmbiental');
    Route::get('/autorizacaoAmbiental',             ['uses' => 'CertidaoController@autorizacaoAmbiental'])->name('autorizacaoAmbiental');
    Route::get('/certidaoZoneamento',               ['uses' => 'CertidaoController@certidaoZoneamento'])->name('certidaoZoneamento');
    Route::get('/certidaoLancamento',               ['uses' => 'CertidaoController@certidaoLancamento'])->name('certidaoLancamento');
    Route::get('/certidaoDebitosTributos',          ['uses' => 'CertidaoController@certidaoDebitosTributos'])->name('certidaoDebitosTributos');
    Route::get('/certidaoValorVenal',               ['uses' => 'CertidaoController@certidaoValorVenal'])->name('certidaoValorVenal');

    Route::get('/certidaoNegativa',                 ['uses' => 'CertidaoController@certidaoNegativa'])->name('certidaoNegativa');
    Route::get('/certidaoNumeroPorta',              ['uses' => 'CertidaoController@certidaoNumeroPorta'])->name('certidaoNumeroPorta');
    Route::get('/certidaoITBI',                     ['uses' => 'CertidaoController@certidaoITBI'])->name('certidaoITBI');

    Route::get('/certidaoQuitacaoItbi/{matricula}',            ['uses' => 'CertidaoController@certidaoQuitacaoItbi'])->name('certidaoQuitacaoItbi');

    Route::get('/certidaoNegativaImprimir/{id}',    ['uses' => 'CertidaoController@certidaoNegativaImprimir'])->name('certidaoNegativaImprimir');
    Route::get('/certidaoValorVenalImprimir/{id}',  ['uses' => 'CertidaoController@certidaoValorVenalImprimir'])->name('certidaoValorVenalImprimir');
    Route::get('/certidaoITBIImprimir/{matr}/{guia}',  ['uses' => 'CertidaoController@certidaoITBIImprimir'])->name('certidaoITBIImprimir');
    Route::get('/certidaoNumeroPortaImprimir/{id}', ['uses' => 'CertidaoController@certidaoNumeroPortaImprimir'])->name('certidaoNumeroPortaImprimir');

    Route::get('/cadastroCGM',                      ['uses' => 'CgmController@cadastroCGM'])->name('cadastroCGM');
    Route::post('cgm_informacoes_adicionais',       ['uses' => 'CgmController@cgm_informacoes_adicionais'])->name('cgm_informacoes_adicionais');
    Route::post('cgm_informacoes_contato',          ['uses' => 'CgmController@cgm_informacoes_contato'])->name('cgm_informacoes_contato');
    Route::post('cgm_informacoes_pessoais',         ['uses' => 'CgmController@cgm_informacoes_pessoais'])->name('cgm_informacoes_pessoais');
    Route::post('cgm_endereco_correspondencia',     ['uses' => 'CgmController@cgm_endereco_correspondencia'])->name('cgm_endereco_correspondencia');
    Route::post('cgm_endereco',                     ['uses' => 'CgmController@cgm_endereco'])->name('cgm_endereco');
    Route::post('cgm_documentos',                   ['uses' => 'CgmController@cgm_documentos'])->name('cgm_documentos');
    Route::get('cgm_documentos_excluir/{id}',       ['uses' => 'CgmController@cgm_documentos_excluir'])->name('cgm_documentos_excluir');
    Route::get('cgm_valida_email/{token}',          ['uses' => 'CgmController@cgm_valida_email'])->name('cgm_valida_email');
    Route::get('consultaCEPcgm/{id}',               ['uses' => 'CgmController@consultaCEPcgm'])->name('consultaCEPcgm');
    Route::get('recuperaRuasCGM/{id}',              ['uses' => 'CgmController@recuperaRuasCGM'])->name('recuperaRuasCGM');
    Route::get('cancelaEnvioEmail',                 ['uses' => 'CgmController@cancelaEnvioEmail'])->name('cancelaEnvioEmail');
    Route::get('recuperaMunicipiosCGM/{estado}',    ['uses' => 'CgmController@recuperaMunicipiosCGM'])->name('recuperaMunicipiosCGM');
    Route::get('/licencaMaternidade',               ['uses' => 'ServidorController@solicitarLicencaMaternidade'])->name('licencaMaternidade');
    Route::get('/licencaPremio',                    ['uses' => 'ServidorController@solicitarLicencaPremio'])->name('licencaPremio');
    Route::get('/auxilioTransporte',                    ['uses' => 'ServidorController@solicitarAuxilioTransporte'])->name('auxilioTransporte');

    Route::get('/lotacaoServidor',                    ['uses' => 'ServidorController@alterarLotacaoServidor'])->name('lotacaoServidor');

    Route::post('/buscar/{url_servico}',                           ['uses' => 'ImovelController@consultaInformacoes'])->name('buscar');
    Route::get('/buscar/{url_servico}',                           ['uses' => 'ImovelController@consultaInformacoes'])->name('buscar');
    Route::get('/consultaInformacoes/{url_servico}',              ['uses' => 'ImovelController@consultaInformacoes'])->name('consultaInformacoes');
    Route::get('/imovelInformacao/{id}',            ['uses' => 'ImovelController@imovelInformacao'])->name('imovelInformacao');
    Route::get('/imprimeInformacoes/{id}',          ['uses' => 'ImovelController@imprimeInformacoes'])->name('imprimeInformacoes');
    Route::get('/averbacaoImovel/{processo?}',                  ['uses' => 'ImovelController@averbacaoImovel'])->name('averbacaoImovel');
    Route::get('/imoveis/{cpf?}/{matr?}/{page?}',            ['uses' => 'ImovelController@listaImoveis'])->name('imoveis');
    Route::get('/listaImoveis/{cpf}/{matr?}',       ['uses' => 'ImovelController@listaImoveisAverbacao'])->name('listaImoveis');
    Route::get('/listaItbiImovel/{matr}',           ['uses' => 'ImovelController@listaItbiImovel'])->name('listaItbiImovel');
    Route::get('/listaAdquirentes/{matr}/{guia}',   ['uses' => 'ImovelController@listaAdquirentes'])->name('listaAdquirentes');

    Route::get('recupera_cgm/{cpf}', ['uses' => 'ImovelController@recuperaCGM'])->name('recupera_cgm');

    Route::get('recupera_dados_cgm/{document}', ['uses' => 'ImovelController@getDadosCGM'])->name('recupera_dados_cgm');
    Route::post('salvar_adquirente_sessao', ['uses' => 'ImovelController@salvarAdquirenteSessao'])->name('salvar_adquirente_sessao');
    Route::post('remover_adquirente_sessao/{document}',     ['uses' => 'ImovelController@removerAdquirenteSessao'])->name('remover_adquirente_sessao');
    Route::post('salvar_documento_averbacao', ['uses' => 'ImovelController@salvarDocumentoAverbacao'])->name('salvar_documento_averbacao');
    Route::post('remover_documento_adquirente_sessao/{documento}', ['uses' => 'ImovelController@removerDocumentoAdquirenteSessao'])->name('remover_documento_adquirente_sessao');
    Route::post('gravar_averbacao', ['uses' => 'ImovelController@gravarAverbacao'])->name('gravar_averbacao');


    Route::get('recupera_endereco_cgm/{cpf}', ['uses' => 'ImovelController@recuperaEnderecoCGM'])->name('recupera_endereco_cgm');
    Route::get('recupera_cgm_endereco/{cpf}', ['uses' => 'ImovelController@recuperaCgmEndereco'])->name('recupera_cgm_endereco');
    Route::get('lancamentoITBI', ['uses' => 'ImovelController@lancamentoITBI'])->name('lancamentoITBI');
    Route::get('recupera_adquirentes_imovel/{guia}', ['uses' => 'ImovelController@recupera_adquirentes_imovel'])->name('recupera_adquirentes_imovel');
    Route::get('verifica_status_lecom/{matricula}', ['uses' => 'ImovelController@verifica_status_lecom'])->name('verifica_status_lecom');
    Route::post('pendencias_gravar', ['uses' => 'ImovelController@pendencias_gravar'])->name('pendencias_gravar');
    Route::get('verifica_debitos/{matricula}', ['uses' => 'ImovelController@verifica_debitos'])->name('verifica_debitos');


    // TRAMITAÇÃO DE IMÓVEIS
    Route::get('tramitacao-processos-legalizacao-imoveis',          ['uses' => 'TramitacaoImoveisController@index'])->name('tramitacao-processos-legalizacao-imoveis');
    Route::get('tramitacao-imoveis-tabela-documentacao',            ['uses' => 'TramitacaoImoveisController@renderTabelaDocumentacao'])->name('tramitacao-imoveis-tabela-documentacao');
    Route::post('tramitacao-imoveis-dados-imovel-json',             ['uses' => 'TramitacaoImoveisController@dadosImovelItbiJson'])->name('tramitacao-imoveis-dados-imovel-json');
    Route::post('tramitacao-imoveis-dados-processo-json',           ['uses' => 'TramitacaoImoveisController@dadosProcessoJson'])->name('tramitacao-imoveis-dados-processo-json');
    Route::post('salvar-documento-tramitacao-imoveis',              ['uses' => 'TramitacaoImoveisController@enviarDocumentoParaServidorEsalvarNaSessao'])->name('salvar-documento-tramitacao-imoveis');
    Route::post('remover-documento-tramitacao-imoveis',             ['uses' => 'TramitacaoImoveisController@removerDocumentoDoServidorEsessao'])->name('remover-documento-tramitacao-imoveis');
    Route::post('tramitacao-imoveis-verificar-documentos-exigidos', ['uses' => 'TramitacaoImoveisController@verificarDocumentosExigidos'])->name('tramitacao-imoveis-verificar-documentos-exigidos');
    Route::post('salvar-tramitacao-processos-legalizacao-imoveis',  ['uses' => 'TramitacaoImoveisController@salvar'])->name('salvar-tramitacao-processos-legalizacao-imoveis');
    Route::get('tramitacao-imoveis-exigencias/{id_chamado}',        ['uses' => 'TramitacaoImoveisController@exigencias'])->name('tramitacao-imoveis-exigencias');
    Route::post('tramitacao-imoveis-enviar-exigencia',              ['uses' => 'TramitacaoImoveisController@enviarExigencia'])->name('tramitacao-imoveis-enviar-exigencia');
    Route::get('download-arquivo-prefeitura/{arquivo}',             ['uses' => 'TramitacaoImoveisController@downloadArquivoPrefeitura'])->name('download-arquivo-prefeitura');




    Route::get('/boletosTaxas',                                 ['uses' => 'TaxasController@boletosTaxas'])->name('boletosTaxas');
    Route::get('/taxasconsultacgm/{cpf}',                       ['uses' => 'TaxasController@taxasconsultacgm'])->name('taxasconsultacgm');
    Route::get('/taxasconsultainscricao/{cpf}',                 ['uses' => 'TaxasController@taxasconsultainscricao'])->name('taxasconsultainscricao');
    Route::get('/taxasconsultamatricula/{cpf}/{matr?}',         ['uses' => 'TaxasController@taxasconsultamatricula'])->name('taxasconsultamatricula');
    Route::get('/taxasconsultavalor/{tipo}/{valor}/{grupo}',    ['uses' => 'TaxasController@taxasconsultavalor'])->name('taxasconsultavalor');
    Route::post('/emitirboletotaxa',                            ['uses' => 'TaxasController@emitirboletotaxa'])->name('emitirboletotaxa');

    Route::get('/lancamentoITBI',                               ['uses' => 'ItbiController@lancamentoITBI'])->name('lancamentoITBI');
    Route::get('/editarAverbacao/{id}',                         ['uses' => 'ImovelController@editarAverbacao'])->name('editarAverbacao');
    Route::get('/editarItbi/{id}',                              ['uses' => 'ItbiController@editarItbi'])->name('editarItbi');
    Route::get('/baixarguiaitbi/{id}',                          ['uses' => 'ItbiController@baixarguiaitbi'])->name('baixarguiaitbi');
    Route::get('/itbitransacao/{id}',                           ['uses' => 'ItbiController@itbitransacao'])->name('itbitransacao');
    Route::get('/itbiconsultaCGM/{cpf}',                        ['uses' => 'ItbiController@itbiconsultaCGM'])->name('itbiconsultaCGM');
    Route::get('itbi_documentos_remove/{arquivo}',              ['uses' => 'ItbiController@itbi_documentos_remove'])->name('itbi_documentos_remove');
    Route::post('itbi_documentos_transacao',                    ['uses' => 'ItbiController@itbi_documentos_transacao'])->name('itbi_documentos_transacao');
    Route::post('itbi_documentos_transmitente',                 ['uses' => 'ItbiController@itbi_documentos_transmitente'])->name('itbi_documentos_transmitente');
    Route::post('itbi_documentos_adquirente',                   ['uses' => 'ItbiController@itbi_documentos_adquirente'])->name('itbi_documentos_adquirente');
    Route::post('itbi_documentos_imovel',                       ['uses' => 'ItbiController@itbi_documentos_imovel'])->name('itbi_documentos_imovel');
    Route::post('itbi_lancar',                                  ['uses' => 'ItbiController@itbi_lancar'])->name('itbi_lancar');

    // Defesa Prévia de Autuação (Transito)
    Route::get('/defesa-previa-autuacao', ['uses' => 'DefesaPreviaAutuacaoController@index'])->name('defesa-previa-autuacao');
    Route::post('/defesa-previa-autuacao', ['uses' => 'DefesaPreviaAutuacaoController@salvar'])->name('defesa-previa-autuacao');
    Route::post('/salvar-documento-defesa-previa-autuacao', ['uses' => 'DefesaPreviaAutuacaoController@enviarDocumento'])->name('salvar-documento-defesa-previa-autuacao');
    Route::post('/remover-documento-defesa-previa-autuacao', ['uses' => 'DefesaPreviaAutuacaoController@removerDocumento'])->name('remover-documento-defesa-previa-autuacao');
    Route::post('/verificar-documentos-defesa-previa-autuacao', ['uses' => 'DefesaPreviaAutuacaoController@verificarDocumentosExigidos'])->name('verificar-documentos-defesa-previa-autuacao');
});

Route::group(['namespace' => 'Financeiro'], function () {
    Route::get('/prestacaoContasPAE', ['uses' => 'FinanceiroSocialController@prestacaoContasPAE'])->name('prestacaoContasPAE');
    Route::post('gravar_prestacao_contas', ['uses' => 'FinanceiroSocialController@gravarPrestacaoContas'])->name('gravar_prestacao_contas');
    Route::post('salvar_documento_funcionario_pae', ['uses' => 'FinanceiroSocialController@salvarDocumentoFuncionarioPae'])->name('salvar_documento_funcionario_pae');
    Route::get('remover_documento_funcionario_sessao/{documento}', ['uses' => 'FinanceiroSocialController@removerDocumentoFuncionarioSessao'])->name('remover_documento_funcionario_sessao');
    Route::post('salvar_termo_adesao', ['uses' => 'FinanceiroSocialController@salvarTermoAdesao'])->name('salvar_termo_adesao');
    Route::get('remover_termo_adesao_sessao/{documento}', ['uses' => 'FinanceiroSocialController@removerTermoAdesaoSessao'])->name('remover_termo_adesao_sessao');

    Route::get('/selecaoDebito', ['uses' => 'FinanceiroController@selecaoDebito'])->name('selecaoDebito');
    Route::get('/debitosAbertos', ['uses' => 'FinanceiroController@debitosAbertos'])->name('debitosAbertos');
    Route::post('/relatorioDebitosAbertos', ['uses' => 'FinanceiroController@relatorioDebitosAbertos'])->name('relatorioDebitosAbertos');
    Route::get('/pagamentosEfetuados', ['uses' => 'FinanceiroController@pagamentosEfetuados'])->name('pagamentosEfetuados');
    Route::get('/extratoFinanceiro', ['uses' => 'FinanceiroController@extratoFinanceiro'])->name('extratoFinanceiro');
    Route::post('/filtroExtratoFinanceiro', ['uses' => 'FinanceiroController@filtroExtratoFinanceiro'])->name('filtroExtratoFinanceiro');

    Route::post('/integracaoPesquisaTipoDebitos', ['uses' => 'FinanceiroController@integracaoPesquisaTipoDebitos'])->name('integracaoPesquisaTipoDebitos');
    Route::post('/integracaoPesquisaDebitos', ['uses' => 'FinanceiroController@integracaoPesquisaDebitos'])->name('integracaoPesquisaDebitos');
    Route::post('/integracaoReciboDebitos', ['uses' => 'FinanceiroController@integracaoReciboDebitos'])->name('integracaoReciboDebitos');
    Route::post('/integracaoRelatorioTotalDebitos', ['uses' => 'FinanceiroController@integracaoRelatorioTotalDebitos'])->name('integracaoRelatorioTotalDebitos');

    Route::post('/integracaoPagamentosEfetuados', ['uses' => 'FinanceiroController@integracaoPagamentosEfetuados'])->name('integracaoPagamentosEfetuados');
    Route::post('/integracaoRelatorioPagamentosEfetuados', ['uses' => 'FinanceiroController@integracaoRelatorioPagamentosEfetuados'])->name('integracaoRelatorioPagamentosEfetuados');
    Route::get('/pesquisarMatricula', ['uses' => 'FinanceiroController@pesquisarMatricula'])->name('pesquisarMatricula');
    Route::get('/pesquisarInscricao', ['uses' => 'FinanceiroController@pesquisarInscricao'])->name('pesquisarInscricao');

    Route::get('/parcelar_divida', ['uses' => 'FinanceiroController@parcelarDivida'])->name('parcelar_divida');
    Route::post('/salvarParcelarDivida', ['uses' => 'FinanceiroController@salvarParcelarDivida'])->name('salvarParcelarDivida');
    Route::post('salvar_doc_sessao', ['uses' => 'FinanceiroController@salvarDocSessao'])->name('salvar_doc_sessao');
    Route::post('remover_doc_sessao/{documento}', ['uses' => 'FinanceiroController@removeDocSessao'])->name('remover_doc_sessao');
    Route::post('/anosPagamento', ['uses' => 'FinanceiroController@anosPagamento'])->name('anosPagamento');
    Route::post('dia_util_ecidade', ['uses' => 'FinanceiroController@diaUtilEcidade'])->name('dia_util_ecidade');
});

// Baixa Inscrição Municipal (Empreendedorismo)
Route::group([
    'namespace' => 'Empreendedorismo',
    'middleware' => 'verified'
], function () {
    Route::get('/baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@index'])->name('baixa-inscricao-municipal');
    Route::post('/dados-contribuinte', ['uses' => 'BaixaInscricaoMunicipalController@getDadosContribuinte'])->name('dados-contribuinte');
    Route::post('/salvar-documento-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@enviarDocumento'])->name('salvar-documento-baixa-inscricao-municipal');
    Route::post('/remover-documento-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@removerDocumento'])->name('remover-documento-baixa-inscricao-municipal');
    Route::post('/verificar-documentos-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@verificarDocumentosExigidos'])->name('verificar-documentos-baixa-inscricao-municipal');
    Route::post('/salvar-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@salvar'])->name('salvar-baixa-inscricao-municipal');
    Route::post('/remover-documentos-sessao-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@removerDocumentosDaSessao'])->name('remover-documentos-sessao-baixa-inscricao-municipal');

    // Exigências
    Route::get('baixa-inscricao-municipal-exigencia/{id_chamado}', ['uses' => 'BaixaInscricaoMunicipalController@exigencia'])->name('baixa-inscricao-municipal-exigencia');
    Route::post('salvar-documento-exigencia-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@enviarDocumento'])->name('salvar-documento-exigencia-baixa-inscricao-municipal');
    Route::post('remover-documento-exigencia-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@removerDocumento'])->name('remover-documento-exigencia-baixa-inscricao-municipal');
    Route::post('enviar-exigencia-baixa-inscricao-municipal', ['uses' => 'BaixaInscricaoMunicipalController@enviarExigencia'])->name('enviar-exigencia-baixa-inscricao-municipal');
});

Route::get('/documentacao-layout', function () {
    return view('documentacao_layout.index');
});

Route::get('/documentacao-layout-exigencia', function () {
    return view('documentacao_layout.exigencia');
})->name('documentacao-layout-exigencia');

/**
 *
 * ROTAS APEX
 *
 */

Route::get('/define-idioma/{locale}', ['uses' => 'Site\SiteController@defineIdioma'])->name('define-idioma');

Route::get('/cadastro-usuario', ['uses' => 'Apex\UsuarioController@index'])->name('cadastro-usuario');
Route::post('/cadastro-usuario', ['uses' => 'Apex\UsuarioController@store'])->name('cadastro-usuario');

Route::get('/processos-de-aquisicoes', ['uses' => 'Apex\ProcessosDeAquisicoesController@index'])->name('processos-de-aquisicoes');
Route::post('/carregar-documentos-processos-de-aquisicoes', ['uses' => 'Apex\ProcessosDeAquisicoesController@carregarDocumentos'])->name('carregar-documentos-processos-de-aquisicoes');
Route::get('/download-documento-processos-de-aquisicoes/{documento}/{numeroProcesso}', ['uses' => 'Apex\ProcessosDeAquisicoesController@downloadDocumento'])->name('download-documento-processos-de-aquisicoes');
Route::post('/notificar-usuario-processos-de-aquisicoes', ['uses' => 'Apex\ProcessosDeAquisicoesController@notificarUsuario'])->name('notificar-usuario-processos-de-aquisicoes');
Route::post('/carregar-comunicados', ['uses' => 'Apex\ProcessosDeAquisicoesController@carregarComunicados'])->name('carregar-comunicados');
Route::post('/carregar-ofertas', ['uses' => 'Apex\ProcessosDeAquisicoesController@carregarOfertas'])->name('carregar-ofertas');

Route::get('/produtos-servicos', ['uses' => 'Apex\ProdutosEservicosController@index'])->name('produtos-servicos');
Route::post('/carregar-produtos-servicos', ['uses' => 'Apex\ProdutosEservicosController@carregarProdutosEservicos'])->name('carregar-produtos-servicos');

Route::get('/gerenciar-fornecedor', ['uses' => 'Apex\FornecedoresController@index'])->name('gerenciar-fornecedor')->middleware('auth');
Route::get('/cadastrar-fornecedor', ['uses' => 'Apex\FornecedoresController@create'])->name('cadastrar-fornecedor')->middleware('auth');
Route::post('/cadastrar-fornecedor', ['uses' => 'Apex\FornecedoresController@store'])->name('cadastrar-fornecedor')->middleware('auth');
Route::get('/editar-fornecedor/{idFornecedor}', ['uses' => 'Apex\FornecedoresController@edit'])->name('editar-fornecedor')->middleware('auth');
Route::post('/editar-fornecedor/{idFornecedor}', ['uses' => 'Apex\FornecedoresController@update'])->name('editar-fornecedor')->middleware('auth');
Route::post('/carregar-informacoes-apartir-cnpj-fornecedor', ['uses' => 'Apex\FornecedoresController@carregarInformacoesApartirDoCNPJ'])->name('carregar-informacoes-apartir-cnpj-fornecedor')->middleware('auth');
Route::post('/excluir-fornecedor/{idFornecedor}', ['uses' => 'Apex\FornecedoresController@destroy'])->name('excluir-fornecedor')->middleware('auth');
Route::post('/salvar-documentos-fornecedores', ['uses' => 'Apex\FornecedoresController@enviarDocumento'])->name('salvar-documentos-fornecedores')->middleware('auth');
Route::post('/remover-documentos-fornecedores', ['uses' => 'Apex\FornecedoresController@removerDocumento'])->name('remover-documentos-fornecedores')->middleware('auth');

Route::get('/cotacoes', ['uses' => 'Apex\CotacoesController@index'])->name('cotacoes')->middleware('auth');
Route::post('/carregar-produtos-cotacao', ['uses' => 'Apex\CotacoesController@carregarProdutos'])->name('carregar-produtos-cotacao')->middleware('auth');
Route::post('/salvar-cotacoes-produtos', ['uses' => 'Apex\CotacoesController@salvarCotacaoProdutos'])->name('salvar-cotacoes-produtos')->middleware('auth');
Route::post('/salvar-documentos-cotacoes', ['uses' => 'Apex\CotacoesController@enviarDocumento'])->name('salvar-documentos-cotacoes')->middleware('auth');
Route::post('/remover-documentos-cotacoes', ['uses' => 'Apex\CotacoesController@removerDocumento'])->name('remover-documentos-cotacoes')->middleware('auth');
Route::post('/salvar-documentos-cotacoes-na-base-de-dados', ['uses' => 'Apex\CotacoesController@salvarDocumentosNaBaseDeDados'])->name('salvar-documentos-cotacoes-na-base-de-dados')->middleware('auth');
Route::post('/carregar-documentos-cotacoes', ['uses' => 'Apex\CotacoesController@carregarDocumentos'])->name('carregar-documentos-cotacoes')->middleware('auth');
Route::post('/carregar-notificacoes', ['uses' => 'Apex\CotacoesController@carregarNotificacoes'])->name('carregar-notificacoes')->middleware('auth');
Route::post('/marcar-notificacao-como-lida', ['uses' => 'Apex\CotacoesController@marcarNotificacaoComoLida'])->name('marcar-notificacao-como-lida')->middleware('auth');
Route::post('/salvar-esclarecimento-cotacao', ['uses' => 'Apex\CotacoesController@enviarEsclarecimento'])->name('salvar-esclarecimento-cotacao')->middleware('auth');

Route::get('/legislacao', ['uses' => 'Apex\LegislacaoController@index'])->name('legislacao');
Route::get('/download-documento-legislacao/{documento}', ['uses' => 'Apex\LegislacaoController@downloadDocumento'])->name('download-documento-legislacao');

Route::get('/faq', ['uses' => 'Apex\FaqController@index'])->name('faq');
Route::get('/ajuda', ['uses' => 'Apex\FaqController@ajuda'])->name('ajuda');
