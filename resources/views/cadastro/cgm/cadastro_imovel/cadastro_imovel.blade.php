
<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-home mt-3 mb-3"></i> Dados do Imóvel:</h5>

        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="matricula_imovel">Matricula do Imóvel:</label>
                <input type="text" name="matricula_imovel" id="matricula_imovel" 
                    class="form-control form-control-sm mask-matricula_imovel" />
            </div>
        
            <div class="col-lg-6 form-group">
                <label for="tipo_vinculo">Tipo de Vínculo:</label>
                <select name="tipo_vinculo" id="tipo_vinculo" 
                    class="form-control form-control-sm">
                    <option></option>
                    <option value="proprietario">Proprietário</option>
                    <option value="possuidor">Possuidor</option>
                </select>
            </div>
        
            <div class="col-lg-12 form-group">
                <label for="tipo_documento">Tipo de Documento:</label>
                <select name="tipo_documento" id="tipo_documento" 
                    class="form-control form-control-sm">
        
                    <option></option>
                    <option>Contrato de compra e venda</option>
                    <option>Promessa de compra e venda</option>
                    <option>Escritura pública ou particular</option>
                    <option>Documento de doação</option>
                    <option>Conta de luz em nome do possuidor com o endereço do imóvel em questão</option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="documento_comprovante_vinculo"><i class="fa fa-paperclip"></i> Comprovante de vínculo:</label>
                <input type="file" name="documento_comprovante_vinculo" 
                    id="documento_comprovante_vinculo" />
            </div>
        </div>
        
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <button class="btn btn-primary btn-enviar-dados-imovel"
                    data-nome_input="documento_comprovante_vinculo" 
                    data-descricao="Comprovante de Vínculo"
                    data-nome_sessao="documentos_imovel"
                    data-conteudo_documentos="conteudo_documentos_imovel"
                    data-id_tabela="tabela_documentos_imovel"
                    data-pasta="cadastro_imovel">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
        </div> 
    </div>
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_documentos_imovel">
            @include('cadastro.cgm.cadastro_imovel.tabela_documentos')
        </div>
    </div>
</div>