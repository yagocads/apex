<div class="row">
    <div class="col-lg-12 form-group">
        <label for="name">Documento do processo:</label>
        <select class="form-control form-control-sm" id="processo">
            <option value="">Selecione</option>
            <option value="Comprovação de vínculo com o fato gerador">Comprovação de vínculo com o fato gerador</option>
            <option value="0">Outros</option>
        </select>
    </div>
    <div class="col-lg-12 form-group" id="inputOculto">
        <input type="text" name="outro" id="outro" maxlength="40" class="form-control" />
    </div>
    <div class="col-lg-12 form-group">
        <label for="name">Anexar documentos: (Somente PDF)</label>
        <div class="input-group control-group increment" >
            <input type="file" name="doc_processo" id="doc_processo" class="form-control">
            <div class="input-group-btn">
                <button class="btn btn-primary" id="salvar_documento"><i class="fa fa-save"></i> Salvar Documento</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 form-group" id="conteudo_documentos">

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#add").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){
            $(this).parents(".control-group").remove();
        });

        $('#inputOculto').hide();
        $('#processo').change(function() {
            $('#processo').val() == '0' ? $('#inputOculto').show() : $('#inputOculto').hide();
        });

        /**
         *
         * # Salvar Documento na sessão
         *
         */
        $("#salvar_documento").click(function(e){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            e.preventDefault();

            let load = $(".ajax_load");
            let arquivo = $('#doc_processo').prop('files')[0];
            let processo = $("#processo").val();
            let outro =  $("#outro").val();

            if (!validarAnexo(arquivo, ["pdf"],'10485760')) {
                return false;
            }

            if (!arquivo || processo == '') {
                alert("Escolha um arquivo e o documento do processo.");
                return false;
            }

            let formData = new FormData();
            if (processo == '0') {
                processo = 'outros';
            }

            formData.append("arquivo", arquivo);
            formData.append("processo", processo);
            formData.append("outro", outro);

            let url = "{{ route('salvar_doc_sessao') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }
                    $('#processo').val('');
                    $('#outro').val('');
                    $('#doc_processo').val('');
                    $("#conteudo_documentos").html(response);
                }
            });
        });
    });
</script>
