<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Notice;
use App\Models\Contact;
use App\Models\Log;
use App\Mail\FaleConosco;
use App\Mail\ResetPassword;
use App\Support\LoginSupport;
use App\User;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SiteController extends Controller
{

    public function __construct()
    {
    }

    public function index()
    {
        $user = Auth::user();

        if ($user) {
            Log::criarLog($user->cpf, 'Acesso a página Home');
        }

        return view('site.welcome', ['pagina' => "Geral"]);
    }

    public function defineIdioma($locale)
    {
        $languages = ['pt-br', 'en', 'es'];

        // Verifica se o locale passado na URL é válido
        // Se sim, então muda o idioma da aplicação e retorna o locale
        // Se não, então deixa o idioma padrão
        if (in_array($locale, $languages)) {
            session(['localex' => $locale]);

            $user = Auth::user();

            if ($user) {
                Log::criarLog($user->cpf, 'Alteração de idioma para "' . $locale . '"');
            }
        }

        return redirect()->route('principal');
    }

    public function login(Request $request)
    {
        GoogleRecaptcha::verify(
            '6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        $request->validate([
            'cpf' => 'required|exists:users,cpf',
            'password' => 'required'
        ]);

        $attributes = $request->only(['cpf', 'password']);

        if (Auth::attempt($attributes)) {
            Log::criarLog($request->cpf, 'Login realizado no sistema');

            return redirect()->intended();
        } else {
            Log::criarLog($request->cpf, 'Falha na tentativa de fazer o login');

            return redirect()->back()
                ->withInput()
                ->withErrors([
                    'cpf' => 'Essas credenciais não correspondem aos nossos registros.'
                ]);
        }
    }

    public function esqueceuSenha(Request $request)
    {
        $user = DB::table('users')->where('cpf', $request->cpf)->first();

        if (!$user) {
            return redirect()->back()->withErrors(['cpf' => trans('CPF não cadastrado.')]);
        }

        $tokenData = Str::random();

        DB::table('password_resets')->insert([
            'email' => $user->email,
            'token' => $tokenData,
            'created_at' => Carbon::now()
        ]);

        if ($this->sendResetEmail($user, $tokenData)) {
            Log::criarLog($user->cpf, 'Iniciou o processo de recuperação de senha pelo e-mail');

            return redirect()->back()->with(
                'status',
                "Um link de recuperação de senha foi enviado para o seguinte e-mail cadastrado: $user->email"
            );
        } else {
            return redirect()->back()->withErrors(['error' => trans('Erro de rede. Por favor Tente Novamente em alguns instantes.')]);
        }
    }

    private function sendResetEmail($user, $tokenData)
    {
        try {
            Mail::to($user->email)->send(new ResetPassword($tokenData, $user));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function resetPassword(Request $request)
    {
        GoogleRecaptcha::verify(
            '6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        $tokenReset = DB::table('password_resets')
            ->where('token', $request->token)
            ->first();

        if (!$tokenReset) {
            return redirect()->back()
                ->withInput()
                ->with(['error' => 'Token inválido']);
        }

        $user = User::where('email', $tokenReset->email)->first();

        if (!$user) {
            return redirect()->back()
                ->withInput()
                ->with(['error' => 'Usuário não encontrado, verifique se as informações estão corretas']);
        }

        if ($request->password != $request->passwordconfirm) {
            return redirect()->back()
                ->withInput()
                ->with(['error' => 'As senhas informadas não são iguais']);
        }

        $user->password = bcrypt($request->password);
        $user->save();

        Auth::login($user);

        DB::table('password_resets')
            ->where('email', $user->email)
            ->delete();

        Log::criarLog($user->cpf, 'Senha alterada com sucesso no processo de recuperar senha');

        return redirect()->route('principal');
    }

    public function faleconosco()
    {
        $showRecaptcha = GoogleRecaptcha::show(
            '6LfoadQUAAAAACajB7ERoSFGt_C_tFXHyW-9CxYt',
            'mensagemfc',
            'no_debug',
            'mt-3 mb-3',
            'Por favor clique no checkbox do reCAPTCHA primeiro!'
        );

        return view('site.faleconosco', [
            'pagina' => "Geral",
            'name'           => isset($name) ? ($name) : "",
            'email'          => isset($email) ? ($email) : "",
            'phone'          => isset($phone) ? ($phone) : "",
            'showRecaptcha'  => $showRecaptcha
        ]);
    }

    public function enviarmensagem(Request $request)
    {
        GoogleRecaptcha::verify('6LfoadQUAAAAABKl8V57r_lCizXbqD-2n8V2NrMG', 'Por favor clique no checkbox do reCAPTCHA primeiro!');
        Log::create(['servico' => 1004,  'descricao' => "Envio de Mensagem Fale Conosco", 'cpf' => isset(Auth::user()->cpf) ? Auth::user()->cpf : "", 'data' =>  date('Y-m-d H:i:s'), 'browser' => $_SERVER['HTTP_USER_AGENT'], 'ip' => $_SERVER['REMOTE_ADDR'],]);
        $this->contact = $request->all();
        if ($this->contact["name"] != "") {
            Contact::create($this->contact);

            if ($request->solicitacao == "Programa de Amparo ao Trabalhador - PAT") {
                $destino = DESTINO_FALE_CONOSCO_PAT;
            } elseif ($request->solicitacao == "pae") {
                $destino = "pae@marica.rj.gov.br";
            } else {
                $destino = DESTINO_FALE_CONOSCO;
            }

            Mail::to($destino)->send(new FaleConosco($this->contact));

            return view('site.welcome', ['mensagem' => "respostafaleconosco"]);
        } else {
            return view('site.welcome');
        }
    }

    public function valida_cpf_login(Request $request)
    {
        $user = User::where('cpf', $request['cpf_login'])->first();

        if ($user) {
            return redirect()->route('login')->with('cpf', $request['cpf_login']);
        } else {
            return redirect()->route('cadastro-usuario')->with('cpf', $request['cpf_login']);
        }
    }

    public function valida_cpf_cadastro($id)
    {

        $u = User::where('cpf', $id)->first();
        if (is_null($u)) {
            return redirect()->route('register')->with('cpf', $id);
        } else {
            return redirect()->route('login')->with('cpf', $id);
        }
    }
}
