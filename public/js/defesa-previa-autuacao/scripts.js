let load = $(".ajax_load");

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

$('.mask_cnh_requerente').mask('00000000000');
$('.mask_numero_auto').mask('0000000000');

popularEnderecoRequerenteOnLoad();

function popularEnderecoRequerenteOnLoad() {
    const cep = $('#cep').val();

    if (cep !== '') {
        pesquisaCep(cep);
    }
}

onChangeAndOnLoadNotificacaoNaoRecebida();

function onChangeAndOnLoadNotificacaoNaoRecebida() {

    $("#notificacao_nao_recebida").on('change', function () {
        const checkOnChange = $(this).is(':checked');

        const todayDate = new Date();
        const year = todayDate.getFullYear();
        const month = todayDate.getMonth() + 1;
        const day = (todayDate.getDate() < 10 ? '0' + todayDate.getDate() : todayDate.getDate());

        if (checkOnChange) {
            $("#data_recebimento_notificacao").attr('disabled', true);
            $('#data_recebimento_notificacao').val("");
            $('#data_entrada_requerimento').val(`${year}-${month}-${day}`);
            $("#data_entrada_requerimento").attr('readonly', true);
        } else {
            $("#data_recebimento_notificacao").attr('disabled', false);
            $('#data_recebimento_notificacao').val("");
            $('#data_entrada_requerimento').val(`${year}-${month}-${day}`);
            $("#data_entrada_requerimento").attr('readonly', true);
        }
    });

    const checkOnLoad = $('#notificacao_nao_recebida').is(':checked');

    if (checkOnLoad) {
        $('#data_entrada_requerimento').attr('readonly', true);
        $("#data_recebimento_notificacao").attr('disabled', true);
    } else {
        $('#data_entrada_requerimento').attr('readonly', true);
        $("#data_recebimento_notificacao").attr('disabled', false);
    }
}

verificarLimiteDataEntradaNoRequerimento();

function verificarLimiteDataEntradaNoRequerimento() {
    $("#data_recebimento_notificacao").on('blur', function () {

        const dataRecebimentoNotificacaoMais15Dias = moment($('#data_recebimento_notificacao').val()).add(15, 'days');
        const dataEntradaRequerimento = moment($("#data_entrada_requerimento").val());

        if (dataRecebimentoNotificacaoMais15Dias < dataEntradaRequerimento
            || dataEntradaRequerimento < dataRecebimentoNotificacaoMais15Dias.subtract(15, 'days')) {

            $(this).val('').trigger('focus');

            alert(
                `Atenção: A defesa prévia pode ser solicitada até 15 dias após o recebimento da notificação. Favor procurar a Secretaria de Trânsito e Engenharia Viária para dar entrada no requerimento.`
            );
        }
    });
}

enviarDocumento({
    tamanhoAnexoPermitido: 5048000,
    servidor: 'ftp',
    pastaServidor: 'defesa_previa_autuacao',
    nomeSessao: 'documentacao_defesa_previa_autuacao',
    caminhoTabela: 'cidadao.transito.defesa_previa_autuacao.tabela_documentacao',
    rota: 'salvar-documento-defesa-previa-autuacao',
    idTabela: 'tabela_documentacao_defesa_previa_autuacao'
});

removerDocumento({
    servidor: 'ftp',
    pastaServidor: 'defesa_previa_autuacao',
    nomeSessao: 'documentacao_defesa_previa_autuacao',
    caminhoTabela: 'cidadao.transito.defesa_previa_autuacao.tabela_documentacao',
    rota: 'remover-documento-defesa-previa-autuacao',
    idTabela: 'tabela_documentacao_defesa_previa_autuacao'
});

popularDocumentacaoExigida();

function popularDocumentacaoExigida() {
    let documentacaoExigida = ['Cópia da CRLV'];

    documentacaoPorCpfOuCnpj();

    function documentacaoPorCpfOuCnpj() {
        if (formatDocument($("#cpf_cnpj_requerente").val()).length === 11) {
            documentacaoExigida.push('Cópia da CNH do proprietário ou do condutor identificado');
        } else {
            documentacaoExigida.push('CNPJ na validade');
            documentacaoExigida.push('Documentos constitutivos da empresa');
            documentacaoExigida.push('Identidade do Sócio (pelo menos uma inserção)');
            documentacaoExigida.push('CPF do Sócio (pelo menos uma inserção)');
        }
    }

    documentacaoResponsavelPeloPreenchimento();

    function documentacaoResponsavelPeloPreenchimento() {
        let responsavelPreenchimento = $('select[name=responsavel_preenchimento]').val();

        $('select[name=responsavel_preenchimento]').on('change', function () {
            responsavelPreenchimento = $(this).val();
            verificarResponsavelChecked();
        });

        verificarResponsavelChecked();

        function verificarResponsavelChecked() {
            if (responsavelPreenchimento === 'Representação Legal') {
                documentacaoExigida.push('Representação Legal');
                documentacaoExigida.push('Identidade do Representante');
                documentacaoExigida.push('CPF Representante Legal');
            } else if (responsavelPreenchimento === 'Próprio') {
                if (documentacaoExigida.indexOf('Representação Legal') !== -1) {
                    documentacaoExigida.splice(documentacaoExigida.indexOf('Representação Legal'), 1);
                }

                if (documentacaoExigida.indexOf('Identidade do Representante') !== -1) {
                    documentacaoExigida.splice(documentacaoExigida.indexOf('Identidade do Representante'), 1);
                }

                if (documentacaoExigida.indexOf('CPF Representante Legal') !== -1) {
                    documentacaoExigida.splice(documentacaoExigida.indexOf('CPF Representante Legal'), 1);
                }
            }

            popularDescricaoDocumento(documentacaoExigida);
            popularListaDocumentacaoObrigatoria(documentacaoExigida);
        }
    }

    documentacaoNotificacaoNaoRecebida();

    function documentacaoNotificacaoNaoRecebida() {

        let notificacaoNaoRecebidaChecked = $('input[name=notificacao_nao_recebida]').is(':checked');

        $('input[name=notificacao_nao_recebida]').on('change', function () {
            notificacaoNaoRecebidaChecked = $(this).is(':checked');

            verificarNotificacaoCheckedEpopularDocumento();
        });

        verificarNotificacaoCheckedEpopularDocumento();

        function verificarNotificacaoCheckedEpopularDocumento() {
            if (notificacaoNaoRecebidaChecked) {
                if (documentacaoExigida.indexOf('Notificação de Autuação ou Auto de Infração') !== -1) {
                    documentacaoExigida.splice(documentacaoExigida.indexOf('Notificação de Autuação ou Auto de Infração'), 1);
                }
            } else {
                documentacaoExigida.push('Notificação de Autuação ou Auto de Infração');
            }

            popularDescricaoDocumento(documentacaoExigida);
            popularListaDocumentacaoObrigatoria(documentacaoExigida);
        }
    }

    popularDescricaoDocumento(documentacaoExigida);

    function popularDescricaoDocumento(documentacaoExigida) {
        let select = document.querySelector("select[name=descricao_documento]");

        select.innerHTML = "";
        select.append(document.createElement("option"));

        const documentacao = numerarDocumentacao(documentacaoExigida);

        documentacao.forEach((documento) => {
            let elementOption = document.createElement("option");
            elementOption.append(documento);
            select.appendChild(elementOption);
        });
    }

    popularListaDocumentacaoObrigatoria(documentacaoExigida);

    function popularListaDocumentacaoObrigatoria(documentacaoExigida) {
        let lista = document.querySelector(".lista_documentacao_obrigatoria");
        lista.innerHTML = "";

        const documentacao = numerarDocumentacao(documentacaoExigida);

        documentacao.forEach((documento) => {
            let elementLi = document.createElement("li");
            elementLi.append(documento);
            lista.appendChild(elementLi);
        });
    }

    function numerarDocumentacao(documentacaoExigida) {
        const documentacaoNumerada = [];

        for (let i = 0; i < documentacaoExigida.length; i++) {
            documentacaoNumerada.push(`${i + 1}. ${documentacaoExigida[i]}`);
        }

        // return documentacaoNumerada;
        return documentacaoExigida;
    }
}

enviarInformacoes();

function enviarInformacoes() {
    $(".enviar_informacoes").on("click", function (e) {
        e.preventDefault();

        if (!$('input[name=confirmacao_de_informacoes]').is(':checked')) {
            alert("Atenção: Você precisa concordar com o termo antes de enviar as informações.");
            return;
        }

        verificarDocumentosExigidos();
    });
}

function verificarDocumentosExigidos() {

    const documentosExigidos = [];

    $("select[name=descricao_documento]").find('option').each(function () {

        if ($(this).text() != '') {
            documentosExigidos.push($(this).text());
        }
    });
    console.log(documentosExigidos);

    $.ajax({
        url: "verificar-documentos-defesa-previa-autuacao",
        method: "POST",
        data: {
            nomeSessao: "documentacao_defesa_previa_autuacao",
            documentosExigidos,
        },
        dataType: "json",
        success(response) {
            if (response.length > 0) {
                alert(
                    `Atenção: Os seguintes documentos são obrigatórios: ${response.join(", ")}.`
                );
                return false;
            }

            $("#formDefesaPreviaAutuacao").trigger("submit");
        },
    });
}
