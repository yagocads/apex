<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DefesaPreviaAutuacaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Dados do Requerente
            'responsavel_preenchimento' => 'required',
            'nome_requerente' => 'required',
            'cpf_cnpj_requerente' => 'required',
            'data_nasc_requerente' => 'required',
            'cnh_requerente' => 'required',
            'identidade_requerente' => 'required',
            'orgao_exp_requerente' => 'required',
            'nacionalidade_requerente' => 'required',
            'naturalidade_requerente' => 'required',
            'celular_requerente' => 'required',
            'email_requerente' => 'required',
            'cep' => 'required',
            'endereco' => 'required',
            'bairro' => 'required',
            'municipio' => 'required',
            'estado' => 'required',
            'numero' => 'required',
            'complemento' => 'required',

            // Dados Solicitação de Defesa Prévia
            'numero_auto' => 'required',
            'data_entrada_requerimento' => 'required',
            'tipo_multa' => 'required',
            'logradouro_infracao' => 'required'
        ];
    }

    public function messages()
    {
        return [
            // Dados do Requerente
            'responsavel_preenchimento.required' => 'Preencha o campo Responsável pelo preenchimento (Dados do Requerente)',
            'nome_requerente.required' => 'Preencha o campo Nome (Dados do Requerente)',
            'cpf_cnpj_requerente.required' => 'Preencha o campo CPF/CNPJ (Dados do Requerente)',
            'data_nasc_requerente.required' => 'Preencha o campo Data de Nascimento (Dados do Requerente)',
            'cnh_requerente.required' => 'Preencha o campo CNH (Dados do Requerente)',
            'identidade_requerente.required' => 'Preencha o campo Doc. Identidade (Dados do Requerente)',
            'orgao_exp_requerente.required' => 'Preencha o campo Órgão Expeditor (Dados do Requerente)',
            'nacionalidade_requerente.required' => 'Preencha o campo Nacionalidade (Dados do Requerente)',
            'naturalidade_requerente.required' => 'Preencha o campo Naturalidade (Dados do Requerente)',
            'celular_requerente.required' => 'Preencha o campo Celular (Dados do Requerente)',
            'email_requerente.required' => 'Preencha o campo E-mail (Dados do Requerente)',
            'cep.required' => 'Preencha o campo Cep (Dados do Requerente)',
            'endereco.required' => 'Preencha o campo Endereço (Dados do Requerente)',
            'bairro.required' => 'Preencha o campo Bairro (Dados do Requerente)',
            'municipio.required' => 'Preencha o campo Município (Dados do Requerente)',
            'estado.required' => 'Preencha o campo Estado (Dados do Requerente)',
            'numero.required' => 'Preencha o campo Número (Dados do Requerente)',
            'complemento.required' => 'Preencha o campo Complemento (Dados do Requerente)',

            // Dados Solicitação de Defesa Prévia
            'numero_auto.required' => 'Preencha o campo Número do auto (Dados da Solicitação de Defesa Prévia)',
            'data_entrada_requerimento.required' => 'Preencha o campo Data de entrada no requerimento (Dados da Solicitação de Defesa Prévia)',
            'tipo_multa.required' => 'Preencha o campo Tipo de Multa (Dados da Solicitação de Defesa Prévia)',
            'logradouro_infracao.required' => 'Preencha o campo Logradouro da Infração (Dados da Solicitação de Defesa Prévia)'
        ];
    }
}
