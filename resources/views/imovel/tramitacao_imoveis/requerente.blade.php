<div class="row">
    <div class="col-lg-6">
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="nome_requerente">Nome Completo:</label>
                <input type="text" name="nome_requerente" id="nome_requerente" 
                class="form-control form-control-sm {{ ($errors->has('nome_requerente') ? 'is-invalid' : '') }}"
                    value="{{ convert_accentuation($dadosCgm->aCgmPessoais->z01_nome) }}"
                    maxlength="100" readonly="true" />

                    @if ($errors->has('nome_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_requerente') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="form-row">
            <div class="col-lg-4 form-group">
                <label for="identidade_requerente">RG Nº (Pessoa Física):</label>
                <input type="text" name="identidade_requerente" id="identidade_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('identidade_requerente') ? 'is-invalid' : '') }}" 
                    value="{{ $dadosCgm->aCgmAdicionais->z01_ident }}" maxlength="10" readonly="true" />

                    @if ($errors->has('identidade_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('identidade_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="orgao_emissor_requerente">Órgão Emissor:</label>
                <input type="text" name="orgao_emissor_requerente" id="orgao_emissor_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('orgao_emissor_requerente') ? 'is-invalid' : '') }}" 
                    value="{{ $dadosCgm->aCgmAdicionais->z01_identorgao }}" maxlength="10" readonly="true" />

                    @if ($errors->has('orgao_emissor_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('orgao_emissor_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-4 form-group">
                <label for="data_emissao_requerente">Data Emissão:</label>
                <input type="date" name="data_emissao_requerente" 
                    id="data_emissao_requerente"
                    max="{{ date('Y-m-d') }}" 
                    class="form-control form-control-sm {{ ($errors->has('data_emissao_requerente') ? 'is-invalid' : '') }}"
                    value="{{ $dadosCgm->aCgmAdicionais->z01_identdtexp }}" readonly="true" />

                    @if ($errors->has('data_emissao_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_emissao_requerente') }}
                        </div>
                    @endif
            </div>
        </div> 
    </div>

    <div class="col-lg-6">
        <div class="form-row">
            <div class="col-lg-8 form-group">
                <label for="endereco_requerente">Endereço:</label>
                <input type="text" name="endereco_requerente" id="endereco_requerente" 
                class="form-control form-control-sm {{ ($errors->has('endereco_requerente') ? 'is-invalid' : '') }}"
                    value="{{ convert_accentuation($dadosCgm->endereco->sRua) }}" maxlength="100" readonly="true" />

                    @if ($errors->has('endereco_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('endereco_requerente') }}
                        </div>
                    @endif
            </div>
        
            <div class="col-lg-4 form-group">
                <label for="telefone_requerente">Telefone:</label>
                <input type="text" name="telefone_requerente" id="telefone_requerente" 
                class="form-control form-control-sm mask-cel {{ ($errors->has('telefone_requerente') ? 'is-invalid' : '') }}"
                    value="{{ $dadosCgm->aCgmContato->z01_telcel }}" 
                        maxlength="15" readonly="true" />

                    @if ($errors->has('telefone_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('telefone_requerente') }}
                        </div>
                    @endif
            </div>
            
            <div class="col-lg-4 form-group">
                <label for="cpf_cnpj_requerente">CPF/CNPJ:</label>
                <input type="text" name="cpf_cnpj_requerente" id="cpf_cnpj_requerente" 
                    class="form-control form-control-sm {{ mb_strlen($dadosCgm->aCgmPessoais->z01_cgccpf) == 14 ? 'mask-cnpj' : 'mask-cpf' }} {{ ($errors->has('cpf_cnpj_requerente') ? 'is-invalid' : '') }}" 
                    value="{{ $dadosCgm->aCgmPessoais->z01_cgccpf }}" maxlength="20" readonly="true" />

                    @if ($errors->has('cpf_cnpj_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpf_cnpj_requerente') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-8 form-group">
                <label for="email_requerente">E-mail:</label>
                <input type="email" name="email_requerente" id="email_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('email_requerente') ? 'is-invalid' : '') }}" 
                    value="{{ convert_accentuation($dadosCgm->aCgmContato->z01_email) }}" maxlength="75" readonly="true" />

                    @if ($errors->has('email_requerente'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email_requerente') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 form-group">
        <label for="tipo_requerente">Tipo de Requerente:</label>
        <select name="tipo_requerente" id="tipo_requerente" class="form-control form-control-sm {{ ($errors->has('tipo_requerente') ? 'is-invalid' : '') }}">
            <option></option>
            <option {{ old('tipo_requerente') == "Procurador" ? 'selected' : '' }}>Procurador</option>
            <option {{ old('tipo_requerente') == "Proprietário" ? 'selected' : '' }}>Proprietário</option>
            <option {{ old('tipo_requerente') == "Responsável Legal" ? 'selected' : '' }}>Responsável Legal</option>
            <option {{ old('tipo_requerente') == "Responsável Técnico" ? 'selected' : '' }}>Responsável Técnico</option>
        </select>

        @if ($errors->has('tipo_requerente'))
            <div class="invalid-feedback">
                {{ $errors->first('tipo_requerente') }}
            </div>
        @endif
    </div>

    <div class="col-lg-3 form-group d-none">
        <label for="local_abertura">Local de Abertura:</label>
        <select name="local_abertura" id="local_abertura" 
            class="form-control form-control-sm {{ ($errors->has('local_abertura') ? 'is-invalid' : '') }}"
            readonly="true">
            <option>Portal Sim</option>
        </select>

        @if ($errors->has('local_abertura'))
            <div class="invalid-feedback">
                {{ $errors->first('local_abertura') }}
            </div>
        @endif
    </div>
</div>