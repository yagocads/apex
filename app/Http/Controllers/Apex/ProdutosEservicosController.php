<?php

namespace App\Http\Controllers\Apex;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProdutosEservicosController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user) {
            Log::criarLog($user->cpf, 'Acesso a página Produtos e Serviços');
        }

        return view('apex.produtos-servicos.index', ['pagina' => "Produtos e Serviços"]);
    }

    public function carregarProdutosEservicos()
    {
        return view('apex.produtos-servicos.tabela_produtos_servicos', [
            'pagina' => "Produtos e Serviços",
            'produtos' => DB::connection('lecom')
                ->table('apex_catalogo_serv_prd')
                ->whereNull('dt_exclusao')
                ->get()
        ]);
    }
}
