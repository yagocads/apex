<div class="card">
    <div class="card-header">
        <h5>Cards</h5>
    </div>

    <div class="card-body">
        <h6><b>Card utilizado:</b> (excelente opção para responsivo, caso não venha o caso usar dataTable)</h6>
        <hr>

        <div class="callout-red shadow-sm mb-4">
            <div class="row">
                <div class="col-lg-6">
                    <p><b>Mátricula:</b> 1111</p>
                    <p><b>Tipo:</b> qualquer</p>
                    <p><b>Bairro:</b> Bangu</p>
                    <p><b>Logradouro:</b> Rua dos Banguenses</p>
                </div>

                <div class="col-lg-6">
                    <p><b>Número:</b> 555</p>
                    <p><b>Complemento:</b> Casa</p>
                    <p><b>Planta/Quadra/Lote:</b> 111 / 222 / 333</p>
                </div>
            </div>
        </div>
    </div>
</div>