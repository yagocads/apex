<div class="form-row">
<div class="col-lg-6 form-group">
        <label for="enviar_processo_para">Enviar processo para:</label>
        <input type="text" name="enviar_processo_para" id="enviar_processo_para" 
        class="form-control form-control-sm {{ ($errors->has('enviar_processo_para') ? 'is-invalid' : '') }}"
            value="triagem_urb" maxlength="100" readonly="true" />

            @if ($errors->has('enviar_processo_para'))
                <div class="invalid-feedback">
                    {{ $errors->first('enviar_processo_para') }}
                </div>
            @endif
    </div>
</div>

<div class="form-row">
    <div class="col-lg-12 form-group">
        <label for="despacho">Despacho:</label>
        <textarea name="despacho" id="despacho"
            class="form-control form-control-sm {{ ($errors->has('despacho') ? 'is-invalid' : '') }}" 
            placeholder="Digite aqui" cols="5" rows="6" maxlength="255" readonly="true">Solicitação de serviço realizada pelo contribuinte no portal SIM</textarea>

        @if ($errors->has('despacho'))
            <div class="invalid-feedback">
                {{ $errors->first('despacho') }}
            </div>
        @endif
    </div>
</div>
