<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([  'namespace' => 'Site',
                'middleware' => 'cors'
                ], function () {
    Route::get('/lecomAtualizaCgm/{chamado?}/{status?}',    'SiteController@lecomAtualizaCgm')->name('lecomAtualizaCgm');

});


Route::get('/verificaRel/{matricula}/{documento}',['uses' => 'FinanceiroController@verificaRel'])->name('verificaRel');
