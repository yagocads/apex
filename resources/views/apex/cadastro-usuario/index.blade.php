@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{__('Cadastro de Usuário')}}</h4>
                </div>

                <form method="POST" action="{{ route('cadastro-usuario') }}" autocomplete="off">
                    @csrf

                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-lg-4 form-group">
                                <label for="nome">{{__('Nome Completo')}}:</label>
                                <input type="text" name="nome" id="nome"
                                    class="form-control form-control-sm {{ ($errors->has('nome') ? 'is-invalid' : '') }}"
                                    value="{{ old('nome') }}" maxlength="255" />

                                    @if ($errors->has('nome'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('nome') }}
                                        </div>
                                    @endif
                            </div>

                            <div class="col-lg-4 form-group">
                                <label for="email">{{__('E-mail')}}:</label>
                                <input type="email" name="email" id="email"
                                    class="form-control form-control-sm {{ ($errors->has('email') ? 'is-invalid' : '') }}"
                                    value="{{ old('email') }}" maxlength="255" />

                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-lg-2 form-group">
                                <label for="telefone">{{__('Telefone')}}:</label>
                                <input type="text" name="telefone" id="telefone"
                                    class="form-control form-control-sm mask-cel {{ ($errors->has('telefone') ? 'is-invalid' : '') }}"
                                    value="{{ old('telefone') }}" />

                                    @if ($errors->has('telefone'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('telefone') }}
                                        </div>
                                    @endif
                            </div>

                            <div class="col-lg-3 form-group">
                                <label for="cpfUsuario">{{__('CPF')}}:</label>
                                <input type="text" name="cpfUsuario" id="cpfUsuario"
                                    class="form-control form-control-sm {{ ($errors->has('cpfUsuario') ? 'is-invalid' : '') }}"
                                    value="{{ session('cpf') ?? old('cpfUsuario') }}" readonly />

                                    @if ($errors->has('cpfUsuario'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('cpfUsuario') }}
                                        </div>
                                    @endif
                            </div>

                            <div class="col-lg-3 form-group">
                                <label for="senha">{{__('Senha')}}:</label>
                                <input type="password" name="senha" id="senha"
                                    class="form-control form-control-sm {{ ($errors->has('senha') ? 'is-invalid' : '') }}"
                                    maxlength="20" />

                                    @if ($errors->has('senha'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('senha') }}
                                        </div>
                                    @endif
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success mb-1">{{__('Cadastrar')}}</button>
                        <a href="./" class="btn btn-danger mb-1">{{__('Cancelar')}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
    <script>
        $('#cpf').on('change', function() {
            let cpf = $(this).val();

            if (!validarCPF(cpf)) {
                alert('Atenção: CPF inválido!');
                $(this).val('');
                $(this).focus();
                return;
            }
        });
    </script>
@endsection
