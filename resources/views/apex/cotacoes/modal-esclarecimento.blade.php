<!-- The Modal -->
<div class="modal fade" id="modal-esclarecimento">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">
                    {{__('Esclarecimento do processo Nº')}} <span class="numero-processo"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <label for="esclarecimento">Esclarecimento: </label>
                <textarea name="esclarecimento" id="esclarecimento"
                    class="form-control form-control-sm" cols="5" rows="5"></textarea>

                <button class="btn btn-success mt-3 enviar-esclarecimento">Enviar</button>
            </div>
        </div>
    </div>
</div>
