@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{__('Legislação')}}</h4>
                </div>

                <div class="card-body">
                    <div class="table-responsive py-1 pl-1 pr-1">
                        <table class="table table-striped table-bordered dataTable dt-responsive w-100">
                            <thead>
                                <tr>
                                    <th>{{__('Descrição do Documento')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($documentos)
                                    @foreach($documentos as $documento)
                                        <tr>
                                            <td>
                                                <a href="{{ route('download-documento-legislacao', $documento->Name_exp_2) }}" style="text-decoration: none">
                                                    <i class="fa fa-download"></i>
                                                    {{ $documento->descricao }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
    <script></script>
@endsection
