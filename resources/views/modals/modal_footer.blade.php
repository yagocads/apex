<!-- The Modal -->
<div class="modal fade" id="footer-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Título Modal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <!-- body content -->
            </div>

            <div class="modal-footer">
                <!-- footer content -->
            </div>

        </div>
    </div>
</div>
