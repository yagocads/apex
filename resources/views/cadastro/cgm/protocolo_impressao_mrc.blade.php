<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MRC</title>

        <!--Custon CSS-->
        <link rel="stylesheet" href="{{ getcwd().'/css/certidao.css' }}">

        <!--Favicon-->
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    </head>
    <body>

        <div class="header">
            <div class="div-logo">
                <img src="{{ getcwd().'/img/brasao.png'  }}" class="logo" alt="Prefeitura de Maricá">
            </div>
            <div class="titulo">
                ESTADO DO RIO DE JANEIRO<br>
                PREFEITURA MUNICIPAL DE MARICÁ<br>
                MRC - MARICÁ NA REDE DA CULTURA
            </div>
        </div>

        <div>
            <h1 class="titulo-autenticacao">
                PROTOCOLO DE SOLICITAÇÃO<br>
                DE BENEFÍCIO
            </h1>

            <p class="dados-certidao">
                A sua solicitação do benefício do MRC - MARICÁ NA REDE DA CULTURA foi realizada com sucesso.<br>
            </p>
            <br>
            <p class="texto-autenticacao">
                Número do protocolo:<br>
            </p>

            <h2 class="status">{{ $protocolo }}</h2>

            <p class="dados-certidao"><b>CPF/CNPJ: </b> <span class="cpfOuCnpj">{{ $cpfOuCnpj }}</span> </p>
            <p class="dados-certidao"><b>Identificação: </b> {{ $identificacao }}</p>
            <p class="dados-certidao"><b>Data de Emissão:</b> {{ \Carbon\Carbon::parse(date("Y-m-d"))->locale("pt-BR")->format('d/m/Y') }} </p>
            
            <p class="data-autenticacao">
                Maricá, {{ \Carbon\Carbon::parse(date("Y-m-d"))->locale("pt-BR")->format('d/m/Y') }}.
            </p>
        </div>

        <script src="{{ getcwd().'/js/scripts.js' }}"></script>
    </body>
</html>
