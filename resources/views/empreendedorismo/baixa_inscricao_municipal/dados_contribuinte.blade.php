<div class="row">
    <div class="col-lg-12">
        <div class="form-row">
            <div class="col-lg-12 form-group">
                <label for="razao_social_contribuinte">Razão Social:</label>
                <input type="text" name="razao_social_contribuinte" id="razao_social_contribuinte"
                    class="form-control form-control-sm {{ ($errors->has('razao_social_contribuinte') ? 'is-invalid' : '') }}"
                    value="{{ old('razao_social_contribuinte') }}" maxlength="100" />

                    @if ($errors->has('razao_social_contribuinte'))
                        <div class="invalid-feedback">
                            {{ $errors->first('razao_social_contribuinte') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="cpfOuCnpj_contribuinte">CPF/CNPJ:</label>
                <input type="text" name="cpfOuCnpj_contribuinte" id="cpfOuCnpj_contribuinte"
                    class="form-control form-control-sm cpfOuCnpj {{ ($errors->has('cpfOuCnpj_contribuinte') ? 'is-invalid' : '') }}"
                    value="{{ old('cpfOuCnpj_contribuinte') }}" />

                    @if ($errors->has('cpfOuCnpj_contribuinte'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cpfOuCnpj_contribuinte') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="inscricao_municipal">Inscrição Municipal:</label>
                <input type="text" name="inscricao_municipal" id="inscricao_municipal"
                    class="form-control form-control-sm {{ ($errors->has('inscricao_municipal') ? 'is-invalid' : '') }}"
                    value="{{ old('inscricao_municipal') }}" maxlength="9" />

                    @if ($errors->has('inscricao_municipal'))
                        <div class="invalid-feedback">
                            {{ $errors->first('inscricao_municipal') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="telefone_contribuinte">Telefone:</label>
                <input type="text" name="telefone_contribuinte" id="telefone_contribuinte"
                    class="form-control form-control-sm mask-tel {{ ($errors->has('telefone_contribuinte') ? 'is-invalid' : '') }}"
                    value="{{ old('telefone_contribuinte') }}" />

                    @if ($errors->has('telefone_contribuinte'))
                        <div class="invalid-feedback">
                            {{ $errors->first('telefone_contribuinte') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="celular_contribuinte">Celular:</label>
                <input type="text" name="celular_contribuinte" id="celular_contribuinte"
                    class="form-control form-control-sm mask-cel {{ ($errors->has('celular_contribuinte') ? 'is-invalid' : '') }}"
                    value="{{ old('celular_contribuinte') }}" />

                    @if ($errors->has('celular_contribuinte'))
                        <div class="invalid-feedback">
                            {{ $errors->first('celular_contribuinte') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="email_contribuinte">E-mail Contribuinte:</label>
                <input type="text" name="email_contribuinte" id="email_contribuinte"
                    class="form-control form-control-sm {{ ($errors->has('email_contribuinte') ? 'is-invalid' : '') }}"
                    value="{{ old('email_contribuinte') }}" />

                    @if ($errors->has('email_contribuinte'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email_contribuinte') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="inscricao_estadual">Inscrição Estadual:</label>
                <input type="text" name="inscricao_estadual" id="inscricao_estadual"
                    class="form-control form-control-sm {{ ($errors->has('inscricao_estadual') ? 'is-invalid' : '') }}"
                    maxlength="8"
                    value="{{ old('inscricao_estadual') }}" />

                    @if ($errors->has('inscricao_estadual'))
                        <div class="invalid-feedback">
                            {{ $errors->first('inscricao_estadual') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-3 form-group">
                <label for="iptu">IPTU:</label>
                <input type="text" name="iptu" id="iptu"
                    class="form-control form-control-sm {{ ($errors->has('iptu') ? 'is-invalid' : '') }}"
                    value="{{ old('iptu') }}" maxlength="6" />

                    @if ($errors->has('iptu'))
                        <div class="invalid-feedback">
                            {{ $errors->first('iptu') }}
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div>
