@extends('layouts.tema_principal')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h2>Faq</h2>
                </div>

                <div class="card-body">
                    <div id="accordion">
                        @foreach($faq as $dados)
                            <div class="card mb-2">
                                <a class="card-link text-dark" data-toggle="collapse" href="#faq{{ $loop->index }}">
                                    <div class="card-header">
                                        <b>{{ $dados->duvida }}</b>
                                        <li class="fa fa-level-down"></li>
                                    </div>
                                </a>

                                <div id="faq{{ $loop->index }}" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>
                                            {{ $dados->resposta }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
    <script></script>
@endsection
