
<div class="solicitante_prosseguimento">
    <div class="row">
        <div class="col-lg-12">
            <h5><i class="fa fa-user mt-3 mb-3"></i> CNAE:</h5>

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="objeto_social">Objeto Social:</label>
                    <input type="text" name="objeto_social" id="objeto_social" 
                        class="form-control form-control-sm {{ ($errors->has('objeto_social') ? 'is-invalid' : '') }}" 
                        value="{{ old('objeto_social') }}" maxlength="100" />

                        @if ($errors->has('objeto_social'))
                            <div class="invalid-feedback">
                                {{ $errors->first('objeto_social') }}
                            </div>
                        @endif
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="tipo_cnae">Tipo CNAE:</label>
                    <select name="tipo_cnae" id="tipo_cnae" 
                        class="form-control form-control-sm {{ ($errors->has('tipo_cnae') ? 'is-invalid' : '') }}">
                        <option></option>
                        <option {{ old('tipo_cnae') == "ATIVIDADE PRINCIPAL" ? "selected" : "" }} value="ATIVIDADE PRINCIPAL">ATIVIDADE PRINCIPAL</option>
                        <option {{ old('tipo_cnae') == "ATIVIDADE SECUNDARIA" ? "selected" : "" }} value="ATIVIDADE SECUNDARIA">ATIVIDADE SECUNDARIA</option>
                    </select>

                        @if ($errors->has('tipo_cnae'))
                            <div class="invalid-feedback">
                                {{ $errors->first('tipo_cnae') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-8 form-group">
                    <label for="atividade_cnae">Atividade CNAE:</label>
                    <select name="atividade_cnae" id="atividade_cnae" 
                        class="form-control form-control-sm {{ ($errors->has('estado_empresa') ? 'is-invalid' : '') }}" 
                        >
                        <option></option>
    
                        @foreach($ativavdesCnae as $atividade)
                            <option value="{{ $atividade->atividade }}|{{ $atividade->codigo }}|{{ $atividade->grupo }}" {{ old('atividade_cnae') == $atividade->atividade."|".$atividade->codigo."|".$atividade->grupo ? 'selected' : '' }}>
                                {{ $atividade->atividade }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="codigo_cnae">Código CNAE:</label>
                    <input type="text" name="codigo_cnae" id="codigo_cnae" 
                        class="form-control form-control-sm {{ ($errors->has('codigo_cnae') ? 'is-invalid' : '') }}" 
                        value="{{ old('codigo_cnae') }}" maxlength="100" disabled/>


                        @if ($errors->has('codigo_cnae'))
                            <div class="invalid-feedback">
                                {{ $errors->first('codigo_cnae') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-8 form-group">
                    <label for="grupo_cnae">Grupo CNAE:</label>
                    <input type="text" name="grupo_cnae" id="grupo_cnae" 
                        class="form-control form-control-sm {{ ($errors->has('grupo_cnae') ? 'is-invalid' : '') }}" 
                        value="{{ old('grupo_cnae') }}" maxlength="100" disabled/>

                        @if ($errors->has('grupo_cnae'))
                            <div class="invalid-feedback">
                                {{ $errors->first('grupo_cnae') }}
                            </div>
                        @endif
                </div>
            </div>

            <div class="form_row">
                <div class="text-right">
                    <button class="btn btn-primary btn-enviar-documento-cnae" 
                        data-nome_sessao="dados_cnae"
                        data-conteudo_documentos="conteudo_dados_cnae"
                        data-id_tabela="dados_cnae"
                        data-pasta="cnae">
                        Incluir Dados CNAE <i class="fa fa-send"></i>
                    </button>
                </div>
            </div>
        </div>
        
    </div>

    <hr/>

    <div class="row">
        <div class="col-lg-12 form-group">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> CNAE Enviados:</h5>

            <div class="conteudo_dados_cnae">
                @include('empresa.mei.cnae.tabela_documentos')
            </div>
        </div>
    </div>
</div>
