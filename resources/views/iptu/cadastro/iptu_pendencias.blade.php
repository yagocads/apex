@extends('layouts.tema_principal')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 ">
            <h3>
                <i class="fa fa-home mb-3"></i> Cadastro do IPTU
            </h3>

            <div class="card">
                <div class="card-header">
                    <h4>
                        <i class="fa fa-exclamation-triangle"></i> Exigência - Chamado: {{ $processo->Chamado }}
                    </h4>
                </div>

                <div class="card-body">
                    <form action="{{ route('formPendenciasIptuSalvar') }}" method="POST" id="FormSanarPendencias" name="FormSanarPendencias">
                        @csrf
                        <input type="hidden" name="cpf_ou_cnpj"  id="cpf_ou_cnpj" value="{{ $processo->cpf }}" />
                        <input type="hidden" name="fase"  id="fase" value="{{ $processo->Etapa }}" />
                        <input type="hidden" name="ciclo"  id="ciclo" value="{{ $processo->Ciclo }}" />
                        <div class="row justify-content-center">
                            <div class="col-lg-4">
                                <label for="justificativa_recurso">Exigências</label>
                                <textarea name="Exigencia" 
                                    class="form-control form-control-sm" 
                                    id="Exigencia"
                                    rows="12" cols="5" maxlength="255" readonly>@isset($processo->motivo){{$processo->motivo}}@endisset</textarea>
                            </div>
                        
                            <div class="col-lg-4">
                                <label for="justificativa_recurso">Cumprimento de exigência:</label>
                                <textarea name="justificativa_recurso" 
                                    class="form-control form-control-sm" 
                                    id="justificativa_recurso"
                                    rows="12" cols="5" maxlength="255"></textarea>
                            </div>
                            <div class="col-lg-4">
                                <input type="hidden" name="chamado" id="chamado" value="{{ $processo->Chamado }}" class="form-control" />
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <label for="tipo_documento"><i class="fa fa-paperclip"></i> Anexar arquivo:</label>
                                        <select class="form-control form-control-sm" name="tipo_documento" id="tipo_documento">
                                            <option value="">Selecione</option>
                                            <option value="Cartão CNPJ">Cartão CNPJ</option>
                                            <option value="CPF Solicitante">CPF Solicitante</option>
                                            <option value="RG Solicitante">RG Solicitante</option>
                                            <option value="Comprovante Residência Solicitante">Comprovante Residência Solicitante</option>
                                            <option value="Comprovante de Endereço">Comprovante de Endereço</option>
                                            <option value="RG Sócio">RG Sócio</option>
                                            <option value="CPF Sócio">CPF Sócio</option>
                                            <option value="CE Sócio">Comprovante de Endereço Sócio</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-12 form-group" id="inputOculto">
                                        <input type="text" name="outro" id="outro" class="form-control" />
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <div class="alert alert-info ">
                                            <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group">
                                        <label for="name">Anexe o documento que deseja enviar:</label>
                                        <div class="input-group control-group increment" >
                                            <input type="file" name="doc_processo" id="doc_processo">
                                        </div>

                                        {{-- <div class="input-group-btn">
                                            <button class="btn btn-primary" id="salvar_documento"><i class="fa fa-save"></i> Salvar Documento</button>
                                        </div> --}}
                                    </div>
                                    <div class="col-lg-12">
                                        <button class="btn btn-primary btn-enviar-documento"
                                            data-nome_input="doc_processo" 
                                            data-descricao=""
                                            data-nome_sessao="documentos_dados_pendencias"
                                            data-conteudo_documentos="conteudo_pendencia"
                                            data-id_tabela="iptu_pendencias_documentos"
                                            data-pasta="dados_pendencias">
                                            <i class="fa fa-plus"></i> Adicionar Documento 
                                        </button>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="col-lg-12">
                                <hr class="mt-4"/>
                                <div class="row">
                                    <div class="col-lg-12 form-group">
                                        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

                                        <div class="conteudo_pendencia">
                                            @include('iptu.cadastro.iptu_pendencias_documentos')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ route('acompanhamento') }}"
                        class="btn btn-primary mt-3">
                        <i class="fa fa-arrow-left"></i> Voltar
                    </a>

                    <button class="btn btn-success mt-3 enviar_informacoes">Enviar Pendências <i class="fa fa-send"></i></button>
                </div>
            </div>
            <div class="row">
                <div id="mgs">

                </div>
            </div>
        </div>


<script type="text/javascript">
    $(document).ready(function(){

        // Spinner
        var load = $(".ajax_load");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#inputOculto').hide();
        $('#processo').change(function() {
            if ($('#processo').val() == '0') {
                $('#inputOculto').show();
            } else {
                $('#inputOculto').hide();
            }
        });

        
        $(".enviar_informacoes").on("click", function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{ route('verificar_documentos_pendencias_iptu') }}",
                method: 'POST',
                success(response) {
                    resposta =  JSON.parse(response);
                    if( !resposta.documentosEnviados ){
                        alert("Por Favor anexe os documentos necessários para sanar as pendências");
                        $("#tipo_documento").focus();                    
                        return false;
                    }
                    else{
                        $("form[name=FormSanarPendencias]").submit();
                    }
                }
            });
        });

        $("body").on("click", ".btn-enviar-documento", function(e) {
            e.preventDefault();

            if ( $("#tipo_documento").val() == "" ){
                alert("ATENÇÃO: Informe qual documento será enviado.");
                $("#tipo_documento").focus();
                return false;
            }

            let processo = {{ $processo->Chamado }};

            let nome_input = $(this).data("nome_input");
            let descricao = $("#tipo_documento").val();
            let nome_sessao = $(this).data("nome_sessao");
            let conteudo_documentos = $(this).data("conteudo_documentos");
            let id_tabela = $(this).data("id_tabela");
            let pasta = $(this).data("pasta");
            
            let arquivo = $('input[name=' + nome_input + ']')[0].files[0];

            if (!validarAnexo(arquivo, ['jpg', 'jpeg', 'png', 'pdf'])) {
                return false;
            }

            if (arquivo.size > 2048000) {
                alert("O arquivo deve ter no máximo 2Mb");
                return false;
            }

            let formData = new FormData();

            formData.append("cpf_ou_cnpj", $("#cpf_ou_cnpj").val());
            formData.append("arquivo", arquivo);
            formData.append("descricao", descricao);
            formData.append("processo", processo);
            formData.append("nome_sessao", nome_sessao);
            formData.append("pasta", pasta);
            formData.append("nome_tabela", "iptu_pendencias_documentos");

            $.ajax({
                url: "{{ route('salvar_documento_pendencia_iptu') }}",
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {

                    $("input[name=" + nome_input + "]").val("");

                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });


        $("body").on("click", ".btn-excluir-documento", function(e) {
            e.preventDefault();
            
            if (!confirm("Deseja realmente excluir?")) {
                return false;
            }

            let nome_arquivo = $(this).data("nome_arquivo");
            let nome_sessao = $(this).data("nome_sessao");
            let id_tabela = $(this).data("id_tabela");
            let nome_tabela = "iptu_pendencias_documentos";
            let pasta = $(this).data("pasta");
            let conteudo_documentos = $(this).data("conteudo_documentos");

            $.ajax({
                url: "{{ route('excluir_documento_pendencia_iptu') }}",
                method: 'POST',
                data: {
                    "nome_arquivo": nome_arquivo,
                    "nome_sessao": nome_sessao,
                    "pasta": pasta,
                    "nome_tabela": nome_tabela,
                },
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    
                    load.fadeOut(200);

                    if (response.error) {
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("." + conteudo_documentos).html(response);
                    recalcularDataTable(id_tabela);
                }
            });
        });

        $("#salvar_documento").click(function(e){
            $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            let load = $(".ajax_load");
            let arquivo = $('#doc_processo').prop('files')[0];
            let processo = $("#processo").val();
            let chamado = $("#chamado").val();

            if (!arquivo) {
                alert("ATENÇÃO: Escolha um arquivo para enviar.");
                return false;
            }

            let formData = new FormData();

            if (processo == '0') {
                processo = $("#outro").val();
            }

            formData.append("arquivo", arquivo);
            formData.append("processo", processo);
            formData.append("chamado", chamado);

            let url = "{{ route('enviar_doc_divida') }}";

            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    load.fadeIn(200).css("display", "flex");
                },
                success(response) {
                    load.fadeOut(200);
                    if (response.error) {
                        load.fadeOut(200);
                        alert("ATENÇÃO: " + response.error);
                        return false;
                    }

                    $("#mgs").html(response);
                }
            });
        });
    });
</script>
@endsection
