
<div style="padding: 20px !important">
    <h2>Olá {{ $name }},</h2>

    <p>Você está recebendo este e-mail porque recebemos um pedido de redefinição de senha para sua conta.</p>

    @component('mail::button', ['url' => $url])
        Redefinir Senha
    @endcomponent
</div>
