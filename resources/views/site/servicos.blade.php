@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    {{-- @include('modals.modal_footer') --}}
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-servicos">
                @if (empty(session('tipoUsuario')) || session('tipoUsuario') == "EMPRESA"  || session('tipoUsuario') == "EMPRESA NÃO ESTABELECIDA")
                    <div class="card-header card-header-title">{{ $guia }}</div>
                @else
                    <div class="card-header card-header-title">{{ session('tipoUsuario') }}</div>
                @endif

                <div class="card-body">
                    <div id="accordion">
                        @inject('servicos', 'App\Services\ServicosService')
                        @foreach ($assuntos as $assunto)
                            <div class="card mb-3">
                                <a data-toggle="collapse" href="#collapse{{$loop->index}}" class="text-secondary no-underline">
                                    <div class="card-header">
                                        <i class="fa fa-arrow-down arrow-red"></i> {{$assunto->descricao}}
                                    </div>
                                </a>

                                <div id="collapse{{$loop->index}}" class="collapse shadow-lg {{ (str_slug($assunto->descricao) == $aba ? 'show' : '') }}" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>
                                            Escolha o seu <strong>Serviço</strong>:
                                        </p>

                                        @if($assunto->descricao == "CERTIDÕES")
                                            <div class="text-right">
                                                <a href="{{ route('certidaoAutenticacao') }}" class="btn btn-primary">
                                                    Autenticar Certidão
                                                <a>
                                            </div>
                                            <br>
                                            <br>
                                        @endif

                                        @foreach ($servicos->servicos($assunto->id) as $servico)
                                        @if( 
                                            (
                                                ( $guia == "CIDADÃO" && $servico->cidadao == 1) ||
                                                ( $guia == "EMPRESA" && $servico->empresa == 1)
                                            ) &&
                                            !(
                                                ($servico->servico=="Cadastrar / Atualizar CGM" && Auth::user()) ||
                                                ($servico->servico=="Cadastro do Cidadão" && !Auth::user())  ||
                                                ($servico->servico=="Cadastro Empresarial" && !Auth::user()) 
                                            )
                                        )
                                                <div class="alert alert-primary pt-4">

                                                    @if($servico->rota != "itbionline")

                                                        @if ($servico->servico == "IPTU")
                                                            <a href="{{ url($servico->rota) }}" class="btn btn-outline-primary w-100" target="_blank">
                                                                <b>{{ $servico->servico }}</b>
                                                            </a>
                                                        @else
                                                            <a href="{{ url($servico->rota) }}" class="btn btn-outline-primary w-100">
                                                                <b>{{ $servico->servico }}</b>
                                                            </a>
                                                        @endif
                                                    @else
                                                        @auth
                                                            {{-- <a href="#abrirITBI" data-toggle="modal" class="btn btn-outline-primary w-100"> --}}
                                                            <a href="#" id="btnAbrirITBI" data-toggle="modal" class="btn btn-outline-primary w-100">
                                                                <b>{{ $servico->servico }}</b>
                                                            </a>
                                                        @else
                                                            <a href="http://itbionline.marica.rj.gov.br/" class="btn btn-outline-primary w-100" target="_blank">
                                                                <b>{{ $servico->servico }}</b>
                                                            </a>
                                                        @endauth
                                                    @endif

                                                    @if($servico->descricao != "")
                                                        <p class="mt-3 mb-4 ml-4">
                                                            {{$servico->descricao}}
                                                        </p>
                                                    @endif
                                                </div>
                                            @endif
                                        @endforeach


                                    </div>
                                </div>
                            </div>
                            
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('post-script')
<script>
    $("#btnAbrirITBI").click(function(){
        $(".modal-title").html("ITBI OnLine");

        $(".modal-body").html("<p>Para solicitar o lançamento do ITBI, siga os passos abaixo:</p><p>1. Faça o download do FORMULÁRIO DE SOLICITAÇÃO DE ITBI, preencha preferencialmente de forma eletrônica e anexe-o na solicitação do processo.<br></p><p class='text-center'><a href='{{  url('fileDownload/1') }}' class='btn btn-primary'><i class='icon-download-alt'></i> FORMULÁRIO DE SOLICITAÇÃO DE ITBI</a></p><p>&nbsp;</p><p>2. Veja as observações e os documentos necessários para a solicitação do ITBI <br></p><p class='text-center'><a href='{{  url('fileDownload/2') }}' class='btn btn-primary'><i class='icon-download-alt'></i> DOCUMENTOS PARA SOLICITAÇÃO DE ITBI</a></p><p>&nbsp;</p>");
        $(".modal-footer").html("<button data-dismiss='modal' class='btn btn-danger' type='button'>Cancelar</button><a href='{{ route('itbionline') }}'><button id='btn_contato' class='btn btn-primary' type='button'>Continuar</button></a>");

        $("#footer-modal").modal('show');
    });
</script>
@endsection
