@extends('layouts.tema_principal')

@section('content')
    <div class="container-fluid">

        <div class="alert alert-info">
            <div class="row">
                <div class="col-lg-12 mt-2">
                    <h3><i class="fa fa-exclamation-triangle"></i> Exigência</h3>
                    <hr/>
                </div>

                <input type="hidden" name="id_chamado" value="" />

                <div class="col-lg-4">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="exigencia">Exigência:</label>
                            <textarea name="exigencia"
                                class="form-control form-control-sm"
                                id="exigencia"
                                rows="8" cols="5" maxlength="255"
                                readonly>Texto com a exigência...</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="cumprimento_exigencia">Cumprimento de exigência:</label>
                            <textarea name="cumprimento_exigencia"
                                class="form-control form-control-sm"
                                id="cumprimento_exigencia"
                                rows="8" cols="5" maxlength="255"></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="descricao_documento">Descrição do Documento:</label>
                            <input type="text" name="descricao_documento"
                                id="descricao_documento"
                                maxlength="40"
                                class="form-control form-control-sm">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <label for="documento_exigencia" class="d-block"><i class="fa fa-paperclip"></i> Anexar Arquivos:</label>
                            <input type="file" name="documento_exigencia"
                                id="documento_exigencia" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-lg-12 form-group">
                            <button class="btn btn-primary btn-enviar-documento">
                                <i class="fa fa-plus"></i> Adicionar Documento
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

                    <div class="conteudo_documentos_exigencias">
                        @include('imovel.tramitacao_imoveis.exigencias.tabela_documentos')
                    </div>
                </div>

                <div class="col-lg-12 mt-3">
                    <button class="btn btn-success btn-enviar-exigencia">Enviar Exigência <i class="fa fa-send"></i></button>
                </div>
            </div>
        </div>

    </div>
@endsection
