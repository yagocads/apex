<?php

namespace App\Http\Controllers\Empresa;

// use App\Http\Requests\CadastroCgmRequest;
use Illuminate\Http\Request;
use GoogleRecaptchaToAnyForm\GoogleRecaptcha;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Support\LoginSupport;
use App\Support\EmpresaSupport;
use App\Support\ImovelSupport;



class EmpresaController extends Controller
{   
    public function __construct(){
        $this->middleware(['auth', 'verified']);
    }

    public function legalizacaoMEI(){

        $atividadesCNAE = DB::connection('lecom')
        ->table('le_cnae')
        ->orderBy('atividade', 'asc')
        ->get();

        return view('empresa.mei.legalizacao', [
            "estados" => getEstados(),
            "ativavdesCnae" => $atividadesCNAE,
        ]);
    }

    public function salvarDocumentoServidor(Request $request){
        $nome_original = $request->arquivo->getClientOriginalName();
        $extensao = $request->arquivo->extension();

        $nome_arquivo = remove_character_document(Auth::user()->cpf) . "_" . $request->descricao . "_" . uniqid() . "." . $extensao;
        $nome_arquivo = remove_accentuation(str_replace(" ", "_", $nome_arquivo));
        
        $upload = $request->arquivo->storeAs('mei', $nome_arquivo, 'ftp');

        if ($upload) {

            session()->push($request->nome_sessao, (object) [
                "descricao" => $request->descricao,
                "nome_original" => $request->arquivo->getClientOriginalName(),
                "nome_novo" => $nome_arquivo
            ]);

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }
            
            return view('empresa.mei.'. $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function salvarDocumentoPendencia(Request $request){
        $nome_original = $request->arquivo->getClientOriginalName();
        $extensao = $request->arquivo->extension();

        $nome_arquivo = remove_character_document(Auth::user()->cpf) . "_" . $request->descricao . "_" . uniqid() . "." . $extensao;
        $nome_arquivo = remove_accentuation(str_replace(" ", "_", $nome_arquivo));
        
        $upload = $request->arquivo->storeAs('mei_pendencia', $nome_arquivo, 'ftp');

        if ($upload) {

            session()->push($request->nome_sessao, (object) [
                "descricao" => $request->descricao,
                "nome_original" => $request->arquivo->getClientOriginalName(),
                "nome_novo" => $nome_arquivo
            ]);

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }
            
            return view('site.'. $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removerDocumentoSocio(Request $request){

        if (!$this->removerDocumentoServidor($request->nome_arquivoRG)) {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }
        if (!$this->removerDocumentoServidor($request->nome_arquivoCPF)) {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }
        if (!$this->removerDocumentoServidor($request->nome_arquivoCE)) {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }

        $session = session()->get("Socios");

        if (!empty($session)) {

            foreach($session as $key => $dados) {
                if (in_array($request->nome_arquivoRG, (array) $dados)) {

                    unset($session[$key]);
                    session()->forget("Socios");
                    session()->put("Socios", $session);
                }
            }
        }

        return view('empresa.mei.socios.tabela_documentos');
    }

    public function excluir_dados_cnae(Request $request){
        
        $session = session()->get("dados_cnae");

        if (!empty($session)) {
            foreach($session as $key => $dados) {
                if (in_array($request->codigo, (array) $dados)) {
                    unset($session[$key]);
                    session()->forget("dados_cnae");
                    session()->put("dados_cnae", $session);
                }
            }
        }

        return view('empresa.mei.cnae.tabela_documentos');
    }

    public function removerDocumentoPendencia(Request $request){
        if ($this->removerArquivoPendencia($request->nome_arquivo)) {
        
            $session = session()->get($request->nome_sessao);

            if (!empty($session)) {

                foreach($session as $key => $dados) {

                    if (in_array($request->nome_arquivo, (array) $dados)) {
                        unset($session[$key]);
                        session()->forget($request->nome_sessao);
                        session()->put($request->nome_sessao, $session);
                    }
                }
            }

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }

            return view('site.'. $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removerDocumentoSessao(Request $request){
        if ($this->removerDocumentoServidor($request->nome_arquivo)) {
        
            $session = session()->get($request->nome_sessao);

            if (!empty($session)) {

                foreach($session as $key => $dados) {

                    if (in_array($request->nome_arquivo, (array) $dados)) {
                        unset($session[$key]);
                        session()->forget($request->nome_sessao);
                        session()->put($request->nome_sessao, $session);
                    }
                }
            }

            if (empty($request->nome_tabela)) {
                $nome_tabela = "tabela_documentos";
            } else {
                $nome_tabela = $request->nome_tabela;
            }

            return view('empresa.mei.'. $request->pasta . '.' . $nome_tabela);
        } else {
            return ['error' => 'Não foi possível remover o documento, procure a administração para resolver o problema.'];
        }
    }

    public function incluirCNAE(Request $request){

        $cnaePrincipal = json_decode($request->principal);
        $cnaeSecundarias = json_decode($request->secundarias);

        $codigo = str_replace(".", "", $cnaePrincipal[0]->code);
        $codArray = explode("-", $codigo);
        $codigo = $codArray[0]."-".$codArray[1]."/".$codArray[2];

        $respostaCNAE = DB::connection('lecom')
        ->table('le_cnae')
        ->where('codigo', '=', $codigo)
        ->first();

        session()->push("dados_cnae", (object) [
            "tipo" => "ATIVIDADE PRINCIPAL",
            "atividade" => $respostaCNAE->atividade,
            "codigo" => $respostaCNAE->codigo,
            "grupo" =>$respostaCNAE->grupo
        ]);

        foreach ($cnaeSecundarias as $key => $cnaeSec) {

            $codigo = str_replace(".", "", $cnaeSec->code);
            $codArray = explode("-", $codigo);
            $codigo = $codArray[0]."-".$codArray[1]."/".$codArray[2];

            $respostaCNAE = DB::connection('lecom')
            ->table('le_cnae')
            ->where('codigo', '=', $codigo)
            ->first();
    
            session()->push("dados_cnae", (object) [
                "tipo" => "ATIVIDADE SECUNDARIA",
                "atividade" => $respostaCNAE->atividade,
                "codigo" => $respostaCNAE->codigo,
                "grupo" =>$respostaCNAE->grupo
            ]);
        }

        return view('empresa.mei.cnae.tabela_documentos');

    }

    private function removerArquivoPendencia(string $arquivo): bool
    {
        return Storage::disk('ftp')->delete('mei_pendencia/'.$arquivo);
    }

    private function removerDocumentoServidor(string $arquivo): bool
    {
        return Storage::disk('ftp')->delete('mei/'.$arquivo);
    }

    public function validaCpfECidade(Request $request){


        $session = session()->get("Socios");

        if (!empty($session)) {

            foreach($session as $key => $dados) {
                if (in_array($request->cpf, (array) $dados)) {
                    return json_encode(["cpfIncluido"=>true]);
                break;
                }
            }
        }
                    
        $cpfUsuario = remove_character_document($request->cpf);

        $CgmSupport = new ImovelSupport();

        $resposta = $CgmSupport->getDadosCGM($cpfUsuario);
        
        if ($resposta->iStatus == 1){

            // Encontrou usuário no e-cidade
            $respostaEndereco = $CgmSupport->getEnderecoCGM($cpfUsuario,"P");
            return json_encode(["cpfIncluido"=>false, "cpfCadastrado" => true, "dados" => json_decode(convert_accentuation(json_encode($resposta))), "endereco" => json_decode(convert_accentuation(json_encode($respostaEndereco)))]);
        }
        else{
            return json_encode(["cpfIncluido"=>false, "cpfCadastrado"=>false]);
        }
    }

    public function salvar_dados_socio(Request $request){

        $nome_originalRG = $request->arquivoRG->getClientOriginalName();
        $extensaoRG = $request->arquivoRG->extension();

        $nome_arquivoRG = remove_character_document(Auth::user()->cpf) . "_". remove_character_document($request->cpf_socio) . "_RG_Socio_" . uniqid() . "." . $extensaoRG;
        $nome_arquivoRG = remove_accentuation(str_replace(" ", "_", $nome_arquivoRG));
        
        $upload = $request->arquivoRG->storeAs('mei', $nome_arquivoRG, 'ftp');

        if (!$upload) {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }

        $nome_originalCPF = $request->arquivoCPF->getClientOriginalName();
        $extensaoCPF = $request->arquivoCPF->extension();

        $nome_arquivoCPF = remove_character_document(Auth::user()->cpf) . "_". remove_character_document($request->cpf_socio) . "_CPF_Socio_" . uniqid() . "." . $extensaoCPF;
        $nome_arquivoCPF = remove_accentuation(str_replace(" ", "_", $nome_arquivoCPF));
        
        $upload = $request->arquivoCPF->storeAs('mei', $nome_arquivoCPF, 'ftp');

        if (!$upload) {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }

        $nome_originalCE = $request->arquivoCE->getClientOriginalName();
        $extensaoCE = $request->arquivoCE->extension();

        $nome_arquivoCE = remove_character_document(Auth::user()->cpf) . "_". remove_character_document($request->cpf_socio) . "_CE_Socio_" . uniqid() . "." . $extensaoCE;
        $nome_arquivoCE = remove_accentuation(str_replace(" ", "_", $nome_arquivoCE));
        
        $upload = $request->arquivoCE->storeAs('mei', $nome_arquivoCE, 'ftp');

        if (!$upload) {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }

        session()->push("Socios", (object) [
            "cpf" => $request->cpf_socio,
            "nome" => $request->nome_socio,
            "dataNasc" => implode('/', array_reverse(explode('-', $request->data_nascimento))),
            "email" =>$request->email_socio,
            "mae" =>$request->mae_socio,
            "percentual" =>$request->percentual_socio,
            "cep" =>$request->cep_socios,
            "endereco" =>$request->endereco_socios,
            "numero" =>$request->numero_socios,
            "complemento" =>$request->complemento_socios,
            "bairro" =>$request->bairro_socios,
            "municipio" =>$request->municipio_socios,
            "estado" =>$request->estado_socios,
            "nomeRGAntes" =>$nome_originalRG,
            "nomeRGDepois" =>$nome_arquivoRG,
            "nomeCPFAntes" =>$nome_originalCPF,
            "nomeCPFDepois" =>$nome_arquivoCPF,
            "nomeCEAntes" =>$nome_originalCE,
            "nomeCEDepois" =>$nome_arquivoCE,
        ]);

        return view('empresa.mei.socios.tabela_documentos');
    }

    public function verificarEnvioDocumentos(Request $request) {
        $documentos_em_falta = [];


        foreach($request->documentos_necessarios as $documento) {
            $achei = false;
            if (session()->has($documento['nome_sessao'])){
                foreach(session($documento['nome_sessao']) as $documento_sessao) {
                    if($documento_sessao->descricao == $documento["data_descricao"]){
                        $achei = true;
                        break;
                    }
                }
            }
            if(!$achei){
                $documentos_em_falta[] = [
                    "descricao" => $documento['descricao']
                ];
            }
        }

        return response()->json(["documentos_em_falta" => $documentos_em_falta]);
    }

    public function formPendenciasMeiSalvar(Request $request) {

        $processoLecom = DB::connection('lecom')
        ->table('v_consulta_legal_empresa')
        ->where('chamado', $request->chamado)
        ->first();

        // dd($processoLecom, $request);

        DB::beginTransaction();

        try{
            $idPendenciaMei = DB::connection('integracao')
            ->table('lecom_legaliza_mei_recursos')
            ->insertGetId([
                'cnpj' => "",
                'processo' => $request->chamado,
                'fase' => $processoLecom->Etapa,
                'ciclo' => $processoLecom->Ciclo,
                'recurso' => $request->justificativa_recurso,
                'data' => date("Y-m-d H:i:s"),
                ]
            );

            $this->enviarPendenciasLecom(
                $request,
                $idPendenciaMei
            );

            DB::commit();

            session()->forget("pendencias");

            $rota = "acompanhamento";
            $mensagem  = '<p><b>Processo:</b> '. $request->chamado. '</p>';
            $mensagem .= '<p><b>ATENÇÃO:</b> Para consultar o andamento do processo, acesse a página de consulta no link abaixo.</p>';
            $mensagem .= '<p><a href="'.route($rota).'">Clique aqui para consultar o processo</a></p>';

            return view('messages.confirmacao_MEI', [
                "icone" => "fa-home",
                "titulo" => "Legalização de Empresa - MEI",
                "mensagem" => $mensagem,
                "rota" => "acompanhamento",
                "retorno" => "",
            ]);
           
        }
        catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => "", 
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "EmpresaController",
                    'rotina'    => "formPendenciasMeiSalvar",
                    'erro'      => $th, 
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Legalização de Empresa - MEI",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "legalizacaoMEI",
                "retorno" => "",
                "codigo" => "MEI_002",
                ]);
        }

    }

    public function formLegalizacaoMeiSalvar(Request $request) {

        if(strtoupper($request->possuiCNPJ) == "SIM"){
            $totalCadastros = DB::connection('integracao')
            ->table('lecom_legaliza_mei')
            ->where('cnpj', '=', $request->cnpj_empresa)
            ->count();
    
            if($totalCadastros > 0){
                return view('errors.erro_geral', [
                    "icone" => "fa-home",
                    "titulo" => "Legalização de Empresa - MEI",
                    "mensagem" => '<p>Já existe uma solicitação para este CNPJ',
                    "rota" => "legalizacaoMEI",
                    "retorno" => "",
                    "codigo" => "MEI_001",
                ]);
            }
        }

        DB::beginTransaction();

        try{
            $idMei = DB::connection('integracao')
            ->table('lecom_legaliza_mei')
            ->insertGetId(
                array_merge(
                    $this->dadosEmpresa($request),
                    $this->dadosSolicitante($request),
                    $this->dadosEnderecoEmpresa($request),
                    $this->dadosInformacoesContato($request)
                )
            );

            $this->gravarDadosCnae(
                $request->cnpj_empresa,
                $idMei
            );

            $this->gravarDadosSocios(
                $request->cnpj_empresa,
                $idMei
            );

            $this->enviarDocumentosParaLecom(
                $request->cnpj_empresa,
                $idMei
            );

            DB::commit();

            $this->removerDocumentosSessao();

            $rota = "acompanhamento";
            $mensagem  = '<p><b>Identificação:</b> '. $request->nome_solicitante. '</p>';
            $mensagem .= '<p><b>CPF/CNPJ:</b> ' . $request->cpf_solicitante. '</p>';
            $mensagem .= '<p><b>ATENÇÃO:</b> Para consultar o andamento do processo, acesse a página de consulta no link abaixo.</p>';
            $mensagem .= '<p><a href="'.route($rota).'">Clique aqui para consultar o processo</a></p>';

            return view('messages.confirmacao_MEI', [
                "icone" => "fa-home",
                "titulo" => "Legalização de Empresa - MEI",
                "mensagem" => $mensagem,
                "rota" => "legalizacaoMEI",
                "retorno" => "",
            ]);
           
        }
        catch (\Throwable $th) {
            // dd($th);
            DB::rollBack();

            $r = DB::table('log_erro')->insert(
                [
                    'cpf_cnpj'  => $request->cnpj, 
                    'data'      => date('Y-m-d H:i:s'),
                    'controler'  => "EmpresaController",
                    'rotina'    => "formLegalizacaoMeiSalvar",
                    'erro'      => $th, 
                ]
            );

            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Legalização de Empresa - MEI",
                "mensagem" => '<p>Não foi possível gravar as suas informações.  Entre em contato com o Administrador',
                "rota" => "legalizacaoMEI",
                "retorno" => "",
                "codigo" => "MEI_002",
                ]);
        }
    }

    private function removerDocumentosSessao() {
        session()->forget("documentos_empresa");
        session()->forget("documento_endereco");
        session()->forget("documentos_solicitante");
        session()->forget("Socios");
        session()->forget("dados_cnae");
    }

    private function enviarPendenciasLecom($processo, $idMei){    
        $sessionDocumentos = [
            'pendencias',
        ];

        foreach($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach(session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_legaliza_mei_recursos_documentos')
                        ->insert([
                            'id_recurso' => $idMei,
                            'cnpj' => "",
                            'documento' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    private function enviarDocumentosParaLecom($cnpj, $idMei){    
        $sessionDocumentos = [
            'documentos_empresa',
            'documento_endereco',
            'documentos_solicitante',
        ];

        foreach($sessionDocumentos as $nomeDocumento) {
            if (session()->has($nomeDocumento)) {
                foreach(session($nomeDocumento) as $dados) {
                    DB::connection('integracao')
                        ->table('lecom_legaliza_mei_documentos')
                        ->insert([
                            'id_Mei' => $idMei,
                            'cnpj' => $cnpj,
                            'documento' => $dados->nome_novo,
                            'descricao' => $dados->descricao
                        ]);
                }
            }
        }
    }

    
    private function dadosEmpresa(Request $request){
        return [
            'tem_cnpj' => $request->possuiCNPJ,
            'cnpj' => $request->cnpj_empresa,
            'tipo_inscricao' => $request->tipo_inscricao,
            'simples_nacional' => $request->simples_nacional,
            'horario_funcionamento' => $request->horario_funcionamento,
            'outros_horarios' => $request->outros_horarios,
            'anexo_residencia' => $request->anexo_residencia,
            'porte_empresa' => $request->porte_empresa,
            'tipo_uso' => $request->tipo_uso,
            'outra_empresa' => $request->outra_empresa,
            'atendimento_publico' => $request->atendimento_publico,
            'autorizacao_proprietario' => $request->autorizacao,
            'separacao_fisica' => $request->separacao_fisica,
            'tem_protocolo_viabilidade' => $request->possui_protocolo_viabilidade,
            'protocolo_viabilidade' => $request->protocolo_viabilidade,
            'tem_recurso_viabilidade' => $request->possui_recurso_viabilidade,
            'recurso_viabilidade' => $request->recurso_viabilidade,
            'empresa_ponto_referencia' => $request->pr_empresa,
            'inscricao_imobiliaria' => $request->ii_empresa,

            'razao_social' => $request->razao_social,
            'nome_fantasia' => $request->nome_fantasia,
            'natureza_juridica' => $request->fator_tipo_juridico,

            'data_cadastro' => date("Y-m-d H:i:s"),
        ];
    }

    private function dadosSolicitante(Request $request){
        return [
            'solicitante_nome' => $request->nome_solicitante,
            'solicitante_cpf' => $request->cpf_solicitante,
            'solicitante_email' => $request->email_solicitante,
            'solicitante_rg' => $request->identidade_solicitante,
            'solicitante_rg_orgao_expedidor' => $request->orgao_emissor_solicitante,
            'solicitante_rg_data_expedicao' => $request->data_emissao_solicitante,
            'solicitante_cep' => $request->cep_solicitante,
            'solicitante_endereco' => $request->endereco_solicitante,
            'solicitante_numero' => $request->numero_solicitante,
            'solicitante_complemento' => $request->complemento_solicitante,
            'solicitante_bairro' => $request->bairro_solicitante,
            'solicitante_cidade' => $request->municipio_solicitante,
            'solicitante_uf' => $request->estado_solicitante,
        ];
    }

    private function dadosEnderecoEmpresa(Request $request){
        return [
            'empresa_cep' => str_replace(".", "", $request->cep_empresa),
            'empresa_endereco' => $request->endereco_empresa,
            'empresa_numero' => $request->numero_empresa,
            'empresa_complemento' => $request->complemento_empresa,
            'empresa_bairro' => $request->bairro_empresa,
            'empresa_cidade' => $request->municipio_empresa,
            'empresa_uf' => $request->estado_empresa,
            'empresa_iptu' => $request->iptu_empresa,
        ];
    }

    private function dadosInformacoesContato(Request $request){
        return [
            'telefone_residencial' => $request->residencial,
            'telefone_comercial' => substr($request->comercial, 0, 14),
            'celular' => $request->celular,
        ];
    }

    public function verificarDocumentosPendencias(){
        
        $session = session()->get("pendencias");

        if (!empty($session)) {
            return json_encode([ "documentosEnciados" => true]);
        }
        else{
            return json_encode([ "documentosEnciados" => false]);
        }
    }

    public function verificar_atividade_cnae(){
        
        $session = session()->get("dados_cnae");

        if (!empty($session)) {
            return json_encode([ "atividade" => true]);
        }
        else{
            return json_encode([ "atividade" => false]);
        }
    }

    public function verificar_socios_cadastrados(){
        
        $session = session()->get("Socios");

        if (!empty($session)) {
            return json_encode([ "cadastrados" => true]);
        }
        else{
            return json_encode([ "cadastrados" => false]);
        }
    }

    public function gravarDadosCnae($cnpj, $idMei){

        $session = session()->get("dados_cnae");

        if (!empty($session)) {

            foreach($session as $key => $dados) {
                DB::connection('integracao')
                ->table('lecom_legaliza_mei_cnae')
                ->insert([
                    'id_mei' => $idMei,
                    'cnpj' => $cnpj,
                    'tipo' => $dados->tipo,
                    'atividade' => $dados->atividade,
                    'codigo' => $dados->codigo,
                    'grupo' => $dados->grupo,
                ]);
            }
        }

    }

    public function gravarDadosSocios($cnpj, $idMei){

        $session = session()->get("Socios");

        if (!empty($session)) {

            foreach($session as $key => $dados) {
                
                DB::connection('integracao')
                ->table('lecom_legaliza_mei_socios')
                ->insert([
                    'id_mei' => $idMei,
                    'cnpj' => $cnpj,
                    'nome' => $dados->nome,
                    'data_nascimento' => implode('-', array_reverse(explode('/',  $dados->dataNasc))),
                    'email' => $dados->email,
                    'cpf' => $dados->cpf,
                    'nome_mae' => $dados->mae,
                    'percentual' => str_replace(",", ".", $dados->percentual),
                    'cep' => $dados->cep,
                    'endereco' => $dados->endereco,
                    'numero' => $dados->numero,
                    'complemento' => $dados->complemento,
                    'bairro' => $dados->bairro,
                    'cidade' => $dados->municipio,
                    'uf' => $dados->estado,
                    'tipo' => "Responsávei MEI",
                ]);

                DB::connection('integracao')
                ->table('lecom_legaliza_mei_socios_documentos')
                ->insert([
                    'id_mei' => $idMei,
                    'cnpj' => $cnpj,
                    'cpf' => $dados->cpf,
                    'descricao' => "RG Sócio",
                    'documento' => $dados->nomeRGDepois,
                ]);

                DB::connection('integracao')
                ->table('lecom_legaliza_mei_socios_documentos')
                ->insert([
                    'id_mei' => $idMei,
                    'cnpj' => $cnpj,
                    'cpf' => $dados->cpf,
                    'descricao' => "CPF Sócio",
                    'documento' => $dados->nomeCPFDepois,
                ]);

                DB::connection('integracao')
                ->table('lecom_legaliza_mei_socios_documentos')
                ->insert([
                    'id_mei' => $idMei,
                    'cnpj' => $cnpj,
                    'cpf' => $dados->cpf,
                    'descricao' => "CE Sócio",
                    'documento' => $dados->nomeCEDepois,
                ]);


            }
        }

    }

    public function salvar_dados_cnae(Request $request){

        $session = session()->get("dados_cnae");

        if (!empty($session)) {
            if($request->tipo_cnae == "Principal"){
                foreach($session as $key => $dados) {
                    if (in_array($request->tipo_cnae, (array) $dados)) {
                        return ["principalIncluido" => true];
                    }
                }
            }

            foreach($session as $key => $dados) {
                if (in_array($request->codigo_cnae, (array) $dados)) {
                    return ["codigoIncluido"=>true];
                }
            }
        }

        session()->push("dados_cnae", (object) [
            "tipo" => $request->tipo_cnae,
            "atividade" => $request->atividade_cnae,
            "codigo" =>$request->codigo_cnae,
            "grupo" =>$request->grupo_cnae,
        ]);

        return view('empresa.mei.cnae.tabela_documentos');
    }

    public function validaCnpjECidade(Request $request){
       
        $cnpjEmpresa = remove_character_document($request->cnpj);

        $this->removerDocumentosSessao();

        $respostaCadastro = DB::connection('integracao')
        ->table('lecom_legaliza_mei')
        ->where('cnpj', '=', $request->cnpj)
        ->get();

        if(count($respostaCadastro) > 0){
            // Empresa já cadastrada no portal
            return json_encode(["empresaCadastrada"=>true, "sistema"=>"portal"]);            
        }
            
        $loginSupport = new LoginSupport();

        $resposta = $loginSupport->login($cnpjEmpresa);

        if ($resposta->iStatus == 1){
            // Encontrou a empresa no e-cidade
            return json_encode(["empresaCadastrada"=>true, "sistema"=>"e-cidade"]);
        }

        $empresaSupport = new EmpresaSupport();

        $dadosEmpresa = $empresaSupport->empresa($cnpjEmpresa);

        return json_encode(["empresaCadastrada"=>false, "dados" => $dadosEmpresa]);

    }

    public function limpar_arquivos_sessao(Request $request){

        $session = session()->get($request->sessao);

        $apagar = filter_var($request->apagarArquivos, FILTER_VALIDATE_BOOLEAN);

        if ($apagar) {
            if (!empty($session)) {
                foreach($session as $key => $dados) {
                    if($request->sessao == "Socios"){
                        $this->removerDocumentoServidor($dados->nomeRGDepois);
                        $this->removerDocumentoServidor($dados->nomeCPFDepois);
                        $this->removerDocumentoServidor($dados->nomeCEDepois);
                    }
                    else{
                        $this->removerDocumentoServidor($dados->nome_novo);
                    }
                    unset($session[$key]);
                }
            }
        }

        session()->forget($request->sessao);
        return view('empresa.mei.'. $request->pasta . '.' . $request->tabela);
    }

    public function exigenciasMei($chamado){

        $chamadoLecom = DB::connection('lecom')
        ->table('v_consulta_legal_empresa')
        ->where('chamado', '=', $chamado)
        ->first();

        if (!isset($chamadoLecom)){
            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Legalização de Empresa - MEI",
                "mensagem" => '<p>Processo não encontrado. Confirme as informações',
                "rota" => "acompanhamento",
                "retorno" => "",
                "codigo" => "MEI_004",
                ]); 
        }

        $totalCadastros = DB::connection('integracao')
        ->table('lecom_legaliza_mei_recursos')
        ->where('processo', '=', $chamado)
        ->where('fase', '=', $chamadoLecom->Etapa)
        ->where('ciclo', '=', $chamadoLecom->Ciclo)
        ->count();

        if ($totalCadastros == 0){
            return view('site.acompanhamento.legalizacao_empresa_pendencias',  ['chamado' => $chamado, "motivo" => $chamadoLecom->motivo ]);
        }
        else{
            return view('errors.erro_geral', [
                "icone" => "fa-home",
                "titulo" => "Legalização de Empresa - MEI",
                "mensagem" => '<p>Já existe uma solicitação para o saneamento das pendências.  Aguarde o andamento do processo.',
                "rota" => "acompanhamento",
                "retorno" => "",
                "codigo" => "MEI_003",
                ]);   
        }
    }
}