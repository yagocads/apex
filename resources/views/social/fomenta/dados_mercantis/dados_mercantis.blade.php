<div class="row">
    <div class="col-lg-6">
        <h5><i class="fa fa-home mt-3 mb-3"></i> Dados da Empresa:</h5>

        <div class="form-row">
            <div class="col-lg-6 form-group">
                <label for="cnpj">CNPJ:</label>
                <input type="text" name="cnpj" id="cnpj" 
                    class="form-control form-control-sm mask-cnpj {{ ($errors->has('cnpj') ? 'is-invalid' : '') }}" 
                    value="{{ $cnpj }}" readonly />

                    @if ($errors->has('cnpj'))
                        <div class="invalid-feedback">
                            {{ $errors->first('cnpj') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">Nome Empresarial:</label>
                <input type="text" name="razao_social" id="razao_social" 
                    class="form-control form-control-sm {{ ($errors->has('razao_social') ? 'is-invalid' : '') }}" 
                    value="{{ $empresa->nome }}" maxlength="100" readonly/>

                    @if ($errors->has('razao_social'))
                        <div class="invalid-feedback">
                            {{ $errors->first('razao_social') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="nome_fantasia">Nome Fantasia:</label>
                <input type="text" name="nome_fantasia" id="nome_fantasia" 
                    class="form-control form-control-sm {{ ($errors->has('nome_fantasia') ? 'is-invalid' : '') }}" 
                    value="{{ $empresa->fantasia }}" maxlength="100" 
                    @isset($empresa->fantasia)
                        @unless (empty($empresa->fantasia))
                            readonly
                        @endunless
                    @endisset
                    />

                    @if ($errors->has('nome_fantasia'))
                        <div class="invalid-feedback">
                            {{ $errors->first('nome_fantasia') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="natureza_juridica">Natureza Jurídica:</label>
                <input type="text" name="natureza_juridica" id="natureza_juridica" 
                    class="form-control form-control-sm {{ ($errors->has('natureza_juridica') ? 'is-invalid' : '') }}"
                    value="{{ $empresa->natureza_juridica }}" maxlength="40" readonly/>

                    @if ($errors->has('natureza_juridica'))
                        <div class="invalid-feedback">
                            {{ $errors->first('natureza_juridica') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="data_abertura_empresa">Data de Abertura:</label>
                <input type="text" name="data_abertura_empresa" id="data_abertura_empresa" 
                    class="form-control form-control-sm {{ ($errors->has('data_abertura_empresa') ? 'is-invalid' : '') }}" 
                    value="{{ $empresa->abertura }}" readonly/>

                    @if ($errors->has('data_abertura_empresa'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_abertura_empresa') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-6 form-group">
                <label for="data_inicio_atividade"> Data de Início das Atividades:</label>
                <input type="date" name="data_inicio_atividade" id="data_inicio_atividade" 
                    max="{{ date('Y-m-d') }}" 
                    class="form-control form-control-sm {{ ($errors->has('data_inicio_atividade') ? 'is-invalid' : '') }}" 
                    value="{{ old('data_inicio_atividade') }}"/>

                    @if ($errors->has('data_inicio_atividade'))
                        <div class="invalid-feedback">
                            {{ $errors->first('data_inicio_atividade') }}
                        </div>
                    @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="cnae">CNAE:</label>
                <input type="text" name="cnae" id="cnae" 
                    class="form-control form-control-sm {{ ($errors->has('cnae') ? 'is-invalid' : '') }}"
                    value="{{ $empresa->atividade_principal[0]->code . ' - ' . $empresa->atividade_principal[0]->text }}" maxlength="150" readonly/>

                @if ($errors->has('cnae'))
                    <div class="invalid-feedback">
                        {{ $errors->first('cnae') }}
                    </div>
                @endif
            </div>    
            
            <div class="col-lg-12 form-group">
                <div class="alert alert-info">
                    <p>
                        Declaro que estou ciente e de acordo com a abertura de uma conta digital 
                        pré - paga em meu nome junto ao Banco Mumbuca, através da qual receberei 
                        o crédito, sem direito a resgate / saque, caso minha solicitação de crédito 
                        ao Programa Fomenta Maricá MEI Emergencial Covid-19 seja aprovada.
                    </p>

                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="confirmacao_mumbuca" 
                                class="form-check-input" value="sim" 
                                @if(old('confirmacao_mumbuca'))
                                    checked 
                                @endif
                                required >
                            Li e concordo.
                        </label>
                    </div>
                </div>

                @if ($errors->has('confirmacao_mumbuca'))
                    <div class="invalid-feedback">
                        {{ $errors->first('confirmacao_mumbuca') }}
                    </div>
                @endif
            </div>

            <div class="col-lg-12 form-group">
                <label for="agPreferencia">
                    Agência de preferência do banco Mumbuca:
                </label>
        
                <select id="agPreferencia" 
                class="form-control form-control-sm {{ ($errors->has('agPreferencia') ? 'is-invalid' : '') }}"
                name="agPreferencia">
                    <option value=""></option>
                    <option value="001" {{ old('agPreferencia')=="001"?"selected":"" }}>Centro</option>
                    <option value="002" {{ old('agPreferencia')=="002"?"selected":"" }}>Inoã</option>
                    <option value="003" {{ old('agPreferencia')=="003"?"selected":"" }}>Itaipuaçu</option>
                    <option value="004" {{ old('agPreferencia')=="004"?"selected":"" }}>Cordeirinho</option>
                </select>   
                
                @if ($errors->has('agPreferencia'))
                    <div class="invalid-feedback">
                        {{ $errors->first('agPreferencia') }}
                    </div>
                @endif
            </div> 

        </div>
    </div>
    
    <div class="col-lg-6">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

        <div class="form-row">
            <div class="col-lg-12">
                <div class="alert alert-info">
                    <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                </div>
            </div>
{{--             
            <div class="col-lg-6 form-group">
                <br>
                <label for="documento_identididade_socio"><i class="fa fa-paperclip"></i> Anexar Documento de Identidade:</label><br>
                <input type="file" name="documento_identididade_socio" 
                    id="documento_identididade_socio" />
            </div>

            <div class="col-lg-12 mb-3">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_identididade_socio" 
                    data-descricao="Documento de Identidade"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
            
            <div class="col-lg-12 form-group">
                <hr>
                <br>
                <label for="documento_cpf_socio"><i class="fa fa-paperclip"></i> Anexar CPF:</label><br>
                <input type="file" name="documento_cpf_socio" 
                    id="documento_cpf_socio" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_cpf_socio" 
                    data-descricao="CPF"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div> --}}

            <div class="col-lg-12 form-group">
                <label for="documento_comprovante_mei"><i class="fa fa-paperclip"></i> Anexar Comprovante do MEI (Certificado do MEI ou cartão de CNPJ):</label><br>
                <input type="file" name="documento_comprovante_mei" 
                    id="documento_comprovante_mei" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_comprovante_mei" 
                    data-descricao="Comprovante MEI"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>

            <div class="col-lg-12 form-group">
                <hr>
                <br>
                <div class="alert alert-info">
                    <p>
                        <i class="fa fa-volume-up"></i> Anexe os documentos que sejam prova inequívoca do exercício de sua atividade empresarial em Maricá sendo aconselhável que encaminhe mais de um dos documentos abaixo sugeridos:
                    </p>
                    a) Foto do estabelecimento, se houver;<br>
                    b) Foto de rede social ativa antes de março de 2020 informando o desempenho de atividade empreendedora;<br>
                    c) Foto de cartão de visitas/divulgação contendo a atividade e número de contato do solicitante;<br>
                    d) Foto de anúncio de vendas/ oferta do serviço;<br>
                    e) Declaração de tomador de serviço;<br>
                    f) Outros sites que informem acerca da atividade desempenhada próprio ou de terceiros;<br>
                    g) Nota fiscal de faturamento;<br>
                    h) Alvará de funcionamento do MEI dentro do prazo de validade<br>
                </div>
                <label for="documento_probatorio"><i class="fa fa-paperclip"></i> Anexar Documento Probatório da Atividade:</label><br>
                <input type="file" name="documento_probatorio" 
                    id="documento_probatorio" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_probatorio" 
                    data-descricao="Documento Probatório da Atividade"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>

            <div class="col-lg-12 form-group">
                <hr>
                <br>
                <label for="documento_foto_empreendimento"><i class="fa fa-paperclip"></i> Anexar Fotos do Empreendimento (com o empreendedor aparecendo na foto):</label><br>
                <input type="file" name="documento_foto_empreendimento" 
                    id="documento_foto_empreendimento" />
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary btn-enviar-documento"
                    data-nome_input="documento_foto_empreendimento" 
                    data-descricao="Foto do Empreendimento"
                    data-nome_sessao="documentos_dados_mercantis"
                    data-conteudo_documentos="conteudo_dados_mercantis"
                    data-id_tabela="tabela_dados_mercantis"
                    data-pasta="dados_mercantis">
                    Enviar Documento <i class="fa fa-send"></i>
                </button>
            </div>
        </div>    
    </div>  
</div>

<hr/>

<div class="row">
    <div class="col-lg-12 form-group">
        <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

        <div class="conteudo_dados_mercantis">
            @include('social.fomenta.dados_mercantis.tabela_documentos')
        </div>
    </div>
</div>