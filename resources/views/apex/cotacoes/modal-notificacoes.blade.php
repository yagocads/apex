<!-- The Modal -->
<div class="modal fade" id="modal-notificacoes">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">
                    {{__('Notificações')}}
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
