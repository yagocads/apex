<div class="form-row">
    <div class="col-lg-4 form-group">
        <label for="responsavel">Responsável pelo Preenchimento:</label>
        <select name="responsavel" id="responsavel" 
            class="form-control form-control-sm"
            @if (mb_strlen(remove_character_document($cpfOuCnpj)) == 11)
                @auth
                    readonly    
                @endauth
            @endif
            >

            @if (mb_strlen(remove_character_document($cpfOuCnpj)) == 11)
                <option value="1" {{ old('responsavel') == "1" ? 'selected' : '' }}
                >Próprio</option>
            @endif
            
            <option value="2" {{ old('responsavel') == "2" ? 'selected' : '' }}>Responsável Legal</option>
        </select>
    </div>
</div>

<div class="requerente_prosseguimento {{ mb_strlen(remove_character_document($cpfOuCnpj)) == 11 && (old('responsavel') == null || old('responsavel') == "1") ? 'd-none' : '' }}">
    <div class="row">
        <div class="col-lg-6">
            <h5><i class="fa fa-user mt-3 mb-3"></i> Dados do Requerente:</h5>

            <div class="form-row">
                <div class="col-lg-12 form-group">
                    <label for="nome_requerente">Nome Completo:</label>
                    <input type="text" name="nome_requerente" id="nome_requerente" 
                    class="form-control form-control-sm {{ ($errors->has('nome_requerente') ? 'is-invalid' : '') }}"
                        value="{{ old('nome_requerente') }}" maxlength="100" />

                        @if ($errors->has('nome_requerente'))
                            <div class="invalid-feedback">
                                {{ $errors->first('nome_requerente') }}
                            </div>
                        @endif
                </div>
            </div>

            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="identidade_requerente">Identidade:</label>
                    <input type="text" name="identidade_requerente" id="identidade_requerente" 
                        class="form-control form-control-sm {{ ($errors->has('identidade_requerente') ? 'is-invalid' : '') }}" 
                        value="{{ old('identidade_requerente') }}" maxlength="20" />

                        @if ($errors->has('identidade_requerente'))
                            <div class="invalid-feedback">
                                {{ $errors->first('identidade_requerente') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-4 form-group">
                    <label for="orgao_emissor_requerente">Órgão Emissor:</label>
                    <input type="text" name="orgao_emissor_requerente" id="orgao_emissor_requerente" 
                        class="form-control form-control-sm {{ ($errors->has('orgao_emissor_requerente') ? 'is-invalid' : '') }}" 
                        value="{{ old('orgao_emissor_requerente') }}" maxlength="45" />

                        @if ($errors->has('orgao_emissor_requerente'))
                            <div class="invalid-feedback">
                                {{ $errors->first('orgao_emissor_requerente') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-4 form-group">
                    <label for="data_emissao_requerente">Data Emissão:</label>
                    <input type="date" name="data_emissao_requerente" 
                        id="data_emissao_requerente"
                        max="{{ date('Y-m-d') }}" 
                        class="form-control form-control-sm {{ ($errors->has('data_emissao_requerente') ? 'is-invalid' : '') }}"
                        value="{{ old('data_emissao_requerente') }}" />

                        @if ($errors->has('data_emissao_requerente'))
                            <div class="invalid-feedback">
                                {{ $errors->first('data_emissao_requerente') }}
                            </div>
                        @endif
                </div>
            </div> 

            <div class="form-row">
                <div class="col-lg-4 form-group">
                    <label for="cpf_requerente">CPF:</label>
                    <input type="text" name="cpf_requerente" id="cpf_requerente" 
                        class="form-control form-control-sm mask-cpf {{ ($errors->has('cpf_requerente') ? 'is-invalid' : '') }}" 
                        value="{{ old('cpf_requerente') }}" maxlength="20" />

                        @if ($errors->has('cpf_requerente'))
                            <div class="invalid-feedback">
                                {{ $errors->first('cpf_requerente') }}
                            </div>
                        @endif
                </div>

                <div class="col-lg-8 form-group">
                    <label for="email_requerente">E-mail:</label>
                    <input type="email" name="email_requerente" id="email_requerente" 
                        class="form-control form-control-sm {{ ($errors->has('email_requerente') ? 'is-invalid' : '') }}" 
                        value="{{ old('email_requerente') }}" maxlength="100" />

                        @if ($errors->has('email_requerente'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email_requerente') }}
                            </div>
                        @endif
                </div>
            </div>
        </div>
        
        <div class="col-lg-6">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Envio de Documentos:</h5>

            <div class="form-row">
                <div class="col-lg-12">
                    <div class="alert alert-info">
                        <i class="fa fa-volume-up"></i> OBS: O tamanho máximo dos arquivos é de 2Mb.
                    </div>
                </div>
                
                <div class="col-lg-6 form-group">
                    <label for="documento_representacao_legal"><i class="fa fa-paperclip"></i> Anexar Representação Legal:</label>
                    <input type="file" name="documento_representacao_legal" 
                        id="documento_representacao_legal" />
                </div>

                <div class="col-lg-12">
                    <button class="btn btn-primary btn-enviar-documento" 
                        data-nome_input="documento_representacao_legal" 
                        data-descricao="Representação Legal"
                        data-nome_sessao="documentos_requerente"
                        data-conteudo_documentos="conteudo_documentos_requerente"
                        data-id_tabela="documentos_requerente"
                        data-pasta="requerente">
                        Enviar Documento <i class="fa fa-send"></i>
                    </button>
                </div>
            </div>    
        </div>  
    </div>

    <hr/>

    <div class="row">
        <div class="col-lg-12 form-group">
            <h5><i class="fa fa-folder-open mt-3 mb-3"></i> Documentos Enviados:</h5>

            <div class="conteudo_documentos_requerente">
                @include('cadastro.cgm.requerente.tabela_documentos')
            </div>
        </div>
    </div>
</div>
