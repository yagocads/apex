@extends('layouts.tema_principal')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center">
            <div class="col-lg-6">

                @if (session()->has('usuarioCadastradoComSucesso'))
                    <div class="alert alert-success">
                        {{ session('usuarioCadastradoComSucesso') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h4><i class="fa fa-lock"></i> {{__('Faça login na sua conta')}}</h4>
                    </div>

                    <div class="card-body">
                        <form class="" method="POST" action="{{ route('login') }}" id="frm_login" autocomplete="off">
                            @csrf

                            <div class="form-group">
                                <label for="cpf">{{__('Digite seu CPF')}}</label>
                                <input type="text" name="cpf" id="cpf" class="form-control {{ $errors->has('cpf') ? ' is-invalid' : '' }}" value="{{ $cpf ?? old('cpf') }}" required="true" {{ !$cpf ? 'autofocus' : '' }} />

                                @if ($errors->has('cpf'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('cpf') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password">{{__('Digite sua Senha')}}</label>
                                <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required="true" {{ $cpf ? 'autofocus' : '' }} />

                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                {!! $showRecaptcha !!}

                                @if ($errors->has('g-recaptcha-response'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" id="btn_submit" class="btn btn-primary"><i class="fa fa-lock"></i> {{__('Acessar')}}</button>
                        </form>

                        <p class="mt-3">
                            <a href="{{ route('password.request', ['cpf' => $cpf]) }}">{{__('Esqueceu a sua senha?')}}</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="modal-exigencias-apex" data-backdrop="static">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">{{__('Confirmação')}}</h4>
                    </div>

                    <div class="modal-body">
                        <div class="form-check mb-3">
                            <label class="form-check-label">
                                <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                                {{__('Possuo conhecimento da legislação vigente, mormente quanto à não existência de fato impeditivo de sua participação no processo')}}
                            </label>
                        </div>

                        <div class="form-check mb-3">
                            <label class="form-check-label">
                                <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                                {{__('Estou ciente das regras de uso do Portal de Aquisições')}}
                            </label>
                        </div>

                        <div class="form-check mb-3">
                            <label class="form-check-label">
                                <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                                {{__('Sou integralmente responsável pelo uso correto e adequado das minhas credenciais de acesso')}}
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" name="exigencias[]" class="form-check-input exigencias">
                                {{__('Estou em dia com as obrigações fiscais e sociais exigidas em legislação para participar de um processo de aquisição')}}
                            </label>
                        </div>

                        <button class="btn btn-primary btn-concordo-exigencias mt-3">{{__('Concordo')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('post-script')

<script>
    var tmpcpf = $("#cpf").val()
    $("#btn_submit").prop('disabled', true);

    function onReCaptchaTimeOut() {
        console.log("Bloquear Botão");
        $("#btn_submit").prop('disabled', true);
    }
    function onReCaptchaSuccess() {
        console.log("Liberar Botão");
        $("#btn_submit").prop('disabled', false);
    }
    function onReCaptchaError() {
        console.log("Erro no captcha");
        $("#btn_submit").prop('disabled', true);
    }

    jQuery(document).ready(function () {
        $('#cpf').mask('00.000.000/0000-00')
        var cpf_opt = {
            onKeyPress: function (cpf, e, field, cpf_opt) {
                console.log("Keypress: " + cpf.length)
                var masks = ['00.000.000/0000-00', '000.000.000-00'];
                var mask = (cpf.length > 14 || cpf.length == 0) ? masks[0] : masks[1];
                $('#cpf').mask(mask, cpf_opt);
            }
        };

        if (typeof ($("#cpf")) !== "undefined") {
            var masks = ['00.000.000/0000-00', '000.000.000-00'];
            var mask = ($("#cpf").val().length > 14 || $("#cpf").val().length == 0) ? masks[0] : masks[1];
            $("#cpf").mask(mask, cpf_opt);
        }

        $("#cpf").val(tmpcpf);

    });

    $(function() {
        $('#frm_login').on('submit', function(e) {
            e.preventDefault();
            $('#modal-exigencias-apex').modal('show');
        });

        $('.btn-concordo-exigencias').on('click', function() {

            const exigencias = $('.exigencias');
            let existeAlgumTermoNaoCheck = false;

            let checkSerNotificado = $(this).data('notificar');

            exigencias.each(function() {
                let exigenciaCheck = $(this).is(':checked');

                if (!exigenciaCheck) {
                    existeAlgumTermoNaoCheck = true;
                }
            });

            if (existeAlgumTermoNaoCheck) {
                alert("{{__('Atenção: Para continuar é necessário concordar com todos os termos.')}}");
                existeAlgumTermoNaoCheck = false;
            } else {
                document.getElementById("frm_login").submit();
            }
        });
    });
</script>

@endsection
