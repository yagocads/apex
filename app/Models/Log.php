<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';

    public $timestamps = false;

    public static function criarLog(string $cpf, string $descricao): void
    {
        $log = new Log();
        $log->cpf = $cpf;
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->descricao = $descricao;
        $log->data = date('Y-m-d H:i:s');
        $log->save();
    }
}
