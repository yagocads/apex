@extends('layouts.tema_principal')

@section('content')

<style>
    .sidebar li:hover {
        background-color: #F7F7F7 !important;
        cursor: pointer;
    }

    .sidebar li a {
        color: #000;
        text-decoration: none;
    }

    .nav-link.active {
        background-color: #DE182A !important;
        color: #FFF;
    }

    .card-header h5 {
        letter-spacing: 3px;
    }
</style>

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12 mb-3">
            <h2><i class="fa fa-book"></i> Documentação do Layout Portal SIM</h2>
        </div>

        <div class="col-lg-3">
            <ul class="nav nav-pills list-group text-center sidebar mb-3 shadow-sm">
                <li class="nav-item list-group-item"><a class="nav-link active" data-toggle="pill" href="#botoes">Botões</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#tabelas">Tabelas</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#agrupamentos">Agrupamentos</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#inputs">Inputs (Formulário)</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#cards">Cards</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#icones">Icones</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#alertas">Alertas</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#modal">Modal</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#confirmacao_processo">Confirmação do Processo</a></li>
                <li class="nav-item list-group-item"><a class="nav-link" data-toggle="pill" href="#exigencia_processo">Exigências do Processo</a></li>
            </ul>
        </div>

        <div class="col-lg-9">
            <div class="tab-content">
                <div id="botoes" class="tab-pane active show fade mb-3">
                    @include('documentacao_layout.botoes')
                </div>

                <div id="tabelas" class="tab-pane fade mb-3">
                    @include('documentacao_layout.tabelas')
                </div>

                <div id="agrupamentos" class="tab-pane fade mb-3">
                    @include('documentacao_layout.agrupamentos')
                </div>

                <div id="inputs" class="tab-pane fade mb-3">
                    @include('documentacao_layout.inputs')
                </div>

                <div id="cards" class="tab-pane fade">
                    @include('documentacao_layout.cards')
                </div>

                <div id="icones" class="tab-pane fade">
                    @include('documentacao_layout.icones')
                </div>

                <div id="alertas" class="tab-pane fade">
                    @include('documentacao_layout.alertas')
                </div>

                <div id="modal" class="tab-pane fade">
                    @include('documentacao_layout.modal')
                </div>

                <div id="confirmacao_processo" class="tab-pane fade">
                    @include('documentacao_layout.confirmacao_processo')
                </div>

                <div id="exigencia_processo" class="tab-pane fade">
                    @include('documentacao_layout.exigencia_processo')
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
