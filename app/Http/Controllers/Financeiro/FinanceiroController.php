<?php

namespace App\Http\Controllers\Financeiro;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Cidadao\CgmController;
use App\Models\DividaAtiva;
use App\Models\DividaAtivaDocumento;
use App\Support\FinanceiroSupport;
use Auth;
use Illuminate\Support\Facades\DB;

class FinanceiroController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->financial = new FinanceiroSupport;

        // Definição das contantes para acesso à API e-cidade
        if (!defined('API_GFD'))   define('API_GFD',  '/integracaoGeralFinanceiraDebitos.RPC.php');
        if (!defined('API_URL_GFD'))   define('API_URL_GFD',  API_URL);
    }

	public function selecaoDebito(){
		return view('financeiro.selecao_debito', ["pagina" => "Seleção de Débito"] );
	}

    public function debitosAbertos(){
        return view('financeiro.debitos_abertos', ["pagina" => "Débitos em Aberto", "cpf" => Auth::user()->cpf, "nome" => Auth::user()->name ] );
    }

    public function relatorioDebitosAbertos(Request $request)
    {
        // dd($request);
        if($request->matricula != ""){
            $matricula_inscricao = $request->matricula;
        }
        if($request->inscricao != ""){
            $matricula_inscricao = $request->inscricao;
        }

        $registros = $this->integracaoPesquisaDebitos($request);

        if(isset($registros->registros) && isset($registros->registros_unicos)){

            if(is_array($registros->registros) && is_array($registros->registros_unicos)){
                return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "registros" => $registros->registros, "registros_unicos" => $registros->registros_unicos, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
            } else {
                if(!is_array($registros->registros) && is_array($registros->registros_unicos)){
                    return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "registros_unicos" => $registros->registros_unicos, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
                }
                if(is_array($registros->registros) && !is_array($registros->registros_unicos)){
                    return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "registros" => $registros->registros, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
                }
                if(!is_array($registros->registros) && !is_array($registros->registros_unicos)){
                    return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
                }
            }
        }

        if(isset($registros->registros) && !isset($registros->registros_unicos)){
            if(is_array($registros->registros)){
                return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "registros" => $registros->registros, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
            }else{
                return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
            }
        }

        if(!isset($registros->registros) && isset($registros->registros_unicos)){
            if(is_array($registros->registros_unicos)){
                return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "registros_unicos" => $registros->registros_unicos, "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
            }
            else{
                return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
            }
        }

        if(!isset($registros->registros) && !isset($registros->registros_unicos)){
            return view('financeiro.relatorio_debitos_abertos', ["pagina" => "Débitos em Aberto", "tipo_pesquisa" => $request->tipo_pesquisa, "tipo_debito" => $request->tipo_debito, "matricula_inscricao" => $matricula_inscricao ] );
        }
    }

	public function geralFinanceira(){
		return view('financeiro.geral_financeira', ["pagina" => "Geral Financeira", "cpf" => Auth::user()->cpf, "nome" => Auth::user()->name ] );
	}

	public function pagamentosEfetuados(){
		return view('financeiro.pagamentos_efetuados', ["pagina" => "Pagamentos Efetuados", "cpf" => Auth::user()->cpf, "nome" => Auth::user()->name ] );
	}

	public function extratoFinanceiro(){
		return view('financeiro.extrato_financeiro', ["pagina" => "Extrato Financeiro", "cpf" => Auth::user()->cpf, "nome" => Auth::user()->name ] );
	}

    public function filtroExtratoFinanceiro(Request $request)
    {
        $request->tipo == 'm'  ? $value = $request->matricula : $value = $request->inscricao ;

        return view('financeiro.filtro_extrato_financeiro', ["pagina" => "Extrato Financeiro", "cpf" => Auth::user()->cpf, "nome" => Auth::user()->name, "tipo" => $request->tipo , "value" => $value ] );
    }

	public function integracaoPesquisaTipoDebitos(Request $request){
        if(isset($request->cpf)){
            $cpfUsuario = str_replace(".", "",  $request->cpf);
            $cpfUsuario = str_replace("-", "",  $cpfUsuario);
            $cpfUsuario = str_replace("/", "",  $cpfUsuario);
            $cpfUsuario = str_replace(" ", "",  $cpfUsuario);

            if($request->tipo == 'i'){
                $i_matricula_inscricao = $request->inscricao;
            }
            if($request->tipo == 'm'){
                $i_matricula_inscricao = $request->matricula;
            }

            $data1 = [
                'sExec'         => 'integracaopesquisatipodebitos',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_matricula_inscricao' => $i_matricula_inscricao,
                'c_tipo_pesquisa' => $request->tipo,
                'sTokeValiadacao'   => API_TOKEN,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json'=>$json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));


            if( USAR_SSL ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_API);
            }

            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            return $response;
        }
	}

    public function integracaoPesquisaDebitos(Request $request)
    {
        if(isset($request->cpf)){
            $cpfUsuario = remove_character_document($request->cpf);

            if($request->tipo_pesquisa == 'i'){
                $i_matricula_inscricao = $request->inscricao;
            }
            if($request->tipo_pesquisa == 'm'){
                $i_matricula_inscricao = $request->matricula;
            }

            $data1 = [
                'sExec'         => 'integracaopesquisadebitos',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_matricula_inscricao' => $i_matricula_inscricao,
                'c_tipo_pesquisa' => $request->tipo_pesquisa,
                'i_tipo_debito' => $request->tipo_debito,
                'sTokeValiadacao'   => API_TOKEN,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json'=>$json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));


            if( USAR_SSL ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_API);
            }

            $response = json_decode(curl_exec($ch));

            $err = curl_error($ch);
            curl_close($ch);

            if(isset($response->aDebitosUnicosEncontrados) && isset($response->aDebitosNormaisEncontrados)){
                if ($request->tipo_debito == '86') {

                    if ($response->aDebitosUnicosEncontrados != '') {
                        $list = [];
                        foreach($response->aDebitosUnicosEncontrados as $parcelas){
                            array_push($list,$parcelas->k00_numpar);
                        }

                        if (is_array($list)) {
                            $parcela = $list[0];
                            $collection = collect($response->aDebitosNormaisEncontrados);
                            $result = $collection->filter(function ($value, $key)  use ($parcela) {
                                return $value->k00_numpar < $parcela;
                            });
                            $response->aDebitosNormaisEncontrados = json_decode($result);
                        }
                    }
                }

                return view('financeiro.relatorio_debitos_abertos', [ 'pagina' => 'Consulta Geral Financeira - Débitos Abertos', 'registros_unicos' => $response->aDebitosUnicosEncontrados, 'registros' => $response->aDebitosNormaisEncontrados ]);
            }
            if(isset($response->aDebitosUnicosEncontrados) && !isset($response->aDebitosNormaisEncontrados)){
                return view('financeiro.relatorio_debitos_abertos', [ 'pagina' => 'Consulta Geral Financeira - Débitos Abertos', 'registros_unicos' => $response->aDebitosUnicosEncontrados ]);
            }
            if(!isset($response->aDebitosUnicosEncontrados) && isset($response->aDebitosNormaisEncontrados)){
                return view('financeiro.relatorio_debitos_abertos', [ 'pagina' => 'Consulta Geral Financeira - Débitos Abertos', 'registros' => $response->aDebitosNormaisEncontrados ]);
            }
            if(!isset($response->aDebitosUnicosEncontrados) && !isset($response->aDebitosNormaisEncontrados)){
                return view('financeiro.relatorio_debitos_abertos', [ 'pagina' => 'Consulta Geral Financeira - Débitos Abertos' ]);
            }
        }
	}

    public function integracaoReciboDebitos(Request $request)
    {
        $cpfUsuario = remove_character_document(Auth::user()->cpf);

        if(isset($request->registros_unicos)){

            if($request->numpar_unica > 0){
                $data1 = [
                    'sExec'         => 'integracaorecibodebitos',
                    'i_cpfcnpj'    => $cpfUsuario,
                    'i_matricula_inscricao' => $request->matricula_inscricao,
                    'c_tipo_pesquisa' => $request->tipo_pesquisa,
                    'i_tipo_debito' => $request->tipo_debito,
                    'a_lista_debitos' => [['i_numpre' => $request->numpre_unica, 'i_numpar' => $request->numpar_unica ]],
                    'i_numpre_unica' => '',
                    'i_numpar_unica' => '',
                    'd_data_vencimento' => $request->dataVencimento,
                    'sTokeValiadacao'   => API_TOKEN,
                ];
            }
            else{
                $data1 = [
                    'sExec'         => 'integracaorecibodebitos',
                    'i_cpfcnpj'    => $cpfUsuario,
                    'i_matricula_inscricao' => $request->matricula_inscricao,
                    'c_tipo_pesquisa' => $request->tipo_pesquisa,
                    'i_tipo_debito' => $request->tipo_debito,
                    'a_lista_inicial' => [],
                    'a_lista_debitos' => [],
                    'i_numpre_unica' =>  $request->numpre_unica,
                    'i_numpar_unica' => $request->numpar_unica,
                    'd_data_vencimento' => $request->dataVencimento,
                    'sTokeValiadacao'   => API_TOKEN,
                ];
            }

        }

        if(isset($request->registros_normais)){
            $i=0;
            $i_numpre = array();
            $i_numpar = array();

            if ($request->tipo_debito == 34) {

                foreach($request->registros_normais as $registros_normais){
                    $dados = explode("_", $registros_normais);
                    $i_inicial[$i] = $dados[0];
                    $i++;
                }

                $a_lista_inicial = [];
                $i=0;
                foreach($i_inicial as $value){
                    $a_lista_inicial[$i]['i_inicial'] = $value;
                    $i++;
                }


                $data1 = [
                    'sExec'         => 'integracaorecibodebitos',
                    'i_cpfcnpj'    => $cpfUsuario,
                    'i_matricula_inscricao' => $request->matricula_inscricao,
                    'c_tipo_pesquisa' => $request->tipo_pesquisa,
                    'i_tipo_debito' => $request->tipo_debito,
                    'a_lista_inicial' => $a_lista_inicial,
                    'a_lista_debitos' => $a_lista_inicial,
                    'd_data_vencimento' => $request->dataVencimento,
                    'sTokeValiadacao'   => API_TOKEN,
                ];

            } else {
                foreach($request->registros_normais as $registros_normais){
                    $dados = explode("_",$registros_normais);
                    $i_numpre[$i] = $dados[0];
                    $i_numpar[$i] = $dados[1];
                    $i++;
                }

                $numpre_numpar = array();
                $i=0;
                foreach($i_numpre as $numpre){
                    $numpre_numpar[$i]['i_numpre'] = $numpre;
                    $i++;
                }
                $i=0;
                foreach($i_numpar as $numpar){
                    $numpre_numpar[$i]['i_numpar'] = $numpar;
                    $i++;
                }

                if ($request->tipo_debito == 86 && $request->desconto_total != '0') {
                    $data1 = [
                        'sExec'         => 'integracaorecibodebitos',
                        'i_cpfcnpj'    => $cpfUsuario,
                        'i_matricula_inscricao' => $request->matricula_inscricao,
                        'c_tipo_pesquisa' => $request->tipo_pesquisa,
                        'i_tipo_debito' => $request->tipo_debito,
                        'a_lista_inicial' => [],
                        'a_lista_debitos' => [],
                        'i_numpre_unica' => $dados[0],
                        'i_numpar_unica' => $dados[1],
                        'd_data_vencimento' => $request->dataVencimento,
                        'sTokeValiadacao'   => API_TOKEN,
                    ];

                } else {
                    $data1 = [
                        'sExec'         => 'integracaorecibodebitos',
                        'i_cpfcnpj'    => $cpfUsuario,
                        'i_matricula_inscricao' => $request->matricula_inscricao,
                        'c_tipo_pesquisa' => $request->tipo_pesquisa,
                        'i_tipo_debito' => $request->tipo_debito,
                        'a_lista_debitos' => $numpre_numpar,
                        'i_numpre_unica' => '',
                        'i_numpar_unica' => '',
                        'd_data_vencimento' => $request->dataVencimento,
                        'sTokeValiadacao'   => API_TOKEN,
                    ];
                }
            }
        }
            $json_data = json_encode($data1);
            $data_string = ['json'=>$json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));


            if( USAR_SSL ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_API);
            }
            
            $response = json_decode(curl_exec($ch));
            $err = curl_error($ch);
            curl_close($ch);

            $tipo = "application/pdf";
            $arquivo = $response->sUrl;
            $vet_arquivo = explode("/",$arquivo);
            $novoNome = "DebitosAbertos.pdf";

            ini_set('max_execution_time', 60);
            stream_context_set_default([
                'ssl' => [
                    'verify_peer' => SSL_API_VERIFICA,
                    'verify_peer_name' => SSL_API_VERIFICA,
                ]
            ]);
            
            //Log IPTU
            if(Auth::user()->cpf){
                $data = DB::connection('mysql')->table('log_iptu')->insertGetId([
                    'cpf_cnpj'  => Auth::user()->cpf,
                    'matricula' => $request->matricula_inscricao,
                    'data'      => date("Y-m-d H:i:s"),
                ]);
            }
            
            $tempImage = tempnam(sys_get_temp_dir(), $response->sUrl);
            copy($response->sUrl, $tempImage);

            $headers = array(
                'Content-Type: application/pdf',
                );

            return response()->download($tempImage, $novoNome, $headers);

	}

    public function integracaoRelatorioTotalDebitos(Request $request){

        if(isset($request->cpf)){
            $cpfUsuario = str_replace(".", "",  $request->cpf);
            $cpfUsuario = str_replace("-", "",  $cpfUsuario);
            $cpfUsuario = str_replace("/", "",  $cpfUsuario);
            $cpfUsuario = str_replace(" ", "",  $cpfUsuario);

            if($request->tipo == 'i'){
                $i_codigo_pesquisa = $request->inscricao;
            }
            else{
                $i_codigo_pesquisa = $request->matricula;
            }

            $data1 = [
                'sExec'         => 'integracaorelatoriototaldebitos',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_codigo_pesquisa' => $i_codigo_pesquisa,
                'c_tipo_pesquisa' => $request->tipo,
                'c_tipo_retatorio' => $request->tipo_relatorio,
                'sTokeValiadacao'   => API_TOKEN,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json'=>$json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));

            if( USAR_SSL ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_API);
            }

            $response = json_decode(curl_exec($ch));
            $err = curl_error($ch);
            curl_close($ch);

            if(isset($request->tipo_relatorio)){
                if(isset($response->sUrl)){
                    $tipo = "application/pdf";
                    $arquivo = $response->sUrl;
                    $vet_arquivo = explode("/",$arquivo);
                    if($request->tipo_relatorio == 'c'){
                        $novoNome = "ExtratoFinanceiroAnalítico.pdf";
                    }
                    else{
                        $novoNome = "ExtratoFinanceiroSintético.pdf";
                    }

                    ini_set('max_execution_time', 60);
                    stream_context_set_default([
                        'ssl' => [
                            'verify_peer' => SSL_API_VERIFICA,
                            'verify_peer_name' => SSL_API_VERIFICA,
                        ]
                    ]);

                    $tempImage = tempnam(sys_get_temp_dir(), $response->sUrl);
                    copy($response->sUrl, $tempImage);

                    $headers = array(
                        'Content-Type: application/pdf',
                        );

                    return response()->download($tempImage, $novoNome, $headers);

                } else {

                    // switch ($response->eErro) {
                    //     case 'E10_GFD_RTOTADEB_DEBITOS_NAO_ENCONTADO':
                    //         $msg = 'Débitos não encontratos para esse código!';
                    //         break;
                    //     case 'E09_GFD_RTOTADEB_TIPO_DEBITOS_NAO_LIBERADOS_WEB':
                    //         $msg = 'Debitos não estão liberados para serem emitidos pela WEB!';
                    //         break;
                    //     case 'E14_GFD_RTOTADEBF_CERTIDAO_NAO_GERADA':
                    //         $msg = 'Erro ao gerar relatório de pagamentos!';
                    //         break;
                    //     case 'E11_GFD_RTOTADEB_INFO_CGM_NAO_ENCONTRADO_{M/I}	':
                    //         $msg = 'CPF/CNPJ infromado não encontrado no ecidade!';
                    //         break;
                    //     default:
                    //         $msg = 'CPF/CNPJ infromado não encontrado no ecidade!';
                    //         break;
                    // }

                    // return response()->json(['error' => convert_accentuation($response->sMessage)]);
                    return response()->json(['error' => $response->sMessage]);
                }

            } else{
                return $response->sUrl;
            }

        }
    }

    public function integracaoPagamentosEfetuados(Request $request){

        if(isset($request->cpf)){

            $cpfUsuario = remove_character_document($request->cpf);

            if(isset($request->inscricao) && $request->inscricao != ''){
                $c_tipo_pesquisa = 'i';
                $i_matricula_inscricao = $request->inscricao;
            }
            if(isset($request->matricula) && $request->matricula != ''){
                $c_tipo_pesquisa = 'm';
                $i_matricula_inscricao = $request->matricula;
            }

            $data1 = [
                'sExec'         => 'integracaopagamentosefetuados',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_matricula_inscricao' => $i_matricula_inscricao,
                'c_tipo_pesquisa' => $c_tipo_pesquisa,
                'i_ano_pesq' => isset($request->exercicio) ? $request->exercicio : '',
                'd_dt_inicial_pesq' => isset($request->periodo_inicial) ? $request->periodo_inicial : '',
                'd_dt_final_pesq' =>  isset($request->periodo_final) ? $request->periodo_final : '',
                'sTokeValiadacao'   => API_TOKEN,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json'=>$json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));


            if( USAR_SSL ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_API);
            }

            $response = curl_exec($ch);

            $err = curl_error($ch);
            curl_close($ch);

            $data = json_decode($response);

            $total = 0;

            if($data->iStatus == 1){
                foreach($data->aPagamentosEfetuadoss as $value){
                    $total += $value->k00_valor;
                }

                return view('financeiro/lista_pagamentos')->with(['data' => json_decode($response),'request' => $request,'total' => $total,'pagina' => "Pagamentos Efetuados"]);
            } else {
                return response()->json(['error' => $data->eErro]);
            }
        }
    }

    public function diaUtilEcidade(Request $request)
    {
        $result = $this->financial->verificaDiaUtil($request->date);
        return response()->json($result->bDiaUtil, 200);
    }

    public function anosPagamento(Request $request)
    {
        isset($request->matricula) ? $matInsc = $request->matricula :  $matInsc = $request->inscricao;
        isset($request->matricula) ? $tipoPesquisa = 'm' :  $tipoPesquisa = 'i';

        $cpfUsuario = remove_character_document($request->cpf);
        $years = $this->financial->verificaAnosPg($matInsc,$cpfUsuario,$tipoPesquisa);

        return response()->json($years, 200);
    }

    public function integracaoRelatorioPagamentosEfetuados(Request $request)
    {
        if(isset($request->cpf)){
            isset($request->matricula) ? $matInsc = $request->matricula :  $matInsc = $request->inscricao;
            isset($request->matricula) ? $tipoPesquisa = 'm' :  $tipoPesquisa = 'i';

            $cpfUsuario = remove_character_document($request->cpf);

            $data1 = [
                'sExec'         => 'integracaorelatoriopagamentosefetuados',
                'i_cpfcnpj'    => $cpfUsuario,
                'i_matricula_inscricao' => $matInsc,
                'c_tipo_pesquisa' => $tipoPesquisa,
                'sTokeValiadacao'   => API_TOKEN,
            ];

            $json_data = json_encode($data1);
            $data_string = ['json'=>$json_data];

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => API_URL_GFD . API_GFD,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data_string),
                CURLOPT_HTTPHEADER => array(
                    'Content-Length: ' . strlen(http_build_query($data_string))
                ),
            ));


            if( USAR_SSL ){
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, SSL_API_VERIFICA);
                curl_setopt($ch, CURLOPT_CAINFO,  getcwd().'/cert/' . SSL_API);
            }

            $response = json_decode(curl_exec($ch));
            $err = curl_error($ch);
            curl_close($ch);
            if(isset($response->sUrl)){
                $tipo = "application/pdf";
                $arquivo = $response->sUrl;
                $vet_arquivo = explode("/",$arquivo);
                $novoNome = "PagamentosEfetuados.pdf";

                ini_set('max_execution_time', 60);
                stream_context_set_default([
                    'ssl' => [
                        'verify_peer' => SSL_API_VERIFICA,
                        'verify_peer_name' => SSL_API_VERIFICA,
                    ]
                ]);

                $tempImage = tempnam(sys_get_temp_dir(), $response->sUrl);
                copy($response->sUrl, $tempImage);

                $headers = array(
                    'Content-Type: application/pdf',
                    );

                return response()->download($tempImage, $novoNome, $headers);

            }
            else{
                return "";
            }

        }
    }

    public function pesquisarMatricula(){
        return "<div class='callout-white'>
                <div class'table_head'>
                    <div class='row'>
                        <div class='col-lg-3'>
                            <p><strong>Matrícula</strong></p>
                        </div>
                        <div class='col-lg-3'>
                            <p><strong>Tipo</strong></p>
                        </div>
                        <div class='col-lg-3'>
                            <p><strong>Planta/Quadra/Lote</strong></p>
                        </div>
                        <div class='col-lg-3'>
                            <p><strong>Ações</strong></p>
                        </div>
                    </div>
                </div>
                <div class='table_body'></div>
            </div>";
    }

    public function pesquisarInscricao(){
        return "<div class='callout-white'>
                <div class'table_head'>
                    <div class='row'>
                        <div class='col-lg-3'>
                            <p><strong>Inscrição</strong></p>
                        </div>
                        <div class='col-lg-3'>
                            <p><strong>Nome</strong></p>
                        </div>
                        <div class='col-lg-3'>
                            <p><strong>Endereço</strong></p>
                        </div>
                        <div class='col-lg-3'>
                            <p><strong>Ações</strong></p>
                        </div>
                    </div>
                </div>
                <div class='table_body'></div>
            </div>";
    }
    /**
     * parcelarDivida retona formulario de cadastro
     *
     * @return void
     */
    public function parcelarDivida()
    {
        return view('financeiro.parcelar_divida', ["pagina" => "Parcelamento de dívida"] );
    }

    public function acompanhamento()
    {
        return view('financeiro.acompanhamento', ["pagina" => "Acompanhamento de dívida"] );
    }

    /**
     * salvarParcelarDivida salva os dados do formulário divida ativa
     *
     * @param  mixed $request
     * @return void
     */
    public function salvarParcelarDivida(Request $request)
    {
        if(!$request->session()->exists('documentos_processo')){
            return response()->json(['error' => 'Favor enviar documentos obrigatórios do processo.'], 200);
        }
        if (isset($_FILES['documento']) && !empty($_FILES['documento']['name']))
        {
            $document = remove_character_document(Auth::user()->cpf);
            $nameRepLegal =  $document . "_representacao_legal_" . uniqid(). '.pdf';
            $namecCpf =  $document . "_cpf_" . uniqid(). '.pdf' ;
            $nameRg =  $document . "_rg_" . uniqid(). '.pdf';

            $file = file_get_contents($_FILES['documento']['tmp_name']);
            $fileRg = file_get_contents($_FILES['rg_rep_doc']['tmp_name']);
            $fileCpf = file_get_contents($_FILES['cpf_rep_doc']['tmp_name']);
            $rgRep = base64_encode($fileRg);
            $cpfRep = base64_encode($fileCpf);
            $docRep = base64_encode($file);
            $docs = array(
                ['name' => $nameRepLegal, 'file' => $docRep, 'description' => 'documento representação legal'],
                ['name' => $nameRg, 'file' => $rgRep, 'description' => 'rg' ],
                ['name' => $namecCpf, 'file' => $cpfRep, 'description' => 'cpf']
            );
        }

        get_guia_services() == 1 ? $respPreenchimento = 'Próprio' : $respPreenchimento = 'Representante Legal' ;

        $divida = DB::connection('integracao')->table('lecom_da_parcelamento')->insertGetId([
                    'tipo_pessoa'           => get_guia_services(),
                    'resp_preenchimento'    => $respPreenchimento,
                    'nome_rep_legal'        => $request->representante,
                    'celular_rep_legal'     => $request->cel_rep,
                    'telefone_rep_legal'    => $request->tel_rep,
                    'cpf_rep_legal'         => $request->cpf_rep,
                    'email_rep_legal'       => $request->email_rep,
                    'nome_razao_social'     => $request->nome,
                    'cpf_cnpj'              => $request->document,
                    'telefone_contribuinte' => $request->telefone,
                    'celular_contribuinte'  => $request->celular,
                    'email_contribuinte'    => $request->email,
                    'numero_parcelas_sol'   => $request->parcela,
                    'tipo_imposto'          => $request->tipo_tributo,
                    'descricao_divida'      => $request->div_ativa,
                    'data_cadastro'         => date('Y-m-d H:i:s')
                ]);

        if ($divida) {
            session()->forget("documentos_processo");
            foreach($docs as $doc){
                DB::connection('integracao')->table('lecom_da_parcelamento_documentos')->insertGetId([
                    'cpf'             => Auth::user()->cpf,
                    'descricao'       => $doc['description'],
                    'nome_documento'  => $doc['name'],
                    'documento'       => $doc['file']
                ]);
            }
            //atualizo informando o id
            DB::connection('integracao')->table('lecom_da_parcelamento_documentos')
              ->where('cpf', Auth::user()->cpf)
              ->where('id_da_parcelamento', null)
              ->update(['id_da_parcelamento' => $divida]);

            return json_encode('Sua solicitação foi recebida e em até 24 horas você receberá em seu e-mail o protocolo de abertura', JSON_UNESCAPED_UNICODE);

        } else {
            return response()->json(['error' => 'Houve um erro no envio de seus dados, favor tente mais tarde'], 200);
        }

    }

    public function salvarDocSessao(Request $request)
    {
        $extensoesPermitidas = ["pdf"];
        $extensao = $request->arquivo->extension();

        if (!in_array($extensao, $extensoesPermitidas)) {
            return ['error' => 'O arquivo deve ser uma imagem ou um pdf com as seguintes extensões (pdf).'];
        }

        $tamanhoArquivo = $request->arquivo->getClientSize();

        if ($tamanhoArquivo > 10024000) {
            return ['error' => 'O arquivo deve ter no máximo 10MB.'];
        }
        $nameFile = $request->processo == 'outros' ? $request->processo : 'fato_gerador';
        $cpf = remove_character_document(Auth::user()->cpf);
        $nomeNovoArquivo =  $cpf . "_" . remove_accentuation($nameFile) . "_" . uniqid() . '.' . $extensao;
        $nomeNovoArquivo = str_replace("-", "_", $nomeNovoArquivo);

        if (isset($_FILES['arquivo']) && !empty($_FILES['arquivo']['name']))
        {
            $file = file_get_contents($_FILES['arquivo']['tmp_name']);
            $doc = base64_encode($file);
            if ($doc) {
                //crio na base
                $data = DB::connection('integracao')->table('lecom_da_parcelamento_documentos')->insertGetId([
                    'cpf'             => Auth::user()->cpf,
                    'descricao'       => $request->processo == 'outros' ? $request->outro : $request->processo,
                    'nome_documento'  => $nomeNovoArquivo,
                    'documento'       => $doc
                ]);
            }
        }

        $upload = $request->arquivo->storeAs('documentos', $nomeNovoArquivo, 'ftp');

        if ($upload) {

            session()->push("documentos_processo", (object) [
                "processo" => $request->processo,
                "nome_original" => $request->arquivo->getClientOriginalName(),
                'id' => $data
            ]);

            return view('financeiro.tabela_documentos');
        } else {
            return ['error' => 'Não foi possível enviar o documento, procure a administração para resolver o problema.'];
        }
    }

    public function removeDocSessao($documento)
    {
        $documentos = session()->get("documentos_processo");

        if (!empty($documentos)) {
            //deleto da base
            DB::connection('integracao')->table('lecom_da_parcelamento_documentos')->delete($documento);

            foreach($documentos as $key => $value) {

                if (in_array($documento, (array) $value)) {
                    unset($documentos[$key]);
                    session()->forget("documentos_processo");
                    session()->put('documentos_processo', $documentos);
                }
            }
        }

        return view("financeiro.tabela_documentos");
    }
}
