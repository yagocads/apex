<div class="card">
    <div class="card-header">
        <h5>Botões</h5>
    </div>

    <div class="card-body">
        <h6><b>Cores utilizadas:</b></h6>
        <hr>
        <div class="mb-3">
            <p>Azul (Bom para selecionar algo, cadastrar algo no formulário, fazer download de algum arquivo etc...)</p>
            <button class="btn btn-primary">Conteúdo</button>
        </div>

        <div class="mb-3">
            <p>Verde (Bom para finalizar etapas, formulários, cadastrar algo, e também usado para alertas positivos etc...)</p>
            <button class="btn btn-success">Conteúdo</button>
        </div>

        <div class="mb-5">
            <p>Vermelho (Bom para cancelar etapas, excluir documentos, e também utilizado para alertas negativos etc...)</p>
            <button class="btn btn-danger">Conteúdo</button>
        </div>

        <h6><b>Tamanhos utilizados:</b></h6>
        <hr>
        <div class="row">
            <div class="col-lg-3 mb-3">
                <p>Tamanho pequeno:</p>
                <button class="btn btn-sm btn-primary">Conteúdo</button>
            </div>
            
            <div class="col-lg-3">
                <p>Tamanho grande:</p>
                <button class="btn btn-primary">Conteúdo</button>
            </div>
        </div>
    </div>
</div>