<div class="row">
    <div class="col-lg-12">
        <div class="banner mb-4">
            <img src="img/banner_mrc.png" alt="Maricá na rede da Cultura"
                title="Maricá na rede da Cultura" width="100%" />
        </div>

        <div class="mt-3 text-center">
            <div class="alert alert-warning">
                <p><b>Ainda não realizou o cadastro ?</b></p>
                <p><a href="{{ route("atualizaCGM") }}" class="btn btn-success">Clique aqui para se cadastrar</a></p>
                <a href="https://www.marica.rj.gov.br/2020/07/27/jom-1073/" target="_blank" class="d-block mt-3"><i class="fa fa-link"></i> Decreto Municipal n.º 570, de 27 de julho de 2020 - Cadastro Cultural Municipal</a>
                <a href="http://www.planalto.gov.br/ccivil_03/_ato2019-2022/2020/Lei/L14017.htm" target="_blank" class="d-block mt-3"><i class="fa fa-link"></i> Lei Aldir Blanc</a>
                <a  href="{{ asset('/docs/decreto_591-2020.pdf') }}" download="Decreto_591-2020" target="_blank" class="d-block mt-3"><i class="fa fa-link"></i> Decreto Municipal n.º 591/2020 - Regulamenta, no âmbito municipal, a Lei Federal n.º 14.017/2020 - Lei Aldir Blanc, que dispõe sobre as ações emergenciais destinadas ao setor cultural a serem adotadas durante o estado de calamidade pública</a>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h3>Consulta MRC</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form method="POST" action="{{ route('consultarsituacaomrc') }}">
                            @csrf

                            <div class="form-row">
                                <div class="col-lg-6 form-group">
                                    <label for="protocolo">Protocolo:</label>
                                    <input type="text" name="protocolo" id="protocolo"
                                        class="form-control form-control-sm"
                                        value="{{ old('protocolo') }}" required />
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="cpf_cnpj">Digite seu CPF/CNPJ:</label>
                                    <input type="text" name="cpf_cnpj" id="cpf_cnpj"
                                        class="form-control form-control-sm cpfOuCnpj"
                                        value="{{ old('cpf_cnpj') }}" required />
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-6 form-group">
                                    <label for="data_nascimento">Data de Nascimento:</label>
                                    <input type="date" name="data_nascimento" id="data_nascimento"
                                        class="form-control form-control-sm"
                                        value="{{ old('data_nascimento') }}"
                                        max="{{ date('Y-m-d') }}"
                                        />
                                </div>

                                <div class="col-lg-6 form-group">
                                    <label for="data_abertura">Data de Abertura:</label>
                                    <input type="date" name="data_abertura" id="data_abertura"
                                        class="form-control form-control-sm"
                                        value="{{ old('data_abertura') }}"
                                        max="{{ date('Y-m-d') }}" />
                                </div>
                            </div>

                            <button class="btn btn-primary mb-2">
                                Consultar <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @if (session()->has('solicitacao_em_abertura'))
            <div class="alert alert-info mt-3">
                <i class="fa fa-volume-up"></i> {{ session('solicitacao_em_abertura') }}
            </div>
        @endif

        @if (session()->has('solicitacao_nao_encontrada'))
            <div class="alert alert-danger mt-3">
                {{ session('solicitacao_nao_encontrada') }}
            </div>
        @endif
    </div>
</div>
